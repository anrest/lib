import { InjectionToken } from '@angular/core';
export declare abstract class ResponseCache {
    abstract has(id: string): boolean;
    abstract get(id: string): any;
    abstract add(id: string, data: any): any;
    abstract remove(id: string): any;
    abstract clear(): any;
    abstract addAlias(id: string, alias: string): any;
    replace(data: any, id: string): void;
}
export declare let ANREST_CACHE_SERVICES: InjectionToken<ResponseCache[]>;
