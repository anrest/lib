import { ResponseCache } from './response-cache';
export declare class NoResponseCache extends ResponseCache {
    has(id: string): boolean;
    add(id: string, data: any): void;
    clear(): void;
    get(id: string): void;
    remove(id: string): void;
    addAlias(id: string, alias: string): void;
    replace(data: any, id: string): void;
}
