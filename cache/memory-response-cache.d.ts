import { ResponseCache } from './response-cache';
import { ObjectCollector } from '../core/object-collector';
import { ApiConfig } from '../core/config';
export declare class MemoryResponseCache extends ResponseCache {
    private readonly collector;
    protected readonly config: ApiConfig;
    private times;
    private aliases;
    constructor(collector: ObjectCollector, config: ApiConfig);
    add(id: string, data: any): void;
    clear(): void;
    get(id: string): any;
    remove(id: string): void;
    addAlias(id: string, alias: string): void;
    has(id: string): boolean;
}
