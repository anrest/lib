import { ResponseCache } from './response-cache';
import { ObjectCollector } from '../core/object-collector';
import { ApiProcessor } from '../core/api.processor';
import { ApiConfig } from '../core/config';
import { DataInfoService } from '../core';
export declare class LocalStorageResponseCache extends ResponseCache {
    private collector;
    private processor;
    private info;
    private config;
    private tokenPrefix;
    constructor(collector: ObjectCollector, processor: ApiProcessor, info: DataInfoService, config: ApiConfig);
    get(id: string): any;
    add(id: string, data: any): void;
    addAlias(id: string, alias: string): void;
    has(id: string): boolean;
    remove(id: string): void;
    clear(): void;
    private doAdd(id, data, complete);
    private getNode(id);
    private getCollectionInfo(id);
    private setTimestamp(id);
    private getTimestamp(id);
    private removeTimestamp(id);
}
