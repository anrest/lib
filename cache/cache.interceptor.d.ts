import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiConfig, DataInfoService } from '../core';
import { ResponseCache } from './response-cache';
export declare class CacheInterceptor implements HttpInterceptor {
    protected readonly config: ApiConfig;
    private readonly info;
    private readonly cacheServices;
    constructor(config: ApiConfig, info: DataInfoService, cacheServices: ResponseCache[]);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
