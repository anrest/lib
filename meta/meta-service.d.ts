import { EntityMetaInfo, PropertyMetaInfo, MetaInfo, ServiceMetaInfo, EventListenerMetaInfo } from './meta-info';
export declare class MetaService {
    private static map;
    static get(key: any): any;
    static getByName(key: string): any;
    static getOrCreateForEntity(key: Function): EntityMetaInfo;
    static getOrCreateForEventListener(key: Function): EventListenerMetaInfo;
    static getOrCreateForProperty(key: any, property: string, type?: any): PropertyMetaInfo;
    static getOrCreateForHttpService(key: Function, entity: Function): ServiceMetaInfo;
    static set(meta: MetaInfo): void;
}
