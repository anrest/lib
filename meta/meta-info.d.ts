import { HttpHeaders } from '@angular/common/http';
export declare class MetaInfo {
    private _type;
    constructor(_type: Function);
    readonly type: any;
}
export declare class PropertyMetaInfo extends MetaInfo {
    isCollection: boolean;
    excludeWhenSaving: boolean;
}
export declare class EventListenerMetaInfo extends MetaInfo {
    events: {
        entity: Function;
        type: Function;
    }[];
}
export declare class EntityMetaInfo extends MetaInfo {
    name: string;
    id: string;
    service: Function;
    headers: {
        name: string;
        value: () => string;
        append: boolean;
        dynamic: boolean;
    }[];
    properties: {
        [index: string]: PropertyMetaInfo;
    };
    subresources: {
        [index: string]: {
            meta: EntityMetaInfo;
            path: string;
        };
    };
    body: (object: any) => string;
    cache: Function;
    getHeaders(object?: any): HttpHeaders;
    getPropertiesForSave(): any[];
}
export declare class ServiceMetaInfo extends MetaInfo {
    private _entityMeta;
    path: string;
    constructor(type: Function, _entityMeta: EntityMetaInfo);
    readonly entityMeta: EntityMetaInfo;
}
