(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs/operators'), require('@angular/common/http'), require('rxjs'), require('jwt-decode'), require('pluralize'), require('reflect-metadata')) :
    typeof define === 'function' && define.amd ? define('@anrest/lib', ['exports', '@angular/core', 'rxjs/operators', '@angular/common/http', 'rxjs', 'jwt-decode', 'pluralize', 'reflect-metadata'], factory) :
    (factory((global.anrest = global.anrest || {}, global.anrest.lib = {}),global.ng.core,global.rxjs.operators,global.ng.common.http,global.rxjs,null,null));
}(this, (function (exports,core,operators,http,rxjs,jwt_decode,pluralize) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ AnRestConfig = new core.InjectionToken('anrest.api_config');

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b)
            if (b.hasOwnProperty(p))
                d[p] = b[p]; };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m)
            return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length)
                    o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var EntityInfo = (function () {
        function EntityInfo(_path) {
            this._path = _path;
        }
        Object.defineProperty(EntityInfo.prototype, "path", {
            get: /**
             * @return {?}
             */ function () {
                return this._path;
            },
            enumerable: true,
            configurable: true
        });
        return EntityInfo;
    }());
    var CollectionInfo = (function () {
        function CollectionInfo(_path) {
            this._path = _path;
        }
        Object.defineProperty(CollectionInfo.prototype, "path", {
            get: /**
             * @return {?}
             */ function () {
                return this._path;
            },
            enumerable: true,
            configurable: true
        });
        return CollectionInfo;
    }());
    var DataInfoService = (function () {
        function DataInfoService() {
        }
        /**
         * @param {?} object
         * @return {?}
         */
        DataInfoService.prototype.get = /**
         * @param {?} object
         * @return {?}
         */
            function (object) {
                return Reflect.getMetadata('anrest:info', object);
            };
        /**
         * @param {?} object
         * @param {?} info
         * @return {?}
         */
        DataInfoService.prototype.set = /**
         * @param {?} object
         * @param {?} info
         * @return {?}
         */
            function (object, info) {
                Reflect.defineMetadata('anrest:info', info, object);
            };
        /**
         * @param {?} object
         * @return {?}
         */
        DataInfoService.prototype.getForCollection = /**
         * @param {?} object
         * @return {?}
         */
            function (object) {
                return /** @type {?} */ (this.get(object));
            };
        /**
         * @param {?} object
         * @return {?}
         */
        DataInfoService.prototype.getForEntity = /**
         * @param {?} object
         * @return {?}
         */
            function (object) {
                return /** @type {?} */ (this.get(object));
            };
        /**
         * @param {?} object
         * @param {?} info
         * @return {?}
         */
        DataInfoService.prototype.setForCollection = /**
         * @param {?} object
         * @param {?} info
         * @return {?}
         */
            function (object, info) {
                this.set(object, info);
            };
        /**
         * @param {?} object
         * @param {?} info
         * @return {?}
         */
        DataInfoService.prototype.setForEntity = /**
         * @param {?} object
         * @param {?} info
         * @return {?}
         */
            function (object, info) {
                this.set(object, info);
            };
        DataInfoService.decorators = [
            { type: core.Injectable },
        ];
        return DataInfoService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @template T
     */
    var /**
     * @template T
     */ Collection = (function (_super) {
        __extends(Collection, _super);
        function Collection(service, info) {
            var items = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                items[_i - 2] = arguments[_i];
            }
            var _this = _super.apply(this, __spread(items)) || this;
            _this.service = service;
            _this.info = info;
            Object.setPrototypeOf(_this, Collection.prototype);
            return _this;
        }
        /**
         * @template T
         * @param {?} c1
         * @param {?} c2
         * @return {?}
         */
        Collection.merge = /**
         * @template T
         * @param {?} c1
         * @param {?} c2
         * @return {?}
         */
            function (c1, c2) {
                var /** @type {?} */ info = new CollectionInfo(c1.info.path);
                info.type = c1.info.type;
                info.next = c2.info.next;
                info.previous = c1.info.previous;
                info.first = c1.info.first;
                info.last = c1.info.last;
                info.total = c1.info.total;
                info.page = c1.info.page;
                info.lastPage = c1.info.lastPage;
                var /** @type {?} */ c = new Collection(c1.service, info);
                c1.forEach(function (v) { return c.push(v); });
                c2.forEach(function (v) { return c.push(v); });
                return c;
            };
        Object.defineProperty(Collection.prototype, "total", {
            get: /**
             * @return {?}
             */ function () {
                return this.info.total;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Collection.prototype, "page", {
            get: /**
             * @return {?}
             */ function () {
                return this.info.page;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Collection.prototype, "pageTotal", {
            get: /**
             * @return {?}
             */ function () {
                return this.info.lastPage;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        Collection.prototype.first = /**
         * @return {?}
         */
            function () {
                return /** @type {?} */ (this.service.doGet(this.info.first));
            };
        /**
         * @return {?}
         */
        Collection.prototype.previous = /**
         * @return {?}
         */
            function () {
                return /** @type {?} */ (this.service.doGet(this.info.previous));
            };
        /**
         * @return {?}
         */
        Collection.prototype.next = /**
         * @return {?}
         */
            function () {
                return /** @type {?} */ (this.service.doGet(this.info.next));
            };
        /**
         * @return {?}
         */
        Collection.prototype.last = /**
         * @return {?}
         */
            function () {
                return /** @type {?} */ (this.service.doGet(this.info.last));
            };
        /**
         * @return {?}
         */
        Collection.prototype.firstPath = /**
         * @return {?}
         */
            function () {
                return this.info.first;
            };
        /**
         * @return {?}
         */
        Collection.prototype.previousPath = /**
         * @return {?}
         */
            function () {
                return this.info.previous;
            };
        /**
         * @return {?}
         */
        Collection.prototype.nextPath = /**
         * @return {?}
         */
            function () {
                return this.info.next;
            };
        /**
         * @return {?}
         */
        Collection.prototype.lastPath = /**
         * @return {?}
         */
            function () {
                return this.info.last;
            };
        /**
         * @return {?}
         */
        Collection.prototype.loadMore = /**
         * @return {?}
         */
            function () {
                var _this = this;
                return this.next().pipe(operators.map(function (result) { return Collection.merge(_this, result); }));
            };
        /**
         * @return {?}
         */
        Collection.prototype.hasMore = /**
         * @return {?}
         */
            function () {
                return this.hasNext();
            };
        /**
         * @return {?}
         */
        Collection.prototype.hasNext = /**
         * @return {?}
         */
            function () {
                return !!this.info.next;
            };
        /**
         * @return {?}
         */
        Collection.prototype.hasPrevious = /**
         * @return {?}
         */
            function () {
                return !!this.info.previous;
            };
        /**
         * @return {?}
         */
        Collection.prototype.hasFirst = /**
         * @return {?}
         */
            function () {
                return this.hasPrevious();
            };
        /**
         * @return {?}
         */
        Collection.prototype.hasLast = /**
         * @return {?}
         */
            function () {
                return this.hasNext();
            };
        return Collection;
    }(Array));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var BaseEvent = (function () {
        function BaseEvent(data, type) {
            this._data = data;
            this.type = type;
        }
        /**
         * @return {?}
         */
        BaseEvent.prototype.data = /**
         * @return {?}
         */
            function () {
                return this._data;
            };
        /**
         * @return {?}
         */
        BaseEvent.prototype.entity = /**
         * @return {?}
         */
            function () {
                return this.type || (Array.isArray(this._data) ? this._data[0].constructor : this._data.constructor);
            };
        return BaseEvent;
    }());
    var BeforeGetEvent = (function (_super) {
        __extends(BeforeGetEvent, _super);
        function BeforeGetEvent(path, type, filter) {
            var _this = _super.call(this, path, type) || this;
            _this._filter = filter;
            return _this;
        }
        /**
         * @return {?}
         */
        BeforeGetEvent.prototype.entity = /**
         * @return {?}
         */
            function () {
                return this.type;
            };
        /**
         * @return {?}
         */
        BeforeGetEvent.prototype.filter = /**
         * @return {?}
         */
            function () {
                return this._filter;
            };
        return BeforeGetEvent;
    }(BaseEvent));
    var AfterGetEvent = (function (_super) {
        __extends(AfterGetEvent, _super);
        function AfterGetEvent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return AfterGetEvent;
    }(BaseEvent));
    var BeforeSaveEvent = (function (_super) {
        __extends(BeforeSaveEvent, _super);
        function BeforeSaveEvent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return BeforeSaveEvent;
    }(BaseEvent));
    var AfterSaveEvent = (function (_super) {
        __extends(AfterSaveEvent, _super);
        function AfterSaveEvent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return AfterSaveEvent;
    }(BaseEvent));
    var BeforeRemoveEvent = (function (_super) {
        __extends(BeforeRemoveEvent, _super);
        function BeforeRemoveEvent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return BeforeRemoveEvent;
    }(BaseEvent));
    var AfterRemoveEvent = (function (_super) {
        __extends(AfterRemoveEvent, _super);
        function AfterRemoveEvent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return AfterRemoveEvent;
    }(BaseEvent));
    var /** @type {?} */ ANREST_HTTP_EVENT_LISTENERS = new core.InjectionToken('anrest.http_event_listeners');

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MetaInfo = (function () {
        function MetaInfo(_type) {
            this._type = _type;
        }
        Object.defineProperty(MetaInfo.prototype, "type", {
            get: /**
             * @return {?}
             */ function () {
                return this._type;
            },
            enumerable: true,
            configurable: true
        });
        return MetaInfo;
    }());
    var PropertyMetaInfo = (function (_super) {
        __extends(PropertyMetaInfo, _super);
        function PropertyMetaInfo() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return PropertyMetaInfo;
    }(MetaInfo));
    var EventListenerMetaInfo = (function (_super) {
        __extends(EventListenerMetaInfo, _super);
        function EventListenerMetaInfo() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.events = [];
            return _this;
        }
        return EventListenerMetaInfo;
    }(MetaInfo));
    var EntityMetaInfo = (function (_super) {
        __extends(EntityMetaInfo, _super);
        function EntityMetaInfo() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.id = 'id';
            _this.headers = [];
            _this.properties = {};
            _this.subresources = {};
            return _this;
        }
        /**
         * @param {?=} object
         * @return {?}
         */
        EntityMetaInfo.prototype.getHeaders = /**
         * @param {?=} object
         * @return {?}
         */
            function (object) {
                var /** @type {?} */ headers = new http.HttpHeaders();
                headers = headers.set('X-AnRest-Type', this.name);
                try {
                    for (var _a = __values(this.headers), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var header = _b.value;
                        if (!header.dynamic || object) {
                            headers = headers[header.append ? 'append' : 'set'](header.name, header.value.call(object));
                        }
                    }
                }
                catch (e_1_1) {
                    e_1 = { error: e_1_1 };
                }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return))
                            _c.call(_a);
                    }
                    finally {
                        if (e_1)
                            throw e_1.error;
                    }
                }
                return headers;
                var e_1, _c;
            };
        /**
         * @return {?}
         */
        EntityMetaInfo.prototype.getPropertiesForSave = /**
         * @return {?}
         */
            function () {
                var /** @type {?} */ included = [];
                for (var /** @type {?} */ property in this.properties) {
                    if (!this.properties[property].excludeWhenSaving) {
                        included.push(property);
                    }
                }
                return included;
            };
        return EntityMetaInfo;
    }(MetaInfo));
    var ServiceMetaInfo = (function (_super) {
        __extends(ServiceMetaInfo, _super);
        function ServiceMetaInfo(type, _entityMeta) {
            var _this = _super.call(this, type) || this;
            _this._entityMeta = _entityMeta;
            return _this;
        }
        Object.defineProperty(ServiceMetaInfo.prototype, "entityMeta", {
            get: /**
             * @return {?}
             */ function () {
                return this._entityMeta;
            },
            enumerable: true,
            configurable: true
        });
        return ServiceMetaInfo;
    }(MetaInfo));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MetaService = (function () {
        function MetaService() {
        }
        /**
         * @param {?} key
         * @return {?}
         */
        MetaService.get = /**
         * @param {?} key
         * @return {?}
         */
            function (key) {
                return MetaService.map.get(key);
            };
        /**
         * @param {?} key
         * @return {?}
         */
        MetaService.getByName = /**
         * @param {?} key
         * @return {?}
         */
            function (key) {
                try {
                    for (var _a = __values(MetaService.map.values()), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var meta = _b.value;
                        if (meta instanceof EntityMetaInfo && meta.name === key) {
                            return meta;
                        }
                    }
                }
                catch (e_1_1) {
                    e_1 = { error: e_1_1 };
                }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return))
                            _c.call(_a);
                    }
                    finally {
                        if (e_1)
                            throw e_1.error;
                    }
                }
                var e_1, _c;
            };
        /**
         * @param {?} key
         * @return {?}
         */
        MetaService.getOrCreateForEntity = /**
         * @param {?} key
         * @return {?}
         */
            function (key) {
                var /** @type {?} */ meta = MetaService.get(key);
                if (!meta) {
                    meta = new EntityMetaInfo(key);
                    MetaService.set(meta);
                }
                return /** @type {?} */ (meta);
            };
        /**
         * @param {?} key
         * @return {?}
         */
        MetaService.getOrCreateForEventListener = /**
         * @param {?} key
         * @return {?}
         */
            function (key) {
                var /** @type {?} */ meta = MetaService.get(key);
                if (!meta) {
                    meta = new EventListenerMetaInfo(key);
                    MetaService.set(meta);
                }
                return /** @type {?} */ (meta);
            };
        /**
         * @param {?} key
         * @param {?} property
         * @param {?=} type
         * @return {?}
         */
        MetaService.getOrCreateForProperty = /**
         * @param {?} key
         * @param {?} property
         * @param {?=} type
         * @return {?}
         */
            function (key, property, type) {
                var /** @type {?} */ entityMeta = MetaService.getOrCreateForEntity(key.constructor);
                if (!entityMeta.properties[property]) {
                    entityMeta.properties[property] = new PropertyMetaInfo(type || Reflect.getMetadata('design:type', key, property));
                }
                return entityMeta.properties[property];
            };
        /**
         * @param {?} key
         * @param {?} entity
         * @return {?}
         */
        MetaService.getOrCreateForHttpService = /**
         * @param {?} key
         * @param {?} entity
         * @return {?}
         */
            function (key, entity) {
                var /** @type {?} */ meta = MetaService.get(key);
                if (!meta) {
                    meta = new ServiceMetaInfo(key, MetaService.getOrCreateForEntity(entity));
                    MetaService.set(meta);
                }
                return /** @type {?} */ (meta);
            };
        /**
         * @param {?} meta
         * @return {?}
         */
        MetaService.set = /**
         * @param {?} meta
         * @return {?}
         */
            function (meta) {
                MetaService.map.set(meta.type, meta);
            };
        MetaService.map = new Map();
        return MetaService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var EventsService = (function () {
        function EventsService(handlers) {
            this.handlers = handlers;
            if (!Array.isArray(this.handlers)) {
                this.handlers = [];
            }
        }
        /**
         * @param {?} event
         * @return {?}
         */
        EventsService.prototype.handle = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.handlers.forEach(function (handler) {
                    var /** @type {?} */ meta = MetaService.getOrCreateForEventListener(handler.constructor);
                    try {
                        for (var _a = __values(meta.events), _b = _a.next(); !_b.done; _b = _a.next()) {
                            var eventMeta = _b.value;
                            if (eventMeta.type === event.constructor && eventMeta.entity === event.entity()) {
                                handler.handle(event);
                            }
                        }
                    }
                    catch (e_1_1) {
                        e_1 = { error: e_1_1 };
                    }
                    finally {
                        try {
                            if (_b && !_b.done && (_c = _a.return))
                                _c.call(_a);
                        }
                        finally {
                            if (e_1)
                                throw e_1.error;
                        }
                    }
                    var e_1, _c;
                });
                return event;
            };
        EventsService.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        EventsService.ctorParameters = function () {
            return [
                { type: Array, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANREST_HTTP_EVENT_LISTENERS,] },] },
            ];
        };
        return EventsService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ApiService = (function () {
        function ApiService(http$$1, events, infoService, config) {
            this.http = http$$1;
            this.events = events;
            this.infoService = infoService;
            this.config = config;
            this.meta = MetaService.get(this.constructor);
        }
        /**
         * @param {?=} filter
         * @return {?}
         */
        ApiService.prototype.getList = /**
         * @param {?=} filter
         * @return {?}
         */
            function (filter) {
                return this.doGet(this.meta.path, filter);
            };
        /**
         * @param {?} id
         * @return {?}
         */
        ApiService.prototype.get = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return this.doGet(this.meta.path + '/' + id);
            };
        /**
         * @param {?} entity
         * @return {?}
         */
        ApiService.prototype.reload = /**
         * @param {?} entity
         * @return {?}
         */
            function (entity) {
                return this.doGet(this.infoService.getForEntity(entity).path);
            };
        /**
         * @param {?} id
         * @return {?}
         */
        ApiService.prototype.getReference = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return this.doGet('&' + this.meta.path + '/' + id + '#' + this.meta.entityMeta.name);
            };
        /**
         * @param {?} path
         * @param {?=} filter
         * @return {?}
         */
        ApiService.prototype.doGet = /**
         * @param {?} path
         * @param {?=} filter
         * @return {?}
         */
            function (path, filter) {
                var _this = this;
                var /** @type {?} */ f;
                if (filter instanceof http.HttpParams) {
                    f = filter;
                }
                else {
                    f = new http.HttpParams();
                    for (var /** @type {?} */ key in filter) {
                        f = f.set(key, typeof filter[key] === 'boolean' ? (filter[key] ? 'true' : 'false') : String(filter[key]));
                    }
                }
                this.events.handle(new BeforeGetEvent(path, this.meta.entityMeta.type, f));
                return this.http.get(this.config.baseUrl + path, { headers: this.meta.entityMeta.getHeaders(), params: f }).pipe(operators.tap(function (data) { return _this.events.handle(new AfterGetEvent(data, _this.meta.entityMeta.type)); }));
            };
        /**
         * @param {?} entity
         * @return {?}
         */
        ApiService.prototype.save = /**
         * @param {?} entity
         * @return {?}
         */
            function (entity) {
                var _this = this;
                this.events.handle(new BeforeSaveEvent(entity));
                var /** @type {?} */ body = {};
                if (this.meta.entityMeta.body) {
                    body = this.meta.entityMeta.body(entity);
                }
                else {
                    this.meta.entityMeta.getPropertiesForSave().forEach(function (property) { return body[property] = entity[property]; });
                }
                var /** @type {?} */ meta = this.infoService.getForEntity(entity);
                return this.http[meta ? 'put' : 'post'](meta ? this.config.baseUrl + meta.path : this.config.baseUrl + this.meta.path, body, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(operators.tap(function (data) {
                    meta = _this.infoService.getForEntity(data);
                    if (!meta) {
                        _this.infoService.setForEntity(entity, meta);
                    }
                }), operators.tap(function () { return _this.events.handle(new AfterSaveEvent(entity)); }));
            };
        /**
         * @param {?} entity
         * @return {?}
         */
        ApiService.prototype.remove = /**
         * @param {?} entity
         * @return {?}
         */
            function (entity) {
                var _this = this;
                this.events.handle(new BeforeRemoveEvent(entity));
                var /** @type {?} */ meta = this.infoService.getForEntity(entity);
                if (meta) {
                    return this.http.delete(this.config.baseUrl + meta.path, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(operators.tap(function () { return _this.events.handle(new AfterRemoveEvent(entity)); }));
                }
            };
        ApiService.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        ApiService.ctorParameters = function () {
            return [
                { type: http.HttpClient, },
                { type: EventsService, },
                { type: DataInfoService, },
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
            ];
        };
        return ApiService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ReferenceInterceptor = (function () {
        function ReferenceInterceptor(config) {
            this.config = config;
        }
        /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
        ReferenceInterceptor.prototype.intercept = /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
            function (request, next) {
                if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl || !this.isReferenceRequest(request)) {
                    return next.handle(request);
                }
                return this.createReferenceResponse(request);
            };
        /**
         * @param {?} request
         * @return {?}
         */
        ReferenceInterceptor.prototype.isReferenceRequest = /**
         * @param {?} request
         * @return {?}
         */
            function (request) {
                return request.method === 'GET' && request.url.slice(this.config.baseUrl.length).indexOf('&') === 0;
            };
        /**
         * @param {?} request
         * @return {?}
         */
        ReferenceInterceptor.prototype.createReferenceResponse = /**
         * @param {?} request
         * @return {?}
         */
            function (request) {
                var /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
                return rxjs.of(new http.HttpResponse({
                    body: { 'path': path.substring(1, path.indexOf('#')), 'meta': MetaService.getByName(path.substring(path.indexOf('#') + 1)) },
                    status: 200,
                    url: request.url
                })).pipe(operators.map(function (response) {
                    if (response instanceof http.HttpResponse) {
                        return response.clone({
                            headers: response.headers.set('Content-Type', '@reference')
                        });
                    }
                }));
            };
        ReferenceInterceptor.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        ReferenceInterceptor.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
            ];
        };
        return ReferenceInterceptor;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ ANREST_HTTP_DATA_NORMALIZERS = new core.InjectionToken('anrest.http_data_normalizers');

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ResponseNode = (function () {
        function ResponseNode() {
        }
        return ResponseNode;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ObjectCollector = (function () {
        function ObjectCollector(info) {
            this.info = info;
            this.map = new Map();
        }
        /**
         * @param {?} data
         * @return {?}
         */
        ObjectCollector.prototype.set = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                this.map.set(this.info.get(data).path, data);
            };
        /**
         * @param {?} data
         * @return {?}
         */
        ObjectCollector.prototype.remove = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                var /** @type {?} */ info = this.info.get(data);
                if (info) {
                    data = info.path;
                }
                this.map.delete(data);
            };
        /**
         * @param {?} id
         * @return {?}
         */
        ObjectCollector.prototype.get = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return this.map.get(id);
            };
        /**
         * @param {?} id
         * @return {?}
         */
        ObjectCollector.prototype.has = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return this.map.has(id);
            };
        /**
         * @return {?}
         */
        ObjectCollector.prototype.clear = /**
         * @return {?}
         */
            function () {
                this.map.clear();
            };
        ObjectCollector.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        ObjectCollector.ctorParameters = function () {
            return [
                { type: DataInfoService, },
            ];
        };
        return ObjectCollector;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ApiProcessor = (function () {
        function ApiProcessor(injector, info, collector) {
            this.injector = injector;
            this.info = info;
            this.collector = collector;
        }
        /**
         * @param {?} node
         * @param {?} method
         * @return {?}
         */
        ApiProcessor.prototype.process = /**
         * @param {?} node
         * @param {?} method
         * @return {?}
         */
            function (node, method) {
                return node.collection ? this.processCollection(node) : this.processObject(node);
            };
        /**
         * @param {?} node
         * @return {?}
         */
        ApiProcessor.prototype.processCollection = /**
         * @param {?} node
         * @return {?}
         */
            function (node) {
                var /** @type {?} */ data;
                if (node.info.path !== undefined) {
                    data = this.collector.get(node.info.path) || new Collection(this.injector.get(node.meta.service), node.info);
                }
                else {
                    data = new Collection(this.injector.get(node.meta.service), node.info);
                }
                data.length = 0;
                try {
                    for (var _a = __values(node.data), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var object = _b.value;
                        data.push(this.processObject(object));
                    }
                }
                catch (e_1_1) {
                    e_1 = { error: e_1_1 };
                }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return))
                            _c.call(_a);
                    }
                    finally {
                        if (e_1)
                            throw e_1.error;
                    }
                }
                this.info.setForCollection(data, node.info);
                this.collector.set(data);
                return data;
                var e_1, _c;
            };
        /**
         * @param {?} node
         * @return {?}
         */
        ApiProcessor.prototype.processObject = /**
         * @param {?} node
         * @return {?}
         */
            function (node) {
                if (node.meta instanceof EntityMetaInfo) {
                    var /** @type {?} */ object = this.collector.get(node.info.path) || new node.meta.type();
                    for (var /** @type {?} */ property in node.meta.properties) {
                        if (object.hasOwnProperty(property) && !node.data.hasOwnProperty(property)) {
                            continue;
                        }
                        if (node.data[property] instanceof ResponseNode) {
                            object[property] = node.meta.properties[property].isCollection ?
                                this.processCollection(node.data[property]) :
                                this.processObject(node.data[property]);
                        }
                        else {
                            var /** @type {?} */ type = node.meta.properties[property].type;
                            switch (type) {
                                case Number:
                                    object[property] = Number(node.data[property]);
                                    break;
                                case Date:
                                    object[property] = new Date(node.data[property]);
                                    break;
                                case String:
                                default:
                                    object[property] = node.data[property];
                                    break;
                            }
                        }
                    }
                    for (var /** @type {?} */ property in node.meta.subresources) {
                        var /** @type {?} */ service = this.injector.get(node.meta.subresources[property].meta.service);
                        object[property] = service.doGet.bind(service, node.info.path + node.meta.subresources[property].path);
                    }
                    this.info.setForEntity(object, node.info);
                    this.collector.set(object);
                    return object;
                }
                else {
                    return node.data;
                }
            };
        ApiProcessor.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        ApiProcessor.ctorParameters = function () {
            return [
                { type: core.Injector, },
                { type: DataInfoService, },
                { type: ObjectCollector, },
            ];
        };
        return ApiProcessor;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ApiInterceptor = (function () {
        function ApiInterceptor(config, normalizers, processor) {
            this.config = config;
            this.normalizers = normalizers;
            this.processor = processor;
            if (!Array.isArray(this.normalizers)) {
                this.normalizers = [];
            }
        }
        /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
        ApiInterceptor.prototype.intercept = /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
            function (request, next) {
                var _this = this;
                if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
                    || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
                    return next.handle(request);
                }
                var /** @type {?} */ type;
                var /** @type {?} */ headers = request.headers;
                if (headers.has('X-AnRest-Type')) {
                    type = headers.get('X-AnRest-Type');
                    headers = headers.delete('x-AnRest-Type');
                }
                if (this.config.defaultHeaders) {
                    try {
                        for (var _a = __values(this.config.defaultHeaders), _b = _a.next(); !_b.done; _b = _a.next()) {
                            var header = _b.value;
                            if (!headers.has(header.name) || header.append) {
                                headers = headers[header.append ? 'append' : 'set'](header.name, header.value);
                            }
                        }
                    }
                    catch (e_1_1) {
                        e_1 = { error: e_1_1 };
                    }
                    finally {
                        try {
                            if (_b && !_b.done && (_c = _a.return))
                                _c.call(_a);
                        }
                        finally {
                            if (e_1)
                                throw e_1.error;
                        }
                    }
                }
                request = request.clone({
                    headers: headers
                });
                return next.handle(request).pipe(operators.map(function (response) {
                    if (response instanceof http.HttpResponse) {
                        var /** @type {?} */ contentType = response.headers.get('Content-Type');
                        try {
                            for (var _a = __values(_this.normalizers), _b = _a.next(); !_b.done; _b = _a.next()) {
                                var normalizer = _b.value;
                                if (normalizer.supports(contentType)) {
                                    return response.clone({ body: _this.processor.process(normalizer.normalize(response, type), request.method) });
                                }
                            }
                        }
                        catch (e_2_1) {
                            e_2 = { error: e_2_1 };
                        }
                        finally {
                            try {
                                if (_b && !_b.done && (_c = _a.return))
                                    _c.call(_a);
                            }
                            finally {
                                if (e_2)
                                    throw e_2.error;
                            }
                        }
                    }
                    return response;
                    var e_2, _c;
                }));
                var e_1, _c;
            };
        ApiInterceptor.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        ApiInterceptor.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
                { type: Array, decorators: [{ type: core.Optional }, { type: core.Inject, args: [ANREST_HTTP_DATA_NORMALIZERS,] },] },
                { type: ApiProcessor, },
            ];
        };
        return ApiInterceptor;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     */
    var ResponseCache = (function () {
        function ResponseCache() {
        }
        /**
         * @param {?} data
         * @param {?} id
         * @return {?}
         */
        ResponseCache.prototype.replace = /**
         * @param {?} data
         * @param {?} id
         * @return {?}
         */
            function (data, id) {
                this.remove(id);
                this.add(data, id);
            };
        ResponseCache.decorators = [
            { type: core.Injectable },
        ];
        return ResponseCache;
    }());
    var /** @type {?} */ ANREST_CACHE_SERVICES = new core.InjectionToken('anrest.cache_services');

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var NoResponseCache = (function (_super) {
        __extends(NoResponseCache, _super);
        function NoResponseCache() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @param {?} id
         * @return {?}
         */
        NoResponseCache.prototype.has = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return false;
            };
        /**
         * @param {?} id
         * @param {?} data
         * @return {?}
         */
        NoResponseCache.prototype.add = /**
         * @param {?} id
         * @param {?} data
         * @return {?}
         */
            function (id, data) {
            };
        /**
         * @return {?}
         */
        NoResponseCache.prototype.clear = /**
         * @return {?}
         */
            function () {
            };
        /**
         * @param {?} id
         * @return {?}
         */
        NoResponseCache.prototype.get = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
            };
        /**
         * @param {?} id
         * @return {?}
         */
        NoResponseCache.prototype.remove = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
            };
        /**
         * @param {?} id
         * @param {?} alias
         * @return {?}
         */
        NoResponseCache.prototype.addAlias = /**
         * @param {?} id
         * @param {?} alias
         * @return {?}
         */
            function (id, alias) {
            };
        /**
         * @param {?} data
         * @param {?} id
         * @return {?}
         */
        NoResponseCache.prototype.replace = /**
         * @param {?} data
         * @param {?} id
         * @return {?}
         */
            function (data, id) {
            };
        NoResponseCache.decorators = [
            { type: core.Injectable },
        ];
        return NoResponseCache;
    }(ResponseCache));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CacheInterceptor = (function () {
        function CacheInterceptor(config, info, cacheServices) {
            this.config = config;
            this.info = info;
            this.cacheServices = cacheServices;
        }
        /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
        CacheInterceptor.prototype.intercept = /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
            function (request, next) {
                var _this = this;
                var /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
                if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
                    || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
                    return next.handle(request);
                }
                var /** @type {?} */ cacheType = MetaService.getByName(request.headers.get('X-AnRest-Type')).cache || this.config.cache || NoResponseCache;
                var /** @type {?} */ cache;
                try {
                    for (var _a = __values(this.cacheServices), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var cacheService = _b.value;
                        if (cacheService instanceof cacheType) {
                            cache = cacheService;
                            break;
                        }
                    }
                }
                catch (e_1_1) {
                    e_1 = { error: e_1_1 };
                }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return))
                            _c.call(_a);
                    }
                    finally {
                        if (e_1)
                            throw e_1.error;
                    }
                }
                if (request.method !== 'GET') {
                    cache.remove(path);
                    return next.handle(request);
                }
                if (cache.has(path)) {
                    return rxjs.of(new http.HttpResponse({
                        body: cache.get(path),
                        status: 200,
                        url: request.url
                    }));
                }
                return next.handle(request).pipe(operators.tap(function (response) {
                    if (response.body) {
                        var /** @type {?} */ info = _this.info.get(response.body);
                        if (info) {
                            cache.add(info.path, response.body);
                            if (info && info.alias) {
                                cache.addAlias(info.path, info.alias);
                            }
                        }
                    }
                }));
                var e_1, _c;
            };
        CacheInterceptor.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        CacheInterceptor.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
                { type: DataInfoService, },
                { type: Array, decorators: [{ type: core.Inject, args: [ANREST_CACHE_SERVICES,] },] },
            ];
        };
        return CacheInterceptor;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var MemoryResponseCache = (function (_super) {
        __extends(MemoryResponseCache, _super);
        function MemoryResponseCache(collector, config) {
            var _this = _super.call(this) || this;
            _this.collector = collector;
            _this.config = config;
            _this.times = {};
            _this.aliases = {};
            return _this;
        }
        /**
         * @param {?} id
         * @param {?} data
         * @return {?}
         */
        MemoryResponseCache.prototype.add = /**
         * @param {?} id
         * @param {?} data
         * @return {?}
         */
            function (id, data) {
                this.times[id] = Date.now();
            };
        /**
         * @return {?}
         */
        MemoryResponseCache.prototype.clear = /**
         * @return {?}
         */
            function () {
                this.collector.clear();
            };
        /**
         * @param {?} id
         * @return {?}
         */
        MemoryResponseCache.prototype.get = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return this.collector.get(id);
            };
        /**
         * @param {?} id
         * @return {?}
         */
        MemoryResponseCache.prototype.remove = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                this.collector.remove(id);
            };
        /**
         * @param {?} id
         * @param {?} alias
         * @return {?}
         */
        MemoryResponseCache.prototype.addAlias = /**
         * @param {?} id
         * @param {?} alias
         * @return {?}
         */
            function (id, alias) {
                this.aliases[alias] = id;
            };
        /**
         * @param {?} id
         * @return {?}
         */
        MemoryResponseCache.prototype.has = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                var /** @type {?} */ time = this.aliases[id] ? this.times[this.aliases[id]] : this.times[id];
                return time && (Date.now() <= this.config.cacheTTL + time);
            };
        MemoryResponseCache.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        MemoryResponseCache.ctorParameters = function () {
            return [
                { type: ObjectCollector, },
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
            ];
        };
        return MemoryResponseCache;
    }(ResponseCache));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var ReferenceDataNormalizer = (function () {
        function ReferenceDataNormalizer() {
        }
        /**
         * @param {?} response
         * @param {?=} type
         * @return {?}
         */
        ReferenceDataNormalizer.prototype.normalize = /**
         * @param {?} response
         * @param {?=} type
         * @return {?}
         */
            function (response, type) {
                var /** @type {?} */ r = new ResponseNode();
                r.collection = false;
                r.info = new EntityInfo(response.body.path);
                r.meta = response.body.meta;
                r.data = {};
                return r;
            };
        /**
         * @param {?} type
         * @return {?}
         */
        ReferenceDataNormalizer.prototype.supports = /**
         * @param {?} type
         * @return {?}
         */
            function (type) {
                return type === '@reference';
            };
        return ReferenceDataNormalizer;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LdJsonDataNormalizer = (function () {
        function LdJsonDataNormalizer() {
        }
        /**
         * @param {?} response
         * @param {?=} type
         * @return {?}
         */
        LdJsonDataNormalizer.prototype.normalize = /**
         * @param {?} response
         * @param {?=} type
         * @return {?}
         */
            function (response, type) {
                return this.process(response.body);
            };
        /**
         * @param {?} type
         * @return {?}
         */
        LdJsonDataNormalizer.prototype.supports = /**
         * @param {?} type
         * @return {?}
         */
            function (type) {
                return type.split(';')[0] === 'application/ld+json';
            };
        /**
         * @param {?} data
         * @param {?=} meta
         * @param {?=} collection
         * @return {?}
         */
        LdJsonDataNormalizer.prototype.process = /**
         * @param {?} data
         * @param {?=} meta
         * @param {?=} collection
         * @return {?}
         */
            function (data, meta, collection) {
                var /** @type {?} */ r = new ResponseNode();
                r.collection = collection || data['@type'] === 'hydra:Collection';
                var /** @type {?} */ path = data['@id'];
                r.meta = meta || MetaService.getByName(r.collection ? /\/contexts\/(\w+)/g.exec(data['@context'])[1] : data['@type']);
                if (r.collection) {
                    var /** @type {?} */ items = data['hydra:member'] || data;
                    r.info = new CollectionInfo(data['hydra:view'] ? data['hydra:view']['@id'] : path);
                    r.info.type = r.meta.name;
                    if (data['hydra:view']) {
                        r.info.first = data['hydra:view']['hydra:first'];
                        r.info.previous = data['hydra:view']['hydra:previous'];
                        r.info.next = data['hydra:view']['hydra:next'];
                        r.info.last = data['hydra:view']['hydra:last'];
                        if (r.info.path === r.info.first) {
                            r.info.alias = data['@id'];
                        }
                    }
                    r.info.total = data['hydra:totalItems'] || items.length;
                    var /** @type {?} */ regexPage = /page=(\d+)/g;
                    var /** @type {?} */ matches = void 0;
                    matches = regexPage.exec(r.info.path);
                    r.info.page = matches ? Number(matches[1]) : 1;
                    matches = regexPage.exec(r.info.last);
                    r.info.lastPage = matches ? Number(matches[1]) : 1;
                    r.data = [];
                    try {
                        for (var items_1 = __values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                            var object = items_1_1.value;
                            r.data.push(this.process(object));
                        }
                    }
                    catch (e_1_1) {
                        e_1 = { error: e_1_1 };
                    }
                    finally {
                        try {
                            if (items_1_1 && !items_1_1.done && (_a = items_1.return))
                                _a.call(items_1);
                        }
                        finally {
                            if (e_1)
                                throw e_1.error;
                        }
                    }
                }
                else {
                    r.info = new EntityInfo(path);
                    r.data = {};
                    if (r.meta === undefined && Array.isArray(data)) {
                        r.data = data;
                    }
                    else {
                        for (var /** @type {?} */ key in data) {
                            if (key.indexOf('@') === 0 || key.indexOf('hydra:') === 0) {
                                continue;
                            }
                            var /** @type {?} */ propertyMeta = r.meta.properties[key];
                            r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                                this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
                        }
                    }
                }
                return r;
                var e_1, _a;
            };
        return LdJsonDataNormalizer;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var JsonDataNormalizer = (function () {
        function JsonDataNormalizer() {
        }
        /**
         * @param {?} response
         * @param {?} type
         * @return {?}
         */
        JsonDataNormalizer.prototype.normalize = /**
         * @param {?} response
         * @param {?} type
         * @return {?}
         */
            function (response, type) {
                return this.process(response.body, MetaService.getByName(type), Array.isArray(response.body), response.headers);
            };
        /**
         * @param {?} type
         * @return {?}
         */
        JsonDataNormalizer.prototype.supports = /**
         * @param {?} type
         * @return {?}
         */
            function (type) {
                return type.split(';')[0] === 'application/json';
            };
        /**
         * @param {?} data
         * @param {?} meta
         * @param {?} collection
         * @param {?=} headers
         * @return {?}
         */
        JsonDataNormalizer.prototype.process = /**
         * @param {?} data
         * @param {?} meta
         * @param {?} collection
         * @param {?=} headers
         * @return {?}
         */
            function (data, meta, collection, headers) {
                var /** @type {?} */ r = new ResponseNode();
                r.collection = collection;
                var /** @type {?} */ collectionPath = MetaService.get(meta.service) ? MetaService.get(meta.service).path : undefined;
                var /** @type {?} */ path = collectionPath ? (collection ? collectionPath : collectionPath + '/' + data[meta.id]) : undefined;
                r.meta = meta;
                if (r.collection) {
                    r.info = new CollectionInfo(headers.get('X-Current-Page') || path);
                    r.info.type = r.meta.name;
                    r.info.first = headers.get('X-First-Page') || undefined;
                    r.info.previous = headers.get('X-Prev-Page') || undefined;
                    r.info.next = headers.get('X-Next-Page') || undefined;
                    r.info.last = headers.get('X-Last-Page') || undefined;
                    if (r.info.path === r.info.first) {
                        r.info.alias = path;
                    }
                    r.info.total = headers.get('X-Total') || data.length;
                    var /** @type {?} */ regexPage = /page=(\d+)/g;
                    var /** @type {?} */ matches = void 0;
                    matches = regexPage.exec(r.info.path);
                    r.info.page = matches ? Number(matches[1]) : 1;
                    matches = regexPage.exec(r.info.last);
                    r.info.lastPage = matches ? Number(matches[1]) : 1;
                    r.data = [];
                    try {
                        for (var data_1 = __values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                            var object = data_1_1.value;
                            r.data.push(this.process(object, r.meta, false));
                        }
                    }
                    catch (e_1_1) {
                        e_1 = { error: e_1_1 };
                    }
                    finally {
                        try {
                            if (data_1_1 && !data_1_1.done && (_a = data_1.return))
                                _a.call(data_1);
                        }
                        finally {
                            if (e_1)
                                throw e_1.error;
                        }
                    }
                }
                else {
                    r.info = new EntityInfo(path);
                    r.data = {};
                    for (var /** @type {?} */ key in data) {
                        var /** @type {?} */ propertyMeta = r.meta.properties[key];
                        r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                            this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
                    }
                }
                return r;
                var e_1, _a;
            };
        return JsonDataNormalizer;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LocalStorageResponseCache = (function (_super) {
        __extends(LocalStorageResponseCache, _super);
        function LocalStorageResponseCache(collector, processor, info, config) {
            var _this = _super.call(this) || this;
            _this.collector = collector;
            _this.processor = processor;
            _this.info = info;
            _this.config = config;
            _this.tokenPrefix = 'anrest:cache:';
            var /** @type {?} */ timestampPrefix = _this.tokenPrefix + 'timestamp:';
            Object.keys(localStorage).filter(function (e) { return e.indexOf(timestampPrefix) === 0; }).forEach(function (k) {
                var /** @type {?} */ id = k.substring(timestampPrefix.length);
                var /** @type {?} */ timestamp = _this.getTimestamp(id);
                if (!timestamp || timestamp + _this.config.cacheTTL < Date.now()) {
                    _this.remove(id);
                }
            });
            return _this;
        }
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.get = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
                if (this.has(id)) {
                    return this.processor.process(this.getNode(id), 'GET');
                }
            };
        /**
         * @param {?} id
         * @param {?} data
         * @return {?}
         */
        LocalStorageResponseCache.prototype.add = /**
         * @param {?} id
         * @param {?} data
         * @return {?}
         */
            function (id, data) {
                this.doAdd(id, data, true);
            };
        /**
         * @param {?} id
         * @param {?} alias
         * @return {?}
         */
        LocalStorageResponseCache.prototype.addAlias = /**
         * @param {?} id
         * @param {?} alias
         * @return {?}
         */
            function (id, alias) {
                localStorage.setItem(this.tokenPrefix + 'alias:' + alias, id);
            };
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.has = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
                var /** @type {?} */ complete = JSON.parse(localStorage.getItem(this.tokenPrefix + 'complete:' + id));
                var /** @type {?} */ timestamp = this.getTimestamp(id);
                return !!timestamp && complete && timestamp + this.config.cacheTTL >= Date.now();
            };
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.remove = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                localStorage.removeItem(this.tokenPrefix + id);
                localStorage.removeItem(this.tokenPrefix + 'type:' + id);
                localStorage.removeItem(this.tokenPrefix + 'collection:' + id);
                localStorage.removeItem(this.tokenPrefix + 'complete:' + id);
                localStorage.removeItem(this.tokenPrefix + 'references:' + id);
                this.removeTimestamp(id);
            };
        /**
         * @return {?}
         */
        LocalStorageResponseCache.prototype.clear = /**
         * @return {?}
         */
            function () {
                var _this = this;
                Object.keys(localStorage).filter(function (e) { return e.indexOf(_this.tokenPrefix) === 0; }).forEach(function (k) {
                    localStorage.removeItem(k);
                });
            };
        /**
         * @param {?} id
         * @param {?} data
         * @param {?} complete
         * @return {?}
         */
        LocalStorageResponseCache.prototype.doAdd = /**
         * @param {?} id
         * @param {?} data
         * @param {?} complete
         * @return {?}
         */
            function (id, data, complete) {
                var /** @type {?} */ info = this.info.get(data);
                var /** @type {?} */ values;
                if (info instanceof CollectionInfo) {
                    values = [];
                    try {
                        for (var data_1 = __values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                            var item = data_1_1.value;
                            var /** @type {?} */ itemPath = this.info.get(item).path;
                            values.push(itemPath);
                            this.doAdd(itemPath, item, false);
                        }
                    }
                    catch (e_1_1) {
                        e_1 = { error: e_1_1 };
                    }
                    finally {
                        try {
                            if (data_1_1 && !data_1_1.done && (_a = data_1.return))
                                _a.call(data_1);
                        }
                        finally {
                            if (e_1)
                                throw e_1.error;
                        }
                    }
                    localStorage.setItem(this.tokenPrefix + 'collection:' + id, JSON.stringify(info));
                }
                else {
                    var /** @type {?} */ references = {};
                    values = {};
                    for (var /** @type {?} */ key in data) {
                        var /** @type {?} */ itemInfo = void 0;
                        if (typeof data[key] === 'object') {
                            itemInfo = this.info.get(data[key]);
                        }
                        if (itemInfo) {
                            this.doAdd(itemInfo.path, data[key], false);
                            references[key] = itemInfo.path;
                        }
                        else {
                            values[key] = data[key];
                        }
                    }
                    localStorage.setItem(this.tokenPrefix + 'type:' + id, MetaService.get(data.constructor).name);
                    localStorage.setItem(this.tokenPrefix + 'references:' + id, JSON.stringify(references));
                }
                localStorage.setItem(this.tokenPrefix + 'complete:' + id, JSON.stringify(complete));
                localStorage.setItem(this.tokenPrefix + id, JSON.stringify(values));
                this.setTimestamp(id);
                var e_1, _a;
            };
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.getNode = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                var /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + id));
                var /** @type {?} */ collectionInfo = this.getCollectionInfo(id);
                var /** @type {?} */ response = new ResponseNode();
                if (collectionInfo) {
                    response.collection = true;
                    response.info = collectionInfo;
                    response.meta = MetaService.getByName(collectionInfo.type);
                    response.data = [];
                    try {
                        for (var data_2 = __values(data), data_2_1 = data_2.next(); !data_2_1.done; data_2_1 = data_2.next()) {
                            var itemId = data_2_1.value;
                            response.data.push(this.getNode(itemId));
                        }
                    }
                    catch (e_2_1) {
                        e_2 = { error: e_2_1 };
                    }
                    finally {
                        try {
                            if (data_2_1 && !data_2_1.done && (_a = data_2.return))
                                _a.call(data_2);
                        }
                        finally {
                            if (e_2)
                                throw e_2.error;
                        }
                    }
                }
                else {
                    response.collection = false;
                    response.info = new EntityInfo(id);
                    response.meta = MetaService.getByName(localStorage.getItem(this.tokenPrefix + 'type:' + id));
                    response.data = data;
                    var /** @type {?} */ references = JSON.parse(localStorage.getItem(this.tokenPrefix + 'references:' + id));
                    if (references) {
                        for (var /** @type {?} */ key in references) {
                            response.data[key] = this.getNode(references[key]);
                        }
                    }
                }
                return response;
                var e_2, _a;
            };
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.getCollectionInfo = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                var /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + 'collection:' + id));
                if (data) {
                    var /** @type {?} */ info = new CollectionInfo(id);
                    for (var /** @type {?} */ key in data) {
                        if (key !== '_path') {
                            info[key] = data[key];
                        }
                    }
                    return info;
                }
            };
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.setTimestamp = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                localStorage.setItem(this.tokenPrefix + 'timestamp:' + id, String(Date.now()));
            };
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.getTimestamp = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return Number(localStorage.getItem(this.tokenPrefix + 'timestamp:' + id));
            };
        /**
         * @param {?} id
         * @return {?}
         */
        LocalStorageResponseCache.prototype.removeTimestamp = /**
         * @param {?} id
         * @return {?}
         */
            function (id) {
                return localStorage.removeItem(this.tokenPrefix + 'timestamp:' + id);
            };
        LocalStorageResponseCache.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        LocalStorageResponseCache.ctorParameters = function () {
            return [
                { type: ObjectCollector, },
                { type: ApiProcessor, },
                { type: DataInfoService, },
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
            ];
        };
        return LocalStorageResponseCache;
    }(ResponseCache));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ AuthService = (function () {
        function AuthService() {
        }
        return AuthService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ AuthTokenProviderService = (function () {
        function AuthTokenProviderService() {
        }
        return AuthTokenProviderService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var LocalStorageAuthTokenProviderService = (function () {
        function LocalStorageAuthTokenProviderService() {
            this.tokenName = 'anrest:auth_token';
        }
        /**
         * @return {?}
         */
        LocalStorageAuthTokenProviderService.prototype.get = /**
         * @return {?}
         */
            function () {
                return localStorage.getItem(this.tokenName);
            };
        /**
         * @param {?} token
         * @return {?}
         */
        LocalStorageAuthTokenProviderService.prototype.set = /**
         * @param {?} token
         * @return {?}
         */
            function (token) {
                localStorage.setItem(this.tokenName, token);
            };
        /**
         * @return {?}
         */
        LocalStorageAuthTokenProviderService.prototype.remove = /**
         * @return {?}
         */
            function () {
                localStorage.removeItem(this.tokenName);
            };
        /**
         * @return {?}
         */
        LocalStorageAuthTokenProviderService.prototype.isSet = /**
         * @return {?}
         */
            function () {
                return localStorage.getItem(this.tokenName) != null;
            };
        LocalStorageAuthTokenProviderService.decorators = [
            { type: core.Injectable },
        ];
        return LocalStorageAuthTokenProviderService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AuthInterceptor = (function () {
        function AuthInterceptor(auth, config) {
            this.auth = auth;
            this.config = config;
        }
        /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
        AuthInterceptor.prototype.intercept = /**
         * @param {?} request
         * @param {?} next
         * @return {?}
         */
            function (request, next) {
                if (!this.config.authUrl || request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl) {
                    return next.handle(request);
                }
                if (this.auth.isSignedIn()) {
                    request = request.clone({
                        setHeaders: {
                            Authorization: "Bearer " + this.auth.getToken()
                        }
                    });
                }
                return next.handle(request);
            };
        AuthInterceptor.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        AuthInterceptor.ctorParameters = function () {
            return [
                { type: AuthService, },
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
            ];
        };
        return AuthInterceptor;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var JwtAuthService = (function () {
        function JwtAuthService(http$$1, token, config) {
            this.http = http$$1;
            this.token = token;
            this.config = config;
        }
        /**
         * @return {?}
         */
        JwtAuthService.prototype.getToken = /**
         * @return {?}
         */
            function () {
                return this.token.get();
            };
        /**
         * @param {?} token
         * @return {?}
         */
        JwtAuthService.prototype.setToken = /**
         * @param {?} token
         * @return {?}
         */
            function (token) {
                this.token.set(token);
            };
        /**
         * @return {?}
         */
        JwtAuthService.prototype.remove = /**
         * @return {?}
         */
            function () {
                this.token.remove();
            };
        /**
         * @return {?}
         */
        JwtAuthService.prototype.isSignedIn = /**
         * @return {?}
         */
            function () {
                return this.token.isSet();
            };
        /**
         * @return {?}
         */
        JwtAuthService.prototype.getInfo = /**
         * @return {?}
         */
            function () {
                return jwt_decode.call(this.getToken());
            };
        /**
         * @param {?} username
         * @param {?} password
         * @return {?}
         */
        JwtAuthService.prototype.signIn = /**
         * @param {?} username
         * @param {?} password
         * @return {?}
         */
            function (username, password) {
                var _this = this;
                return this.http.post(this.config.authUrl, {
                    login: username,
                    password: password
                }).pipe(operators.tap(function (data) {
                    _this.setToken(data.token);
                }));
            };
        JwtAuthService.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        JwtAuthService.ctorParameters = function () {
            return [
                { type: http.HttpClient, },
                { type: AuthTokenProviderService, },
                { type: undefined, decorators: [{ type: core.Inject, args: [AnRestConfig,] },] },
            ];
        };
        return JwtAuthService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @param {?=} info
     * @return {?}
     */
    function Resource(info) {
        if (info === void 0) {
            info = {};
        }
        return function (target) {
            var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
            meta.name = info.name || target.name;
            meta.cache = info.cache;
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @param {?=} info
     * @return {?}
     */
    function Property(info) {
        if (info === void 0) {
            info = {};
        }
        return function (target, key) {
            var /** @type {?} */ meta = MetaService.getOrCreateForProperty(target, key, info.type);
            meta.isCollection = info.collection || false;
            meta.excludeWhenSaving = info.excludeWhenSaving || false;
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @param {?} entity
     * @param {?=} path
     * @return {?}
     */
    function HttpService(entity, path) {
        return function (target) {
            var /** @type {?} */ meta = MetaService.getOrCreateForHttpService(target, entity);
            meta.path = path || '/' + pluralize.plural(entity.name).replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase();
            meta.entityMeta.service = target;
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @param {?} headerName
     * @param {?=} append
     * @return {?}
     */
    function Header(headerName, append) {
        return function (target, key, value) {
            if (value === void 0) {
                value = undefined;
            }
            MetaService.getOrCreateForEntity(target.constructor).headers.push({
                name: headerName,
                value: value ? value.value : function () { return this[key]; },
                append: append || false,
                dynamic: true
            });
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @param {?} headers
     * @return {?}
     */
    function Headers(headers) {
        return function (target) {
            var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
            var _loop_1 = function (header) {
                meta.headers.push({
                    name: header.name,
                    value: function () { return header.value; },
                    append: header.append || false,
                    dynamic: false
                });
            };
            try {
                for (var headers_1 = __values(headers), headers_1_1 = headers_1.next(); !headers_1_1.done; headers_1_1 = headers_1.next()) {
                    var header = headers_1_1.value;
                    _loop_1(header);
                }
            }
            catch (e_1_1) {
                e_1 = { error: e_1_1 };
            }
            finally {
                try {
                    if (headers_1_1 && !headers_1_1.done && (_a = headers_1.return))
                        _a.call(headers_1);
                }
                finally {
                    if (e_1)
                        throw e_1.error;
                }
            }
            var e_1, _a;
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @return {?}
     */
    function Body() {
        return function (target, key, value) {
            if (key === void 0) {
                key = undefined;
            }
            if (value === void 0) {
                value = undefined;
            }
            MetaService.getOrCreateForEntity(target.constructor).body = function (object) {
                return (value ? value.value : function () { return this[key]; }).call(object);
            };
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @param {?} type
     * @param {?=} path
     * @return {?}
     */
    function Subresource(type, path) {
        return function (target, key, value) {
            MetaService.getOrCreateForEntity(target.constructor).subresources[key] = {
                meta: MetaService.getOrCreateForEntity(type),
                path: path || '/' + key.replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase()
            };
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @return {?}
     */
    function Id() {
        return function (target, key) {
            var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target.constructor);
            meta.id = key;
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ Loader = (function () {
        function Loader(service, waitFor, replay) {
            if (waitFor === void 0) {
                waitFor = 0;
            }
            if (replay === void 0) {
                replay = 0;
            }
            var _this = this;
            this.service = service;
            this.fields = {};
            this.loading = false;
            this.observable = rxjs.combineLatest(this.getSubjects())
                .pipe(replay ? operators.shareReplay(replay) : operators.share(), operators.debounceTime(waitFor), operators.distinctUntilChanged(function (d1, d2) { return _this.checkDistinct(d1, d2); }), operators.map(function (values) { return _this.mapParams(values); }), operators.mergeMap(function (data) {
                _this.loading = true;
                return _this.getServiceObservable(data).pipe(operators.tap(function (v) {
                    _this.lastData = v;
                    _this.loading = false;
                }));
            }));
        }
        /**
         * @param {?} name
         * @return {?}
         */
        Loader.prototype.field = /**
         * @param {?} name
         * @return {?}
         */
            function (name) {
                return this.fields[name];
            };
        Object.defineProperty(Loader.prototype, "data", {
            get: /**
             * @return {?}
             */ function () {
                return this.observable;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Loader.prototype, "isLoading", {
            get: /**
             * @return {?}
             */ function () {
                return this.loading;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} __0
         * @return {?}
         */
        Loader.prototype.getServiceObservable = /**
         * @param {?} __0
         * @return {?}
         */
            function (_a) {
                var _b = __read(_a, 1), data = _b[0];
                return this.service.getList(data);
            };
        /**
         * @param {?} d1
         * @param {?} d2
         * @return {?}
         */
        Loader.prototype.checkDistinct = /**
         * @param {?} d1
         * @param {?} d2
         * @return {?}
         */
            function (d1, d2) {
                var /** @type {?} */ offset = d1.length - this.getAvailableFields().length;
                for (var /** @type {?} */ i = d1.length; i >= 0; i--) {
                    if ((i < offset && d2[i]) || (i >= offset && d1[i] !== d2[i])) {
                        if (i >= offset) {
                            d2.fill(undefined, 0, offset);
                        }
                        return false;
                    }
                }
                return true;
            };
        /**
         * @param {?} values
         * @return {?}
         */
        Loader.prototype.mapParams = /**
         * @param {?} values
         * @return {?}
         */
            function (values) {
                var /** @type {?} */ fields = this.getAvailableFields();
                var /** @type {?} */ offset = values.length - fields.length;
                var /** @type {?} */ data = {};
                values.slice(offset).forEach(function (v, i) {
                    if (v !== undefined && v !== null && v !== '') {
                        if (typeof v === 'boolean') {
                            v = v ? 'true' : 'false';
                        }
                        data[fields[i]] = String(v);
                    }
                });
                return values.slice(0, offset).concat([data]);
            };
        /**
         * @return {?}
         */
        Loader.prototype.getSubjects = /**
         * @return {?}
         */
            function () {
                var _this = this;
                return this.getAvailableFields().map(function (name) {
                    return _this.fields[name] = new rxjs.BehaviorSubject(undefined);
                });
            };
        return Loader;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ InfiniteScrollLoader = (function (_super) {
        __extends(InfiniteScrollLoader, _super);
        function InfiniteScrollLoader() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @return {?}
         */
        InfiniteScrollLoader.prototype.loadMore = /**
         * @return {?}
         */
            function () {
                this.loadMoreSubject.next(true);
            };
        /**
         * @return {?}
         */
        InfiniteScrollLoader.prototype.hasMore = /**
         * @return {?}
         */
            function () {
                return this.lastData && this.lastData.hasMore();
            };
        /**
         * @param {?} __0
         * @return {?}
         */
        InfiniteScrollLoader.prototype.getServiceObservable = /**
         * @param {?} __0
         * @return {?}
         */
            function (_a) {
                var _b = __read(_a, 2), loadMore = _b[0], data = _b[1];
                return loadMore ? this.lastData.loadMore() : _super.prototype.getServiceObservable.call(this, [data]);
            };
        /**
         * @return {?}
         */
        InfiniteScrollLoader.prototype.getSubjects = /**
         * @return {?}
         */
            function () {
                this.loadMoreSubject = new rxjs.BehaviorSubject(false);
                var /** @type {?} */ subjects = _super.prototype.getSubjects.call(this);
                subjects.unshift(this.loadMoreSubject);
                return subjects;
            };
        return InfiniteScrollLoader;
    }(Loader));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * @abstract
     */
    var /**
     * @abstract
     */ PageLoader = (function (_super) {
        __extends(PageLoader, _super);
        function PageLoader() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @return {?}
         */
        PageLoader.prototype.first = /**
         * @return {?}
         */
            function () {
                if (!this.lastData || !this.lastData.hasFirst()) {
                    return;
                }
                this.pageSubject.next(this.lastData.firstPath());
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.previous = /**
         * @return {?}
         */
            function () {
                if (!this.lastData || !this.lastData.hasPrevious()) {
                    return;
                }
                this.pageSubject.next(this.lastData.previousPath());
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.next = /**
         * @return {?}
         */
            function () {
                if (!this.lastData || !this.lastData.hasNext()) {
                    return;
                }
                this.pageSubject.next(this.lastData.nextPath());
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.last = /**
         * @return {?}
         */
            function () {
                if (!this.lastData || !this.lastData.hasLast()) {
                    return;
                }
                this.pageSubject.next(this.lastData.lastPath());
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.hasFirst = /**
         * @return {?}
         */
            function () {
                return this.lastData && this.lastData.hasFirst();
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.hasPrevious = /**
         * @return {?}
         */
            function () {
                return this.lastData && this.lastData.hasPrevious();
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.hasNext = /**
         * @return {?}
         */
            function () {
                return this.lastData && this.lastData.hasNext();
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.hasLast = /**
         * @return {?}
         */
            function () {
                return this.lastData && this.lastData.hasLast();
            };
        /**
         * @param {?} __0
         * @return {?}
         */
        PageLoader.prototype.getServiceObservable = /**
         * @param {?} __0
         * @return {?}
         */
            function (_a) {
                var _b = __read(_a, 2), page = _b[0], data = _b[1];
                return page ? this.service.doGet(page, data) : _super.prototype.getServiceObservable.call(this, [data]);
            };
        /**
         * @return {?}
         */
        PageLoader.prototype.getSubjects = /**
         * @return {?}
         */
            function () {
                this.pageSubject = new rxjs.BehaviorSubject(undefined);
                var /** @type {?} */ subjects = _super.prototype.getSubjects.call(this);
                subjects.unshift(this.pageSubject);
                return subjects;
            };
        return PageLoader;
    }(Loader));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AnRestModule = (function () {
        function AnRestModule() {
        }
        /**
         * @param {?=} config
         * @return {?}
         */
        AnRestModule.config = /**
         * @param {?=} config
         * @return {?}
         */
            function (config) {
                return {
                    ngModule: AnRestModule,
                    providers: [
                        {
                            provide: AnRestConfig,
                            useValue: config,
                        },
                        EventsService,
                        DataInfoService,
                        {
                            provide: ANREST_CACHE_SERVICES,
                            useClass: LocalStorageResponseCache,
                            multi: true
                        },
                        {
                            provide: ANREST_CACHE_SERVICES,
                            useClass: MemoryResponseCache,
                            multi: true
                        },
                        {
                            provide: ANREST_CACHE_SERVICES,
                            useClass: NoResponseCache,
                            multi: true
                        },
                        {
                            provide: ResponseCache,
                            useClass: config.cache || NoResponseCache
                        },
                        {
                            provide: AuthTokenProviderService,
                            useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                        },
                        {
                            provide: AuthService,
                            useClass: config.authService || JwtAuthService
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: AuthInterceptor,
                            multi: true
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: ReferenceInterceptor,
                            multi: true
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: CacheInterceptor,
                            multi: true
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: ApiInterceptor,
                            multi: true
                        },
                        {
                            provide: ANREST_HTTP_DATA_NORMALIZERS,
                            useClass: ReferenceDataNormalizer,
                            multi: true
                        },
                        {
                            provide: ANREST_HTTP_DATA_NORMALIZERS,
                            useClass: LdJsonDataNormalizer,
                            multi: true
                        },
                        {
                            provide: ANREST_HTTP_DATA_NORMALIZERS,
                            useClass: JsonDataNormalizer,
                            multi: true
                        },
                        ObjectCollector,
                        ApiProcessor,
                        ApiService
                    ]
                };
            };
        /**
         * @param {?=} config
         * @return {?}
         */
        AnRestModule.configWithoutNormalizers = /**
         * @param {?=} config
         * @return {?}
         */
            function (config) {
                return {
                    ngModule: AnRestModule,
                    providers: [
                        {
                            provide: AnRestConfig,
                            useValue: config,
                        },
                        EventsService,
                        DataInfoService,
                        {
                            provide: ANREST_CACHE_SERVICES,
                            useClass: LocalStorageResponseCache,
                            multi: true
                        },
                        {
                            provide: ANREST_CACHE_SERVICES,
                            useClass: MemoryResponseCache,
                            multi: true
                        },
                        {
                            provide: ANREST_CACHE_SERVICES,
                            useClass: NoResponseCache,
                            multi: true
                        },
                        {
                            provide: ResponseCache,
                            useClass: config.cache || NoResponseCache
                        },
                        {
                            provide: AuthTokenProviderService,
                            useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                        },
                        {
                            provide: AuthService,
                            useClass: config.authService || JwtAuthService
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: AuthInterceptor,
                            multi: true
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: ReferenceInterceptor,
                            multi: true
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: CacheInterceptor,
                            multi: true
                        },
                        {
                            provide: http.HTTP_INTERCEPTORS,
                            useClass: ApiInterceptor,
                            multi: true
                        },
                        {
                            provide: ANREST_HTTP_DATA_NORMALIZERS,
                            useClass: ReferenceDataNormalizer,
                            multi: true
                        },
                        ObjectCollector,
                        ApiProcessor,
                        ApiService
                    ]
                };
            };
        AnRestModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            http.HttpClientModule,
                        ]
                    },] },
        ];
        return AnRestModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.AnRestConfig = AnRestConfig;
    exports.Collection = Collection;
    exports.ApiService = ApiService;
    exports.EntityInfo = EntityInfo;
    exports.CollectionInfo = CollectionInfo;
    exports.DataInfoService = DataInfoService;
    exports.ReferenceInterceptor = ReferenceInterceptor;
    exports.ApiInterceptor = ApiInterceptor;
    exports.ApiProcessor = ApiProcessor;
    exports.ObjectCollector = ObjectCollector;
    exports.CacheInterceptor = CacheInterceptor;
    exports.ResponseCache = ResponseCache;
    exports.ANREST_CACHE_SERVICES = ANREST_CACHE_SERVICES;
    exports.NoResponseCache = NoResponseCache;
    exports.MemoryResponseCache = MemoryResponseCache;
    exports.LocalStorageResponseCache = LocalStorageResponseCache;
    exports.AuthService = AuthService;
    exports.AuthTokenProviderService = AuthTokenProviderService;
    exports.LocalStorageAuthTokenProviderService = LocalStorageAuthTokenProviderService;
    exports.AuthInterceptor = AuthInterceptor;
    exports.JwtAuthService = JwtAuthService;
    exports.Resource = Resource;
    exports.Property = Property;
    exports.HttpService = HttpService;
    exports.Header = Header;
    exports.Headers = Headers;
    exports.Body = Body;
    exports.Subresource = Subresource;
    exports.Id = Id;
    exports.EventsService = EventsService;
    exports.BaseEvent = BaseEvent;
    exports.BeforeGetEvent = BeforeGetEvent;
    exports.AfterGetEvent = AfterGetEvent;
    exports.BeforeSaveEvent = BeforeSaveEvent;
    exports.AfterSaveEvent = AfterSaveEvent;
    exports.BeforeRemoveEvent = BeforeRemoveEvent;
    exports.AfterRemoveEvent = AfterRemoveEvent;
    exports.ANREST_HTTP_EVENT_LISTENERS = ANREST_HTTP_EVENT_LISTENERS;
    exports.MetaService = MetaService;
    exports.MetaInfo = MetaInfo;
    exports.PropertyMetaInfo = PropertyMetaInfo;
    exports.EventListenerMetaInfo = EventListenerMetaInfo;
    exports.EntityMetaInfo = EntityMetaInfo;
    exports.ServiceMetaInfo = ServiceMetaInfo;
    exports.ResponseNode = ResponseNode;
    exports.ANREST_HTTP_DATA_NORMALIZERS = ANREST_HTTP_DATA_NORMALIZERS;
    exports.ReferenceDataNormalizer = ReferenceDataNormalizer;
    exports.LdJsonDataNormalizer = LdJsonDataNormalizer;
    exports.JsonDataNormalizer = JsonDataNormalizer;
    exports.Loader = Loader;
    exports.InfiniteScrollLoader = InfiniteScrollLoader;
    exports.PageLoader = PageLoader;
    exports.AnRestModule = AnRestModule;
    exports.ɵm = AuthInterceptor;
    exports.ɵk = AuthService;
    exports.ɵi = AuthTokenProviderService;
    exports.ɵl = JwtAuthService;
    exports.ɵj = LocalStorageAuthTokenProviderService;
    exports.ɵd = ANREST_CACHE_SERVICES;
    exports.ɵo = CacheInterceptor;
    exports.ɵe = LocalStorageResponseCache;
    exports.ɵf = MemoryResponseCache;
    exports.ɵg = NoResponseCache;
    exports.ɵh = ResponseCache;
    exports.ɵa = AnRestConfig;
    exports.ɵp = ApiInterceptor;
    exports.ɵv = ApiProcessor;
    exports.ɵw = ApiService;
    exports.ɵc = DataInfoService;
    exports.ɵu = ObjectCollector;
    exports.ɵn = ReferenceInterceptor;
    exports.ɵb = EventsService;
    exports.ɵq = ANREST_HTTP_DATA_NORMALIZERS;
    exports.ɵt = JsonDataNormalizer;
    exports.ɵs = LdJsonDataNormalizer;
    exports.ɵr = ReferenceDataNormalizer;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5yZXN0LWxpYi51bWQuanMubWFwIiwic291cmNlcyI6WyJuZzovL0BhbnJlc3QvbGliL2NvcmUvY29uZmlnLnRzIixudWxsLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvZGF0YS1pbmZvLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvY29sbGVjdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvZXZlbnRzL2V2ZW50cy50cyIsIm5nOi8vQGFucmVzdC9saWIvbWV0YS9tZXRhLWluZm8udHMiLCJuZzovL0BhbnJlc3QvbGliL21ldGEvbWV0YS1zZXJ2aWNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9ldmVudHMvZXZlbnRzLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvYXBpLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvcmVmZXJlbmNlLmludGVyY2VwdG9yLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9yZXNwb25zZS9kYXRhLW5vcm1hbGl6ZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3Jlc3BvbnNlL3Jlc3BvbnNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jb3JlL29iamVjdC1jb2xsZWN0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvYXBpLnByb2Nlc3Nvci50cyIsIm5nOi8vQGFucmVzdC9saWIvY29yZS9hcGkuaW50ZXJjZXB0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NhY2hlL3Jlc3BvbnNlLWNhY2hlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jYWNoZS9uby1yZXNwb25zZS1jYWNoZS50cyIsIm5nOi8vQGFucmVzdC9saWIvY2FjaGUvY2FjaGUuaW50ZXJjZXB0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NhY2hlL21lbW9yeS1yZXNwb25zZS1jYWNoZS50cyIsIm5nOi8vQGFucmVzdC9saWIvcmVzcG9uc2UvcmVmZXJlbmNlLWRhdGEtbm9ybWFsaXplci50cyIsIm5nOi8vQGFucmVzdC9saWIvcmVzcG9uc2UvbGQtanNvbi1kYXRhLW5vcm1hbGl6ZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3Jlc3BvbnNlL2pzb24tZGF0YS1ub3JtYWxpemVyLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jYWNoZS9sb2NhbC1zdG9yYWdlLXJlc3BvbnNlLWNhY2hlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hdXRoL2F1dGguc2VydmljZS50cyIsIm5nOi8vQGFucmVzdC9saWIvYXV0aC9hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2F1dGgvbG9jYWwtc3RvcmFnZS1hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2F1dGgvYXV0aC5pbnRlcmNlcHRvci50cyIsIm5nOi8vQGFucmVzdC9saWIvYXV0aC9qd3QtYXV0aC5zZXJ2aWNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9yZXNvdXJjZS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9wcm9wZXJ0eS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9odHRwLXNlcnZpY2UuYW5ub3RhdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvYW5ub3RhdGlvbnMvaGVhZGVyLmFubm90YXRpb24udHMiLCJuZzovL0BhbnJlc3QvbGliL2Fubm90YXRpb25zL2hlYWRlcnMuYW5ub3RhdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvYW5ub3RhdGlvbnMvYm9keS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9zdWJyZXNvdXJjZS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9pZC5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi91dGlscy9sb2FkZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3V0aWxzL2luZmluaXRlLXNjcm9sbC1sb2FkZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3V0aWxzL3BhZ2UtbG9hZGVyLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbnJlc3QubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQXBpQ29uZmlnIHtcbiAgcmVzcG9uc2VUeXBlPzogc3RyaW5nO1xuICBiYXNlVXJsOiBzdHJpbmc7XG4gIGF1dGhVcmw/OiBzdHJpbmc7XG4gIGNhY2hlPzogYW55O1xuICB0b2tlblByb3ZpZGVyPzogYW55O1xuICBhdXRoU2VydmljZT86IGFueTtcbiAgY2FjaGVUVEw/OiBudW1iZXI7XG4gIGV4Y2x1ZGVkVXJscz86IHN0cmluZ1tdO1xuICBkZWZhdWx0SGVhZGVycz86IHsgbmFtZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBhcHBlbmQ/OiBib29sZWFufVtdO1xufVxuXG5leHBvcnQgY29uc3QgQW5SZXN0Q29uZmlnID0gbmV3IEluamVjdGlvblRva2VuPEFwaUNvbmZpZz4oJ2FucmVzdC5hcGlfY29uZmlnJyk7XG4iLCIvKiEgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuQ29weXJpZ2h0IChjKSBNaWNyb3NvZnQgQ29ycG9yYXRpb24uIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZVxyXG50aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS4gWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZVxyXG5MaWNlbnNlIGF0IGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVEhJUyBDT0RFIElTIFBST1ZJREVEIE9OIEFOICpBUyBJUyogQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxyXG5LSU5ELCBFSVRIRVIgRVhQUkVTUyBPUiBJTVBMSUVELCBJTkNMVURJTkcgV0lUSE9VVCBMSU1JVEFUSU9OIEFOWSBJTVBMSUVEXHJcbldBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBUSVRMRSwgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UsXHJcbk1FUkNIQU5UQUJMSVRZIE9SIE5PTi1JTkZSSU5HRU1FTlQuXHJcblxyXG5TZWUgdGhlIEFwYWNoZSBWZXJzaW9uIDIuMCBMaWNlbnNlIGZvciBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnNcclxuYW5kIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xyXG4vKiBnbG9iYWwgUmVmbGVjdCwgUHJvbWlzZSAqL1xyXG5cclxudmFyIGV4dGVuZFN0YXRpY3MgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHxcclxuICAgICh7IF9fcHJvdG9fXzogW10gfSBpbnN0YW5jZW9mIEFycmF5ICYmIGZ1bmN0aW9uIChkLCBiKSB7IGQuX19wcm90b19fID0gYjsgfSkgfHxcclxuICAgIGZ1bmN0aW9uIChkLCBiKSB7IGZvciAodmFyIHAgaW4gYikgaWYgKGIuaGFzT3duUHJvcGVydHkocCkpIGRbcF0gPSBiW3BdOyB9O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXh0ZW5kcyhkLCBiKSB7XHJcbiAgICBleHRlbmRTdGF0aWNzKGQsIGIpO1xyXG4gICAgZnVuY3Rpb24gX18oKSB7IHRoaXMuY29uc3RydWN0b3IgPSBkOyB9XHJcbiAgICBkLnByb3RvdHlwZSA9IGIgPT09IG51bGwgPyBPYmplY3QuY3JlYXRlKGIpIDogKF9fLnByb3RvdHlwZSA9IGIucHJvdG90eXBlLCBuZXcgX18oKSk7XHJcbn1cclxuXHJcbmV4cG9ydCB2YXIgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uIF9fYXNzaWduKHQpIHtcclxuICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG4gICAgICAgIHMgPSBhcmd1bWVudHNbaV07XHJcbiAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKSB0W3BdID0gc1twXTtcclxuICAgIH1cclxuICAgIHJldHVybiB0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19yZXN0KHMsIGUpIHtcclxuICAgIHZhciB0ID0ge307XHJcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcclxuICAgICAgICB0W3BdID0gc1twXTtcclxuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcclxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMClcclxuICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XHJcbiAgICByZXR1cm4gdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3BhcmFtKHBhcmFtSW5kZXgsIGRlY29yYXRvcikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIGtleSkgeyBkZWNvcmF0b3IodGFyZ2V0LCBrZXksIHBhcmFtSW5kZXgpOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKSB7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEobWV0YWRhdGFLZXksIG1ldGFkYXRhVmFsdWUpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hd2FpdGVyKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xyXG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZShyZXN1bHQudmFsdWUpOyB9KS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XHJcbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2dlbmVyYXRvcih0aGlzQXJnLCBib2R5KSB7XHJcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xyXG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcclxuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XHJcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XHJcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcclxuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcclxuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cclxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXhwb3J0U3RhcihtLCBleHBvcnRzKSB7XHJcbiAgICBmb3IgKHZhciBwIGluIG0pIGlmICghZXhwb3J0cy5oYXNPd25Qcm9wZXJ0eShwKSkgZXhwb3J0c1twXSA9IG1bcF07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3ZhbHVlcyhvKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl0sIGkgPSAwO1xyXG4gICAgaWYgKG0pIHJldHVybiBtLmNhbGwobyk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG5leHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgaWYgKG8gJiYgaSA+PSBvLmxlbmd0aCkgbyA9IHZvaWQgMDtcclxuICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6IG8gJiYgb1tpKytdLCBkb25lOiAhbyB9O1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3JlYWQobywgbikge1xyXG4gICAgdmFyIG0gPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb1tTeW1ib2wuaXRlcmF0b3JdO1xyXG4gICAgaWYgKCFtKSByZXR1cm4gbztcclxuICAgIHZhciBpID0gbS5jYWxsKG8pLCByLCBhciA9IFtdLCBlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICB3aGlsZSAoKG4gPT09IHZvaWQgMCB8fCBuLS0gPiAwKSAmJiAhKHIgPSBpLm5leHQoKSkuZG9uZSkgYXIucHVzaChyLnZhbHVlKTtcclxuICAgIH1cclxuICAgIGNhdGNoIChlcnJvcikgeyBlID0geyBlcnJvcjogZXJyb3IgfTsgfVxyXG4gICAgZmluYWxseSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKHIgJiYgIXIuZG9uZSAmJiAobSA9IGlbXCJyZXR1cm5cIl0pKSBtLmNhbGwoaSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpbmFsbHkgeyBpZiAoZSkgdGhyb3cgZS5lcnJvcjsgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19zcHJlYWQoKSB7XHJcbiAgICBmb3IgKHZhciBhciA9IFtdLCBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKylcclxuICAgICAgICBhciA9IGFyLmNvbmNhdChfX3JlYWQoYXJndW1lbnRzW2ldKSk7XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2F3YWl0KHYpIHtcclxuICAgIHJldHVybiB0aGlzIGluc3RhbmNlb2YgX19hd2FpdCA/ICh0aGlzLnYgPSB2LCB0aGlzKSA6IG5ldyBfX2F3YWl0KHYpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY0dlbmVyYXRvcih0aGlzQXJnLCBfYXJndW1lbnRzLCBnZW5lcmF0b3IpIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgZyA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSwgaSwgcSA9IFtdO1xyXG4gICAgcmV0dXJuIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IGlmIChnW25dKSBpW25dID0gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChhLCBiKSB7IHEucHVzaChbbiwgdiwgYSwgYl0pID4gMSB8fCByZXN1bWUobiwgdik7IH0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiByZXN1bWUobiwgdikgeyB0cnkgeyBzdGVwKGdbbl0odikpOyB9IGNhdGNoIChlKSB7IHNldHRsZShxWzBdWzNdLCBlKTsgfSB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKHIpIHsgci52YWx1ZSBpbnN0YW5jZW9mIF9fYXdhaXQgPyBQcm9taXNlLnJlc29sdmUoci52YWx1ZS52KS50aGVuKGZ1bGZpbGwsIHJlamVjdCkgOiBzZXR0bGUocVswXVsyXSwgcik7IH1cclxuICAgIGZ1bmN0aW9uIGZ1bGZpbGwodmFsdWUpIHsgcmVzdW1lKFwibmV4dFwiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHJlamVjdCh2YWx1ZSkgeyByZXN1bWUoXCJ0aHJvd1wiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShmLCB2KSB7IGlmIChmKHYpLCBxLnNoaWZ0KCksIHEubGVuZ3RoKSByZXN1bWUocVswXVswXSwgcVswXVsxXSk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNEZWxlZ2F0b3Iobykge1xyXG4gICAgdmFyIGksIHA7XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIsIGZ1bmN0aW9uIChlKSB7IHRocm93IGU7IH0pLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuLCBmKSB7IGlbbl0gPSBvW25dID8gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIChwID0gIXApID8geyB2YWx1ZTogX19hd2FpdChvW25dKHYpKSwgZG9uZTogbiA9PT0gXCJyZXR1cm5cIiB9IDogZiA/IGYodikgOiB2OyB9IDogZjsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY1ZhbHVlcyhvKSB7XHJcbiAgICBpZiAoIVN5bWJvbC5hc3luY0l0ZXJhdG9yKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3ltYm9sLmFzeW5jSXRlcmF0b3IgaXMgbm90IGRlZmluZWQuXCIpO1xyXG4gICAgdmFyIG0gPSBvW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSwgaTtcclxuICAgIHJldHVybiBtID8gbS5jYWxsKG8pIDogKG8gPSB0eXBlb2YgX192YWx1ZXMgPT09IFwiZnVuY3Rpb25cIiA/IF9fdmFsdWVzKG8pIDogb1tTeW1ib2wuaXRlcmF0b3JdKCksIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpKTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyBpW25dID0gb1tuXSAmJiBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkgeyB2ID0gb1tuXSh2KSwgc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgdi5kb25lLCB2LnZhbHVlKTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShyZXNvbHZlLCByZWplY3QsIGQsIHYpIHsgUHJvbWlzZS5yZXNvbHZlKHYpLnRoZW4oZnVuY3Rpb24odikgeyByZXNvbHZlKHsgdmFsdWU6IHYsIGRvbmU6IGQgfSk7IH0sIHJlamVjdCk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fbWFrZVRlbXBsYXRlT2JqZWN0KGNvb2tlZCwgcmF3KSB7XHJcbiAgICBpZiAoT2JqZWN0LmRlZmluZVByb3BlcnR5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb29rZWQsIFwicmF3XCIsIHsgdmFsdWU6IHJhdyB9KTsgfSBlbHNlIHsgY29va2VkLnJhdyA9IHJhdzsgfVxyXG4gICAgcmV0dXJuIGNvb2tlZDtcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydFN0YXIobW9kKSB7XHJcbiAgICBpZiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSByZXR1cm4gbW9kO1xyXG4gICAgdmFyIHJlc3VsdCA9IHt9O1xyXG4gICAgaWYgKG1vZCAhPSBudWxsKSBmb3IgKHZhciBrIGluIG1vZCkgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcclxuICAgIHJlc3VsdC5kZWZhdWx0ID0gbW9kO1xyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9faW1wb3J0RGVmYXVsdChtb2QpIHtcclxuICAgIHJldHVybiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSA/IG1vZCA6IHsgZGVmYXVsdDogbW9kIH07XHJcbn1cclxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgRW50aXR5SW5mbyB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcGF0aCkge31cblxuICBnZXQgcGF0aCgpIHtcbiAgICByZXR1cm4gdGhpcy5fcGF0aDtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgQ29sbGVjdGlvbkluZm8ge1xuICBwdWJsaWMgdG90YWw6IG51bWJlcjtcbiAgcHVibGljIHBhZ2U6IG51bWJlcjtcbiAgcHVibGljIGxhc3RQYWdlOiBudW1iZXI7XG4gIHB1YmxpYyBmaXJzdDogc3RyaW5nO1xuICBwdWJsaWMgcHJldmlvdXM6IHN0cmluZztcbiAgcHVibGljIG5leHQ6IHN0cmluZztcbiAgcHVibGljIGxhc3Q6IHN0cmluZztcbiAgcHVibGljIGFsaWFzOiBzdHJpbmc7XG4gIHB1YmxpYyB0eXBlOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfcGF0aCkge31cblxuICBnZXQgcGF0aCgpIHtcbiAgICByZXR1cm4gdGhpcy5fcGF0aDtcbiAgfVxufVxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRGF0YUluZm9TZXJ2aWNlIHtcblxuICBnZXQob2JqZWN0KTogYW55IHtcbiAgICByZXR1cm4gUmVmbGVjdC5nZXRNZXRhZGF0YSgnYW5yZXN0OmluZm8nLCBvYmplY3QpO1xuICB9XG5cbiAgc2V0KG9iamVjdCwgaW5mbzogYW55KSB7XG4gICAgUmVmbGVjdC5kZWZpbmVNZXRhZGF0YSgnYW5yZXN0OmluZm8nLCBpbmZvLCBvYmplY3QpO1xuICB9XG5cbiAgZ2V0Rm9yQ29sbGVjdGlvbihvYmplY3QpOiBDb2xsZWN0aW9uSW5mbyB7XG4gICAgcmV0dXJuIDxDb2xsZWN0aW9uSW5mbz50aGlzLmdldChvYmplY3QpO1xuICB9XG5cbiAgZ2V0Rm9yRW50aXR5KG9iamVjdCk6IEVudGl0eUluZm8ge1xuICAgIHJldHVybiA8RW50aXR5SW5mbz50aGlzLmdldChvYmplY3QpO1xuICB9XG5cbiAgc2V0Rm9yQ29sbGVjdGlvbihvYmplY3QsIGluZm86IENvbGxlY3Rpb25JbmZvKSB7XG4gICAgdGhpcy5zZXQob2JqZWN0LCBpbmZvKTtcbiAgfVxuXG4gIHNldEZvckVudGl0eShvYmplY3QsIGluZm86IEVudGl0eUluZm8pIHtcbiAgICB0aGlzLnNldChvYmplY3QsIGluZm8pO1xuICB9XG59XG4iLCJpbXBvcnQgeyBDb2xsZWN0aW9uSW5mbyB9IGZyb20gJy4vZGF0YS1pbmZvLnNlcnZpY2UnO1xuaW1wb3J0IHsgQXBpU2VydmljZSB9IGZyb20gJy4vYXBpLnNlcnZpY2UnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5leHBvcnQgY2xhc3MgQ29sbGVjdGlvbjxUPiBleHRlbmRzIEFycmF5PFQ+IHtcblxuICBzdGF0aWMgbWVyZ2U8VD4oYzE6IENvbGxlY3Rpb248VD4sIGMyOiBDb2xsZWN0aW9uPFQ+KTogQ29sbGVjdGlvbjxUPiB7XG4gICAgY29uc3QgaW5mbyA9IG5ldyBDb2xsZWN0aW9uSW5mbyhjMS5pbmZvLnBhdGgpO1xuICAgIGluZm8udHlwZSA9IGMxLmluZm8udHlwZTtcbiAgICBpbmZvLm5leHQgPSBjMi5pbmZvLm5leHQ7XG4gICAgaW5mby5wcmV2aW91cyA9IGMxLmluZm8ucHJldmlvdXM7XG4gICAgaW5mby5maXJzdCA9IGMxLmluZm8uZmlyc3Q7XG4gICAgaW5mby5sYXN0ID0gYzEuaW5mby5sYXN0O1xuICAgIGluZm8udG90YWwgPSBjMS5pbmZvLnRvdGFsO1xuICAgIGluZm8ucGFnZSA9IGMxLmluZm8ucGFnZTtcbiAgICBpbmZvLmxhc3RQYWdlID0gYzEuaW5mby5sYXN0UGFnZTtcbiAgICBjb25zdCBjID0gbmV3IENvbGxlY3Rpb248VD4oYzEuc2VydmljZSwgaW5mbyk7XG4gICAgYzEuZm9yRWFjaCgodikgPT4gYy5wdXNoKHYpKTtcbiAgICBjMi5mb3JFYWNoKCh2KSA9PiBjLnB1c2godikpO1xuICAgIHJldHVybiBjO1xuICB9XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXJ2aWNlOiBBcGlTZXJ2aWNlLCBwcml2YXRlIGluZm86IENvbGxlY3Rpb25JbmZvLCAuLi5pdGVtczogVFtdKSB7XG4gICAgc3VwZXIoLi4uaXRlbXMpO1xuICAgIE9iamVjdC5zZXRQcm90b3R5cGVPZih0aGlzLCBDb2xsZWN0aW9uLnByb3RvdHlwZSk7XG4gIH1cblxuICBnZXQgdG90YWwoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLnRvdGFsO1xuICB9XG5cbiAgZ2V0IHBhZ2UoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLnBhZ2U7XG4gIH1cblxuICBnZXQgcGFnZVRvdGFsKCk6IG51bWJlciB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5sYXN0UGFnZTtcbiAgfVxuXG4gIGZpcnN0KCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5maXJzdCk7XG4gIH1cblxuICBwcmV2aW91cygpOiBPYnNlcnZhYmxlPFRbXT4ge1xuICAgIHJldHVybiA8T2JzZXJ2YWJsZTxUW10+PnRoaXMuc2VydmljZS5kb0dldCh0aGlzLmluZm8ucHJldmlvdXMpO1xuICB9XG5cbiAgbmV4dCgpOiBPYnNlcnZhYmxlPFRbXT4ge1xuICAgIHJldHVybiA8T2JzZXJ2YWJsZTxUW10+PnRoaXMuc2VydmljZS5kb0dldCh0aGlzLmluZm8ubmV4dCk7XG4gIH1cblxuICBsYXN0KCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5sYXN0KTtcbiAgfVxuXG4gIGZpcnN0UGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8uZmlyc3Q7XG4gIH1cblxuICBwcmV2aW91c1BhdGgoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLnByZXZpb3VzO1xuICB9XG5cbiAgbmV4dFBhdGgoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLm5leHQ7XG4gIH1cblxuICBsYXN0UGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ubGFzdDtcbiAgfVxuXG4gIGxvYWRNb3JlKCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIHRoaXMubmV4dCgpLnBpcGUoXG4gICAgICBtYXAoKHJlc3VsdDogQ29sbGVjdGlvbjxUPikgPT4gQ29sbGVjdGlvbi5tZXJnZTxUPih0aGlzLCByZXN1bHQpKVxuICAgICk7XG4gIH1cblxuICBoYXNNb3JlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc05leHQoKTtcbiAgfVxuXG4gIGhhc05leHQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuICEhdGhpcy5pbmZvLm5leHQ7XG4gIH1cblxuICBoYXNQcmV2aW91cygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gISF0aGlzLmluZm8ucHJldmlvdXM7XG4gIH1cblxuICBoYXNGaXJzdCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5oYXNQcmV2aW91cygpO1xuICB9XG5cbiAgaGFzTGFzdCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5oYXNOZXh0KCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEV2ZW50IHtcbiAgZW50aXR5KCk6IEZ1bmN0aW9uO1xuICBkYXRhKCk6IGFueTtcbn1cblxuZXhwb3J0IGNsYXNzIEJhc2VFdmVudCBpbXBsZW1lbnRzIEV2ZW50IHtcbiAgX2RhdGE/OiBhbnk7XG4gIHByb3RlY3RlZCByZWFkb25seSB0eXBlOiBGdW5jdGlvbjtcblxuICBjb25zdHJ1Y3RvcihkYXRhOiBhbnksIHR5cGU/OiBGdW5jdGlvbikge1xuICAgIHRoaXMuX2RhdGEgPSBkYXRhO1xuICAgIHRoaXMudHlwZSA9IHR5cGU7XG4gIH1cblxuICBkYXRhKCk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuX2RhdGE7XG4gIH1cblxuICBlbnRpdHkoKTogRnVuY3Rpb24ge1xuICAgIHJldHVybiB0aGlzLnR5cGUgfHwgKEFycmF5LmlzQXJyYXkodGhpcy5fZGF0YSkgPyB0aGlzLl9kYXRhWzBdLmNvbnN0cnVjdG9yIDogdGhpcy5fZGF0YS5jb25zdHJ1Y3Rvcik7XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIEJlZm9yZUdldEV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHtcbiAgcHJpdmF0ZSByZWFkb25seSBfZmlsdGVyOiBIdHRwUGFyYW1zO1xuXG4gIGNvbnN0cnVjdG9yKHBhdGg6IHN0cmluZywgdHlwZT86IEZ1bmN0aW9uLCBmaWx0ZXI/OiBIdHRwUGFyYW1zKSB7XG4gICAgc3VwZXIocGF0aCwgdHlwZSk7XG4gICAgdGhpcy5fZmlsdGVyID0gZmlsdGVyO1xuICB9XG5cbiAgZW50aXR5KCk6IEZ1bmN0aW9uIHtcbiAgICByZXR1cm4gdGhpcy50eXBlO1xuICB9XG5cbiAgZmlsdGVyKCk6IEh0dHBQYXJhbXMge1xuICAgIHJldHVybiB0aGlzLl9maWx0ZXI7XG4gIH1cbn1cbmV4cG9ydCBjbGFzcyBBZnRlckdldEV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHt9XG5leHBvcnQgY2xhc3MgQmVmb3JlU2F2ZUV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHt9XG5leHBvcnQgY2xhc3MgQWZ0ZXJTYXZlRXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBCZWZvcmVSZW1vdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuZXhwb3J0IGNsYXNzIEFmdGVyUmVtb3ZlRXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cblxuZXhwb3J0IGludGVyZmFjZSBFdmVudExpc3RlbmVyIHtcbiAgaGFuZGxlKGV2ZW50OiBFdmVudCk7XG59XG5cbmV4cG9ydCBsZXQgQU5SRVNUX0hUVFBfRVZFTlRfTElTVEVORVJTID0gbmV3IEluamVjdGlvblRva2VuPEV2ZW50TGlzdGVuZXJbXT4oJ2FucmVzdC5odHRwX2V2ZW50X2xpc3RlbmVycycpO1xuIiwiaW1wb3J0IHsgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbmV4cG9ydCBjbGFzcyBNZXRhSW5mbyB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfdHlwZTogRnVuY3Rpb24pIHt9XG5cbiAgZ2V0IHR5cGUoKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5fdHlwZTtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgUHJvcGVydHlNZXRhSW5mbyBleHRlbmRzIE1ldGFJbmZvIHtcbiAgcHVibGljIGlzQ29sbGVjdGlvbjogYm9vbGVhbjtcbiAgcHVibGljIGV4Y2x1ZGVXaGVuU2F2aW5nOiBib29sZWFuO1xufVxuXG5leHBvcnQgY2xhc3MgRXZlbnRMaXN0ZW5lck1ldGFJbmZvIGV4dGVuZHMgTWV0YUluZm8ge1xuICBwdWJsaWMgZXZlbnRzOiB7IGVudGl0eTogRnVuY3Rpb24sIHR5cGU6IEZ1bmN0aW9uIH1bXSA9IFtdO1xufVxuXG5leHBvcnQgY2xhc3MgRW50aXR5TWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBuYW1lOiBzdHJpbmc7XG4gIHB1YmxpYyBpZCA9ICdpZCc7XG4gIHB1YmxpYyBzZXJ2aWNlOiBGdW5jdGlvbjtcbiAgcHVibGljIGhlYWRlcnM6IHsgbmFtZTogc3RyaW5nLCB2YWx1ZTogKCkgPT4gc3RyaW5nLCBhcHBlbmQ6IGJvb2xlYW4sIGR5bmFtaWM6IGJvb2xlYW4gfVtdID0gW107XG4gIHB1YmxpYyBwcm9wZXJ0aWVzOiB7IFtpbmRleDogc3RyaW5nXTogUHJvcGVydHlNZXRhSW5mbyB9ID0ge307XG4gIHB1YmxpYyBzdWJyZXNvdXJjZXM6IHsgW2luZGV4OiBzdHJpbmddOiB7IG1ldGE6IEVudGl0eU1ldGFJbmZvLCBwYXRoOiBzdHJpbmd9IH0gPSB7fTtcbiAgcHVibGljIGJvZHk6IChvYmplY3Q6IGFueSkgPT4gc3RyaW5nO1xuICBwdWJsaWMgY2FjaGU6IEZ1bmN0aW9uO1xuXG4gIHB1YmxpYyBnZXRIZWFkZXJzKG9iamVjdD86IGFueSk6IEh0dHBIZWFkZXJzIHtcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycygpO1xuICAgIGhlYWRlcnMgPSBoZWFkZXJzLnNldCgnWC1BblJlc3QtVHlwZScsIHRoaXMubmFtZSk7XG4gICAgZm9yIChjb25zdCBoZWFkZXIgb2YgdGhpcy5oZWFkZXJzKSB7XG4gICAgICBpZiAoIWhlYWRlci5keW5hbWljIHx8IG9iamVjdCkge1xuICAgICAgICBoZWFkZXJzID0gaGVhZGVyc1toZWFkZXIuYXBwZW5kID8gJ2FwcGVuZCcgOiAnc2V0J10oaGVhZGVyLm5hbWUsIGhlYWRlci52YWx1ZS5jYWxsKG9iamVjdCkpO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gaGVhZGVycztcbiAgfVxuXG4gIHB1YmxpYyBnZXRQcm9wZXJ0aWVzRm9yU2F2ZSgpIHtcbiAgICBjb25zdCBpbmNsdWRlZCA9IFtdO1xuICAgIGZvciAoY29uc3QgcHJvcGVydHkgaW4gdGhpcy5wcm9wZXJ0aWVzKSB7XG4gICAgICBpZiAoIXRoaXMucHJvcGVydGllc1twcm9wZXJ0eV0uZXhjbHVkZVdoZW5TYXZpbmcpIHtcbiAgICAgICAgaW5jbHVkZWQucHVzaChwcm9wZXJ0eSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBpbmNsdWRlZDtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgU2VydmljZU1ldGFJbmZvIGV4dGVuZHMgTWV0YUluZm8ge1xuICBwdWJsaWMgcGF0aDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHR5cGU6IEZ1bmN0aW9uLCBwcml2YXRlIF9lbnRpdHlNZXRhOiBFbnRpdHlNZXRhSW5mbykge1xuICAgIHN1cGVyKHR5cGUpO1xuICB9XG5cbiAgZ2V0IGVudGl0eU1ldGEoKTogRW50aXR5TWV0YUluZm8ge1xuICAgIHJldHVybiB0aGlzLl9lbnRpdHlNZXRhO1xuICB9XG59XG4iLCJpbXBvcnQgeyBFbnRpdHlNZXRhSW5mbywgUHJvcGVydHlNZXRhSW5mbywgTWV0YUluZm8sIFNlcnZpY2VNZXRhSW5mbywgRXZlbnRMaXN0ZW5lck1ldGFJbmZvIH0gZnJvbSAnLi9tZXRhLWluZm8nO1xuXG5leHBvcnQgY2xhc3MgTWV0YVNlcnZpY2Uge1xuXG4gIHByaXZhdGUgc3RhdGljIG1hcDogTWFwPGFueSwgTWV0YUluZm8+ID0gbmV3IE1hcDxhbnksIE1ldGFJbmZvPigpO1xuXG4gIHN0YXRpYyBnZXQoa2V5OiBhbnkpOiBhbnkge1xuICAgIHJldHVybiBNZXRhU2VydmljZS5tYXAuZ2V0KGtleSk7XG4gIH1cblxuICBzdGF0aWMgZ2V0QnlOYW1lKGtleTogc3RyaW5nKTogYW55IHtcbiAgICBmb3IgKGNvbnN0IG1ldGEgb2YgTWV0YVNlcnZpY2UubWFwLnZhbHVlcygpKSB7XG4gICAgICBpZiAobWV0YSBpbnN0YW5jZW9mIEVudGl0eU1ldGFJbmZvICYmIG1ldGEubmFtZSA9PT0ga2V5KSB7XG4gICAgICAgIHJldHVybiBtZXRhO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBnZXRPckNyZWF0ZUZvckVudGl0eShrZXk6IEZ1bmN0aW9uKTogRW50aXR5TWV0YUluZm8ge1xuICAgIGxldCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0KGtleSk7XG4gICAgaWYgKCFtZXRhKSB7XG4gICAgICBtZXRhID0gbmV3IEVudGl0eU1ldGFJbmZvKGtleSk7XG4gICAgICBNZXRhU2VydmljZS5zZXQobWV0YSk7XG4gICAgfVxuICAgIHJldHVybiA8RW50aXR5TWV0YUluZm8+bWV0YTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRPckNyZWF0ZUZvckV2ZW50TGlzdGVuZXIoa2V5OiBGdW5jdGlvbik6IEV2ZW50TGlzdGVuZXJNZXRhSW5mbyB7XG4gICAgbGV0IG1ldGEgPSBNZXRhU2VydmljZS5nZXQoa2V5KTtcbiAgICBpZiAoIW1ldGEpIHtcbiAgICAgIG1ldGEgPSBuZXcgRXZlbnRMaXN0ZW5lck1ldGFJbmZvKGtleSk7XG4gICAgICBNZXRhU2VydmljZS5zZXQobWV0YSk7XG4gICAgfVxuICAgIHJldHVybiA8RXZlbnRMaXN0ZW5lck1ldGFJbmZvPm1ldGE7XG4gIH1cblxuICBzdGF0aWMgZ2V0T3JDcmVhdGVGb3JQcm9wZXJ0eShrZXk6IGFueSwgcHJvcGVydHk6IHN0cmluZywgdHlwZT86IGFueSk6IFByb3BlcnR5TWV0YUluZm8ge1xuICAgIGNvbnN0IGVudGl0eU1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eShrZXkuY29uc3RydWN0b3IpO1xuICAgIGlmICghZW50aXR5TWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XSkge1xuICAgICAgZW50aXR5TWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XSA9IG5ldyBQcm9wZXJ0eU1ldGFJbmZvKHR5cGUgfHwgUmVmbGVjdC5nZXRNZXRhZGF0YSgnZGVzaWduOnR5cGUnLCBrZXksIHByb3BlcnR5KSk7XG4gICAgfVxuICAgIHJldHVybiBlbnRpdHlNZXRhLnByb3BlcnRpZXNbcHJvcGVydHldO1xuICB9XG5cbiAgc3RhdGljIGdldE9yQ3JlYXRlRm9ySHR0cFNlcnZpY2Uoa2V5OiBGdW5jdGlvbiwgZW50aXR5OiBGdW5jdGlvbik6IFNlcnZpY2VNZXRhSW5mbyB7XG4gICAgbGV0IG1ldGEgPSBNZXRhU2VydmljZS5nZXQoa2V5KTtcbiAgICBpZiAoIW1ldGEpIHtcbiAgICAgIG1ldGEgPSBuZXcgU2VydmljZU1ldGFJbmZvKGtleSwgTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkoZW50aXR5KSk7XG4gICAgICBNZXRhU2VydmljZS5zZXQobWV0YSk7XG4gICAgfVxuICAgIHJldHVybiA8U2VydmljZU1ldGFJbmZvPm1ldGE7XG4gIH1cblxuICBzdGF0aWMgc2V0KG1ldGE6IE1ldGFJbmZvKSB7XG4gICAgTWV0YVNlcnZpY2UubWFwLnNldChtZXRhLnR5cGUsIG1ldGEpO1xuICB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1xuICBBTlJFU1RfSFRUUF9FVkVOVF9MSVNURU5FUlMsIEV2ZW50LCBFdmVudExpc3RlbmVyXG59IGZyb20gJy4vZXZlbnRzJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBFdmVudHNTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEFOUkVTVF9IVFRQX0VWRU5UX0xJU1RFTkVSUykgcHJpdmF0ZSByZWFkb25seSBoYW5kbGVyczogRXZlbnRMaXN0ZW5lcltdXG4gICkge1xuICAgIGlmICghQXJyYXkuaXNBcnJheSh0aGlzLmhhbmRsZXJzKSkge1xuICAgICAgdGhpcy5oYW5kbGVycyA9IFtdO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBoYW5kbGUoZXZlbnQ6IEV2ZW50KTogRXZlbnQge1xuICAgIHRoaXMuaGFuZGxlcnMuZm9yRWFjaCgoaGFuZGxlcjogRXZlbnRMaXN0ZW5lcikgPT4ge1xuICAgICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRXZlbnRMaXN0ZW5lcihoYW5kbGVyLmNvbnN0cnVjdG9yKTtcbiAgICAgIGZvciAoY29uc3QgZXZlbnRNZXRhIG9mIG1ldGEuZXZlbnRzKSB7XG4gICAgICAgIGlmIChldmVudE1ldGEudHlwZSA9PT0gZXZlbnQuY29uc3RydWN0b3IgJiYgZXZlbnRNZXRhLmVudGl0eSA9PT0gZXZlbnQuZW50aXR5KCkpIHtcbiAgICAgICAgICBoYW5kbGVyLmhhbmRsZShldmVudCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gZXZlbnQ7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IERhdGFJbmZvU2VydmljZSB9IGZyb20gJy4vZGF0YS1pbmZvLnNlcnZpY2UnO1xuaW1wb3J0IHtcbiAgQWZ0ZXJHZXRFdmVudCxcbiAgQWZ0ZXJSZW1vdmVFdmVudCxcbiAgQWZ0ZXJTYXZlRXZlbnQsXG4gIEV2ZW50c1NlcnZpY2UsXG4gIEJlZm9yZUdldEV2ZW50LFxuICBCZWZvcmVSZW1vdmVFdmVudCxcbiAgQmVmb3JlU2F2ZUV2ZW50XG59IGZyb20gJy4uL2V2ZW50cyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlLCBTZXJ2aWNlTWV0YUluZm8gfSBmcm9tICcuLi9tZXRhJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFwaVNlcnZpY2Uge1xuICBwcm90ZWN0ZWQgbWV0YTogU2VydmljZU1ldGFJbmZvO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByb3RlY3RlZCBldmVudHM6IEV2ZW50c1NlcnZpY2UsXG4gICAgcHJvdGVjdGVkIGluZm9TZXJ2aWNlOiBEYXRhSW5mb1NlcnZpY2UsXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCBjb25maWc6IEFwaUNvbmZpZ1xuICApIHtcbiAgICB0aGlzLm1ldGEgPSBNZXRhU2VydmljZS5nZXQodGhpcy5jb25zdHJ1Y3Rvcik7XG4gIH1cblxuICBnZXRMaXN0KGZpbHRlcj86IEh0dHBQYXJhbXN8eyBbaW5kZXg6IHN0cmluZ106IHN0cmluZ3xudW1iZXJ8Ym9vbGVhbiB9KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5kb0dldCh0aGlzLm1ldGEucGF0aCwgZmlsdGVyKTtcbiAgfVxuXG4gIGdldChpZDogbnVtYmVyIHwgc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5kb0dldCh0aGlzLm1ldGEucGF0aCArICcvJyArIGlkKTtcbiAgfVxuXG4gIHJlbG9hZChlbnRpdHk6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZG9HZXQodGhpcy5pbmZvU2VydmljZS5nZXRGb3JFbnRpdHkoZW50aXR5KS5wYXRoKTtcbiAgfVxuXG4gIGdldFJlZmVyZW5jZShpZDogbnVtYmVyIHwgc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5kb0dldCgnJicgKyB0aGlzLm1ldGEucGF0aCArICcvJyArIGlkICsgJyMnICsgdGhpcy5tZXRhLmVudGl0eU1ldGEubmFtZSk7XG4gIH1cblxuICBkb0dldChwYXRoOiBzdHJpbmcsIGZpbHRlcj86IEh0dHBQYXJhbXN8eyBbaW5kZXg6IHN0cmluZ106IHN0cmluZ3xudW1iZXJ8Ym9vbGVhbiB9KSB7XG4gICAgbGV0IGY6IEh0dHBQYXJhbXM7XG4gICAgaWYgKGZpbHRlciBpbnN0YW5jZW9mIEh0dHBQYXJhbXMpIHtcbiAgICAgIGYgPSBmaWx0ZXI7XG4gICAgfSBlbHNlIHtcbiAgICAgIGYgPSBuZXcgSHR0cFBhcmFtcygpO1xuICAgICAgZm9yIChjb25zdCBrZXkgaW4gZmlsdGVyKSB7XG4gICAgICAgIGYgPSBmLnNldChrZXksIHR5cGVvZiBmaWx0ZXJba2V5XSA9PT0gJ2Jvb2xlYW4nID8gKGZpbHRlcltrZXldID8gJ3RydWUnIDogJ2ZhbHNlJykgOiBTdHJpbmcoZmlsdGVyW2tleV0pKTtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBCZWZvcmVHZXRFdmVudChwYXRoLCB0aGlzLm1ldGEuZW50aXR5TWV0YS50eXBlLCBmKSk7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodGhpcy5jb25maWcuYmFzZVVybCArIHBhdGgsIHtoZWFkZXJzOiB0aGlzLm1ldGEuZW50aXR5TWV0YS5nZXRIZWFkZXJzKCksIHBhcmFtczogZn0pLnBpcGUoXG4gICAgICB0YXAoKGRhdGEpID0+IHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQWZ0ZXJHZXRFdmVudChkYXRhLCB0aGlzLm1ldGEuZW50aXR5TWV0YS50eXBlKSkpXG4gICAgKTtcbiAgfVxuXG4gIHNhdmUoZW50aXR5OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQmVmb3JlU2F2ZUV2ZW50KGVudGl0eSkpO1xuICAgIGxldCBib2R5ID0ge307XG4gICAgaWYgKHRoaXMubWV0YS5lbnRpdHlNZXRhLmJvZHkpIHtcbiAgICAgIGJvZHkgPSB0aGlzLm1ldGEuZW50aXR5TWV0YS5ib2R5KGVudGl0eSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldFByb3BlcnRpZXNGb3JTYXZlKCkuZm9yRWFjaCgocHJvcGVydHkpID0+IGJvZHlbcHJvcGVydHldID0gZW50aXR5W3Byb3BlcnR5XSk7XG4gICAgfVxuICAgIGxldCBtZXRhID0gdGhpcy5pbmZvU2VydmljZS5nZXRGb3JFbnRpdHkoZW50aXR5KTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBbbWV0YSA/ICdwdXQnIDogJ3Bvc3QnXShcbiAgICAgICAgbWV0YSA/IHRoaXMuY29uZmlnLmJhc2VVcmwgKyBtZXRhLnBhdGggOiB0aGlzLmNvbmZpZy5iYXNlVXJsICsgdGhpcy5tZXRhLnBhdGgsXG4gICAgICAgIGJvZHksXG4gICAgICAgIHtoZWFkZXJzOiB0aGlzLm1ldGEuZW50aXR5TWV0YS5nZXRIZWFkZXJzKGVudGl0eSl9XG4gICAgICApLnBpcGUoXG4gICAgICAgIHRhcCgoZGF0YSkgPT4ge1xuICAgICAgICAgIG1ldGEgPSB0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShkYXRhKTtcbiAgICAgICAgICBpZiAoIW1ldGEpIHtcbiAgICAgICAgICAgIHRoaXMuaW5mb1NlcnZpY2Uuc2V0Rm9yRW50aXR5KGVudGl0eSwgbWV0YSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KSxcbiAgICAgICAgdGFwKCgpID0+IHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQWZ0ZXJTYXZlRXZlbnQoZW50aXR5KSkpXG4gICAgICApO1xuICB9XG5cbiAgcmVtb3ZlKGVudGl0eTogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEJlZm9yZVJlbW92ZUV2ZW50KGVudGl0eSkpO1xuICAgIGNvbnN0IG1ldGEgPSB0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShlbnRpdHkpO1xuICAgIGlmIChtZXRhKSB7XG4gICAgICByZXR1cm4gdGhpcy5odHRwLmRlbGV0ZSh0aGlzLmNvbmZpZy5iYXNlVXJsICsgbWV0YS5wYXRoLCB7aGVhZGVyczogdGhpcy5tZXRhLmVudGl0eU1ldGEuZ2V0SGVhZGVycyhlbnRpdHkpfSkucGlwZShcbiAgICAgICAgdGFwKCgpID0+IHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQWZ0ZXJSZW1vdmVFdmVudChlbnRpdHkpKSlcbiAgICAgICk7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIEh0dHBSZXF1ZXN0LFxuICBIdHRwSGFuZGxlcixcbiAgSHR0cEV2ZW50LFxuICBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXNwb25zZVxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUmVmZXJlbmNlSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgcmVhZG9ubHkgY29uZmlnOiBBcGlDb25maWdcbiAgKSB7fVxuXG4gIGludGVyY2VwdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICBpZiAocmVxdWVzdC51cmwuaW5kZXhPZih0aGlzLmNvbmZpZy5iYXNlVXJsKSAhPT0gMCB8fCByZXF1ZXN0LnVybCA9PT0gdGhpcy5jb25maWcuYXV0aFVybCB8fCAhdGhpcy5pc1JlZmVyZW5jZVJlcXVlc3QocmVxdWVzdCkpIHtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMuY3JlYXRlUmVmZXJlbmNlUmVzcG9uc2UocmVxdWVzdCk7XG4gIH1cblxuICBwcml2YXRlIGlzUmVmZXJlbmNlUmVxdWVzdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHJlcXVlc3QubWV0aG9kID09PSAnR0VUJyAmJiByZXF1ZXN0LnVybC5zbGljZSh0aGlzLmNvbmZpZy5iYXNlVXJsLmxlbmd0aCkuaW5kZXhPZignJicpID09PSAwO1xuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVSZWZlcmVuY2VSZXNwb25zZShyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+KSB7XG4gICAgY29uc3QgcGF0aCA9IHJlcXVlc3QudXJsV2l0aFBhcmFtcy5zbGljZSh0aGlzLmNvbmZpZy5iYXNlVXJsLmxlbmd0aCk7XG4gICAgcmV0dXJuIG9mKG5ldyBIdHRwUmVzcG9uc2Uoe1xuICAgICAgYm9keTogeydwYXRoJzogcGF0aC5zdWJzdHJpbmcoMSwgcGF0aC5pbmRleE9mKCcjJykpLCAnbWV0YSc6IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShwYXRoLnN1YnN0cmluZyhwYXRoLmluZGV4T2YoJyMnKSArIDEpKX0sXG4gICAgICBzdGF0dXM6IDIwMCxcbiAgICAgIHVybDogcmVxdWVzdC51cmxcbiAgICB9KSkucGlwZShcbiAgICAgIG1hcCgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlIGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XG4gICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNsb25lKHtcbiAgICAgICAgICAgIGhlYWRlcnM6IHJlc3BvbnNlLmhlYWRlcnMuc2V0KCdDb250ZW50LVR5cGUnLCAnQHJlZmVyZW5jZScpXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4vcmVzcG9uc2UnO1xuXG5leHBvcnQgaW50ZXJmYWNlIERhdGFOb3JtYWxpemVyIHtcbiAgbm9ybWFsaXplKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2U8YW55PiwgdHlwZT86IHN0cmluZyk6IFJlc3BvbnNlTm9kZTtcbiAgc3VwcG9ydHModHlwZTogc3RyaW5nKTogYm9vbGVhbjtcbn1cblxuZXhwb3J0IGxldCBBTlJFU1RfSFRUUF9EQVRBX05PUk1BTElaRVJTID0gbmV3IEluamVjdGlvblRva2VuPERhdGFOb3JtYWxpemVyW10+KCdhbnJlc3QuaHR0cF9kYXRhX25vcm1hbGl6ZXJzJyk7XG4iLCJleHBvcnQgY2xhc3MgUmVzcG9uc2VOb2RlIHtcbiAgcHVibGljIGRhdGE6IGFueTtcbiAgcHVibGljIGNvbGxlY3Rpb246IGJvb2xlYW47XG4gIHB1YmxpYyBtZXRhOiBhbnk7XG4gIHB1YmxpYyBpbmZvOiBhbnk7XG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEYXRhSW5mb1NlcnZpY2UgfSBmcm9tICcuL2RhdGEtaW5mby5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE9iamVjdENvbGxlY3RvciB7XG4gICAgbWFwOiBNYXA8c3RyaW5nLCBhbnk+ICA9IG5ldyBNYXA8c3RyaW5nLCBhbnk+KCk7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGluZm86IERhdGFJbmZvU2VydmljZSkge31cblxuICAgIHNldChkYXRhOiBhbnkpIHtcbiAgICAgIHRoaXMubWFwLnNldCh0aGlzLmluZm8uZ2V0KGRhdGEpLnBhdGgsIGRhdGEpO1xuICAgIH1cblxuICAgIHJlbW92ZShkYXRhOiBhbnkpIHtcbiAgICAgIGNvbnN0IGluZm8gPSB0aGlzLmluZm8uZ2V0KGRhdGEpO1xuICAgICAgaWYgKGluZm8pIHtcbiAgICAgICAgZGF0YSA9IGluZm8ucGF0aDtcbiAgICAgIH1cbiAgICAgIHRoaXMubWFwLmRlbGV0ZShkYXRhKTtcbiAgICB9XG5cbiAgICBnZXQoaWQ6IHN0cmluZykge1xuICAgICAgcmV0dXJuIHRoaXMubWFwLmdldChpZCk7XG4gICAgfVxuXG4gICAgaGFzKGlkOiBzdHJpbmcpIHtcbiAgICAgIHJldHVybiB0aGlzLm1hcC5oYXMoaWQpO1xuICAgIH1cblxuICAgIGNsZWFyKCkge1xuICAgICAgdGhpcy5tYXAuY2xlYXIoKTtcbiAgICB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi4vcmVzcG9uc2UvcmVzcG9uc2UnO1xuaW1wb3J0IHsgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQgeyBFbnRpdHlNZXRhSW5mbyB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgT2JqZWN0Q29sbGVjdG9yIH0gZnJvbSAnLi9vYmplY3QtY29sbGVjdG9yJztcbmltcG9ydCB7IENvbGxlY3Rpb24gfSBmcm9tICcuL2NvbGxlY3Rpb24nO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXBpUHJvY2Vzc29yIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcixcbiAgICBwcml2YXRlIGluZm86IERhdGFJbmZvU2VydmljZSxcbiAgICBwcml2YXRlIGNvbGxlY3RvcjogT2JqZWN0Q29sbGVjdG9yXG4gICkge31cblxuICBwcm9jZXNzKG5vZGU6IFJlc3BvbnNlTm9kZSwgbWV0aG9kOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiBub2RlLmNvbGxlY3Rpb24gPyB0aGlzLnByb2Nlc3NDb2xsZWN0aW9uKG5vZGUpIDogdGhpcy5wcm9jZXNzT2JqZWN0KG5vZGUpO1xuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzQ29sbGVjdGlvbihub2RlOiBSZXNwb25zZU5vZGUpIHtcbiAgICBsZXQgZGF0YTtcbiAgICBpZiAobm9kZS5pbmZvLnBhdGggIT09IHVuZGVmaW5lZCkge1xuICAgICAgZGF0YSA9IHRoaXMuY29sbGVjdG9yLmdldChub2RlLmluZm8ucGF0aCkgfHwgbmV3IENvbGxlY3Rpb24odGhpcy5pbmplY3Rvci5nZXQobm9kZS5tZXRhLnNlcnZpY2UpLCBub2RlLmluZm8pO1xuICAgIH0gZWxzZSB7XG4gICAgICBkYXRhID0gbmV3IENvbGxlY3Rpb24odGhpcy5pbmplY3Rvci5nZXQobm9kZS5tZXRhLnNlcnZpY2UpLCBub2RlLmluZm8pO1xuICAgIH1cbiAgICBkYXRhLmxlbmd0aCA9IDA7XG4gICAgZm9yIChjb25zdCBvYmplY3Qgb2Ygbm9kZS5kYXRhKSB7XG4gICAgICBkYXRhLnB1c2godGhpcy5wcm9jZXNzT2JqZWN0KG9iamVjdCkpO1xuICAgIH1cbiAgICB0aGlzLmluZm8uc2V0Rm9yQ29sbGVjdGlvbihkYXRhLCBub2RlLmluZm8pO1xuICAgIHRoaXMuY29sbGVjdG9yLnNldChkYXRhKTtcbiAgICByZXR1cm4gZGF0YTtcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2Vzc09iamVjdChub2RlOiBSZXNwb25zZU5vZGUpIHtcbiAgICBpZiAobm9kZS5tZXRhIGluc3RhbmNlb2YgRW50aXR5TWV0YUluZm8pIHtcbiAgICAgIGNvbnN0IG9iamVjdCA9IHRoaXMuY29sbGVjdG9yLmdldChub2RlLmluZm8ucGF0aCkgfHwgbmV3IG5vZGUubWV0YS50eXBlKCk7XG4gICAgICBmb3IgKGNvbnN0IHByb3BlcnR5IGluIG5vZGUubWV0YS5wcm9wZXJ0aWVzKSB7XG4gICAgICAgIGlmIChvYmplY3QuaGFzT3duUHJvcGVydHkocHJvcGVydHkpICYmICFub2RlLmRhdGEuaGFzT3duUHJvcGVydHkocHJvcGVydHkpKSB7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG5vZGUuZGF0YVtwcm9wZXJ0eV0gaW5zdGFuY2VvZiBSZXNwb25zZU5vZGUpIHtcbiAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gbm9kZS5tZXRhLnByb3BlcnRpZXNbcHJvcGVydHldLmlzQ29sbGVjdGlvbiA/XG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NDb2xsZWN0aW9uKG5vZGUuZGF0YVtwcm9wZXJ0eV0pIDpcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc09iamVjdChub2RlLmRhdGFbcHJvcGVydHldKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zdCB0eXBlOiBhbnkgPSBub2RlLm1ldGEucHJvcGVydGllc1twcm9wZXJ0eV0udHlwZTtcbiAgICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgICAgIGNhc2UgTnVtYmVyOlxuICAgICAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gTnVtYmVyKG5vZGUuZGF0YVtwcm9wZXJ0eV0pO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgRGF0ZTpcbiAgICAgICAgICAgICAgb2JqZWN0W3Byb3BlcnR5XSA9IG5ldyBEYXRlKG5vZGUuZGF0YVtwcm9wZXJ0eV0pO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgU3RyaW5nOlxuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgb2JqZWN0W3Byb3BlcnR5XSA9IG5vZGUuZGF0YVtwcm9wZXJ0eV07XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiBub2RlLm1ldGEuc3VicmVzb3VyY2VzKSB7XG4gICAgICAgIGNvbnN0IHNlcnZpY2UgPSB0aGlzLmluamVjdG9yLmdldChub2RlLm1ldGEuc3VicmVzb3VyY2VzW3Byb3BlcnR5XS5tZXRhLnNlcnZpY2UpO1xuICAgICAgICBvYmplY3RbcHJvcGVydHldID0gc2VydmljZS5kb0dldC5iaW5kKHNlcnZpY2UsIG5vZGUuaW5mby5wYXRoICsgbm9kZS5tZXRhLnN1YnJlc291cmNlc1twcm9wZXJ0eV0ucGF0aCk7XG4gICAgICB9XG4gICAgICB0aGlzLmluZm8uc2V0Rm9yRW50aXR5KG9iamVjdCwgbm9kZS5pbmZvKTtcbiAgICAgIHRoaXMuY29sbGVjdG9yLnNldChvYmplY3QpO1xuICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIG5vZGUuZGF0YTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIEh0dHBSZXF1ZXN0LFxuICBIdHRwSGFuZGxlcixcbiAgSHR0cEV2ZW50LFxuICBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXNwb25zZVxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsIERhdGFOb3JtYWxpemVyIH0gZnJvbSAnLi4vcmVzcG9uc2UvZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEFwaVByb2Nlc3NvciB9IGZyb20gJy4vYXBpLnByb2Nlc3Nvcic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBcGlJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZyxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMpIHByaXZhdGUgcmVhZG9ubHkgbm9ybWFsaXplcnM6IERhdGFOb3JtYWxpemVyW10sXG4gICAgcHJpdmF0ZSByZWFkb25seSBwcm9jZXNzb3I6IEFwaVByb2Nlc3NvclxuICApIHtcbiAgICBpZiAoIUFycmF5LmlzQXJyYXkodGhpcy5ub3JtYWxpemVycykpIHtcbiAgICAgIHRoaXMubm9ybWFsaXplcnMgPSBbXTtcbiAgICB9XG4gIH1cblxuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgaWYgKHJlcXVlc3QudXJsLmluZGV4T2YodGhpcy5jb25maWcuYmFzZVVybCkgIT09IDAgfHwgcmVxdWVzdC51cmwgPT09IHRoaXMuY29uZmlnLmF1dGhVcmxcbiAgICAgIHx8IChBcnJheS5pc0FycmF5KHRoaXMuY29uZmlnLmV4Y2x1ZGVkVXJscykgJiYgdGhpcy5jb25maWcuZXhjbHVkZWRVcmxzLmluZGV4T2YocmVxdWVzdC51cmwpICE9PSAtMSkpIHtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgbGV0IHR5cGU6IHN0cmluZztcbiAgICBsZXQgaGVhZGVycyA9IHJlcXVlc3QuaGVhZGVycztcbiAgICBpZiAoaGVhZGVycy5oYXMoJ1gtQW5SZXN0LVR5cGUnKSkge1xuICAgICAgdHlwZSA9IGhlYWRlcnMuZ2V0KCdYLUFuUmVzdC1UeXBlJyk7XG4gICAgICBoZWFkZXJzID0gaGVhZGVycy5kZWxldGUoJ3gtQW5SZXN0LVR5cGUnKTtcbiAgICB9XG4gICAgaWYgKHRoaXMuY29uZmlnLmRlZmF1bHRIZWFkZXJzKSB7XG4gICAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiB0aGlzLmNvbmZpZy5kZWZhdWx0SGVhZGVycykge1xuICAgICAgICBpZiAoIWhlYWRlcnMuaGFzKGhlYWRlci5uYW1lKSB8fCBoZWFkZXIuYXBwZW5kKSB7XG4gICAgICAgICAgaGVhZGVycyA9IGhlYWRlcnNbaGVhZGVyLmFwcGVuZCA/ICdhcHBlbmQnIDogJ3NldCddKGhlYWRlci5uYW1lLCBoZWFkZXIudmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcbiAgICAgIGhlYWRlcnM6IGhlYWRlcnNcbiAgICB9KTtcbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCkucGlwZShcbiAgICAgIG1hcCgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlIGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XG4gICAgICAgICAgY29uc3QgY29udGVudFR5cGUgPSByZXNwb25zZS5oZWFkZXJzLmdldCgnQ29udGVudC1UeXBlJyk7XG4gICAgICAgICAgZm9yIChjb25zdCBub3JtYWxpemVyIG9mIHRoaXMubm9ybWFsaXplcnMpIHtcbiAgICAgICAgICAgIGlmIChub3JtYWxpemVyLnN1cHBvcnRzKGNvbnRlbnRUeXBlKSkge1xuICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuY2xvbmUoeyBib2R5OiB0aGlzLnByb2Nlc3Nvci5wcm9jZXNzKG5vcm1hbGl6ZXIubm9ybWFsaXplKHJlc3BvbnNlLCB0eXBlKSwgcmVxdWVzdC5tZXRob2QpIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICB9KVxuICAgICk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBSZXNwb25zZUNhY2hlIHtcblxuICBhYnN0cmFjdCBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW47XG5cbiAgYWJzdHJhY3QgZ2V0KGlkOiBzdHJpbmcpO1xuXG4gIGFic3RyYWN0IGFkZChpZDogc3RyaW5nLCBkYXRhOiBhbnkpO1xuXG4gIGFic3RyYWN0IHJlbW92ZShpZDogc3RyaW5nKTtcblxuICBhYnN0cmFjdCBjbGVhcigpO1xuXG4gIGFic3RyYWN0IGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpO1xuXG4gIHJlcGxhY2UoZGF0YTogYW55LCBpZDogc3RyaW5nKSB7XG4gICAgdGhpcy5yZW1vdmUoaWQpO1xuICAgIHRoaXMuYWRkKGRhdGEsIGlkKTtcbiAgfVxufVxuXG5leHBvcnQgbGV0IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyA9IG5ldyBJbmplY3Rpb25Ub2tlbjxSZXNwb25zZUNhY2hlW10+KCdhbnJlc3QuY2FjaGVfc2VydmljZXMnKTtcbiIsImltcG9ydCB7IFJlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL3Jlc3BvbnNlLWNhY2hlJztcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE5vUmVzcG9uc2VDYWNoZSBleHRlbmRzIFJlc3BvbnNlQ2FjaGUge1xuICBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGFkZChpZDogc3RyaW5nLCBkYXRhOiBhbnkpIHtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICB9XG5cbiAgZ2V0KGlkOiBzdHJpbmcpIHtcbiAgfVxuXG4gIHJlbW92ZShpZDogc3RyaW5nKSB7XG4gIH1cblxuICBhZGRBbGlhcyhpZDogc3RyaW5nLCBhbGlhczogc3RyaW5nKSB7XG4gIH1cblxuICByZXBsYWNlKGRhdGE6IGFueSwgaWQ6IHN0cmluZyk6IHZvaWQge1xuICB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIEh0dHBSZXF1ZXN0LFxuICBIdHRwSGFuZGxlcixcbiAgSHR0cEV2ZW50LFxuICBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXNwb25zZVxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcsIERhdGFJbmZvU2VydmljZSB9IGZyb20gJy4uL2NvcmUnO1xuaW1wb3J0IHsgQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLCBSZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgTm9SZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9uby1yZXNwb25zZS1jYWNoZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBDYWNoZUludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIHJlYWRvbmx5IGNvbmZpZzogQXBpQ29uZmlnLFxuICAgIHByaXZhdGUgcmVhZG9ubHkgaW5mbzogRGF0YUluZm9TZXJ2aWNlLFxuICAgIEBJbmplY3QoQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTKSBwcml2YXRlIHJlYWRvbmx5IGNhY2hlU2VydmljZXM6IFJlc3BvbnNlQ2FjaGVbXSxcbiAgKSB7fVxuXG4gIGludGVyY2VwdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICBjb25zdCBwYXRoID0gcmVxdWVzdC51cmxXaXRoUGFyYW1zLnNsaWNlKHRoaXMuY29uZmlnLmJhc2VVcmwubGVuZ3RoKTtcbiAgICBpZiAocmVxdWVzdC51cmwuaW5kZXhPZih0aGlzLmNvbmZpZy5iYXNlVXJsKSAhPT0gMCB8fCByZXF1ZXN0LnVybCA9PT0gdGhpcy5jb25maWcuYXV0aFVybFxuICAgICAgfHwgKEFycmF5LmlzQXJyYXkodGhpcy5jb25maWcuZXhjbHVkZWRVcmxzKSAmJiB0aGlzLmNvbmZpZy5leGNsdWRlZFVybHMuaW5kZXhPZihyZXF1ZXN0LnVybCkgIT09IC0xKSkge1xuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbiAgICBjb25zdCBjYWNoZVR5cGUgPSBNZXRhU2VydmljZS5nZXRCeU5hbWUocmVxdWVzdC5oZWFkZXJzLmdldCgnWC1BblJlc3QtVHlwZScpKS5jYWNoZSB8fCB0aGlzLmNvbmZpZy5jYWNoZSB8fCBOb1Jlc3BvbnNlQ2FjaGU7XG4gICAgbGV0IGNhY2hlOiBhbnk7XG4gICAgZm9yIChjb25zdCBjYWNoZVNlcnZpY2Ugb2YgdGhpcy5jYWNoZVNlcnZpY2VzKSB7XG4gICAgICBpZiAoY2FjaGVTZXJ2aWNlIGluc3RhbmNlb2YgY2FjaGVUeXBlKSB7XG4gICAgICAgIGNhY2hlID0gY2FjaGVTZXJ2aWNlO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKHJlcXVlc3QubWV0aG9kICE9PSAnR0VUJykge1xuICAgICAgY2FjaGUucmVtb3ZlKHBhdGgpO1xuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbiAgICBpZiAoY2FjaGUuaGFzKHBhdGgpKSB7XG4gICAgICByZXR1cm4gb2YobmV3IEh0dHBSZXNwb25zZSh7XG4gICAgICAgIGJvZHk6IGNhY2hlLmdldChwYXRoKSxcbiAgICAgICAgc3RhdHVzOiAyMDAsXG4gICAgICAgIHVybDogcmVxdWVzdC51cmxcbiAgICAgIH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpLnBpcGUoXG4gICAgICB0YXAoKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2U8YW55PikgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2UuYm9keSkge1xuICAgICAgICAgIGNvbnN0IGluZm8gPSB0aGlzLmluZm8uZ2V0KHJlc3BvbnNlLmJvZHkpO1xuICAgICAgICAgIGlmIChpbmZvKSB7XG4gICAgICAgICAgICBjYWNoZS5hZGQoaW5mby5wYXRoLCByZXNwb25zZS5ib2R5KTtcbiAgICAgICAgICAgIGlmIChpbmZvICYmIGluZm8uYWxpYXMpIHtcbiAgICAgICAgICAgICAgY2FjaGUuYWRkQWxpYXMoaW5mby5wYXRoLCBpbmZvLmFsaWFzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vcmVzcG9uc2UtY2FjaGUnO1xuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYmplY3RDb2xsZWN0b3IgfSBmcm9tICcuLi9jb3JlL29iamVjdC1jb2xsZWN0b3InO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBNZW1vcnlSZXNwb25zZUNhY2hlIGV4dGVuZHMgUmVzcG9uc2VDYWNoZSB7XG5cbiAgcHJpdmF0ZSB0aW1lczogeyBbaW5kZXg6IHN0cmluZ106IG51bWJlciB9ID0ge307XG5cbiAgcHJpdmF0ZSBhbGlhc2VzOiB7IFtpbmRleDogc3RyaW5nXTogc3RyaW5nIH0gPSB7fTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJlYWRvbmx5IGNvbGxlY3RvcjogT2JqZWN0Q29sbGVjdG9yLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgcmVhZG9ubHkgY29uZmlnOiBBcGlDb25maWdcbiAgKSB7XG4gICAgc3VwZXIoKTtcbiAgfVxuXG4gIGFkZChpZDogc3RyaW5nLCBkYXRhOiBhbnkpIHtcbiAgICB0aGlzLnRpbWVzW2lkXSA9IERhdGUubm93KCk7XG4gIH1cblxuICBjbGVhcigpIHtcbiAgICB0aGlzLmNvbGxlY3Rvci5jbGVhcigpO1xuICB9XG5cbiAgZ2V0KGlkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gdGhpcy5jb2xsZWN0b3IuZ2V0KGlkKTtcbiAgfVxuXG4gIHJlbW92ZShpZDogc3RyaW5nKSB7XG4gICAgdGhpcy5jb2xsZWN0b3IucmVtb3ZlKGlkKTtcbiAgfVxuXG4gIGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpIHtcbiAgICB0aGlzLmFsaWFzZXNbYWxpYXNdID0gaWQ7XG4gIH1cblxuICBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IHRpbWUgPSB0aGlzLmFsaWFzZXNbaWRdID8gdGhpcy50aW1lc1t0aGlzLmFsaWFzZXNbaWRdXSA6IHRoaXMudGltZXNbaWRdO1xuICAgIHJldHVybiB0aW1lICYmIChEYXRlLm5vdygpIDw9IHRoaXMuY29uZmlnLmNhY2hlVFRMICsgdGltZSk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuL2RhdGEtbm9ybWFsaXplcic7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuL3Jlc3BvbnNlJztcbmltcG9ydCB7IEVudGl0eUluZm8gfSBmcm9tICcuLi9jb3JlJztcblxuZXhwb3J0IGNsYXNzIFJlZmVyZW5jZURhdGFOb3JtYWxpemVyIGltcGxlbWVudHMgRGF0YU5vcm1hbGl6ZXIge1xuICBub3JtYWxpemUocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+LCB0eXBlPzogc3RyaW5nKTogUmVzcG9uc2VOb2RlIHtcbiAgICBjb25zdCByID0gbmV3IFJlc3BvbnNlTm9kZSgpO1xuICAgIHIuY29sbGVjdGlvbiA9IGZhbHNlO1xuICAgIHIuaW5mbyA9IG5ldyBFbnRpdHlJbmZvKHJlc3BvbnNlLmJvZHkucGF0aCk7XG4gICAgci5tZXRhID0gcmVzcG9uc2UuYm9keS5tZXRhO1xuICAgIHIuZGF0YSA9IHt9O1xuICAgIHJldHVybiByO1xuICB9XG5cbiAgc3VwcG9ydHModHlwZTogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHR5cGUgPT09ICdAcmVmZXJlbmNlJztcbiAgfVxufVxuIiwiaW1wb3J0IHsgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuL2RhdGEtbm9ybWFsaXplcic7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuL3Jlc3BvbnNlJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uSW5mbywgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgTGRKc29uRGF0YU5vcm1hbGl6ZXIgaW1wbGVtZW50cyBEYXRhTm9ybWFsaXplciB7XG4gIG5vcm1hbGl6ZShyZXNwb25zZTogSHR0cFJlc3BvbnNlPGFueT4sIHR5cGU/OiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIHJldHVybiB0aGlzLnByb2Nlc3MocmVzcG9uc2UuYm9keSk7XG4gIH1cblxuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHlwZS5zcGxpdCgnOycpWzBdID09PSAnYXBwbGljYXRpb24vbGQranNvbic7XG4gIH1cblxuICBwcml2YXRlIHByb2Nlc3MoZGF0YSwgbWV0YT86IGFueSwgY29sbGVjdGlvbj86IGJvb2xlYW4pIHtcbiAgICBjb25zdCByID0gbmV3IFJlc3BvbnNlTm9kZSgpO1xuICAgIHIuY29sbGVjdGlvbiA9IGNvbGxlY3Rpb24gfHwgZGF0YVsnQHR5cGUnXSA9PT0gJ2h5ZHJhOkNvbGxlY3Rpb24nO1xuICAgIGNvbnN0IHBhdGggPSBkYXRhWydAaWQnXTtcbiAgICByLm1ldGEgPSBtZXRhIHx8IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShyLmNvbGxlY3Rpb24gPyAvXFwvY29udGV4dHNcXC8oXFx3KykvZy5leGVjKGRhdGFbJ0Bjb250ZXh0J10pWzFdIDogZGF0YVsnQHR5cGUnXSk7XG4gICAgaWYgKHIuY29sbGVjdGlvbikge1xuICAgICAgY29uc3QgaXRlbXMgPSBkYXRhWydoeWRyYTptZW1iZXInXSB8fCBkYXRhO1xuICAgICAgci5pbmZvID0gbmV3IENvbGxlY3Rpb25JbmZvKGRhdGFbJ2h5ZHJhOnZpZXcnXSA/IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnQGlkJ10gOiBwYXRoKTtcbiAgICAgIHIuaW5mby50eXBlID0gci5tZXRhLm5hbWU7XG4gICAgICBpZiAoZGF0YVsnaHlkcmE6dmlldyddKSB7XG4gICAgICAgIHIuaW5mby5maXJzdCA9IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnaHlkcmE6Zmlyc3QnXTtcbiAgICAgICAgci5pbmZvLnByZXZpb3VzID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpwcmV2aW91cyddO1xuICAgICAgICByLmluZm8ubmV4dCA9IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnaHlkcmE6bmV4dCddO1xuICAgICAgICByLmluZm8ubGFzdCA9IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnaHlkcmE6bGFzdCddO1xuICAgICAgICBpZiAoci5pbmZvLnBhdGggPT09IHIuaW5mby5maXJzdCkge1xuICAgICAgICAgIHIuaW5mby5hbGlhcyA9IGRhdGFbJ0BpZCddO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByLmluZm8udG90YWwgPSBkYXRhWydoeWRyYTp0b3RhbEl0ZW1zJ10gfHwgaXRlbXMubGVuZ3RoO1xuICAgICAgY29uc3QgcmVnZXhQYWdlID0gL3BhZ2U9KFxcZCspL2c7XG4gICAgICBsZXQgbWF0Y2hlcztcbiAgICAgIG1hdGNoZXMgPSByZWdleFBhZ2UuZXhlYyhyLmluZm8ucGF0aCk7XG4gICAgICByLmluZm8ucGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgbWF0Y2hlcyA9IHJlZ2V4UGFnZS5leGVjKHIuaW5mby5sYXN0KTtcbiAgICAgIHIuaW5mby5sYXN0UGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgci5kYXRhID0gW107XG4gICAgICBmb3IgKGNvbnN0IG9iamVjdCBvZiBpdGVtcykge1xuICAgICAgICByLmRhdGEucHVzaCh0aGlzLnByb2Nlc3Mob2JqZWN0KSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHIuaW5mbyA9IG5ldyBFbnRpdHlJbmZvKHBhdGgpO1xuICAgICAgci5kYXRhID0ge307XG4gICAgICBpZiAoci5tZXRhID09PSB1bmRlZmluZWQgJiYgQXJyYXkuaXNBcnJheShkYXRhKSkge1xuICAgICAgICByLmRhdGEgPSBkYXRhO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuICAgICAgICAgIGlmIChrZXkuaW5kZXhPZignQCcpID09PSAwIHx8IGtleS5pbmRleE9mKCdoeWRyYTonKSA9PT0gMCkge1xuICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnN0IHByb3BlcnR5TWV0YSA9IHIubWV0YS5wcm9wZXJ0aWVzW2tleV07XG4gICAgICAgICAgci5kYXRhW2tleV0gPSAoQXJyYXkuaXNBcnJheShkYXRhW2tleV0pIHx8IChkYXRhW2tleV0gIT09IG51bGwgJiYgcHJvcGVydHlNZXRhICYmIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSkpKSA/XG4gICAgICAgICAgICB0aGlzLnByb2Nlc3MoZGF0YVtrZXldLCBNZXRhU2VydmljZS5nZXQocHJvcGVydHlNZXRhLnR5cGUpLCBwcm9wZXJ0eU1ldGEuaXNDb2xsZWN0aW9uKSA6IGRhdGFba2V5XTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcjtcbiAgfVxufVxuIiwiaW1wb3J0IHsgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuL2RhdGEtbm9ybWFsaXplcic7XG5pbXBvcnQgeyBIdHRwSGVhZGVycywgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi9yZXNwb25zZSc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbkluZm8sIEVudGl0eUluZm8gfSBmcm9tICcuLi9jb3JlJztcblxuZXhwb3J0IGNsYXNzIEpzb25EYXRhTm9ybWFsaXplciBpbXBsZW1lbnRzIERhdGFOb3JtYWxpemVyIHtcbiAgbm9ybWFsaXplKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2U8YW55PiwgdHlwZTogc3RyaW5nKTogUmVzcG9uc2VOb2RlIHtcbiAgICByZXR1cm4gdGhpcy5wcm9jZXNzKHJlc3BvbnNlLmJvZHksIE1ldGFTZXJ2aWNlLmdldEJ5TmFtZSh0eXBlKSwgQXJyYXkuaXNBcnJheShyZXNwb25zZS5ib2R5KSwgcmVzcG9uc2UuaGVhZGVycyk7XG4gIH1cblxuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHlwZS5zcGxpdCgnOycpWzBdID09PSAnYXBwbGljYXRpb24vanNvbic7XG4gIH1cblxuICBwcml2YXRlIHByb2Nlc3MoZGF0YSwgbWV0YTogYW55LCBjb2xsZWN0aW9uOiBib29sZWFuLCBoZWFkZXJzPzogSHR0cEhlYWRlcnMpIHtcbiAgICBjb25zdCByID0gbmV3IFJlc3BvbnNlTm9kZSgpO1xuICAgIHIuY29sbGVjdGlvbiA9IGNvbGxlY3Rpb247XG4gICAgY29uc3QgY29sbGVjdGlvblBhdGggPSBNZXRhU2VydmljZS5nZXQobWV0YS5zZXJ2aWNlKSA/IE1ldGFTZXJ2aWNlLmdldChtZXRhLnNlcnZpY2UpLnBhdGggOiB1bmRlZmluZWQ7XG4gICAgY29uc3QgcGF0aCA9IGNvbGxlY3Rpb25QYXRoID8gKGNvbGxlY3Rpb24gPyBjb2xsZWN0aW9uUGF0aCA6IGNvbGxlY3Rpb25QYXRoICsgJy8nICsgZGF0YVttZXRhLmlkXSkgOiB1bmRlZmluZWQ7XG4gICAgci5tZXRhID0gbWV0YTtcbiAgICBpZiAoci5jb2xsZWN0aW9uKSB7XG4gICAgICByLmluZm8gPSBuZXcgQ29sbGVjdGlvbkluZm8oaGVhZGVycy5nZXQoJ1gtQ3VycmVudC1QYWdlJykgfHwgcGF0aCk7XG4gICAgICByLmluZm8udHlwZSA9IHIubWV0YS5uYW1lO1xuICAgICAgci5pbmZvLmZpcnN0ID0gaGVhZGVycy5nZXQoJ1gtRmlyc3QtUGFnZScpIHx8IHVuZGVmaW5lZDtcbiAgICAgIHIuaW5mby5wcmV2aW91cyA9IGhlYWRlcnMuZ2V0KCdYLVByZXYtUGFnZScpIHx8IHVuZGVmaW5lZDtcbiAgICAgIHIuaW5mby5uZXh0ID0gaGVhZGVycy5nZXQoJ1gtTmV4dC1QYWdlJykgfHwgdW5kZWZpbmVkO1xuICAgICAgci5pbmZvLmxhc3QgPSBoZWFkZXJzLmdldCgnWC1MYXN0LVBhZ2UnKSB8fCB1bmRlZmluZWQ7XG4gICAgICBpZiAoci5pbmZvLnBhdGggPT09IHIuaW5mby5maXJzdCkge1xuICAgICAgICByLmluZm8uYWxpYXMgPSBwYXRoO1xuICAgICAgfVxuICAgICAgci5pbmZvLnRvdGFsID0gaGVhZGVycy5nZXQoJ1gtVG90YWwnKSB8fCBkYXRhLmxlbmd0aDtcbiAgICAgIGNvbnN0IHJlZ2V4UGFnZSA9IC9wYWdlPShcXGQrKS9nO1xuICAgICAgbGV0IG1hdGNoZXM7XG4gICAgICBtYXRjaGVzID0gcmVnZXhQYWdlLmV4ZWMoci5pbmZvLnBhdGgpO1xuICAgICAgci5pbmZvLnBhZ2UgPSBtYXRjaGVzID8gTnVtYmVyKG1hdGNoZXNbMV0pIDogMTtcbiAgICAgIG1hdGNoZXMgPSByZWdleFBhZ2UuZXhlYyhyLmluZm8ubGFzdCk7XG4gICAgICByLmluZm8ubGFzdFBhZ2UgPSBtYXRjaGVzID8gTnVtYmVyKG1hdGNoZXNbMV0pIDogMTtcbiAgICAgIHIuZGF0YSA9IFtdO1xuICAgICAgZm9yIChjb25zdCBvYmplY3Qgb2YgZGF0YSkge1xuICAgICAgICByLmRhdGEucHVzaCh0aGlzLnByb2Nlc3Mob2JqZWN0LCByLm1ldGEsIGZhbHNlKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHIuaW5mbyA9IG5ldyBFbnRpdHlJbmZvKHBhdGgpO1xuICAgICAgci5kYXRhID0ge307XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgIGNvbnN0IHByb3BlcnR5TWV0YSA9IHIubWV0YS5wcm9wZXJ0aWVzW2tleV07XG4gICAgICAgIHIuZGF0YVtrZXldID0gKEFycmF5LmlzQXJyYXkoZGF0YVtrZXldKSB8fCAoZGF0YVtrZXldICE9PSBudWxsICYmIHByb3BlcnR5TWV0YSAmJiBNZXRhU2VydmljZS5nZXQocHJvcGVydHlNZXRhLnR5cGUpKSkgP1xuICAgICAgICAgIHRoaXMucHJvY2VzcyhkYXRhW2tleV0sIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSksIHByb3BlcnR5TWV0YS5pc0NvbGxlY3Rpb24pIDogZGF0YVtrZXldO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcjtcbiAgfVxufVxuIiwiaW1wb3J0IHsgUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vcmVzcG9uc2UtY2FjaGUnO1xuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYmplY3RDb2xsZWN0b3IgfSBmcm9tICcuLi9jb3JlL29iamVjdC1jb2xsZWN0b3InO1xuaW1wb3J0IHsgQXBpUHJvY2Vzc29yIH0gZnJvbSAnLi4vY29yZS9hcGkucHJvY2Vzc29yJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi4vY29yZS9jb25maWcnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbkluZm8sIERhdGFJbmZvU2VydmljZSwgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4uL3Jlc3BvbnNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvY2FsU3RvcmFnZVJlc3BvbnNlQ2FjaGUgZXh0ZW5kcyBSZXNwb25zZUNhY2hlIHtcblxuICBwcml2YXRlIHRva2VuUHJlZml4ID0gJ2FucmVzdDpjYWNoZTonO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgY29sbGVjdG9yOiBPYmplY3RDb2xsZWN0b3IsXG4gICAgcHJpdmF0ZSBwcm9jZXNzb3I6IEFwaVByb2Nlc3NvcixcbiAgICBwcml2YXRlIGluZm86IERhdGFJbmZvU2VydmljZSxcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJpdmF0ZSBjb25maWc6IEFwaUNvbmZpZ1xuICApIHtcbiAgICBzdXBlcigpO1xuICAgIGNvbnN0IHRpbWVzdGFtcFByZWZpeCA9IHRoaXMudG9rZW5QcmVmaXggKyAndGltZXN0YW1wOic7XG4gICAgT2JqZWN0LmtleXMobG9jYWxTdG9yYWdlKS5maWx0ZXIoKGUpID0+IGUuaW5kZXhPZih0aW1lc3RhbXBQcmVmaXgpID09PSAwKS5mb3JFYWNoKChrKSA9PiB7XG4gICAgICBjb25zdCBpZCA9IGsuc3Vic3RyaW5nKHRpbWVzdGFtcFByZWZpeC5sZW5ndGgpO1xuICAgICAgY29uc3QgdGltZXN0YW1wID0gdGhpcy5nZXRUaW1lc3RhbXAoaWQpO1xuICAgICAgaWYgKCF0aW1lc3RhbXAgfHwgdGltZXN0YW1wICsgdGhpcy5jb25maWcuY2FjaGVUVEwgPCBEYXRlLm5vdygpKSB7XG4gICAgICAgIHRoaXMucmVtb3ZlKGlkKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGdldChpZDogc3RyaW5nKSB7XG4gICAgaWQgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2FsaWFzOicgKyBpZCkgfHwgaWQ7XG4gICAgaWYgKHRoaXMuaGFzKGlkKSkge1xuICAgICAgcmV0dXJuIHRoaXMucHJvY2Vzc29yLnByb2Nlc3ModGhpcy5nZXROb2RlKGlkKSwgJ0dFVCcpO1xuICAgIH1cbiAgfVxuXG4gIGFkZChpZDogc3RyaW5nLCBkYXRhOiBhbnkpIHtcbiAgICB0aGlzLmRvQWRkKGlkLCBkYXRhLCB0cnVlKTtcbiAgfVxuXG4gIGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpIHtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2FsaWFzOicgKyBhbGlhcywgaWQpO1xuICB9XG5cbiAgaGFzKGlkOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBpZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGlkKSB8fCBpZDtcbiAgICBjb25zdCBjb21wbGV0ZSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb21wbGV0ZTonICsgaWQpKTtcbiAgICBjb25zdCB0aW1lc3RhbXAgPSB0aGlzLmdldFRpbWVzdGFtcChpZCk7XG4gICAgcmV0dXJuICEhdGltZXN0YW1wICYmIGNvbXBsZXRlICYmIHRpbWVzdGFtcCArIHRoaXMuY29uZmlnLmNhY2hlVFRMID49IERhdGUubm93KCk7XG4gIH1cblxuICByZW1vdmUoaWQ6IHN0cmluZykge1xuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICd0eXBlOicgKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb2xsZWN0aW9uOicgKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb21wbGV0ZTonICsgaWQpO1xuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAncmVmZXJlbmNlczonICsgaWQpO1xuICAgIHRoaXMucmVtb3ZlVGltZXN0YW1wKGlkKTtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICAgIE9iamVjdC5rZXlzKGxvY2FsU3RvcmFnZSkuZmlsdGVyKChlKSA9PiBlLmluZGV4T2YodGhpcy50b2tlblByZWZpeCkgPT09IDApLmZvckVhY2goKGspID0+IHtcbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGspO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkb0FkZChpZDogc3RyaW5nLCBkYXRhOiBhbnksIGNvbXBsZXRlOiBib29sZWFuKSB7XG4gICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQoZGF0YSk7XG4gICAgbGV0IHZhbHVlczogYW55O1xuICAgIGlmIChpbmZvIGluc3RhbmNlb2YgQ29sbGVjdGlvbkluZm8pIHtcbiAgICAgIHZhbHVlcyA9IFtdO1xuICAgICAgZm9yIChjb25zdCBpdGVtIG9mIGRhdGEpIHtcbiAgICAgICAgY29uc3QgaXRlbVBhdGggPSB0aGlzLmluZm8uZ2V0KGl0ZW0pLnBhdGg7XG4gICAgICAgIHZhbHVlcy5wdXNoKGl0ZW1QYXRoKTtcbiAgICAgICAgdGhpcy5kb0FkZChpdGVtUGF0aCwgaXRlbSwgZmFsc2UpO1xuICAgICAgfVxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb2xsZWN0aW9uOicgKyBpZCwgSlNPTi5zdHJpbmdpZnkoaW5mbykpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zdCByZWZlcmVuY2VzID0ge307XG4gICAgICB2YWx1ZXMgPSB7fTtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGRhdGEpIHtcbiAgICAgICAgbGV0IGl0ZW1JbmZvOiBhbnk7XG4gICAgICAgIGlmICh0eXBlb2YgZGF0YVtrZXldID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgIGl0ZW1JbmZvID0gdGhpcy5pbmZvLmdldChkYXRhW2tleV0pO1xuICAgICAgICB9XG4gICAgICAgIGlmIChpdGVtSW5mbykge1xuICAgICAgICAgIHRoaXMuZG9BZGQoaXRlbUluZm8ucGF0aCwgZGF0YVtrZXldLCBmYWxzZSk7XG4gICAgICAgICAgcmVmZXJlbmNlc1trZXldID0gaXRlbUluZm8ucGF0aDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWx1ZXNba2V5XSA9IGRhdGFba2V5XTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICd0eXBlOicgKyBpZCwgTWV0YVNlcnZpY2UuZ2V0KGRhdGEuY29uc3RydWN0b3IpLm5hbWUpO1xuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCwgSlNPTi5zdHJpbmdpZnkocmVmZXJlbmNlcykpO1xuICAgIH1cbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCwgSlNPTi5zdHJpbmdpZnkoY29tcGxldGUpKTtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgaWQsIEpTT04uc3RyaW5naWZ5KHZhbHVlcykpO1xuICAgIHRoaXMuc2V0VGltZXN0YW1wKGlkKTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0Tm9kZShpZDogc3RyaW5nKTogUmVzcG9uc2VOb2RlIHtcbiAgICBjb25zdCBkYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgaWQpKTtcbiAgICBjb25zdCBjb2xsZWN0aW9uSW5mbyA9IHRoaXMuZ2V0Q29sbGVjdGlvbkluZm8oaWQpO1xuICAgIGNvbnN0IHJlc3BvbnNlID0gbmV3IFJlc3BvbnNlTm9kZSgpO1xuICAgIGlmIChjb2xsZWN0aW9uSW5mbykge1xuICAgICAgcmVzcG9uc2UuY29sbGVjdGlvbiA9IHRydWU7XG4gICAgICByZXNwb25zZS5pbmZvID0gY29sbGVjdGlvbkluZm87XG4gICAgICByZXNwb25zZS5tZXRhID0gTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKGNvbGxlY3Rpb25JbmZvLnR5cGUpO1xuICAgICAgcmVzcG9uc2UuZGF0YSA9IFtdO1xuICAgICAgZm9yIChjb25zdCBpdGVtSWQgb2YgZGF0YSkge1xuICAgICAgICByZXNwb25zZS5kYXRhLnB1c2godGhpcy5nZXROb2RlKGl0ZW1JZCkpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByZXNwb25zZS5jb2xsZWN0aW9uID0gZmFsc2U7XG4gICAgICByZXNwb25zZS5pbmZvID0gbmV3IEVudGl0eUluZm8oaWQpO1xuICAgICAgcmVzcG9uc2UubWV0YSA9IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkKSk7XG4gICAgICByZXNwb25zZS5kYXRhID0gZGF0YTtcbiAgICAgIGNvbnN0IHJlZmVyZW5jZXMgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAncmVmZXJlbmNlczonICsgaWQpKTtcbiAgICAgIGlmIChyZWZlcmVuY2VzKSB7XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIHJlZmVyZW5jZXMpIHtcbiAgICAgICAgICByZXNwb25zZS5kYXRhW2tleV0gPSB0aGlzLmdldE5vZGUocmVmZXJlbmNlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcmVzcG9uc2U7XG4gIH1cblxuICBwcml2YXRlIGdldENvbGxlY3Rpb25JbmZvKGlkOiBzdHJpbmcpIHtcbiAgICBjb25zdCBkYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkKSk7XG4gICAgaWYgKGRhdGEpIHtcbiAgICAgIGNvbnN0IGluZm8gPSBuZXcgQ29sbGVjdGlvbkluZm8oaWQpO1xuICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuICAgICAgICBpZiAoa2V5ICE9PSAnX3BhdGgnKSB7XG4gICAgICAgICAgaW5mb1trZXldID0gZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gaW5mbztcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHNldFRpbWVzdGFtcChpZDogc3RyaW5nKSB7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JyArIGlkLCBTdHJpbmcoRGF0ZS5ub3coKSkpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRUaW1lc3RhbXAoaWQ6IHN0cmluZykge1xuICAgIHJldHVybiBOdW1iZXIobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JyArIGlkKSk7XG4gIH1cblxuICBwcml2YXRlIHJlbW92ZVRpbWVzdGFtcChpZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAndGltZXN0YW1wOicgKyBpZCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGggfSBmcm9tICcuL2F1dGgnO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQXV0aFNlcnZpY2UgaW1wbGVtZW50cyBBdXRoIHtcbiAgYWJzdHJhY3QgZ2V0SW5mbygpOiBhbnk7XG5cbiAgYWJzdHJhY3QgZ2V0VG9rZW4oKTogc3RyaW5nO1xuXG4gIGFic3RyYWN0IGlzU2lnbmVkSW4oKTogYm9vbGVhbjtcblxuICBhYnN0cmFjdCByZW1vdmUoKTtcblxuICBhYnN0cmFjdCBzZXRUb2tlbih0b2tlbjogc3RyaW5nKTtcblxuICBhYnN0cmFjdCBzaWduSW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PjtcblxufVxuIiwiaW1wb3J0IHsgQXV0aFRva2VuIH0gZnJvbSAnLi9hdXRoLXRva2VuJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEF1dGhUb2tlblByb3ZpZGVyU2VydmljZSBpbXBsZW1lbnRzIEF1dGhUb2tlbntcbiAgYWJzdHJhY3QgZ2V0KCk6IHN0cmluZztcbiAgYWJzdHJhY3Qgc2V0KHRva2VuOiBzdHJpbmcpO1xuICBhYnN0cmFjdCByZW1vdmUoKTtcbiAgYWJzdHJhY3QgaXNTZXQoKTogYm9vbGVhbjtcbn1cbiIsImltcG9ydCB7IEF1dGhUb2tlbiB9IGZyb20gJy4vYXV0aC10b2tlbic7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UgaW1wbGVtZW50cyBBdXRoVG9rZW4ge1xuICBwcml2YXRlIHRva2VuTmFtZSA9ICdhbnJlc3Q6YXV0aF90b2tlbic7XG5cbiAgZ2V0KCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5OYW1lKTtcbiAgfVxuXG4gIHNldCh0b2tlbjogc3RyaW5nKSB7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlbk5hbWUsIHRva2VuKTtcbiAgfVxuXG4gIHJlbW92ZSgpIHtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuTmFtZSk7XG4gIH1cblxuICBpc1NldCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlbk5hbWUpICE9IG51bGw7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvclxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi4vY29yZS9jb25maWcnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXV0aEludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgYXV0aDogQXV0aFNlcnZpY2UsIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgY29uZmlnOiBBcGlDb25maWcpIHt9XG5cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGlmICghdGhpcy5jb25maWcuYXV0aFVybCB8fCByZXF1ZXN0LnVybC5pbmRleE9mKHRoaXMuY29uZmlnLmJhc2VVcmwpICE9PSAwIHx8IHJlcXVlc3QudXJsID09PSB0aGlzLmNvbmZpZy5hdXRoVXJsKSB7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIGlmICh0aGlzLmF1dGguaXNTaWduZWRJbigpKSB7XG4gICAgICByZXF1ZXN0ID0gcmVxdWVzdC5jbG9uZSh7XG4gICAgICAgIHNldEhlYWRlcnM6IHtcbiAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7dGhpcy5hdXRoLmdldFRva2VuKCl9YFxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICB9XG59XG4iLCJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgKiBhcyBqd3RfZGVjb2RlIGZyb20gJ2p3dC1kZWNvZGUnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBBdXRoIH0gZnJvbSAnLi9hdXRoJztcbmltcG9ydCB7IEF1dGhUb2tlblByb3ZpZGVyU2VydmljZSB9IGZyb20gJy4vYXV0aC10b2tlbi1wcm92aWRlci5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEp3dEF1dGhTZXJ2aWNlIGltcGxlbWVudHMgQXV0aCB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByaXZhdGUgdG9rZW46IEF1dGhUb2tlblByb3ZpZGVyU2VydmljZSxcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge31cblxuICBnZXRUb2tlbigpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLnRva2VuLmdldCgpO1xuICB9XG5cbiAgc2V0VG9rZW4odG9rZW46IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMudG9rZW4uc2V0KHRva2VuKTtcbiAgfVxuXG4gIHJlbW92ZSgpOiB2b2lkIHtcbiAgICB0aGlzLnRva2VuLnJlbW92ZSgpO1xuICB9XG5cbiAgaXNTaWduZWRJbigpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy50b2tlbi5pc1NldCgpO1xuICB9XG5cbiAgZ2V0SW5mbygpOiBhbnkge1xuICAgIHJldHVybiBqd3RfZGVjb2RlLmNhbGwodGhpcy5nZXRUb2tlbigpKTtcbiAgfVxuXG4gIHNpZ25Jbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWcuYXV0aFVybCwge1xuICAgICAgbG9naW46IHVzZXJuYW1lLFxuICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkXG4gICAgfSkucGlwZShcbiAgICAgIHRhcCgoZGF0YTogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuc2V0VG9rZW4oZGF0YS50b2tlbik7XG4gICAgICB9KVxuICAgICk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGludGVyZmFjZSBSZXNvdXJjZUluZm8ge1xuICBwYXRoPzogc3RyaW5nO1xuICBuYW1lPzogc3RyaW5nO1xuICBjYWNoZT86IEZ1bmN0aW9uO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gUmVzb3VyY2UgKGluZm86IFJlc291cmNlSW5mbyA9IHt9KSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IEZ1bmN0aW9uKSA9PiB7XG4gICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldCk7XG4gICAgbWV0YS5uYW1lID0gaW5mby5uYW1lIHx8IHRhcmdldC5uYW1lO1xuICAgIG1ldGEuY2FjaGUgPSBpbmZvLmNhY2hlO1xuICB9O1xufVxuIiwiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGludGVyZmFjZSBQcm9wZXJ0eUluZm8ge1xuICBjb2xsZWN0aW9uPzogYm9vbGVhbjtcbiAgZXhjbHVkZVdoZW5TYXZpbmc/OiBib29sZWFuO1xuICB0eXBlPzogYW55O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gUHJvcGVydHkgKGluZm86IFByb3BlcnR5SW5mbyA9IHt9KSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcpID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JQcm9wZXJ0eSh0YXJnZXQsIGtleSwgaW5mby50eXBlKTtcbiAgICBtZXRhLmlzQ29sbGVjdGlvbiA9IGluZm8uY29sbGVjdGlvbiB8fCBmYWxzZTtcbiAgICBtZXRhLmV4Y2x1ZGVXaGVuU2F2aW5nID0gaW5mby5leGNsdWRlV2hlblNhdmluZyB8fCBmYWxzZTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgKiBhcyBwbHVyYWxpemUgZnJvbSAncGx1cmFsaXplJztcblxuZXhwb3J0IGZ1bmN0aW9uIEh0dHBTZXJ2aWNlKGVudGl0eTogRnVuY3Rpb24sIHBhdGg/OiBzdHJpbmcpIHtcblxuICByZXR1cm4gKHRhcmdldDogRnVuY3Rpb24pID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JIdHRwU2VydmljZSh0YXJnZXQsIGVudGl0eSk7XG4gICAgbWV0YS5wYXRoID0gcGF0aCB8fCAnLycgKyBwbHVyYWxpemUucGx1cmFsKGVudGl0eS5uYW1lKS5yZXBsYWNlKC9cXC4/KFtBLVpdKS9nLCAnLSQxJykucmVwbGFjZSgvXi0vLCAnJykudG9Mb3dlckNhc2UoKTtcbiAgICBtZXRhLmVudGl0eU1ldGEuc2VydmljZSA9IHRhcmdldDtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBIZWFkZXIoaGVhZGVyTmFtZTogc3RyaW5nLCBhcHBlbmQ/OiBib29sZWFuKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkgPSB1bmRlZmluZWQpID0+IHtcbiAgICBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0YXJnZXQuY29uc3RydWN0b3IpLmhlYWRlcnMucHVzaCh7XG4gICAgICBuYW1lOiBoZWFkZXJOYW1lLFxuICAgICAgdmFsdWU6IHZhbHVlID8gdmFsdWUudmFsdWUgOiBmdW5jdGlvbiAoKSB7IHJldHVybiAgdGhpc1trZXldOyB9LFxuICAgICAgYXBwZW5kOiBhcHBlbmQgfHwgZmFsc2UsXG4gICAgICBkeW5hbWljOiB0cnVlXG4gICAgfSk7XG4gIH07XG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gSGVhZGVycyAoaGVhZGVyczogeyBuYW1lOiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcsIGFwcGVuZD86IGJvb2xlYW59W10pIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55KSA9PiB7XG4gICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldCk7XG4gICAgZm9yIChjb25zdCBoZWFkZXIgb2YgaGVhZGVycykge1xuICAgICAgbWV0YS5oZWFkZXJzLnB1c2goe1xuICAgICAgICBuYW1lOiBoZWFkZXIubmFtZSxcbiAgICAgICAgdmFsdWU6ICgpID0+IGhlYWRlci52YWx1ZSxcbiAgICAgICAgYXBwZW5kOiBoZWFkZXIuYXBwZW5kIHx8IGZhbHNlLFxuICAgICAgICBkeW5hbWljOiBmYWxzZVxuICAgICAgfSk7XG4gICAgfVxuICB9O1xufVxuIiwiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGZ1bmN0aW9uIEJvZHkoKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBhbnkgPSB1bmRlZmluZWQsIHZhbHVlOiBhbnkgPSB1bmRlZmluZWQpID0+IHtcbiAgICBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0YXJnZXQuY29uc3RydWN0b3IpLmJvZHkgPSAob2JqZWN0OiBhbnkpID0+IHtcbiAgICAgIHJldHVybiAodmFsdWUgPyB2YWx1ZS52YWx1ZSA6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXNba2V5XTsgfSkuY2FsbChvYmplY3QpO1xuICAgIH07XG4gIH07XG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gU3VicmVzb3VyY2UgKHR5cGU6IEZ1bmN0aW9uLCBwYXRoPzogc3RyaW5nKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpID0+IHtcbiAgICBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0YXJnZXQuY29uc3RydWN0b3IpLnN1YnJlc291cmNlc1trZXldID0ge1xuICAgICAgbWV0YTogTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodHlwZSksXG4gICAgICBwYXRoOiBwYXRoIHx8ICcvJyArIGtleS5yZXBsYWNlKC9cXC4/KFtBLVpdKS9nLCAnLSQxJykucmVwbGFjZSgvXi0vLCAnJykudG9Mb3dlckNhc2UoKVxuICAgIH07XG4gIH07XG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gSWQgKCkge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nKSA9PiB7XG4gICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3Rvcik7XG4gICAgbWV0YS5pZCA9IGtleTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgY29tYmluZUxhdGVzdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgZGVib3VuY2VUaW1lLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCwgbWFwLCBtZXJnZU1hcCwgc2hhcmUsIHNoYXJlUmVwbGF5LCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBcGlTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi4vY29yZS9jb2xsZWN0aW9uJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIExvYWRlciB7XG5cbiAgcHJpdmF0ZSByZWFkb25seSBmaWVsZHMgPSB7fTtcbiAgcHJpdmF0ZSByZWFkb25seSBvYnNlcnZhYmxlOiBPYnNlcnZhYmxlPGFueT47XG4gIHByaXZhdGUgbG9hZGluZyA9IGZhbHNlO1xuICBwcm90ZWN0ZWQgbGFzdERhdGE6IENvbGxlY3Rpb248YW55PjtcblxuICBhYnN0cmFjdCBnZXRBdmFpbGFibGVGaWVsZHMoKTogc3RyaW5nW107XG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIHNlcnZpY2U6IEFwaVNlcnZpY2UsIHdhaXRGb3IgPSAwLCByZXBsYXkgPSAwKSB7XG4gICAgdGhpcy5vYnNlcnZhYmxlID0gY29tYmluZUxhdGVzdCh0aGlzLmdldFN1YmplY3RzKCkpXG4gICAgICAucGlwZShcbiAgICAgICAgcmVwbGF5ID8gc2hhcmVSZXBsYXkocmVwbGF5KSA6IHNoYXJlKCksXG4gICAgICAgIGRlYm91bmNlVGltZSh3YWl0Rm9yKSxcbiAgICAgICAgZGlzdGluY3RVbnRpbENoYW5nZWQoKGQxOiBhbnlbXSwgZDI6IGFueVtdKSA9PiB0aGlzLmNoZWNrRGlzdGluY3QoZDEsIGQyKSksXG4gICAgICAgIG1hcCgodmFsdWVzOiBhbnlbXSkgPT4gdGhpcy5tYXBQYXJhbXModmFsdWVzKSksXG4gICAgICAgIG1lcmdlTWFwKChkYXRhKSA9PiB7XG4gICAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICByZXR1cm4gdGhpcy5nZXRTZXJ2aWNlT2JzZXJ2YWJsZShkYXRhKS5waXBlKFxuICAgICAgICAgICAgdGFwKCh2OiBDb2xsZWN0aW9uPGFueT4pID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5sYXN0RGF0YSA9IHY7XG4gICAgICAgICAgICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICApO1xuICAgICAgICB9KSxcbiAgICAgICk7XG4gIH1cblxuICBmaWVsZChuYW1lKTogQmVoYXZpb3JTdWJqZWN0PGFueT4ge1xuICAgIHJldHVybiB0aGlzLmZpZWxkc1tuYW1lXTtcbiAgfVxuXG4gIGdldCBkYXRhKCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMub2JzZXJ2YWJsZTtcbiAgfVxuXG4gIGdldCBpc0xvYWRpbmcoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMubG9hZGluZztcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTZXJ2aWNlT2JzZXJ2YWJsZShbZGF0YV06IGFueVtdKSB7XG4gICAgcmV0dXJuIHRoaXMuc2VydmljZS5nZXRMaXN0KGRhdGEpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGNoZWNrRGlzdGluY3QoZDE6IGFueVtdLCBkMjogYW55W10pIHtcbiAgICBjb25zdCBvZmZzZXQgPSBkMS5sZW5ndGggLSB0aGlzLmdldEF2YWlsYWJsZUZpZWxkcygpLmxlbmd0aDtcbiAgICBmb3IgKGxldCBpID0gZDEubGVuZ3RoOyBpID49IDA7IGktLSkge1xuICAgICAgaWYgKChpIDwgb2Zmc2V0ICYmIGQyW2ldKSB8fCAoaSA+PSBvZmZzZXQgJiYgZDFbaV0gIT09IGQyW2ldKSkge1xuICAgICAgICBpZiAoaSA+PSBvZmZzZXQpIHtcbiAgICAgICAgICBkMi5maWxsKHVuZGVmaW5lZCwgMCwgb2Zmc2V0KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgcHJvdGVjdGVkIG1hcFBhcmFtcyh2YWx1ZXM6IGFueVtdKSB7XG4gICAgY29uc3QgZmllbGRzID0gdGhpcy5nZXRBdmFpbGFibGVGaWVsZHMoKTtcbiAgICBjb25zdCBvZmZzZXQgPSB2YWx1ZXMubGVuZ3RoIC0gZmllbGRzLmxlbmd0aDtcbiAgICBjb25zdCBkYXRhID0ge307XG4gICAgdmFsdWVzLnNsaWNlKG9mZnNldCkuZm9yRWFjaCgodiwgaSkgPT4ge1xuICAgICAgaWYgKHYgIT09IHVuZGVmaW5lZCAmJiB2ICE9PSBudWxsICYmIHYgIT09ICcnKSB7XG4gICAgICAgIGlmICh0eXBlb2YgdiA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICAgICAgdiA9IHYgPyAndHJ1ZScgOiAnZmFsc2UnO1xuICAgICAgICB9XG4gICAgICAgIGRhdGFbZmllbGRzW2ldXSA9IFN0cmluZyh2KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gdmFsdWVzLnNsaWNlKDAsIG9mZnNldCkuY29uY2F0KFtkYXRhXSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U3ViamVjdHMoKTogQmVoYXZpb3JTdWJqZWN0PGFueT5bXSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QXZhaWxhYmxlRmllbGRzKCkubWFwKChuYW1lKSA9PiB7XG4gICAgICByZXR1cm4gdGhpcy5maWVsZHNbbmFtZV0gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGFueT4odW5kZWZpbmVkKTtcbiAgICB9KTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBMb2FkZXIgfSBmcm9tICcuL2xvYWRlcic7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBJbmZpbml0ZVNjcm9sbExvYWRlciBleHRlbmRzIExvYWRlciB7XG5cbiAgcHJpdmF0ZSBsb2FkTW9yZVN1YmplY3Q7XG5cbiAgbG9hZE1vcmUoKSB7XG4gICAgdGhpcy5sb2FkTW9yZVN1YmplY3QubmV4dCh0cnVlKTtcbiAgfVxuXG4gIGhhc01vcmUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNNb3JlKCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U2VydmljZU9ic2VydmFibGUoW2xvYWRNb3JlLCBkYXRhXTogYW55W10pOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiBsb2FkTW9yZSA/IHRoaXMubGFzdERhdGEubG9hZE1vcmUoKSA6IHN1cGVyLmdldFNlcnZpY2VPYnNlcnZhYmxlKFtkYXRhXSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U3ViamVjdHMoKTogQmVoYXZpb3JTdWJqZWN0PGFueT5bXSB7XG4gICAgdGhpcy5sb2FkTW9yZVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgICBjb25zdCBzdWJqZWN0cyA9IHN1cGVyLmdldFN1YmplY3RzKCk7XG4gICAgc3ViamVjdHMudW5zaGlmdCh0aGlzLmxvYWRNb3JlU3ViamVjdCk7XG4gICAgcmV0dXJuIHN1YmplY3RzO1xuICB9XG59XG4iLCJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIGNvbWJpbmVMYXRlc3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGRlYm91bmNlVGltZSwgZGlzdGluY3RVbnRpbENoYW5nZWQsIG1hcCwgbWVyZ2VNYXAsIHNoYXJlLCBzaGFyZVJlcGxheSwgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXBpU2VydmljZSB9IGZyb20gJy4uL2NvcmUnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4uL2NvcmUvY29sbGVjdGlvbic7XG5pbXBvcnQgeyBMb2FkZXIgfSBmcm9tICcuL2xvYWRlcic7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBQYWdlTG9hZGVyIGV4dGVuZHMgTG9hZGVyIHtcblxuICBwcml2YXRlIHBhZ2VTdWJqZWN0O1xuXG4gIGZpcnN0KCkge1xuICAgIGlmICghdGhpcy5sYXN0RGF0YSB8fCAhdGhpcy5sYXN0RGF0YS5oYXNGaXJzdCgpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLmZpcnN0UGF0aCgpKTtcbiAgfVxuXG4gIHByZXZpb3VzKCkge1xuICAgIGlmICghdGhpcy5sYXN0RGF0YSB8fCAhdGhpcy5sYXN0RGF0YS5oYXNQcmV2aW91cygpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLnByZXZpb3VzUGF0aCgpKTtcbiAgfVxuXG4gIG5leHQoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc05leHQoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnBhZ2VTdWJqZWN0Lm5leHQodGhpcy5sYXN0RGF0YS5uZXh0UGF0aCgpKTtcbiAgfVxuXG4gIGxhc3QoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc0xhc3QoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnBhZ2VTdWJqZWN0Lm5leHQodGhpcy5sYXN0RGF0YS5sYXN0UGF0aCgpKTtcbiAgfVxuXG4gIGhhc0ZpcnN0KCkge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzRmlyc3QoKTtcbiAgfVxuXG4gIGhhc1ByZXZpb3VzKCkge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzUHJldmlvdXMoKTtcbiAgfVxuXG4gIGhhc05leHQoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNOZXh0KCk7XG4gIH1cblxuICBoYXNMYXN0KCkge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzTGFzdCgpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFNlcnZpY2VPYnNlcnZhYmxlKFtwYWdlLCBkYXRhXTogYW55W10pIHtcbiAgICByZXR1cm4gcGFnZSA/IHRoaXMuc2VydmljZS5kb0dldChwYWdlLCBkYXRhKSA6IHN1cGVyLmdldFNlcnZpY2VPYnNlcnZhYmxlKFtkYXRhXSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U3ViamVjdHMoKTogQmVoYXZpb3JTdWJqZWN0PGFueT5bXSB7XG4gICAgdGhpcy5wYWdlU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPih1bmRlZmluZWQpO1xuICAgIGNvbnN0IHN1YmplY3RzID0gc3VwZXIuZ2V0U3ViamVjdHMoKTtcbiAgICBzdWJqZWN0cy51bnNoaWZ0KHRoaXMucGFnZVN1YmplY3QpO1xuICAgIHJldHVybiBzdWJqZWN0cztcbiAgfVxufVxuIiwiaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEhUVFBfSU5URVJDRVBUT1JTLCBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0ICdyZWZsZWN0LW1ldGFkYXRhJztcblxuaW1wb3J0IHtcbiAgQXBpU2VydmljZSxcbiAgRGF0YUluZm9TZXJ2aWNlLFxuICBBblJlc3RDb25maWcsXG4gIEFwaUNvbmZpZyxcbiAgQXBpSW50ZXJjZXB0b3IsXG4gIEFwaVByb2Nlc3NvcixcbiAgT2JqZWN0Q29sbGVjdG9yLFxuICBSZWZlcmVuY2VJbnRlcmNlcHRvclxufSBmcm9tICcuL2NvcmUnO1xuaW1wb3J0IHsgRXZlbnRzU2VydmljZSB9IGZyb20gJy4vZXZlbnRzJztcbmltcG9ydCB7XG4gIEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsXG4gIFJlZmVyZW5jZURhdGFOb3JtYWxpemVyLFxuICBMZEpzb25EYXRhTm9ybWFsaXplcixcbiAgSnNvbkRhdGFOb3JtYWxpemVyXG59IGZyb20gJy4vcmVzcG9uc2UnO1xuaW1wb3J0IHtcbiAgQXV0aEludGVyY2VwdG9yLFxuICBBdXRoU2VydmljZSxcbiAgQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICBKd3RBdXRoU2VydmljZSxcbiAgTG9jYWxTdG9yYWdlQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlXG59IGZyb20gJy4vYXV0aCc7XG5pbXBvcnQge1xuICBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gIENhY2hlSW50ZXJjZXB0b3IsXG4gIExvY2FsU3RvcmFnZVJlc3BvbnNlQ2FjaGUsXG4gIE1lbW9yeVJlc3BvbnNlQ2FjaGUsXG4gIE5vUmVzcG9uc2VDYWNoZSxcbiAgUmVzcG9uc2VDYWNoZVxufSBmcm9tICcuL2NhY2hlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQW5SZXN0TW9kdWxlIHtcbiAgc3RhdGljIGNvbmZpZyhjb25maWc/OiBBcGlDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IEFuUmVzdE1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQW5SZXN0Q29uZmlnLFxuICAgICAgICAgIHVzZVZhbHVlOiBjb25maWcsXG4gICAgICAgIH0sXG4gICAgICAgIEV2ZW50c1NlcnZpY2UsXG4gICAgICAgIERhdGFJbmZvU2VydmljZSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBNZW1vcnlSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IE5vUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICB1c2VDbGFzczogY29uZmlnLmNhY2hlIHx8IE5vUmVzcG9uc2VDYWNoZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcudG9rZW5Qcm92aWRlciB8fCBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEF1dGhTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcuYXV0aFNlcnZpY2UgfHwgSnd0QXV0aFNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBdXRoSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBSZWZlcmVuY2VJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IENhY2hlSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBcGlJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgICAgICAgICB1c2VDbGFzczogUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IExkSnNvbkRhdGFOb3JtYWxpemVyLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfSFRUUF9EQVRBX05PUk1BTElaRVJTLFxuICAgICAgICAgIHVzZUNsYXNzOiBKc29uRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAgT2JqZWN0Q29sbGVjdG9yLFxuICAgICAgICBBcGlQcm9jZXNzb3IsXG4gICAgICAgIEFwaVNlcnZpY2VcbiAgICAgIF1cbiAgICB9O1xuICB9XG4gIHN0YXRpYyBjb25maWdXaXRob3V0Tm9ybWFsaXplcnMoY29uZmlnPzogQXBpQ29uZmlnKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5nTW9kdWxlOiBBblJlc3RNb2R1bGUsXG4gICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFuUmVzdENvbmZpZyxcbiAgICAgICAgICB1c2VWYWx1ZTogY29uZmlnLFxuICAgICAgICB9LFxuICAgICAgICBFdmVudHNTZXJ2aWNlLFxuICAgICAgICBEYXRhSW5mb1NlcnZpY2UsXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IExvY2FsU3RvcmFnZVJlc3BvbnNlQ2FjaGUsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTWVtb3J5UmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBOb1Jlc3BvbnNlQ2FjaGUsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IFJlc3BvbnNlQ2FjaGUsXG4gICAgICAgICAgdXNlQ2xhc3M6IGNvbmZpZy5jYWNoZSB8fCBOb1Jlc3BvbnNlQ2FjaGVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEF1dGhUb2tlblByb3ZpZGVyU2VydmljZSxcbiAgICAgICAgICB1c2VDbGFzczogY29uZmlnLnRva2VuUHJvdmlkZXIgfHwgTG9jYWxTdG9yYWdlQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBdXRoU2VydmljZSxcbiAgICAgICAgICB1c2VDbGFzczogY29uZmlnLmF1dGhTZXJ2aWNlIHx8IEp3dEF1dGhTZXJ2aWNlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICAgICAgICB1c2VDbGFzczogQXV0aEludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICAgICAgICB1c2VDbGFzczogUmVmZXJlbmNlSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBDYWNoZUludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICAgICAgICB1c2VDbGFzczogQXBpSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IFJlZmVyZW5jZURhdGFOb3JtYWxpemVyLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIE9iamVjdENvbGxlY3RvcixcbiAgICAgICAgQXBpUHJvY2Vzc29yLFxuICAgICAgICBBcGlTZXJ2aWNlXG4gICAgICBdXG4gICAgfTtcbiAgfVxufVxuIl0sIm5hbWVzIjpbIkluamVjdGlvblRva2VuIiwiSW5qZWN0YWJsZSIsInRzbGliXzEuX19leHRlbmRzIiwibWFwIiwiSHR0cEhlYWRlcnMiLCJ0c2xpYl8xLl9fdmFsdWVzIiwiT3B0aW9uYWwiLCJJbmplY3QiLCJodHRwIiwiSHR0cFBhcmFtcyIsInRhcCIsIkh0dHBDbGllbnQiLCJvZiIsIkh0dHBSZXNwb25zZSIsIkluamVjdG9yIiwiand0X2RlY29kZS5jYWxsIiwicGx1cmFsaXplLnBsdXJhbCIsImNvbWJpbmVMYXRlc3QiLCJzaGFyZVJlcGxheSIsInNoYXJlIiwiZGVib3VuY2VUaW1lIiwiZGlzdGluY3RVbnRpbENoYW5nZWQiLCJtZXJnZU1hcCIsIkJlaGF2aW9yU3ViamVjdCIsIkhUVFBfSU5URVJDRVBUT1JTIiwiTmdNb2R1bGUiLCJIdHRwQ2xpZW50TW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEseUJBY2EsWUFBWSxHQUFHLElBQUlBLG1CQUFjLENBQVksbUJBQW1CLENBQUM7O0lDZDlFOzs7Ozs7Ozs7Ozs7OztJQWNBO0lBRUEsSUFBSSxhQUFhLEdBQUcsTUFBTSxDQUFDLGNBQWM7U0FDcEMsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLFlBQVksS0FBSyxJQUFJLFVBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDNUUsVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQztZQUFFLElBQUksQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7QUFFL0UsdUJBQTBCLENBQUMsRUFBRSxDQUFDO1FBQzFCLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEIsZ0JBQWdCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEVBQUU7UUFDdkMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLEtBQUssSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6RixDQUFDO0FBRUQsc0JBMEV5QixDQUFDO1FBQ3RCLElBQUksQ0FBQyxHQUFHLE9BQU8sTUFBTSxLQUFLLFVBQVUsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLE9BQU87WUFDSCxJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNO29CQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztnQkFDbkMsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDM0M7U0FDSixDQUFDO0lBQ04sQ0FBQztBQUVELG9CQUF1QixDQUFDLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsR0FBRyxPQUFPLE1BQU0sS0FBSyxVQUFVLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsQ0FBQztZQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ2pDLElBQUk7WUFDQSxPQUFPLENBQUMsQ0FBQyxLQUFLLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxJQUFJO2dCQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzlFO1FBQ0QsT0FBTyxLQUFLLEVBQUU7WUFBRSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUM7U0FBRTtnQkFDL0I7WUFDSixJQUFJO2dCQUNBLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDcEQ7b0JBQ087Z0JBQUUsSUFBSSxDQUFDO29CQUFFLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUFFO1NBQ3BDO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0FBRUQ7UUFDSSxLQUFLLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtZQUM5QyxFQUFFLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN6QyxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7OztBQ3BJRCxRQUVBO1FBRUUsb0JBQW9CLEtBQUs7WUFBTCxVQUFLLEdBQUwsS0FBSyxDQUFBO1NBQUk7UUFFN0Isc0JBQUksNEJBQUk7OztnQkFBUjtnQkFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7YUFDbkI7OztXQUFBO3lCQVJIO1FBU0MsQ0FBQTtBQVBELFFBU0E7UUFXRSx3QkFBb0IsS0FBSztZQUFMLFVBQUssR0FBTCxLQUFLLENBQUE7U0FBSTtRQUU3QixzQkFBSSxnQ0FBSTs7O2dCQUFSO2dCQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNuQjs7O1dBQUE7NkJBMUJIO1FBMkJDLENBQUE7QUFoQkQ7Ozs7Ozs7UUFxQkUsNkJBQUc7Ozs7WUFBSCxVQUFJLE1BQU07Z0JBQ1IsT0FBTyxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQzthQUNuRDs7Ozs7O1FBRUQsNkJBQUc7Ozs7O1lBQUgsVUFBSSxNQUFNLEVBQUUsSUFBUztnQkFDbkIsT0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ3JEOzs7OztRQUVELDBDQUFnQjs7OztZQUFoQixVQUFpQixNQUFNO2dCQUNyQix5QkFBdUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQzthQUN6Qzs7Ozs7UUFFRCxzQ0FBWTs7OztZQUFaLFVBQWEsTUFBTTtnQkFDakIseUJBQW1CLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUM7YUFDckM7Ozs7OztRQUVELDBDQUFnQjs7Ozs7WUFBaEIsVUFBaUIsTUFBTSxFQUFFLElBQW9CO2dCQUMzQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUN4Qjs7Ozs7O1FBRUQsc0NBQVk7Ozs7O1lBQVosVUFBYSxNQUFNLEVBQUUsSUFBZ0I7Z0JBQ25DLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3hCOztvQkF6QkZDLGVBQVU7OzhCQTdCWDs7Ozs7Ozs7OztBQ0tBOztRQUFBO1FBQW1DQyw4QkFBUTtRQWtCekMsb0JBQW9CLE9BQW1CLEVBQVUsSUFBb0I7WUFBRSxlQUFhO2lCQUFiLFVBQWEsRUFBYixxQkFBYSxFQUFiLElBQWE7Z0JBQWIsOEJBQWE7O1lBQXBGLHdDQUNXLEtBQUssV0FFZjtZQUhtQixhQUFPLEdBQVAsT0FBTyxDQUFZO1lBQVUsVUFBSSxHQUFKLElBQUksQ0FBZ0I7WUFFbkUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFJLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDOztTQUNuRDs7Ozs7OztRQW5CTSxnQkFBSzs7Ozs7O1lBQVosVUFBZ0IsRUFBaUIsRUFBRSxFQUFpQjtnQkFDbEQscUJBQU0sSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ2pDLHFCQUFNLENBQUMsR0FBRyxJQUFJLFVBQVUsQ0FBSSxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUM5QyxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQSxDQUFDLENBQUM7Z0JBQzdCLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsQ0FBQztnQkFDN0IsT0FBTyxDQUFDLENBQUM7YUFDVjtRQU9ELHNCQUFJLDZCQUFLOzs7Z0JBQVQ7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzthQUN4Qjs7O1dBQUE7UUFFRCxzQkFBSSw0QkFBSTs7O2dCQUFSO2dCQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDdkI7OztXQUFBO1FBRUQsc0JBQUksaUNBQVM7OztnQkFBYjtnQkFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQzNCOzs7V0FBQTs7OztRQUVELDBCQUFLOzs7WUFBTDtnQkFDRSx5QkFBd0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBQzthQUM3RDs7OztRQUVELDZCQUFROzs7WUFBUjtnQkFDRSx5QkFBd0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQzthQUNoRTs7OztRQUVELHlCQUFJOzs7WUFBSjtnQkFDRSx5QkFBd0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQzthQUM1RDs7OztRQUVELHlCQUFJOzs7WUFBSjtnQkFDRSx5QkFBd0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQzthQUM1RDs7OztRQUVELDhCQUFTOzs7WUFBVDtnQkFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO2FBQ3hCOzs7O1FBRUQsaUNBQVk7OztZQUFaO2dCQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDM0I7Ozs7UUFFRCw2QkFBUTs7O1lBQVI7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUN2Qjs7OztRQUVELDZCQUFROzs7WUFBUjtnQkFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ3ZCOzs7O1FBRUQsNkJBQVE7OztZQUFSO2dCQUFBLGlCQUlDO2dCQUhDLE9BQU8sSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FDckJDLGFBQUcsQ0FBQyxVQUFDLE1BQXFCLElBQUssT0FBQSxVQUFVLENBQUMsS0FBSyxDQUFJLEtBQUksRUFBRSxNQUFNLENBQUMsR0FBQSxDQUFDLENBQ2xFLENBQUM7YUFDSDs7OztRQUVELDRCQUFPOzs7WUFBUDtnQkFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUN2Qjs7OztRQUVELDRCQUFPOzs7WUFBUDtnQkFDRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzthQUN6Qjs7OztRQUVELGdDQUFXOzs7WUFBWDtnQkFDRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQzthQUM3Qjs7OztRQUVELDZCQUFROzs7WUFBUjtnQkFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUMzQjs7OztRQUVELDRCQUFPOzs7WUFBUDtnQkFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUN2Qjt5QkFoR0g7TUFLbUMsS0FBSyxFQTRGdkM7Ozs7OztRQ3pGRDtRQUlFLG1CQUFZLElBQVMsRUFBRSxJQUFlO1lBQ3BDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1NBQ2xCOzs7O1FBRUQsd0JBQUk7OztZQUFKO2dCQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNuQjs7OztRQUVELDBCQUFNOzs7WUFBTjtnQkFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUN0Rzt3QkF2Qkg7UUF3QkMsQ0FBQTtBQWhCRCxRQWtCQTtRQUFvQ0Qsa0NBQVM7UUFHM0Msd0JBQVksSUFBWSxFQUFFLElBQWUsRUFBRSxNQUFtQjtZQUE5RCxZQUNFLGtCQUFNLElBQUksRUFBRSxJQUFJLENBQUMsU0FFbEI7WUFEQyxLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs7U0FDdkI7Ozs7UUFFRCwrQkFBTTs7O1lBQU47Z0JBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQ2xCOzs7O1FBRUQsK0JBQU07OztZQUFOO2dCQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUNyQjs2QkF4Q0g7TUEwQm9DLFNBQVMsRUFlNUMsQ0FBQTtBQWZELFFBZ0JBO1FBQW1DQSxpQ0FBUzs7Ozs0QkExQzVDO01BMENtQyxTQUFTLEVBQUcsQ0FBQTtBQUEvQyxRQUNBO1FBQXFDQSxtQ0FBUzs7Ozs4QkEzQzlDO01BMkNxQyxTQUFTLEVBQUcsQ0FBQTtBQUFqRCxRQUNBO1FBQW9DQSxrQ0FBUzs7Ozs2QkE1QzdDO01BNENvQyxTQUFTLEVBQUcsQ0FBQTtBQUFoRCxRQUNBO1FBQXVDQSxxQ0FBUzs7OztnQ0E3Q2hEO01BNkN1QyxTQUFTLEVBQUcsQ0FBQTtBQUFuRCxRQUNBO1FBQXNDQSxvQ0FBUzs7OzsrQkE5Qy9DO01BOENzQyxTQUFTLEVBQUcsQ0FBQTtBQUFsRCx5QkFNVywyQkFBMkIsR0FBRyxJQUFJRixtQkFBYyxDQUFrQiw2QkFBNkIsQ0FBQzs7Ozs7O1FDbEQzRztRQUVFLGtCQUFvQixLQUFlO1lBQWYsVUFBSyxHQUFMLEtBQUssQ0FBVTtTQUFJO1FBRXZDLHNCQUFJLDBCQUFJOzs7Z0JBQVI7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO2FBQ25COzs7V0FBQTt1QkFSSDtRQVNDLENBQUE7QUFQRCxRQVNBO1FBQXNDRSxvQ0FBUTs7OzsrQkFYOUM7TUFXc0MsUUFBUSxFQUc3QyxDQUFBO0FBSEQsUUFLQTtRQUEyQ0EseUNBQVE7OzsyQkFDTyxFQUFFOzs7b0NBakI1RDtNQWdCMkMsUUFBUSxFQUVsRCxDQUFBO0FBRkQsUUFJQTtRQUFvQ0Esa0NBQVE7Ozt1QkFFOUIsSUFBSTs0QkFFNkUsRUFBRTsrQkFDcEMsRUFBRTtpQ0FDcUIsRUFBRTs7Ozs7OztRQUk3RSxtQ0FBVTs7OztzQkFBQyxNQUFZO2dCQUM1QixxQkFBSSxPQUFPLEdBQUcsSUFBSUUsZ0JBQVcsRUFBRSxDQUFDO2dCQUNoQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOztvQkFDbEQsS0FBcUIsSUFBQSxLQUFBQyxTQUFBLElBQUksQ0FBQyxPQUFPLENBQUEsZ0JBQUE7d0JBQTVCLElBQU0sTUFBTSxXQUFBO3dCQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sRUFBRTs0QkFDN0IsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLFFBQVEsR0FBRyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7eUJBQzdGO3FCQUNGOzs7Ozs7Ozs7Ozs7Ozs7Z0JBQ0QsT0FBTyxPQUFPLENBQUM7Ozs7OztRQUdWLDZDQUFvQjs7OztnQkFDekIscUJBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztnQkFDcEIsS0FBSyxxQkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsaUJBQWlCLEVBQUU7d0JBQ2hELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ3pCO2lCQUNGO2dCQUNELE9BQU8sUUFBUSxDQUFDOzs2QkFoRHBCO01Bb0JvQyxRQUFRLEVBOEIzQyxDQUFBO0FBOUJELFFBZ0NBO1FBQXFDSCxtQ0FBUTtRQUczQyx5QkFBWSxJQUFjLEVBQVUsV0FBMkI7WUFBL0QsWUFDRSxrQkFBTSxJQUFJLENBQUMsU0FDWjtZQUZtQyxpQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7O1NBRTlEO1FBRUQsc0JBQUksdUNBQVU7OztnQkFBZDtnQkFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7YUFDekI7OztXQUFBOzhCQTdESDtNQW9EcUMsUUFBUSxFQVU1Qzs7Ozs7Ozs7Ozs7OztRQ3hEUSxlQUFHOzs7O1lBQVYsVUFBVyxHQUFRO2dCQUNqQixPQUFPLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2pDOzs7OztRQUVNLHFCQUFTOzs7O1lBQWhCLFVBQWlCLEdBQVc7O29CQUMxQixLQUFtQixJQUFBLEtBQUFHLFNBQUEsV0FBVyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQSxnQkFBQTt3QkFBdEMsSUFBTSxJQUFJLFdBQUE7d0JBQ2IsSUFBSSxJQUFJLFlBQVksY0FBYyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssR0FBRyxFQUFFOzRCQUN2RCxPQUFPLElBQUksQ0FBQzt5QkFDYjtxQkFDRjs7Ozs7Ozs7Ozs7Ozs7OzthQUNGOzs7OztRQUVNLGdDQUFvQjs7OztZQUEzQixVQUE0QixHQUFhO2dCQUN2QyxxQkFBSSxJQUFJLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDVCxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQy9CLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3ZCO2dCQUNELHlCQUF1QixJQUFJLEVBQUM7YUFDN0I7Ozs7O1FBRU0sdUNBQTJCOzs7O1lBQWxDLFVBQW1DLEdBQWE7Z0JBQzlDLHFCQUFJLElBQUksR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLENBQUMsSUFBSSxFQUFFO29CQUNULElBQUksR0FBRyxJQUFJLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN0QyxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN2QjtnQkFDRCx5QkFBOEIsSUFBSSxFQUFDO2FBQ3BDOzs7Ozs7O1FBRU0sa0NBQXNCOzs7Ozs7WUFBN0IsVUFBOEIsR0FBUSxFQUFFLFFBQWdCLEVBQUUsSUFBVTtnQkFDbEUscUJBQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JFLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNwQyxVQUFVLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksZ0JBQWdCLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO2lCQUNuSDtnQkFDRCxPQUFPLFVBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDeEM7Ozs7OztRQUVNLHFDQUF5Qjs7Ozs7WUFBaEMsVUFBaUMsR0FBYSxFQUFFLE1BQWdCO2dCQUM5RCxxQkFBSSxJQUFJLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDVCxJQUFJLEdBQUcsSUFBSSxlQUFlLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUMxRSxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN2QjtnQkFDRCx5QkFBd0IsSUFBSSxFQUFDO2FBQzlCOzs7OztRQUVNLGVBQUc7Ozs7WUFBVixVQUFXLElBQWM7Z0JBQ3ZCLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDdEM7MEJBbkR3QyxJQUFJLEdBQUcsRUFBaUI7MEJBSm5FOzs7Ozs7Ozs7Ozs7O1FDU0UsdUJBQ29FO1lBQUEsYUFBUSxHQUFSLFFBQVE7WUFFMUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQzthQUNwQjtTQUNGOzs7OztRQUVNLDhCQUFNOzs7O3NCQUFDLEtBQVk7Z0JBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBc0I7b0JBQzNDLHFCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzt3QkFDMUUsS0FBd0IsSUFBQSxLQUFBQSxTQUFBLElBQUksQ0FBQyxNQUFNLENBQUEsZ0JBQUE7NEJBQTlCLElBQU0sU0FBUyxXQUFBOzRCQUNsQixJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLFdBQVcsSUFBSSxTQUFTLENBQUMsTUFBTSxLQUFLLEtBQUssQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQ0FDL0UsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDdkI7eUJBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozs7aUJBQ0YsQ0FBQyxDQUFDO2dCQUNILE9BQU8sS0FBSyxDQUFDOzs7b0JBcEJoQkosZUFBVTs7Ozs7b0RBSU5LLGFBQVEsWUFBSUMsV0FBTSxTQUFDLDJCQUEyQjs7OzRCQVZuRDs7Ozs7Ozs7Ozs7O0FDQUE7UUFxQkUsb0JBQ1lDLE9BQWdCLEVBQ2hCLE1BQXFCLEVBQ3JCLFdBQTRCLEVBQ047WUFIdEIsU0FBSSxHQUFKQSxPQUFJLENBQVk7WUFDaEIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtZQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBaUI7WUFDTixXQUFNLEdBQU4sTUFBTTtZQUV0QyxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQy9DOzs7OztRQUVELDRCQUFPOzs7O1lBQVAsVUFBUSxNQUE4RDtnQkFDcEUsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQzNDOzs7OztRQUVELHdCQUFHOzs7O1lBQUgsVUFBSSxFQUFtQjtnQkFDckIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQzthQUM5Qzs7Ozs7UUFFRCwyQkFBTTs7OztZQUFOLFVBQU8sTUFBVztnQkFDaEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQy9EOzs7OztRQUVELGlDQUFZOzs7O1lBQVosVUFBYSxFQUFtQjtnQkFDOUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN0Rjs7Ozs7O1FBRUQsMEJBQUs7Ozs7O1lBQUwsVUFBTSxJQUFZLEVBQUUsTUFBOEQ7Z0JBQWxGLGlCQWNDO2dCQWJDLHFCQUFJLENBQWEsQ0FBQztnQkFDbEIsSUFBSSxNQUFNLFlBQVlDLGVBQVUsRUFBRTtvQkFDaEMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztpQkFDWjtxQkFBTTtvQkFDTCxDQUFDLEdBQUcsSUFBSUEsZUFBVSxFQUFFLENBQUM7b0JBQ3JCLEtBQUsscUJBQU0sR0FBRyxJQUFJLE1BQU0sRUFBRTt3QkFDeEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxHQUFHLE9BQU8sSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDM0c7aUJBQ0Y7Z0JBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQzVHQyxhQUFHLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBQSxDQUFDLENBQ3RGLENBQUM7YUFDSDs7Ozs7UUFFRCx5QkFBSTs7OztZQUFKLFVBQUssTUFBVztnQkFBaEIsaUJBdUJDO2dCQXRCQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxxQkFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO2dCQUNkLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFO29CQUM3QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUMxQztxQkFBTTtvQkFDTCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUEsQ0FBQyxDQUFDO2lCQUN0RztnQkFDRCxxQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRWpELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUNuQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFDN0UsSUFBSSxFQUNKLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUNuRCxDQUFDLElBQUksQ0FDSkEsYUFBRyxDQUFDLFVBQUMsSUFBSTtvQkFDUCxJQUFJLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNDLElBQUksQ0FBQyxJQUFJLEVBQUU7d0JBQ1QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO3FCQUM3QztpQkFDRixDQUFDLEVBQ0ZBLGFBQUcsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBQSxDQUFDLENBQzFELENBQUM7YUFDTDs7Ozs7UUFFRCwyQkFBTTs7OztZQUFOLFVBQU8sTUFBVztnQkFBbEIsaUJBUUM7Z0JBUEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ25ELElBQUksSUFBSSxFQUFFO29CQUNSLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FDL0dBLGFBQUcsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFBLENBQUMsQ0FDNUQsQ0FBQztpQkFDSDthQUNGOztvQkE5RUZULGVBQVU7Ozs7O3dCQWhCRlUsZUFBVTt3QkFRakIsYUFBYTt3QkFMTixlQUFlO3dEQXFCbkJKLFdBQU0sU0FBQyxZQUFZOzs7eUJBekJ4Qjs7Ozs7OztBQ0FBO1FBZUUsOEJBQzJDO1lBQUEsV0FBTSxHQUFOLE1BQU07U0FDN0M7Ozs7OztRQUVKLHdDQUFTOzs7OztZQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtnQkFDcEQsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUM5SCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzdCO2dCQUNELE9BQU8sSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzlDOzs7OztRQUVPLGlEQUFrQjs7OztzQkFBQyxPQUF5QjtnQkFDbEQsT0FBTyxPQUFPLENBQUMsTUFBTSxLQUFLLEtBQUssSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7UUFHOUYsc0RBQXVCOzs7O3NCQUFDLE9BQXlCO2dCQUN2RCxxQkFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3JFLE9BQU9LLE9BQUUsQ0FBQyxJQUFJQyxpQkFBWSxDQUFDO29CQUN6QixJQUFJLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFDO29CQUMxSCxNQUFNLEVBQUUsR0FBRztvQkFDWCxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUc7aUJBQ2pCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FDTlYsYUFBRyxDQUFDLFVBQUMsUUFBUTtvQkFDWCxJQUFJLFFBQVEsWUFBWVUsaUJBQVksRUFBRTt3QkFDcEMsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDOzRCQUNwQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQzt5QkFDNUQsQ0FBQyxDQUFDO3FCQUNKO2lCQUNGLENBQUMsQ0FDSCxDQUFDOzs7b0JBaENMWixlQUFVOzs7Ozt3REFJTk0sV0FBTSxTQUFDLFlBQVk7OzttQ0FoQnhCOzs7Ozs7O0FDQ0EseUJBUVcsNEJBQTRCLEdBQUcsSUFBSVAsbUJBQWMsQ0FBbUIsOEJBQThCLENBQUM7Ozs7OztBQ1Q5RyxRQUFBOzs7MkJBQUE7UUFLQzs7Ozs7O0FDTEQ7UUFPSSx5QkFBb0IsSUFBcUI7WUFBckIsU0FBSSxHQUFKLElBQUksQ0FBaUI7dUJBRmhCLElBQUksR0FBRyxFQUFlO1NBRUY7Ozs7O1FBRTdDLDZCQUFHOzs7O1lBQUgsVUFBSSxJQUFTO2dCQUNYLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM5Qzs7Ozs7UUFFRCxnQ0FBTTs7OztZQUFOLFVBQU8sSUFBUztnQkFDZCxxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pDLElBQUksSUFBSSxFQUFFO29CQUNSLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNsQjtnQkFDRCxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN2Qjs7Ozs7UUFFRCw2QkFBRzs7OztZQUFILFVBQUksRUFBVTtnQkFDWixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3pCOzs7OztRQUVELDZCQUFHOzs7O1lBQUgsVUFBSSxFQUFVO2dCQUNaLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDekI7Ozs7UUFFRCwrQkFBSzs7O1lBQUw7Z0JBQ0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNsQjs7b0JBNUJKQyxlQUFVOzs7Ozt3QkFGRixlQUFlOzs7OEJBRHhCOzs7Ozs7OztRQ1VFLHNCQUNVLFVBQ0EsTUFDQTtZQUZBLGFBQVEsR0FBUixRQUFRO1lBQ1IsU0FBSSxHQUFKLElBQUk7WUFDSixjQUFTLEdBQVQsU0FBUztTQUNmOzs7Ozs7UUFFSiw4QkFBTzs7Ozs7WUFBUCxVQUFRLElBQWtCLEVBQUUsTUFBYztnQkFDeEMsT0FBTyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2xGOzs7OztRQUVPLHdDQUFpQjs7OztzQkFBQyxJQUFrQjtnQkFDMUMscUJBQUksSUFBSSxDQUFDO2dCQUNULElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO29CQUNoQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDOUc7cUJBQU07b0JBQ0wsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN4RTtnQkFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7b0JBQ2hCLEtBQXFCLElBQUEsS0FBQUksU0FBQSxJQUFJLENBQUMsSUFBSSxDQUFBLGdCQUFBO3dCQUF6QixJQUFNLE1BQU0sV0FBQTt3QkFDZixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztxQkFDdkM7Ozs7Ozs7Ozs7Ozs7OztnQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6QixPQUFPLElBQUksQ0FBQzs7Ozs7OztRQUdOLG9DQUFhOzs7O3NCQUFDLElBQWtCO2dCQUN0QyxJQUFJLElBQUksQ0FBQyxJQUFJLFlBQVksY0FBYyxFQUFFO29CQUN2QyxxQkFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFFLEtBQUsscUJBQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO3dCQUMzQyxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsRUFBRTs0QkFDMUUsU0FBUzt5QkFDVjt3QkFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksWUFBWSxFQUFFOzRCQUMvQyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWTtnQ0FDNUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0NBQzNDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3lCQUMzQzs2QkFBTTs0QkFDTCxxQkFBTSxJQUFJLEdBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDOzRCQUN0RCxRQUFRLElBQUk7Z0NBQ1YsS0FBSyxNQUFNO29DQUNULE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29DQUMvQyxNQUFNO2dDQUNSLEtBQUssSUFBSTtvQ0FDUCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29DQUNqRCxNQUFNO2dDQUNSLEtBQUssTUFBTSxDQUFDO2dDQUNaO29DQUNFLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29DQUN2QyxNQUFNOzZCQUNUO3lCQUNGO3FCQUNGO29CQUNELEtBQUsscUJBQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO3dCQUM3QyxxQkFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUNqRixNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUN4RztvQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDM0IsT0FBTyxNQUFNLENBQUM7aUJBQ2Y7cUJBQU07b0JBQ0wsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUNsQjs7O29CQWpFSkosZUFBVTs7Ozs7d0JBUFVhLGFBQVE7d0JBRXBCLGVBQWU7d0JBRWYsZUFBZTs7OzJCQUp4Qjs7Ozs7Ozs7UUNnQkUsd0JBQzJDLFFBQzBCLGFBQ2xEO1lBRndCLFdBQU0sR0FBTixNQUFNO1lBQ29CLGdCQUFXLEdBQVgsV0FBVztZQUM3RCxjQUFTLEdBQVQsU0FBUztZQUUxQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2FBQ3ZCO1NBQ0Y7Ozs7OztRQUVELGtDQUFTOzs7OztZQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtnQkFBdEQsaUJBa0NDO2dCQWpDQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPO3dCQUNuRixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUN0RyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzdCO2dCQUNELHFCQUFJLElBQVksQ0FBQztnQkFDakIscUJBQUksT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7Z0JBQzlCLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsRUFBRTtvQkFDaEMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQ3BDLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2lCQUMzQztnQkFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFOzt3QkFDOUIsS0FBcUIsSUFBQSxLQUFBVCxTQUFBLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFBLGdCQUFBOzRCQUExQyxJQUFNLE1BQU0sV0FBQTs0QkFDZixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtnQ0FDOUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLFFBQVEsR0FBRyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDaEY7eUJBQ0Y7Ozs7Ozs7Ozs7Ozs7OztpQkFDRjtnQkFDRCxPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztvQkFDdEIsT0FBTyxFQUFFLE9BQU87aUJBQ2pCLENBQUMsQ0FBQztnQkFDSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUM5QkYsYUFBRyxDQUFDLFVBQUMsUUFBUTtvQkFDWCxJQUFJLFFBQVEsWUFBWVUsaUJBQVksRUFBRTt3QkFDcEMscUJBQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDOzs0QkFDekQsS0FBeUIsSUFBQSxLQUFBUixTQUFBLEtBQUksQ0FBQyxXQUFXLENBQUEsZ0JBQUE7Z0NBQXBDLElBQU0sVUFBVSxXQUFBO2dDQUNuQixJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUU7b0NBQ3BDLE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lDQUMvRzs2QkFDRjs7Ozs7Ozs7Ozs7Ozs7O3FCQUNGO29CQUNELE9BQU8sUUFBUSxDQUFDOztpQkFDakIsQ0FBQyxDQUNILENBQUM7O2FBQ0g7O29CQS9DRkosZUFBVTs7Ozs7d0RBSU5NLFdBQU0sU0FBQyxZQUFZO29EQUNuQkQsYUFBUSxZQUFJQyxXQUFNLFNBQUMsNEJBQTRCO3dCQVAzQyxZQUFZOzs7NkJBWHJCOzs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7UUFpQkUsK0JBQU87Ozs7O1lBQVAsVUFBUSxJQUFTLEVBQUUsRUFBVTtnQkFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDcEI7O29CQWxCRk4sZUFBVTs7NEJBRlg7O3lCQXVCVyxxQkFBcUIsR0FBRyxJQUFJRCxtQkFBYyxDQUFrQix1QkFBdUIsQ0FBQzs7Ozs7OztRQ25CMURFLG1DQUFhOzs7Ozs7OztRQUNoRCw2QkFBRzs7OztZQUFILFVBQUksRUFBVTtnQkFDWixPQUFPLEtBQUssQ0FBQzthQUNkOzs7Ozs7UUFFRCw2QkFBRzs7Ozs7WUFBSCxVQUFJLEVBQVUsRUFBRSxJQUFTO2FBQ3hCOzs7O1FBRUQsK0JBQUs7OztZQUFMO2FBQ0M7Ozs7O1FBRUQsNkJBQUc7Ozs7WUFBSCxVQUFJLEVBQVU7YUFDYjs7Ozs7UUFFRCxnQ0FBTTs7OztZQUFOLFVBQU8sRUFBVTthQUNoQjs7Ozs7O1FBRUQsa0NBQVE7Ozs7O1lBQVIsVUFBUyxFQUFVLEVBQUUsS0FBYTthQUNqQzs7Ozs7O1FBRUQsaUNBQU87Ozs7O1lBQVAsVUFBUSxJQUFTLEVBQUUsRUFBVTthQUM1Qjs7b0JBdEJGRCxlQUFVOzs4QkFIWDtNQUlxQyxhQUFhOzs7Ozs7O1FDYWhELDBCQUMyQyxRQUN4QixNQUMrQjtZQUZQLFdBQU0sR0FBTixNQUFNO1lBQzlCLFNBQUksR0FBSixJQUFJO1lBQzJCLGtCQUFhLEdBQWIsYUFBYTtTQUMzRDs7Ozs7O1FBRUosb0NBQVM7Ozs7O1lBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO2dCQUF0RCxpQkFzQ0M7Z0JBckNDLHFCQUFNLElBQUksR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDckUsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTzt3QkFDbkYsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDdEcsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUM3QjtnQkFDRCxxQkFBTSxTQUFTLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxlQUFlLENBQUM7Z0JBQzVILHFCQUFJLEtBQVUsQ0FBQzs7b0JBQ2YsS0FBMkIsSUFBQSxLQUFBSSxTQUFBLElBQUksQ0FBQyxhQUFhLENBQUEsZ0JBQUE7d0JBQXhDLElBQU0sWUFBWSxXQUFBO3dCQUNyQixJQUFJLFlBQVksWUFBWSxTQUFTLEVBQUU7NEJBQ3JDLEtBQUssR0FBRyxZQUFZLENBQUM7NEJBQ3JCLE1BQU07eUJBQ1A7cUJBQ0Y7Ozs7Ozs7Ozs7Ozs7OztnQkFDRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO29CQUM1QixLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzdCO2dCQUNELElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkIsT0FBT08sT0FBRSxDQUFDLElBQUlDLGlCQUFZLENBQUM7d0JBQ3pCLElBQUksRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQzt3QkFDckIsTUFBTSxFQUFFLEdBQUc7d0JBQ1gsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHO3FCQUNqQixDQUFDLENBQUMsQ0FBQztpQkFDTDtnQkFDRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUM5QkgsYUFBRyxDQUFDLFVBQUMsUUFBMkI7b0JBQzlCLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTt3QkFDakIscUJBQU0sSUFBSSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDMUMsSUFBSSxJQUFJLEVBQUU7NEJBQ1IsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDcEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtnQ0FDdEIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDdkM7eUJBQ0Y7cUJBQ0Y7aUJBQ0YsQ0FBQyxDQUNILENBQUM7O2FBQ0g7O29CQS9DRlQsZUFBVTs7Ozs7d0RBSU5NLFdBQU0sU0FBQyxZQUFZO3dCQVRVLGVBQWU7b0RBVzVDQSxXQUFNLFNBQUMscUJBQXFCOzs7K0JBcEJqQzs7Ozs7Ozs7UUNNeUNMLHVDQUFhO1FBTXBELDZCQUNtQixXQUN3QjtZQUYzQyxZQUlFLGlCQUFPLFNBQ1I7WUFKa0IsZUFBUyxHQUFULFNBQVM7WUFDZSxZQUFNLEdBQU4sTUFBTTswQkFOSixFQUFFOzRCQUVBLEVBQUU7O1NBT2hEOzs7Ozs7UUFFRCxpQ0FBRzs7Ozs7WUFBSCxVQUFJLEVBQVUsRUFBRSxJQUFTO2dCQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUM3Qjs7OztRQUVELG1DQUFLOzs7WUFBTDtnQkFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ3hCOzs7OztRQUVELGlDQUFHOzs7O1lBQUgsVUFBSSxFQUFVO2dCQUNaLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDL0I7Ozs7O1FBRUQsb0NBQU07Ozs7WUFBTixVQUFPLEVBQVU7Z0JBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDM0I7Ozs7OztRQUVELHNDQUFROzs7OztZQUFSLFVBQVMsRUFBVSxFQUFFLEtBQWE7Z0JBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQzFCOzs7OztRQUVELGlDQUFHOzs7O1lBQUgsVUFBSSxFQUFVO2dCQUNaLHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzlFLE9BQU8sSUFBSSxLQUFLLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQzthQUM1RDs7b0JBckNGRCxlQUFVOzs7Ozt3QkFIRixlQUFlO3dEQVluQk0sV0FBTSxTQUFDLFlBQVk7OztrQ0FkeEI7TUFNeUMsYUFBYTs7Ozs7O0FDSnRELFFBR0E7Ozs7Ozs7O1FBQ0UsMkNBQVM7Ozs7O1lBQVQsVUFBVSxRQUEyQixFQUFFLElBQWE7Z0JBQ2xELHFCQUFNLENBQUMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO2dCQUM3QixDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDckIsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QyxDQUFDLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUM1QixDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztnQkFDWixPQUFPLENBQUMsQ0FBQzthQUNWOzs7OztRQUVELDBDQUFROzs7O1lBQVIsVUFBUyxJQUFZO2dCQUNuQixPQUFPLElBQUksS0FBSyxZQUFZLENBQUM7YUFDOUI7c0NBakJIO1FBa0JDOzs7Ozs7UUNaRDs7Ozs7Ozs7UUFDRSx3Q0FBUzs7Ozs7WUFBVCxVQUFVLFFBQTJCLEVBQUUsSUFBYTtnQkFDbEQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNwQzs7Ozs7UUFFRCx1Q0FBUTs7OztZQUFSLFVBQVMsSUFBWTtnQkFDbkIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLHFCQUFxQixDQUFDO2FBQ3JEOzs7Ozs7O1FBRU8sc0NBQU87Ozs7OztzQkFBQyxJQUFJLEVBQUUsSUFBVSxFQUFFLFVBQW9CO2dCQUNwRCxxQkFBTSxDQUFDLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztnQkFDN0IsQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLGtCQUFrQixDQUFDO2dCQUNsRSxxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN6QixDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUN0SCxJQUFJLENBQUMsQ0FBQyxVQUFVLEVBQUU7b0JBQ2hCLHFCQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDO29CQUMzQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7b0JBQ25GLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUMxQixJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTt3QkFDdEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUNqRCxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzt3QkFDdkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUMvQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBQy9DLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7NEJBQ2hDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDNUI7cUJBQ0Y7b0JBQ0QsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQztvQkFDeEQscUJBQU0sU0FBUyxHQUFHLGFBQWEsQ0FBQztvQkFDaEMscUJBQUksT0FBTyxTQUFBLENBQUM7b0JBQ1osT0FBTyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQy9DLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNuRCxDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzs7d0JBQ1osS0FBcUIsSUFBQSxVQUFBRixTQUFBLEtBQUssQ0FBQSw0QkFBQTs0QkFBckIsSUFBTSxNQUFNLGtCQUFBOzRCQUNmLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt5QkFDbkM7Ozs7Ozs7Ozs7Ozs7OztpQkFDRjtxQkFBTTtvQkFDTCxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM5QixDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDWixJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQy9DLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO3FCQUNmO3lCQUFNO3dCQUNMLEtBQUsscUJBQU0sR0FBRyxJQUFJLElBQUksRUFBRTs0QkFDdEIsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQ0FDekQsU0FBUzs2QkFDVjs0QkFDRCxxQkFBTSxZQUFZLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7NEJBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxJQUFJLElBQUksWUFBWSxJQUFJLFdBQVcsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dDQUNuSCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxZQUFZLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3lCQUN0RztxQkFDRjtpQkFDRjtnQkFDRCxPQUFPLENBQUMsQ0FBQzs7O21DQTVEYjtRQThEQzs7Ozs7O1FDeEREOzs7Ozs7OztRQUNFLHNDQUFTOzs7OztZQUFULFVBQVUsUUFBMkIsRUFBRSxJQUFZO2dCQUNqRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNqSDs7Ozs7UUFFRCxxQ0FBUTs7OztZQUFSLFVBQVMsSUFBWTtnQkFDbkIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLGtCQUFrQixDQUFDO2FBQ2xEOzs7Ozs7OztRQUVPLG9DQUFPOzs7Ozs7O3NCQUFDLElBQUksRUFBRSxJQUFTLEVBQUUsVUFBbUIsRUFBRSxPQUFxQjtnQkFDekUscUJBQU0sQ0FBQyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO2dCQUMxQixxQkFBTSxjQUFjLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQztnQkFDdEcscUJBQU0sSUFBSSxHQUFHLGNBQWMsSUFBSSxVQUFVLEdBQUcsY0FBYyxHQUFHLGNBQWMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxTQUFTLENBQUM7Z0JBQy9HLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNkLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRTtvQkFDaEIsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLElBQUksSUFBSSxDQUFDLENBQUM7b0JBQ25FLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLFNBQVMsQ0FBQztvQkFDeEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxTQUFTLENBQUM7b0JBQzFELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLElBQUksU0FBUyxDQUFDO29CQUN0RCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLFNBQVMsQ0FBQztvQkFDdEQsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTt3QkFDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO3FCQUNyQjtvQkFDRCxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ3JELHFCQUFNLFNBQVMsR0FBRyxhQUFhLENBQUM7b0JBQ2hDLHFCQUFJLE9BQU8sU0FBQSxDQUFDO29CQUNaLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMvQyxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbkQsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7O3dCQUNaLEtBQXFCLElBQUEsU0FBQUEsU0FBQSxJQUFJLENBQUEsMEJBQUE7NEJBQXBCLElBQU0sTUFBTSxpQkFBQTs0QkFDZixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7eUJBQ2xEOzs7Ozs7Ozs7Ozs7Ozs7aUJBQ0Y7cUJBQU07b0JBQ0wsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDOUIsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7b0JBQ1osS0FBSyxxQkFBTSxHQUFHLElBQUksSUFBSSxFQUFFO3dCQUN0QixxQkFBTSxZQUFZLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxJQUFJLElBQUksWUFBWSxJQUFJLFdBQVcsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNuSCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxZQUFZLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUN0RztpQkFDRjtnQkFDRCxPQUFPLENBQUMsQ0FBQzs7O2lDQW5EYjtRQXFEQzs7Ozs7Ozs7Ozs7O1FDM0M4Q0gsNkNBQWE7UUFJMUQsbUNBQ1UsV0FDQSxXQUNBLE1BQ3NCO1lBSmhDLFlBTUUsaUJBQU8sU0FTUjtZQWRTLGVBQVMsR0FBVCxTQUFTO1lBQ1QsZUFBUyxHQUFULFNBQVM7WUFDVCxVQUFJLEdBQUosSUFBSTtZQUNrQixZQUFNLEdBQU4sTUFBTTtnQ0FOaEIsZUFBZTtZQVNuQyxxQkFBTSxlQUFlLEdBQUcsS0FBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLENBQUM7WUFDeEQsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsR0FBQSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQztnQkFDbEYscUJBQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMvQyxxQkFBTSxTQUFTLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLFNBQVMsSUFBSSxTQUFTLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUMvRCxLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNqQjthQUNGLENBQUMsQ0FBQzs7U0FDSjs7Ozs7UUFFRCx1Q0FBRzs7OztZQUFILFVBQUksRUFBVTtnQkFDWixFQUFFLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsR0FBRyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2xFLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRTtvQkFDaEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUN4RDthQUNGOzs7Ozs7UUFFRCx1Q0FBRzs7Ozs7WUFBSCxVQUFJLEVBQVUsRUFBRSxJQUFTO2dCQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDNUI7Ozs7OztRQUVELDRDQUFROzs7OztZQUFSLFVBQVMsRUFBVSxFQUFFLEtBQWE7Z0JBQ2hDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLEdBQUcsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQy9EOzs7OztRQUVELHVDQUFHOzs7O1lBQUgsVUFBSSxFQUFVO2dCQUNaLEVBQUUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDbEUscUJBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN2RixxQkFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDeEMsT0FBTyxDQUFDLENBQUMsU0FBUyxJQUFJLFFBQVEsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO2FBQ2xGOzs7OztRQUVELDBDQUFNOzs7O1lBQU4sVUFBTyxFQUFVO2dCQUNmLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDL0MsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDekQsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDL0QsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDN0QsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMxQjs7OztRQUVELHlDQUFLOzs7WUFBTDtnQkFBQSxpQkFJQztnQkFIQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBQSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQztvQkFDbkYsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDNUIsQ0FBQyxDQUFDO2FBQ0o7Ozs7Ozs7UUFFTyx5Q0FBSzs7Ozs7O3NCQUFDLEVBQVUsRUFBRSxJQUFTLEVBQUUsUUFBaUI7Z0JBQ3BELHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakMscUJBQUksTUFBVyxDQUFDO2dCQUNoQixJQUFJLElBQUksWUFBWSxjQUFjLEVBQUU7b0JBQ2xDLE1BQU0sR0FBRyxFQUFFLENBQUM7O3dCQUNaLEtBQW1CLElBQUEsU0FBQUcsU0FBQSxJQUFJLENBQUEsMEJBQUE7NEJBQWxCLElBQU0sSUFBSSxpQkFBQTs0QkFDYixxQkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDOzRCQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzRCQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ25DOzs7Ozs7Ozs7Ozs7Ozs7b0JBQ0QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNuRjtxQkFBTTtvQkFDTCxxQkFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDO29CQUN0QixNQUFNLEdBQUcsRUFBRSxDQUFDO29CQUNaLEtBQUsscUJBQU0sR0FBRyxJQUFJLElBQUksRUFBRTt3QkFDdEIscUJBQUksUUFBUSxTQUFLLENBQUM7d0JBQ2xCLElBQUksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxFQUFFOzRCQUNqQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7eUJBQ3JDO3dCQUNELElBQUksUUFBUSxFQUFFOzRCQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7NEJBQzVDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO3lCQUNqQzs2QkFBTTs0QkFDTCxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3lCQUN6QjtxQkFDRjtvQkFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxHQUFHLEVBQUUsRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDOUYsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2lCQUN6RjtnQkFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BGLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNwRSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzs7Ozs7O1FBR2hCLDJDQUFPOzs7O3NCQUFDLEVBQVU7Z0JBQ3hCLHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxxQkFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxxQkFBTSxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztnQkFDcEMsSUFBSSxjQUFjLEVBQUU7b0JBQ2xCLFFBQVEsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUMzQixRQUFRLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQztvQkFDL0IsUUFBUSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDM0QsUUFBUSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7O3dCQUNuQixLQUFxQixJQUFBLFNBQUFBLFNBQUEsSUFBSSxDQUFBLDBCQUFBOzRCQUFwQixJQUFNLE1BQU0saUJBQUE7NEJBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO3lCQUMxQzs7Ozs7Ozs7Ozs7Ozs7O2lCQUNGO3FCQUFNO29CQUNMLFFBQVEsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUM1QixRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUNuQyxRQUFRLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM3RixRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztvQkFDckIscUJBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUMzRixJQUFJLFVBQVUsRUFBRTt3QkFDZCxLQUFLLHFCQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUU7NEJBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt5QkFDcEQ7cUJBQ0Y7aUJBQ0Y7Z0JBQ0QsT0FBTyxRQUFRLENBQUM7Ozs7Ozs7UUFHVixxREFBaUI7Ozs7c0JBQUMsRUFBVTtnQkFDbEMscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNyRixJQUFJLElBQUksRUFBRTtvQkFDUixxQkFBTSxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ3BDLEtBQUsscUJBQU0sR0FBRyxJQUFJLElBQUksRUFBRTt3QkFDdEIsSUFBSSxHQUFHLEtBQUssT0FBTyxFQUFFOzRCQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3lCQUN2QjtxQkFDRjtvQkFDRCxPQUFPLElBQUksQ0FBQztpQkFDYjs7Ozs7O1FBR0ssZ0RBQVk7Ozs7c0JBQUMsRUFBVTtnQkFDN0IsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksR0FBRyxFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Ozs7OztRQUd6RSxnREFBWTs7OztzQkFBQyxFQUFVO2dCQUM3QixPQUFPLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Ozs7OztRQUdwRSxtREFBZTs7OztzQkFBQyxFQUFVO2dCQUNoQyxPQUFPLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsRUFBRSxDQUFDLENBQUM7OztvQkE5SXhFSixlQUFVOzs7Ozt3QkFQRixlQUFlO3dCQUNmLFlBQVk7d0JBRUksZUFBZTt3REFhbkNNLFdBQU0sU0FBQyxZQUFZOzs7d0NBbEJ4QjtNQVUrQyxhQUFhOzs7Ozs7Ozs7Ozs7OztBQ1A1RDs7UUFBQTs7OzBCQUhBO1FBZ0JDOzs7Ozs7Ozs7QUNkRDs7UUFBQTs7O3VDQUZBO1FBT0M7Ozs7OztBQ05EOzs2QkFJc0IsbUJBQW1COzs7OztRQUV2QyxrREFBRzs7O1lBQUg7Z0JBQ0UsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUM3Qzs7Ozs7UUFFRCxrREFBRzs7OztZQUFILFVBQUksS0FBYTtnQkFDZixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDN0M7Ozs7UUFFRCxxREFBTTs7O1lBQU47Z0JBQ0UsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDekM7Ozs7UUFFRCxvREFBSzs7O1lBQUw7Z0JBQ0UsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUM7YUFDckQ7O29CQWxCRk4sZUFBVTs7bURBSFg7Ozs7Ozs7QUNBQTtRQWNFLHlCQUFtQixJQUFpQixFQUFrQztZQUFuRCxTQUFJLEdBQUosSUFBSSxDQUFhO1lBQWtDLFdBQU0sR0FBTixNQUFNO1NBQWU7Ozs7OztRQUUzRixtQ0FBUzs7Ozs7WUFBVCxVQUFVLE9BQXlCLEVBQUUsSUFBaUI7Z0JBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFO29CQUNqSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzdCO2dCQUNELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtvQkFDMUIsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7d0JBQ3RCLFVBQVUsRUFBRTs0QkFDVixhQUFhLEVBQUUsWUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBSTt5QkFDaEQ7cUJBQ0YsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUM3Qjs7b0JBakJGQSxlQUFVOzs7Ozt3QkFIRixXQUFXO3dEQU1xQk0sV0FBTSxTQUFDLFlBQVk7Ozs4QkFkNUQ7Ozs7Ozs7QUNBQTtRQVlFLHdCQUNVQyxTQUNBLE9BQ3dCO1lBRnhCLFNBQUksR0FBSkEsT0FBSTtZQUNKLFVBQUssR0FBTCxLQUFLO1lBQ21CLFdBQU0sR0FBTixNQUFNO1NBQ3BDOzs7O1FBRUosaUNBQVE7OztZQUFSO2dCQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUN6Qjs7Ozs7UUFFRCxpQ0FBUTs7OztZQUFSLFVBQVMsS0FBYTtnQkFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkI7Ozs7UUFFRCwrQkFBTTs7O1lBQU47Z0JBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNyQjs7OztRQUVELG1DQUFVOzs7WUFBVjtnQkFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDM0I7Ozs7UUFFRCxnQ0FBTzs7O1lBQVA7Z0JBQ0UsT0FBT08sZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2FBQ3pDOzs7Ozs7UUFFRCwrQkFBTTs7Ozs7WUFBTixVQUFPLFFBQWdCLEVBQUUsUUFBZ0I7Z0JBQXpDLGlCQVNDO2dCQVJDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ3pDLEtBQUssRUFBRSxRQUFRO29CQUNmLFFBQVEsRUFBRSxRQUFRO2lCQUNuQixDQUFDLENBQUMsSUFBSSxDQUNMTCxhQUFHLENBQUMsVUFBQyxJQUFTO29CQUNaLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUMzQixDQUFDLENBQ0gsQ0FBQzthQUNIOztvQkF0Q0ZULGVBQVU7Ozs7O3dCQVJGVSxlQUFVO3dCQU1WLHdCQUF3Qjt3REFRNUJKLFdBQU0sU0FBQyxZQUFZOzs7NkJBZnhCOzs7Ozs7Ozs7Ozs7QUNBQTs7OztBQVFBLHNCQUEwQixJQUF1QjtRQUF2QixxQkFBQTtZQUFBLFNBQXVCOztRQUUvQyxPQUFPLFVBQUMsTUFBZ0I7WUFDdEIscUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQztZQUNyQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDekIsQ0FBQztLQUNIOzs7Ozs7QUNmRDs7OztBQVFBLHNCQUEwQixJQUF1QjtRQUF2QixxQkFBQTtZQUFBLFNBQXVCOztRQUUvQyxPQUFPLFVBQUMsTUFBVyxFQUFFLEdBQVc7WUFDOUIscUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4RSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxDQUFDO1lBQzdDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUksS0FBSyxDQUFDO1NBQzFELENBQUM7S0FDSDs7Ozs7O0FDZkQ7Ozs7O0FBR0EseUJBQTRCLE1BQWdCLEVBQUUsSUFBYTtRQUV6RCxPQUFPLFVBQUMsTUFBZ0I7WUFDdEIscUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDbkUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksR0FBRyxHQUFHUyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3RILElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztTQUNsQyxDQUFDO0tBQ0g7Ozs7OztBQ1ZEOzs7OztBQUVBLG9CQUF1QixVQUFrQixFQUFFLE1BQWdCO1FBRXpELE9BQU8sVUFBQyxNQUFXLEVBQUUsR0FBVyxFQUFFLEtBQXNCO1lBQXRCLHNCQUFBO2dCQUFBLGlCQUFzQjs7WUFDdEQsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNoRSxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxHQUFHLGNBQWMsT0FBUSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDL0QsTUFBTSxFQUFFLE1BQU0sSUFBSSxLQUFLO2dCQUN2QixPQUFPLEVBQUUsSUFBSTthQUNkLENBQUMsQ0FBQztTQUNKLENBQUM7S0FDSDs7Ozs7Ozs7OztBQ1ZELHFCQUF5QixPQUEyRDtRQUVsRixPQUFPLFVBQUMsTUFBVztZQUNqQixxQkFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO29DQUMzQyxNQUFNO2dCQUNmLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO29CQUNoQixJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7b0JBQ2pCLEtBQUssRUFBRSxjQUFNLE9BQUEsTUFBTSxDQUFDLEtBQUssR0FBQTtvQkFDekIsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLElBQUksS0FBSztvQkFDOUIsT0FBTyxFQUFFLEtBQUs7aUJBQ2YsQ0FBQyxDQUFDOzs7Z0JBTkwsS0FBcUIsSUFBQSxZQUFBWCxTQUFBLE9BQU8sQ0FBQSxnQ0FBQTtvQkFBdkIsSUFBTSxNQUFNLG9CQUFBOzRCQUFOLE1BQU07aUJBT2hCOzs7Ozs7Ozs7Ozs7Ozs7O1NBQ0YsQ0FBQztLQUNIOzs7Ozs7QUNmRDs7O0FBRUE7UUFFRSxPQUFPLFVBQUMsTUFBVyxFQUFFLEdBQW9CLEVBQUUsS0FBc0I7WUFBNUMsb0JBQUE7Z0JBQUEsZUFBb0I7O1lBQUUsc0JBQUE7Z0JBQUEsaUJBQXNCOztZQUMvRCxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksR0FBRyxVQUFDLE1BQVc7Z0JBQ3RFLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxjQUFjLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDL0UsQ0FBQztTQUNILENBQUM7S0FDSDs7Ozs7O0FDVEQ7Ozs7O0FBRUEseUJBQTZCLElBQWMsRUFBRSxJQUFhO1FBRXhELE9BQU8sVUFBQyxNQUFXLEVBQUUsR0FBVyxFQUFFLEtBQVU7WUFDMUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEdBQUc7Z0JBQ3ZFLElBQUksRUFBRSxXQUFXLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO2dCQUM1QyxJQUFJLEVBQUUsSUFBSSxJQUFJLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRTthQUN0RixDQUFDO1NBQ0gsQ0FBQztLQUNIOzs7Ozs7QUNWRDs7O0FBRUE7UUFFRSxPQUFPLFVBQUMsTUFBVyxFQUFFLEdBQVc7WUFDOUIscUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDbEUsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUM7U0FDZixDQUFDO0tBQ0g7Ozs7Ozs7Ozs7Ozs7O0FDSEQ7O1FBQUE7UUFTRSxnQkFBc0IsT0FBbUIsRUFBRSxPQUFXLEVBQUUsTUFBVTtZQUF2Qix3QkFBQTtnQkFBQSxXQUFXOztZQUFFLHVCQUFBO2dCQUFBLFVBQVU7O1lBQWxFLGlCQWlCQztZQWpCcUIsWUFBTyxHQUFQLE9BQU8sQ0FBWTswQkFQZixFQUFFOzJCQUVWLEtBQUs7WUFNckIsSUFBSSxDQUFDLFVBQVUsR0FBR1ksa0JBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ2hELElBQUksQ0FDSCxNQUFNLEdBQUdDLHFCQUFXLENBQUMsTUFBTSxDQUFDLEdBQUdDLGVBQUssRUFBRSxFQUN0Q0Msc0JBQVksQ0FBQyxPQUFPLENBQUMsRUFDckJDLDhCQUFvQixDQUFDLFVBQUMsRUFBUyxFQUFFLEVBQVMsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxHQUFBLENBQUMsRUFDMUVsQixhQUFHLENBQUMsVUFBQyxNQUFhLElBQUssT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFBLENBQUMsRUFDOUNtQixrQkFBUSxDQUFDLFVBQUMsSUFBSTtnQkFDWixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztnQkFDcEIsT0FBTyxLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUN6Q1osYUFBRyxDQUFDLFVBQUMsQ0FBa0I7b0JBQ3JCLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO29CQUNsQixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztpQkFDdEIsQ0FBQyxDQUNILENBQUM7YUFDSCxDQUFDLENBQ0gsQ0FBQztTQUNMOzs7OztRQUVELHNCQUFLOzs7O1lBQUwsVUFBTSxJQUFJO2dCQUNSLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMxQjtRQUVELHNCQUFJLHdCQUFJOzs7Z0JBQVI7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO2FBQ3hCOzs7V0FBQTtRQUVELHNCQUFJLDZCQUFTOzs7Z0JBQWI7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ3JCOzs7V0FBQTs7Ozs7UUFFUyxxQ0FBb0I7Ozs7WUFBOUIsVUFBK0IsRUFBYTtvQkFBYixrQkFBYSxFQUFaLFlBQUk7Z0JBQ2xDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDbkM7Ozs7OztRQUVTLDhCQUFhOzs7OztZQUF2QixVQUF3QixFQUFTLEVBQUUsRUFBUztnQkFDMUMscUJBQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsTUFBTSxDQUFDO2dCQUM1RCxLQUFLLHFCQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ25DLElBQUksQ0FBQyxDQUFDLEdBQUcsTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDN0QsSUFBSSxDQUFDLElBQUksTUFBTSxFQUFFOzRCQUNmLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQzt5QkFDL0I7d0JBQ0QsT0FBTyxLQUFLLENBQUM7cUJBQ2Q7aUJBQ0Y7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7YUFDYjs7Ozs7UUFFUywwQkFBUzs7OztZQUFuQixVQUFvQixNQUFhO2dCQUMvQixxQkFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQ3pDLHFCQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7Z0JBQzdDLHFCQUFNLElBQUksR0FBRyxFQUFFLENBQUM7Z0JBQ2hCLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2hDLElBQUksQ0FBQyxLQUFLLFNBQVMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUU7d0JBQzdDLElBQUksT0FBTyxDQUFDLEtBQUssU0FBUyxFQUFFOzRCQUMxQixDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sR0FBRyxPQUFPLENBQUM7eUJBQzFCO3dCQUNELElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQzdCO2lCQUNGLENBQUMsQ0FBQztnQkFDSCxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDL0M7Ozs7UUFFUyw0QkFBVzs7O1lBQXJCO2dCQUFBLGlCQUlDO2dCQUhDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSTtvQkFDeEMsT0FBTyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUlhLG9CQUFlLENBQU0sU0FBUyxDQUFDLENBQUM7aUJBQ2hFLENBQUMsQ0FBQzthQUNKO3FCQWpGSDtRQWtGQzs7Ozs7Ozs7O0FDL0VEOztRQUFBO1FBQW1EckIsd0NBQU07Ozs7Ozs7UUFJdkQsdUNBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pDOzs7O1FBRUQsc0NBQU87OztZQUFQO2dCQUNFLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ2pEOzs7OztRQUVTLG1EQUFvQjs7OztZQUE5QixVQUErQixFQUF1QjtvQkFBdkIsa0JBQXVCLEVBQXRCLGdCQUFRLEVBQUUsWUFBSTtnQkFDNUMsT0FBTyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsR0FBRyxpQkFBTSxvQkFBb0IsWUFBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDakY7Ozs7UUFFUywwQ0FBVzs7O1lBQXJCO2dCQUNFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSXFCLG9CQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7Z0JBQzNELHFCQUFNLFFBQVEsR0FBRyxpQkFBTSxXQUFXLFdBQUUsQ0FBQztnQkFDckMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQ3ZDLE9BQU8sUUFBUSxDQUFDO2FBQ2pCO21DQXhCSDtNQUdtRCxNQUFNLEVBc0J4RDs7Ozs7Ozs7O0FDbkJEOztRQUFBO1FBQXlDckIsOEJBQU07Ozs7Ozs7UUFJN0MsMEJBQUs7OztZQUFMO2dCQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsRUFBRTtvQkFDL0MsT0FBTztpQkFDUjtnQkFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7YUFDbEQ7Ozs7UUFFRCw2QkFBUTs7O1lBQVI7Z0JBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUFFO29CQUNsRCxPQUFPO2lCQUNSO2dCQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQzthQUNyRDs7OztRQUVELHlCQUFJOzs7WUFBSjtnQkFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLEVBQUU7b0JBQzlDLE9BQU87aUJBQ1I7Z0JBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2FBQ2pEOzs7O1FBRUQseUJBQUk7OztZQUFKO2dCQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsRUFBRTtvQkFDOUMsT0FBTztpQkFDUjtnQkFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7YUFDakQ7Ozs7UUFFRCw2QkFBUTs7O1lBQVI7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDbEQ7Ozs7UUFFRCxnQ0FBVzs7O1lBQVg7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDckQ7Ozs7UUFFRCw0QkFBTzs7O1lBQVA7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDakQ7Ozs7UUFFRCw0QkFBTzs7O1lBQVA7Z0JBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDakQ7Ozs7O1FBRVMseUNBQW9COzs7O1lBQTlCLFVBQStCLEVBQW1CO29CQUFuQixrQkFBbUIsRUFBbEIsWUFBSSxFQUFFLFlBQUk7Z0JBQ3hDLE9BQU8sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxpQkFBTSxvQkFBb0IsWUFBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDbkY7Ozs7UUFFUyxnQ0FBVzs7O1lBQXJCO2dCQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSXFCLG9CQUFlLENBQVMsU0FBUyxDQUFDLENBQUM7Z0JBQzFELHFCQUFNLFFBQVEsR0FBRyxpQkFBTSxXQUFXLFdBQUUsQ0FBQztnQkFDckMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ25DLE9BQU8sUUFBUSxDQUFDO2FBQ2pCO3lCQS9ESDtNQU15QyxNQUFNLEVBMEQ5Qzs7Ozs7Ozs7Ozs7QUNoRUQ7Ozs7Ozs7UUEyQ1MsbUJBQU07Ozs7WUFBYixVQUFjLE1BQWtCO2dCQUM5QixPQUFPO29CQUNMLFFBQVEsRUFBRSxZQUFZO29CQUN0QixTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLFlBQVk7NEJBQ3JCLFFBQVEsRUFBRSxNQUFNO3lCQUNqQjt3QkFDRCxhQUFhO3dCQUNiLGVBQWU7d0JBQ2Y7NEJBQ0UsT0FBTyxFQUFFLHFCQUFxQjs0QkFDOUIsUUFBUSxFQUFFLHlCQUF5Qjs0QkFDbkMsS0FBSyxFQUFFLElBQUk7eUJBQ1o7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFLHFCQUFxQjs0QkFDOUIsUUFBUSxFQUFFLG1CQUFtQjs0QkFDN0IsS0FBSyxFQUFFLElBQUk7eUJBQ1o7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFLHFCQUFxQjs0QkFDOUIsUUFBUSxFQUFFLGVBQWU7NEJBQ3pCLEtBQUssRUFBRSxJQUFJO3lCQUNaO3dCQUNEOzRCQUNFLE9BQU8sRUFBRSxhQUFhOzRCQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxlQUFlO3lCQUMxQzt3QkFDRDs0QkFDRSxPQUFPLEVBQUUsd0JBQXdCOzRCQUNqQyxRQUFRLEVBQUUsTUFBTSxDQUFDLGFBQWEsSUFBSSxvQ0FBb0M7eUJBQ3ZFO3dCQUNEOzRCQUNFLE9BQU8sRUFBRSxXQUFXOzRCQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxjQUFjO3lCQUMvQzt3QkFDRDs0QkFDRSxPQUFPLEVBQUVDLHNCQUFpQjs0QkFDMUIsUUFBUSxFQUFFLGVBQWU7NEJBQ3pCLEtBQUssRUFBRSxJQUFJO3lCQUNaO3dCQUNEOzRCQUNFLE9BQU8sRUFBRUEsc0JBQWlCOzRCQUMxQixRQUFRLEVBQUUsb0JBQW9COzRCQUM5QixLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRDs0QkFDRSxPQUFPLEVBQUVBLHNCQUFpQjs0QkFDMUIsUUFBUSxFQUFFLGdCQUFnQjs0QkFDMUIsS0FBSyxFQUFFLElBQUk7eUJBQ1o7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFQSxzQkFBaUI7NEJBQzFCLFFBQVEsRUFBRSxjQUFjOzRCQUN4QixLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRDs0QkFDRSxPQUFPLEVBQUUsNEJBQTRCOzRCQUNyQyxRQUFRLEVBQUUsdUJBQXVCOzRCQUNqQyxLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRDs0QkFDRSxPQUFPLEVBQUUsNEJBQTRCOzRCQUNyQyxRQUFRLEVBQUUsb0JBQW9COzRCQUM5QixLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRDs0QkFDRSxPQUFPLEVBQUUsNEJBQTRCOzRCQUNyQyxRQUFRLEVBQUUsa0JBQWtCOzRCQUM1QixLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRCxlQUFlO3dCQUNmLFlBQVk7d0JBQ1osVUFBVTtxQkFDWDtpQkFDRixDQUFDO2FBQ0g7Ozs7O1FBQ00scUNBQXdCOzs7O1lBQS9CLFVBQWdDLE1BQWtCO2dCQUNoRCxPQUFPO29CQUNMLFFBQVEsRUFBRSxZQUFZO29CQUN0QixTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLFlBQVk7NEJBQ3JCLFFBQVEsRUFBRSxNQUFNO3lCQUNqQjt3QkFDRCxhQUFhO3dCQUNiLGVBQWU7d0JBQ2Y7NEJBQ0UsT0FBTyxFQUFFLHFCQUFxQjs0QkFDOUIsUUFBUSxFQUFFLHlCQUF5Qjs0QkFDbkMsS0FBSyxFQUFFLElBQUk7eUJBQ1o7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFLHFCQUFxQjs0QkFDOUIsUUFBUSxFQUFFLG1CQUFtQjs0QkFDN0IsS0FBSyxFQUFFLElBQUk7eUJBQ1o7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFLHFCQUFxQjs0QkFDOUIsUUFBUSxFQUFFLGVBQWU7NEJBQ3pCLEtBQUssRUFBRSxJQUFJO3lCQUNaO3dCQUNEOzRCQUNFLE9BQU8sRUFBRSxhQUFhOzRCQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxlQUFlO3lCQUMxQzt3QkFDRDs0QkFDRSxPQUFPLEVBQUUsd0JBQXdCOzRCQUNqQyxRQUFRLEVBQUUsTUFBTSxDQUFDLGFBQWEsSUFBSSxvQ0FBb0M7eUJBQ3ZFO3dCQUNEOzRCQUNFLE9BQU8sRUFBRSxXQUFXOzRCQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxjQUFjO3lCQUMvQzt3QkFDRDs0QkFDRSxPQUFPLEVBQUVBLHNCQUFpQjs0QkFDMUIsUUFBUSxFQUFFLGVBQWU7NEJBQ3pCLEtBQUssRUFBRSxJQUFJO3lCQUNaO3dCQUNEOzRCQUNFLE9BQU8sRUFBRUEsc0JBQWlCOzRCQUMxQixRQUFRLEVBQUUsb0JBQW9COzRCQUM5QixLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRDs0QkFDRSxPQUFPLEVBQUVBLHNCQUFpQjs0QkFDMUIsUUFBUSxFQUFFLGdCQUFnQjs0QkFDMUIsS0FBSyxFQUFFLElBQUk7eUJBQ1o7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFQSxzQkFBaUI7NEJBQzFCLFFBQVEsRUFBRSxjQUFjOzRCQUN4QixLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRDs0QkFDRSxPQUFPLEVBQUUsNEJBQTRCOzRCQUNyQyxRQUFRLEVBQUUsdUJBQXVCOzRCQUNqQyxLQUFLLEVBQUUsSUFBSTt5QkFDWjt3QkFDRCxlQUFlO3dCQUNmLFlBQVk7d0JBQ1osVUFBVTtxQkFDWDtpQkFDRixDQUFDO2FBQ0g7O29CQXZKRkMsYUFBUSxTQUFDO3dCQUNSLE9BQU8sRUFBRTs0QkFDUEMscUJBQWdCO3lCQUNqQjtxQkFDRjs7MkJBekNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==