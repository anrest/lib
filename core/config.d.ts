import { InjectionToken } from '@angular/core';
export interface ApiConfig {
    responseType?: string;
    baseUrl: string;
    authUrl?: string;
    cache?: any;
    tokenProvider?: any;
    authService?: any;
    cacheTTL?: number;
    excludedUrls?: string[];
    defaultHeaders?: {
        name: string;
        value: string;
        append?: boolean;
    }[];
}
export declare const AnRestConfig: InjectionToken<ApiConfig>;
