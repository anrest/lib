import { CollectionInfo } from './data-info.service';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
export declare class Collection<T> extends Array<T> {
    private service;
    private info;
    static merge<T>(c1: Collection<T>, c2: Collection<T>): Collection<T>;
    constructor(service: ApiService, info: CollectionInfo, ...items: T[]);
    readonly total: number;
    readonly page: number;
    readonly pageTotal: number;
    first(): Observable<T[]>;
    previous(): Observable<T[]>;
    next(): Observable<T[]>;
    last(): Observable<T[]>;
    firstPath(): string;
    previousPath(): string;
    nextPath(): string;
    lastPath(): string;
    loadMore(): Observable<T[]>;
    hasMore(): boolean;
    hasNext(): boolean;
    hasPrevious(): boolean;
    hasFirst(): boolean;
    hasLast(): boolean;
}
