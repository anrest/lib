import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiConfig } from './config';
export declare class ReferenceInterceptor implements HttpInterceptor {
    protected readonly config: ApiConfig;
    constructor(config: ApiConfig);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
    private isReferenceRequest(request);
    private createReferenceResponse(request);
}
