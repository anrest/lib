export declare class EntityInfo {
    private _path;
    constructor(_path: any);
    readonly path: any;
}
export declare class CollectionInfo {
    private _path;
    total: number;
    page: number;
    lastPage: number;
    first: string;
    previous: string;
    next: string;
    last: string;
    alias: string;
    type: any;
    constructor(_path: any);
    readonly path: any;
}
export declare class DataInfoService {
    get(object: any): any;
    set(object: any, info: any): void;
    getForCollection(object: any): CollectionInfo;
    getForEntity(object: any): EntityInfo;
    setForCollection(object: any, info: CollectionInfo): void;
    setForEntity(object: any, info: EntityInfo): void;
}
