import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiConfig } from './config';
import { DataNormalizer } from '../response/data-normalizer';
import { ApiProcessor } from './api.processor';
export declare class ApiInterceptor implements HttpInterceptor {
    protected readonly config: ApiConfig;
    private readonly normalizers;
    private readonly processor;
    constructor(config: ApiConfig, normalizers: DataNormalizer[], processor: ApiProcessor);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
