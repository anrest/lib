import { Injector } from '@angular/core';
import { ResponseNode } from '../response/response';
import { DataInfoService } from './data-info.service';
import { ObjectCollector } from './object-collector';
export declare class ApiProcessor {
    private injector;
    private info;
    private collector;
    constructor(injector: Injector, info: DataInfoService, collector: ObjectCollector);
    process(node: ResponseNode, method: string): any;
    private processCollection(node);
    private processObject(node);
}
