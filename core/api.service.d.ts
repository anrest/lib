import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataInfoService } from './data-info.service';
import { EventsService } from '../events';
import { ApiConfig } from './config';
import { ServiceMetaInfo } from '../meta';
export declare class ApiService {
    protected http: HttpClient;
    protected events: EventsService;
    protected infoService: DataInfoService;
    protected config: ApiConfig;
    protected meta: ServiceMetaInfo;
    constructor(http: HttpClient, events: EventsService, infoService: DataInfoService, config: ApiConfig);
    getList(filter?: HttpParams | {
        [index: string]: string | number | boolean;
    }): Observable<any>;
    get(id: number | string): Observable<any>;
    reload(entity: any): Observable<any>;
    getReference(id: number | string): Observable<any>;
    doGet(path: string, filter?: HttpParams | {
        [index: string]: string | number | boolean;
    }): Observable<Object>;
    save(entity: any): Observable<any>;
    remove(entity: any): Observable<any>;
}
