import { DataInfoService } from './data-info.service';
export declare class ObjectCollector {
    private info;
    map: Map<string, any>;
    constructor(info: DataInfoService);
    set(data: any): void;
    remove(data: any): void;
    get(id: string): any;
    has(id: string): boolean;
    clear(): void;
}
