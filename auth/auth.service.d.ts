import { Observable } from 'rxjs';
import { Auth } from './auth';
export declare abstract class AuthService implements Auth {
    abstract getInfo(): any;
    abstract getToken(): string;
    abstract isSignedIn(): boolean;
    abstract remove(): any;
    abstract setToken(token: string): any;
    abstract signIn(username: string, password: string): Observable<any>;
}
