import { Observable } from 'rxjs';
export interface Auth {
    getToken(): string;
    setToken(token: string): any;
    remove(): any;
    isSignedIn(): boolean;
    getInfo(): any;
    signIn(username: string, password: string): Observable<any>;
}
