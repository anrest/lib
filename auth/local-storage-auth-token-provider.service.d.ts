import { AuthToken } from './auth-token';
export declare class LocalStorageAuthTokenProviderService implements AuthToken {
    private tokenName;
    get(): string;
    set(token: string): void;
    remove(): void;
    isSet(): boolean;
}
