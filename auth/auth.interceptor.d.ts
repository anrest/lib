import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { ApiConfig } from '../core/config';
export declare class AuthInterceptor implements HttpInterceptor {
    auth: AuthService;
    protected config: ApiConfig;
    constructor(auth: AuthService, config: ApiConfig);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
