import { AuthToken } from './auth-token';
export declare abstract class AuthTokenProviderService implements AuthToken {
    abstract get(): string;
    abstract set(token: string): any;
    abstract remove(): any;
    abstract isSet(): boolean;
}
