import { HttpClient } from '@angular/common/http';
import { ApiConfig } from '../core/config';
import { Observable } from 'rxjs';
import { Auth } from './auth';
import { AuthTokenProviderService } from './auth-token-provider.service';
export declare class JwtAuthService implements Auth {
    private http;
    private token;
    protected config: ApiConfig;
    constructor(http: HttpClient, token: AuthTokenProviderService, config: ApiConfig);
    getToken(): string;
    setToken(token: string): void;
    remove(): void;
    isSignedIn(): boolean;
    getInfo(): any;
    signIn(username: string, password: string): Observable<any>;
}
