export interface AuthToken {
    get(): string;
    set(token: string): any;
    isSet(): boolean;
    remove(): any;
}
