import { HttpResponse } from '@angular/common/http';
import { InjectionToken } from '@angular/core';
import { ResponseNode } from './response';
export interface DataNormalizer {
    normalize(response: HttpResponse<any>, type?: string): ResponseNode;
    supports(type: string): boolean;
}
export declare let ANREST_HTTP_DATA_NORMALIZERS: InjectionToken<DataNormalizer[]>;
