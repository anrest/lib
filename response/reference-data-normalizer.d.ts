import { DataNormalizer } from './data-normalizer';
import { HttpResponse } from '@angular/common/http';
import { ResponseNode } from './response';
export declare class ReferenceDataNormalizer implements DataNormalizer {
    normalize(response: HttpResponse<any>, type?: string): ResponseNode;
    supports(type: string): boolean;
}
