import { InjectionToken, Injectable, Inject, Optional, Injector, NgModule } from '@angular/core';
import { __values, __extends, __spread, __read } from 'tslib';
import { map, tap, debounceTime, distinctUntilChanged, mergeMap, share, shareReplay } from 'rxjs/operators';
import { HttpHeaders, HttpClient, HttpParams, HttpResponse, HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { of, BehaviorSubject, combineLatest } from 'rxjs';
import { call } from 'jwt-decode';
import { plural } from 'pluralize';
import 'reflect-metadata';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ AnRestConfig = new InjectionToken('anrest.api_config');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var EntityInfo = /** @class */ (function () {
    function EntityInfo(_path) {
        this._path = _path;
    }
    Object.defineProperty(EntityInfo.prototype, "path", {
        get: /**
         * @return {?}
         */
        function () {
            return this._path;
        },
        enumerable: true,
        configurable: true
    });
    return EntityInfo;
}());
var CollectionInfo = /** @class */ (function () {
    function CollectionInfo(_path) {
        this._path = _path;
    }
    Object.defineProperty(CollectionInfo.prototype, "path", {
        get: /**
         * @return {?}
         */
        function () {
            return this._path;
        },
        enumerable: true,
        configurable: true
    });
    return CollectionInfo;
}());
var DataInfoService = /** @class */ (function () {
    function DataInfoService() {
    }
    /**
     * @param {?} object
     * @return {?}
     */
    DataInfoService.prototype.get = /**
     * @param {?} object
     * @return {?}
     */
    function (object) {
        return Reflect.getMetadata('anrest:info', object);
    };
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    DataInfoService.prototype.set = /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    function (object, info) {
        Reflect.defineMetadata('anrest:info', info, object);
    };
    /**
     * @param {?} object
     * @return {?}
     */
    DataInfoService.prototype.getForCollection = /**
     * @param {?} object
     * @return {?}
     */
    function (object) {
        return /** @type {?} */ (this.get(object));
    };
    /**
     * @param {?} object
     * @return {?}
     */
    DataInfoService.prototype.getForEntity = /**
     * @param {?} object
     * @return {?}
     */
    function (object) {
        return /** @type {?} */ (this.get(object));
    };
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    DataInfoService.prototype.setForCollection = /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    function (object, info) {
        this.set(object, info);
    };
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    DataInfoService.prototype.setForEntity = /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    function (object, info) {
        this.set(object, info);
    };
    DataInfoService.decorators = [
        { type: Injectable },
    ];
    return DataInfoService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @template T
 */
var  /**
 * @template T
 */
Collection = /** @class */ (function (_super) {
    __extends(Collection, _super);
    function Collection(service, info) {
        var items = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            items[_i - 2] = arguments[_i];
        }
        var _this = _super.apply(this, __spread(items)) || this;
        _this.service = service;
        _this.info = info;
        Object.setPrototypeOf(_this, Collection.prototype);
        return _this;
    }
    /**
     * @template T
     * @param {?} c1
     * @param {?} c2
     * @return {?}
     */
    Collection.merge = /**
     * @template T
     * @param {?} c1
     * @param {?} c2
     * @return {?}
     */
    function (c1, c2) {
        var /** @type {?} */ info = new CollectionInfo(c1.info.path);
        info.type = c1.info.type;
        info.next = c2.info.next;
        info.previous = c1.info.previous;
        info.first = c1.info.first;
        info.last = c1.info.last;
        info.total = c1.info.total;
        info.page = c1.info.page;
        info.lastPage = c1.info.lastPage;
        var /** @type {?} */ c = new Collection(c1.service, info);
        c1.forEach(function (v) { return c.push(v); });
        c2.forEach(function (v) { return c.push(v); });
        return c;
    };
    Object.defineProperty(Collection.prototype, "total", {
        get: /**
         * @return {?}
         */
        function () {
            return this.info.total;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Collection.prototype, "page", {
        get: /**
         * @return {?}
         */
        function () {
            return this.info.page;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Collection.prototype, "pageTotal", {
        get: /**
         * @return {?}
         */
        function () {
            return this.info.lastPage;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    Collection.prototype.first = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.first));
    };
    /**
     * @return {?}
     */
    Collection.prototype.previous = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.previous));
    };
    /**
     * @return {?}
     */
    Collection.prototype.next = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.next));
    };
    /**
     * @return {?}
     */
    Collection.prototype.last = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.last));
    };
    /**
     * @return {?}
     */
    Collection.prototype.firstPath = /**
     * @return {?}
     */
    function () {
        return this.info.first;
    };
    /**
     * @return {?}
     */
    Collection.prototype.previousPath = /**
     * @return {?}
     */
    function () {
        return this.info.previous;
    };
    /**
     * @return {?}
     */
    Collection.prototype.nextPath = /**
     * @return {?}
     */
    function () {
        return this.info.next;
    };
    /**
     * @return {?}
     */
    Collection.prototype.lastPath = /**
     * @return {?}
     */
    function () {
        return this.info.last;
    };
    /**
     * @return {?}
     */
    Collection.prototype.loadMore = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return this.next().pipe(map(function (result) { return Collection.merge(_this, result); }));
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasMore = /**
     * @return {?}
     */
    function () {
        return this.hasNext();
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasNext = /**
     * @return {?}
     */
    function () {
        return !!this.info.next;
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasPrevious = /**
     * @return {?}
     */
    function () {
        return !!this.info.previous;
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasFirst = /**
     * @return {?}
     */
    function () {
        return this.hasPrevious();
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasLast = /**
     * @return {?}
     */
    function () {
        return this.hasNext();
    };
    return Collection;
}(Array));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var BaseEvent = /** @class */ (function () {
    function BaseEvent(data, type) {
        this._data = data;
        this.type = type;
    }
    /**
     * @return {?}
     */
    BaseEvent.prototype.data = /**
     * @return {?}
     */
    function () {
        return this._data;
    };
    /**
     * @return {?}
     */
    BaseEvent.prototype.entity = /**
     * @return {?}
     */
    function () {
        return this.type || (Array.isArray(this._data) ? this._data[0].constructor : this._data.constructor);
    };
    return BaseEvent;
}());
var BeforeGetEvent = /** @class */ (function (_super) {
    __extends(BeforeGetEvent, _super);
    function BeforeGetEvent(path, type, filter) {
        var _this = _super.call(this, path, type) || this;
        _this._filter = filter;
        return _this;
    }
    /**
     * @return {?}
     */
    BeforeGetEvent.prototype.entity = /**
     * @return {?}
     */
    function () {
        return this.type;
    };
    /**
     * @return {?}
     */
    BeforeGetEvent.prototype.filter = /**
     * @return {?}
     */
    function () {
        return this._filter;
    };
    return BeforeGetEvent;
}(BaseEvent));
var AfterGetEvent = /** @class */ (function (_super) {
    __extends(AfterGetEvent, _super);
    function AfterGetEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AfterGetEvent;
}(BaseEvent));
var BeforeSaveEvent = /** @class */ (function (_super) {
    __extends(BeforeSaveEvent, _super);
    function BeforeSaveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return BeforeSaveEvent;
}(BaseEvent));
var AfterSaveEvent = /** @class */ (function (_super) {
    __extends(AfterSaveEvent, _super);
    function AfterSaveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AfterSaveEvent;
}(BaseEvent));
var BeforeRemoveEvent = /** @class */ (function (_super) {
    __extends(BeforeRemoveEvent, _super);
    function BeforeRemoveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return BeforeRemoveEvent;
}(BaseEvent));
var AfterRemoveEvent = /** @class */ (function (_super) {
    __extends(AfterRemoveEvent, _super);
    function AfterRemoveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AfterRemoveEvent;
}(BaseEvent));
var /** @type {?} */ ANREST_HTTP_EVENT_LISTENERS = new InjectionToken('anrest.http_event_listeners');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MetaInfo = /** @class */ (function () {
    function MetaInfo(_type) {
        this._type = _type;
    }
    Object.defineProperty(MetaInfo.prototype, "type", {
        get: /**
         * @return {?}
         */
        function () {
            return this._type;
        },
        enumerable: true,
        configurable: true
    });
    return MetaInfo;
}());
var PropertyMetaInfo = /** @class */ (function (_super) {
    __extends(PropertyMetaInfo, _super);
    function PropertyMetaInfo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return PropertyMetaInfo;
}(MetaInfo));
var EventListenerMetaInfo = /** @class */ (function (_super) {
    __extends(EventListenerMetaInfo, _super);
    function EventListenerMetaInfo() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.events = [];
        return _this;
    }
    return EventListenerMetaInfo;
}(MetaInfo));
var EntityMetaInfo = /** @class */ (function (_super) {
    __extends(EntityMetaInfo, _super);
    function EntityMetaInfo() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.id = 'id';
        _this.headers = [];
        _this.properties = {};
        _this.subresources = {};
        return _this;
    }
    /**
     * @param {?=} object
     * @return {?}
     */
    EntityMetaInfo.prototype.getHeaders = /**
     * @param {?=} object
     * @return {?}
     */
    function (object) {
        var /** @type {?} */ headers = new HttpHeaders();
        headers = headers.set('X-AnRest-Type', this.name);
        try {
            for (var _a = __values(this.headers), _b = _a.next(); !_b.done; _b = _a.next()) {
                var header = _b.value;
                if (!header.dynamic || object) {
                    headers = headers[header.append ? 'append' : 'set'](header.name, header.value.call(object));
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return headers;
        var e_1, _c;
    };
    /**
     * @return {?}
     */
    EntityMetaInfo.prototype.getPropertiesForSave = /**
     * @return {?}
     */
    function () {
        var /** @type {?} */ included = [];
        for (var /** @type {?} */ property in this.properties) {
            if (!this.properties[property].excludeWhenSaving) {
                included.push(property);
            }
        }
        return included;
    };
    return EntityMetaInfo;
}(MetaInfo));
var ServiceMetaInfo = /** @class */ (function (_super) {
    __extends(ServiceMetaInfo, _super);
    function ServiceMetaInfo(type, _entityMeta) {
        var _this = _super.call(this, type) || this;
        _this._entityMeta = _entityMeta;
        return _this;
    }
    Object.defineProperty(ServiceMetaInfo.prototype, "entityMeta", {
        get: /**
         * @return {?}
         */
        function () {
            return this._entityMeta;
        },
        enumerable: true,
        configurable: true
    });
    return ServiceMetaInfo;
}(MetaInfo));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MetaService = /** @class */ (function () {
    function MetaService() {
    }
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.get = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return MetaService.map.get(key);
    };
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.getByName = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        try {
            for (var _a = __values(MetaService.map.values()), _b = _a.next(); !_b.done; _b = _a.next()) {
                var meta = _b.value;
                if (meta instanceof EntityMetaInfo && meta.name === key) {
                    return meta;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        var e_1, _c;
    };
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.getOrCreateForEntity = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        var /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EntityMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    };
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.getOrCreateForEventListener = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        var /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EventListenerMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    };
    /**
     * @param {?} key
     * @param {?} property
     * @param {?=} type
     * @return {?}
     */
    MetaService.getOrCreateForProperty = /**
     * @param {?} key
     * @param {?} property
     * @param {?=} type
     * @return {?}
     */
    function (key, property, type) {
        var /** @type {?} */ entityMeta = MetaService.getOrCreateForEntity(key.constructor);
        if (!entityMeta.properties[property]) {
            entityMeta.properties[property] = new PropertyMetaInfo(type || Reflect.getMetadata('design:type', key, property));
        }
        return entityMeta.properties[property];
    };
    /**
     * @param {?} key
     * @param {?} entity
     * @return {?}
     */
    MetaService.getOrCreateForHttpService = /**
     * @param {?} key
     * @param {?} entity
     * @return {?}
     */
    function (key, entity) {
        var /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new ServiceMetaInfo(key, MetaService.getOrCreateForEntity(entity));
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    };
    /**
     * @param {?} meta
     * @return {?}
     */
    MetaService.set = /**
     * @param {?} meta
     * @return {?}
     */
    function (meta) {
        MetaService.map.set(meta.type, meta);
    };
    MetaService.map = new Map();
    return MetaService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var EventsService = /** @class */ (function () {
    function EventsService(handlers) {
        this.handlers = handlers;
        if (!Array.isArray(this.handlers)) {
            this.handlers = [];
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    EventsService.prototype.handle = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.handlers.forEach(function (handler) {
            var /** @type {?} */ meta = MetaService.getOrCreateForEventListener(handler.constructor);
            try {
                for (var _a = __values(meta.events), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var eventMeta = _b.value;
                    if (eventMeta.type === event.constructor && eventMeta.entity === event.entity()) {
                        handler.handle(event);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
            var e_1, _c;
        });
        return event;
    };
    EventsService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    EventsService.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_EVENT_LISTENERS,] },] },
    ]; };
    return EventsService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ApiService = /** @class */ (function () {
    function ApiService(http, events, infoService, config) {
        this.http = http;
        this.events = events;
        this.infoService = infoService;
        this.config = config;
        this.meta = MetaService.get(this.constructor);
    }
    /**
     * @param {?=} filter
     * @return {?}
     */
    ApiService.prototype.getList = /**
     * @param {?=} filter
     * @return {?}
     */
    function (filter) {
        return this.doGet(this.meta.path, filter);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ApiService.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.doGet(this.meta.path + '/' + id);
    };
    /**
     * @param {?} entity
     * @return {?}
     */
    ApiService.prototype.reload = /**
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return this.doGet(this.infoService.getForEntity(entity).path);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ApiService.prototype.getReference = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.doGet('&' + this.meta.path + '/' + id + '#' + this.meta.entityMeta.name);
    };
    /**
     * @param {?} path
     * @param {?=} filter
     * @return {?}
     */
    ApiService.prototype.doGet = /**
     * @param {?} path
     * @param {?=} filter
     * @return {?}
     */
    function (path, filter) {
        var _this = this;
        var /** @type {?} */ f;
        if (filter instanceof HttpParams) {
            f = filter;
        }
        else {
            f = new HttpParams();
            for (var /** @type {?} */ key in filter) {
                f = f.set(key, typeof filter[key] === 'boolean' ? (filter[key] ? 'true' : 'false') : String(filter[key]));
            }
        }
        this.events.handle(new BeforeGetEvent(path, this.meta.entityMeta.type, f));
        return this.http.get(this.config.baseUrl + path, { headers: this.meta.entityMeta.getHeaders(), params: f }).pipe(tap(function (data) { return _this.events.handle(new AfterGetEvent(data, _this.meta.entityMeta.type)); }));
    };
    /**
     * @param {?} entity
     * @return {?}
     */
    ApiService.prototype.save = /**
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        var _this = this;
        this.events.handle(new BeforeSaveEvent(entity));
        var /** @type {?} */ body = {};
        if (this.meta.entityMeta.body) {
            body = this.meta.entityMeta.body(entity);
        }
        else {
            this.meta.entityMeta.getPropertiesForSave().forEach(function (property) { return body[property] = entity[property]; });
        }
        var /** @type {?} */ meta = this.infoService.getForEntity(entity);
        return this.http[meta ? 'put' : 'post'](meta ? this.config.baseUrl + meta.path : this.config.baseUrl + this.meta.path, body, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap(function (data) {
            meta = _this.infoService.getForEntity(data);
            if (!meta) {
                _this.infoService.setForEntity(entity, meta);
            }
        }), tap(function () { return _this.events.handle(new AfterSaveEvent(entity)); }));
    };
    /**
     * @param {?} entity
     * @return {?}
     */
    ApiService.prototype.remove = /**
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        var _this = this;
        this.events.handle(new BeforeRemoveEvent(entity));
        var /** @type {?} */ meta = this.infoService.getForEntity(entity);
        if (meta) {
            return this.http.delete(this.config.baseUrl + meta.path, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap(function () { return _this.events.handle(new AfterRemoveEvent(entity)); }));
        }
    };
    ApiService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ApiService.ctorParameters = function () { return [
        { type: HttpClient, },
        { type: EventsService, },
        { type: DataInfoService, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return ApiService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ReferenceInterceptor = /** @class */ (function () {
    function ReferenceInterceptor(config) {
        this.config = config;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    ReferenceInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl || !this.isReferenceRequest(request)) {
            return next.handle(request);
        }
        return this.createReferenceResponse(request);
    };
    /**
     * @param {?} request
     * @return {?}
     */
    ReferenceInterceptor.prototype.isReferenceRequest = /**
     * @param {?} request
     * @return {?}
     */
    function (request) {
        return request.method === 'GET' && request.url.slice(this.config.baseUrl.length).indexOf('&') === 0;
    };
    /**
     * @param {?} request
     * @return {?}
     */
    ReferenceInterceptor.prototype.createReferenceResponse = /**
     * @param {?} request
     * @return {?}
     */
    function (request) {
        var /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        return of(new HttpResponse({
            body: { 'path': path.substring(1, path.indexOf('#')), 'meta': MetaService.getByName(path.substring(path.indexOf('#') + 1)) },
            status: 200,
            url: request.url
        })).pipe(map(function (response) {
            if (response instanceof HttpResponse) {
                return response.clone({
                    headers: response.headers.set('Content-Type', '@reference')
                });
            }
        }));
    };
    ReferenceInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ReferenceInterceptor.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return ReferenceInterceptor;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var /** @type {?} */ ANREST_HTTP_DATA_NORMALIZERS = new InjectionToken('anrest.http_data_normalizers');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ResponseNode = /** @class */ (function () {
    function ResponseNode() {
    }
    return ResponseNode;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ObjectCollector = /** @class */ (function () {
    function ObjectCollector(info) {
        this.info = info;
        this.map = new Map();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    ObjectCollector.prototype.set = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.map.set(this.info.get(data).path, data);
    };
    /**
     * @param {?} data
     * @return {?}
     */
    ObjectCollector.prototype.remove = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var /** @type {?} */ info = this.info.get(data);
        if (info) {
            data = info.path;
        }
        this.map.delete(data);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ObjectCollector.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.map.get(id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ObjectCollector.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.map.has(id);
    };
    /**
     * @return {?}
     */
    ObjectCollector.prototype.clear = /**
     * @return {?}
     */
    function () {
        this.map.clear();
    };
    ObjectCollector.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ObjectCollector.ctorParameters = function () { return [
        { type: DataInfoService, },
    ]; };
    return ObjectCollector;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ApiProcessor = /** @class */ (function () {
    function ApiProcessor(injector, info, collector) {
        this.injector = injector;
        this.info = info;
        this.collector = collector;
    }
    /**
     * @param {?} node
     * @param {?} method
     * @return {?}
     */
    ApiProcessor.prototype.process = /**
     * @param {?} node
     * @param {?} method
     * @return {?}
     */
    function (node, method) {
        return node.collection ? this.processCollection(node) : this.processObject(node);
    };
    /**
     * @param {?} node
     * @return {?}
     */
    ApiProcessor.prototype.processCollection = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var /** @type {?} */ data;
        if (node.info.path !== undefined) {
            data = this.collector.get(node.info.path) || new Collection(this.injector.get(node.meta.service), node.info);
        }
        else {
            data = new Collection(this.injector.get(node.meta.service), node.info);
        }
        data.length = 0;
        try {
            for (var _a = __values(node.data), _b = _a.next(); !_b.done; _b = _a.next()) {
                var object = _b.value;
                data.push(this.processObject(object));
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.info.setForCollection(data, node.info);
        this.collector.set(data);
        return data;
        var e_1, _c;
    };
    /**
     * @param {?} node
     * @return {?}
     */
    ApiProcessor.prototype.processObject = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node.meta instanceof EntityMetaInfo) {
            var /** @type {?} */ object = this.collector.get(node.info.path) || new node.meta.type();
            for (var /** @type {?} */ property in node.meta.properties) {
                if (object.hasOwnProperty(property) && !node.data.hasOwnProperty(property)) {
                    continue;
                }
                if (node.data[property] instanceof ResponseNode) {
                    object[property] = node.meta.properties[property].isCollection ?
                        this.processCollection(node.data[property]) :
                        this.processObject(node.data[property]);
                }
                else {
                    var /** @type {?} */ type = node.meta.properties[property].type;
                    switch (type) {
                        case Number:
                            object[property] = Number(node.data[property]);
                            break;
                        case Date:
                            object[property] = new Date(node.data[property]);
                            break;
                        case String:
                        default:
                            object[property] = node.data[property];
                            break;
                    }
                }
            }
            for (var /** @type {?} */ property in node.meta.subresources) {
                var /** @type {?} */ service = this.injector.get(node.meta.subresources[property].meta.service);
                object[property] = service.doGet.bind(service, node.info.path + node.meta.subresources[property].path);
            }
            this.info.setForEntity(object, node.info);
            this.collector.set(object);
            return object;
        }
        else {
            return node.data;
        }
    };
    ApiProcessor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ApiProcessor.ctorParameters = function () { return [
        { type: Injector, },
        { type: DataInfoService, },
        { type: ObjectCollector, },
    ]; };
    return ApiProcessor;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ApiInterceptor = /** @class */ (function () {
    function ApiInterceptor(config, normalizers, processor) {
        this.config = config;
        this.normalizers = normalizers;
        this.processor = processor;
        if (!Array.isArray(this.normalizers)) {
            this.normalizers = [];
        }
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    ApiInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        var _this = this;
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        var /** @type {?} */ type;
        var /** @type {?} */ headers = request.headers;
        if (headers.has('X-AnRest-Type')) {
            type = headers.get('X-AnRest-Type');
            headers = headers.delete('x-AnRest-Type');
        }
        if (this.config.defaultHeaders) {
            try {
                for (var _a = __values(this.config.defaultHeaders), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var header = _b.value;
                    if (!headers.has(header.name) || header.append) {
                        headers = headers[header.append ? 'append' : 'set'](header.name, header.value);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        request = request.clone({
            headers: headers
        });
        return next.handle(request).pipe(map(function (response) {
            if (response instanceof HttpResponse) {
                var /** @type {?} */ contentType = response.headers.get('Content-Type');
                try {
                    for (var _a = __values(_this.normalizers), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var normalizer = _b.value;
                        if (normalizer.supports(contentType)) {
                            return response.clone({ body: _this.processor.process(normalizer.normalize(response, type), request.method) });
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            return response;
            var e_2, _c;
        }));
        var e_1, _c;
    };
    ApiInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ApiInterceptor.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
        { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_DATA_NORMALIZERS,] },] },
        { type: ApiProcessor, },
    ]; };
    return ApiInterceptor;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var ResponseCache = /** @class */ (function () {
    function ResponseCache() {
    }
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.replace = /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    function (data, id) {
        this.remove(id);
        this.add(data, id);
    };
    ResponseCache.decorators = [
        { type: Injectable },
    ];
    return ResponseCache;
}());
var /** @type {?} */ ANREST_CACHE_SERVICES = new InjectionToken('anrest.cache_services');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var NoResponseCache = /** @class */ (function (_super) {
    __extends(NoResponseCache, _super);
    function NoResponseCache() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return false;
    };
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    NoResponseCache.prototype.add = /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    function (id, data) {
    };
    /**
     * @return {?}
     */
    NoResponseCache.prototype.clear = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.remove = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
    };
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    NoResponseCache.prototype.addAlias = /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    function (id, alias) {
    };
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.replace = /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    function (data, id) {
    };
    NoResponseCache.decorators = [
        { type: Injectable },
    ];
    return NoResponseCache;
}(ResponseCache));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CacheInterceptor = /** @class */ (function () {
    function CacheInterceptor(config, info, cacheServices) {
        this.config = config;
        this.info = info;
        this.cacheServices = cacheServices;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    CacheInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        var _this = this;
        var /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        var /** @type {?} */ cacheType = MetaService.getByName(request.headers.get('X-AnRest-Type')).cache || this.config.cache || NoResponseCache;
        var /** @type {?} */ cache;
        try {
            for (var _a = __values(this.cacheServices), _b = _a.next(); !_b.done; _b = _a.next()) {
                var cacheService = _b.value;
                if (cacheService instanceof cacheType) {
                    cache = cacheService;
                    break;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        if (request.method !== 'GET') {
            cache.remove(path);
            return next.handle(request);
        }
        if (cache.has(path)) {
            return of(new HttpResponse({
                body: cache.get(path),
                status: 200,
                url: request.url
            }));
        }
        return next.handle(request).pipe(tap(function (response) {
            if (response.body) {
                var /** @type {?} */ info = _this.info.get(response.body);
                if (info) {
                    cache.add(info.path, response.body);
                    if (info && info.alias) {
                        cache.addAlias(info.path, info.alias);
                    }
                }
            }
        }));
        var e_1, _c;
    };
    CacheInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    CacheInterceptor.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
        { type: DataInfoService, },
        { type: Array, decorators: [{ type: Inject, args: [ANREST_CACHE_SERVICES,] },] },
    ]; };
    return CacheInterceptor;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MemoryResponseCache = /** @class */ (function (_super) {
    __extends(MemoryResponseCache, _super);
    function MemoryResponseCache(collector, config) {
        var _this = _super.call(this) || this;
        _this.collector = collector;
        _this.config = config;
        _this.times = {};
        _this.aliases = {};
        return _this;
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    MemoryResponseCache.prototype.add = /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    function (id, data) {
        this.times[id] = Date.now();
    };
    /**
     * @return {?}
     */
    MemoryResponseCache.prototype.clear = /**
     * @return {?}
     */
    function () {
        this.collector.clear();
    };
    /**
     * @param {?} id
     * @return {?}
     */
    MemoryResponseCache.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.collector.get(id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    MemoryResponseCache.prototype.remove = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        this.collector.remove(id);
    };
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    MemoryResponseCache.prototype.addAlias = /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    function (id, alias) {
        this.aliases[alias] = id;
    };
    /**
     * @param {?} id
     * @return {?}
     */
    MemoryResponseCache.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var /** @type {?} */ time = this.aliases[id] ? this.times[this.aliases[id]] : this.times[id];
        return time && (Date.now() <= this.config.cacheTTL + time);
    };
    MemoryResponseCache.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    MemoryResponseCache.ctorParameters = function () { return [
        { type: ObjectCollector, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return MemoryResponseCache;
}(ResponseCache));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ReferenceDataNormalizer = /** @class */ (function () {
    function ReferenceDataNormalizer() {
    }
    /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    ReferenceDataNormalizer.prototype.normalize = /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    function (response, type) {
        var /** @type {?} */ r = new ResponseNode();
        r.collection = false;
        r.info = new EntityInfo(response.body.path);
        r.meta = response.body.meta;
        r.data = {};
        return r;
    };
    /**
     * @param {?} type
     * @return {?}
     */
    ReferenceDataNormalizer.prototype.supports = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type === '@reference';
    };
    return ReferenceDataNormalizer;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var LdJsonDataNormalizer = /** @class */ (function () {
    function LdJsonDataNormalizer() {
    }
    /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    LdJsonDataNormalizer.prototype.normalize = /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    function (response, type) {
        return this.process(response.body);
    };
    /**
     * @param {?} type
     * @return {?}
     */
    LdJsonDataNormalizer.prototype.supports = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type.split(';')[0] === 'application/ld+json';
    };
    /**
     * @param {?} data
     * @param {?=} meta
     * @param {?=} collection
     * @return {?}
     */
    LdJsonDataNormalizer.prototype.process = /**
     * @param {?} data
     * @param {?=} meta
     * @param {?=} collection
     * @return {?}
     */
    function (data, meta, collection) {
        var /** @type {?} */ r = new ResponseNode();
        r.collection = collection || data['@type'] === 'hydra:Collection';
        var /** @type {?} */ path = data['@id'];
        r.meta = meta || MetaService.getByName(r.collection ? /\/contexts\/(\w+)/g.exec(data['@context'])[1] : data['@type']);
        if (r.collection) {
            var /** @type {?} */ items = data['hydra:member'] || data;
            r.info = new CollectionInfo(data['hydra:view'] ? data['hydra:view']['@id'] : path);
            r.info.type = r.meta.name;
            if (data['hydra:view']) {
                r.info.first = data['hydra:view']['hydra:first'];
                r.info.previous = data['hydra:view']['hydra:previous'];
                r.info.next = data['hydra:view']['hydra:next'];
                r.info.last = data['hydra:view']['hydra:last'];
                if (r.info.path === r.info.first) {
                    r.info.alias = data['@id'];
                }
            }
            r.info.total = data['hydra:totalItems'] || items.length;
            var /** @type {?} */ regexPage = /page=(\d+)/g;
            var /** @type {?} */ matches = void 0;
            matches = regexPage.exec(r.info.path);
            r.info.page = matches ? Number(matches[1]) : 1;
            matches = regexPage.exec(r.info.last);
            r.info.lastPage = matches ? Number(matches[1]) : 1;
            r.data = [];
            try {
                for (var items_1 = __values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                    var object = items_1_1.value;
                    r.data.push(this.process(object));
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (items_1_1 && !items_1_1.done && (_a = items_1.return)) _a.call(items_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            r.info = new EntityInfo(path);
            r.data = {};
            if (r.meta === undefined && Array.isArray(data)) {
                r.data = data;
            }
            else {
                for (var /** @type {?} */ key in data) {
                    if (key.indexOf('@') === 0 || key.indexOf('hydra:') === 0) {
                        continue;
                    }
                    var /** @type {?} */ propertyMeta = r.meta.properties[key];
                    r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                        this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
                }
            }
        }
        return r;
        var e_1, _a;
    };
    return LdJsonDataNormalizer;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var JsonDataNormalizer = /** @class */ (function () {
    function JsonDataNormalizer() {
    }
    /**
     * @param {?} response
     * @param {?} type
     * @return {?}
     */
    JsonDataNormalizer.prototype.normalize = /**
     * @param {?} response
     * @param {?} type
     * @return {?}
     */
    function (response, type) {
        return this.process(response.body, MetaService.getByName(type), Array.isArray(response.body), response.headers);
    };
    /**
     * @param {?} type
     * @return {?}
     */
    JsonDataNormalizer.prototype.supports = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type.split(';')[0] === 'application/json';
    };
    /**
     * @param {?} data
     * @param {?} meta
     * @param {?} collection
     * @param {?=} headers
     * @return {?}
     */
    JsonDataNormalizer.prototype.process = /**
     * @param {?} data
     * @param {?} meta
     * @param {?} collection
     * @param {?=} headers
     * @return {?}
     */
    function (data, meta, collection, headers) {
        var /** @type {?} */ r = new ResponseNode();
        r.collection = collection;
        var /** @type {?} */ collectionPath = MetaService.get(meta.service) ? MetaService.get(meta.service).path : undefined;
        var /** @type {?} */ path = collectionPath ? (collection ? collectionPath : collectionPath + '/' + data[meta.id]) : undefined;
        r.meta = meta;
        if (r.collection) {
            r.info = new CollectionInfo(headers.get('X-Current-Page') || path);
            r.info.type = r.meta.name;
            r.info.first = headers.get('X-First-Page') || undefined;
            r.info.previous = headers.get('X-Prev-Page') || undefined;
            r.info.next = headers.get('X-Next-Page') || undefined;
            r.info.last = headers.get('X-Last-Page') || undefined;
            if (r.info.path === r.info.first) {
                r.info.alias = path;
            }
            r.info.total = headers.get('X-Total') || data.length;
            var /** @type {?} */ regexPage = /page=(\d+)/g;
            var /** @type {?} */ matches = void 0;
            matches = regexPage.exec(r.info.path);
            r.info.page = matches ? Number(matches[1]) : 1;
            matches = regexPage.exec(r.info.last);
            r.info.lastPage = matches ? Number(matches[1]) : 1;
            r.data = [];
            try {
                for (var data_1 = __values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                    var object = data_1_1.value;
                    r.data.push(this.process(object, r.meta, false));
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (data_1_1 && !data_1_1.done && (_a = data_1.return)) _a.call(data_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            r.info = new EntityInfo(path);
            r.data = {};
            for (var /** @type {?} */ key in data) {
                var /** @type {?} */ propertyMeta = r.meta.properties[key];
                r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                    this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
            }
        }
        return r;
        var e_1, _a;
    };
    return JsonDataNormalizer;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var LocalStorageResponseCache = /** @class */ (function (_super) {
    __extends(LocalStorageResponseCache, _super);
    function LocalStorageResponseCache(collector, processor, info, config) {
        var _this = _super.call(this) || this;
        _this.collector = collector;
        _this.processor = processor;
        _this.info = info;
        _this.config = config;
        _this.tokenPrefix = 'anrest:cache:';
        var /** @type {?} */ timestampPrefix = _this.tokenPrefix + 'timestamp:';
        Object.keys(localStorage).filter(function (e) { return e.indexOf(timestampPrefix) === 0; }).forEach(function (k) {
            var /** @type {?} */ id = k.substring(timestampPrefix.length);
            var /** @type {?} */ timestamp = _this.getTimestamp(id);
            if (!timestamp || timestamp + _this.config.cacheTTL < Date.now()) {
                _this.remove(id);
            }
        });
        return _this;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        if (this.has(id)) {
            return this.processor.process(this.getNode(id), 'GET');
        }
    };
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    LocalStorageResponseCache.prototype.add = /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    function (id, data) {
        this.doAdd(id, data, true);
    };
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    LocalStorageResponseCache.prototype.addAlias = /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    function (id, alias) {
        localStorage.setItem(this.tokenPrefix + 'alias:' + alias, id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        var /** @type {?} */ complete = JSON.parse(localStorage.getItem(this.tokenPrefix + 'complete:' + id));
        var /** @type {?} */ timestamp = this.getTimestamp(id);
        return !!timestamp && complete && timestamp + this.config.cacheTTL >= Date.now();
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.remove = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        localStorage.removeItem(this.tokenPrefix + id);
        localStorage.removeItem(this.tokenPrefix + 'type:' + id);
        localStorage.removeItem(this.tokenPrefix + 'collection:' + id);
        localStorage.removeItem(this.tokenPrefix + 'complete:' + id);
        localStorage.removeItem(this.tokenPrefix + 'references:' + id);
        this.removeTimestamp(id);
    };
    /**
     * @return {?}
     */
    LocalStorageResponseCache.prototype.clear = /**
     * @return {?}
     */
    function () {
        var _this = this;
        Object.keys(localStorage).filter(function (e) { return e.indexOf(_this.tokenPrefix) === 0; }).forEach(function (k) {
            localStorage.removeItem(k);
        });
    };
    /**
     * @param {?} id
     * @param {?} data
     * @param {?} complete
     * @return {?}
     */
    LocalStorageResponseCache.prototype.doAdd = /**
     * @param {?} id
     * @param {?} data
     * @param {?} complete
     * @return {?}
     */
    function (id, data, complete) {
        var /** @type {?} */ info = this.info.get(data);
        var /** @type {?} */ values;
        if (info instanceof CollectionInfo) {
            values = [];
            try {
                for (var data_1 = __values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                    var item = data_1_1.value;
                    var /** @type {?} */ itemPath = this.info.get(item).path;
                    values.push(itemPath);
                    this.doAdd(itemPath, item, false);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (data_1_1 && !data_1_1.done && (_a = data_1.return)) _a.call(data_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            localStorage.setItem(this.tokenPrefix + 'collection:' + id, JSON.stringify(info));
        }
        else {
            var /** @type {?} */ references = {};
            values = {};
            for (var /** @type {?} */ key in data) {
                var /** @type {?} */ itemInfo = void 0;
                if (typeof data[key] === 'object') {
                    itemInfo = this.info.get(data[key]);
                }
                if (itemInfo) {
                    this.doAdd(itemInfo.path, data[key], false);
                    references[key] = itemInfo.path;
                }
                else {
                    values[key] = data[key];
                }
            }
            localStorage.setItem(this.tokenPrefix + 'type:' + id, MetaService.get(data.constructor).name);
            localStorage.setItem(this.tokenPrefix + 'references:' + id, JSON.stringify(references));
        }
        localStorage.setItem(this.tokenPrefix + 'complete:' + id, JSON.stringify(complete));
        localStorage.setItem(this.tokenPrefix + id, JSON.stringify(values));
        this.setTimestamp(id);
        var e_1, _a;
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.getNode = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + id));
        var /** @type {?} */ collectionInfo = this.getCollectionInfo(id);
        var /** @type {?} */ response = new ResponseNode();
        if (collectionInfo) {
            response.collection = true;
            response.info = collectionInfo;
            response.meta = MetaService.getByName(collectionInfo.type);
            response.data = [];
            try {
                for (var data_2 = __values(data), data_2_1 = data_2.next(); !data_2_1.done; data_2_1 = data_2.next()) {
                    var itemId = data_2_1.value;
                    response.data.push(this.getNode(itemId));
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (data_2_1 && !data_2_1.done && (_a = data_2.return)) _a.call(data_2);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        else {
            response.collection = false;
            response.info = new EntityInfo(id);
            response.meta = MetaService.getByName(localStorage.getItem(this.tokenPrefix + 'type:' + id));
            response.data = data;
            var /** @type {?} */ references = JSON.parse(localStorage.getItem(this.tokenPrefix + 'references:' + id));
            if (references) {
                for (var /** @type {?} */ key in references) {
                    response.data[key] = this.getNode(references[key]);
                }
            }
        }
        return response;
        var e_2, _a;
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.getCollectionInfo = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + 'collection:' + id));
        if (data) {
            var /** @type {?} */ info = new CollectionInfo(id);
            for (var /** @type {?} */ key in data) {
                if (key !== '_path') {
                    info[key] = data[key];
                }
            }
            return info;
        }
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.setTimestamp = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        localStorage.setItem(this.tokenPrefix + 'timestamp:' + id, String(Date.now()));
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.getTimestamp = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return Number(localStorage.getItem(this.tokenPrefix + 'timestamp:' + id));
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.removeTimestamp = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return localStorage.removeItem(this.tokenPrefix + 'timestamp:' + id);
    };
    LocalStorageResponseCache.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    LocalStorageResponseCache.ctorParameters = function () { return [
        { type: ObjectCollector, },
        { type: ApiProcessor, },
        { type: DataInfoService, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return LocalStorageResponseCache;
}(ResponseCache));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var  /**
 * @abstract
 */
AuthService = /** @class */ (function () {
    function AuthService() {
    }
    return AuthService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var  /**
 * @abstract
 */
AuthTokenProviderService = /** @class */ (function () {
    function AuthTokenProviderService() {
    }
    return AuthTokenProviderService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var LocalStorageAuthTokenProviderService = /** @class */ (function () {
    function LocalStorageAuthTokenProviderService() {
        this.tokenName = 'anrest:auth_token';
    }
    /**
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.get = /**
     * @return {?}
     */
    function () {
        return localStorage.getItem(this.tokenName);
    };
    /**
     * @param {?} token
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.set = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        localStorage.setItem(this.tokenName, token);
    };
    /**
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.remove = /**
     * @return {?}
     */
    function () {
        localStorage.removeItem(this.tokenName);
    };
    /**
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.isSet = /**
     * @return {?}
     */
    function () {
        return localStorage.getItem(this.tokenName) != null;
    };
    LocalStorageAuthTokenProviderService.decorators = [
        { type: Injectable },
    ];
    return LocalStorageAuthTokenProviderService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(auth, config) {
        this.auth = auth;
        this.config = config;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    AuthInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        if (!this.config.authUrl || request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl) {
            return next.handle(request);
        }
        if (this.auth.isSignedIn()) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + this.auth.getToken()
                }
            });
        }
        return next.handle(request);
    };
    AuthInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AuthInterceptor.ctorParameters = function () { return [
        { type: AuthService, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return AuthInterceptor;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var JwtAuthService = /** @class */ (function () {
    function JwtAuthService(http, token, config) {
        this.http = http;
        this.token = token;
        this.config = config;
    }
    /**
     * @return {?}
     */
    JwtAuthService.prototype.getToken = /**
     * @return {?}
     */
    function () {
        return this.token.get();
    };
    /**
     * @param {?} token
     * @return {?}
     */
    JwtAuthService.prototype.setToken = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        this.token.set(token);
    };
    /**
     * @return {?}
     */
    JwtAuthService.prototype.remove = /**
     * @return {?}
     */
    function () {
        this.token.remove();
    };
    /**
     * @return {?}
     */
    JwtAuthService.prototype.isSignedIn = /**
     * @return {?}
     */
    function () {
        return this.token.isSet();
    };
    /**
     * @return {?}
     */
    JwtAuthService.prototype.getInfo = /**
     * @return {?}
     */
    function () {
        return call(this.getToken());
    };
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    JwtAuthService.prototype.signIn = /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    function (username, password) {
        var _this = this;
        return this.http.post(this.config.authUrl, {
            login: username,
            password: password
        }).pipe(tap(function (data) {
            _this.setToken(data.token);
        }));
    };
    JwtAuthService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    JwtAuthService.ctorParameters = function () { return [
        { type: HttpClient, },
        { type: AuthTokenProviderService, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return JwtAuthService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?=} info
 * @return {?}
 */
function Resource(info) {
    if (info === void 0) { info = {}; }
    return function (target) {
        var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        meta.name = info.name || target.name;
        meta.cache = info.cache;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?=} info
 * @return {?}
 */
function Property(info) {
    if (info === void 0) { info = {}; }
    return function (target, key) {
        var /** @type {?} */ meta = MetaService.getOrCreateForProperty(target, key, info.type);
        meta.isCollection = info.collection || false;
        meta.excludeWhenSaving = info.excludeWhenSaving || false;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} entity
 * @param {?=} path
 * @return {?}
 */
function HttpService(entity, path) {
    return function (target) {
        var /** @type {?} */ meta = MetaService.getOrCreateForHttpService(target, entity);
        meta.path = path || '/' + plural(entity.name).replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase();
        meta.entityMeta.service = target;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} headerName
 * @param {?=} append
 * @return {?}
 */
function Header(headerName, append) {
    return function (target, key, value) {
        if (value === void 0) { value = undefined; }
        MetaService.getOrCreateForEntity(target.constructor).headers.push({
            name: headerName,
            value: value ? value.value : function () { return this[key]; },
            append: append || false,
            dynamic: true
        });
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} headers
 * @return {?}
 */
function Headers(headers) {
    return function (target) {
        var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        var _loop_1 = function (header) {
            meta.headers.push({
                name: header.name,
                value: function () { return header.value; },
                append: header.append || false,
                dynamic: false
            });
        };
        try {
            for (var headers_1 = __values(headers), headers_1_1 = headers_1.next(); !headers_1_1.done; headers_1_1 = headers_1.next()) {
                var header = headers_1_1.value;
                _loop_1(header);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (headers_1_1 && !headers_1_1.done && (_a = headers_1.return)) _a.call(headers_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        var e_1, _a;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @return {?}
 */
function Body() {
    return function (target, key, value) {
        if (key === void 0) { key = undefined; }
        if (value === void 0) { value = undefined; }
        MetaService.getOrCreateForEntity(target.constructor).body = function (object) {
            return (value ? value.value : function () { return this[key]; }).call(object);
        };
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} type
 * @param {?=} path
 * @return {?}
 */
function Subresource(type, path) {
    return function (target, key, value) {
        MetaService.getOrCreateForEntity(target.constructor).subresources[key] = {
            meta: MetaService.getOrCreateForEntity(type),
            path: path || '/' + key.replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase()
        };
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @return {?}
 */
function Id() {
    return function (target, key) {
        var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target.constructor);
        meta.id = key;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var  /**
 * @abstract
 */
Loader = /** @class */ (function () {
    function Loader(service, waitFor, replay) {
        if (waitFor === void 0) { waitFor = 0; }
        if (replay === void 0) { replay = 0; }
        var _this = this;
        this.service = service;
        this.fields = {};
        this.loading = false;
        this.observable = combineLatest(this.getSubjects())
            .pipe(replay ? shareReplay(replay) : share(), debounceTime(waitFor), distinctUntilChanged(function (d1, d2) { return _this.checkDistinct(d1, d2); }), map(function (values) { return _this.mapParams(values); }), mergeMap(function (data) {
            _this.loading = true;
            return _this.getServiceObservable(data).pipe(tap(function (v) {
                _this.lastData = v;
                _this.loading = false;
            }));
        }));
    }
    /**
     * @param {?} name
     * @return {?}
     */
    Loader.prototype.field = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.fields[name];
    };
    Object.defineProperty(Loader.prototype, "data", {
        get: /**
         * @return {?}
         */
        function () {
            return this.observable;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Loader.prototype, "isLoading", {
        get: /**
         * @return {?}
         */
        function () {
            return this.loading;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} __0
     * @return {?}
     */
    Loader.prototype.getServiceObservable = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = __read(_a, 1), data = _b[0];
        return this.service.getList(data);
    };
    /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    Loader.prototype.checkDistinct = /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    function (d1, d2) {
        var /** @type {?} */ offset = d1.length - this.getAvailableFields().length;
        for (var /** @type {?} */ i = d1.length; i >= 0; i--) {
            if ((i < offset && d2[i]) || (i >= offset && d1[i] !== d2[i])) {
                if (i >= offset) {
                    d2.fill(undefined, 0, offset);
                }
                return false;
            }
        }
        return true;
    };
    /**
     * @param {?} values
     * @return {?}
     */
    Loader.prototype.mapParams = /**
     * @param {?} values
     * @return {?}
     */
    function (values) {
        var /** @type {?} */ fields = this.getAvailableFields();
        var /** @type {?} */ offset = values.length - fields.length;
        var /** @type {?} */ data = {};
        values.slice(offset).forEach(function (v, i) {
            if (v !== undefined && v !== null && v !== '') {
                if (typeof v === 'boolean') {
                    v = v ? 'true' : 'false';
                }
                data[fields[i]] = String(v);
            }
        });
        return values.slice(0, offset).concat([data]);
    };
    /**
     * @return {?}
     */
    Loader.prototype.getSubjects = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return this.getAvailableFields().map(function (name) {
            return _this.fields[name] = new BehaviorSubject(undefined);
        });
    };
    return Loader;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var  /**
 * @abstract
 */
InfiniteScrollLoader = /** @class */ (function (_super) {
    __extends(InfiniteScrollLoader, _super);
    function InfiniteScrollLoader() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @return {?}
     */
    InfiniteScrollLoader.prototype.loadMore = /**
     * @return {?}
     */
    function () {
        this.loadMoreSubject.next(true);
    };
    /**
     * @return {?}
     */
    InfiniteScrollLoader.prototype.hasMore = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasMore();
    };
    /**
     * @param {?} __0
     * @return {?}
     */
    InfiniteScrollLoader.prototype.getServiceObservable = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = __read(_a, 2), loadMore = _b[0], data = _b[1];
        return loadMore ? this.lastData.loadMore() : _super.prototype.getServiceObservable.call(this, [data]);
    };
    /**
     * @return {?}
     */
    InfiniteScrollLoader.prototype.getSubjects = /**
     * @return {?}
     */
    function () {
        this.loadMoreSubject = new BehaviorSubject(false);
        var /** @type {?} */ subjects = _super.prototype.getSubjects.call(this);
        subjects.unshift(this.loadMoreSubject);
        return subjects;
    };
    return InfiniteScrollLoader;
}(Loader));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var  /**
 * @abstract
 */
PageLoader = /** @class */ (function (_super) {
    __extends(PageLoader, _super);
    function PageLoader() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @return {?}
     */
    PageLoader.prototype.first = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasFirst()) {
            return;
        }
        this.pageSubject.next(this.lastData.firstPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.previous = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasPrevious()) {
            return;
        }
        this.pageSubject.next(this.lastData.previousPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.next = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasNext()) {
            return;
        }
        this.pageSubject.next(this.lastData.nextPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.last = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasLast()) {
            return;
        }
        this.pageSubject.next(this.lastData.lastPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasFirst = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasFirst();
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasPrevious = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasPrevious();
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasNext = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasNext();
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasLast = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasLast();
    };
    /**
     * @param {?} __0
     * @return {?}
     */
    PageLoader.prototype.getServiceObservable = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = __read(_a, 2), page = _b[0], data = _b[1];
        return page ? this.service.doGet(page, data) : _super.prototype.getServiceObservable.call(this, [data]);
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.getSubjects = /**
     * @return {?}
     */
    function () {
        this.pageSubject = new BehaviorSubject(undefined);
        var /** @type {?} */ subjects = _super.prototype.getSubjects.call(this);
        subjects.unshift(this.pageSubject);
        return subjects;
    };
    return PageLoader;
}(Loader));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AnRestModule = /** @class */ (function () {
    function AnRestModule() {
    }
    /**
     * @param {?=} config
     * @return {?}
     */
    AnRestModule.config = /**
     * @param {?=} config
     * @return {?}
     */
    function (config) {
        return {
            ngModule: AnRestModule,
            providers: [
                {
                    provide: AnRestConfig,
                    useValue: config,
                },
                EventsService,
                DataInfoService,
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: LocalStorageResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: MemoryResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: NoResponseCache,
                    multi: true
                },
                {
                    provide: ResponseCache,
                    useClass: config.cache || NoResponseCache
                },
                {
                    provide: AuthTokenProviderService,
                    useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                },
                {
                    provide: AuthService,
                    useClass: config.authService || JwtAuthService
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ReferenceInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CacheInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ApiInterceptor,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: ReferenceDataNormalizer,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: LdJsonDataNormalizer,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: JsonDataNormalizer,
                    multi: true
                },
                ObjectCollector,
                ApiProcessor,
                ApiService
            ]
        };
    };
    /**
     * @param {?=} config
     * @return {?}
     */
    AnRestModule.configWithoutNormalizers = /**
     * @param {?=} config
     * @return {?}
     */
    function (config) {
        return {
            ngModule: AnRestModule,
            providers: [
                {
                    provide: AnRestConfig,
                    useValue: config,
                },
                EventsService,
                DataInfoService,
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: LocalStorageResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: MemoryResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: NoResponseCache,
                    multi: true
                },
                {
                    provide: ResponseCache,
                    useClass: config.cache || NoResponseCache
                },
                {
                    provide: AuthTokenProviderService,
                    useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                },
                {
                    provide: AuthService,
                    useClass: config.authService || JwtAuthService
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ReferenceInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CacheInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ApiInterceptor,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: ReferenceDataNormalizer,
                    multi: true
                },
                ObjectCollector,
                ApiProcessor,
                ApiService
            ]
        };
    };
    AnRestModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        HttpClientModule,
                    ]
                },] },
    ];
    return AnRestModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { AnRestConfig, Collection, ApiService, EntityInfo, CollectionInfo, DataInfoService, ReferenceInterceptor, ApiInterceptor, ApiProcessor, ObjectCollector, CacheInterceptor, ResponseCache, ANREST_CACHE_SERVICES, NoResponseCache, MemoryResponseCache, LocalStorageResponseCache, AuthService, AuthTokenProviderService, LocalStorageAuthTokenProviderService, AuthInterceptor, JwtAuthService, Resource, Property, HttpService, Header, Headers, Body, Subresource, Id, EventsService, BaseEvent, BeforeGetEvent, AfterGetEvent, BeforeSaveEvent, AfterSaveEvent, BeforeRemoveEvent, AfterRemoveEvent, ANREST_HTTP_EVENT_LISTENERS, MetaService, MetaInfo, PropertyMetaInfo, EventListenerMetaInfo, EntityMetaInfo, ServiceMetaInfo, ResponseNode, ANREST_HTTP_DATA_NORMALIZERS, ReferenceDataNormalizer, LdJsonDataNormalizer, JsonDataNormalizer, Loader, InfiniteScrollLoader, PageLoader, AnRestModule, AuthInterceptor as ɵm, AuthService as ɵk, AuthTokenProviderService as ɵi, JwtAuthService as ɵl, LocalStorageAuthTokenProviderService as ɵj, ANREST_CACHE_SERVICES as ɵd, CacheInterceptor as ɵo, LocalStorageResponseCache as ɵe, MemoryResponseCache as ɵf, NoResponseCache as ɵg, ResponseCache as ɵh, AnRestConfig as ɵa, ApiInterceptor as ɵp, ApiProcessor as ɵv, ApiService as ɵw, DataInfoService as ɵc, ObjectCollector as ɵu, ReferenceInterceptor as ɵn, EventsService as ɵb, ANREST_HTTP_DATA_NORMALIZERS as ɵq, JsonDataNormalizer as ɵt, LdJsonDataNormalizer as ɵs, ReferenceDataNormalizer as ɵr };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5yZXN0LWxpYi5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vQGFucmVzdC9saWIvY29yZS9jb25maWcudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvZGF0YS1pbmZvLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvY29sbGVjdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvZXZlbnRzL2V2ZW50cy50cyIsIm5nOi8vQGFucmVzdC9saWIvbWV0YS9tZXRhLWluZm8udHMiLCJuZzovL0BhbnJlc3QvbGliL21ldGEvbWV0YS1zZXJ2aWNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9ldmVudHMvZXZlbnRzLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvYXBpLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvcmVmZXJlbmNlLmludGVyY2VwdG9yLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9yZXNwb25zZS9kYXRhLW5vcm1hbGl6ZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3Jlc3BvbnNlL3Jlc3BvbnNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jb3JlL29iamVjdC1jb2xsZWN0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvYXBpLnByb2Nlc3Nvci50cyIsIm5nOi8vQGFucmVzdC9saWIvY29yZS9hcGkuaW50ZXJjZXB0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NhY2hlL3Jlc3BvbnNlLWNhY2hlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jYWNoZS9uby1yZXNwb25zZS1jYWNoZS50cyIsIm5nOi8vQGFucmVzdC9saWIvY2FjaGUvY2FjaGUuaW50ZXJjZXB0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NhY2hlL21lbW9yeS1yZXNwb25zZS1jYWNoZS50cyIsIm5nOi8vQGFucmVzdC9saWIvcmVzcG9uc2UvcmVmZXJlbmNlLWRhdGEtbm9ybWFsaXplci50cyIsIm5nOi8vQGFucmVzdC9saWIvcmVzcG9uc2UvbGQtanNvbi1kYXRhLW5vcm1hbGl6ZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3Jlc3BvbnNlL2pzb24tZGF0YS1ub3JtYWxpemVyLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jYWNoZS9sb2NhbC1zdG9yYWdlLXJlc3BvbnNlLWNhY2hlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hdXRoL2F1dGguc2VydmljZS50cyIsIm5nOi8vQGFucmVzdC9saWIvYXV0aC9hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2F1dGgvbG9jYWwtc3RvcmFnZS1hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2F1dGgvYXV0aC5pbnRlcmNlcHRvci50cyIsIm5nOi8vQGFucmVzdC9saWIvYXV0aC9qd3QtYXV0aC5zZXJ2aWNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9yZXNvdXJjZS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9wcm9wZXJ0eS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9odHRwLXNlcnZpY2UuYW5ub3RhdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvYW5ub3RhdGlvbnMvaGVhZGVyLmFubm90YXRpb24udHMiLCJuZzovL0BhbnJlc3QvbGliL2Fubm90YXRpb25zL2hlYWRlcnMuYW5ub3RhdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvYW5ub3RhdGlvbnMvYm9keS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9zdWJyZXNvdXJjZS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9pZC5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi91dGlscy9sb2FkZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3V0aWxzL2luZmluaXRlLXNjcm9sbC1sb2FkZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3V0aWxzL3BhZ2UtbG9hZGVyLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbnJlc3QubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQXBpQ29uZmlnIHtcbiAgcmVzcG9uc2VUeXBlPzogc3RyaW5nO1xuICBiYXNlVXJsOiBzdHJpbmc7XG4gIGF1dGhVcmw/OiBzdHJpbmc7XG4gIGNhY2hlPzogYW55O1xuICB0b2tlblByb3ZpZGVyPzogYW55O1xuICBhdXRoU2VydmljZT86IGFueTtcbiAgY2FjaGVUVEw/OiBudW1iZXI7XG4gIGV4Y2x1ZGVkVXJscz86IHN0cmluZ1tdO1xuICBkZWZhdWx0SGVhZGVycz86IHsgbmFtZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBhcHBlbmQ/OiBib29sZWFufVtdO1xufVxuXG5leHBvcnQgY29uc3QgQW5SZXN0Q29uZmlnID0gbmV3IEluamVjdGlvblRva2VuPEFwaUNvbmZpZz4oJ2FucmVzdC5hcGlfY29uZmlnJyk7XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBjbGFzcyBFbnRpdHlJbmZvIHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9wYXRoKSB7fVxuXG4gIGdldCBwYXRoKCkge1xuICAgIHJldHVybiB0aGlzLl9wYXRoO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBDb2xsZWN0aW9uSW5mbyB7XG4gIHB1YmxpYyB0b3RhbDogbnVtYmVyO1xuICBwdWJsaWMgcGFnZTogbnVtYmVyO1xuICBwdWJsaWMgbGFzdFBhZ2U6IG51bWJlcjtcbiAgcHVibGljIGZpcnN0OiBzdHJpbmc7XG4gIHB1YmxpYyBwcmV2aW91czogc3RyaW5nO1xuICBwdWJsaWMgbmV4dDogc3RyaW5nO1xuICBwdWJsaWMgbGFzdDogc3RyaW5nO1xuICBwdWJsaWMgYWxpYXM6IHN0cmluZztcbiAgcHVibGljIHR5cGU6IGFueTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9wYXRoKSB7fVxuXG4gIGdldCBwYXRoKCkge1xuICAgIHJldHVybiB0aGlzLl9wYXRoO1xuICB9XG59XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBEYXRhSW5mb1NlcnZpY2Uge1xuXG4gIGdldChvYmplY3QpOiBhbnkge1xuICAgIHJldHVybiBSZWZsZWN0LmdldE1ldGFkYXRhKCdhbnJlc3Q6aW5mbycsIG9iamVjdCk7XG4gIH1cblxuICBzZXQob2JqZWN0LCBpbmZvOiBhbnkpIHtcbiAgICBSZWZsZWN0LmRlZmluZU1ldGFkYXRhKCdhbnJlc3Q6aW5mbycsIGluZm8sIG9iamVjdCk7XG4gIH1cblxuICBnZXRGb3JDb2xsZWN0aW9uKG9iamVjdCk6IENvbGxlY3Rpb25JbmZvIHtcbiAgICByZXR1cm4gPENvbGxlY3Rpb25JbmZvPnRoaXMuZ2V0KG9iamVjdCk7XG4gIH1cblxuICBnZXRGb3JFbnRpdHkob2JqZWN0KTogRW50aXR5SW5mbyB7XG4gICAgcmV0dXJuIDxFbnRpdHlJbmZvPnRoaXMuZ2V0KG9iamVjdCk7XG4gIH1cblxuICBzZXRGb3JDb2xsZWN0aW9uKG9iamVjdCwgaW5mbzogQ29sbGVjdGlvbkluZm8pIHtcbiAgICB0aGlzLnNldChvYmplY3QsIGluZm8pO1xuICB9XG5cbiAgc2V0Rm9yRW50aXR5KG9iamVjdCwgaW5mbzogRW50aXR5SW5mbykge1xuICAgIHRoaXMuc2V0KG9iamVjdCwgaW5mbyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IENvbGxlY3Rpb25JbmZvIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQgeyBBcGlTZXJ2aWNlIH0gZnJvbSAnLi9hcGkuc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmV4cG9ydCBjbGFzcyBDb2xsZWN0aW9uPFQ+IGV4dGVuZHMgQXJyYXk8VD4ge1xuXG4gIHN0YXRpYyBtZXJnZTxUPihjMTogQ29sbGVjdGlvbjxUPiwgYzI6IENvbGxlY3Rpb248VD4pOiBDb2xsZWN0aW9uPFQ+IHtcbiAgICBjb25zdCBpbmZvID0gbmV3IENvbGxlY3Rpb25JbmZvKGMxLmluZm8ucGF0aCk7XG4gICAgaW5mby50eXBlID0gYzEuaW5mby50eXBlO1xuICAgIGluZm8ubmV4dCA9IGMyLmluZm8ubmV4dDtcbiAgICBpbmZvLnByZXZpb3VzID0gYzEuaW5mby5wcmV2aW91cztcbiAgICBpbmZvLmZpcnN0ID0gYzEuaW5mby5maXJzdDtcbiAgICBpbmZvLmxhc3QgPSBjMS5pbmZvLmxhc3Q7XG4gICAgaW5mby50b3RhbCA9IGMxLmluZm8udG90YWw7XG4gICAgaW5mby5wYWdlID0gYzEuaW5mby5wYWdlO1xuICAgIGluZm8ubGFzdFBhZ2UgPSBjMS5pbmZvLmxhc3RQYWdlO1xuICAgIGNvbnN0IGMgPSBuZXcgQ29sbGVjdGlvbjxUPihjMS5zZXJ2aWNlLCBpbmZvKTtcbiAgICBjMS5mb3JFYWNoKCh2KSA9PiBjLnB1c2godikpO1xuICAgIGMyLmZvckVhY2goKHYpID0+IGMucHVzaCh2KSk7XG4gICAgcmV0dXJuIGM7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZpY2U6IEFwaVNlcnZpY2UsIHByaXZhdGUgaW5mbzogQ29sbGVjdGlvbkluZm8sIC4uLml0ZW1zOiBUW10pIHtcbiAgICBzdXBlciguLi5pdGVtcyk7XG4gICAgT2JqZWN0LnNldFByb3RvdHlwZU9mKHRoaXMsIENvbGxlY3Rpb24ucHJvdG90eXBlKTtcbiAgfVxuXG4gIGdldCB0b3RhbCgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLmluZm8udG90YWw7XG4gIH1cblxuICBnZXQgcGFnZSgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLmluZm8ucGFnZTtcbiAgfVxuXG4gIGdldCBwYWdlVG90YWwoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLmxhc3RQYWdlO1xuICB9XG5cbiAgZmlyc3QoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gPE9ic2VydmFibGU8VFtdPj50aGlzLnNlcnZpY2UuZG9HZXQodGhpcy5pbmZvLmZpcnN0KTtcbiAgfVxuXG4gIHByZXZpb3VzKCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5wcmV2aW91cyk7XG4gIH1cblxuICBuZXh0KCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5uZXh0KTtcbiAgfVxuXG4gIGxhc3QoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gPE9ic2VydmFibGU8VFtdPj50aGlzLnNlcnZpY2UuZG9HZXQodGhpcy5pbmZvLmxhc3QpO1xuICB9XG5cbiAgZmlyc3RQYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5maXJzdDtcbiAgfVxuXG4gIHByZXZpb3VzUGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ucHJldmlvdXM7XG4gIH1cblxuICBuZXh0UGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ubmV4dDtcbiAgfVxuXG4gIGxhc3RQYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5sYXN0O1xuICB9XG5cbiAgbG9hZE1vcmUoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gdGhpcy5uZXh0KCkucGlwZShcbiAgICAgIG1hcCgocmVzdWx0OiBDb2xsZWN0aW9uPFQ+KSA9PiBDb2xsZWN0aW9uLm1lcmdlPFQ+KHRoaXMsIHJlc3VsdCkpXG4gICAgKTtcbiAgfVxuXG4gIGhhc01vcmUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuaGFzTmV4dCgpO1xuICB9XG5cbiAgaGFzTmV4dCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gISF0aGlzLmluZm8ubmV4dDtcbiAgfVxuXG4gIGhhc1ByZXZpb3VzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAhIXRoaXMuaW5mby5wcmV2aW91cztcbiAgfVxuXG4gIGhhc0ZpcnN0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc1ByZXZpb3VzKCk7XG4gIH1cblxuICBoYXNMYXN0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc05leHQoKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRXZlbnQge1xuICBlbnRpdHkoKTogRnVuY3Rpb247XG4gIGRhdGEoKTogYW55O1xufVxuXG5leHBvcnQgY2xhc3MgQmFzZUV2ZW50IGltcGxlbWVudHMgRXZlbnQge1xuICBfZGF0YT86IGFueTtcbiAgcHJvdGVjdGVkIHJlYWRvbmx5IHR5cGU6IEZ1bmN0aW9uO1xuXG4gIGNvbnN0cnVjdG9yKGRhdGE6IGFueSwgdHlwZT86IEZ1bmN0aW9uKSB7XG4gICAgdGhpcy5fZGF0YSA9IGRhdGE7XG4gICAgdGhpcy50eXBlID0gdHlwZTtcbiAgfVxuXG4gIGRhdGEoKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5fZGF0YTtcbiAgfVxuXG4gIGVudGl0eSgpOiBGdW5jdGlvbiB7XG4gICAgcmV0dXJuIHRoaXMudHlwZSB8fCAoQXJyYXkuaXNBcnJheSh0aGlzLl9kYXRhKSA/IHRoaXMuX2RhdGFbMF0uY29uc3RydWN0b3IgOiB0aGlzLl9kYXRhLmNvbnN0cnVjdG9yKTtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgQmVmb3JlR2V0RXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge1xuICBwcml2YXRlIHJlYWRvbmx5IF9maWx0ZXI6IEh0dHBQYXJhbXM7XG5cbiAgY29uc3RydWN0b3IocGF0aDogc3RyaW5nLCB0eXBlPzogRnVuY3Rpb24sIGZpbHRlcj86IEh0dHBQYXJhbXMpIHtcbiAgICBzdXBlcihwYXRoLCB0eXBlKTtcbiAgICB0aGlzLl9maWx0ZXIgPSBmaWx0ZXI7XG4gIH1cblxuICBlbnRpdHkoKTogRnVuY3Rpb24ge1xuICAgIHJldHVybiB0aGlzLnR5cGU7XG4gIH1cblxuICBmaWx0ZXIoKTogSHR0cFBhcmFtcyB7XG4gICAgcmV0dXJuIHRoaXMuX2ZpbHRlcjtcbiAgfVxufVxuZXhwb3J0IGNsYXNzIEFmdGVyR2V0RXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBCZWZvcmVTYXZlRXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBBZnRlclNhdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuZXhwb3J0IGNsYXNzIEJlZm9yZVJlbW92ZUV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHt9XG5leHBvcnQgY2xhc3MgQWZ0ZXJSZW1vdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuXG5leHBvcnQgaW50ZXJmYWNlIEV2ZW50TGlzdGVuZXIge1xuICBoYW5kbGUoZXZlbnQ6IEV2ZW50KTtcbn1cblxuZXhwb3J0IGxldCBBTlJFU1RfSFRUUF9FVkVOVF9MSVNURU5FUlMgPSBuZXcgSW5qZWN0aW9uVG9rZW48RXZlbnRMaXN0ZW5lcltdPignYW5yZXN0Lmh0dHBfZXZlbnRfbGlzdGVuZXJzJyk7XG4iLCJpbXBvcnQgeyBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuZXhwb3J0IGNsYXNzIE1ldGFJbmZvIHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF90eXBlOiBGdW5jdGlvbikge31cblxuICBnZXQgdHlwZSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLl90eXBlO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBQcm9wZXJ0eU1ldGFJbmZvIGV4dGVuZHMgTWV0YUluZm8ge1xuICBwdWJsaWMgaXNDb2xsZWN0aW9uOiBib29sZWFuO1xuICBwdWJsaWMgZXhjbHVkZVdoZW5TYXZpbmc6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBjbGFzcyBFdmVudExpc3RlbmVyTWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBldmVudHM6IHsgZW50aXR5OiBGdW5jdGlvbiwgdHlwZTogRnVuY3Rpb24gfVtdID0gW107XG59XG5cbmV4cG9ydCBjbGFzcyBFbnRpdHlNZXRhSW5mbyBleHRlbmRzIE1ldGFJbmZvIHtcbiAgcHVibGljIG5hbWU6IHN0cmluZztcbiAgcHVibGljIGlkID0gJ2lkJztcbiAgcHVibGljIHNlcnZpY2U6IEZ1bmN0aW9uO1xuICBwdWJsaWMgaGVhZGVyczogeyBuYW1lOiBzdHJpbmcsIHZhbHVlOiAoKSA9PiBzdHJpbmcsIGFwcGVuZDogYm9vbGVhbiwgZHluYW1pYzogYm9vbGVhbiB9W10gPSBbXTtcbiAgcHVibGljIHByb3BlcnRpZXM6IHsgW2luZGV4OiBzdHJpbmddOiBQcm9wZXJ0eU1ldGFJbmZvIH0gPSB7fTtcbiAgcHVibGljIHN1YnJlc291cmNlczogeyBbaW5kZXg6IHN0cmluZ106IHsgbWV0YTogRW50aXR5TWV0YUluZm8sIHBhdGg6IHN0cmluZ30gfSA9IHt9O1xuICBwdWJsaWMgYm9keTogKG9iamVjdDogYW55KSA9PiBzdHJpbmc7XG4gIHB1YmxpYyBjYWNoZTogRnVuY3Rpb247XG5cbiAgcHVibGljIGdldEhlYWRlcnMob2JqZWN0PzogYW55KTogSHR0cEhlYWRlcnMge1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XG4gICAgaGVhZGVycyA9IGhlYWRlcnMuc2V0KCdYLUFuUmVzdC1UeXBlJywgdGhpcy5uYW1lKTtcbiAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiB0aGlzLmhlYWRlcnMpIHtcbiAgICAgIGlmICghaGVhZGVyLmR5bmFtaWMgfHwgb2JqZWN0KSB7XG4gICAgICAgIGhlYWRlcnMgPSBoZWFkZXJzW2hlYWRlci5hcHBlbmQgPyAnYXBwZW5kJyA6ICdzZXQnXShoZWFkZXIubmFtZSwgaGVhZGVyLnZhbHVlLmNhbGwob2JqZWN0KSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBoZWFkZXJzO1xuICB9XG5cbiAgcHVibGljIGdldFByb3BlcnRpZXNGb3JTYXZlKCkge1xuICAgIGNvbnN0IGluY2x1ZGVkID0gW107XG4gICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiB0aGlzLnByb3BlcnRpZXMpIHtcbiAgICAgIGlmICghdGhpcy5wcm9wZXJ0aWVzW3Byb3BlcnR5XS5leGNsdWRlV2hlblNhdmluZykge1xuICAgICAgICBpbmNsdWRlZC5wdXNoKHByb3BlcnR5KTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGluY2x1ZGVkO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTZXJ2aWNlTWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBwYXRoOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IodHlwZTogRnVuY3Rpb24sIHByaXZhdGUgX2VudGl0eU1ldGE6IEVudGl0eU1ldGFJbmZvKSB7XG4gICAgc3VwZXIodHlwZSk7XG4gIH1cblxuICBnZXQgZW50aXR5TWV0YSgpOiBFbnRpdHlNZXRhSW5mbyB7XG4gICAgcmV0dXJuIHRoaXMuX2VudGl0eU1ldGE7XG4gIH1cbn1cbiIsImltcG9ydCB7IEVudGl0eU1ldGFJbmZvLCBQcm9wZXJ0eU1ldGFJbmZvLCBNZXRhSW5mbywgU2VydmljZU1ldGFJbmZvLCBFdmVudExpc3RlbmVyTWV0YUluZm8gfSBmcm9tICcuL21ldGEtaW5mbyc7XG5cbmV4cG9ydCBjbGFzcyBNZXRhU2VydmljZSB7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgbWFwOiBNYXA8YW55LCBNZXRhSW5mbz4gPSBuZXcgTWFwPGFueSwgTWV0YUluZm8+KCk7XG5cbiAgc3RhdGljIGdldChrZXk6IGFueSk6IGFueSB7XG4gICAgcmV0dXJuIE1ldGFTZXJ2aWNlLm1hcC5nZXQoa2V5KTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRCeU5hbWUoa2V5OiBzdHJpbmcpOiBhbnkge1xuICAgIGZvciAoY29uc3QgbWV0YSBvZiBNZXRhU2VydmljZS5tYXAudmFsdWVzKCkpIHtcbiAgICAgIGlmIChtZXRhIGluc3RhbmNlb2YgRW50aXR5TWV0YUluZm8gJiYgbWV0YS5uYW1lID09PSBrZXkpIHtcbiAgICAgICAgcmV0dXJuIG1ldGE7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGdldE9yQ3JlYXRlRm9yRW50aXR5KGtleTogRnVuY3Rpb24pOiBFbnRpdHlNZXRhSW5mbyB7XG4gICAgbGV0IG1ldGEgPSBNZXRhU2VydmljZS5nZXQoa2V5KTtcbiAgICBpZiAoIW1ldGEpIHtcbiAgICAgIG1ldGEgPSBuZXcgRW50aXR5TWV0YUluZm8oa2V5KTtcbiAgICAgIE1ldGFTZXJ2aWNlLnNldChtZXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIDxFbnRpdHlNZXRhSW5mbz5tZXRhO1xuICB9XG5cbiAgc3RhdGljIGdldE9yQ3JlYXRlRm9yRXZlbnRMaXN0ZW5lcihrZXk6IEZ1bmN0aW9uKTogRXZlbnRMaXN0ZW5lck1ldGFJbmZvIHtcbiAgICBsZXQgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldChrZXkpO1xuICAgIGlmICghbWV0YSkge1xuICAgICAgbWV0YSA9IG5ldyBFdmVudExpc3RlbmVyTWV0YUluZm8oa2V5KTtcbiAgICAgIE1ldGFTZXJ2aWNlLnNldChtZXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIDxFdmVudExpc3RlbmVyTWV0YUluZm8+bWV0YTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRPckNyZWF0ZUZvclByb3BlcnR5KGtleTogYW55LCBwcm9wZXJ0eTogc3RyaW5nLCB0eXBlPzogYW55KTogUHJvcGVydHlNZXRhSW5mbyB7XG4gICAgY29uc3QgZW50aXR5TWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KGtleS5jb25zdHJ1Y3Rvcik7XG4gICAgaWYgKCFlbnRpdHlNZXRhLnByb3BlcnRpZXNbcHJvcGVydHldKSB7XG4gICAgICBlbnRpdHlNZXRhLnByb3BlcnRpZXNbcHJvcGVydHldID0gbmV3IFByb3BlcnR5TWV0YUluZm8odHlwZSB8fCBSZWZsZWN0LmdldE1ldGFkYXRhKCdkZXNpZ246dHlwZScsIGtleSwgcHJvcGVydHkpKTtcbiAgICB9XG4gICAgcmV0dXJuIGVudGl0eU1ldGEucHJvcGVydGllc1twcm9wZXJ0eV07XG4gIH1cblxuICBzdGF0aWMgZ2V0T3JDcmVhdGVGb3JIdHRwU2VydmljZShrZXk6IEZ1bmN0aW9uLCBlbnRpdHk6IEZ1bmN0aW9uKTogU2VydmljZU1ldGFJbmZvIHtcbiAgICBsZXQgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldChrZXkpO1xuICAgIGlmICghbWV0YSkge1xuICAgICAgbWV0YSA9IG5ldyBTZXJ2aWNlTWV0YUluZm8oa2V5LCBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eShlbnRpdHkpKTtcbiAgICAgIE1ldGFTZXJ2aWNlLnNldChtZXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIDxTZXJ2aWNlTWV0YUluZm8+bWV0YTtcbiAgfVxuXG4gIHN0YXRpYyBzZXQobWV0YTogTWV0YUluZm8pIHtcbiAgICBNZXRhU2VydmljZS5tYXAuc2V0KG1ldGEudHlwZSwgbWV0YSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIEFOUkVTVF9IVFRQX0VWRU5UX0xJU1RFTkVSUywgRXZlbnQsIEV2ZW50TGlzdGVuZXJcbn0gZnJvbSAnLi9ldmVudHMnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEV2ZW50c1NlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoQU5SRVNUX0hUVFBfRVZFTlRfTElTVEVORVJTKSBwcml2YXRlIHJlYWRvbmx5IGhhbmRsZXJzOiBFdmVudExpc3RlbmVyW11cbiAgKSB7XG4gICAgaWYgKCFBcnJheS5pc0FycmF5KHRoaXMuaGFuZGxlcnMpKSB7XG4gICAgICB0aGlzLmhhbmRsZXJzID0gW107XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGhhbmRsZShldmVudDogRXZlbnQpOiBFdmVudCB7XG4gICAgdGhpcy5oYW5kbGVycy5mb3JFYWNoKChoYW5kbGVyOiBFdmVudExpc3RlbmVyKSA9PiB7XG4gICAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFdmVudExpc3RlbmVyKGhhbmRsZXIuY29uc3RydWN0b3IpO1xuICAgICAgZm9yIChjb25zdCBldmVudE1ldGEgb2YgbWV0YS5ldmVudHMpIHtcbiAgICAgICAgaWYgKGV2ZW50TWV0YS50eXBlID09PSBldmVudC5jb25zdHJ1Y3RvciAmJiBldmVudE1ldGEuZW50aXR5ID09PSBldmVudC5lbnRpdHkoKSkge1xuICAgICAgICAgIGhhbmRsZXIuaGFuZGxlKGV2ZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBldmVudDtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQge1xuICBBZnRlckdldEV2ZW50LFxuICBBZnRlclJlbW92ZUV2ZW50LFxuICBBZnRlclNhdmVFdmVudCxcbiAgRXZlbnRzU2VydmljZSxcbiAgQmVmb3JlR2V0RXZlbnQsXG4gIEJlZm9yZVJlbW92ZUV2ZW50LFxuICBCZWZvcmVTYXZlRXZlbnRcbn0gZnJvbSAnLi4vZXZlbnRzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UsIFNlcnZpY2VNZXRhSW5mbyB9IGZyb20gJy4uL21ldGEnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXBpU2VydmljZSB7XG4gIHByb3RlY3RlZCBtZXRhOiBTZXJ2aWNlTWV0YUluZm87XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJvdGVjdGVkIGV2ZW50czogRXZlbnRzU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgaW5mb1NlcnZpY2U6IERhdGFJbmZvU2VydmljZSxcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge1xuICAgIHRoaXMubWV0YSA9IE1ldGFTZXJ2aWNlLmdldCh0aGlzLmNvbnN0cnVjdG9yKTtcbiAgfVxuXG4gIGdldExpc3QoZmlsdGVyPzogSHR0cFBhcmFtc3x7IFtpbmRleDogc3RyaW5nXTogc3RyaW5nfG51bWJlcnxib29sZWFuIH0pOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KHRoaXMubWV0YS5wYXRoLCBmaWx0ZXIpO1xuICB9XG5cbiAgZ2V0KGlkOiBudW1iZXIgfCBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KHRoaXMubWV0YS5wYXRoICsgJy8nICsgaWQpO1xuICB9XG5cbiAgcmVsb2FkKGVudGl0eTogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5kb0dldCh0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShlbnRpdHkpLnBhdGgpO1xuICB9XG5cbiAgZ2V0UmVmZXJlbmNlKGlkOiBudW1iZXIgfCBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KCcmJyArIHRoaXMubWV0YS5wYXRoICsgJy8nICsgaWQgKyAnIycgKyB0aGlzLm1ldGEuZW50aXR5TWV0YS5uYW1lKTtcbiAgfVxuXG4gIGRvR2V0KHBhdGg6IHN0cmluZywgZmlsdGVyPzogSHR0cFBhcmFtc3x7IFtpbmRleDogc3RyaW5nXTogc3RyaW5nfG51bWJlcnxib29sZWFuIH0pIHtcbiAgICBsZXQgZjogSHR0cFBhcmFtcztcbiAgICBpZiAoZmlsdGVyIGluc3RhbmNlb2YgSHR0cFBhcmFtcykge1xuICAgICAgZiA9IGZpbHRlcjtcbiAgICB9IGVsc2Uge1xuICAgICAgZiA9IG5ldyBIdHRwUGFyYW1zKCk7XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBmaWx0ZXIpIHtcbiAgICAgICAgZiA9IGYuc2V0KGtleSwgdHlwZW9mIGZpbHRlcltrZXldID09PSAnYm9vbGVhbicgPyAoZmlsdGVyW2tleV0gPyAndHJ1ZScgOiAnZmFsc2UnKSA6IFN0cmluZyhmaWx0ZXJba2V5XSkpO1xuICAgICAgfVxuICAgIH1cbiAgICB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEJlZm9yZUdldEV2ZW50KHBhdGgsIHRoaXMubWV0YS5lbnRpdHlNZXRhLnR5cGUsIGYpKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLmNvbmZpZy5iYXNlVXJsICsgcGF0aCwge2hlYWRlcnM6IHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldEhlYWRlcnMoKSwgcGFyYW1zOiBmfSkucGlwZShcbiAgICAgIHRhcCgoZGF0YSkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlckdldEV2ZW50KGRhdGEsIHRoaXMubWV0YS5lbnRpdHlNZXRhLnR5cGUpKSlcbiAgICApO1xuICB9XG5cbiAgc2F2ZShlbnRpdHk6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBCZWZvcmVTYXZlRXZlbnQoZW50aXR5KSk7XG4gICAgbGV0IGJvZHkgPSB7fTtcbiAgICBpZiAodGhpcy5tZXRhLmVudGl0eU1ldGEuYm9keSkge1xuICAgICAgYm9keSA9IHRoaXMubWV0YS5lbnRpdHlNZXRhLmJvZHkoZW50aXR5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5tZXRhLmVudGl0eU1ldGEuZ2V0UHJvcGVydGllc0ZvclNhdmUoKS5mb3JFYWNoKChwcm9wZXJ0eSkgPT4gYm9keVtwcm9wZXJ0eV0gPSBlbnRpdHlbcHJvcGVydHldKTtcbiAgICB9XG4gICAgbGV0IG1ldGEgPSB0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShlbnRpdHkpO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cFttZXRhID8gJ3B1dCcgOiAncG9zdCddKFxuICAgICAgICBtZXRhID8gdGhpcy5jb25maWcuYmFzZVVybCArIG1ldGEucGF0aCA6IHRoaXMuY29uZmlnLmJhc2VVcmwgKyB0aGlzLm1ldGEucGF0aCxcbiAgICAgICAgYm9keSxcbiAgICAgICAge2hlYWRlcnM6IHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldEhlYWRlcnMoZW50aXR5KX1cbiAgICAgICkucGlwZShcbiAgICAgICAgdGFwKChkYXRhKSA9PiB7XG4gICAgICAgICAgbWV0YSA9IHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGRhdGEpO1xuICAgICAgICAgIGlmICghbWV0YSkge1xuICAgICAgICAgICAgdGhpcy5pbmZvU2VydmljZS5zZXRGb3JFbnRpdHkoZW50aXR5LCBtZXRhKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pLFxuICAgICAgICB0YXAoKCkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlclNhdmVFdmVudChlbnRpdHkpKSlcbiAgICAgICk7XG4gIH1cblxuICByZW1vdmUoZW50aXR5OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQmVmb3JlUmVtb3ZlRXZlbnQoZW50aXR5KSk7XG4gICAgY29uc3QgbWV0YSA9IHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGVudGl0eSk7XG4gICAgaWYgKG1ldGEpIHtcbiAgICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlKHRoaXMuY29uZmlnLmJhc2VVcmwgKyBtZXRhLnBhdGgsIHtoZWFkZXJzOiB0aGlzLm1ldGEuZW50aXR5TWV0YS5nZXRIZWFkZXJzKGVudGl0eSl9KS5waXBlKFxuICAgICAgICB0YXAoKCkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlclJlbW92ZUV2ZW50KGVudGl0eSkpKVxuICAgICAgKTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSZWZlcmVuY2VJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZ1xuICApIHt9XG5cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGlmIChyZXF1ZXN0LnVybC5pbmRleE9mKHRoaXMuY29uZmlnLmJhc2VVcmwpICE9PSAwIHx8IHJlcXVlc3QudXJsID09PSB0aGlzLmNvbmZpZy5hdXRoVXJsIHx8ICF0aGlzLmlzUmVmZXJlbmNlUmVxdWVzdChyZXF1ZXN0KSkge1xuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5jcmVhdGVSZWZlcmVuY2VSZXNwb25zZShyZXF1ZXN0KTtcbiAgfVxuXG4gIHByaXZhdGUgaXNSZWZlcmVuY2VSZXF1ZXN0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcbiAgICByZXR1cm4gcmVxdWVzdC5tZXRob2QgPT09ICdHRVQnICYmIHJlcXVlc3QudXJsLnNsaWNlKHRoaXMuY29uZmlnLmJhc2VVcmwubGVuZ3RoKS5pbmRleE9mKCcmJykgPT09IDA7XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZVJlZmVyZW5jZVJlc3BvbnNlKHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4pIHtcbiAgICBjb25zdCBwYXRoID0gcmVxdWVzdC51cmxXaXRoUGFyYW1zLnNsaWNlKHRoaXMuY29uZmlnLmJhc2VVcmwubGVuZ3RoKTtcbiAgICByZXR1cm4gb2YobmV3IEh0dHBSZXNwb25zZSh7XG4gICAgICBib2R5OiB7J3BhdGgnOiBwYXRoLnN1YnN0cmluZygxLCBwYXRoLmluZGV4T2YoJyMnKSksICdtZXRhJzogTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKHBhdGguc3Vic3RyaW5nKHBhdGguaW5kZXhPZignIycpICsgMSkpfSxcbiAgICAgIHN0YXR1czogMjAwLFxuICAgICAgdXJsOiByZXF1ZXN0LnVybFxuICAgIH0pKS5waXBlKFxuICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2UgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcbiAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuY2xvbmUoe1xuICAgICAgICAgICAgaGVhZGVyczogcmVzcG9uc2UuaGVhZGVycy5zZXQoJ0NvbnRlbnQtVHlwZScsICdAcmVmZXJlbmNlJylcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApO1xuICB9XG59XG4iLCJpbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi9yZXNwb25zZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRGF0YU5vcm1hbGl6ZXIge1xuICBub3JtYWxpemUocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+LCB0eXBlPzogc3RyaW5nKTogUmVzcG9uc2VOb2RlO1xuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuO1xufVxuXG5leHBvcnQgbGV0IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMgPSBuZXcgSW5qZWN0aW9uVG9rZW48RGF0YU5vcm1hbGl6ZXJbXT4oJ2FucmVzdC5odHRwX2RhdGFfbm9ybWFsaXplcnMnKTtcbiIsImV4cG9ydCBjbGFzcyBSZXNwb25zZU5vZGUge1xuICBwdWJsaWMgZGF0YTogYW55O1xuICBwdWJsaWMgY29sbGVjdGlvbjogYm9vbGVhbjtcbiAgcHVibGljIG1ldGE6IGFueTtcbiAgcHVibGljIGluZm86IGFueTtcbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERhdGFJbmZvU2VydmljZSB9IGZyb20gJy4vZGF0YS1pbmZvLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT2JqZWN0Q29sbGVjdG9yIHtcbiAgICBtYXA6IE1hcDxzdHJpbmcsIGFueT4gID0gbmV3IE1hcDxzdHJpbmcsIGFueT4oKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlKSB7fVxuXG4gICAgc2V0KGRhdGE6IGFueSkge1xuICAgICAgdGhpcy5tYXAuc2V0KHRoaXMuaW5mby5nZXQoZGF0YSkucGF0aCwgZGF0YSk7XG4gICAgfVxuXG4gICAgcmVtb3ZlKGRhdGE6IGFueSkge1xuICAgICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQoZGF0YSk7XG4gICAgICBpZiAoaW5mbykge1xuICAgICAgICBkYXRhID0gaW5mby5wYXRoO1xuICAgICAgfVxuICAgICAgdGhpcy5tYXAuZGVsZXRlKGRhdGEpO1xuICAgIH1cblxuICAgIGdldChpZDogc3RyaW5nKSB7XG4gICAgICByZXR1cm4gdGhpcy5tYXAuZ2V0KGlkKTtcbiAgICB9XG5cbiAgICBoYXMoaWQ6IHN0cmluZykge1xuICAgICAgcmV0dXJuIHRoaXMubWFwLmhhcyhpZCk7XG4gICAgfVxuXG4gICAgY2xlYXIoKSB7XG4gICAgICB0aGlzLm1hcC5jbGVhcigpO1xuICAgIH1cbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuLi9yZXNwb25zZS9yZXNwb25zZSc7XG5pbXBvcnQgeyBEYXRhSW5mb1NlcnZpY2UgfSBmcm9tICcuL2RhdGEtaW5mby5zZXJ2aWNlJztcbmltcG9ydCB7IEVudGl0eU1ldGFJbmZvIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBPYmplY3RDb2xsZWN0b3IgfSBmcm9tICcuL29iamVjdC1jb2xsZWN0b3InO1xuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4vY29sbGVjdGlvbic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBcGlQcm9jZXNzb3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yLFxuICAgIHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlLFxuICAgIHByaXZhdGUgY29sbGVjdG9yOiBPYmplY3RDb2xsZWN0b3JcbiAgKSB7fVxuXG4gIHByb2Nlc3Mobm9kZTogUmVzcG9uc2VOb2RlLCBtZXRob2Q6IHN0cmluZyk6IGFueSB7XG4gICAgcmV0dXJuIG5vZGUuY29sbGVjdGlvbiA/IHRoaXMucHJvY2Vzc0NvbGxlY3Rpb24obm9kZSkgOiB0aGlzLnByb2Nlc3NPYmplY3Qobm9kZSk7XG4gIH1cblxuICBwcml2YXRlIHByb2Nlc3NDb2xsZWN0aW9uKG5vZGU6IFJlc3BvbnNlTm9kZSkge1xuICAgIGxldCBkYXRhO1xuICAgIGlmIChub2RlLmluZm8ucGF0aCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBkYXRhID0gdGhpcy5jb2xsZWN0b3IuZ2V0KG5vZGUuaW5mby5wYXRoKSB8fCBuZXcgQ29sbGVjdGlvbih0aGlzLmluamVjdG9yLmdldChub2RlLm1ldGEuc2VydmljZSksIG5vZGUuaW5mbyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGRhdGEgPSBuZXcgQ29sbGVjdGlvbih0aGlzLmluamVjdG9yLmdldChub2RlLm1ldGEuc2VydmljZSksIG5vZGUuaW5mbyk7XG4gICAgfVxuICAgIGRhdGEubGVuZ3RoID0gMDtcbiAgICBmb3IgKGNvbnN0IG9iamVjdCBvZiBub2RlLmRhdGEpIHtcbiAgICAgIGRhdGEucHVzaCh0aGlzLnByb2Nlc3NPYmplY3Qob2JqZWN0KSk7XG4gICAgfVxuICAgIHRoaXMuaW5mby5zZXRGb3JDb2xsZWN0aW9uKGRhdGEsIG5vZGUuaW5mbyk7XG4gICAgdGhpcy5jb2xsZWN0b3Iuc2V0KGRhdGEpO1xuICAgIHJldHVybiBkYXRhO1xuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzT2JqZWN0KG5vZGU6IFJlc3BvbnNlTm9kZSkge1xuICAgIGlmIChub2RlLm1ldGEgaW5zdGFuY2VvZiBFbnRpdHlNZXRhSW5mbykge1xuICAgICAgY29uc3Qgb2JqZWN0ID0gdGhpcy5jb2xsZWN0b3IuZ2V0KG5vZGUuaW5mby5wYXRoKSB8fCBuZXcgbm9kZS5tZXRhLnR5cGUoKTtcbiAgICAgIGZvciAoY29uc3QgcHJvcGVydHkgaW4gbm9kZS5tZXRhLnByb3BlcnRpZXMpIHtcbiAgICAgICAgaWYgKG9iamVjdC5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkgJiYgIW5vZGUuZGF0YS5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobm9kZS5kYXRhW3Byb3BlcnR5XSBpbnN0YW5jZW9mIFJlc3BvbnNlTm9kZSkge1xuICAgICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBub2RlLm1ldGEucHJvcGVydGllc1twcm9wZXJ0eV0uaXNDb2xsZWN0aW9uID9cbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc0NvbGxlY3Rpb24obm9kZS5kYXRhW3Byb3BlcnR5XSkgOlxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzT2JqZWN0KG5vZGUuZGF0YVtwcm9wZXJ0eV0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnN0IHR5cGU6IGFueSA9IG5vZGUubWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XS50eXBlO1xuICAgICAgICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICAgICAgY2FzZSBOdW1iZXI6XG4gICAgICAgICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBOdW1iZXIobm9kZS5kYXRhW3Byb3BlcnR5XSk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBEYXRlOlxuICAgICAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gbmV3IERhdGUobm9kZS5kYXRhW3Byb3BlcnR5XSk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBTdHJpbmc6XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gbm9kZS5kYXRhW3Byb3BlcnR5XTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBmb3IgKGNvbnN0IHByb3BlcnR5IGluIG5vZGUubWV0YS5zdWJyZXNvdXJjZXMpIHtcbiAgICAgICAgY29uc3Qgc2VydmljZSA9IHRoaXMuaW5qZWN0b3IuZ2V0KG5vZGUubWV0YS5zdWJyZXNvdXJjZXNbcHJvcGVydHldLm1ldGEuc2VydmljZSk7XG4gICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBzZXJ2aWNlLmRvR2V0LmJpbmQoc2VydmljZSwgbm9kZS5pbmZvLnBhdGggKyBub2RlLm1ldGEuc3VicmVzb3VyY2VzW3Byb3BlcnR5XS5wYXRoKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuaW5mby5zZXRGb3JFbnRpdHkob2JqZWN0LCBub2RlLmluZm8pO1xuICAgICAgdGhpcy5jb2xsZWN0b3Iuc2V0KG9iamVjdCk7XG4gICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbm9kZS5kYXRhO1xuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUywgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuLi9yZXNwb25zZS9kYXRhLW5vcm1hbGl6ZXInO1xuaW1wb3J0IHsgQXBpUHJvY2Vzc29yIH0gZnJvbSAnLi9hcGkucHJvY2Vzc29yJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFwaUludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIHJlYWRvbmx5IGNvbmZpZzogQXBpQ29uZmlnLFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUykgcHJpdmF0ZSByZWFkb25seSBub3JtYWxpemVyczogRGF0YU5vcm1hbGl6ZXJbXSxcbiAgICBwcml2YXRlIHJlYWRvbmx5IHByb2Nlc3NvcjogQXBpUHJvY2Vzc29yXG4gICkge1xuICAgIGlmICghQXJyYXkuaXNBcnJheSh0aGlzLm5vcm1hbGl6ZXJzKSkge1xuICAgICAgdGhpcy5ub3JtYWxpemVycyA9IFtdO1xuICAgIH1cbiAgfVxuXG4gIGludGVyY2VwdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICBpZiAocmVxdWVzdC51cmwuaW5kZXhPZih0aGlzLmNvbmZpZy5iYXNlVXJsKSAhPT0gMCB8fCByZXF1ZXN0LnVybCA9PT0gdGhpcy5jb25maWcuYXV0aFVybFxuICAgICAgfHwgKEFycmF5LmlzQXJyYXkodGhpcy5jb25maWcuZXhjbHVkZWRVcmxzKSAmJiB0aGlzLmNvbmZpZy5leGNsdWRlZFVybHMuaW5kZXhPZihyZXF1ZXN0LnVybCkgIT09IC0xKSkge1xuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbiAgICBsZXQgdHlwZTogc3RyaW5nO1xuICAgIGxldCBoZWFkZXJzID0gcmVxdWVzdC5oZWFkZXJzO1xuICAgIGlmIChoZWFkZXJzLmhhcygnWC1BblJlc3QtVHlwZScpKSB7XG4gICAgICB0eXBlID0gaGVhZGVycy5nZXQoJ1gtQW5SZXN0LVR5cGUnKTtcbiAgICAgIGhlYWRlcnMgPSBoZWFkZXJzLmRlbGV0ZSgneC1BblJlc3QtVHlwZScpO1xuICAgIH1cbiAgICBpZiAodGhpcy5jb25maWcuZGVmYXVsdEhlYWRlcnMpIHtcbiAgICAgIGZvciAoY29uc3QgaGVhZGVyIG9mIHRoaXMuY29uZmlnLmRlZmF1bHRIZWFkZXJzKSB7XG4gICAgICAgIGlmICghaGVhZGVycy5oYXMoaGVhZGVyLm5hbWUpIHx8IGhlYWRlci5hcHBlbmQpIHtcbiAgICAgICAgICBoZWFkZXJzID0gaGVhZGVyc1toZWFkZXIuYXBwZW5kID8gJ2FwcGVuZCcgOiAnc2V0J10oaGVhZGVyLm5hbWUsIGhlYWRlci52YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xuICAgICAgaGVhZGVyczogaGVhZGVyc1xuICAgIH0pO1xuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KS5waXBlKFxuICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2UgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcbiAgICAgICAgICBjb25zdCBjb250ZW50VHlwZSA9IHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKTtcbiAgICAgICAgICBmb3IgKGNvbnN0IG5vcm1hbGl6ZXIgb2YgdGhpcy5ub3JtYWxpemVycykge1xuICAgICAgICAgICAgaWYgKG5vcm1hbGl6ZXIuc3VwcG9ydHMoY29udGVudFR5cGUpKSB7XG4gICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5jbG9uZSh7IGJvZHk6IHRoaXMucHJvY2Vzc29yLnByb2Nlc3Mobm9ybWFsaXplci5ub3JtYWxpemUocmVzcG9uc2UsIHR5cGUpLCByZXF1ZXN0Lm1ldGhvZCkgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIFJlc3BvbnNlQ2FjaGUge1xuXG4gIGFic3RyYWN0IGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbjtcblxuICBhYnN0cmFjdCBnZXQoaWQ6IHN0cmluZyk7XG5cbiAgYWJzdHJhY3QgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSk7XG5cbiAgYWJzdHJhY3QgcmVtb3ZlKGlkOiBzdHJpbmcpO1xuXG4gIGFic3RyYWN0IGNsZWFyKCk7XG5cbiAgYWJzdHJhY3QgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZyk7XG5cbiAgcmVwbGFjZShkYXRhOiBhbnksIGlkOiBzdHJpbmcpIHtcbiAgICB0aGlzLnJlbW92ZShpZCk7XG4gICAgdGhpcy5hZGQoZGF0YSwgaWQpO1xuICB9XG59XG5cbmV4cG9ydCBsZXQgQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTID0gbmV3IEluamVjdGlvblRva2VuPFJlc3BvbnNlQ2FjaGVbXT4oJ2FucmVzdC5jYWNoZV9zZXJ2aWNlcycpO1xuIiwiaW1wb3J0IHsgUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vcmVzcG9uc2UtY2FjaGUnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTm9SZXNwb25zZUNhY2hlIGV4dGVuZHMgUmVzcG9uc2VDYWNoZSB7XG4gIGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICB9XG5cbiAgY2xlYXIoKSB7XG4gIH1cblxuICBnZXQoaWQ6IHN0cmluZykge1xuICB9XG5cbiAgcmVtb3ZlKGlkOiBzdHJpbmcpIHtcbiAgfVxuXG4gIGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpIHtcbiAgfVxuXG4gIHJlcGxhY2UoZGF0YTogYW55LCBpZDogc3RyaW5nKTogdm9pZCB7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZywgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsIFJlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL3Jlc3BvbnNlLWNhY2hlJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBOb1Jlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL25vLXJlc3BvbnNlLWNhY2hlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIENhY2hlSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgcmVhZG9ubHkgY29uZmlnOiBBcGlDb25maWcsXG4gICAgcHJpdmF0ZSByZWFkb25seSBpbmZvOiBEYXRhSW5mb1NlcnZpY2UsXG4gICAgQEluamVjdChBTlJFU1RfQ0FDSEVfU0VSVklDRVMpIHByaXZhdGUgcmVhZG9ubHkgY2FjaGVTZXJ2aWNlczogUmVzcG9uc2VDYWNoZVtdLFxuICApIHt9XG5cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGNvbnN0IHBhdGggPSByZXF1ZXN0LnVybFdpdGhQYXJhbXMuc2xpY2UodGhpcy5jb25maWcuYmFzZVVybC5sZW5ndGgpO1xuICAgIGlmIChyZXF1ZXN0LnVybC5pbmRleE9mKHRoaXMuY29uZmlnLmJhc2VVcmwpICE9PSAwIHx8IHJlcXVlc3QudXJsID09PSB0aGlzLmNvbmZpZy5hdXRoVXJsXG4gICAgICB8fCAoQXJyYXkuaXNBcnJheSh0aGlzLmNvbmZpZy5leGNsdWRlZFVybHMpICYmIHRoaXMuY29uZmlnLmV4Y2x1ZGVkVXJscy5pbmRleE9mKHJlcXVlc3QudXJsKSAhPT0gLTEpKSB7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIGNvbnN0IGNhY2hlVHlwZSA9IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShyZXF1ZXN0LmhlYWRlcnMuZ2V0KCdYLUFuUmVzdC1UeXBlJykpLmNhY2hlIHx8IHRoaXMuY29uZmlnLmNhY2hlIHx8IE5vUmVzcG9uc2VDYWNoZTtcbiAgICBsZXQgY2FjaGU6IGFueTtcbiAgICBmb3IgKGNvbnN0IGNhY2hlU2VydmljZSBvZiB0aGlzLmNhY2hlU2VydmljZXMpIHtcbiAgICAgIGlmIChjYWNoZVNlcnZpY2UgaW5zdGFuY2VvZiBjYWNoZVR5cGUpIHtcbiAgICAgICAgY2FjaGUgPSBjYWNoZVNlcnZpY2U7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAocmVxdWVzdC5tZXRob2QgIT09ICdHRVQnKSB7XG4gICAgICBjYWNoZS5yZW1vdmUocGF0aCk7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIGlmIChjYWNoZS5oYXMocGF0aCkpIHtcbiAgICAgIHJldHVybiBvZihuZXcgSHR0cFJlc3BvbnNlKHtcbiAgICAgICAgYm9keTogY2FjaGUuZ2V0KHBhdGgpLFxuICAgICAgICBzdGF0dXM6IDIwMCxcbiAgICAgICAgdXJsOiByZXF1ZXN0LnVybFxuICAgICAgfSkpO1xuICAgIH1cbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCkucGlwZShcbiAgICAgIHRhcCgocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XG4gICAgICAgIGlmIChyZXNwb25zZS5ib2R5KSB7XG4gICAgICAgICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQocmVzcG9uc2UuYm9keSk7XG4gICAgICAgICAgaWYgKGluZm8pIHtcbiAgICAgICAgICAgIGNhY2hlLmFkZChpbmZvLnBhdGgsIHJlc3BvbnNlLmJvZHkpO1xuICAgICAgICAgICAgaWYgKGluZm8gJiYgaW5mby5hbGlhcykge1xuICAgICAgICAgICAgICBjYWNoZS5hZGRBbGlhcyhpbmZvLnBhdGgsIGluZm8uYWxpYXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApO1xuICB9XG59XG4iLCJpbXBvcnQgeyBSZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9iamVjdENvbGxlY3RvciB9IGZyb20gJy4uL2NvcmUvb2JqZWN0LWNvbGxlY3Rvcic7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4uL2NvcmUvY29uZmlnJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE1lbW9yeVJlc3BvbnNlQ2FjaGUgZXh0ZW5kcyBSZXNwb25zZUNhY2hlIHtcblxuICBwcml2YXRlIHRpbWVzOiB7IFtpbmRleDogc3RyaW5nXTogbnVtYmVyIH0gPSB7fTtcblxuICBwcml2YXRlIGFsaWFzZXM6IHsgW2luZGV4OiBzdHJpbmddOiBzdHJpbmcgfSA9IHt9O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcmVhZG9ubHkgY29sbGVjdG9yOiBPYmplY3RDb2xsZWN0b3IsXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZ1xuICApIHtcbiAgICBzdXBlcigpO1xuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICAgIHRoaXMudGltZXNbaWRdID0gRGF0ZS5ub3coKTtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICAgIHRoaXMuY29sbGVjdG9yLmNsZWFyKCk7XG4gIH1cblxuICBnZXQoaWQ6IHN0cmluZykge1xuICAgIHJldHVybiB0aGlzLmNvbGxlY3Rvci5nZXQoaWQpO1xuICB9XG5cbiAgcmVtb3ZlKGlkOiBzdHJpbmcpIHtcbiAgICB0aGlzLmNvbGxlY3Rvci5yZW1vdmUoaWQpO1xuICB9XG5cbiAgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZykge1xuICAgIHRoaXMuYWxpYXNlc1thbGlhc10gPSBpZDtcbiAgfVxuXG4gIGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgY29uc3QgdGltZSA9IHRoaXMuYWxpYXNlc1tpZF0gPyB0aGlzLnRpbWVzW3RoaXMuYWxpYXNlc1tpZF1dIDogdGhpcy50aW1lc1tpZF07XG4gICAgcmV0dXJuIHRpbWUgJiYgKERhdGUubm93KCkgPD0gdGhpcy5jb25maWcuY2FjaGVUVEwgKyB0aW1lKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBEYXRhTm9ybWFsaXplciB9IGZyb20gJy4vZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4vcmVzcG9uc2UnO1xuaW1wb3J0IHsgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIgaW1wbGVtZW50cyBEYXRhTm9ybWFsaXplciB7XG4gIG5vcm1hbGl6ZShyZXNwb25zZTogSHR0cFJlc3BvbnNlPGFueT4sIHR5cGU/OiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIGNvbnN0IHIgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgci5jb2xsZWN0aW9uID0gZmFsc2U7XG4gICAgci5pbmZvID0gbmV3IEVudGl0eUluZm8ocmVzcG9uc2UuYm9keS5wYXRoKTtcbiAgICByLm1ldGEgPSByZXNwb25zZS5ib2R5Lm1ldGE7XG4gICAgci5kYXRhID0ge307XG4gICAgcmV0dXJuIHI7XG4gIH1cblxuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHlwZSA9PT0gJ0ByZWZlcmVuY2UnO1xuICB9XG59XG4iLCJpbXBvcnQgeyBEYXRhTm9ybWFsaXplciB9IGZyb20gJy4vZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4vcmVzcG9uc2UnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCB7IENvbGxlY3Rpb25JbmZvLCBFbnRpdHlJbmZvIH0gZnJvbSAnLi4vY29yZSc7XG5cbmV4cG9ydCBjbGFzcyBMZEpzb25EYXRhTm9ybWFsaXplciBpbXBsZW1lbnRzIERhdGFOb3JtYWxpemVyIHtcbiAgbm9ybWFsaXplKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2U8YW55PiwgdHlwZT86IHN0cmluZyk6IFJlc3BvbnNlTm9kZSB7XG4gICAgcmV0dXJuIHRoaXMucHJvY2VzcyhyZXNwb25zZS5ib2R5KTtcbiAgfVxuXG4gIHN1cHBvcnRzKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0eXBlLnNwbGl0KCc7JylbMF0gPT09ICdhcHBsaWNhdGlvbi9sZCtqc29uJztcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2VzcyhkYXRhLCBtZXRhPzogYW55LCBjb2xsZWN0aW9uPzogYm9vbGVhbikge1xuICAgIGNvbnN0IHIgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgci5jb2xsZWN0aW9uID0gY29sbGVjdGlvbiB8fCBkYXRhWydAdHlwZSddID09PSAnaHlkcmE6Q29sbGVjdGlvbic7XG4gICAgY29uc3QgcGF0aCA9IGRhdGFbJ0BpZCddO1xuICAgIHIubWV0YSA9IG1ldGEgfHwgTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKHIuY29sbGVjdGlvbiA/IC9cXC9jb250ZXh0c1xcLyhcXHcrKS9nLmV4ZWMoZGF0YVsnQGNvbnRleHQnXSlbMV0gOiBkYXRhWydAdHlwZSddKTtcbiAgICBpZiAoci5jb2xsZWN0aW9uKSB7XG4gICAgICBjb25zdCBpdGVtcyA9IGRhdGFbJ2h5ZHJhOm1lbWJlciddIHx8IGRhdGE7XG4gICAgICByLmluZm8gPSBuZXcgQ29sbGVjdGlvbkluZm8oZGF0YVsnaHlkcmE6dmlldyddID8gZGF0YVsnaHlkcmE6dmlldyddWydAaWQnXSA6IHBhdGgpO1xuICAgICAgci5pbmZvLnR5cGUgPSByLm1ldGEubmFtZTtcbiAgICAgIGlmIChkYXRhWydoeWRyYTp2aWV3J10pIHtcbiAgICAgICAgci5pbmZvLmZpcnN0ID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpmaXJzdCddO1xuICAgICAgICByLmluZm8ucHJldmlvdXMgPSBkYXRhWydoeWRyYTp2aWV3J11bJ2h5ZHJhOnByZXZpb3VzJ107XG4gICAgICAgIHIuaW5mby5uZXh0ID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpuZXh0J107XG4gICAgICAgIHIuaW5mby5sYXN0ID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpsYXN0J107XG4gICAgICAgIGlmIChyLmluZm8ucGF0aCA9PT0gci5pbmZvLmZpcnN0KSB7XG4gICAgICAgICAgci5pbmZvLmFsaWFzID0gZGF0YVsnQGlkJ107XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHIuaW5mby50b3RhbCA9IGRhdGFbJ2h5ZHJhOnRvdGFsSXRlbXMnXSB8fCBpdGVtcy5sZW5ndGg7XG4gICAgICBjb25zdCByZWdleFBhZ2UgPSAvcGFnZT0oXFxkKykvZztcbiAgICAgIGxldCBtYXRjaGVzO1xuICAgICAgbWF0Y2hlcyA9IHJlZ2V4UGFnZS5leGVjKHIuaW5mby5wYXRoKTtcbiAgICAgIHIuaW5mby5wYWdlID0gbWF0Y2hlcyA/IE51bWJlcihtYXRjaGVzWzFdKSA6IDE7XG4gICAgICBtYXRjaGVzID0gcmVnZXhQYWdlLmV4ZWMoci5pbmZvLmxhc3QpO1xuICAgICAgci5pbmZvLmxhc3RQYWdlID0gbWF0Y2hlcyA/IE51bWJlcihtYXRjaGVzWzFdKSA6IDE7XG4gICAgICByLmRhdGEgPSBbXTtcbiAgICAgIGZvciAoY29uc3Qgb2JqZWN0IG9mIGl0ZW1zKSB7XG4gICAgICAgIHIuZGF0YS5wdXNoKHRoaXMucHJvY2VzcyhvYmplY3QpKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgci5pbmZvID0gbmV3IEVudGl0eUluZm8ocGF0aCk7XG4gICAgICByLmRhdGEgPSB7fTtcbiAgICAgIGlmIChyLm1ldGEgPT09IHVuZGVmaW5lZCAmJiBBcnJheS5pc0FycmF5KGRhdGEpKSB7XG4gICAgICAgIHIuZGF0YSA9IGRhdGE7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgICAgaWYgKGtleS5pbmRleE9mKCdAJykgPT09IDAgfHwga2V5LmluZGV4T2YoJ2h5ZHJhOicpID09PSAwKSB7XG4gICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgY29uc3QgcHJvcGVydHlNZXRhID0gci5tZXRhLnByb3BlcnRpZXNba2V5XTtcbiAgICAgICAgICByLmRhdGFba2V5XSA9IChBcnJheS5pc0FycmF5KGRhdGFba2V5XSkgfHwgKGRhdGFba2V5XSAhPT0gbnVsbCAmJiBwcm9wZXJ0eU1ldGEgJiYgTWV0YVNlcnZpY2UuZ2V0KHByb3BlcnR5TWV0YS50eXBlKSkpID9cbiAgICAgICAgICAgIHRoaXMucHJvY2VzcyhkYXRhW2tleV0sIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSksIHByb3BlcnR5TWV0YS5pc0NvbGxlY3Rpb24pIDogZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByO1xuICB9XG59XG4iLCJpbXBvcnQgeyBEYXRhTm9ybWFsaXplciB9IGZyb20gJy4vZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEh0dHBIZWFkZXJzLCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuL3Jlc3BvbnNlJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uSW5mbywgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgSnNvbkRhdGFOb3JtYWxpemVyIGltcGxlbWVudHMgRGF0YU5vcm1hbGl6ZXIge1xuICBub3JtYWxpemUocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+LCB0eXBlOiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIHJldHVybiB0aGlzLnByb2Nlc3MocmVzcG9uc2UuYm9keSwgTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKHR5cGUpLCBBcnJheS5pc0FycmF5KHJlc3BvbnNlLmJvZHkpLCByZXNwb25zZS5oZWFkZXJzKTtcbiAgfVxuXG4gIHN1cHBvcnRzKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0eXBlLnNwbGl0KCc7JylbMF0gPT09ICdhcHBsaWNhdGlvbi9qc29uJztcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2VzcyhkYXRhLCBtZXRhOiBhbnksIGNvbGxlY3Rpb246IGJvb2xlYW4sIGhlYWRlcnM/OiBIdHRwSGVhZGVycykge1xuICAgIGNvbnN0IHIgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgci5jb2xsZWN0aW9uID0gY29sbGVjdGlvbjtcbiAgICBjb25zdCBjb2xsZWN0aW9uUGF0aCA9IE1ldGFTZXJ2aWNlLmdldChtZXRhLnNlcnZpY2UpID8gTWV0YVNlcnZpY2UuZ2V0KG1ldGEuc2VydmljZSkucGF0aCA6IHVuZGVmaW5lZDtcbiAgICBjb25zdCBwYXRoID0gY29sbGVjdGlvblBhdGggPyAoY29sbGVjdGlvbiA/IGNvbGxlY3Rpb25QYXRoIDogY29sbGVjdGlvblBhdGggKyAnLycgKyBkYXRhW21ldGEuaWRdKSA6IHVuZGVmaW5lZDtcbiAgICByLm1ldGEgPSBtZXRhO1xuICAgIGlmIChyLmNvbGxlY3Rpb24pIHtcbiAgICAgIHIuaW5mbyA9IG5ldyBDb2xsZWN0aW9uSW5mbyhoZWFkZXJzLmdldCgnWC1DdXJyZW50LVBhZ2UnKSB8fCBwYXRoKTtcbiAgICAgIHIuaW5mby50eXBlID0gci5tZXRhLm5hbWU7XG4gICAgICByLmluZm8uZmlyc3QgPSBoZWFkZXJzLmdldCgnWC1GaXJzdC1QYWdlJykgfHwgdW5kZWZpbmVkO1xuICAgICAgci5pbmZvLnByZXZpb3VzID0gaGVhZGVycy5nZXQoJ1gtUHJldi1QYWdlJykgfHwgdW5kZWZpbmVkO1xuICAgICAgci5pbmZvLm5leHQgPSBoZWFkZXJzLmdldCgnWC1OZXh0LVBhZ2UnKSB8fCB1bmRlZmluZWQ7XG4gICAgICByLmluZm8ubGFzdCA9IGhlYWRlcnMuZ2V0KCdYLUxhc3QtUGFnZScpIHx8IHVuZGVmaW5lZDtcbiAgICAgIGlmIChyLmluZm8ucGF0aCA9PT0gci5pbmZvLmZpcnN0KSB7XG4gICAgICAgIHIuaW5mby5hbGlhcyA9IHBhdGg7XG4gICAgICB9XG4gICAgICByLmluZm8udG90YWwgPSBoZWFkZXJzLmdldCgnWC1Ub3RhbCcpIHx8IGRhdGEubGVuZ3RoO1xuICAgICAgY29uc3QgcmVnZXhQYWdlID0gL3BhZ2U9KFxcZCspL2c7XG4gICAgICBsZXQgbWF0Y2hlcztcbiAgICAgIG1hdGNoZXMgPSByZWdleFBhZ2UuZXhlYyhyLmluZm8ucGF0aCk7XG4gICAgICByLmluZm8ucGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgbWF0Y2hlcyA9IHJlZ2V4UGFnZS5leGVjKHIuaW5mby5sYXN0KTtcbiAgICAgIHIuaW5mby5sYXN0UGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgci5kYXRhID0gW107XG4gICAgICBmb3IgKGNvbnN0IG9iamVjdCBvZiBkYXRhKSB7XG4gICAgICAgIHIuZGF0YS5wdXNoKHRoaXMucHJvY2VzcyhvYmplY3QsIHIubWV0YSwgZmFsc2UpKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgci5pbmZvID0gbmV3IEVudGl0eUluZm8ocGF0aCk7XG4gICAgICByLmRhdGEgPSB7fTtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGRhdGEpIHtcbiAgICAgICAgY29uc3QgcHJvcGVydHlNZXRhID0gci5tZXRhLnByb3BlcnRpZXNba2V5XTtcbiAgICAgICAgci5kYXRhW2tleV0gPSAoQXJyYXkuaXNBcnJheShkYXRhW2tleV0pIHx8IChkYXRhW2tleV0gIT09IG51bGwgJiYgcHJvcGVydHlNZXRhICYmIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSkpKSA/XG4gICAgICAgICAgdGhpcy5wcm9jZXNzKGRhdGFba2V5XSwgTWV0YVNlcnZpY2UuZ2V0KHByb3BlcnR5TWV0YS50eXBlKSwgcHJvcGVydHlNZXRhLmlzQ29sbGVjdGlvbikgOiBkYXRhW2tleV07XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByO1xuICB9XG59XG4iLCJpbXBvcnQgeyBSZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9iamVjdENvbGxlY3RvciB9IGZyb20gJy4uL2NvcmUvb2JqZWN0LWNvbGxlY3Rvcic7XG5pbXBvcnQgeyBBcGlQcm9jZXNzb3IgfSBmcm9tICcuLi9jb3JlL2FwaS5wcm9jZXNzb3InO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uSW5mbywgRGF0YUluZm9TZXJ2aWNlLCBFbnRpdHlJbmZvIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi4vcmVzcG9uc2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSBleHRlbmRzIFJlc3BvbnNlQ2FjaGUge1xuXG4gIHByaXZhdGUgdG9rZW5QcmVmaXggPSAnYW5yZXN0OmNhY2hlOic7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjb2xsZWN0b3I6IE9iamVjdENvbGxlY3RvcixcbiAgICBwcml2YXRlIHByb2Nlc3NvcjogQXBpUHJvY2Vzc29yLFxuICAgIHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcml2YXRlIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge1xuICAgIHN1cGVyKCk7XG4gICAgY29uc3QgdGltZXN0YW1wUHJlZml4ID0gdGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JztcbiAgICBPYmplY3Qua2V5cyhsb2NhbFN0b3JhZ2UpLmZpbHRlcigoZSkgPT4gZS5pbmRleE9mKHRpbWVzdGFtcFByZWZpeCkgPT09IDApLmZvckVhY2goKGspID0+IHtcbiAgICAgIGNvbnN0IGlkID0gay5zdWJzdHJpbmcodGltZXN0YW1wUHJlZml4Lmxlbmd0aCk7XG4gICAgICBjb25zdCB0aW1lc3RhbXAgPSB0aGlzLmdldFRpbWVzdGFtcChpZCk7XG4gICAgICBpZiAoIXRpbWVzdGFtcCB8fCB0aW1lc3RhbXAgKyB0aGlzLmNvbmZpZy5jYWNoZVRUTCA8IERhdGUubm93KCkpIHtcbiAgICAgICAgdGhpcy5yZW1vdmUoaWQpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgZ2V0KGlkOiBzdHJpbmcpIHtcbiAgICBpZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGlkKSB8fCBpZDtcbiAgICBpZiAodGhpcy5oYXMoaWQpKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9jZXNzb3IucHJvY2Vzcyh0aGlzLmdldE5vZGUoaWQpLCAnR0VUJyk7XG4gICAgfVxuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICAgIHRoaXMuZG9BZGQoaWQsIGRhdGEsIHRydWUpO1xuICB9XG5cbiAgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZykge1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGFsaWFzLCBpZCk7XG4gIH1cblxuICBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIGlkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdhbGlhczonICsgaWQpIHx8IGlkO1xuICAgIGNvbnN0IGNvbXBsZXRlID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCkpO1xuICAgIGNvbnN0IHRpbWVzdGFtcCA9IHRoaXMuZ2V0VGltZXN0YW1wKGlkKTtcbiAgICByZXR1cm4gISF0aW1lc3RhbXAgJiYgY29tcGxldGUgJiYgdGltZXN0YW1wICsgdGhpcy5jb25maWcuY2FjaGVUVEwgPj0gRGF0ZS5ub3coKTtcbiAgfVxuXG4gIHJlbW92ZShpZDogc3RyaW5nKSB7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCk7XG4gICAgdGhpcy5yZW1vdmVUaW1lc3RhbXAoaWQpO1xuICB9XG5cbiAgY2xlYXIoKSB7XG4gICAgT2JqZWN0LmtleXMobG9jYWxTdG9yYWdlKS5maWx0ZXIoKGUpID0+IGUuaW5kZXhPZih0aGlzLnRva2VuUHJlZml4KSA9PT0gMCkuZm9yRWFjaCgoaykgPT4ge1xuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oayk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGRvQWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSwgY29tcGxldGU6IGJvb2xlYW4pIHtcbiAgICBjb25zdCBpbmZvID0gdGhpcy5pbmZvLmdldChkYXRhKTtcbiAgICBsZXQgdmFsdWVzOiBhbnk7XG4gICAgaWYgKGluZm8gaW5zdGFuY2VvZiBDb2xsZWN0aW9uSW5mbykge1xuICAgICAgdmFsdWVzID0gW107XG4gICAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YSkge1xuICAgICAgICBjb25zdCBpdGVtUGF0aCA9IHRoaXMuaW5mby5nZXQoaXRlbSkucGF0aDtcbiAgICAgICAgdmFsdWVzLnB1c2goaXRlbVBhdGgpO1xuICAgICAgICB0aGlzLmRvQWRkKGl0ZW1QYXRoLCBpdGVtLCBmYWxzZSk7XG4gICAgICB9XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkLCBKU09OLnN0cmluZ2lmeShpbmZvKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHJlZmVyZW5jZXMgPSB7fTtcbiAgICAgIHZhbHVlcyA9IHt9O1xuICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuICAgICAgICBsZXQgaXRlbUluZm86IGFueTtcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhW2tleV0gPT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgaXRlbUluZm8gPSB0aGlzLmluZm8uZ2V0KGRhdGFba2V5XSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGl0ZW1JbmZvKSB7XG4gICAgICAgICAgdGhpcy5kb0FkZChpdGVtSW5mby5wYXRoLCBkYXRhW2tleV0sIGZhbHNlKTtcbiAgICAgICAgICByZWZlcmVuY2VzW2tleV0gPSBpdGVtSW5mby5wYXRoO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZhbHVlc1trZXldID0gZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkLCBNZXRhU2VydmljZS5nZXQoZGF0YS5jb25zdHJ1Y3RvcikubmFtZSk7XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3JlZmVyZW5jZXM6JyArIGlkLCBKU09OLnN0cmluZ2lmeShyZWZlcmVuY2VzKSk7XG4gICAgfVxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnY29tcGxldGU6JyArIGlkLCBKU09OLnN0cmluZ2lmeShjb21wbGV0ZSkpO1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCwgSlNPTi5zdHJpbmdpZnkodmFsdWVzKSk7XG4gICAgdGhpcy5zZXRUaW1lc3RhbXAoaWQpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXROb2RlKGlkOiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCkpO1xuICAgIGNvbnN0IGNvbGxlY3Rpb25JbmZvID0gdGhpcy5nZXRDb2xsZWN0aW9uSW5mbyhpZCk7XG4gICAgY29uc3QgcmVzcG9uc2UgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgaWYgKGNvbGxlY3Rpb25JbmZvKSB7XG4gICAgICByZXNwb25zZS5jb2xsZWN0aW9uID0gdHJ1ZTtcbiAgICAgIHJlc3BvbnNlLmluZm8gPSBjb2xsZWN0aW9uSW5mbztcbiAgICAgIHJlc3BvbnNlLm1ldGEgPSBNZXRhU2VydmljZS5nZXRCeU5hbWUoY29sbGVjdGlvbkluZm8udHlwZSk7XG4gICAgICByZXNwb25zZS5kYXRhID0gW107XG4gICAgICBmb3IgKGNvbnN0IGl0ZW1JZCBvZiBkYXRhKSB7XG4gICAgICAgIHJlc3BvbnNlLmRhdGEucHVzaCh0aGlzLmdldE5vZGUoaXRlbUlkKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3BvbnNlLmNvbGxlY3Rpb24gPSBmYWxzZTtcbiAgICAgIHJlc3BvbnNlLmluZm8gPSBuZXcgRW50aXR5SW5mbyhpZCk7XG4gICAgICByZXNwb25zZS5tZXRhID0gTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAndHlwZTonICsgaWQpKTtcbiAgICAgIHJlc3BvbnNlLmRhdGEgPSBkYXRhO1xuICAgICAgY29uc3QgcmVmZXJlbmNlcyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCkpO1xuICAgICAgaWYgKHJlZmVyZW5jZXMpIHtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gcmVmZXJlbmNlcykge1xuICAgICAgICAgIHJlc3BvbnNlLmRhdGFba2V5XSA9IHRoaXMuZ2V0Tm9kZShyZWZlcmVuY2VzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXNwb25zZTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0Q29sbGVjdGlvbkluZm8oaWQ6IHN0cmluZykge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnY29sbGVjdGlvbjonICsgaWQpKTtcbiAgICBpZiAoZGF0YSkge1xuICAgICAgY29uc3QgaW5mbyA9IG5ldyBDb2xsZWN0aW9uSW5mbyhpZCk7XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgIGlmIChrZXkgIT09ICdfcGF0aCcpIHtcbiAgICAgICAgICBpbmZvW2tleV0gPSBkYXRhW2tleV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBpbmZvO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0VGltZXN0YW1wKGlkOiBzdHJpbmcpIHtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3RpbWVzdGFtcDonICsgaWQsIFN0cmluZyhEYXRlLm5vdygpKSk7XG4gIH1cblxuICBwcml2YXRlIGdldFRpbWVzdGFtcChpZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIE51bWJlcihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3RpbWVzdGFtcDonICsgaWQpKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVtb3ZlVGltZXN0YW1wKGlkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JyArIGlkKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgQXV0aCB9IGZyb20gJy4vYXV0aCc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBdXRoU2VydmljZSBpbXBsZW1lbnRzIEF1dGgge1xuICBhYnN0cmFjdCBnZXRJbmZvKCk6IGFueTtcblxuICBhYnN0cmFjdCBnZXRUb2tlbigpOiBzdHJpbmc7XG5cbiAgYWJzdHJhY3QgaXNTaWduZWRJbigpOiBib29sZWFuO1xuXG4gIGFic3RyYWN0IHJlbW92ZSgpO1xuXG4gIGFic3RyYWN0IHNldFRva2VuKHRva2VuOiBzdHJpbmcpO1xuXG4gIGFic3RyYWN0IHNpZ25Jbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+O1xuXG59XG4iLCJpbXBvcnQgeyBBdXRoVG9rZW4gfSBmcm9tICcuL2F1dGgtdG9rZW4nO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlIGltcGxlbWVudHMgQXV0aFRva2Vue1xuICBhYnN0cmFjdCBnZXQoKTogc3RyaW5nO1xuICBhYnN0cmFjdCBzZXQodG9rZW46IHN0cmluZyk7XG4gIGFic3RyYWN0IHJlbW92ZSgpO1xuICBhYnN0cmFjdCBpc1NldCgpOiBib29sZWFuO1xufVxuIiwiaW1wb3J0IHsgQXV0aFRva2VuIH0gZnJvbSAnLi9hdXRoLXRva2VuJztcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvY2FsU3RvcmFnZUF1dGhUb2tlblByb3ZpZGVyU2VydmljZSBpbXBsZW1lbnRzIEF1dGhUb2tlbiB7XG4gIHByaXZhdGUgdG9rZW5OYW1lID0gJ2FucmVzdDphdXRoX3Rva2VuJztcblxuICBnZXQoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlbk5hbWUpO1xuICB9XG5cbiAgc2V0KHRva2VuOiBzdHJpbmcpIHtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuTmFtZSwgdG9rZW4pO1xuICB9XG5cbiAgcmVtb3ZlKCkge1xuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5OYW1lKTtcbiAgfVxuXG4gIGlzU2V0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuTmFtZSkgIT0gbnVsbDtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1xuICBIdHRwUmVxdWVzdCxcbiAgSHR0cEhhbmRsZXIsXG4gIEh0dHBFdmVudCxcbiAgSHR0cEludGVyY2VwdG9yXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBdXRoSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBhdXRoOiBBdXRoU2VydmljZSwgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCBjb25maWc6IEFwaUNvbmZpZykge31cblxuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgaWYgKCF0aGlzLmNvbmZpZy5hdXRoVXJsIHx8IHJlcXVlc3QudXJsLmluZGV4T2YodGhpcy5jb25maWcuYmFzZVVybCkgIT09IDAgfHwgcmVxdWVzdC51cmwgPT09IHRoaXMuY29uZmlnLmF1dGhVcmwpIHtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgaWYgKHRoaXMuYXV0aC5pc1NpZ25lZEluKCkpIHtcbiAgICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcbiAgICAgICAgc2V0SGVhZGVyczoge1xuICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHt0aGlzLmF1dGguZ2V0VG9rZW4oKX1gXG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCAqIGFzIGp3dF9kZWNvZGUgZnJvbSAnand0LWRlY29kZSc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4uL2NvcmUvY29uZmlnJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGggfSBmcm9tICcuL2F1dGgnO1xuaW1wb3J0IHsgQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgSnd0QXV0aFNlcnZpY2UgaW1wbGVtZW50cyBBdXRoIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJpdmF0ZSB0b2tlbjogQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgY29uZmlnOiBBcGlDb25maWdcbiAgKSB7fVxuXG4gIGdldFRva2VuKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMudG9rZW4uZ2V0KCk7XG4gIH1cblxuICBzZXRUb2tlbih0b2tlbjogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy50b2tlbi5zZXQodG9rZW4pO1xuICB9XG5cbiAgcmVtb3ZlKCk6IHZvaWQge1xuICAgIHRoaXMudG9rZW4ucmVtb3ZlKCk7XG4gIH1cblxuICBpc1NpZ25lZEluKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnRva2VuLmlzU2V0KCk7XG4gIH1cblxuICBnZXRJbmZvKCk6IGFueSB7XG4gICAgcmV0dXJuIGp3dF9kZWNvZGUuY2FsbCh0aGlzLmdldFRva2VuKCkpO1xuICB9XG5cbiAgc2lnbkluKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZy5hdXRoVXJsLCB7XG4gICAgICBsb2dpbjogdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZDogcGFzc3dvcmRcbiAgICB9KS5waXBlKFxuICAgICAgdGFwKChkYXRhOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5zZXRUb2tlbihkYXRhLnRva2VuKTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFJlc291cmNlSW5mbyB7XG4gIHBhdGg/OiBzdHJpbmc7XG4gIG5hbWU/OiBzdHJpbmc7XG4gIGNhY2hlPzogRnVuY3Rpb247XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBSZXNvdXJjZSAoaW5mbzogUmVzb3VyY2VJbmZvID0ge30pIHtcblxuICByZXR1cm4gKHRhcmdldDogRnVuY3Rpb24pID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0KTtcbiAgICBtZXRhLm5hbWUgPSBpbmZvLm5hbWUgfHwgdGFyZ2V0Lm5hbWU7XG4gICAgbWV0YS5jYWNoZSA9IGluZm8uY2FjaGU7XG4gIH07XG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFByb3BlcnR5SW5mbyB7XG4gIGNvbGxlY3Rpb24/OiBib29sZWFuO1xuICBleGNsdWRlV2hlblNhdmluZz86IGJvb2xlYW47XG4gIHR5cGU/OiBhbnk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBQcm9wZXJ0eSAoaW5mbzogUHJvcGVydHlJbmZvID0ge30pIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZykgPT4ge1xuICAgIGNvbnN0IG1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvclByb3BlcnR5KHRhcmdldCwga2V5LCBpbmZvLnR5cGUpO1xuICAgIG1ldGEuaXNDb2xsZWN0aW9uID0gaW5mby5jb2xsZWN0aW9uIHx8IGZhbHNlO1xuICAgIG1ldGEuZXhjbHVkZVdoZW5TYXZpbmcgPSBpbmZvLmV4Y2x1ZGVXaGVuU2F2aW5nIHx8IGZhbHNlO1xuICB9O1xufVxuIiwiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCAqIGFzIHBsdXJhbGl6ZSBmcm9tICdwbHVyYWxpemUnO1xuXG5leHBvcnQgZnVuY3Rpb24gSHR0cFNlcnZpY2UoZW50aXR5OiBGdW5jdGlvbiwgcGF0aD86IHN0cmluZykge1xuXG4gIHJldHVybiAodGFyZ2V0OiBGdW5jdGlvbikgPT4ge1xuICAgIGNvbnN0IG1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckh0dHBTZXJ2aWNlKHRhcmdldCwgZW50aXR5KTtcbiAgICBtZXRhLnBhdGggPSBwYXRoIHx8ICcvJyArIHBsdXJhbGl6ZS5wbHVyYWwoZW50aXR5Lm5hbWUpLnJlcGxhY2UoL1xcLj8oW0EtWl0pL2csICctJDEnKS5yZXBsYWNlKC9eLS8sICcnKS50b0xvd2VyQ2FzZSgpO1xuICAgIG1ldGEuZW50aXR5TWV0YS5zZXJ2aWNlID0gdGFyZ2V0O1xuICB9O1xufVxuIiwiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGZ1bmN0aW9uIEhlYWRlcihoZWFkZXJOYW1lOiBzdHJpbmcsIGFwcGVuZD86IGJvb2xlYW4pIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZywgdmFsdWU6IGFueSA9IHVuZGVmaW5lZCkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3RvcikuaGVhZGVycy5wdXNoKHtcbiAgICAgIG5hbWU6IGhlYWRlck5hbWUsXG4gICAgICB2YWx1ZTogdmFsdWUgPyB2YWx1ZS52YWx1ZSA6IGZ1bmN0aW9uICgpIHsgcmV0dXJuICB0aGlzW2tleV07IH0sXG4gICAgICBhcHBlbmQ6IGFwcGVuZCB8fCBmYWxzZSxcbiAgICAgIGR5bmFtaWM6IHRydWVcbiAgICB9KTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBIZWFkZXJzIChoZWFkZXJzOiB7IG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZywgYXBwZW5kPzogYm9vbGVhbn1bXSkge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnkpID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0KTtcbiAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiBoZWFkZXJzKSB7XG4gICAgICBtZXRhLmhlYWRlcnMucHVzaCh7XG4gICAgICAgIG5hbWU6IGhlYWRlci5uYW1lLFxuICAgICAgICB2YWx1ZTogKCkgPT4gaGVhZGVyLnZhbHVlLFxuICAgICAgICBhcHBlbmQ6IGhlYWRlci5hcHBlbmQgfHwgZmFsc2UsXG4gICAgICAgIGR5bmFtaWM6IGZhbHNlXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gQm9keSgpIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IGFueSA9IHVuZGVmaW5lZCwgdmFsdWU6IGFueSA9IHVuZGVmaW5lZCkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3RvcikuYm9keSA9IChvYmplY3Q6IGFueSkgPT4ge1xuICAgICAgcmV0dXJuICh2YWx1ZSA/IHZhbHVlLnZhbHVlIDogZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpc1trZXldOyB9KS5jYWxsKG9iamVjdCk7XG4gICAgfTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBTdWJyZXNvdXJjZSAodHlwZTogRnVuY3Rpb24sIHBhdGg/OiBzdHJpbmcpIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZywgdmFsdWU6IGFueSkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3Rvcikuc3VicmVzb3VyY2VzW2tleV0gPSB7XG4gICAgICBtZXRhOiBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0eXBlKSxcbiAgICAgIHBhdGg6IHBhdGggfHwgJy8nICsga2V5LnJlcGxhY2UoL1xcLj8oW0EtWl0pL2csICctJDEnKS5yZXBsYWNlKC9eLS8sICcnKS50b0xvd2VyQ2FzZSgpXG4gICAgfTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBJZCAoKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcpID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0LmNvbnN0cnVjdG9yKTtcbiAgICBtZXRhLmlkID0ga2V5O1xuICB9O1xufVxuIiwiaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBjb21iaW5lTGF0ZXN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUsIGRpc3RpbmN0VW50aWxDaGFuZ2VkLCBtYXAsIG1lcmdlTWFwLCBzaGFyZSwgc2hhcmVSZXBsYXksIHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFwaVNlcnZpY2UgfSBmcm9tICcuLi9jb3JlJztcbmltcG9ydCB7IENvbGxlY3Rpb24gfSBmcm9tICcuLi9jb3JlL2NvbGxlY3Rpb24nO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgTG9hZGVyIHtcblxuICBwcml2YXRlIHJlYWRvbmx5IGZpZWxkcyA9IHt9O1xuICBwcml2YXRlIHJlYWRvbmx5IG9ic2VydmFibGU6IE9ic2VydmFibGU8YW55PjtcbiAgcHJpdmF0ZSBsb2FkaW5nID0gZmFsc2U7XG4gIHByb3RlY3RlZCBsYXN0RGF0YTogQ29sbGVjdGlvbjxhbnk+O1xuXG4gIGFic3RyYWN0IGdldEF2YWlsYWJsZUZpZWxkcygpOiBzdHJpbmdbXTtcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgc2VydmljZTogQXBpU2VydmljZSwgd2FpdEZvciA9IDAsIHJlcGxheSA9IDApIHtcbiAgICB0aGlzLm9ic2VydmFibGUgPSBjb21iaW5lTGF0ZXN0KHRoaXMuZ2V0U3ViamVjdHMoKSlcbiAgICAgIC5waXBlKFxuICAgICAgICByZXBsYXkgPyBzaGFyZVJlcGxheShyZXBsYXkpIDogc2hhcmUoKSxcbiAgICAgICAgZGVib3VuY2VUaW1lKHdhaXRGb3IpLFxuICAgICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgoZDE6IGFueVtdLCBkMjogYW55W10pID0+IHRoaXMuY2hlY2tEaXN0aW5jdChkMSwgZDIpKSxcbiAgICAgICAgbWFwKCh2YWx1ZXM6IGFueVtdKSA9PiB0aGlzLm1hcFBhcmFtcyh2YWx1ZXMpKSxcbiAgICAgICAgbWVyZ2VNYXAoKGRhdGEpID0+IHtcbiAgICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgIHJldHVybiB0aGlzLmdldFNlcnZpY2VPYnNlcnZhYmxlKGRhdGEpLnBpcGUoXG4gICAgICAgICAgICB0YXAoKHY6IENvbGxlY3Rpb248YW55PikgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmxhc3REYXRhID0gdjtcbiAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICk7XG4gICAgICAgIH0pLFxuICAgICAgKTtcbiAgfVxuXG4gIGZpZWxkKG5hbWUpOiBCZWhhdmlvclN1YmplY3Q8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGRzW25hbWVdO1xuICB9XG5cbiAgZ2V0IGRhdGEoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5vYnNlcnZhYmxlO1xuICB9XG5cbiAgZ2V0IGlzTG9hZGluZygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5sb2FkaW5nO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFNlcnZpY2VPYnNlcnZhYmxlKFtkYXRhXTogYW55W10pIHtcbiAgICByZXR1cm4gdGhpcy5zZXJ2aWNlLmdldExpc3QoZGF0YSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgY2hlY2tEaXN0aW5jdChkMTogYW55W10sIGQyOiBhbnlbXSkge1xuICAgIGNvbnN0IG9mZnNldCA9IGQxLmxlbmd0aCAtIHRoaXMuZ2V0QXZhaWxhYmxlRmllbGRzKCkubGVuZ3RoO1xuICAgIGZvciAobGV0IGkgPSBkMS5sZW5ndGg7IGkgPj0gMDsgaS0tKSB7XG4gICAgICBpZiAoKGkgPCBvZmZzZXQgJiYgZDJbaV0pIHx8IChpID49IG9mZnNldCAmJiBkMVtpXSAhPT0gZDJbaV0pKSB7XG4gICAgICAgIGlmIChpID49IG9mZnNldCkge1xuICAgICAgICAgIGQyLmZpbGwodW5kZWZpbmVkLCAwLCBvZmZzZXQpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBwcm90ZWN0ZWQgbWFwUGFyYW1zKHZhbHVlczogYW55W10pIHtcbiAgICBjb25zdCBmaWVsZHMgPSB0aGlzLmdldEF2YWlsYWJsZUZpZWxkcygpO1xuICAgIGNvbnN0IG9mZnNldCA9IHZhbHVlcy5sZW5ndGggLSBmaWVsZHMubGVuZ3RoO1xuICAgIGNvbnN0IGRhdGEgPSB7fTtcbiAgICB2YWx1ZXMuc2xpY2Uob2Zmc2V0KS5mb3JFYWNoKCh2LCBpKSA9PiB7XG4gICAgICBpZiAodiAhPT0gdW5kZWZpbmVkICYmIHYgIT09IG51bGwgJiYgdiAhPT0gJycpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB2ID09PSAnYm9vbGVhbicpIHtcbiAgICAgICAgICB2ID0gdiA/ICd0cnVlJyA6ICdmYWxzZSc7XG4gICAgICAgIH1cbiAgICAgICAgZGF0YVtmaWVsZHNbaV1dID0gU3RyaW5nKHYpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiB2YWx1ZXMuc2xpY2UoMCwgb2Zmc2V0KS5jb25jYXQoW2RhdGFdKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTdWJqZWN0cygpOiBCZWhhdmlvclN1YmplY3Q8YW55PltdIHtcbiAgICByZXR1cm4gdGhpcy5nZXRBdmFpbGFibGVGaWVsZHMoKS5tYXAoKG5hbWUpID0+IHtcbiAgICAgIHJldHVybiB0aGlzLmZpZWxkc1tuYW1lXSA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55Pih1bmRlZmluZWQpO1xuICAgIH0pO1xuICB9XG59XG4iLCJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IExvYWRlciB9IGZyb20gJy4vbG9hZGVyJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEluZmluaXRlU2Nyb2xsTG9hZGVyIGV4dGVuZHMgTG9hZGVyIHtcblxuICBwcml2YXRlIGxvYWRNb3JlU3ViamVjdDtcblxuICBsb2FkTW9yZSgpIHtcbiAgICB0aGlzLmxvYWRNb3JlU3ViamVjdC5uZXh0KHRydWUpO1xuICB9XG5cbiAgaGFzTW9yZSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5sYXN0RGF0YSAmJiB0aGlzLmxhc3REYXRhLmhhc01vcmUoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTZXJ2aWNlT2JzZXJ2YWJsZShbbG9hZE1vcmUsIGRhdGFdOiBhbnlbXSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIGxvYWRNb3JlID8gdGhpcy5sYXN0RGF0YS5sb2FkTW9yZSgpIDogc3VwZXIuZ2V0U2VydmljZU9ic2VydmFibGUoW2RhdGFdKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTdWJqZWN0cygpOiBCZWhhdmlvclN1YmplY3Q8YW55PltdIHtcbiAgICB0aGlzLmxvYWRNb3JlU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICAgIGNvbnN0IHN1YmplY3RzID0gc3VwZXIuZ2V0U3ViamVjdHMoKTtcbiAgICBzdWJqZWN0cy51bnNoaWZ0KHRoaXMubG9hZE1vcmVTdWJqZWN0KTtcbiAgICByZXR1cm4gc3ViamVjdHM7XG4gIH1cbn1cbiIsImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgY29tYmluZUxhdGVzdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgZGVib3VuY2VUaW1lLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCwgbWFwLCBtZXJnZU1hcCwgc2hhcmUsIHNoYXJlUmVwbGF5LCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBcGlTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi4vY29yZS9jb2xsZWN0aW9uJztcbmltcG9ydCB7IExvYWRlciB9IGZyb20gJy4vbG9hZGVyJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIFBhZ2VMb2FkZXIgZXh0ZW5kcyBMb2FkZXIge1xuXG4gIHByaXZhdGUgcGFnZVN1YmplY3Q7XG5cbiAgZmlyc3QoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc0ZpcnN0KCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5wYWdlU3ViamVjdC5uZXh0KHRoaXMubGFzdERhdGEuZmlyc3RQYXRoKCkpO1xuICB9XG5cbiAgcHJldmlvdXMoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc1ByZXZpb3VzKCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5wYWdlU3ViamVjdC5uZXh0KHRoaXMubGFzdERhdGEucHJldmlvdXNQYXRoKCkpO1xuICB9XG5cbiAgbmV4dCgpIHtcbiAgICBpZiAoIXRoaXMubGFzdERhdGEgfHwgIXRoaXMubGFzdERhdGEuaGFzTmV4dCgpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLm5leHRQYXRoKCkpO1xuICB9XG5cbiAgbGFzdCgpIHtcbiAgICBpZiAoIXRoaXMubGFzdERhdGEgfHwgIXRoaXMubGFzdERhdGEuaGFzTGFzdCgpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLmxhc3RQYXRoKCkpO1xuICB9XG5cbiAgaGFzRmlyc3QoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNGaXJzdCgpO1xuICB9XG5cbiAgaGFzUHJldmlvdXMoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNQcmV2aW91cygpO1xuICB9XG5cbiAgaGFzTmV4dCgpIHtcbiAgICByZXR1cm4gdGhpcy5sYXN0RGF0YSAmJiB0aGlzLmxhc3REYXRhLmhhc05leHQoKTtcbiAgfVxuXG4gIGhhc0xhc3QoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNMYXN0KCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U2VydmljZU9ic2VydmFibGUoW3BhZ2UsIGRhdGFdOiBhbnlbXSkge1xuICAgIHJldHVybiBwYWdlID8gdGhpcy5zZXJ2aWNlLmRvR2V0KHBhZ2UsIGRhdGEpIDogc3VwZXIuZ2V0U2VydmljZU9ic2VydmFibGUoW2RhdGFdKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTdWJqZWN0cygpOiBCZWhhdmlvclN1YmplY3Q8YW55PltdIHtcbiAgICB0aGlzLnBhZ2VTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+KHVuZGVmaW5lZCk7XG4gICAgY29uc3Qgc3ViamVjdHMgPSBzdXBlci5nZXRTdWJqZWN0cygpO1xuICAgIHN1YmplY3RzLnVuc2hpZnQodGhpcy5wYWdlU3ViamVjdCk7XG4gICAgcmV0dXJuIHN1YmplY3RzO1xuICB9XG59XG4iLCJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMsIEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgJ3JlZmxlY3QtbWV0YWRhdGEnO1xuXG5pbXBvcnQge1xuICBBcGlTZXJ2aWNlLFxuICBEYXRhSW5mb1NlcnZpY2UsXG4gIEFuUmVzdENvbmZpZyxcbiAgQXBpQ29uZmlnLFxuICBBcGlJbnRlcmNlcHRvcixcbiAgQXBpUHJvY2Vzc29yLFxuICBPYmplY3RDb2xsZWN0b3IsXG4gIFJlZmVyZW5jZUludGVyY2VwdG9yXG59IGZyb20gJy4vY29yZSc7XG5pbXBvcnQgeyBFdmVudHNTZXJ2aWNlIH0gZnJvbSAnLi9ldmVudHMnO1xuaW1wb3J0IHtcbiAgQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIsXG4gIExkSnNvbkRhdGFOb3JtYWxpemVyLFxuICBKc29uRGF0YU5vcm1hbGl6ZXJcbn0gZnJvbSAnLi9yZXNwb25zZSc7XG5pbXBvcnQge1xuICBBdXRoSW50ZXJjZXB0b3IsXG4gIEF1dGhTZXJ2aWNlLFxuICBBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UsXG4gIEp3dEF1dGhTZXJ2aWNlLFxuICBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2Vcbn0gZnJvbSAnLi9hdXRoJztcbmltcG9ydCB7XG4gIEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgQ2FjaGVJbnRlcmNlcHRvcixcbiAgTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSxcbiAgTWVtb3J5UmVzcG9uc2VDYWNoZSxcbiAgTm9SZXNwb25zZUNhY2hlLFxuICBSZXNwb25zZUNhY2hlXG59IGZyb20gJy4vY2FjaGUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgSHR0cENsaWVudE1vZHVsZSxcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBblJlc3RNb2R1bGUge1xuICBzdGF0aWMgY29uZmlnKGNvbmZpZz86IEFwaUNvbmZpZyk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogQW5SZXN0TW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBblJlc3RDb25maWcsXG4gICAgICAgICAgdXNlVmFsdWU6IGNvbmZpZyxcbiAgICAgICAgfSxcbiAgICAgICAgRXZlbnRzU2VydmljZSxcbiAgICAgICAgRGF0YUluZm9TZXJ2aWNlLFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBMb2NhbFN0b3JhZ2VSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IE1lbW9yeVJlc3BvbnNlQ2FjaGUsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTm9SZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcuY2FjaGUgfHwgTm9SZXNwb25zZUNhY2hlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UsXG4gICAgICAgICAgdXNlQ2xhc3M6IGNvbmZpZy50b2tlblByb3ZpZGVyIHx8IExvY2FsU3RvcmFnZUF1dGhUb2tlblByb3ZpZGVyU2VydmljZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQXV0aFNlcnZpY2UsXG4gICAgICAgICAgdXNlQ2xhc3M6IGNvbmZpZy5hdXRoU2VydmljZSB8fCBKd3RBdXRoU2VydmljZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEF1dGhJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IFJlZmVyZW5jZUludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICAgICAgICB1c2VDbGFzczogQ2FjaGVJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEFwaUludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfSFRUUF9EQVRBX05PUk1BTElaRVJTLFxuICAgICAgICAgIHVzZUNsYXNzOiBSZWZlcmVuY2VEYXRhTm9ybWFsaXplcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgICAgICAgICB1c2VDbGFzczogTGRKc29uRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEpzb25EYXRhTm9ybWFsaXplcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICBPYmplY3RDb2xsZWN0b3IsXG4gICAgICAgIEFwaVByb2Nlc3NvcixcbiAgICAgICAgQXBpU2VydmljZVxuICAgICAgXVxuICAgIH07XG4gIH1cbiAgc3RhdGljIGNvbmZpZ1dpdGhvdXROb3JtYWxpemVycyhjb25maWc/OiBBcGlDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IEFuUmVzdE1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQW5SZXN0Q29uZmlnLFxuICAgICAgICAgIHVzZVZhbHVlOiBjb25maWcsXG4gICAgICAgIH0sXG4gICAgICAgIEV2ZW50c1NlcnZpY2UsXG4gICAgICAgIERhdGFJbmZvU2VydmljZSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBNZW1vcnlSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IE5vUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICB1c2VDbGFzczogY29uZmlnLmNhY2hlIHx8IE5vUmVzcG9uc2VDYWNoZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcudG9rZW5Qcm92aWRlciB8fCBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEF1dGhTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcuYXV0aFNlcnZpY2UgfHwgSnd0QXV0aFNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBdXRoSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBSZWZlcmVuY2VJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IENhY2hlSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBcGlJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgICAgICAgICB1c2VDbGFzczogUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAgT2JqZWN0Q29sbGVjdG9yLFxuICAgICAgICBBcGlQcm9jZXNzb3IsXG4gICAgICAgIEFwaVNlcnZpY2VcbiAgICAgIF1cbiAgICB9O1xuICB9XG59XG4iXSwibmFtZXMiOlsidHNsaWJfMS5fX2V4dGVuZHMiLCJ0c2xpYl8xLl9fdmFsdWVzIiwiand0X2RlY29kZS5jYWxsIiwicGx1cmFsaXplLnBsdXJhbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBLHFCQWNhLFlBQVksR0FBRyxJQUFJLGNBQWMsQ0FBWSxtQkFBbUIsQ0FBQzs7Ozs7O0FDZDlFLElBRUE7SUFFRSxvQkFBb0IsS0FBSztRQUFMLFVBQUssR0FBTCxLQUFLLENBQUE7S0FBSTtJQUU3QixzQkFBSSw0QkFBSTs7OztRQUFSO1lBQ0UsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQ25COzs7T0FBQTtxQkFSSDtJQVNDLENBQUE7QUFQRCxJQVNBO0lBV0Usd0JBQW9CLEtBQUs7UUFBTCxVQUFLLEdBQUwsS0FBSyxDQUFBO0tBQUk7SUFFN0Isc0JBQUksZ0NBQUk7Ozs7UUFBUjtZQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjs7O09BQUE7eUJBMUJIO0lBMkJDLENBQUE7QUFoQkQ7Ozs7Ozs7SUFxQkUsNkJBQUc7Ozs7SUFBSCxVQUFJLE1BQU07UUFDUixPQUFPLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0tBQ25EOzs7Ozs7SUFFRCw2QkFBRzs7Ozs7SUFBSCxVQUFJLE1BQU0sRUFBRSxJQUFTO1FBQ25CLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztLQUNyRDs7Ozs7SUFFRCwwQ0FBZ0I7Ozs7SUFBaEIsVUFBaUIsTUFBTTtRQUNyQix5QkFBdUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQztLQUN6Qzs7Ozs7SUFFRCxzQ0FBWTs7OztJQUFaLFVBQWEsTUFBTTtRQUNqQix5QkFBbUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQztLQUNyQzs7Ozs7O0lBRUQsMENBQWdCOzs7OztJQUFoQixVQUFpQixNQUFNLEVBQUUsSUFBb0I7UUFDM0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDeEI7Ozs7OztJQUVELHNDQUFZOzs7OztJQUFaLFVBQWEsTUFBTSxFQUFFLElBQWdCO1FBQ25DLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3hCOztnQkF6QkYsVUFBVTs7MEJBN0JYOzs7Ozs7Ozs7O0FDS0E7OztBQUFBO0lBQW1DQSw4QkFBUTtJQWtCekMsb0JBQW9CLE9BQW1CLEVBQVUsSUFBb0I7UUFBRSxlQUFhO2FBQWIsVUFBYSxFQUFiLHFCQUFhLEVBQWIsSUFBYTtZQUFiLDhCQUFhOztRQUFwRix3Q0FDVyxLQUFLLFdBRWY7UUFIbUIsYUFBTyxHQUFQLE9BQU8sQ0FBWTtRQUFVLFVBQUksR0FBSixJQUFJLENBQWdCO1FBRW5FLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQzs7S0FDbkQ7Ozs7Ozs7SUFuQk0sZ0JBQUs7Ozs7OztJQUFaLFVBQWdCLEVBQWlCLEVBQUUsRUFBaUI7UUFDbEQscUJBQU0sSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2pDLHFCQUFNLENBQUMsR0FBRyxJQUFJLFVBQVUsQ0FBSSxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFBLENBQUMsQ0FBQztRQUM3QixFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBQSxDQUFDLENBQUM7UUFDN0IsT0FBTyxDQUFDLENBQUM7S0FDVjtJQU9ELHNCQUFJLDZCQUFLOzs7O1FBQVQ7WUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQ3hCOzs7T0FBQTtJQUVELHNCQUFJLDRCQUFJOzs7O1FBQVI7WUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3ZCOzs7T0FBQTtJQUVELHNCQUFJLGlDQUFTOzs7O1FBQWI7WUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQzNCOzs7T0FBQTs7OztJQUVELDBCQUFLOzs7SUFBTDtRQUNFLHlCQUF3QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFDO0tBQzdEOzs7O0lBRUQsNkJBQVE7OztJQUFSO1FBQ0UseUJBQXdCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUM7S0FDaEU7Ozs7SUFFRCx5QkFBSTs7O0lBQUo7UUFDRSx5QkFBd0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQztLQUM1RDs7OztJQUVELHlCQUFJOzs7SUFBSjtRQUNFLHlCQUF3QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFDO0tBQzVEOzs7O0lBRUQsOEJBQVM7OztJQUFUO1FBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztLQUN4Qjs7OztJQUVELGlDQUFZOzs7SUFBWjtRQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7S0FDM0I7Ozs7SUFFRCw2QkFBUTs7O0lBQVI7UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3ZCOzs7O0lBRUQsNkJBQVE7OztJQUFSO1FBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztLQUN2Qjs7OztJQUVELDZCQUFROzs7SUFBUjtRQUFBLGlCQUlDO1FBSEMsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUNyQixHQUFHLENBQUMsVUFBQyxNQUFxQixJQUFLLE9BQUEsVUFBVSxDQUFDLEtBQUssQ0FBSSxLQUFJLEVBQUUsTUFBTSxDQUFDLEdBQUEsQ0FBQyxDQUNsRSxDQUFDO0tBQ0g7Ozs7SUFFRCw0QkFBTzs7O0lBQVA7UUFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUN2Qjs7OztJQUVELDRCQUFPOzs7SUFBUDtRQUNFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3pCOzs7O0lBRUQsZ0NBQVc7OztJQUFYO1FBQ0UsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7S0FDN0I7Ozs7SUFFRCw2QkFBUTs7O0lBQVI7UUFDRSxPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztLQUMzQjs7OztJQUVELDRCQUFPOzs7SUFBUDtRQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ3ZCO3FCQWhHSDtFQUttQyxLQUFLLEVBNEZ2Qzs7Ozs7O0lDekZEO0lBSUUsbUJBQVksSUFBUyxFQUFFLElBQWU7UUFDcEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7S0FDbEI7Ozs7SUFFRCx3QkFBSTs7O0lBQUo7UUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDbkI7Ozs7SUFFRCwwQkFBTTs7O0lBQU47UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUN0RztvQkF2Qkg7SUF3QkMsQ0FBQTtBQWhCRCxJQWtCQTtJQUFvQ0Esa0NBQVM7SUFHM0Msd0JBQVksSUFBWSxFQUFFLElBQWUsRUFBRSxNQUFtQjtRQUE5RCxZQUNFLGtCQUFNLElBQUksRUFBRSxJQUFJLENBQUMsU0FFbEI7UUFEQyxLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs7S0FDdkI7Ozs7SUFFRCwrQkFBTTs7O0lBQU47UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7S0FDbEI7Ozs7SUFFRCwrQkFBTTs7O0lBQU47UUFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7S0FDckI7eUJBeENIO0VBMEJvQyxTQUFTLEVBZTVDLENBQUE7QUFmRCxJQWdCQTtJQUFtQ0EsaUNBQVM7Ozs7d0JBMUM1QztFQTBDbUMsU0FBUyxFQUFHLENBQUE7QUFBL0MsSUFDQTtJQUFxQ0EsbUNBQVM7Ozs7MEJBM0M5QztFQTJDcUMsU0FBUyxFQUFHLENBQUE7QUFBakQsSUFDQTtJQUFvQ0Esa0NBQVM7Ozs7eUJBNUM3QztFQTRDb0MsU0FBUyxFQUFHLENBQUE7QUFBaEQsSUFDQTtJQUF1Q0EscUNBQVM7Ozs7NEJBN0NoRDtFQTZDdUMsU0FBUyxFQUFHLENBQUE7QUFBbkQsSUFDQTtJQUFzQ0Esb0NBQVM7Ozs7MkJBOUMvQztFQThDc0MsU0FBUyxFQUFHLENBQUE7QUFBbEQscUJBTVcsMkJBQTJCLEdBQUcsSUFBSSxjQUFjLENBQWtCLDZCQUE2QixDQUFDOzs7Ozs7SUNsRDNHO0lBRUUsa0JBQW9CLEtBQWU7UUFBZixVQUFLLEdBQUwsS0FBSyxDQUFVO0tBQUk7SUFFdkMsc0JBQUksMEJBQUk7Ozs7UUFBUjtZQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjs7O09BQUE7bUJBUkg7SUFTQyxDQUFBO0FBUEQsSUFTQTtJQUFzQ0Esb0NBQVE7Ozs7MkJBWDlDO0VBV3NDLFFBQVEsRUFHN0MsQ0FBQTtBQUhELElBS0E7SUFBMkNBLHlDQUFROzs7dUJBQ08sRUFBRTs7O2dDQWpCNUQ7RUFnQjJDLFFBQVEsRUFFbEQsQ0FBQTtBQUZELElBSUE7SUFBb0NBLGtDQUFROzs7bUJBRTlCLElBQUk7d0JBRTZFLEVBQUU7MkJBQ3BDLEVBQUU7NkJBQ3FCLEVBQUU7Ozs7Ozs7SUFJN0UsbUNBQVU7Ozs7Y0FBQyxNQUFZO1FBQzVCLHFCQUFJLE9BQU8sR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBQ2hDLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7O1lBQ2xELEtBQXFCLElBQUEsS0FBQUMsU0FBQSxJQUFJLENBQUMsT0FBTyxDQUFBLGdCQUFBO2dCQUE1QixJQUFNLE1BQU0sV0FBQTtnQkFDZixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLEVBQUU7b0JBQzdCLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUM3RjthQUNGOzs7Ozs7Ozs7UUFDRCxPQUFPLE9BQU8sQ0FBQzs7Ozs7O0lBR1YsNkNBQW9COzs7O1FBQ3pCLHFCQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDcEIsS0FBSyxxQkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxpQkFBaUIsRUFBRTtnQkFDaEQsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN6QjtTQUNGO1FBQ0QsT0FBTyxRQUFRLENBQUM7O3lCQWhEcEI7RUFvQm9DLFFBQVEsRUE4QjNDLENBQUE7QUE5QkQsSUFnQ0E7SUFBcUNELG1DQUFRO0lBRzNDLHlCQUFZLElBQWMsRUFBVSxXQUEyQjtRQUEvRCxZQUNFLGtCQUFNLElBQUksQ0FBQyxTQUNaO1FBRm1DLGlCQUFXLEdBQVgsV0FBVyxDQUFnQjs7S0FFOUQ7SUFFRCxzQkFBSSx1Q0FBVTs7OztRQUFkO1lBQ0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1NBQ3pCOzs7T0FBQTswQkE3REg7RUFvRHFDLFFBQVEsRUFVNUM7Ozs7Ozs7Ozs7Ozs7SUN4RFEsZUFBRzs7OztJQUFWLFVBQVcsR0FBUTtRQUNqQixPQUFPLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0tBQ2pDOzs7OztJQUVNLHFCQUFTOzs7O0lBQWhCLFVBQWlCLEdBQVc7O1lBQzFCLEtBQW1CLElBQUEsS0FBQUMsU0FBQSxXQUFXLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFBLGdCQUFBO2dCQUF0QyxJQUFNLElBQUksV0FBQTtnQkFDYixJQUFJLElBQUksWUFBWSxjQUFjLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxHQUFHLEVBQUU7b0JBQ3ZELE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7Ozs7Ozs7Ozs7S0FDRjs7Ozs7SUFFTSxnQ0FBb0I7Ozs7SUFBM0IsVUFBNEIsR0FBYTtRQUN2QyxxQkFBSSxJQUFJLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9CLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCx5QkFBdUIsSUFBSSxFQUFDO0tBQzdCOzs7OztJQUVNLHVDQUEyQjs7OztJQUFsQyxVQUFtQyxHQUFhO1FBQzlDLHFCQUFJLElBQUksR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDVCxJQUFJLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0QyxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QseUJBQThCLElBQUksRUFBQztLQUNwQzs7Ozs7OztJQUVNLGtDQUFzQjs7Ozs7O0lBQTdCLFVBQThCLEdBQVEsRUFBRSxRQUFnQixFQUFFLElBQVU7UUFDbEUscUJBQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLGdCQUFnQixDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUNuSDtRQUNELE9BQU8sVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztLQUN4Qzs7Ozs7O0lBRU0scUNBQXlCOzs7OztJQUFoQyxVQUFpQyxHQUFhLEVBQUUsTUFBZ0I7UUFDOUQscUJBQUksSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULElBQUksR0FBRyxJQUFJLGVBQWUsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDMUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2QjtRQUNELHlCQUF3QixJQUFJLEVBQUM7S0FDOUI7Ozs7O0lBRU0sZUFBRzs7OztJQUFWLFVBQVcsSUFBYztRQUN2QixXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3RDO3NCQW5Ed0MsSUFBSSxHQUFHLEVBQWlCO3NCQUpuRTs7Ozs7Ozs7Ozs7OztJQ1NFLHVCQUNvRTtRQUFBLGFBQVEsR0FBUixRQUFRO1FBRTFFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUNwQjtLQUNGOzs7OztJQUVNLDhCQUFNOzs7O2NBQUMsS0FBWTtRQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQXNCO1lBQzNDLHFCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDOztnQkFDMUUsS0FBd0IsSUFBQSxLQUFBQSxTQUFBLElBQUksQ0FBQyxNQUFNLENBQUEsZ0JBQUE7b0JBQTlCLElBQU0sU0FBUyxXQUFBO29CQUNsQixJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLFdBQVcsSUFBSSxTQUFTLENBQUMsTUFBTSxLQUFLLEtBQUssQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDL0UsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDdkI7aUJBQ0Y7Ozs7Ozs7Ozs7U0FDRixDQUFDLENBQUM7UUFDSCxPQUFPLEtBQUssQ0FBQzs7O2dCQXBCaEIsVUFBVTs7Ozs0Q0FJTixRQUFRLFlBQUksTUFBTSxTQUFDLDJCQUEyQjs7d0JBVm5EOzs7Ozs7Ozs7Ozs7QUNBQTtJQXFCRSxvQkFDWSxJQUFnQixFQUNoQixNQUFxQixFQUNyQixXQUE0QixFQUNOO1FBSHRCLFNBQUksR0FBSixJQUFJLENBQVk7UUFDaEIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBaUI7UUFDTixXQUFNLEdBQU4sTUFBTTtRQUV0QyxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQy9DOzs7OztJQUVELDRCQUFPOzs7O0lBQVAsVUFBUSxNQUE4RDtRQUNwRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7S0FDM0M7Ozs7O0lBRUQsd0JBQUc7Ozs7SUFBSCxVQUFJLEVBQW1CO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUM7S0FDOUM7Ozs7O0lBRUQsMkJBQU07Ozs7SUFBTixVQUFPLE1BQVc7UUFDaEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQy9EOzs7OztJQUVELGlDQUFZOzs7O0lBQVosVUFBYSxFQUFtQjtRQUM5QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxFQUFFLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3RGOzs7Ozs7SUFFRCwwQkFBSzs7Ozs7SUFBTCxVQUFNLElBQVksRUFBRSxNQUE4RDtRQUFsRixpQkFjQztRQWJDLHFCQUFJLENBQWEsQ0FBQztRQUNsQixJQUFJLE1BQU0sWUFBWSxVQUFVLEVBQUU7WUFDaEMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztTQUNaO2FBQU07WUFDTCxDQUFDLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztZQUNyQixLQUFLLHFCQUFNLEdBQUcsSUFBSSxNQUFNLEVBQUU7Z0JBQ3hCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sR0FBRyxPQUFPLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDM0c7U0FDRjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksY0FBYyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMzRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQzVHLEdBQUcsQ0FBQyxVQUFDLElBQUksSUFBSyxPQUFBLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFBLENBQUMsQ0FDdEYsQ0FBQztLQUNIOzs7OztJQUVELHlCQUFJOzs7O0lBQUosVUFBSyxNQUFXO1FBQWhCLGlCQXVCQztRQXRCQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ2hELHFCQUFJLElBQUksR0FBRyxFQUFFLENBQUM7UUFDZCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRTtZQUM3QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzFDO2FBQU07WUFDTCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUEsQ0FBQyxDQUFDO1NBQ3RHO1FBQ0QscUJBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRWpELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUNuQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFDN0UsSUFBSSxFQUNKLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUNuRCxDQUFDLElBQUksQ0FDSixHQUFHLENBQUMsVUFBQyxJQUFJO1lBQ1AsSUFBSSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ1QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQzdDO1NBQ0YsQ0FBQyxFQUNGLEdBQUcsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBQSxDQUFDLENBQzFELENBQUM7S0FDTDs7Ozs7SUFFRCwyQkFBTTs7OztJQUFOLFVBQU8sTUFBVztRQUFsQixpQkFRQztRQVBDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNsRCxxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkQsSUFBSSxJQUFJLEVBQUU7WUFDUixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQy9HLEdBQUcsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFBLENBQUMsQ0FDNUQsQ0FBQztTQUNIO0tBQ0Y7O2dCQTlFRixVQUFVOzs7O2dCQWhCRixVQUFVO2dCQVFqQixhQUFhO2dCQUxOLGVBQWU7Z0RBcUJuQixNQUFNLFNBQUMsWUFBWTs7cUJBekJ4Qjs7Ozs7OztBQ0FBO0lBZUUsOEJBQzJDO1FBQUEsV0FBTSxHQUFOLE1BQU07S0FDN0M7Ozs7OztJQUVKLHdDQUFTOzs7OztJQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtRQUNwRCxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDOUgsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDOUM7Ozs7O0lBRU8saURBQWtCOzs7O2NBQUMsT0FBeUI7UUFDbEQsT0FBTyxPQUFPLENBQUMsTUFBTSxLQUFLLEtBQUssSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7SUFHOUYsc0RBQXVCOzs7O2NBQUMsT0FBeUI7UUFDdkQscUJBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLE9BQU8sRUFBRSxDQUFDLElBQUksWUFBWSxDQUFDO1lBQ3pCLElBQUksRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUM7WUFDMUgsTUFBTSxFQUFFLEdBQUc7WUFDWCxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUc7U0FDakIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNOLEdBQUcsQ0FBQyxVQUFDLFFBQVE7WUFDWCxJQUFJLFFBQVEsWUFBWSxZQUFZLEVBQUU7Z0JBQ3BDLE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDcEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxZQUFZLENBQUM7aUJBQzVELENBQUMsQ0FBQzthQUNKO1NBQ0YsQ0FBQyxDQUNILENBQUM7OztnQkFoQ0wsVUFBVTs7OztnREFJTixNQUFNLFNBQUMsWUFBWTs7K0JBaEJ4Qjs7Ozs7OztBQ0NBLHFCQVFXLDRCQUE0QixHQUFHLElBQUksY0FBYyxDQUFtQiw4QkFBOEIsQ0FBQzs7Ozs7O0FDVDlHLElBQUE7Ozt1QkFBQTtJQUtDOzs7Ozs7QUNMRDtJQU9JLHlCQUFvQixJQUFxQjtRQUFyQixTQUFJLEdBQUosSUFBSSxDQUFpQjttQkFGaEIsSUFBSSxHQUFHLEVBQWU7S0FFRjs7Ozs7SUFFN0MsNkJBQUc7Ozs7SUFBSCxVQUFJLElBQVM7UUFDWCxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDOUM7Ozs7O0lBRUQsZ0NBQU07Ozs7SUFBTixVQUFPLElBQVM7UUFDZCxxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsSUFBSSxJQUFJLEVBQUU7WUFDUixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNsQjtRQUNELElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3ZCOzs7OztJQUVELDZCQUFHOzs7O0lBQUgsVUFBSSxFQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUN6Qjs7Ozs7SUFFRCw2QkFBRzs7OztJQUFILFVBQUksRUFBVTtRQUNaLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDekI7Ozs7SUFFRCwrQkFBSzs7O0lBQUw7UUFDRSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO0tBQ2xCOztnQkE1QkosVUFBVTs7OztnQkFGRixlQUFlOzswQkFEeEI7Ozs7Ozs7O0lDVUUsc0JBQ1UsVUFDQSxNQUNBO1FBRkEsYUFBUSxHQUFSLFFBQVE7UUFDUixTQUFJLEdBQUosSUFBSTtRQUNKLGNBQVMsR0FBVCxTQUFTO0tBQ2Y7Ozs7OztJQUVKLDhCQUFPOzs7OztJQUFQLFVBQVEsSUFBa0IsRUFBRSxNQUFjO1FBQ3hDLE9BQU8sSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNsRjs7Ozs7SUFFTyx3Q0FBaUI7Ozs7Y0FBQyxJQUFrQjtRQUMxQyxxQkFBSSxJQUFJLENBQUM7UUFDVCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtZQUNoQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5RzthQUFNO1lBQ0wsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hFO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7O1lBQ2hCLEtBQXFCLElBQUEsS0FBQUEsU0FBQSxJQUFJLENBQUMsSUFBSSxDQUFBLGdCQUFBO2dCQUF6QixJQUFNLE1BQU0sV0FBQTtnQkFDZixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUN2Qzs7Ozs7Ozs7O1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sSUFBSSxDQUFDOzs7Ozs7O0lBR04sb0NBQWE7Ozs7Y0FBQyxJQUFrQjtRQUN0QyxJQUFJLElBQUksQ0FBQyxJQUFJLFlBQVksY0FBYyxFQUFFO1lBQ3ZDLHFCQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxRSxLQUFLLHFCQUFNLFFBQVEsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDM0MsSUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzFFLFNBQVM7aUJBQ1Y7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLFlBQVksRUFBRTtvQkFDL0MsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVk7d0JBQzVELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUMzQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztpQkFDM0M7cUJBQU07b0JBQ0wscUJBQU0sSUFBSSxHQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDdEQsUUFBUSxJQUFJO3dCQUNWLEtBQUssTUFBTTs0QkFDVCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFDL0MsTUFBTTt3QkFDUixLQUFLLElBQUk7NEJBQ1AsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFDakQsTUFBTTt3QkFDUixLQUFLLE1BQU0sQ0FBQzt3QkFDWjs0QkFDRSxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDdkMsTUFBTTtxQkFDVDtpQkFDRjthQUNGO1lBQ0QsS0FBSyxxQkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQzdDLHFCQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2pGLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDeEc7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzNCLE9BQU8sTUFBTSxDQUFDO1NBQ2Y7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztTQUNsQjs7O2dCQWpFSixVQUFVOzs7O2dCQVBVLFFBQVE7Z0JBRXBCLGVBQWU7Z0JBRWYsZUFBZTs7dUJBSnhCOzs7Ozs7OztJQ2dCRSx3QkFDMkMsUUFDMEIsYUFDbEQ7UUFGd0IsV0FBTSxHQUFOLE1BQU07UUFDb0IsZ0JBQVcsR0FBWCxXQUFXO1FBQzdELGNBQVMsR0FBVCxTQUFTO1FBRTFCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNwQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztTQUN2QjtLQUNGOzs7Ozs7SUFFRCxrQ0FBUzs7Ozs7SUFBVCxVQUFVLE9BQXlCLEVBQUUsSUFBaUI7UUFBdEQsaUJBa0NDO1FBakNDLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87Z0JBQ25GLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEcsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QscUJBQUksSUFBWSxDQUFDO1FBQ2pCLHFCQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBQzlCLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUNoQyxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNwQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUU7O2dCQUM5QixLQUFxQixJQUFBLEtBQUFBLFNBQUEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUEsZ0JBQUE7b0JBQTFDLElBQU0sTUFBTSxXQUFBO29CQUNmLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO3dCQUM5QyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUNoRjtpQkFDRjs7Ozs7Ozs7O1NBQ0Y7UUFDRCxPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUN0QixPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUM5QixHQUFHLENBQUMsVUFBQyxRQUFRO1lBQ1gsSUFBSSxRQUFRLFlBQVksWUFBWSxFQUFFO2dCQUNwQyxxQkFBTSxXQUFXLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7O29CQUN6RCxLQUF5QixJQUFBLEtBQUFBLFNBQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQSxnQkFBQTt3QkFBcEMsSUFBTSxVQUFVLFdBQUE7d0JBQ25CLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFBRTs0QkFDcEMsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7eUJBQy9HO3FCQUNGOzs7Ozs7Ozs7YUFDRjtZQUNELE9BQU8sUUFBUSxDQUFDOztTQUNqQixDQUFDLENBQ0gsQ0FBQzs7S0FDSDs7Z0JBL0NGLFVBQVU7Ozs7Z0RBSU4sTUFBTSxTQUFDLFlBQVk7NENBQ25CLFFBQVEsWUFBSSxNQUFNLFNBQUMsNEJBQTRCO2dCQVAzQyxZQUFZOzt5QkFYckI7Ozs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7OztJQWlCRSwrQkFBTzs7Ozs7SUFBUCxVQUFRLElBQVMsRUFBRSxFQUFVO1FBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDcEI7O2dCQWxCRixVQUFVOzt3QkFGWDs7cUJBdUJXLHFCQUFxQixHQUFHLElBQUksY0FBYyxDQUFrQix1QkFBdUIsQ0FBQzs7Ozs7OztJQ25CMURELG1DQUFhOzs7Ozs7OztJQUNoRCw2QkFBRzs7OztJQUFILFVBQUksRUFBVTtRQUNaLE9BQU8sS0FBSyxDQUFDO0tBQ2Q7Ozs7OztJQUVELDZCQUFHOzs7OztJQUFILFVBQUksRUFBVSxFQUFFLElBQVM7S0FDeEI7Ozs7SUFFRCwrQkFBSzs7O0lBQUw7S0FDQzs7Ozs7SUFFRCw2QkFBRzs7OztJQUFILFVBQUksRUFBVTtLQUNiOzs7OztJQUVELGdDQUFNOzs7O0lBQU4sVUFBTyxFQUFVO0tBQ2hCOzs7Ozs7SUFFRCxrQ0FBUTs7Ozs7SUFBUixVQUFTLEVBQVUsRUFBRSxLQUFhO0tBQ2pDOzs7Ozs7SUFFRCxpQ0FBTzs7Ozs7SUFBUCxVQUFRLElBQVMsRUFBRSxFQUFVO0tBQzVCOztnQkF0QkYsVUFBVTs7MEJBSFg7RUFJcUMsYUFBYTs7Ozs7OztJQ2FoRCwwQkFDMkMsUUFDeEIsTUFDK0I7UUFGUCxXQUFNLEdBQU4sTUFBTTtRQUM5QixTQUFJLEdBQUosSUFBSTtRQUMyQixrQkFBYSxHQUFiLGFBQWE7S0FDM0Q7Ozs7OztJQUVKLG9DQUFTOzs7OztJQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtRQUF0RCxpQkFzQ0M7UUFyQ0MscUJBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87Z0JBQ25GLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEcsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QscUJBQU0sU0FBUyxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksZUFBZSxDQUFDO1FBQzVILHFCQUFJLEtBQVUsQ0FBQzs7WUFDZixLQUEyQixJQUFBLEtBQUFDLFNBQUEsSUFBSSxDQUFDLGFBQWEsQ0FBQSxnQkFBQTtnQkFBeEMsSUFBTSxZQUFZLFdBQUE7Z0JBQ3JCLElBQUksWUFBWSxZQUFZLFNBQVMsRUFBRTtvQkFDckMsS0FBSyxHQUFHLFlBQVksQ0FBQztvQkFDckIsTUFBTTtpQkFDUDthQUNGOzs7Ozs7Ozs7UUFDRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO1lBQzVCLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25CLE9BQU8sRUFBRSxDQUFDLElBQUksWUFBWSxDQUFDO2dCQUN6QixJQUFJLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQ3JCLE1BQU0sRUFBRSxHQUFHO2dCQUNYLEdBQUcsRUFBRSxPQUFPLENBQUMsR0FBRzthQUNqQixDQUFDLENBQUMsQ0FBQztTQUNMO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDOUIsR0FBRyxDQUFDLFVBQUMsUUFBMkI7WUFDOUIsSUFBSSxRQUFRLENBQUMsSUFBSSxFQUFFO2dCQUNqQixxQkFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLElBQUksRUFBRTtvQkFDUixLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO3dCQUN0QixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN2QztpQkFDRjthQUNGO1NBQ0YsQ0FBQyxDQUNILENBQUM7O0tBQ0g7O2dCQS9DRixVQUFVOzs7O2dEQUlOLE1BQU0sU0FBQyxZQUFZO2dCQVRVLGVBQWU7NENBVzVDLE1BQU0sU0FBQyxxQkFBcUI7OzJCQXBCakM7Ozs7Ozs7O0lDTXlDRCx1Q0FBYTtJQU1wRCw2QkFDbUIsV0FDd0I7UUFGM0MsWUFJRSxpQkFBTyxTQUNSO1FBSmtCLGVBQVMsR0FBVCxTQUFTO1FBQ2UsWUFBTSxHQUFOLE1BQU07c0JBTkosRUFBRTt3QkFFQSxFQUFFOztLQU9oRDs7Ozs7O0lBRUQsaUNBQUc7Ozs7O0lBQUgsVUFBSSxFQUFVLEVBQUUsSUFBUztRQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztLQUM3Qjs7OztJQUVELG1DQUFLOzs7SUFBTDtRQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDeEI7Ozs7O0lBRUQsaUNBQUc7Ozs7SUFBSCxVQUFJLEVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQy9COzs7OztJQUVELG9DQUFNOzs7O0lBQU4sVUFBTyxFQUFVO1FBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDM0I7Ozs7OztJQUVELHNDQUFROzs7OztJQUFSLFVBQVMsRUFBVSxFQUFFLEtBQWE7UUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDMUI7Ozs7O0lBRUQsaUNBQUc7Ozs7SUFBSCxVQUFJLEVBQVU7UUFDWixxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzlFLE9BQU8sSUFBSSxLQUFLLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQztLQUM1RDs7Z0JBckNGLFVBQVU7Ozs7Z0JBSEYsZUFBZTtnREFZbkIsTUFBTSxTQUFDLFlBQVk7OzhCQWR4QjtFQU15QyxhQUFhOzs7Ozs7QUNKdEQsSUFHQTs7Ozs7Ozs7SUFDRSwyQ0FBUzs7Ozs7SUFBVCxVQUFVLFFBQTJCLEVBQUUsSUFBYTtRQUNsRCxxQkFBTSxDQUFDLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUNyQixDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsQ0FBQyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUM1QixDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNaLE9BQU8sQ0FBQyxDQUFDO0tBQ1Y7Ozs7O0lBRUQsMENBQVE7Ozs7SUFBUixVQUFTLElBQVk7UUFDbkIsT0FBTyxJQUFJLEtBQUssWUFBWSxDQUFDO0tBQzlCO2tDQWpCSDtJQWtCQzs7Ozs7O0lDWkQ7Ozs7Ozs7O0lBQ0Usd0NBQVM7Ozs7O0lBQVQsVUFBVSxRQUEyQixFQUFFLElBQWE7UUFDbEQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNwQzs7Ozs7SUFFRCx1Q0FBUTs7OztJQUFSLFVBQVMsSUFBWTtRQUNuQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUsscUJBQXFCLENBQUM7S0FDckQ7Ozs7Ozs7SUFFTyxzQ0FBTzs7Ozs7O2NBQUMsSUFBSSxFQUFFLElBQVUsRUFBRSxVQUFvQjtRQUNwRCxxQkFBTSxDQUFDLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsVUFBVSxHQUFHLFVBQVUsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssa0JBQWtCLENBQUM7UUFDbEUscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3RILElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRTtZQUNoQixxQkFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQztZQUMzQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDbkYsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ3RCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDakQsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3ZELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDL0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUNoQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzVCO2FBQ0Y7WUFDRCxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDO1lBQ3hELHFCQUFNLFNBQVMsR0FBRyxhQUFhLENBQUM7WUFDaEMscUJBQUksT0FBTyxTQUFBLENBQUM7WUFDWixPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9DLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkQsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7O2dCQUNaLEtBQXFCLElBQUEsVUFBQUMsU0FBQSxLQUFLLENBQUEsNEJBQUE7b0JBQXJCLElBQU0sTUFBTSxrQkFBQTtvQkFDZixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7aUJBQ25DOzs7Ozs7Ozs7U0FDRjthQUFNO1lBQ0wsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNaLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDL0MsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDZjtpQkFBTTtnQkFDTCxLQUFLLHFCQUFNLEdBQUcsSUFBSSxJQUFJLEVBQUU7b0JBQ3RCLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ3pELFNBQVM7cUJBQ1Y7b0JBQ0QscUJBQU0sWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUM1QyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLFlBQVksSUFBSSxXQUFXLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbkgsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsWUFBWSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDdEc7YUFDRjtTQUNGO1FBQ0QsT0FBTyxDQUFDLENBQUM7OzsrQkE1RGI7SUE4REM7Ozs7OztJQ3hERDs7Ozs7Ozs7SUFDRSxzQ0FBUzs7Ozs7SUFBVCxVQUFVLFFBQTJCLEVBQUUsSUFBWTtRQUNqRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUNqSDs7Ozs7SUFFRCxxQ0FBUTs7OztJQUFSLFVBQVMsSUFBWTtRQUNuQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssa0JBQWtCLENBQUM7S0FDbEQ7Ozs7Ozs7O0lBRU8sb0NBQU87Ozs7Ozs7Y0FBQyxJQUFJLEVBQUUsSUFBUyxFQUFFLFVBQW1CLEVBQUUsT0FBcUI7UUFDekUscUJBQU0sQ0FBQyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDMUIscUJBQU0sY0FBYyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksR0FBRyxTQUFTLENBQUM7UUFDdEcscUJBQU0sSUFBSSxHQUFHLGNBQWMsSUFBSSxVQUFVLEdBQUcsY0FBYyxHQUFHLGNBQWMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxTQUFTLENBQUM7UUFDL0csQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDZCxJQUFJLENBQUMsQ0FBQyxVQUFVLEVBQUU7WUFDaEIsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLElBQUksSUFBSSxDQUFDLENBQUM7WUFDbkUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsSUFBSSxTQUFTLENBQUM7WUFDeEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxTQUFTLENBQUM7WUFDMUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxTQUFTLENBQUM7WUFDdEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxTQUFTLENBQUM7WUFDdEQsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtnQkFDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO2FBQ3JCO1lBQ0QsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ3JELHFCQUFNLFNBQVMsR0FBRyxhQUFhLENBQUM7WUFDaEMscUJBQUksT0FBTyxTQUFBLENBQUM7WUFDWixPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9DLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkQsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7O2dCQUNaLEtBQXFCLElBQUEsU0FBQUEsU0FBQSxJQUFJLENBQUEsMEJBQUE7b0JBQXBCLElBQU0sTUFBTSxpQkFBQTtvQkFDZixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7aUJBQ2xEOzs7Ozs7Ozs7U0FDRjthQUFNO1lBQ0wsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNaLEtBQUsscUJBQU0sR0FBRyxJQUFJLElBQUksRUFBRTtnQkFDdEIscUJBQU0sWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLFlBQVksSUFBSSxXQUFXLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbkgsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsWUFBWSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUN0RztTQUNGO1FBQ0QsT0FBTyxDQUFDLENBQUM7Ozs2QkFuRGI7SUFxREM7Ozs7Ozs7Ozs7OztJQzNDOENELDZDQUFhO0lBSTFELG1DQUNVLFdBQ0EsV0FDQSxNQUNzQjtRQUpoQyxZQU1FLGlCQUFPLFNBU1I7UUFkUyxlQUFTLEdBQVQsU0FBUztRQUNULGVBQVMsR0FBVCxTQUFTO1FBQ1QsVUFBSSxHQUFKLElBQUk7UUFDa0IsWUFBTSxHQUFOLE1BQU07NEJBTmhCLGVBQWU7UUFTbkMscUJBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxDQUFDO1FBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEdBQUEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQUM7WUFDbEYscUJBQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQy9DLHFCQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxTQUFTLElBQUksU0FBUyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRTtnQkFDL0QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNqQjtTQUNGLENBQUMsQ0FBQzs7S0FDSjs7Ozs7SUFFRCx1Q0FBRzs7OztJQUFILFVBQUksRUFBVTtRQUNaLEVBQUUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsRSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDaEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3hEO0tBQ0Y7Ozs7OztJQUVELHVDQUFHOzs7OztJQUFILFVBQUksRUFBVSxFQUFFLElBQVM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQzVCOzs7Ozs7SUFFRCw0Q0FBUTs7Ozs7SUFBUixVQUFTLEVBQVUsRUFBRSxLQUFhO1FBQ2hDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLEdBQUcsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0tBQy9EOzs7OztJQUVELHVDQUFHOzs7O0lBQUgsVUFBSSxFQUFVO1FBQ1osRUFBRSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLEdBQUcsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2xFLHFCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN2RixxQkFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN4QyxPQUFPLENBQUMsQ0FBQyxTQUFTLElBQUksUUFBUSxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDbEY7Ozs7O0lBRUQsMENBQU07Ozs7SUFBTixVQUFPLEVBQVU7UUFDZixZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDL0MsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztRQUN6RCxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDN0QsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQzFCOzs7O0lBRUQseUNBQUs7OztJQUFMO1FBQUEsaUJBSUM7UUFIQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBQSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQztZQUNuRixZQUFZLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVCLENBQUMsQ0FBQztLQUNKOzs7Ozs7O0lBRU8seUNBQUs7Ozs7OztjQUFDLEVBQVUsRUFBRSxJQUFTLEVBQUUsUUFBaUI7UUFDcEQscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLHFCQUFJLE1BQVcsQ0FBQztRQUNoQixJQUFJLElBQUksWUFBWSxjQUFjLEVBQUU7WUFDbEMsTUFBTSxHQUFHLEVBQUUsQ0FBQzs7Z0JBQ1osS0FBbUIsSUFBQSxTQUFBQyxTQUFBLElBQUksQ0FBQSwwQkFBQTtvQkFBbEIsSUFBTSxJQUFJLGlCQUFBO29CQUNiLHFCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQzFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDbkM7Ozs7Ozs7OztZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNuRjthQUFNO1lBQ0wscUJBQU0sVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUN0QixNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ1osS0FBSyxxQkFBTSxHQUFHLElBQUksSUFBSSxFQUFFO2dCQUN0QixxQkFBSSxRQUFRLFNBQUssQ0FBQztnQkFDbEIsSUFBSSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLEVBQUU7b0JBQ2pDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDckM7Z0JBQ0QsSUFBSSxRQUFRLEVBQUU7b0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDNUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7aUJBQ2pDO3FCQUFNO29CQUNMLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3pCO2FBQ0Y7WUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxHQUFHLEVBQUUsRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5RixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7U0FDekY7UUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDcEYsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Ozs7OztJQUdoQiwyQ0FBTzs7OztjQUFDLEVBQVU7UUFDeEIscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDckUscUJBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNsRCxxQkFBTSxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNwQyxJQUFJLGNBQWMsRUFBRTtZQUNsQixRQUFRLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUMzQixRQUFRLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQztZQUMvQixRQUFRLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNELFFBQVEsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDOztnQkFDbkIsS0FBcUIsSUFBQSxTQUFBQSxTQUFBLElBQUksQ0FBQSwwQkFBQTtvQkFBcEIsSUFBTSxNQUFNLGlCQUFBO29CQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztpQkFDMUM7Ozs7Ozs7OztTQUNGO2FBQU07WUFDTCxRQUFRLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUM1QixRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ25DLFFBQVEsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0YsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDckIscUJBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNGLElBQUksVUFBVSxFQUFFO2dCQUNkLEtBQUsscUJBQU0sR0FBRyxJQUFJLFVBQVUsRUFBRTtvQkFDNUIsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lCQUNwRDthQUNGO1NBQ0Y7UUFDRCxPQUFPLFFBQVEsQ0FBQzs7Ozs7OztJQUdWLHFEQUFpQjs7OztjQUFDLEVBQVU7UUFDbEMscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLElBQUksSUFBSSxFQUFFO1lBQ1IscUJBQU0sSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3BDLEtBQUsscUJBQU0sR0FBRyxJQUFJLElBQUksRUFBRTtnQkFDdEIsSUFBSSxHQUFHLEtBQUssT0FBTyxFQUFFO29CQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN2QjthQUNGO1lBQ0QsT0FBTyxJQUFJLENBQUM7U0FDYjs7Ozs7O0lBR0ssZ0RBQVk7Ozs7Y0FBQyxFQUFVO1FBQzdCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDOzs7Ozs7SUFHekUsZ0RBQVk7Ozs7Y0FBQyxFQUFVO1FBQzdCLE9BQU8sTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQzs7Ozs7O0lBR3BFLG1EQUFlOzs7O2NBQUMsRUFBVTtRQUNoQyxPQUFPLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsRUFBRSxDQUFDLENBQUM7OztnQkE5SXhFLFVBQVU7Ozs7Z0JBUEYsZUFBZTtnQkFDZixZQUFZO2dCQUVJLGVBQWU7Z0RBYW5DLE1BQU0sU0FBQyxZQUFZOztvQ0FsQnhCO0VBVStDLGFBQWE7Ozs7Ozs7Ozs7Ozs7O0FDUDVEOzs7QUFBQTs7O3NCQUhBO0lBZ0JDOzs7Ozs7Ozs7QUNkRDs7O0FBQUE7OzttQ0FGQTtJQU9DOzs7Ozs7QUNORDs7eUJBSXNCLG1CQUFtQjs7Ozs7SUFFdkMsa0RBQUc7OztJQUFIO1FBQ0UsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUM3Qzs7Ozs7SUFFRCxrREFBRzs7OztJQUFILFVBQUksS0FBYTtRQUNmLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztLQUM3Qzs7OztJQUVELHFEQUFNOzs7SUFBTjtRQUNFLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0tBQ3pDOzs7O0lBRUQsb0RBQUs7OztJQUFMO1FBQ0UsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUM7S0FDckQ7O2dCQWxCRixVQUFVOzsrQ0FIWDs7Ozs7OztBQ0FBO0lBY0UseUJBQW1CLElBQWlCLEVBQWtDO1FBQW5ELFNBQUksR0FBSixJQUFJLENBQWE7UUFBa0MsV0FBTSxHQUFOLE1BQU07S0FBZTs7Ozs7O0lBRTNGLG1DQUFTOzs7OztJQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtRQUNwRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUNqSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDN0I7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ3RCLFVBQVUsRUFBRTtvQkFDVixhQUFhLEVBQUUsWUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBSTtpQkFDaEQ7YUFDRixDQUFDLENBQUM7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUM3Qjs7Z0JBakJGLFVBQVU7Ozs7Z0JBSEYsV0FBVztnREFNcUIsTUFBTSxTQUFDLFlBQVk7OzBCQWQ1RDs7Ozs7OztBQ0FBO0lBWUUsd0JBQ1UsTUFDQSxPQUN3QjtRQUZ4QixTQUFJLEdBQUosSUFBSTtRQUNKLFVBQUssR0FBTCxLQUFLO1FBQ21CLFdBQU0sR0FBTixNQUFNO0tBQ3BDOzs7O0lBRUosaUNBQVE7OztJQUFSO1FBQ0UsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ3pCOzs7OztJQUVELGlDQUFROzs7O0lBQVIsVUFBUyxLQUFhO1FBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3ZCOzs7O0lBRUQsK0JBQU07OztJQUFOO1FBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztLQUNyQjs7OztJQUVELG1DQUFVOzs7SUFBVjtRQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztLQUMzQjs7OztJQUVELGdDQUFPOzs7SUFBUDtRQUNFLE9BQU9DLElBQWUsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztLQUN6Qzs7Ozs7O0lBRUQsK0JBQU07Ozs7O0lBQU4sVUFBTyxRQUFnQixFQUFFLFFBQWdCO1FBQXpDLGlCQVNDO1FBUkMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUN6QyxLQUFLLEVBQUUsUUFBUTtZQUNmLFFBQVEsRUFBRSxRQUFRO1NBQ25CLENBQUMsQ0FBQyxJQUFJLENBQ0wsR0FBRyxDQUFDLFVBQUMsSUFBUztZQUNaLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzNCLENBQUMsQ0FDSCxDQUFDO0tBQ0g7O2dCQXRDRixVQUFVOzs7O2dCQVJGLFVBQVU7Z0JBTVYsd0JBQXdCO2dEQVE1QixNQUFNLFNBQUMsWUFBWTs7eUJBZnhCOzs7Ozs7Ozs7Ozs7QUNBQTs7OztBQVFBLGtCQUEwQixJQUF1QjtJQUF2QixxQkFBQSxFQUFBLFNBQXVCO0lBRS9DLE9BQU8sVUFBQyxNQUFnQjtRQUN0QixxQkFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztLQUN6QixDQUFDO0NBQ0g7Ozs7OztBQ2ZEOzs7O0FBUUEsa0JBQTBCLElBQXVCO0lBQXZCLHFCQUFBLEVBQUEsU0FBdUI7SUFFL0MsT0FBTyxVQUFDLE1BQVcsRUFBRSxHQUFXO1FBQzlCLHFCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEtBQUssQ0FBQztRQUM3QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFJLEtBQUssQ0FBQztLQUMxRCxDQUFDO0NBQ0g7Ozs7OztBQ2ZEOzs7OztBQUdBLHFCQUE0QixNQUFnQixFQUFFLElBQWE7SUFFekQsT0FBTyxVQUFDLE1BQWdCO1FBQ3RCLHFCQUFNLElBQUksR0FBRyxXQUFXLENBQUMseUJBQXlCLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLEdBQUcsR0FBR0MsTUFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RILElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztLQUNsQyxDQUFDO0NBQ0g7Ozs7OztBQ1ZEOzs7OztBQUVBLGdCQUF1QixVQUFrQixFQUFFLE1BQWdCO0lBRXpELE9BQU8sVUFBQyxNQUFXLEVBQUUsR0FBVyxFQUFFLEtBQXNCO1FBQXRCLHNCQUFBLEVBQUEsaUJBQXNCO1FBQ3RELFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNoRSxJQUFJLEVBQUUsVUFBVTtZQUNoQixLQUFLLEVBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsY0FBYyxPQUFRLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQy9ELE1BQU0sRUFBRSxNQUFNLElBQUksS0FBSztZQUN2QixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUMsQ0FBQztLQUNKLENBQUM7Q0FDSDs7Ozs7Ozs7OztBQ1ZELGlCQUF5QixPQUEyRDtJQUVsRixPQUFPLFVBQUMsTUFBVztRQUNqQixxQkFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUMzQyxNQUFNO1lBQ2YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTtnQkFDakIsS0FBSyxFQUFFLGNBQU0sT0FBQSxNQUFNLENBQUMsS0FBSyxHQUFBO2dCQUN6QixNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sSUFBSSxLQUFLO2dCQUM5QixPQUFPLEVBQUUsS0FBSzthQUNmLENBQUMsQ0FBQzs7O1lBTkwsS0FBcUIsSUFBQSxZQUFBRixTQUFBLE9BQU8sQ0FBQSxnQ0FBQTtnQkFBdkIsSUFBTSxNQUFNLG9CQUFBO3dCQUFOLE1BQU07YUFPaEI7Ozs7Ozs7Ozs7S0FDRixDQUFDO0NBQ0g7Ozs7OztBQ2ZEOzs7QUFFQTtJQUVFLE9BQU8sVUFBQyxNQUFXLEVBQUUsR0FBb0IsRUFBRSxLQUFzQjtRQUE1QyxvQkFBQSxFQUFBLGVBQW9CO1FBQUUsc0JBQUEsRUFBQSxpQkFBc0I7UUFDL0QsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLEdBQUcsVUFBQyxNQUFXO1lBQ3RFLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxjQUFjLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDL0UsQ0FBQztLQUNILENBQUM7Q0FDSDs7Ozs7O0FDVEQ7Ozs7O0FBRUEscUJBQTZCLElBQWMsRUFBRSxJQUFhO0lBRXhELE9BQU8sVUFBQyxNQUFXLEVBQUUsR0FBVyxFQUFFLEtBQVU7UUFDMUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEdBQUc7WUFDdkUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7WUFDNUMsSUFBSSxFQUFFLElBQUksSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUU7U0FDdEYsQ0FBQztLQUNILENBQUM7Q0FDSDs7Ozs7O0FDVkQ7OztBQUVBO0lBRUUsT0FBTyxVQUFDLE1BQVcsRUFBRSxHQUFXO1FBQzlCLHFCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDO0tBQ2YsQ0FBQztDQUNIOzs7Ozs7Ozs7Ozs7OztBQ0hEOzs7QUFBQTtJQVNFLGdCQUFzQixPQUFtQixFQUFFLE9BQVcsRUFBRSxNQUFVO1FBQXZCLHdCQUFBLEVBQUEsV0FBVztRQUFFLHVCQUFBLEVBQUEsVUFBVTtRQUFsRSxpQkFpQkM7UUFqQnFCLFlBQU8sR0FBUCxPQUFPLENBQVk7c0JBUGYsRUFBRTt1QkFFVixLQUFLO1FBTXJCLElBQUksQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNoRCxJQUFJLENBQ0gsTUFBTSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLEVBQUUsRUFDdEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxFQUNyQixvQkFBb0IsQ0FBQyxVQUFDLEVBQVMsRUFBRSxFQUFTLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBQSxDQUFDLEVBQzFFLEdBQUcsQ0FBQyxVQUFDLE1BQWEsSUFBSyxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUEsQ0FBQyxFQUM5QyxRQUFRLENBQUMsVUFBQyxJQUFJO1lBQ1osS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDcEIsT0FBTyxLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUN6QyxHQUFHLENBQUMsVUFBQyxDQUFrQjtnQkFDckIsS0FBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7Z0JBQ2xCLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQ3RCLENBQUMsQ0FDSCxDQUFDO1NBQ0gsQ0FBQyxDQUNILENBQUM7S0FDTDs7Ozs7SUFFRCxzQkFBSzs7OztJQUFMLFVBQU0sSUFBSTtRQUNSLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUMxQjtJQUVELHNCQUFJLHdCQUFJOzs7O1FBQVI7WUFDRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7U0FDeEI7OztPQUFBO0lBRUQsc0JBQUksNkJBQVM7Ozs7UUFBYjtZQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUNyQjs7O09BQUE7Ozs7O0lBRVMscUNBQW9COzs7O0lBQTlCLFVBQStCLEVBQWE7WUFBYixrQkFBYSxFQUFaLFlBQUk7UUFDbEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNuQzs7Ozs7O0lBRVMsOEJBQWE7Ozs7O0lBQXZCLFVBQXdCLEVBQVMsRUFBRSxFQUFTO1FBQzFDLHFCQUFNLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU0sQ0FBQztRQUM1RCxLQUFLLHFCQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbkMsSUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUM3RCxJQUFJLENBQUMsSUFBSSxNQUFNLEVBQUU7b0JBQ2YsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2lCQUMvQjtnQkFDRCxPQUFPLEtBQUssQ0FBQzthQUNkO1NBQ0Y7UUFDRCxPQUFPLElBQUksQ0FBQztLQUNiOzs7OztJQUVTLDBCQUFTOzs7O0lBQW5CLFVBQW9CLE1BQWE7UUFDL0IscUJBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQ3pDLHFCQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDN0MscUJBQU0sSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNoQixNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxLQUFLLFNBQVMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQzdDLElBQUksT0FBTyxDQUFDLEtBQUssU0FBUyxFQUFFO29CQUMxQixDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sR0FBRyxPQUFPLENBQUM7aUJBQzFCO2dCQUNELElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDN0I7U0FDRixDQUFDLENBQUM7UUFDSCxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDL0M7Ozs7SUFFUyw0QkFBVzs7O0lBQXJCO1FBQUEsaUJBSUM7UUFIQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUk7WUFDeEMsT0FBTyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksZUFBZSxDQUFNLFNBQVMsQ0FBQyxDQUFDO1NBQ2hFLENBQUMsQ0FBQztLQUNKO2lCQWpGSDtJQWtGQzs7Ozs7Ozs7O0FDL0VEOzs7QUFBQTtJQUFtREQsd0NBQU07Ozs7Ozs7SUFJdkQsdUNBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDakM7Ozs7SUFFRCxzQ0FBTzs7O0lBQVA7UUFDRSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUNqRDs7Ozs7SUFFUyxtREFBb0I7Ozs7SUFBOUIsVUFBK0IsRUFBdUI7WUFBdkIsa0JBQXVCLEVBQXRCLGdCQUFRLEVBQUUsWUFBSTtRQUM1QyxPQUFPLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFHLGlCQUFNLG9CQUFvQixZQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUNqRjs7OztJQUVTLDBDQUFXOzs7SUFBckI7UUFDRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQzNELHFCQUFNLFFBQVEsR0FBRyxpQkFBTSxXQUFXLFdBQUUsQ0FBQztRQUNyQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN2QyxPQUFPLFFBQVEsQ0FBQztLQUNqQjsrQkF4Qkg7RUFHbUQsTUFBTSxFQXNCeEQ7Ozs7Ozs7OztBQ25CRDs7O0FBQUE7SUFBeUNBLDhCQUFNOzs7Ozs7O0lBSTdDLDBCQUFLOzs7SUFBTDtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUMvQyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7S0FDbEQ7Ozs7SUFFRCw2QkFBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDbEQsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO0tBQ3JEOzs7O0lBRUQseUJBQUk7OztJQUFKO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxFQUFFO1lBQzlDLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztLQUNqRDs7OztJQUVELHlCQUFJOzs7SUFBSjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUM5QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7S0FDakQ7Ozs7SUFFRCw2QkFBUTs7O0lBQVI7UUFDRSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztLQUNsRDs7OztJQUVELGdDQUFXOzs7SUFBWDtRQUNFLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO0tBQ3JEOzs7O0lBRUQsNEJBQU87OztJQUFQO1FBQ0UsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDakQ7Ozs7SUFFRCw0QkFBTzs7O0lBQVA7UUFDRSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUNqRDs7Ozs7SUFFUyx5Q0FBb0I7Ozs7SUFBOUIsVUFBK0IsRUFBbUI7WUFBbkIsa0JBQW1CLEVBQWxCLFlBQUksRUFBRSxZQUFJO1FBQ3hDLE9BQU8sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxpQkFBTSxvQkFBb0IsWUFBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDbkY7Ozs7SUFFUyxnQ0FBVzs7O0lBQXJCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLGVBQWUsQ0FBUyxTQUFTLENBQUMsQ0FBQztRQUMxRCxxQkFBTSxRQUFRLEdBQUcsaUJBQU0sV0FBVyxXQUFFLENBQUM7UUFDckMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkMsT0FBTyxRQUFRLENBQUM7S0FDakI7cUJBL0RIO0VBTXlDLE1BQU0sRUEwRDlDOzs7Ozs7Ozs7OztBQ2hFRDs7Ozs7OztJQTJDUyxtQkFBTTs7OztJQUFiLFVBQWMsTUFBa0I7UUFDOUIsT0FBTztZQUNMLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFNBQVMsRUFBRTtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsWUFBWTtvQkFDckIsUUFBUSxFQUFFLE1BQU07aUJBQ2pCO2dCQUNELGFBQWE7Z0JBQ2IsZUFBZTtnQkFDZjtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUsZUFBZTtvQkFDekIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGFBQWE7b0JBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsS0FBSyxJQUFJLGVBQWU7aUJBQzFDO2dCQUNEO29CQUNFLE9BQU8sRUFBRSx3QkFBd0I7b0JBQ2pDLFFBQVEsRUFBRSxNQUFNLENBQUMsYUFBYSxJQUFJLG9DQUFvQztpQkFDdkU7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLFdBQVc7b0JBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsV0FBVyxJQUFJLGNBQWM7aUJBQy9DO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxlQUFlO29CQUN6QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsY0FBYztvQkFDeEIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0QsZUFBZTtnQkFDZixZQUFZO2dCQUNaLFVBQVU7YUFDWDtTQUNGLENBQUM7S0FDSDs7Ozs7SUFDTSxxQ0FBd0I7Ozs7SUFBL0IsVUFBZ0MsTUFBa0I7UUFDaEQsT0FBTztZQUNMLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFNBQVMsRUFBRTtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsWUFBWTtvQkFDckIsUUFBUSxFQUFFLE1BQU07aUJBQ2pCO2dCQUNELGFBQWE7Z0JBQ2IsZUFBZTtnQkFDZjtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUsZUFBZTtvQkFDekIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGFBQWE7b0JBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsS0FBSyxJQUFJLGVBQWU7aUJBQzFDO2dCQUNEO29CQUNFLE9BQU8sRUFBRSx3QkFBd0I7b0JBQ2pDLFFBQVEsRUFBRSxNQUFNLENBQUMsYUFBYSxJQUFJLG9DQUFvQztpQkFDdkU7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLFdBQVc7b0JBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsV0FBVyxJQUFJLGNBQWM7aUJBQy9DO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxlQUFlO29CQUN6QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsY0FBYztvQkFDeEIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0QsZUFBZTtnQkFDZixZQUFZO2dCQUNaLFVBQVU7YUFDWDtTQUNGLENBQUM7S0FDSDs7Z0JBdkpGLFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsZ0JBQWdCO3FCQUNqQjtpQkFDRjs7dUJBekNEOzs7Ozs7Ozs7Ozs7Ozs7In0=