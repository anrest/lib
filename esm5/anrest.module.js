/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import 'reflect-metadata';
import { ApiService, DataInfoService, AnRestConfig, ApiInterceptor, ApiProcessor, ObjectCollector, ReferenceInterceptor } from './core';
import { EventsService } from './events';
import { ANREST_HTTP_DATA_NORMALIZERS, ReferenceDataNormalizer, LdJsonDataNormalizer, JsonDataNormalizer } from './response';
import { AuthInterceptor, AuthService, AuthTokenProviderService, JwtAuthService, LocalStorageAuthTokenProviderService } from './auth';
import { ANREST_CACHE_SERVICES, CacheInterceptor, LocalStorageResponseCache, MemoryResponseCache, NoResponseCache, ResponseCache } from './cache';
var AnRestModule = /** @class */ (function () {
    function AnRestModule() {
    }
    /**
     * @param {?=} config
     * @return {?}
     */
    AnRestModule.config = /**
     * @param {?=} config
     * @return {?}
     */
    function (config) {
        return {
            ngModule: AnRestModule,
            providers: [
                {
                    provide: AnRestConfig,
                    useValue: config,
                },
                EventsService,
                DataInfoService,
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: LocalStorageResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: MemoryResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: NoResponseCache,
                    multi: true
                },
                {
                    provide: ResponseCache,
                    useClass: config.cache || NoResponseCache
                },
                {
                    provide: AuthTokenProviderService,
                    useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                },
                {
                    provide: AuthService,
                    useClass: config.authService || JwtAuthService
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ReferenceInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CacheInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ApiInterceptor,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: ReferenceDataNormalizer,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: LdJsonDataNormalizer,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: JsonDataNormalizer,
                    multi: true
                },
                ObjectCollector,
                ApiProcessor,
                ApiService
            ]
        };
    };
    /**
     * @param {?=} config
     * @return {?}
     */
    AnRestModule.configWithoutNormalizers = /**
     * @param {?=} config
     * @return {?}
     */
    function (config) {
        return {
            ngModule: AnRestModule,
            providers: [
                {
                    provide: AnRestConfig,
                    useValue: config,
                },
                EventsService,
                DataInfoService,
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: LocalStorageResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: MemoryResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: NoResponseCache,
                    multi: true
                },
                {
                    provide: ResponseCache,
                    useClass: config.cache || NoResponseCache
                },
                {
                    provide: AuthTokenProviderService,
                    useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                },
                {
                    provide: AuthService,
                    useClass: config.authService || JwtAuthService
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ReferenceInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CacheInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ApiInterceptor,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: ReferenceDataNormalizer,
                    multi: true
                },
                ObjectCollector,
                ApiProcessor,
                ApiService
            ]
        };
    };
    AnRestModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        HttpClientModule,
                    ]
                },] },
    ];
    return AnRestModule;
}());
export { AnRestModule };
function AnRestModule_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    AnRestModule.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    AnRestModule.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5yZXN0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5yZXN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUF1QixRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDM0UsT0FBTyxrQkFBa0IsQ0FBQztBQUUxQixPQUFPLEVBQ0wsVUFBVSxFQUNWLGVBQWUsRUFDZixZQUFZLEVBRVosY0FBYyxFQUNkLFlBQVksRUFDWixlQUFlLEVBQ2Ysb0JBQW9CLEVBQ3JCLE1BQU0sUUFBUSxDQUFDO0FBQ2hCLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxVQUFVLENBQUM7QUFDekMsT0FBTyxFQUNMLDRCQUE0QixFQUM1Qix1QkFBdUIsRUFDdkIsb0JBQW9CLEVBQ3BCLGtCQUFrQixFQUNuQixNQUFNLFlBQVksQ0FBQztBQUNwQixPQUFPLEVBQ0wsZUFBZSxFQUNmLFdBQVcsRUFDWCx3QkFBd0IsRUFDeEIsY0FBYyxFQUNkLG9DQUFvQyxFQUNyQyxNQUFNLFFBQVEsQ0FBQztBQUNoQixPQUFPLEVBQ0wscUJBQXFCLEVBQ3JCLGdCQUFnQixFQUNoQix5QkFBeUIsRUFDekIsbUJBQW1CLEVBQ25CLGVBQWUsRUFDZixhQUFhLEVBQ2QsTUFBTSxTQUFTLENBQUM7Ozs7Ozs7O0lBUVIsbUJBQU07Ozs7SUFBYixVQUFjLE1BQWtCO1FBQzlCLE1BQU0sQ0FBQztZQUNMLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFNBQVMsRUFBRTtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsWUFBWTtvQkFDckIsUUFBUSxFQUFFLE1BQU07aUJBQ2pCO2dCQUNELGFBQWE7Z0JBQ2IsZUFBZTtnQkFDZjtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUsZUFBZTtvQkFDekIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGFBQWE7b0JBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsS0FBSyxJQUFJLGVBQWU7aUJBQzFDO2dCQUNEO29CQUNFLE9BQU8sRUFBRSx3QkFBd0I7b0JBQ2pDLFFBQVEsRUFBRSxNQUFNLENBQUMsYUFBYSxJQUFJLG9DQUFvQztpQkFDdkU7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLFdBQVc7b0JBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsV0FBVyxJQUFJLGNBQWM7aUJBQy9DO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxlQUFlO29CQUN6QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsY0FBYztvQkFDeEIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLDRCQUE0QjtvQkFDckMsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0QsZUFBZTtnQkFDZixZQUFZO2dCQUNaLFVBQVU7YUFDWDtTQUNGLENBQUM7S0FDSDs7Ozs7SUFDTSxxQ0FBd0I7Ozs7SUFBL0IsVUFBZ0MsTUFBa0I7UUFDaEQsTUFBTSxDQUFDO1lBQ0wsUUFBUSxFQUFFLFlBQVk7WUFDdEIsU0FBUyxFQUFFO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxZQUFZO29CQUNyQixRQUFRLEVBQUUsTUFBTTtpQkFDakI7Z0JBQ0QsYUFBYTtnQkFDYixlQUFlO2dCQUNmO29CQUNFLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLFFBQVEsRUFBRSx5QkFBeUI7b0JBQ25DLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLFFBQVEsRUFBRSxtQkFBbUI7b0JBQzdCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLFFBQVEsRUFBRSxlQUFlO29CQUN6QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsYUFBYTtvQkFDdEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxLQUFLLElBQUksZUFBZTtpQkFDMUM7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLHdCQUF3QjtvQkFDakMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxhQUFhLElBQUksb0NBQW9DO2lCQUN2RTtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsV0FBVztvQkFDcEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxXQUFXLElBQUksY0FBYztpQkFDL0M7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtvQkFDMUIsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxjQUFjO29CQUN4QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsNEJBQTRCO29CQUNyQyxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyxLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRCxlQUFlO2dCQUNmLFlBQVk7Z0JBQ1osVUFBVTthQUNYO1NBQ0YsQ0FBQztLQUNIOztnQkF2SkYsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRTt3QkFDUCxnQkFBZ0I7cUJBQ2pCO2lCQUNGOzt1QkF6Q0Q7O1NBMENhLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMsIEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgJ3JlZmxlY3QtbWV0YWRhdGEnO1xuXG5pbXBvcnQge1xuICBBcGlTZXJ2aWNlLFxuICBEYXRhSW5mb1NlcnZpY2UsXG4gIEFuUmVzdENvbmZpZyxcbiAgQXBpQ29uZmlnLFxuICBBcGlJbnRlcmNlcHRvcixcbiAgQXBpUHJvY2Vzc29yLFxuICBPYmplY3RDb2xsZWN0b3IsXG4gIFJlZmVyZW5jZUludGVyY2VwdG9yXG59IGZyb20gJy4vY29yZSc7XG5pbXBvcnQgeyBFdmVudHNTZXJ2aWNlIH0gZnJvbSAnLi9ldmVudHMnO1xuaW1wb3J0IHtcbiAgQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIsXG4gIExkSnNvbkRhdGFOb3JtYWxpemVyLFxuICBKc29uRGF0YU5vcm1hbGl6ZXJcbn0gZnJvbSAnLi9yZXNwb25zZSc7XG5pbXBvcnQge1xuICBBdXRoSW50ZXJjZXB0b3IsXG4gIEF1dGhTZXJ2aWNlLFxuICBBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UsXG4gIEp3dEF1dGhTZXJ2aWNlLFxuICBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2Vcbn0gZnJvbSAnLi9hdXRoJztcbmltcG9ydCB7XG4gIEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgQ2FjaGVJbnRlcmNlcHRvcixcbiAgTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSxcbiAgTWVtb3J5UmVzcG9uc2VDYWNoZSxcbiAgTm9SZXNwb25zZUNhY2hlLFxuICBSZXNwb25zZUNhY2hlXG59IGZyb20gJy4vY2FjaGUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgSHR0cENsaWVudE1vZHVsZSxcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBblJlc3RNb2R1bGUge1xuICBzdGF0aWMgY29uZmlnKGNvbmZpZz86IEFwaUNvbmZpZyk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogQW5SZXN0TW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBblJlc3RDb25maWcsXG4gICAgICAgICAgdXNlVmFsdWU6IGNvbmZpZyxcbiAgICAgICAgfSxcbiAgICAgICAgRXZlbnRzU2VydmljZSxcbiAgICAgICAgRGF0YUluZm9TZXJ2aWNlLFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBMb2NhbFN0b3JhZ2VSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IE1lbW9yeVJlc3BvbnNlQ2FjaGUsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTm9SZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcuY2FjaGUgfHwgTm9SZXNwb25zZUNhY2hlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UsXG4gICAgICAgICAgdXNlQ2xhc3M6IGNvbmZpZy50b2tlblByb3ZpZGVyIHx8IExvY2FsU3RvcmFnZUF1dGhUb2tlblByb3ZpZGVyU2VydmljZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQXV0aFNlcnZpY2UsXG4gICAgICAgICAgdXNlQ2xhc3M6IGNvbmZpZy5hdXRoU2VydmljZSB8fCBKd3RBdXRoU2VydmljZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEF1dGhJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IFJlZmVyZW5jZUludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICAgICAgICB1c2VDbGFzczogQ2FjaGVJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEFwaUludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfSFRUUF9EQVRBX05PUk1BTElaRVJTLFxuICAgICAgICAgIHVzZUNsYXNzOiBSZWZlcmVuY2VEYXRhTm9ybWFsaXplcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgICAgICAgICB1c2VDbGFzczogTGRKc29uRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEpzb25EYXRhTm9ybWFsaXplcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICBPYmplY3RDb2xsZWN0b3IsXG4gICAgICAgIEFwaVByb2Nlc3NvcixcbiAgICAgICAgQXBpU2VydmljZVxuICAgICAgXVxuICAgIH07XG4gIH1cbiAgc3RhdGljIGNvbmZpZ1dpdGhvdXROb3JtYWxpemVycyhjb25maWc/OiBBcGlDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IEFuUmVzdE1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQW5SZXN0Q29uZmlnLFxuICAgICAgICAgIHVzZVZhbHVlOiBjb25maWcsXG4gICAgICAgIH0sXG4gICAgICAgIEV2ZW50c1NlcnZpY2UsXG4gICAgICAgIERhdGFJbmZvU2VydmljZSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBNZW1vcnlSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IE5vUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICB1c2VDbGFzczogY29uZmlnLmNhY2hlIHx8IE5vUmVzcG9uc2VDYWNoZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcudG9rZW5Qcm92aWRlciB8fCBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEF1dGhTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcuYXV0aFNlcnZpY2UgfHwgSnd0QXV0aFNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBdXRoSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBSZWZlcmVuY2VJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IENhY2hlSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBcGlJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgICAgICAgICB1c2VDbGFzczogUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAgT2JqZWN0Q29sbGVjdG9yLFxuICAgICAgICBBcGlQcm9jZXNzb3IsXG4gICAgICAgIEFwaVNlcnZpY2VcbiAgICAgIF1cbiAgICB9O1xuICB9XG59XG4iXX0=