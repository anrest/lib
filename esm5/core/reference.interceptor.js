/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnRestConfig } from './config';
import { MetaService } from '../meta';
var ReferenceInterceptor = /** @class */ (function () {
    function ReferenceInterceptor(config) {
        this.config = config;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    ReferenceInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl || !this.isReferenceRequest(request)) {
            return next.handle(request);
        }
        return this.createReferenceResponse(request);
    };
    /**
     * @param {?} request
     * @return {?}
     */
    ReferenceInterceptor.prototype.isReferenceRequest = /**
     * @param {?} request
     * @return {?}
     */
    function (request) {
        return request.method === 'GET' && request.url.slice(this.config.baseUrl.length).indexOf('&') === 0;
    };
    /**
     * @param {?} request
     * @return {?}
     */
    ReferenceInterceptor.prototype.createReferenceResponse = /**
     * @param {?} request
     * @return {?}
     */
    function (request) {
        var /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        return of(new HttpResponse({
            body: { 'path': path.substring(1, path.indexOf('#')), 'meta': MetaService.getByName(path.substring(path.indexOf('#') + 1)) },
            status: 200,
            url: request.url
        })).pipe(map(function (response) {
            if (response instanceof HttpResponse) {
                return response.clone({
                    headers: response.headers.set('Content-Type', '@reference')
                });
            }
        }));
    };
    ReferenceInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ReferenceInterceptor.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return ReferenceInterceptor;
}());
export { ReferenceInterceptor };
function ReferenceInterceptor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ReferenceInterceptor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ReferenceInterceptor.ctorParameters;
    /** @type {?} */
    ReferenceInterceptor.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmZXJlbmNlLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJjb3JlL3JlZmVyZW5jZS5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUlZLFlBQVksRUFDOUIsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QixPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsWUFBWSxFQUFhLE1BQU0sVUFBVSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxTQUFTLENBQUM7O0lBS3BDLDhCQUMyQztRQUFBLFdBQU0sR0FBTixNQUFNO0tBQzdDOzs7Ozs7SUFFSix3Q0FBUzs7Ozs7SUFBVCxVQUFVLE9BQXlCLEVBQUUsSUFBaUI7UUFDcEQsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0gsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDN0I7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQzlDOzs7OztJQUVPLGlEQUFrQjs7OztjQUFDLE9BQXlCO1FBQ2xELE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLEtBQUssSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7SUFHOUYsc0RBQXVCOzs7O2NBQUMsT0FBeUI7UUFDdkQscUJBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxZQUFZLENBQUM7WUFDekIsSUFBSSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBQztZQUMxSCxNQUFNLEVBQUUsR0FBRztZQUNYLEdBQUcsRUFBRSxPQUFPLENBQUMsR0FBRztTQUNqQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQ04sR0FBRyxDQUFDLFVBQUMsUUFBUTtZQUNYLEVBQUUsQ0FBQyxDQUFDLFFBQVEsWUFBWSxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDcEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxZQUFZLENBQUM7aUJBQzVELENBQUMsQ0FBQzthQUNKO1NBQ0YsQ0FBQyxDQUNILENBQUM7OztnQkFoQ0wsVUFBVTs7OztnREFJTixNQUFNLFNBQUMsWUFBWTs7K0JBaEJ4Qjs7U0FhYSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIEh0dHBSZXF1ZXN0LFxuICBIdHRwSGFuZGxlcixcbiAgSHR0cEV2ZW50LFxuICBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXNwb25zZVxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUmVmZXJlbmNlSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgcmVhZG9ubHkgY29uZmlnOiBBcGlDb25maWdcbiAgKSB7fVxuXG4gIGludGVyY2VwdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICBpZiAocmVxdWVzdC51cmwuaW5kZXhPZih0aGlzLmNvbmZpZy5iYXNlVXJsKSAhPT0gMCB8fCByZXF1ZXN0LnVybCA9PT0gdGhpcy5jb25maWcuYXV0aFVybCB8fCAhdGhpcy5pc1JlZmVyZW5jZVJlcXVlc3QocmVxdWVzdCkpIHtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMuY3JlYXRlUmVmZXJlbmNlUmVzcG9uc2UocmVxdWVzdCk7XG4gIH1cblxuICBwcml2YXRlIGlzUmVmZXJlbmNlUmVxdWVzdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHJlcXVlc3QubWV0aG9kID09PSAnR0VUJyAmJiByZXF1ZXN0LnVybC5zbGljZSh0aGlzLmNvbmZpZy5iYXNlVXJsLmxlbmd0aCkuaW5kZXhPZignJicpID09PSAwO1xuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVSZWZlcmVuY2VSZXNwb25zZShyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+KSB7XG4gICAgY29uc3QgcGF0aCA9IHJlcXVlc3QudXJsV2l0aFBhcmFtcy5zbGljZSh0aGlzLmNvbmZpZy5iYXNlVXJsLmxlbmd0aCk7XG4gICAgcmV0dXJuIG9mKG5ldyBIdHRwUmVzcG9uc2Uoe1xuICAgICAgYm9keTogeydwYXRoJzogcGF0aC5zdWJzdHJpbmcoMSwgcGF0aC5pbmRleE9mKCcjJykpLCAnbWV0YSc6IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShwYXRoLnN1YnN0cmluZyhwYXRoLmluZGV4T2YoJyMnKSArIDEpKX0sXG4gICAgICBzdGF0dXM6IDIwMCxcbiAgICAgIHVybDogcmVxdWVzdC51cmxcbiAgICB9KSkucGlwZShcbiAgICAgIG1hcCgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlIGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XG4gICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmNsb25lKHtcbiAgICAgICAgICAgIGhlYWRlcnM6IHJlc3BvbnNlLmhlYWRlcnMuc2V0KCdDb250ZW50LVR5cGUnLCAnQHJlZmVyZW5jZScpXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIl19