/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable, Injector } from '@angular/core';
import { ResponseNode } from '../response/response';
import { DataInfoService } from './data-info.service';
import { EntityMetaInfo } from '../meta';
import { ObjectCollector } from './object-collector';
import { Collection } from './collection';
var ApiProcessor = /** @class */ (function () {
    function ApiProcessor(injector, info, collector) {
        this.injector = injector;
        this.info = info;
        this.collector = collector;
    }
    /**
     * @param {?} node
     * @param {?} method
     * @return {?}
     */
    ApiProcessor.prototype.process = /**
     * @param {?} node
     * @param {?} method
     * @return {?}
     */
    function (node, method) {
        return node.collection ? this.processCollection(node) : this.processObject(node);
    };
    /**
     * @param {?} node
     * @return {?}
     */
    ApiProcessor.prototype.processCollection = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var /** @type {?} */ data;
        if (node.info.path !== undefined) {
            data = this.collector.get(node.info.path) || new Collection(this.injector.get(node.meta.service), node.info);
        }
        else {
            data = new Collection(this.injector.get(node.meta.service), node.info);
        }
        data.length = 0;
        try {
            for (var _a = tslib_1.__values(node.data), _b = _a.next(); !_b.done; _b = _a.next()) {
                var object = _b.value;
                data.push(this.processObject(object));
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.info.setForCollection(data, node.info);
        this.collector.set(data);
        return data;
        var e_1, _c;
    };
    /**
     * @param {?} node
     * @return {?}
     */
    ApiProcessor.prototype.processObject = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node.meta instanceof EntityMetaInfo) {
            var /** @type {?} */ object = this.collector.get(node.info.path) || new node.meta.type();
            for (var /** @type {?} */ property in node.meta.properties) {
                if (object.hasOwnProperty(property) && !node.data.hasOwnProperty(property)) {
                    continue;
                }
                if (node.data[property] instanceof ResponseNode) {
                    object[property] = node.meta.properties[property].isCollection ?
                        this.processCollection(node.data[property]) :
                        this.processObject(node.data[property]);
                }
                else {
                    var /** @type {?} */ type = node.meta.properties[property].type;
                    switch (type) {
                        case Number:
                            object[property] = Number(node.data[property]);
                            break;
                        case Date:
                            object[property] = new Date(node.data[property]);
                            break;
                        case String:
                        default:
                            object[property] = node.data[property];
                            break;
                    }
                }
            }
            for (var /** @type {?} */ property in node.meta.subresources) {
                var /** @type {?} */ service = this.injector.get(node.meta.subresources[property].meta.service);
                object[property] = service.doGet.bind(service, node.info.path + node.meta.subresources[property].path);
            }
            this.info.setForEntity(object, node.info);
            this.collector.set(object);
            return object;
        }
        else {
            return node.data;
        }
    };
    ApiProcessor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ApiProcessor.ctorParameters = function () { return [
        { type: Injector, },
        { type: DataInfoService, },
        { type: ObjectCollector, },
    ]; };
    return ApiProcessor;
}());
export { ApiProcessor };
function ApiProcessor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ApiProcessor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ApiProcessor.ctorParameters;
    /** @type {?} */
    ApiProcessor.prototype.injector;
    /** @type {?} */
    ApiProcessor.prototype.info;
    /** @type {?} */
    ApiProcessor.prototype.collector;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLnByb2Nlc3Nvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY29yZS9hcGkucHJvY2Vzc29yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sY0FBYyxDQUFDOztJQUt4QyxzQkFDVSxVQUNBLE1BQ0E7UUFGQSxhQUFRLEdBQVIsUUFBUTtRQUNSLFNBQUksR0FBSixJQUFJO1FBQ0osY0FBUyxHQUFULFNBQVM7S0FDZjs7Ozs7O0lBRUosOEJBQU87Ozs7O0lBQVAsVUFBUSxJQUFrQixFQUFFLE1BQWM7UUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNsRjs7Ozs7SUFFTyx3Q0FBaUI7Ozs7Y0FBQyxJQUFrQjtRQUMxQyxxQkFBSSxJQUFJLENBQUM7UUFDVCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzlHO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEU7UUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7WUFDaEIsR0FBRyxDQUFDLENBQWlCLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsSUFBSSxDQUFBLGdCQUFBO2dCQUF6QixJQUFNLE1BQU0sV0FBQTtnQkFDZixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUN2Qzs7Ozs7Ozs7O1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7Ozs7SUFHTixvQ0FBYTs7OztjQUFDLElBQWtCO1FBQ3RDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLFlBQVksY0FBYyxDQUFDLENBQUMsQ0FBQztZQUN4QyxxQkFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUUsR0FBRyxDQUFDLENBQUMscUJBQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDNUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDM0UsUUFBUSxDQUFDO2lCQUNWO2dCQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDaEQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUM5RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2lCQUMzQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDTixxQkFBTSxJQUFJLEdBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUN0RCxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNiLEtBQUssTUFBTTs0QkFDVCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFDL0MsS0FBSyxDQUFDO3dCQUNSLEtBQUssSUFBSTs0QkFDUCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDOzRCQUNqRCxLQUFLLENBQUM7d0JBQ1IsS0FBSyxNQUFNLENBQUM7d0JBQ1o7NEJBQ0UsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQ3ZDLEtBQUssQ0FBQztxQkFDVDtpQkFDRjthQUNGO1lBQ0QsR0FBRyxDQUFDLENBQUMscUJBQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDOUMscUJBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDakYsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN4RztZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDM0IsTUFBTSxDQUFDLE1BQU0sQ0FBQztTQUNmO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNsQjs7O2dCQWpFSixVQUFVOzs7O2dCQVBVLFFBQVE7Z0JBRXBCLGVBQWU7Z0JBRWYsZUFBZTs7dUJBSnhCOztTQVFhLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi4vcmVzcG9uc2UvcmVzcG9uc2UnO1xuaW1wb3J0IHsgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQgeyBFbnRpdHlNZXRhSW5mbyB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgT2JqZWN0Q29sbGVjdG9yIH0gZnJvbSAnLi9vYmplY3QtY29sbGVjdG9yJztcbmltcG9ydCB7IENvbGxlY3Rpb24gfSBmcm9tICcuL2NvbGxlY3Rpb24nO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXBpUHJvY2Vzc29yIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcixcbiAgICBwcml2YXRlIGluZm86IERhdGFJbmZvU2VydmljZSxcbiAgICBwcml2YXRlIGNvbGxlY3RvcjogT2JqZWN0Q29sbGVjdG9yXG4gICkge31cblxuICBwcm9jZXNzKG5vZGU6IFJlc3BvbnNlTm9kZSwgbWV0aG9kOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiBub2RlLmNvbGxlY3Rpb24gPyB0aGlzLnByb2Nlc3NDb2xsZWN0aW9uKG5vZGUpIDogdGhpcy5wcm9jZXNzT2JqZWN0KG5vZGUpO1xuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzQ29sbGVjdGlvbihub2RlOiBSZXNwb25zZU5vZGUpIHtcbiAgICBsZXQgZGF0YTtcbiAgICBpZiAobm9kZS5pbmZvLnBhdGggIT09IHVuZGVmaW5lZCkge1xuICAgICAgZGF0YSA9IHRoaXMuY29sbGVjdG9yLmdldChub2RlLmluZm8ucGF0aCkgfHwgbmV3IENvbGxlY3Rpb24odGhpcy5pbmplY3Rvci5nZXQobm9kZS5tZXRhLnNlcnZpY2UpLCBub2RlLmluZm8pO1xuICAgIH0gZWxzZSB7XG4gICAgICBkYXRhID0gbmV3IENvbGxlY3Rpb24odGhpcy5pbmplY3Rvci5nZXQobm9kZS5tZXRhLnNlcnZpY2UpLCBub2RlLmluZm8pO1xuICAgIH1cbiAgICBkYXRhLmxlbmd0aCA9IDA7XG4gICAgZm9yIChjb25zdCBvYmplY3Qgb2Ygbm9kZS5kYXRhKSB7XG4gICAgICBkYXRhLnB1c2godGhpcy5wcm9jZXNzT2JqZWN0KG9iamVjdCkpO1xuICAgIH1cbiAgICB0aGlzLmluZm8uc2V0Rm9yQ29sbGVjdGlvbihkYXRhLCBub2RlLmluZm8pO1xuICAgIHRoaXMuY29sbGVjdG9yLnNldChkYXRhKTtcbiAgICByZXR1cm4gZGF0YTtcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2Vzc09iamVjdChub2RlOiBSZXNwb25zZU5vZGUpIHtcbiAgICBpZiAobm9kZS5tZXRhIGluc3RhbmNlb2YgRW50aXR5TWV0YUluZm8pIHtcbiAgICAgIGNvbnN0IG9iamVjdCA9IHRoaXMuY29sbGVjdG9yLmdldChub2RlLmluZm8ucGF0aCkgfHwgbmV3IG5vZGUubWV0YS50eXBlKCk7XG4gICAgICBmb3IgKGNvbnN0IHByb3BlcnR5IGluIG5vZGUubWV0YS5wcm9wZXJ0aWVzKSB7XG4gICAgICAgIGlmIChvYmplY3QuaGFzT3duUHJvcGVydHkocHJvcGVydHkpICYmICFub2RlLmRhdGEuaGFzT3duUHJvcGVydHkocHJvcGVydHkpKSB7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG5vZGUuZGF0YVtwcm9wZXJ0eV0gaW5zdGFuY2VvZiBSZXNwb25zZU5vZGUpIHtcbiAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gbm9kZS5tZXRhLnByb3BlcnRpZXNbcHJvcGVydHldLmlzQ29sbGVjdGlvbiA/XG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NDb2xsZWN0aW9uKG5vZGUuZGF0YVtwcm9wZXJ0eV0pIDpcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc09iamVjdChub2RlLmRhdGFbcHJvcGVydHldKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zdCB0eXBlOiBhbnkgPSBub2RlLm1ldGEucHJvcGVydGllc1twcm9wZXJ0eV0udHlwZTtcbiAgICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgICAgIGNhc2UgTnVtYmVyOlxuICAgICAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gTnVtYmVyKG5vZGUuZGF0YVtwcm9wZXJ0eV0pO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgRGF0ZTpcbiAgICAgICAgICAgICAgb2JqZWN0W3Byb3BlcnR5XSA9IG5ldyBEYXRlKG5vZGUuZGF0YVtwcm9wZXJ0eV0pO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgU3RyaW5nOlxuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgb2JqZWN0W3Byb3BlcnR5XSA9IG5vZGUuZGF0YVtwcm9wZXJ0eV07XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiBub2RlLm1ldGEuc3VicmVzb3VyY2VzKSB7XG4gICAgICAgIGNvbnN0IHNlcnZpY2UgPSB0aGlzLmluamVjdG9yLmdldChub2RlLm1ldGEuc3VicmVzb3VyY2VzW3Byb3BlcnR5XS5tZXRhLnNlcnZpY2UpO1xuICAgICAgICBvYmplY3RbcHJvcGVydHldID0gc2VydmljZS5kb0dldC5iaW5kKHNlcnZpY2UsIG5vZGUuaW5mby5wYXRoICsgbm9kZS5tZXRhLnN1YnJlc291cmNlc1twcm9wZXJ0eV0ucGF0aCk7XG4gICAgICB9XG4gICAgICB0aGlzLmluZm8uc2V0Rm9yRW50aXR5KG9iamVjdCwgbm9kZS5pbmZvKTtcbiAgICAgIHRoaXMuY29sbGVjdG9yLnNldChvYmplY3QpO1xuICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIG5vZGUuZGF0YTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==