/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
var EntityInfo = /** @class */ (function () {
    function EntityInfo(_path) {
        this._path = _path;
    }
    Object.defineProperty(EntityInfo.prototype, "path", {
        get: /**
         * @return {?}
         */
        function () {
            return this._path;
        },
        enumerable: true,
        configurable: true
    });
    return EntityInfo;
}());
export { EntityInfo };
function EntityInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    EntityInfo.prototype._path;
}
var CollectionInfo = /** @class */ (function () {
    function CollectionInfo(_path) {
        this._path = _path;
    }
    Object.defineProperty(CollectionInfo.prototype, "path", {
        get: /**
         * @return {?}
         */
        function () {
            return this._path;
        },
        enumerable: true,
        configurable: true
    });
    return CollectionInfo;
}());
export { CollectionInfo };
function CollectionInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    CollectionInfo.prototype.total;
    /** @type {?} */
    CollectionInfo.prototype.page;
    /** @type {?} */
    CollectionInfo.prototype.lastPage;
    /** @type {?} */
    CollectionInfo.prototype.first;
    /** @type {?} */
    CollectionInfo.prototype.previous;
    /** @type {?} */
    CollectionInfo.prototype.next;
    /** @type {?} */
    CollectionInfo.prototype.last;
    /** @type {?} */
    CollectionInfo.prototype.alias;
    /** @type {?} */
    CollectionInfo.prototype.type;
    /** @type {?} */
    CollectionInfo.prototype._path;
}
var DataInfoService = /** @class */ (function () {
    function DataInfoService() {
    }
    /**
     * @param {?} object
     * @return {?}
     */
    DataInfoService.prototype.get = /**
     * @param {?} object
     * @return {?}
     */
    function (object) {
        return Reflect.getMetadata('anrest:info', object);
    };
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    DataInfoService.prototype.set = /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    function (object, info) {
        Reflect.defineMetadata('anrest:info', info, object);
    };
    /**
     * @param {?} object
     * @return {?}
     */
    DataInfoService.prototype.getForCollection = /**
     * @param {?} object
     * @return {?}
     */
    function (object) {
        return /** @type {?} */ (this.get(object));
    };
    /**
     * @param {?} object
     * @return {?}
     */
    DataInfoService.prototype.getForEntity = /**
     * @param {?} object
     * @return {?}
     */
    function (object) {
        return /** @type {?} */ (this.get(object));
    };
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    DataInfoService.prototype.setForCollection = /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    function (object, info) {
        this.set(object, info);
    };
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    DataInfoService.prototype.setForEntity = /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    function (object, info) {
        this.set(object, info);
    };
    DataInfoService.decorators = [
        { type: Injectable },
    ];
    return DataInfoService;
}());
export { DataInfoService };
function DataInfoService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    DataInfoService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    DataInfoService.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1pbmZvLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNvcmUvZGF0YS1pbmZvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsSUFBQTtJQUVFLG9CQUFvQixLQUFLO1FBQUwsVUFBSyxHQUFMLEtBQUssQ0FBQTtLQUFJO0lBRTdCLHNCQUFJLDRCQUFJOzs7O1FBQVI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjs7O09BQUE7cUJBUkg7SUFTQyxDQUFBO0FBUEQsc0JBT0M7Ozs7O0FBRUQsSUFBQTtJQVdFLHdCQUFvQixLQUFLO1FBQUwsVUFBSyxHQUFMLEtBQUssQ0FBQTtLQUFJO0lBRTdCLHNCQUFJLGdDQUFJOzs7O1FBQVI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjs7O09BQUE7eUJBMUJIO0lBMkJDLENBQUE7QUFoQkQsMEJBZ0JDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFLQyw2QkFBRzs7OztJQUFILFVBQUksTUFBTTtRQUNSLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQztLQUNuRDs7Ozs7O0lBRUQsNkJBQUc7Ozs7O0lBQUgsVUFBSSxNQUFNLEVBQUUsSUFBUztRQUNuQixPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7S0FDckQ7Ozs7O0lBRUQsMENBQWdCOzs7O0lBQWhCLFVBQWlCLE1BQU07UUFDckIsTUFBTSxtQkFBaUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQztLQUN6Qzs7Ozs7SUFFRCxzQ0FBWTs7OztJQUFaLFVBQWEsTUFBTTtRQUNqQixNQUFNLG1CQUFhLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUM7S0FDckM7Ozs7OztJQUVELDBDQUFnQjs7Ozs7SUFBaEIsVUFBaUIsTUFBTSxFQUFFLElBQW9CO1FBQzNDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3hCOzs7Ozs7SUFFRCxzQ0FBWTs7Ozs7SUFBWixVQUFhLE1BQU0sRUFBRSxJQUFnQjtRQUNuQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztLQUN4Qjs7Z0JBekJGLFVBQVU7OzBCQTdCWDs7U0E4QmEsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGNsYXNzIEVudGl0eUluZm8ge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX3BhdGgpIHt9XG5cbiAgZ2V0IHBhdGgoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3BhdGg7XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIENvbGxlY3Rpb25JbmZvIHtcbiAgcHVibGljIHRvdGFsOiBudW1iZXI7XG4gIHB1YmxpYyBwYWdlOiBudW1iZXI7XG4gIHB1YmxpYyBsYXN0UGFnZTogbnVtYmVyO1xuICBwdWJsaWMgZmlyc3Q6IHN0cmluZztcbiAgcHVibGljIHByZXZpb3VzOiBzdHJpbmc7XG4gIHB1YmxpYyBuZXh0OiBzdHJpbmc7XG4gIHB1YmxpYyBsYXN0OiBzdHJpbmc7XG4gIHB1YmxpYyBhbGlhczogc3RyaW5nO1xuICBwdWJsaWMgdHlwZTogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX3BhdGgpIHt9XG5cbiAgZ2V0IHBhdGgoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3BhdGg7XG4gIH1cbn1cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIERhdGFJbmZvU2VydmljZSB7XG5cbiAgZ2V0KG9iamVjdCk6IGFueSB7XG4gICAgcmV0dXJuIFJlZmxlY3QuZ2V0TWV0YWRhdGEoJ2FucmVzdDppbmZvJywgb2JqZWN0KTtcbiAgfVxuXG4gIHNldChvYmplY3QsIGluZm86IGFueSkge1xuICAgIFJlZmxlY3QuZGVmaW5lTWV0YWRhdGEoJ2FucmVzdDppbmZvJywgaW5mbywgb2JqZWN0KTtcbiAgfVxuXG4gIGdldEZvckNvbGxlY3Rpb24ob2JqZWN0KTogQ29sbGVjdGlvbkluZm8ge1xuICAgIHJldHVybiA8Q29sbGVjdGlvbkluZm8+dGhpcy5nZXQob2JqZWN0KTtcbiAgfVxuXG4gIGdldEZvckVudGl0eShvYmplY3QpOiBFbnRpdHlJbmZvIHtcbiAgICByZXR1cm4gPEVudGl0eUluZm8+dGhpcy5nZXQob2JqZWN0KTtcbiAgfVxuXG4gIHNldEZvckNvbGxlY3Rpb24ob2JqZWN0LCBpbmZvOiBDb2xsZWN0aW9uSW5mbykge1xuICAgIHRoaXMuc2V0KG9iamVjdCwgaW5mbyk7XG4gIH1cblxuICBzZXRGb3JFbnRpdHkob2JqZWN0LCBpbmZvOiBFbnRpdHlJbmZvKSB7XG4gICAgdGhpcy5zZXQob2JqZWN0LCBpbmZvKTtcbiAgfVxufVxuIl19