/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { DataInfoService } from './data-info.service';
import { AfterGetEvent, AfterRemoveEvent, AfterSaveEvent, EventsService, BeforeGetEvent, BeforeRemoveEvent, BeforeSaveEvent } from '../events';
import { AnRestConfig } from './config';
import { MetaService } from '../meta';
var ApiService = /** @class */ (function () {
    function ApiService(http, events, infoService, config) {
        this.http = http;
        this.events = events;
        this.infoService = infoService;
        this.config = config;
        this.meta = MetaService.get(this.constructor);
    }
    /**
     * @param {?=} filter
     * @return {?}
     */
    ApiService.prototype.getList = /**
     * @param {?=} filter
     * @return {?}
     */
    function (filter) {
        return this.doGet(this.meta.path, filter);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ApiService.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.doGet(this.meta.path + '/' + id);
    };
    /**
     * @param {?} entity
     * @return {?}
     */
    ApiService.prototype.reload = /**
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return this.doGet(this.infoService.getForEntity(entity).path);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ApiService.prototype.getReference = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.doGet('&' + this.meta.path + '/' + id + '#' + this.meta.entityMeta.name);
    };
    /**
     * @param {?} path
     * @param {?=} filter
     * @return {?}
     */
    ApiService.prototype.doGet = /**
     * @param {?} path
     * @param {?=} filter
     * @return {?}
     */
    function (path, filter) {
        var _this = this;
        var /** @type {?} */ f;
        if (filter instanceof HttpParams) {
            f = filter;
        }
        else {
            f = new HttpParams();
            for (var /** @type {?} */ key in filter) {
                f = f.set(key, typeof filter[key] === 'boolean' ? (filter[key] ? 'true' : 'false') : String(filter[key]));
            }
        }
        this.events.handle(new BeforeGetEvent(path, this.meta.entityMeta.type, f));
        return this.http.get(this.config.baseUrl + path, { headers: this.meta.entityMeta.getHeaders(), params: f }).pipe(tap(function (data) { return _this.events.handle(new AfterGetEvent(data, _this.meta.entityMeta.type)); }));
    };
    /**
     * @param {?} entity
     * @return {?}
     */
    ApiService.prototype.save = /**
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        var _this = this;
        this.events.handle(new BeforeSaveEvent(entity));
        var /** @type {?} */ body = {};
        if (this.meta.entityMeta.body) {
            body = this.meta.entityMeta.body(entity);
        }
        else {
            this.meta.entityMeta.getPropertiesForSave().forEach(function (property) { return body[property] = entity[property]; });
        }
        var /** @type {?} */ meta = this.infoService.getForEntity(entity);
        return this.http[meta ? 'put' : 'post'](meta ? this.config.baseUrl + meta.path : this.config.baseUrl + this.meta.path, body, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap(function (data) {
            meta = _this.infoService.getForEntity(data);
            if (!meta) {
                _this.infoService.setForEntity(entity, meta);
            }
        }), tap(function () { return _this.events.handle(new AfterSaveEvent(entity)); }));
    };
    /**
     * @param {?} entity
     * @return {?}
     */
    ApiService.prototype.remove = /**
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        var _this = this;
        this.events.handle(new BeforeRemoveEvent(entity));
        var /** @type {?} */ meta = this.infoService.getForEntity(entity);
        if (meta) {
            return this.http.delete(this.config.baseUrl + meta.path, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap(function () { return _this.events.handle(new AfterRemoveEvent(entity)); }));
        }
    };
    ApiService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ApiService.ctorParameters = function () { return [
        { type: HttpClient, },
        { type: EventsService, },
        { type: DataInfoService, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return ApiService;
}());
export { ApiService };
function ApiService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ApiService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ApiService.ctorParameters;
    /** @type {?} */
    ApiService.prototype.meta;
    /** @type {?} */
    ApiService.prototype.http;
    /** @type {?} */
    ApiService.prototype.events;
    /** @type {?} */
    ApiService.prototype.infoService;
    /** @type {?} */
    ApiService.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNvcmUvYXBpLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFOUQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQ0wsYUFBYSxFQUNiLGdCQUFnQixFQUNoQixjQUFjLEVBQ2QsYUFBYSxFQUNiLGNBQWMsRUFDZCxpQkFBaUIsRUFDakIsZUFBZSxFQUNoQixNQUFNLFdBQVcsQ0FBQztBQUNuQixPQUFPLEVBQUUsWUFBWSxFQUFhLE1BQU0sVUFBVSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQW1CLE1BQU0sU0FBUyxDQUFDOztJQU1yRCxvQkFDWSxJQUFnQixFQUNoQixNQUFxQixFQUNyQixXQUE0QixFQUNOO1FBSHRCLFNBQUksR0FBSixJQUFJLENBQVk7UUFDaEIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixnQkFBVyxHQUFYLFdBQVcsQ0FBaUI7UUFDTixXQUFNLEdBQU4sTUFBTTtRQUV0QyxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQy9DOzs7OztJQUVELDRCQUFPOzs7O0lBQVAsVUFBUSxNQUE4RDtRQUNwRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztLQUMzQzs7Ozs7SUFFRCx3QkFBRzs7OztJQUFILFVBQUksRUFBbUI7UUFDckIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0tBQzlDOzs7OztJQUVELDJCQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQy9EOzs7OztJQUVELGlDQUFZOzs7O0lBQVosVUFBYSxFQUFtQjtRQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLEVBQUUsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDdEY7Ozs7OztJQUVELDBCQUFLOzs7OztJQUFMLFVBQU0sSUFBWSxFQUFFLE1BQThEO1FBQWxGLGlCQWNDO1FBYkMscUJBQUksQ0FBYSxDQUFDO1FBQ2xCLEVBQUUsQ0FBQyxDQUFDLE1BQU0sWUFBWSxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLENBQUMsR0FBRyxNQUFNLENBQUM7U0FDWjtRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sQ0FBQyxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7WUFDckIsR0FBRyxDQUFDLENBQUMscUJBQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMzRztTQUNGO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUMsSUFBSSxDQUM1RyxHQUFHLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBdEUsQ0FBc0UsQ0FBQyxDQUN0RixDQUFDO0tBQ0g7Ozs7O0lBRUQseUJBQUk7Ozs7SUFBSixVQUFLLE1BQVc7UUFBaEIsaUJBdUJDO1FBdEJDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDaEQscUJBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNkLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMxQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRLElBQUssT0FBQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFqQyxDQUFpQyxDQUFDLENBQUM7U0FDdEc7UUFDRCxxQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFakQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUNuQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUM3RSxJQUFJLEVBQ0osRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFDLENBQ25ELENBQUMsSUFBSSxDQUNKLEdBQUcsQ0FBQyxVQUFDLElBQUk7WUFDUCxJQUFJLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0MsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNWLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM3QztTQUNGLENBQUMsRUFDRixHQUFHLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQTlDLENBQThDLENBQUMsQ0FDMUQsQ0FBQztLQUNMOzs7OztJQUVELDJCQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQWxCLGlCQVFDO1FBUEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ2xELHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQy9HLEdBQUcsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFoRCxDQUFnRCxDQUFDLENBQzVELENBQUM7U0FDSDtLQUNGOztnQkE5RUYsVUFBVTs7OztnQkFoQkYsVUFBVTtnQkFRakIsYUFBYTtnQkFMTixlQUFlO2dEQXFCbkIsTUFBTSxTQUFDLFlBQVk7O3FCQXpCeEI7O1NBa0JhLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBEYXRhSW5mb1NlcnZpY2UgfSBmcm9tICcuL2RhdGEtaW5mby5zZXJ2aWNlJztcbmltcG9ydCB7XG4gIEFmdGVyR2V0RXZlbnQsXG4gIEFmdGVyUmVtb3ZlRXZlbnQsXG4gIEFmdGVyU2F2ZUV2ZW50LFxuICBFdmVudHNTZXJ2aWNlLFxuICBCZWZvcmVHZXRFdmVudCxcbiAgQmVmb3JlUmVtb3ZlRXZlbnQsXG4gIEJlZm9yZVNhdmVFdmVudFxufSBmcm9tICcuLi9ldmVudHMnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSwgU2VydmljZU1ldGFJbmZvIH0gZnJvbSAnLi4vbWV0YSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBcGlTZXJ2aWNlIHtcbiAgcHJvdGVjdGVkIG1ldGE6IFNlcnZpY2VNZXRhSW5mbztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgaHR0cDogSHR0cENsaWVudCxcbiAgICBwcm90ZWN0ZWQgZXZlbnRzOiBFdmVudHNTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBpbmZvU2VydmljZTogRGF0YUluZm9TZXJ2aWNlLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgY29uZmlnOiBBcGlDb25maWdcbiAgKSB7XG4gICAgdGhpcy5tZXRhID0gTWV0YVNlcnZpY2UuZ2V0KHRoaXMuY29uc3RydWN0b3IpO1xuICB9XG5cbiAgZ2V0TGlzdChmaWx0ZXI/OiBIdHRwUGFyYW1zfHsgW2luZGV4OiBzdHJpbmddOiBzdHJpbmd8bnVtYmVyfGJvb2xlYW4gfSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZG9HZXQodGhpcy5tZXRhLnBhdGgsIGZpbHRlcik7XG4gIH1cblxuICBnZXQoaWQ6IG51bWJlciB8IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZG9HZXQodGhpcy5tZXRhLnBhdGggKyAnLycgKyBpZCk7XG4gIH1cblxuICByZWxvYWQoZW50aXR5OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGVudGl0eSkucGF0aCk7XG4gIH1cblxuICBnZXRSZWZlcmVuY2UoaWQ6IG51bWJlciB8IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZG9HZXQoJyYnICsgdGhpcy5tZXRhLnBhdGggKyAnLycgKyBpZCArICcjJyArIHRoaXMubWV0YS5lbnRpdHlNZXRhLm5hbWUpO1xuICB9XG5cbiAgZG9HZXQocGF0aDogc3RyaW5nLCBmaWx0ZXI/OiBIdHRwUGFyYW1zfHsgW2luZGV4OiBzdHJpbmddOiBzdHJpbmd8bnVtYmVyfGJvb2xlYW4gfSkge1xuICAgIGxldCBmOiBIdHRwUGFyYW1zO1xuICAgIGlmIChmaWx0ZXIgaW5zdGFuY2VvZiBIdHRwUGFyYW1zKSB7XG4gICAgICBmID0gZmlsdGVyO1xuICAgIH0gZWxzZSB7XG4gICAgICBmID0gbmV3IEh0dHBQYXJhbXMoKTtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGZpbHRlcikge1xuICAgICAgICBmID0gZi5zZXQoa2V5LCB0eXBlb2YgZmlsdGVyW2tleV0gPT09ICdib29sZWFuJyA/IChmaWx0ZXJba2V5XSA/ICd0cnVlJyA6ICdmYWxzZScpIDogU3RyaW5nKGZpbHRlcltrZXldKSk7XG4gICAgICB9XG4gICAgfVxuICAgIHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQmVmb3JlR2V0RXZlbnQocGF0aCwgdGhpcy5tZXRhLmVudGl0eU1ldGEudHlwZSwgZikpO1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHRoaXMuY29uZmlnLmJhc2VVcmwgKyBwYXRoLCB7aGVhZGVyczogdGhpcy5tZXRhLmVudGl0eU1ldGEuZ2V0SGVhZGVycygpLCBwYXJhbXM6IGZ9KS5waXBlKFxuICAgICAgdGFwKChkYXRhKSA9PiB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEFmdGVyR2V0RXZlbnQoZGF0YSwgdGhpcy5tZXRhLmVudGl0eU1ldGEudHlwZSkpKVxuICAgICk7XG4gIH1cblxuICBzYXZlKGVudGl0eTogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEJlZm9yZVNhdmVFdmVudChlbnRpdHkpKTtcbiAgICBsZXQgYm9keSA9IHt9O1xuICAgIGlmICh0aGlzLm1ldGEuZW50aXR5TWV0YS5ib2R5KSB7XG4gICAgICBib2R5ID0gdGhpcy5tZXRhLmVudGl0eU1ldGEuYm9keShlbnRpdHkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLm1ldGEuZW50aXR5TWV0YS5nZXRQcm9wZXJ0aWVzRm9yU2F2ZSgpLmZvckVhY2goKHByb3BlcnR5KSA9PiBib2R5W3Byb3BlcnR5XSA9IGVudGl0eVtwcm9wZXJ0eV0pO1xuICAgIH1cbiAgICBsZXQgbWV0YSA9IHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGVudGl0eSk7XG5cbiAgICByZXR1cm4gdGhpcy5odHRwW21ldGEgPyAncHV0JyA6ICdwb3N0J10oXG4gICAgICAgIG1ldGEgPyB0aGlzLmNvbmZpZy5iYXNlVXJsICsgbWV0YS5wYXRoIDogdGhpcy5jb25maWcuYmFzZVVybCArIHRoaXMubWV0YS5wYXRoLFxuICAgICAgICBib2R5LFxuICAgICAgICB7aGVhZGVyczogdGhpcy5tZXRhLmVudGl0eU1ldGEuZ2V0SGVhZGVycyhlbnRpdHkpfVxuICAgICAgKS5waXBlKFxuICAgICAgICB0YXAoKGRhdGEpID0+IHtcbiAgICAgICAgICBtZXRhID0gdGhpcy5pbmZvU2VydmljZS5nZXRGb3JFbnRpdHkoZGF0YSk7XG4gICAgICAgICAgaWYgKCFtZXRhKSB7XG4gICAgICAgICAgICB0aGlzLmluZm9TZXJ2aWNlLnNldEZvckVudGl0eShlbnRpdHksIG1ldGEpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSksXG4gICAgICAgIHRhcCgoKSA9PiB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEFmdGVyU2F2ZUV2ZW50KGVudGl0eSkpKVxuICAgICAgKTtcbiAgfVxuXG4gIHJlbW92ZShlbnRpdHk6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBCZWZvcmVSZW1vdmVFdmVudChlbnRpdHkpKTtcbiAgICBjb25zdCBtZXRhID0gdGhpcy5pbmZvU2VydmljZS5nZXRGb3JFbnRpdHkoZW50aXR5KTtcbiAgICBpZiAobWV0YSkge1xuICAgICAgcmV0dXJuIHRoaXMuaHR0cC5kZWxldGUodGhpcy5jb25maWcuYmFzZVVybCArIG1ldGEucGF0aCwge2hlYWRlcnM6IHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldEhlYWRlcnMoZW50aXR5KX0pLnBpcGUoXG4gICAgICAgIHRhcCgoKSA9PiB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEFmdGVyUmVtb3ZlRXZlbnQoZW50aXR5KSkpXG4gICAgICApO1xuICAgIH1cbiAgfVxufVxuIl19