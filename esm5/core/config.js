/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/**
 * @record
 */
export function ApiConfig() { }
function ApiConfig_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    ApiConfig.prototype.responseType;
    /** @type {?} */
    ApiConfig.prototype.baseUrl;
    /** @type {?|undefined} */
    ApiConfig.prototype.authUrl;
    /** @type {?|undefined} */
    ApiConfig.prototype.cache;
    /** @type {?|undefined} */
    ApiConfig.prototype.tokenProvider;
    /** @type {?|undefined} */
    ApiConfig.prototype.authService;
    /** @type {?|undefined} */
    ApiConfig.prototype.cacheTTL;
    /** @type {?|undefined} */
    ApiConfig.prototype.excludedUrls;
    /** @type {?|undefined} */
    ApiConfig.prototype.defaultHeaders;
}
export var /** @type {?} */ AnRestConfig = new InjectionToken('anrest.api_config');

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJjb3JlL2NvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWMvQyxNQUFNLENBQUMscUJBQU0sWUFBWSxHQUFHLElBQUksY0FBYyxDQUFZLG1CQUFtQixDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEFwaUNvbmZpZyB7XG4gIHJlc3BvbnNlVHlwZT86IHN0cmluZztcbiAgYmFzZVVybDogc3RyaW5nO1xuICBhdXRoVXJsPzogc3RyaW5nO1xuICBjYWNoZT86IGFueTtcbiAgdG9rZW5Qcm92aWRlcj86IGFueTtcbiAgYXV0aFNlcnZpY2U/OiBhbnk7XG4gIGNhY2hlVFRMPzogbnVtYmVyO1xuICBleGNsdWRlZFVybHM/OiBzdHJpbmdbXTtcbiAgZGVmYXVsdEhlYWRlcnM/OiB7IG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZywgYXBwZW5kPzogYm9vbGVhbn1bXTtcbn1cblxuZXhwb3J0IGNvbnN0IEFuUmVzdENvbmZpZyA9IG5ldyBJbmplY3Rpb25Ub2tlbjxBcGlDb25maWc+KCdhbnJlc3QuYXBpX2NvbmZpZycpO1xuIl19