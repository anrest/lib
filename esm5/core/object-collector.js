/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { DataInfoService } from './data-info.service';
var ObjectCollector = /** @class */ (function () {
    function ObjectCollector(info) {
        this.info = info;
        this.map = new Map();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    ObjectCollector.prototype.set = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.map.set(this.info.get(data).path, data);
    };
    /**
     * @param {?} data
     * @return {?}
     */
    ObjectCollector.prototype.remove = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var /** @type {?} */ info = this.info.get(data);
        if (info) {
            data = info.path;
        }
        this.map.delete(data);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ObjectCollector.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.map.get(id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ObjectCollector.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.map.has(id);
    };
    /**
     * @return {?}
     */
    ObjectCollector.prototype.clear = /**
     * @return {?}
     */
    function () {
        this.map.clear();
    };
    ObjectCollector.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ObjectCollector.ctorParameters = function () { return [
        { type: DataInfoService, },
    ]; };
    return ObjectCollector;
}());
export { ObjectCollector };
function ObjectCollector_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ObjectCollector.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ObjectCollector.ctorParameters;
    /** @type {?} */
    ObjectCollector.prototype.map;
    /** @type {?} */
    ObjectCollector.prototype.info;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LWNvbGxlY3Rvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY29yZS9vYmplY3QtY29sbGVjdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7SUFNbEQseUJBQW9CLElBQXFCO1FBQXJCLFNBQUksR0FBSixJQUFJLENBQWlCO21CQUZoQixJQUFJLEdBQUcsRUFBZTtLQUVGOzs7OztJQUU3Qyw2QkFBRzs7OztJQUFILFVBQUksSUFBUztRQUNYLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztLQUM5Qzs7Ozs7SUFFRCxnQ0FBTTs7OztJQUFOLFVBQU8sSUFBUztRQUNkLHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1QsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDbEI7UUFDRCxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUN2Qjs7Ozs7SUFFRCw2QkFBRzs7OztJQUFILFVBQUksRUFBVTtRQUNaLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUN6Qjs7Ozs7SUFFRCw2QkFBRzs7OztJQUFILFVBQUksRUFBVTtRQUNaLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUN6Qjs7OztJQUVELCtCQUFLOzs7SUFBTDtRQUNFLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDbEI7O2dCQTVCSixVQUFVOzs7O2dCQUZGLGVBQWU7OzBCQUR4Qjs7U0FJYSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBPYmplY3RDb2xsZWN0b3Ige1xuICAgIG1hcDogTWFwPHN0cmluZywgYW55PiAgPSBuZXcgTWFwPHN0cmluZywgYW55PigpO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpbmZvOiBEYXRhSW5mb1NlcnZpY2UpIHt9XG5cbiAgICBzZXQoZGF0YTogYW55KSB7XG4gICAgICB0aGlzLm1hcC5zZXQodGhpcy5pbmZvLmdldChkYXRhKS5wYXRoLCBkYXRhKTtcbiAgICB9XG5cbiAgICByZW1vdmUoZGF0YTogYW55KSB7XG4gICAgICBjb25zdCBpbmZvID0gdGhpcy5pbmZvLmdldChkYXRhKTtcbiAgICAgIGlmIChpbmZvKSB7XG4gICAgICAgIGRhdGEgPSBpbmZvLnBhdGg7XG4gICAgICB9XG4gICAgICB0aGlzLm1hcC5kZWxldGUoZGF0YSk7XG4gICAgfVxuXG4gICAgZ2V0KGlkOiBzdHJpbmcpIHtcbiAgICAgIHJldHVybiB0aGlzLm1hcC5nZXQoaWQpO1xuICAgIH1cblxuICAgIGhhcyhpZDogc3RyaW5nKSB7XG4gICAgICByZXR1cm4gdGhpcy5tYXAuaGFzKGlkKTtcbiAgICB9XG5cbiAgICBjbGVhcigpIHtcbiAgICAgIHRoaXMubWFwLmNsZWFyKCk7XG4gICAgfVxufVxuIl19