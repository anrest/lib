/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Inject, Injectable, Optional } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AnRestConfig } from './config';
import { ANREST_HTTP_DATA_NORMALIZERS } from '../response/data-normalizer';
import { ApiProcessor } from './api.processor';
var ApiInterceptor = /** @class */ (function () {
    function ApiInterceptor(config, normalizers, processor) {
        this.config = config;
        this.normalizers = normalizers;
        this.processor = processor;
        if (!Array.isArray(this.normalizers)) {
            this.normalizers = [];
        }
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    ApiInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        var _this = this;
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        var /** @type {?} */ type;
        var /** @type {?} */ headers = request.headers;
        if (headers.has('X-AnRest-Type')) {
            type = headers.get('X-AnRest-Type');
            headers = headers.delete('x-AnRest-Type');
        }
        if (this.config.defaultHeaders) {
            try {
                for (var _a = tslib_1.__values(this.config.defaultHeaders), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var header = _b.value;
                    if (!headers.has(header.name) || header.append) {
                        headers = headers[header.append ? 'append' : 'set'](header.name, header.value);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        request = request.clone({
            headers: headers
        });
        return next.handle(request).pipe(map(function (response) {
            if (response instanceof HttpResponse) {
                var /** @type {?} */ contentType = response.headers.get('Content-Type');
                try {
                    for (var _a = tslib_1.__values(_this.normalizers), _b = _a.next(); !_b.done; _b = _a.next()) {
                        var normalizer = _b.value;
                        if (normalizer.supports(contentType)) {
                            return response.clone({ body: _this.processor.process(normalizer.normalize(response, type), request.method) });
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            return response;
            var e_2, _c;
        }));
        var e_1, _c;
    };
    ApiInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ApiInterceptor.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
        { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_DATA_NORMALIZERS,] },] },
        { type: ApiProcessor, },
    ]; };
    return ApiInterceptor;
}());
export { ApiInterceptor };
function ApiInterceptor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ApiInterceptor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ApiInterceptor.ctorParameters;
    /** @type {?} */
    ApiInterceptor.prototype.config;
    /** @type {?} */
    ApiInterceptor.prototype.normalizers;
    /** @type {?} */
    ApiInterceptor.prototype.processor;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJjb3JlL2FwaS5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBSVksWUFBWSxFQUM5QixNQUFNLHNCQUFzQixDQUFDO0FBRTlCLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsWUFBWSxFQUFhLE1BQU0sVUFBVSxDQUFDO0FBQ25ELE9BQU8sRUFBRSw0QkFBNEIsRUFBa0IsTUFBTSw2QkFBNkIsQ0FBQztBQUMzRixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0lBSzdDLHdCQUMyQyxRQUMwQixhQUNsRDtRQUZ3QixXQUFNLEdBQU4sTUFBTTtRQUNvQixnQkFBVyxHQUFYLFdBQVc7UUFDN0QsY0FBUyxHQUFULFNBQVM7UUFFMUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7U0FDdkI7S0FDRjs7Ozs7O0lBRUQsa0NBQVM7Ozs7O0lBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO1FBQXRELGlCQWtDQztRQWpDQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTztlQUNwRixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QscUJBQUksSUFBWSxDQUFDO1FBQ2pCLHFCQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBQzlCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3BDLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDOztnQkFDL0IsR0FBRyxDQUFDLENBQWlCLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQSxnQkFBQTtvQkFBMUMsSUFBTSxNQUFNLFdBQUE7b0JBQ2YsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt3QkFDL0MsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUNoRjtpQkFDRjs7Ozs7Ozs7O1NBQ0Y7UUFDRCxPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUN0QixPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQzlCLEdBQUcsQ0FBQyxVQUFDLFFBQVE7WUFDWCxFQUFFLENBQUMsQ0FBQyxRQUFRLFlBQVksWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDckMscUJBQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDOztvQkFDekQsR0FBRyxDQUFDLENBQXFCLElBQUEsS0FBQSxpQkFBQSxLQUFJLENBQUMsV0FBVyxDQUFBLGdCQUFBO3dCQUFwQyxJQUFNLFVBQVUsV0FBQTt3QkFDbkIsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3JDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7eUJBQy9HO3FCQUNGOzs7Ozs7Ozs7YUFDRjtZQUNELE1BQU0sQ0FBQyxRQUFRLENBQUM7O1NBQ2pCLENBQUMsQ0FDSCxDQUFDOztLQUNIOztnQkEvQ0YsVUFBVTs7OztnREFJTixNQUFNLFNBQUMsWUFBWTs0Q0FDbkIsUUFBUSxZQUFJLE1BQU0sU0FBQyw0QkFBNEI7Z0JBUDNDLFlBQVk7O3lCQVhyQjs7U0FjYSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUywgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuLi9yZXNwb25zZS9kYXRhLW5vcm1hbGl6ZXInO1xuaW1wb3J0IHsgQXBpUHJvY2Vzc29yIH0gZnJvbSAnLi9hcGkucHJvY2Vzc29yJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFwaUludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIHJlYWRvbmx5IGNvbmZpZzogQXBpQ29uZmlnLFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUykgcHJpdmF0ZSByZWFkb25seSBub3JtYWxpemVyczogRGF0YU5vcm1hbGl6ZXJbXSxcbiAgICBwcml2YXRlIHJlYWRvbmx5IHByb2Nlc3NvcjogQXBpUHJvY2Vzc29yXG4gICkge1xuICAgIGlmICghQXJyYXkuaXNBcnJheSh0aGlzLm5vcm1hbGl6ZXJzKSkge1xuICAgICAgdGhpcy5ub3JtYWxpemVycyA9IFtdO1xuICAgIH1cbiAgfVxuXG4gIGludGVyY2VwdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICBpZiAocmVxdWVzdC51cmwuaW5kZXhPZih0aGlzLmNvbmZpZy5iYXNlVXJsKSAhPT0gMCB8fCByZXF1ZXN0LnVybCA9PT0gdGhpcy5jb25maWcuYXV0aFVybFxuICAgICAgfHwgKEFycmF5LmlzQXJyYXkodGhpcy5jb25maWcuZXhjbHVkZWRVcmxzKSAmJiB0aGlzLmNvbmZpZy5leGNsdWRlZFVybHMuaW5kZXhPZihyZXF1ZXN0LnVybCkgIT09IC0xKSkge1xuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbiAgICBsZXQgdHlwZTogc3RyaW5nO1xuICAgIGxldCBoZWFkZXJzID0gcmVxdWVzdC5oZWFkZXJzO1xuICAgIGlmIChoZWFkZXJzLmhhcygnWC1BblJlc3QtVHlwZScpKSB7XG4gICAgICB0eXBlID0gaGVhZGVycy5nZXQoJ1gtQW5SZXN0LVR5cGUnKTtcbiAgICAgIGhlYWRlcnMgPSBoZWFkZXJzLmRlbGV0ZSgneC1BblJlc3QtVHlwZScpO1xuICAgIH1cbiAgICBpZiAodGhpcy5jb25maWcuZGVmYXVsdEhlYWRlcnMpIHtcbiAgICAgIGZvciAoY29uc3QgaGVhZGVyIG9mIHRoaXMuY29uZmlnLmRlZmF1bHRIZWFkZXJzKSB7XG4gICAgICAgIGlmICghaGVhZGVycy5oYXMoaGVhZGVyLm5hbWUpIHx8IGhlYWRlci5hcHBlbmQpIHtcbiAgICAgICAgICBoZWFkZXJzID0gaGVhZGVyc1toZWFkZXIuYXBwZW5kID8gJ2FwcGVuZCcgOiAnc2V0J10oaGVhZGVyLm5hbWUsIGhlYWRlci52YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xuICAgICAgaGVhZGVyczogaGVhZGVyc1xuICAgIH0pO1xuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KS5waXBlKFxuICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2UgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcbiAgICAgICAgICBjb25zdCBjb250ZW50VHlwZSA9IHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKTtcbiAgICAgICAgICBmb3IgKGNvbnN0IG5vcm1hbGl6ZXIgb2YgdGhpcy5ub3JtYWxpemVycykge1xuICAgICAgICAgICAgaWYgKG5vcm1hbGl6ZXIuc3VwcG9ydHMoY29udGVudFR5cGUpKSB7XG4gICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5jbG9uZSh7IGJvZHk6IHRoaXMucHJvY2Vzc29yLnByb2Nlc3Mobm9ybWFsaXplci5ub3JtYWxpemUocmVzcG9uc2UsIHR5cGUpLCByZXF1ZXN0Lm1ldGhvZCkgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIl19