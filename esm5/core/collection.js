/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { CollectionInfo } from './data-info.service';
import { map } from 'rxjs/operators';
/**
 * @template T
 */
var /**
 * @template T
 */
Collection = /** @class */ (function (_super) {
    tslib_1.__extends(Collection, _super);
    function Collection(service, info) {
        var items = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            items[_i - 2] = arguments[_i];
        }
        var _this = _super.apply(this, tslib_1.__spread(items)) || this;
        _this.service = service;
        _this.info = info;
        Object.setPrototypeOf(_this, Collection.prototype);
        return _this;
    }
    /**
     * @template T
     * @param {?} c1
     * @param {?} c2
     * @return {?}
     */
    Collection.merge = /**
     * @template T
     * @param {?} c1
     * @param {?} c2
     * @return {?}
     */
    function (c1, c2) {
        var /** @type {?} */ info = new CollectionInfo(c1.info.path);
        info.type = c1.info.type;
        info.next = c2.info.next;
        info.previous = c1.info.previous;
        info.first = c1.info.first;
        info.last = c1.info.last;
        info.total = c1.info.total;
        info.page = c1.info.page;
        info.lastPage = c1.info.lastPage;
        var /** @type {?} */ c = new Collection(c1.service, info);
        c1.forEach(function (v) { return c.push(v); });
        c2.forEach(function (v) { return c.push(v); });
        return c;
    };
    Object.defineProperty(Collection.prototype, "total", {
        get: /**
         * @return {?}
         */
        function () {
            return this.info.total;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Collection.prototype, "page", {
        get: /**
         * @return {?}
         */
        function () {
            return this.info.page;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Collection.prototype, "pageTotal", {
        get: /**
         * @return {?}
         */
        function () {
            return this.info.lastPage;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    Collection.prototype.first = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.first));
    };
    /**
     * @return {?}
     */
    Collection.prototype.previous = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.previous));
    };
    /**
     * @return {?}
     */
    Collection.prototype.next = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.next));
    };
    /**
     * @return {?}
     */
    Collection.prototype.last = /**
     * @return {?}
     */
    function () {
        return /** @type {?} */ (this.service.doGet(this.info.last));
    };
    /**
     * @return {?}
     */
    Collection.prototype.firstPath = /**
     * @return {?}
     */
    function () {
        return this.info.first;
    };
    /**
     * @return {?}
     */
    Collection.prototype.previousPath = /**
     * @return {?}
     */
    function () {
        return this.info.previous;
    };
    /**
     * @return {?}
     */
    Collection.prototype.nextPath = /**
     * @return {?}
     */
    function () {
        return this.info.next;
    };
    /**
     * @return {?}
     */
    Collection.prototype.lastPath = /**
     * @return {?}
     */
    function () {
        return this.info.last;
    };
    /**
     * @return {?}
     */
    Collection.prototype.loadMore = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return this.next().pipe(map(function (result) { return Collection.merge(_this, result); }));
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasMore = /**
     * @return {?}
     */
    function () {
        return this.hasNext();
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasNext = /**
     * @return {?}
     */
    function () {
        return !!this.info.next;
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasPrevious = /**
     * @return {?}
     */
    function () {
        return !!this.info.previous;
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasFirst = /**
     * @return {?}
     */
    function () {
        return this.hasPrevious();
    };
    /**
     * @return {?}
     */
    Collection.prototype.hasLast = /**
     * @return {?}
     */
    function () {
        return this.hasNext();
    };
    return Collection;
}(Array));
/**
 * @template T
 */
export { Collection };
function Collection_tsickle_Closure_declarations() {
    /** @type {?} */
    Collection.prototype.service;
    /** @type {?} */
    Collection.prototype.info;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY29yZS9jb2xsZWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR3JELE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUVyQzs7O0FBQUE7SUFBbUMsc0NBQVE7SUFrQnpDLG9CQUFvQixPQUFtQixFQUFVLElBQW9CO1FBQUUsZUFBYTthQUFiLFVBQWEsRUFBYixxQkFBYSxFQUFiLElBQWE7WUFBYiw4QkFBYTs7UUFBcEYsZ0RBQ1csS0FBSyxXQUVmO1FBSG1CLGFBQU8sR0FBUCxPQUFPLENBQVk7UUFBVSxVQUFJLEdBQUosSUFBSSxDQUFnQjtRQUVuRSxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUksRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7O0tBQ25EOzs7Ozs7O0lBbkJNLGdCQUFLOzs7Ozs7SUFBWixVQUFnQixFQUFpQixFQUFFLEVBQWlCO1FBQ2xELHFCQUFNLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNqQyxxQkFBTSxDQUFDLEdBQUcsSUFBSSxVQUFVLENBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5QyxFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBVCxDQUFTLENBQUMsQ0FBQztRQUM3QixFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBVCxDQUFTLENBQUMsQ0FBQztRQUM3QixNQUFNLENBQUMsQ0FBQyxDQUFDO0tBQ1Y7SUFPRCxzQkFBSSw2QkFBSzs7OztRQUFUO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQ3hCOzs7T0FBQTtJQUVELHNCQUFJLDRCQUFJOzs7O1FBQVI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDdkI7OztPQUFBO0lBRUQsc0JBQUksaUNBQVM7Ozs7UUFBYjtZQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztTQUMzQjs7O09BQUE7Ozs7SUFFRCwwQkFBSzs7O0lBQUw7UUFDRSxNQUFNLG1CQUFrQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFDO0tBQzdEOzs7O0lBRUQsNkJBQVE7OztJQUFSO1FBQ0UsTUFBTSxtQkFBa0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQztLQUNoRTs7OztJQUVELHlCQUFJOzs7SUFBSjtRQUNFLE1BQU0sbUJBQWtCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUM7S0FDNUQ7Ozs7SUFFRCx5QkFBSTs7O0lBQUo7UUFDRSxNQUFNLG1CQUFrQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFDO0tBQzVEOzs7O0lBRUQsOEJBQVM7OztJQUFUO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ3hCOzs7O0lBRUQsaUNBQVk7OztJQUFaO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0tBQzNCOzs7O0lBRUQsNkJBQVE7OztJQUFSO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3ZCOzs7O0lBRUQsNkJBQVE7OztJQUFSO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3ZCOzs7O0lBRUQsNkJBQVE7OztJQUFSO1FBQUEsaUJBSUM7UUFIQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FDckIsR0FBRyxDQUFDLFVBQUMsTUFBcUIsSUFBSyxPQUFBLFVBQVUsQ0FBQyxLQUFLLENBQUksS0FBSSxFQUFFLE1BQU0sQ0FBQyxFQUFqQyxDQUFpQyxDQUFDLENBQ2xFLENBQUM7S0FDSDs7OztJQUVELDRCQUFPOzs7SUFBUDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDdkI7Ozs7SUFFRCw0QkFBTzs7O0lBQVA7UUFDRSxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3pCOzs7O0lBRUQsZ0NBQVc7OztJQUFYO1FBQ0UsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztLQUM3Qjs7OztJQUVELDZCQUFROzs7SUFBUjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7S0FDM0I7Ozs7SUFFRCw0QkFBTzs7O0lBQVA7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ3ZCO3FCQWhHSDtFQUttQyxLQUFLLEVBNEZ2QyxDQUFBOzs7O0FBNUZELHNCQTRGQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbGxlY3Rpb25JbmZvIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQgeyBBcGlTZXJ2aWNlIH0gZnJvbSAnLi9hcGkuc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmV4cG9ydCBjbGFzcyBDb2xsZWN0aW9uPFQ+IGV4dGVuZHMgQXJyYXk8VD4ge1xuXG4gIHN0YXRpYyBtZXJnZTxUPihjMTogQ29sbGVjdGlvbjxUPiwgYzI6IENvbGxlY3Rpb248VD4pOiBDb2xsZWN0aW9uPFQ+IHtcbiAgICBjb25zdCBpbmZvID0gbmV3IENvbGxlY3Rpb25JbmZvKGMxLmluZm8ucGF0aCk7XG4gICAgaW5mby50eXBlID0gYzEuaW5mby50eXBlO1xuICAgIGluZm8ubmV4dCA9IGMyLmluZm8ubmV4dDtcbiAgICBpbmZvLnByZXZpb3VzID0gYzEuaW5mby5wcmV2aW91cztcbiAgICBpbmZvLmZpcnN0ID0gYzEuaW5mby5maXJzdDtcbiAgICBpbmZvLmxhc3QgPSBjMS5pbmZvLmxhc3Q7XG4gICAgaW5mby50b3RhbCA9IGMxLmluZm8udG90YWw7XG4gICAgaW5mby5wYWdlID0gYzEuaW5mby5wYWdlO1xuICAgIGluZm8ubGFzdFBhZ2UgPSBjMS5pbmZvLmxhc3RQYWdlO1xuICAgIGNvbnN0IGMgPSBuZXcgQ29sbGVjdGlvbjxUPihjMS5zZXJ2aWNlLCBpbmZvKTtcbiAgICBjMS5mb3JFYWNoKCh2KSA9PiBjLnB1c2godikpO1xuICAgIGMyLmZvckVhY2goKHYpID0+IGMucHVzaCh2KSk7XG4gICAgcmV0dXJuIGM7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZpY2U6IEFwaVNlcnZpY2UsIHByaXZhdGUgaW5mbzogQ29sbGVjdGlvbkluZm8sIC4uLml0ZW1zOiBUW10pIHtcbiAgICBzdXBlciguLi5pdGVtcyk7XG4gICAgT2JqZWN0LnNldFByb3RvdHlwZU9mKHRoaXMsIENvbGxlY3Rpb24ucHJvdG90eXBlKTtcbiAgfVxuXG4gIGdldCB0b3RhbCgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLmluZm8udG90YWw7XG4gIH1cblxuICBnZXQgcGFnZSgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLmluZm8ucGFnZTtcbiAgfVxuXG4gIGdldCBwYWdlVG90YWwoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLmxhc3RQYWdlO1xuICB9XG5cbiAgZmlyc3QoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gPE9ic2VydmFibGU8VFtdPj50aGlzLnNlcnZpY2UuZG9HZXQodGhpcy5pbmZvLmZpcnN0KTtcbiAgfVxuXG4gIHByZXZpb3VzKCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5wcmV2aW91cyk7XG4gIH1cblxuICBuZXh0KCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5uZXh0KTtcbiAgfVxuXG4gIGxhc3QoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gPE9ic2VydmFibGU8VFtdPj50aGlzLnNlcnZpY2UuZG9HZXQodGhpcy5pbmZvLmxhc3QpO1xuICB9XG5cbiAgZmlyc3RQYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5maXJzdDtcbiAgfVxuXG4gIHByZXZpb3VzUGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ucHJldmlvdXM7XG4gIH1cblxuICBuZXh0UGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ubmV4dDtcbiAgfVxuXG4gIGxhc3RQYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5sYXN0O1xuICB9XG5cbiAgbG9hZE1vcmUoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gdGhpcy5uZXh0KCkucGlwZShcbiAgICAgIG1hcCgocmVzdWx0OiBDb2xsZWN0aW9uPFQ+KSA9PiBDb2xsZWN0aW9uLm1lcmdlPFQ+KHRoaXMsIHJlc3VsdCkpXG4gICAgKTtcbiAgfVxuXG4gIGhhc01vcmUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuaGFzTmV4dCgpO1xuICB9XG5cbiAgaGFzTmV4dCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gISF0aGlzLmluZm8ubmV4dDtcbiAgfVxuXG4gIGhhc1ByZXZpb3VzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAhIXRoaXMuaW5mby5wcmV2aW91cztcbiAgfVxuXG4gIGhhc0ZpcnN0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc1ByZXZpb3VzKCk7XG4gIH1cblxuICBoYXNMYXN0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc05leHQoKTtcbiAgfVxufVxuIl19