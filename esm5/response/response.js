/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ResponseNode = /** @class */ (function () {
    function ResponseNode() {
    }
    return ResponseNode;
}());
export { ResponseNode };
function ResponseNode_tsickle_Closure_declarations() {
    /** @type {?} */
    ResponseNode.prototype.data;
    /** @type {?} */
    ResponseNode.prototype.collection;
    /** @type {?} */
    ResponseNode.prototype.meta;
    /** @type {?} */
    ResponseNode.prototype.info;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbInJlc3BvbnNlL3Jlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxJQUFBOzs7dUJBQUE7SUFLQyxDQUFBO0FBTEQsd0JBS0MiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgUmVzcG9uc2VOb2RlIHtcbiAgcHVibGljIGRhdGE6IGFueTtcbiAgcHVibGljIGNvbGxlY3Rpb246IGJvb2xlYW47XG4gIHB1YmxpYyBtZXRhOiBhbnk7XG4gIHB1YmxpYyBpbmZvOiBhbnk7XG59XG4iXX0=