/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ResponseNode } from './response';
import { MetaService } from '../meta';
import { CollectionInfo, EntityInfo } from '../core';
var LdJsonDataNormalizer = /** @class */ (function () {
    function LdJsonDataNormalizer() {
    }
    /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    LdJsonDataNormalizer.prototype.normalize = /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    function (response, type) {
        return this.process(response.body);
    };
    /**
     * @param {?} type
     * @return {?}
     */
    LdJsonDataNormalizer.prototype.supports = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type.split(';')[0] === 'application/ld+json';
    };
    /**
     * @param {?} data
     * @param {?=} meta
     * @param {?=} collection
     * @return {?}
     */
    LdJsonDataNormalizer.prototype.process = /**
     * @param {?} data
     * @param {?=} meta
     * @param {?=} collection
     * @return {?}
     */
    function (data, meta, collection) {
        var /** @type {?} */ r = new ResponseNode();
        r.collection = collection || data['@type'] === 'hydra:Collection';
        var /** @type {?} */ path = data['@id'];
        r.meta = meta || MetaService.getByName(r.collection ? /\/contexts\/(\w+)/g.exec(data['@context'])[1] : data['@type']);
        if (r.collection) {
            var /** @type {?} */ items = data['hydra:member'] || data;
            r.info = new CollectionInfo(data['hydra:view'] ? data['hydra:view']['@id'] : path);
            r.info.type = r.meta.name;
            if (data['hydra:view']) {
                r.info.first = data['hydra:view']['hydra:first'];
                r.info.previous = data['hydra:view']['hydra:previous'];
                r.info.next = data['hydra:view']['hydra:next'];
                r.info.last = data['hydra:view']['hydra:last'];
                if (r.info.path === r.info.first) {
                    r.info.alias = data['@id'];
                }
            }
            r.info.total = data['hydra:totalItems'] || items.length;
            var /** @type {?} */ regexPage = /page=(\d+)/g;
            var /** @type {?} */ matches = void 0;
            matches = regexPage.exec(r.info.path);
            r.info.page = matches ? Number(matches[1]) : 1;
            matches = regexPage.exec(r.info.last);
            r.info.lastPage = matches ? Number(matches[1]) : 1;
            r.data = [];
            try {
                for (var items_1 = tslib_1.__values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                    var object = items_1_1.value;
                    r.data.push(this.process(object));
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (items_1_1 && !items_1_1.done && (_a = items_1.return)) _a.call(items_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            r.info = new EntityInfo(path);
            r.data = {};
            if (r.meta === undefined && Array.isArray(data)) {
                r.data = data;
            }
            else {
                for (var /** @type {?} */ key in data) {
                    if (key.indexOf('@') === 0 || key.indexOf('hydra:') === 0) {
                        continue;
                    }
                    var /** @type {?} */ propertyMeta = r.meta.properties[key];
                    r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                        this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
                }
            }
        }
        return r;
        var e_1, _a;
    };
    return LdJsonDataNormalizer;
}());
export { LdJsonDataNormalizer };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGQtanNvbi1kYXRhLW5vcm1hbGl6ZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbInJlc3BvbnNlL2xkLWpzb24tZGF0YS1ub3JtYWxpemVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBRUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFlBQVksQ0FBQztBQUMxQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxjQUFjLEVBQUUsVUFBVSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXJELElBQUE7Ozs7Ozs7O0lBQ0Usd0NBQVM7Ozs7O0lBQVQsVUFBVSxRQUEyQixFQUFFLElBQWE7UUFDbEQsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3BDOzs7OztJQUVELHVDQUFROzs7O0lBQVIsVUFBUyxJQUFZO1FBQ25CLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLHFCQUFxQixDQUFDO0tBQ3JEOzs7Ozs7O0lBRU8sc0NBQU87Ozs7OztjQUFDLElBQUksRUFBRSxJQUFVLEVBQUUsVUFBb0I7UUFDcEQscUJBQU0sQ0FBQyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLFVBQVUsR0FBRyxVQUFVLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLGtCQUFrQixDQUFDO1FBQ2xFLHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3RILEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLHFCQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDO1lBQzNDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25GLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDakQsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3ZELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDL0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDNUI7YUFDRjtZQUNELENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDeEQscUJBQU0sU0FBUyxHQUFHLGFBQWEsQ0FBQztZQUNoQyxxQkFBSSxPQUFPLFNBQUEsQ0FBQztZQUNaLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQyxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkQsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7O2dCQUNaLEdBQUcsQ0FBQyxDQUFpQixJQUFBLFVBQUEsaUJBQUEsS0FBSyxDQUFBLDRCQUFBO29CQUFyQixJQUFNLE1BQU0sa0JBQUE7b0JBQ2YsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUNuQzs7Ozs7Ozs7O1NBQ0Y7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDWixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDZjtZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLEdBQUcsQ0FBQyxDQUFDLHFCQUFNLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUN2QixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzFELFFBQVEsQ0FBQztxQkFDVjtvQkFDRCxxQkFBTSxZQUFZLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLElBQUksSUFBSSxZQUFZLElBQUksV0FBVyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RILElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN0RzthQUNGO1NBQ0Y7UUFDRCxNQUFNLENBQUMsQ0FBQyxDQUFDOzs7K0JBNURiO0lBOERDLENBQUE7QUF4REQsZ0NBd0RDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuL2RhdGEtbm9ybWFsaXplcic7XG5pbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuL3Jlc3BvbnNlJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uSW5mbywgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgTGRKc29uRGF0YU5vcm1hbGl6ZXIgaW1wbGVtZW50cyBEYXRhTm9ybWFsaXplciB7XG4gIG5vcm1hbGl6ZShyZXNwb25zZTogSHR0cFJlc3BvbnNlPGFueT4sIHR5cGU/OiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIHJldHVybiB0aGlzLnByb2Nlc3MocmVzcG9uc2UuYm9keSk7XG4gIH1cblxuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHlwZS5zcGxpdCgnOycpWzBdID09PSAnYXBwbGljYXRpb24vbGQranNvbic7XG4gIH1cblxuICBwcml2YXRlIHByb2Nlc3MoZGF0YSwgbWV0YT86IGFueSwgY29sbGVjdGlvbj86IGJvb2xlYW4pIHtcbiAgICBjb25zdCByID0gbmV3IFJlc3BvbnNlTm9kZSgpO1xuICAgIHIuY29sbGVjdGlvbiA9IGNvbGxlY3Rpb24gfHwgZGF0YVsnQHR5cGUnXSA9PT0gJ2h5ZHJhOkNvbGxlY3Rpb24nO1xuICAgIGNvbnN0IHBhdGggPSBkYXRhWydAaWQnXTtcbiAgICByLm1ldGEgPSBtZXRhIHx8IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShyLmNvbGxlY3Rpb24gPyAvXFwvY29udGV4dHNcXC8oXFx3KykvZy5leGVjKGRhdGFbJ0Bjb250ZXh0J10pWzFdIDogZGF0YVsnQHR5cGUnXSk7XG4gICAgaWYgKHIuY29sbGVjdGlvbikge1xuICAgICAgY29uc3QgaXRlbXMgPSBkYXRhWydoeWRyYTptZW1iZXInXSB8fCBkYXRhO1xuICAgICAgci5pbmZvID0gbmV3IENvbGxlY3Rpb25JbmZvKGRhdGFbJ2h5ZHJhOnZpZXcnXSA/IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnQGlkJ10gOiBwYXRoKTtcbiAgICAgIHIuaW5mby50eXBlID0gci5tZXRhLm5hbWU7XG4gICAgICBpZiAoZGF0YVsnaHlkcmE6dmlldyddKSB7XG4gICAgICAgIHIuaW5mby5maXJzdCA9IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnaHlkcmE6Zmlyc3QnXTtcbiAgICAgICAgci5pbmZvLnByZXZpb3VzID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpwcmV2aW91cyddO1xuICAgICAgICByLmluZm8ubmV4dCA9IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnaHlkcmE6bmV4dCddO1xuICAgICAgICByLmluZm8ubGFzdCA9IGRhdGFbJ2h5ZHJhOnZpZXcnXVsnaHlkcmE6bGFzdCddO1xuICAgICAgICBpZiAoci5pbmZvLnBhdGggPT09IHIuaW5mby5maXJzdCkge1xuICAgICAgICAgIHIuaW5mby5hbGlhcyA9IGRhdGFbJ0BpZCddO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByLmluZm8udG90YWwgPSBkYXRhWydoeWRyYTp0b3RhbEl0ZW1zJ10gfHwgaXRlbXMubGVuZ3RoO1xuICAgICAgY29uc3QgcmVnZXhQYWdlID0gL3BhZ2U9KFxcZCspL2c7XG4gICAgICBsZXQgbWF0Y2hlcztcbiAgICAgIG1hdGNoZXMgPSByZWdleFBhZ2UuZXhlYyhyLmluZm8ucGF0aCk7XG4gICAgICByLmluZm8ucGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgbWF0Y2hlcyA9IHJlZ2V4UGFnZS5leGVjKHIuaW5mby5sYXN0KTtcbiAgICAgIHIuaW5mby5sYXN0UGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgci5kYXRhID0gW107XG4gICAgICBmb3IgKGNvbnN0IG9iamVjdCBvZiBpdGVtcykge1xuICAgICAgICByLmRhdGEucHVzaCh0aGlzLnByb2Nlc3Mob2JqZWN0KSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHIuaW5mbyA9IG5ldyBFbnRpdHlJbmZvKHBhdGgpO1xuICAgICAgci5kYXRhID0ge307XG4gICAgICBpZiAoci5tZXRhID09PSB1bmRlZmluZWQgJiYgQXJyYXkuaXNBcnJheShkYXRhKSkge1xuICAgICAgICByLmRhdGEgPSBkYXRhO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuICAgICAgICAgIGlmIChrZXkuaW5kZXhPZignQCcpID09PSAwIHx8IGtleS5pbmRleE9mKCdoeWRyYTonKSA9PT0gMCkge1xuICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnN0IHByb3BlcnR5TWV0YSA9IHIubWV0YS5wcm9wZXJ0aWVzW2tleV07XG4gICAgICAgICAgci5kYXRhW2tleV0gPSAoQXJyYXkuaXNBcnJheShkYXRhW2tleV0pIHx8IChkYXRhW2tleV0gIT09IG51bGwgJiYgcHJvcGVydHlNZXRhICYmIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSkpKSA/XG4gICAgICAgICAgICB0aGlzLnByb2Nlc3MoZGF0YVtrZXldLCBNZXRhU2VydmljZS5nZXQocHJvcGVydHlNZXRhLnR5cGUpLCBwcm9wZXJ0eU1ldGEuaXNDb2xsZWN0aW9uKSA6IGRhdGFba2V5XTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcjtcbiAgfVxufVxuIl19