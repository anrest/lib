/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ResponseNode } from './response';
import { MetaService } from '../meta';
import { CollectionInfo, EntityInfo } from '../core';
var JsonDataNormalizer = /** @class */ (function () {
    function JsonDataNormalizer() {
    }
    /**
     * @param {?} response
     * @param {?} type
     * @return {?}
     */
    JsonDataNormalizer.prototype.normalize = /**
     * @param {?} response
     * @param {?} type
     * @return {?}
     */
    function (response, type) {
        return this.process(response.body, MetaService.getByName(type), Array.isArray(response.body), response.headers);
    };
    /**
     * @param {?} type
     * @return {?}
     */
    JsonDataNormalizer.prototype.supports = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type.split(';')[0] === 'application/json';
    };
    /**
     * @param {?} data
     * @param {?} meta
     * @param {?} collection
     * @param {?=} headers
     * @return {?}
     */
    JsonDataNormalizer.prototype.process = /**
     * @param {?} data
     * @param {?} meta
     * @param {?} collection
     * @param {?=} headers
     * @return {?}
     */
    function (data, meta, collection, headers) {
        var /** @type {?} */ r = new ResponseNode();
        r.collection = collection;
        var /** @type {?} */ collectionPath = MetaService.get(meta.service) ? MetaService.get(meta.service).path : undefined;
        var /** @type {?} */ path = collectionPath ? (collection ? collectionPath : collectionPath + '/' + data[meta.id]) : undefined;
        r.meta = meta;
        if (r.collection) {
            r.info = new CollectionInfo(headers.get('X-Current-Page') || path);
            r.info.type = r.meta.name;
            r.info.first = headers.get('X-First-Page') || undefined;
            r.info.previous = headers.get('X-Prev-Page') || undefined;
            r.info.next = headers.get('X-Next-Page') || undefined;
            r.info.last = headers.get('X-Last-Page') || undefined;
            if (r.info.path === r.info.first) {
                r.info.alias = path;
            }
            r.info.total = headers.get('X-Total') || data.length;
            var /** @type {?} */ regexPage = /page=(\d+)/g;
            var /** @type {?} */ matches = void 0;
            matches = regexPage.exec(r.info.path);
            r.info.page = matches ? Number(matches[1]) : 1;
            matches = regexPage.exec(r.info.last);
            r.info.lastPage = matches ? Number(matches[1]) : 1;
            r.data = [];
            try {
                for (var data_1 = tslib_1.__values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                    var object = data_1_1.value;
                    r.data.push(this.process(object, r.meta, false));
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (data_1_1 && !data_1_1.done && (_a = data_1.return)) _a.call(data_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            r.info = new EntityInfo(path);
            r.data = {};
            for (var /** @type {?} */ key in data) {
                var /** @type {?} */ propertyMeta = r.meta.properties[key];
                r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                    this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
            }
        }
        return r;
        var e_1, _a;
    };
    return JsonDataNormalizer;
}());
export { JsonDataNormalizer };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1kYXRhLW5vcm1hbGl6ZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbInJlc3BvbnNlL2pzb24tZGF0YS1ub3JtYWxpemVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBRUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFlBQVksQ0FBQztBQUMxQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxjQUFjLEVBQUUsVUFBVSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXJELElBQUE7Ozs7Ozs7O0lBQ0Usc0NBQVM7Ozs7O0lBQVQsVUFBVSxRQUEyQixFQUFFLElBQVk7UUFDakQsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUNqSDs7Ozs7SUFFRCxxQ0FBUTs7OztJQUFSLFVBQVMsSUFBWTtRQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxrQkFBa0IsQ0FBQztLQUNsRDs7Ozs7Ozs7SUFFTyxvQ0FBTzs7Ozs7OztjQUFDLElBQUksRUFBRSxJQUFTLEVBQUUsVUFBbUIsRUFBRSxPQUFxQjtRQUN6RSxxQkFBTSxDQUFDLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMxQixxQkFBTSxjQUFjLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1FBQ3RHLHFCQUFNLElBQUksR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLGNBQWMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDL0csQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDZCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNqQixDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztZQUNuRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUN4RCxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUMxRCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUN0RCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUN0RCxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQzthQUNyQjtZQUNELENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNyRCxxQkFBTSxTQUFTLEdBQUcsYUFBYSxDQUFDO1lBQ2hDLHFCQUFJLE9BQU8sU0FBQSxDQUFDO1lBQ1osT0FBTyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9DLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRCxDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzs7Z0JBQ1osR0FBRyxDQUFDLENBQWlCLElBQUEsU0FBQSxpQkFBQSxJQUFJLENBQUEsMEJBQUE7b0JBQXBCLElBQU0sTUFBTSxpQkFBQTtvQkFDZixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7aUJBQ2xEOzs7Ozs7Ozs7U0FDRjtRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNaLEdBQUcsQ0FBQyxDQUFDLHFCQUFNLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixxQkFBTSxZQUFZLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLElBQUksSUFBSSxZQUFZLElBQUksV0FBVyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3RILElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3RHO1NBQ0Y7UUFDRCxNQUFNLENBQUMsQ0FBQyxDQUFDOzs7NkJBbkRiO0lBcURDLENBQUE7QUEvQ0QsOEJBK0NDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuL2RhdGEtbm9ybWFsaXplcic7XG5pbXBvcnQgeyBIdHRwSGVhZGVycywgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi9yZXNwb25zZSc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbkluZm8sIEVudGl0eUluZm8gfSBmcm9tICcuLi9jb3JlJztcblxuZXhwb3J0IGNsYXNzIEpzb25EYXRhTm9ybWFsaXplciBpbXBsZW1lbnRzIERhdGFOb3JtYWxpemVyIHtcbiAgbm9ybWFsaXplKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2U8YW55PiwgdHlwZTogc3RyaW5nKTogUmVzcG9uc2VOb2RlIHtcbiAgICByZXR1cm4gdGhpcy5wcm9jZXNzKHJlc3BvbnNlLmJvZHksIE1ldGFTZXJ2aWNlLmdldEJ5TmFtZSh0eXBlKSwgQXJyYXkuaXNBcnJheShyZXNwb25zZS5ib2R5KSwgcmVzcG9uc2UuaGVhZGVycyk7XG4gIH1cblxuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHlwZS5zcGxpdCgnOycpWzBdID09PSAnYXBwbGljYXRpb24vanNvbic7XG4gIH1cblxuICBwcml2YXRlIHByb2Nlc3MoZGF0YSwgbWV0YTogYW55LCBjb2xsZWN0aW9uOiBib29sZWFuLCBoZWFkZXJzPzogSHR0cEhlYWRlcnMpIHtcbiAgICBjb25zdCByID0gbmV3IFJlc3BvbnNlTm9kZSgpO1xuICAgIHIuY29sbGVjdGlvbiA9IGNvbGxlY3Rpb247XG4gICAgY29uc3QgY29sbGVjdGlvblBhdGggPSBNZXRhU2VydmljZS5nZXQobWV0YS5zZXJ2aWNlKSA/IE1ldGFTZXJ2aWNlLmdldChtZXRhLnNlcnZpY2UpLnBhdGggOiB1bmRlZmluZWQ7XG4gICAgY29uc3QgcGF0aCA9IGNvbGxlY3Rpb25QYXRoID8gKGNvbGxlY3Rpb24gPyBjb2xsZWN0aW9uUGF0aCA6IGNvbGxlY3Rpb25QYXRoICsgJy8nICsgZGF0YVttZXRhLmlkXSkgOiB1bmRlZmluZWQ7XG4gICAgci5tZXRhID0gbWV0YTtcbiAgICBpZiAoci5jb2xsZWN0aW9uKSB7XG4gICAgICByLmluZm8gPSBuZXcgQ29sbGVjdGlvbkluZm8oaGVhZGVycy5nZXQoJ1gtQ3VycmVudC1QYWdlJykgfHwgcGF0aCk7XG4gICAgICByLmluZm8udHlwZSA9IHIubWV0YS5uYW1lO1xuICAgICAgci5pbmZvLmZpcnN0ID0gaGVhZGVycy5nZXQoJ1gtRmlyc3QtUGFnZScpIHx8IHVuZGVmaW5lZDtcbiAgICAgIHIuaW5mby5wcmV2aW91cyA9IGhlYWRlcnMuZ2V0KCdYLVByZXYtUGFnZScpIHx8IHVuZGVmaW5lZDtcbiAgICAgIHIuaW5mby5uZXh0ID0gaGVhZGVycy5nZXQoJ1gtTmV4dC1QYWdlJykgfHwgdW5kZWZpbmVkO1xuICAgICAgci5pbmZvLmxhc3QgPSBoZWFkZXJzLmdldCgnWC1MYXN0LVBhZ2UnKSB8fCB1bmRlZmluZWQ7XG4gICAgICBpZiAoci5pbmZvLnBhdGggPT09IHIuaW5mby5maXJzdCkge1xuICAgICAgICByLmluZm8uYWxpYXMgPSBwYXRoO1xuICAgICAgfVxuICAgICAgci5pbmZvLnRvdGFsID0gaGVhZGVycy5nZXQoJ1gtVG90YWwnKSB8fCBkYXRhLmxlbmd0aDtcbiAgICAgIGNvbnN0IHJlZ2V4UGFnZSA9IC9wYWdlPShcXGQrKS9nO1xuICAgICAgbGV0IG1hdGNoZXM7XG4gICAgICBtYXRjaGVzID0gcmVnZXhQYWdlLmV4ZWMoci5pbmZvLnBhdGgpO1xuICAgICAgci5pbmZvLnBhZ2UgPSBtYXRjaGVzID8gTnVtYmVyKG1hdGNoZXNbMV0pIDogMTtcbiAgICAgIG1hdGNoZXMgPSByZWdleFBhZ2UuZXhlYyhyLmluZm8ubGFzdCk7XG4gICAgICByLmluZm8ubGFzdFBhZ2UgPSBtYXRjaGVzID8gTnVtYmVyKG1hdGNoZXNbMV0pIDogMTtcbiAgICAgIHIuZGF0YSA9IFtdO1xuICAgICAgZm9yIChjb25zdCBvYmplY3Qgb2YgZGF0YSkge1xuICAgICAgICByLmRhdGEucHVzaCh0aGlzLnByb2Nlc3Mob2JqZWN0LCByLm1ldGEsIGZhbHNlKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHIuaW5mbyA9IG5ldyBFbnRpdHlJbmZvKHBhdGgpO1xuICAgICAgci5kYXRhID0ge307XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgIGNvbnN0IHByb3BlcnR5TWV0YSA9IHIubWV0YS5wcm9wZXJ0aWVzW2tleV07XG4gICAgICAgIHIuZGF0YVtrZXldID0gKEFycmF5LmlzQXJyYXkoZGF0YVtrZXldKSB8fCAoZGF0YVtrZXldICE9PSBudWxsICYmIHByb3BlcnR5TWV0YSAmJiBNZXRhU2VydmljZS5nZXQocHJvcGVydHlNZXRhLnR5cGUpKSkgP1xuICAgICAgICAgIHRoaXMucHJvY2VzcyhkYXRhW2tleV0sIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSksIHByb3BlcnR5TWV0YS5pc0NvbGxlY3Rpb24pIDogZGF0YVtrZXldO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcjtcbiAgfVxufVxuIl19