/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { ResponseNode } from './response';
import { EntityInfo } from '../core';
var ReferenceDataNormalizer = /** @class */ (function () {
    function ReferenceDataNormalizer() {
    }
    /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    ReferenceDataNormalizer.prototype.normalize = /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    function (response, type) {
        var /** @type {?} */ r = new ResponseNode();
        r.collection = false;
        r.info = new EntityInfo(response.body.path);
        r.meta = response.body.meta;
        r.data = {};
        return r;
    };
    /**
     * @param {?} type
     * @return {?}
     */
    ReferenceDataNormalizer.prototype.supports = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type === '@reference';
    };
    return ReferenceDataNormalizer;
}());
export { ReferenceDataNormalizer };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmZXJlbmNlLWRhdGEtbm9ybWFsaXplci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsicmVzcG9uc2UvcmVmZXJlbmNlLWRhdGEtbm9ybWFsaXplci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBRUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFlBQVksQ0FBQztBQUMxQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBRXJDLElBQUE7Ozs7Ozs7O0lBQ0UsMkNBQVM7Ozs7O0lBQVQsVUFBVSxRQUEyQixFQUFFLElBQWE7UUFDbEQscUJBQU0sQ0FBQyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLENBQUMsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDNUIsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDWixNQUFNLENBQUMsQ0FBQyxDQUFDO0tBQ1Y7Ozs7O0lBRUQsMENBQVE7Ozs7SUFBUixVQUFTLElBQVk7UUFDbkIsTUFBTSxDQUFDLElBQUksS0FBSyxZQUFZLENBQUM7S0FDOUI7a0NBakJIO0lBa0JDLENBQUE7QUFiRCxtQ0FhQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERhdGFOb3JtYWxpemVyIH0gZnJvbSAnLi9kYXRhLW5vcm1hbGl6ZXInO1xuaW1wb3J0IHsgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi9yZXNwb25zZSc7XG5pbXBvcnQgeyBFbnRpdHlJbmZvIH0gZnJvbSAnLi4vY29yZSc7XG5cbmV4cG9ydCBjbGFzcyBSZWZlcmVuY2VEYXRhTm9ybWFsaXplciBpbXBsZW1lbnRzIERhdGFOb3JtYWxpemVyIHtcbiAgbm9ybWFsaXplKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2U8YW55PiwgdHlwZT86IHN0cmluZyk6IFJlc3BvbnNlTm9kZSB7XG4gICAgY29uc3QgciA9IG5ldyBSZXNwb25zZU5vZGUoKTtcbiAgICByLmNvbGxlY3Rpb24gPSBmYWxzZTtcbiAgICByLmluZm8gPSBuZXcgRW50aXR5SW5mbyhyZXNwb25zZS5ib2R5LnBhdGgpO1xuICAgIHIubWV0YSA9IHJlc3BvbnNlLmJvZHkubWV0YTtcbiAgICByLmRhdGEgPSB7fTtcbiAgICByZXR1cm4gcjtcbiAgfVxuXG4gIHN1cHBvcnRzKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0eXBlID09PSAnQHJlZmVyZW5jZSc7XG4gIH1cbn1cbiJdfQ==