/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import { AnRestConfig } from '../core/config';
import { AuthTokenProviderService } from './auth-token-provider.service';
var JwtAuthService = /** @class */ (function () {
    function JwtAuthService(http, token, config) {
        this.http = http;
        this.token = token;
        this.config = config;
    }
    /**
     * @return {?}
     */
    JwtAuthService.prototype.getToken = /**
     * @return {?}
     */
    function () {
        return this.token.get();
    };
    /**
     * @param {?} token
     * @return {?}
     */
    JwtAuthService.prototype.setToken = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        this.token.set(token);
    };
    /**
     * @return {?}
     */
    JwtAuthService.prototype.remove = /**
     * @return {?}
     */
    function () {
        this.token.remove();
    };
    /**
     * @return {?}
     */
    JwtAuthService.prototype.isSignedIn = /**
     * @return {?}
     */
    function () {
        return this.token.isSet();
    };
    /**
     * @return {?}
     */
    JwtAuthService.prototype.getInfo = /**
     * @return {?}
     */
    function () {
        return jwt_decode.call(this.getToken());
    };
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    JwtAuthService.prototype.signIn = /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    function (username, password) {
        var _this = this;
        return this.http.post(this.config.authUrl, {
            login: username,
            password: password
        }).pipe(tap(function (data) {
            _this.setToken(data.token);
        }));
    };
    JwtAuthService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    JwtAuthService.ctorParameters = function () { return [
        { type: HttpClient, },
        { type: AuthTokenProviderService, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return JwtAuthService;
}());
export { JwtAuthService };
function JwtAuthService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    JwtAuthService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    JwtAuthService.ctorParameters;
    /** @type {?} */
    JwtAuthService.prototype.http;
    /** @type {?} */
    JwtAuthService.prototype.token;
    /** @type {?} */
    JwtAuthService.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LWF1dGguc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYXV0aC9qd3QtYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sS0FBSyxVQUFVLE1BQU0sWUFBWSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUd6RCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7SUFLdkUsd0JBQ1UsTUFDQSxPQUN3QjtRQUZ4QixTQUFJLEdBQUosSUFBSTtRQUNKLFVBQUssR0FBTCxLQUFLO1FBQ21CLFdBQU0sR0FBTixNQUFNO0tBQ3BDOzs7O0lBRUosaUNBQVE7OztJQUFSO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDekI7Ozs7O0lBRUQsaUNBQVE7Ozs7SUFBUixVQUFTLEtBQWE7UUFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDdkI7Ozs7SUFFRCwrQkFBTTs7O0lBQU47UUFDRSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO0tBQ3JCOzs7O0lBRUQsbUNBQVU7OztJQUFWO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDM0I7Ozs7SUFFRCxnQ0FBTzs7O0lBQVA7UUFDRSxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztLQUN6Qzs7Ozs7O0lBRUQsK0JBQU07Ozs7O0lBQU4sVUFBTyxRQUFnQixFQUFFLFFBQWdCO1FBQXpDLGlCQVNDO1FBUkMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFO1lBQ3pDLEtBQUssRUFBRSxRQUFRO1lBQ2YsUUFBUSxFQUFFLFFBQVE7U0FDbkIsQ0FBQyxDQUFDLElBQUksQ0FDTCxHQUFHLENBQUMsVUFBQyxJQUFTO1lBQ1osS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0IsQ0FBQyxDQUNILENBQUM7S0FDSDs7Z0JBdENGLFVBQVU7Ozs7Z0JBUkYsVUFBVTtnQkFNVix3QkFBd0I7Z0RBUTVCLE1BQU0sU0FBQyxZQUFZOzt5QkFmeEI7O1NBVWEsY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCAqIGFzIGp3dF9kZWNvZGUgZnJvbSAnand0LWRlY29kZSc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4uL2NvcmUvY29uZmlnJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGggfSBmcm9tICcuL2F1dGgnO1xuaW1wb3J0IHsgQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgSnd0QXV0aFNlcnZpY2UgaW1wbGVtZW50cyBBdXRoIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJpdmF0ZSB0b2tlbjogQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgY29uZmlnOiBBcGlDb25maWdcbiAgKSB7fVxuXG4gIGdldFRva2VuKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMudG9rZW4uZ2V0KCk7XG4gIH1cblxuICBzZXRUb2tlbih0b2tlbjogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy50b2tlbi5zZXQodG9rZW4pO1xuICB9XG5cbiAgcmVtb3ZlKCk6IHZvaWQge1xuICAgIHRoaXMudG9rZW4ucmVtb3ZlKCk7XG4gIH1cblxuICBpc1NpZ25lZEluKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnRva2VuLmlzU2V0KCk7XG4gIH1cblxuICBnZXRJbmZvKCk6IGFueSB7XG4gICAgcmV0dXJuIGp3dF9kZWNvZGUuY2FsbCh0aGlzLmdldFRva2VuKCkpO1xuICB9XG5cbiAgc2lnbkluKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZy5hdXRoVXJsLCB7XG4gICAgICBsb2dpbjogdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZDogcGFzc3dvcmRcbiAgICB9KS5waXBlKFxuICAgICAgdGFwKChkYXRhOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5zZXRUb2tlbihkYXRhLnRva2VuKTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxuXG59XG4iXX0=