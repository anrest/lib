/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */
export function AuthToken() { }
function AuthToken_tsickle_Closure_declarations() {
    /** @type {?} */
    AuthToken.prototype.get;
    /** @type {?} */
    AuthToken.prototype.set;
    /** @type {?} */
    AuthToken.prototype.isSet;
    /** @type {?} */
    AuthToken.prototype.remove;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC10b2tlbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYXV0aC9hdXRoLXRva2VuLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEF1dGhUb2tlbiB7XG4gIGdldCgpOiBzdHJpbmc7XG4gIHNldCh0b2tlbjogc3RyaW5nKTtcbiAgaXNTZXQoKTogYm9vbGVhbjtcbiAgcmVtb3ZlKCk7XG59XG4iXX0=