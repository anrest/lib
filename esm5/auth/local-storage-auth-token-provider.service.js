/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
var LocalStorageAuthTokenProviderService = /** @class */ (function () {
    function LocalStorageAuthTokenProviderService() {
        this.tokenName = 'anrest:auth_token';
    }
    /**
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.get = /**
     * @return {?}
     */
    function () {
        return localStorage.getItem(this.tokenName);
    };
    /**
     * @param {?} token
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.set = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        localStorage.setItem(this.tokenName, token);
    };
    /**
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.remove = /**
     * @return {?}
     */
    function () {
        localStorage.removeItem(this.tokenName);
    };
    /**
     * @return {?}
     */
    LocalStorageAuthTokenProviderService.prototype.isSet = /**
     * @return {?}
     */
    function () {
        return localStorage.getItem(this.tokenName) != null;
    };
    LocalStorageAuthTokenProviderService.decorators = [
        { type: Injectable },
    ];
    return LocalStorageAuthTokenProviderService;
}());
export { LocalStorageAuthTokenProviderService };
function LocalStorageAuthTokenProviderService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    LocalStorageAuthTokenProviderService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    LocalStorageAuthTokenProviderService.ctorParameters;
    /** @type {?} */
    LocalStorageAuthTokenProviderService.prototype.tokenName;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc3RvcmFnZS1hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImF1dGgvbG9jYWwtc3RvcmFnZS1hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozt5QkFJckIsbUJBQW1COzs7OztJQUV2QyxrREFBRzs7O0lBQUg7UUFDRSxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7S0FDN0M7Ozs7O0lBRUQsa0RBQUc7Ozs7SUFBSCxVQUFJLEtBQWE7UUFDZixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7S0FDN0M7Ozs7SUFFRCxxREFBTTs7O0lBQU47UUFDRSxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUN6Qzs7OztJQUVELG9EQUFLOzs7SUFBTDtRQUNFLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUM7S0FDckQ7O2dCQWxCRixVQUFVOzsrQ0FIWDs7U0FJYSxvQ0FBb0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdXRoVG9rZW4gfSBmcm9tICcuL2F1dGgtdG9rZW4nO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9jYWxTdG9yYWdlQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlIGltcGxlbWVudHMgQXV0aFRva2VuIHtcbiAgcHJpdmF0ZSB0b2tlbk5hbWUgPSAnYW5yZXN0OmF1dGhfdG9rZW4nO1xuXG4gIGdldCgpOiBzdHJpbmcge1xuICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuTmFtZSk7XG4gIH1cblxuICBzZXQodG9rZW46IHN0cmluZykge1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5OYW1lLCB0b2tlbik7XG4gIH1cblxuICByZW1vdmUoKSB7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlbk5hbWUpO1xuICB9XG5cbiAgaXNTZXQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5OYW1lKSAhPSBudWxsO1xuICB9XG59XG4iXX0=