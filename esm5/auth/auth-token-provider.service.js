/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
var /**
 * @abstract
 */
AuthTokenProviderService = /** @class */ (function () {
    function AuthTokenProviderService() {
    }
    return AuthTokenProviderService;
}());
/**
 * @abstract
 */
export { AuthTokenProviderService };
function AuthTokenProviderService_tsickle_Closure_declarations() {
    /**
     * @abstract
     * @return {?}
     */
    AuthTokenProviderService.prototype.get = function () { };
    /**
     * @abstract
     * @param {?} token
     * @return {?}
     */
    AuthTokenProviderService.prototype.set = function (token) { };
    /**
     * @abstract
     * @return {?}
     */
    AuthTokenProviderService.prototype.remove = function () { };
    /**
     * @abstract
     * @return {?}
     */
    AuthTokenProviderService.prototype.isSet = function () { };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC10b2tlbi1wcm92aWRlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJhdXRoL2F1dGgtdG9rZW4tcHJvdmlkZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBRUE7OztBQUFBOzs7bUNBRkE7SUFPQyxDQUFBOzs7O0FBTEQsb0NBS0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdXRoVG9rZW4gfSBmcm9tICcuL2F1dGgtdG9rZW4nO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlIGltcGxlbWVudHMgQXV0aFRva2Vue1xuICBhYnN0cmFjdCBnZXQoKTogc3RyaW5nO1xuICBhYnN0cmFjdCBzZXQodG9rZW46IHN0cmluZyk7XG4gIGFic3RyYWN0IHJlbW92ZSgpO1xuICBhYnN0cmFjdCBpc1NldCgpOiBib29sZWFuO1xufVxuIl19