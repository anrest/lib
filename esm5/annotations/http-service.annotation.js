/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
import * as pluralize from 'pluralize';
/**
 * @param {?} entity
 * @param {?=} path
 * @return {?}
 */
export function HttpService(entity, path) {
    return function (target) {
        var /** @type {?} */ meta = MetaService.getOrCreateForHttpService(target, entity);
        meta.path = path || '/' + pluralize.plural(entity.name).replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase();
        meta.entityMeta.service = target;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC1zZXJ2aWNlLmFubm90YXRpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImFubm90YXRpb25zL2h0dHAtc2VydmljZS5hbm5vdGF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ3RDLE9BQU8sS0FBSyxTQUFTLE1BQU0sV0FBVyxDQUFDOzs7Ozs7QUFFdkMsTUFBTSxzQkFBc0IsTUFBZ0IsRUFBRSxJQUFhO0lBRXpELE1BQU0sQ0FBQyxVQUFDLE1BQWdCO1FBQ3RCLHFCQUFNLElBQUksR0FBRyxXQUFXLENBQUMseUJBQXlCLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLEdBQUcsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdEgsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO0tBQ2xDLENBQUM7Q0FDSCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgKiBhcyBwbHVyYWxpemUgZnJvbSAncGx1cmFsaXplJztcblxuZXhwb3J0IGZ1bmN0aW9uIEh0dHBTZXJ2aWNlKGVudGl0eTogRnVuY3Rpb24sIHBhdGg/OiBzdHJpbmcpIHtcblxuICByZXR1cm4gKHRhcmdldDogRnVuY3Rpb24pID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JIdHRwU2VydmljZSh0YXJnZXQsIGVudGl0eSk7XG4gICAgbWV0YS5wYXRoID0gcGF0aCB8fCAnLycgKyBwbHVyYWxpemUucGx1cmFsKGVudGl0eS5uYW1lKS5yZXBsYWNlKC9cXC4/KFtBLVpdKS9nLCAnLSQxJykucmVwbGFjZSgvXi0vLCAnJykudG9Mb3dlckNhc2UoKTtcbiAgICBtZXRhLmVudGl0eU1ldGEuc2VydmljZSA9IHRhcmdldDtcbiAgfTtcbn1cbiJdfQ==