/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @record
 */
export function PropertyInfo() { }
function PropertyInfo_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    PropertyInfo.prototype.collection;
    /** @type {?|undefined} */
    PropertyInfo.prototype.excludeWhenSaving;
    /** @type {?|undefined} */
    PropertyInfo.prototype.type;
}
/**
 * @param {?=} info
 * @return {?}
 */
export function Property(info) {
    if (info === void 0) { info = {}; }
    return function (target, key) {
        var /** @type {?} */ meta = MetaService.getOrCreateForProperty(target, key, info.type);
        meta.isCollection = info.collection || false;
        meta.excludeWhenSaving = info.excludeWhenSaving || false;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvcGVydHkuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvcHJvcGVydHkuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRdEMsTUFBTSxtQkFBb0IsSUFBdUI7SUFBdkIscUJBQUEsRUFBQSxTQUF1QjtJQUUvQyxNQUFNLENBQUMsVUFBQyxNQUFXLEVBQUUsR0FBVztRQUM5QixxQkFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDLHNCQUFzQixDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxLQUFLLENBQUM7S0FDMUQsQ0FBQztDQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGludGVyZmFjZSBQcm9wZXJ0eUluZm8ge1xuICBjb2xsZWN0aW9uPzogYm9vbGVhbjtcbiAgZXhjbHVkZVdoZW5TYXZpbmc/OiBib29sZWFuO1xuICB0eXBlPzogYW55O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gUHJvcGVydHkgKGluZm86IFByb3BlcnR5SW5mbyA9IHt9KSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcpID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JQcm9wZXJ0eSh0YXJnZXQsIGtleSwgaW5mby50eXBlKTtcbiAgICBtZXRhLmlzQ29sbGVjdGlvbiA9IGluZm8uY29sbGVjdGlvbiB8fCBmYWxzZTtcbiAgICBtZXRhLmV4Y2x1ZGVXaGVuU2F2aW5nID0gaW5mby5leGNsdWRlV2hlblNhdmluZyB8fCBmYWxzZTtcbiAgfTtcbn1cbiJdfQ==