/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @return {?}
 */
export function Body() {
    return function (target, key, value) {
        if (key === void 0) { key = undefined; }
        if (value === void 0) { value = undefined; }
        MetaService.getOrCreateForEntity(target.constructor).body = function (object) {
            return (value ? value.value : function () { return this[key]; }).call(object);
        };
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9keS5hbm5vdGF0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJhbm5vdGF0aW9ucy9ib2R5LmFubm90YXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxTQUFTLENBQUM7Ozs7QUFFdEMsTUFBTTtJQUVKLE1BQU0sQ0FBQyxVQUFDLE1BQVcsRUFBRSxHQUFvQixFQUFFLEtBQXNCO1FBQTVDLG9CQUFBLEVBQUEsZUFBb0I7UUFBRSxzQkFBQSxFQUFBLGlCQUFzQjtRQUMvRCxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksR0FBRyxVQUFDLE1BQVc7WUFDdEUsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxjQUFjLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9FLENBQUM7S0FDSCxDQUFDO0NBQ0giLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gQm9keSgpIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IGFueSA9IHVuZGVmaW5lZCwgdmFsdWU6IGFueSA9IHVuZGVmaW5lZCkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3RvcikuYm9keSA9IChvYmplY3Q6IGFueSkgPT4ge1xuICAgICAgcmV0dXJuICh2YWx1ZSA/IHZhbHVlLnZhbHVlIDogZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpc1trZXldOyB9KS5jYWxsKG9iamVjdCk7XG4gICAgfTtcbiAgfTtcbn1cbiJdfQ==