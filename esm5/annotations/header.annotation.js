/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @param {?} headerName
 * @param {?=} append
 * @return {?}
 */
export function Header(headerName, append) {
    return function (target, key, value) {
        if (value === void 0) { value = undefined; }
        MetaService.getOrCreateForEntity(target.constructor).headers.push({
            name: headerName,
            value: value ? value.value : function () { return this[key]; },
            append: append || false,
            dynamic: true
        });
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmFubm90YXRpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImFubm90YXRpb25zL2hlYWRlci5hbm5vdGF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDOzs7Ozs7QUFFdEMsTUFBTSxpQkFBaUIsVUFBa0IsRUFBRSxNQUFnQjtJQUV6RCxNQUFNLENBQUMsVUFBQyxNQUFXLEVBQUUsR0FBVyxFQUFFLEtBQXNCO1FBQXRCLHNCQUFBLEVBQUEsaUJBQXNCO1FBQ3RELFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNoRSxJQUFJLEVBQUUsVUFBVTtZQUNoQixLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxjQUFjLE1BQU0sQ0FBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUMvRCxNQUFNLEVBQUUsTUFBTSxJQUFJLEtBQUs7WUFDdkIsT0FBTyxFQUFFLElBQUk7U0FDZCxDQUFDLENBQUM7S0FDSixDQUFDO0NBQ0giLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gSGVhZGVyKGhlYWRlck5hbWU6IHN0cmluZywgYXBwZW5kPzogYm9vbGVhbikge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nLCB2YWx1ZTogYW55ID0gdW5kZWZpbmVkKSA9PiB7XG4gICAgTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0LmNvbnN0cnVjdG9yKS5oZWFkZXJzLnB1c2goe1xuICAgICAgbmFtZTogaGVhZGVyTmFtZSxcbiAgICAgIHZhbHVlOiB2YWx1ZSA/IHZhbHVlLnZhbHVlIDogZnVuY3Rpb24gKCkgeyByZXR1cm4gIHRoaXNba2V5XTsgfSxcbiAgICAgIGFwcGVuZDogYXBwZW5kIHx8IGZhbHNlLFxuICAgICAgZHluYW1pYzogdHJ1ZVxuICAgIH0pO1xuICB9O1xufVxuIl19