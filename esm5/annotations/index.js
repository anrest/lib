/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
export { Resource } from './resource.annotation';
export { Property } from './property.annotation';
export { HttpService } from './http-service.annotation';
export { Header } from './header.annotation';
export { Headers } from './headers.annotation';
export { Body } from './body.annotation';
export { Subresource } from './subresource.annotation';
export { Id } from './id.annotation';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImFubm90YXRpb25zL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSx5QkFBYyx1QkFBdUIsQ0FBQztBQUN0Qyx5QkFBYyx1QkFBdUIsQ0FBQztBQUN0Qyw0QkFBYywyQkFBMkIsQ0FBQztBQUMxQyx1QkFBYyxxQkFBcUIsQ0FBQztBQUNwQyx3QkFBYyxzQkFBc0IsQ0FBQztBQUNyQyxxQkFBYyxtQkFBbUIsQ0FBQztBQUNsQyw0QkFBYywwQkFBMEIsQ0FBQztBQUN6QyxtQkFBYyxpQkFBaUIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGZyb20gJy4vcmVzb3VyY2UuYW5ub3RhdGlvbic7XG5leHBvcnQgKiBmcm9tICcuL3Byb3BlcnR5LmFubm90YXRpb24nO1xuZXhwb3J0ICogZnJvbSAnLi9odHRwLXNlcnZpY2UuYW5ub3RhdGlvbic7XG5leHBvcnQgKiBmcm9tICcuL2hlYWRlci5hbm5vdGF0aW9uJztcbmV4cG9ydCAqIGZyb20gJy4vaGVhZGVycy5hbm5vdGF0aW9uJztcbmV4cG9ydCAqIGZyb20gJy4vYm9keS5hbm5vdGF0aW9uJztcbmV4cG9ydCAqIGZyb20gJy4vc3VicmVzb3VyY2UuYW5ub3RhdGlvbic7XG5leHBvcnQgKiBmcm9tICcuL2lkLmFubm90YXRpb24nO1xuIl19