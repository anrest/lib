/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @param {?} type
 * @param {?=} path
 * @return {?}
 */
export function Subresource(type, path) {
    return function (target, key, value) {
        MetaService.getOrCreateForEntity(target.constructor).subresources[key] = {
            meta: MetaService.getOrCreateForEntity(type),
            path: path || '/' + key.replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase()
        };
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VicmVzb3VyY2UuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvc3VicmVzb3VyY2UuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7Ozs7O0FBRXRDLE1BQU0sc0JBQXVCLElBQWMsRUFBRSxJQUFhO0lBRXhELE1BQU0sQ0FBQyxVQUFDLE1BQVcsRUFBRSxHQUFXLEVBQUUsS0FBVTtRQUMxQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRztZQUN2RSxJQUFJLEVBQUUsV0FBVyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQztZQUM1QyxJQUFJLEVBQUUsSUFBSSxJQUFJLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRTtTQUN0RixDQUFDO0tBQ0gsQ0FBQztDQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGZ1bmN0aW9uIFN1YnJlc291cmNlICh0eXBlOiBGdW5jdGlvbiwgcGF0aD86IHN0cmluZykge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nLCB2YWx1ZTogYW55KSA9PiB7XG4gICAgTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0LmNvbnN0cnVjdG9yKS5zdWJyZXNvdXJjZXNba2V5XSA9IHtcbiAgICAgIG1ldGE6IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHR5cGUpLFxuICAgICAgcGF0aDogcGF0aCB8fCAnLycgKyBrZXkucmVwbGFjZSgvXFwuPyhbQS1aXSkvZywgJy0kMScpLnJlcGxhY2UoL14tLywgJycpLnRvTG93ZXJDYXNlKClcbiAgICB9O1xuICB9O1xufVxuIl19