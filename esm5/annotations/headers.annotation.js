/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MetaService } from '../meta';
/**
 * @param {?} headers
 * @return {?}
 */
export function Headers(headers) {
    return function (target) {
        var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        var _loop_1 = function (header) {
            meta.headers.push({
                name: header.name,
                value: function () { return header.value; },
                append: header.append || false,
                dynamic: false
            });
        };
        try {
            for (var headers_1 = tslib_1.__values(headers), headers_1_1 = headers_1.next(); !headers_1_1.done; headers_1_1 = headers_1.next()) {
                var header = headers_1_1.value;
                _loop_1(header);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (headers_1_1 && !headers_1_1.done && (_a = headers_1.return)) _a.call(headers_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        var e_1, _a;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVycy5hbm5vdGF0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJhbm5vdGF0aW9ucy9oZWFkZXJzLmFubm90YXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDOzs7OztBQUV0QyxNQUFNLGtCQUFtQixPQUEyRDtJQUVsRixNQUFNLENBQUMsVUFBQyxNQUFXO1FBQ2pCLHFCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUM7Z0NBQzNDLE1BQU07WUFDZixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDaEIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJO2dCQUNqQixLQUFLLEVBQUUsY0FBTSxPQUFBLE1BQU0sQ0FBQyxLQUFLLEVBQVosQ0FBWTtnQkFDekIsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLElBQUksS0FBSztnQkFDOUIsT0FBTyxFQUFFLEtBQUs7YUFDZixDQUFDLENBQUM7OztZQU5MLEdBQUcsQ0FBQyxDQUFpQixJQUFBLFlBQUEsaUJBQUEsT0FBTyxDQUFBLGdDQUFBO2dCQUF2QixJQUFNLE1BQU0sb0JBQUE7d0JBQU4sTUFBTTthQU9oQjs7Ozs7Ozs7OztLQUNGLENBQUM7Q0FDSCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBIZWFkZXJzIChoZWFkZXJzOiB7IG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZywgYXBwZW5kPzogYm9vbGVhbn1bXSkge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnkpID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0KTtcbiAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiBoZWFkZXJzKSB7XG4gICAgICBtZXRhLmhlYWRlcnMucHVzaCh7XG4gICAgICAgIG5hbWU6IGhlYWRlci5uYW1lLFxuICAgICAgICB2YWx1ZTogKCkgPT4gaGVhZGVyLnZhbHVlLFxuICAgICAgICBhcHBlbmQ6IGhlYWRlci5hcHBlbmQgfHwgZmFsc2UsXG4gICAgICAgIGR5bmFtaWM6IGZhbHNlXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG59XG4iXX0=