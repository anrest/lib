/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @record
 */
export function ResourceInfo() { }
function ResourceInfo_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    ResourceInfo.prototype.path;
    /** @type {?|undefined} */
    ResourceInfo.prototype.name;
    /** @type {?|undefined} */
    ResourceInfo.prototype.cache;
}
/**
 * @param {?=} info
 * @return {?}
 */
export function Resource(info) {
    if (info === void 0) { info = {}; }
    return function (target) {
        var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        meta.name = info.name || target.name;
        meta.cache = info.cache;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvcmVzb3VyY2UuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRdEMsTUFBTSxtQkFBb0IsSUFBdUI7SUFBdkIscUJBQUEsRUFBQSxTQUF1QjtJQUUvQyxNQUFNLENBQUMsVUFBQyxNQUFnQjtRQUN0QixxQkFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztLQUN6QixDQUFDO0NBQ0giLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFJlc291cmNlSW5mbyB7XG4gIHBhdGg/OiBzdHJpbmc7XG4gIG5hbWU/OiBzdHJpbmc7XG4gIGNhY2hlPzogRnVuY3Rpb247XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBSZXNvdXJjZSAoaW5mbzogUmVzb3VyY2VJbmZvID0ge30pIHtcblxuICByZXR1cm4gKHRhcmdldDogRnVuY3Rpb24pID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0KTtcbiAgICBtZXRhLm5hbWUgPSBpbmZvLm5hbWUgfHwgdGFyZ2V0Lm5hbWU7XG4gICAgbWV0YS5jYWNoZSA9IGluZm8uY2FjaGU7XG4gIH07XG59XG4iXX0=