/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @return {?}
 */
export function Id() {
    return function (target, key) {
        var /** @type {?} */ meta = MetaService.getOrCreateForEntity(target.constructor);
        meta.id = key;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWQuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvaWQuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7OztBQUV0QyxNQUFNO0lBRUosTUFBTSxDQUFDLFVBQUMsTUFBVyxFQUFFLEdBQVc7UUFDOUIscUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUM7S0FDZixDQUFDO0NBQ0giLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gSWQgKCkge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nKSA9PiB7XG4gICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3Rvcik7XG4gICAgbWV0YS5pZCA9IGtleTtcbiAgfTtcbn1cbiJdfQ==