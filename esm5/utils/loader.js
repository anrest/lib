/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BehaviorSubject, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, mergeMap, share, shareReplay, tap } from 'rxjs/operators';
/**
 * @abstract
 */
var /**
 * @abstract
 */
Loader = /** @class */ (function () {
    function Loader(service, waitFor, replay) {
        if (waitFor === void 0) { waitFor = 0; }
        if (replay === void 0) { replay = 0; }
        var _this = this;
        this.service = service;
        this.fields = {};
        this.loading = false;
        this.observable = combineLatest(this.getSubjects())
            .pipe(replay ? shareReplay(replay) : share(), debounceTime(waitFor), distinctUntilChanged(function (d1, d2) { return _this.checkDistinct(d1, d2); }), map(function (values) { return _this.mapParams(values); }), mergeMap(function (data) {
            _this.loading = true;
            return _this.getServiceObservable(data).pipe(tap(function (v) {
                _this.lastData = v;
                _this.loading = false;
            }));
        }));
    }
    /**
     * @param {?} name
     * @return {?}
     */
    Loader.prototype.field = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.fields[name];
    };
    Object.defineProperty(Loader.prototype, "data", {
        get: /**
         * @return {?}
         */
        function () {
            return this.observable;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Loader.prototype, "isLoading", {
        get: /**
         * @return {?}
         */
        function () {
            return this.loading;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} __0
     * @return {?}
     */
    Loader.prototype.getServiceObservable = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = tslib_1.__read(_a, 1), data = _b[0];
        return this.service.getList(data);
    };
    /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    Loader.prototype.checkDistinct = /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    function (d1, d2) {
        var /** @type {?} */ offset = d1.length - this.getAvailableFields().length;
        for (var /** @type {?} */ i = d1.length; i >= 0; i--) {
            if ((i < offset && d2[i]) || (i >= offset && d1[i] !== d2[i])) {
                if (i >= offset) {
                    d2.fill(undefined, 0, offset);
                }
                return false;
            }
        }
        return true;
    };
    /**
     * @param {?} values
     * @return {?}
     */
    Loader.prototype.mapParams = /**
     * @param {?} values
     * @return {?}
     */
    function (values) {
        var /** @type {?} */ fields = this.getAvailableFields();
        var /** @type {?} */ offset = values.length - fields.length;
        var /** @type {?} */ data = {};
        values.slice(offset).forEach(function (v, i) {
            if (v !== undefined && v !== null && v !== '') {
                if (typeof v === 'boolean') {
                    v = v ? 'true' : 'false';
                }
                data[fields[i]] = String(v);
            }
        });
        return values.slice(0, offset).concat([data]);
    };
    /**
     * @return {?}
     */
    Loader.prototype.getSubjects = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return this.getAvailableFields().map(function (name) {
            return _this.fields[name] = new BehaviorSubject(undefined);
        });
    };
    return Loader;
}());
/**
 * @abstract
 */
export { Loader };
function Loader_tsickle_Closure_declarations() {
    /** @type {?} */
    Loader.prototype.fields;
    /** @type {?} */
    Loader.prototype.observable;
    /** @type {?} */
    Loader.prototype.loading;
    /** @type {?} */
    Loader.prototype.lastData;
    /** @type {?} */
    Loader.prototype.service;
    /**
     * @abstract
     * @return {?}
     */
    Loader.prototype.getAvailableFields = function () { };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJ1dGlscy9sb2FkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLG9CQUFvQixFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUk1Rzs7O0FBQUE7SUFTRSxnQkFBc0IsT0FBbUIsRUFBRSxPQUFXLEVBQUUsTUFBVTtRQUF2Qix3QkFBQSxFQUFBLFdBQVc7UUFBRSx1QkFBQSxFQUFBLFVBQVU7UUFBbEUsaUJBaUJDO1FBakJxQixZQUFPLEdBQVAsT0FBTyxDQUFZO3NCQVBmLEVBQUU7dUJBRVYsS0FBSztRQU1yQixJQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDaEQsSUFBSSxDQUNILE1BQU0sQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFDdEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxFQUNyQixvQkFBb0IsQ0FBQyxVQUFDLEVBQVMsRUFBRSxFQUFTLElBQUssT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBMUIsQ0FBMEIsQ0FBQyxFQUMxRSxHQUFHLENBQUMsVUFBQyxNQUFhLElBQUssT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUF0QixDQUFzQixDQUFDLEVBQzlDLFFBQVEsQ0FBQyxVQUFDLElBQUk7WUFDWixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNwQixNQUFNLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDekMsR0FBRyxDQUFDLFVBQUMsQ0FBa0I7Z0JBQ3JCLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO2dCQUNsQixLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUN0QixDQUFDLENBQ0gsQ0FBQztTQUNILENBQUMsQ0FDSCxDQUFDO0tBQ0w7Ozs7O0lBRUQsc0JBQUs7Ozs7SUFBTCxVQUFNLElBQUk7UUFDUixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUMxQjtJQUVELHNCQUFJLHdCQUFJOzs7O1FBQVI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztTQUN4Qjs7O09BQUE7SUFFRCxzQkFBSSw2QkFBUzs7OztRQUFiO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7U0FDckI7OztPQUFBOzs7OztJQUVTLHFDQUFvQjs7OztJQUE5QixVQUErQixFQUFhO1lBQWIsMEJBQWEsRUFBWixZQUFJO1FBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNuQzs7Ozs7O0lBRVMsOEJBQWE7Ozs7O0lBQXZCLFVBQXdCLEVBQVMsRUFBRSxFQUFTO1FBQzFDLHFCQUFNLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU0sQ0FBQztRQUM1RCxHQUFHLENBQUMsQ0FBQyxxQkFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDcEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLE1BQU0sSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5RCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDaEIsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2lCQUMvQjtnQkFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO2FBQ2Q7U0FDRjtRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7S0FDYjs7Ozs7SUFFUywwQkFBUzs7OztJQUFuQixVQUFvQixNQUFhO1FBQy9CLHFCQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUN6QyxxQkFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzdDLHFCQUFNLElBQUksR0FBRyxFQUFFLENBQUM7UUFDaEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQztZQUNoQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssU0FBUyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQzNCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO2lCQUMxQjtnQkFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzdCO1NBQ0YsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDL0M7Ozs7SUFFUyw0QkFBVzs7O0lBQXJCO1FBQUEsaUJBSUM7UUFIQyxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSTtZQUN4QyxNQUFNLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLGVBQWUsQ0FBTSxTQUFTLENBQUMsQ0FBQztTQUNoRSxDQUFDLENBQUM7S0FDSjtpQkFqRkg7SUFrRkMsQ0FBQTs7OztBQTdFRCxrQkE2RUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIGNvbWJpbmVMYXRlc3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGRlYm91bmNlVGltZSwgZGlzdGluY3RVbnRpbENoYW5nZWQsIG1hcCwgbWVyZ2VNYXAsIHNoYXJlLCBzaGFyZVJlcGxheSwgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXBpU2VydmljZSB9IGZyb20gJy4uL2NvcmUnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4uL2NvcmUvY29sbGVjdGlvbic7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBMb2FkZXIge1xuXG4gIHByaXZhdGUgcmVhZG9ubHkgZmllbGRzID0ge307XG4gIHByaXZhdGUgcmVhZG9ubHkgb2JzZXJ2YWJsZTogT2JzZXJ2YWJsZTxhbnk+O1xuICBwcml2YXRlIGxvYWRpbmcgPSBmYWxzZTtcbiAgcHJvdGVjdGVkIGxhc3REYXRhOiBDb2xsZWN0aW9uPGFueT47XG5cbiAgYWJzdHJhY3QgZ2V0QXZhaWxhYmxlRmllbGRzKCk6IHN0cmluZ1tdO1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBzZXJ2aWNlOiBBcGlTZXJ2aWNlLCB3YWl0Rm9yID0gMCwgcmVwbGF5ID0gMCkge1xuICAgIHRoaXMub2JzZXJ2YWJsZSA9IGNvbWJpbmVMYXRlc3QodGhpcy5nZXRTdWJqZWN0cygpKVxuICAgICAgLnBpcGUoXG4gICAgICAgIHJlcGxheSA/IHNoYXJlUmVwbGF5KHJlcGxheSkgOiBzaGFyZSgpLFxuICAgICAgICBkZWJvdW5jZVRpbWUod2FpdEZvciksXG4gICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKChkMTogYW55W10sIGQyOiBhbnlbXSkgPT4gdGhpcy5jaGVja0Rpc3RpbmN0KGQxLCBkMikpLFxuICAgICAgICBtYXAoKHZhbHVlczogYW55W10pID0+IHRoaXMubWFwUGFyYW1zKHZhbHVlcykpLFxuICAgICAgICBtZXJnZU1hcCgoZGF0YSkgPT4ge1xuICAgICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U2VydmljZU9ic2VydmFibGUoZGF0YSkucGlwZShcbiAgICAgICAgICAgIHRhcCgodjogQ29sbGVjdGlvbjxhbnk+KSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMubGFzdERhdGEgPSB2O1xuICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgKTtcbiAgICAgICAgfSksXG4gICAgICApO1xuICB9XG5cbiAgZmllbGQobmFtZSk6IEJlaGF2aW9yU3ViamVjdDxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZHNbbmFtZV07XG4gIH1cblxuICBnZXQgZGF0YSgpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLm9ic2VydmFibGU7XG4gIH1cblxuICBnZXQgaXNMb2FkaW5nKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmxvYWRpbmc7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U2VydmljZU9ic2VydmFibGUoW2RhdGFdOiBhbnlbXSkge1xuICAgIHJldHVybiB0aGlzLnNlcnZpY2UuZ2V0TGlzdChkYXRhKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBjaGVja0Rpc3RpbmN0KGQxOiBhbnlbXSwgZDI6IGFueVtdKSB7XG4gICAgY29uc3Qgb2Zmc2V0ID0gZDEubGVuZ3RoIC0gdGhpcy5nZXRBdmFpbGFibGVGaWVsZHMoKS5sZW5ndGg7XG4gICAgZm9yIChsZXQgaSA9IGQxLmxlbmd0aDsgaSA+PSAwOyBpLS0pIHtcbiAgICAgIGlmICgoaSA8IG9mZnNldCAmJiBkMltpXSkgfHwgKGkgPj0gb2Zmc2V0ICYmIGQxW2ldICE9PSBkMltpXSkpIHtcbiAgICAgICAgaWYgKGkgPj0gb2Zmc2V0KSB7XG4gICAgICAgICAgZDIuZmlsbCh1bmRlZmluZWQsIDAsIG9mZnNldCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHByb3RlY3RlZCBtYXBQYXJhbXModmFsdWVzOiBhbnlbXSkge1xuICAgIGNvbnN0IGZpZWxkcyA9IHRoaXMuZ2V0QXZhaWxhYmxlRmllbGRzKCk7XG4gICAgY29uc3Qgb2Zmc2V0ID0gdmFsdWVzLmxlbmd0aCAtIGZpZWxkcy5sZW5ndGg7XG4gICAgY29uc3QgZGF0YSA9IHt9O1xuICAgIHZhbHVlcy5zbGljZShvZmZzZXQpLmZvckVhY2goKHYsIGkpID0+IHtcbiAgICAgIGlmICh2ICE9PSB1bmRlZmluZWQgJiYgdiAhPT0gbnVsbCAmJiB2ICE9PSAnJykge1xuICAgICAgICBpZiAodHlwZW9mIHYgPT09ICdib29sZWFuJykge1xuICAgICAgICAgIHYgPSB2ID8gJ3RydWUnIDogJ2ZhbHNlJztcbiAgICAgICAgfVxuICAgICAgICBkYXRhW2ZpZWxkc1tpXV0gPSBTdHJpbmcodik7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIHZhbHVlcy5zbGljZSgwLCBvZmZzZXQpLmNvbmNhdChbZGF0YV0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFN1YmplY3RzKCk6IEJlaGF2aW9yU3ViamVjdDxhbnk+W10ge1xuICAgIHJldHVybiB0aGlzLmdldEF2YWlsYWJsZUZpZWxkcygpLm1hcCgobmFtZSkgPT4ge1xuICAgICAgcmV0dXJuIHRoaXMuZmllbGRzW25hbWVdID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KHVuZGVmaW5lZCk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==