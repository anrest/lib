/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BehaviorSubject } from 'rxjs';
import { Loader } from './loader';
/**
 * @abstract
 */
var /**
 * @abstract
 */
PageLoader = /** @class */ (function (_super) {
    tslib_1.__extends(PageLoader, _super);
    function PageLoader() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @return {?}
     */
    PageLoader.prototype.first = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasFirst()) {
            return;
        }
        this.pageSubject.next(this.lastData.firstPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.previous = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasPrevious()) {
            return;
        }
        this.pageSubject.next(this.lastData.previousPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.next = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasNext()) {
            return;
        }
        this.pageSubject.next(this.lastData.nextPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.last = /**
     * @return {?}
     */
    function () {
        if (!this.lastData || !this.lastData.hasLast()) {
            return;
        }
        this.pageSubject.next(this.lastData.lastPath());
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasFirst = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasFirst();
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasPrevious = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasPrevious();
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasNext = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasNext();
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.hasLast = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasLast();
    };
    /**
     * @param {?} __0
     * @return {?}
     */
    PageLoader.prototype.getServiceObservable = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = tslib_1.__read(_a, 2), page = _b[0], data = _b[1];
        return page ? this.service.doGet(page, data) : _super.prototype.getServiceObservable.call(this, [data]);
    };
    /**
     * @return {?}
     */
    PageLoader.prototype.getSubjects = /**
     * @return {?}
     */
    function () {
        this.pageSubject = new BehaviorSubject(undefined);
        var /** @type {?} */ subjects = _super.prototype.getSubjects.call(this);
        subjects.unshift(this.pageSubject);
        return subjects;
    };
    return PageLoader;
}(Loader));
/**
 * @abstract
 */
export { PageLoader };
function PageLoader_tsickle_Closure_declarations() {
    /** @type {?} */
    PageLoader.prototype.pageSubject;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS1sb2FkZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbInV0aWxzL3BhZ2UtbG9hZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGVBQWUsRUFBNkIsTUFBTSxNQUFNLENBQUM7QUFJbEUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLFVBQVUsQ0FBQzs7OztBQUVsQzs7O0FBQUE7SUFBeUMsc0NBQU07Ozs7Ozs7SUFJN0MsMEJBQUs7OztJQUFMO1FBQ0UsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDaEQsTUFBTSxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7S0FDbEQ7Ozs7SUFFRCw2QkFBUTs7O0lBQVI7UUFDRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNuRCxNQUFNLENBQUM7U0FDUjtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztLQUNyRDs7OztJQUVELHlCQUFJOzs7SUFBSjtRQUNFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQy9DLE1BQU0sQ0FBQztTQUNSO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0tBQ2pEOzs7O0lBRUQseUJBQUk7OztJQUFKO1FBQ0UsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDL0MsTUFBTSxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7S0FDakQ7Ozs7SUFFRCw2QkFBUTs7O0lBQVI7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO0tBQ2xEOzs7O0lBRUQsZ0NBQVc7OztJQUFYO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztLQUNyRDs7OztJQUVELDRCQUFPOzs7SUFBUDtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDakQ7Ozs7SUFFRCw0QkFBTzs7O0lBQVA7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ2pEOzs7OztJQUVTLHlDQUFvQjs7OztJQUE5QixVQUErQixFQUFtQjtZQUFuQiwwQkFBbUIsRUFBbEIsWUFBSSxFQUFFLFlBQUk7UUFDeEMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBTSxvQkFBb0IsWUFBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDbkY7Ozs7SUFFUyxnQ0FBVzs7O0lBQXJCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLGVBQWUsQ0FBUyxTQUFTLENBQUMsQ0FBQztRQUMxRCxxQkFBTSxRQUFRLEdBQUcsaUJBQU0sV0FBVyxXQUFFLENBQUM7UUFDckMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkMsTUFBTSxDQUFDLFFBQVEsQ0FBQztLQUNqQjtxQkEvREg7RUFNeUMsTUFBTSxFQTBEOUMsQ0FBQTs7OztBQTFERCxzQkEwREMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIGNvbWJpbmVMYXRlc3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGRlYm91bmNlVGltZSwgZGlzdGluY3RVbnRpbENoYW5nZWQsIG1hcCwgbWVyZ2VNYXAsIHNoYXJlLCBzaGFyZVJlcGxheSwgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXBpU2VydmljZSB9IGZyb20gJy4uL2NvcmUnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4uL2NvcmUvY29sbGVjdGlvbic7XG5pbXBvcnQgeyBMb2FkZXIgfSBmcm9tICcuL2xvYWRlcic7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBQYWdlTG9hZGVyIGV4dGVuZHMgTG9hZGVyIHtcblxuICBwcml2YXRlIHBhZ2VTdWJqZWN0O1xuXG4gIGZpcnN0KCkge1xuICAgIGlmICghdGhpcy5sYXN0RGF0YSB8fCAhdGhpcy5sYXN0RGF0YS5oYXNGaXJzdCgpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLmZpcnN0UGF0aCgpKTtcbiAgfVxuXG4gIHByZXZpb3VzKCkge1xuICAgIGlmICghdGhpcy5sYXN0RGF0YSB8fCAhdGhpcy5sYXN0RGF0YS5oYXNQcmV2aW91cygpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLnByZXZpb3VzUGF0aCgpKTtcbiAgfVxuXG4gIG5leHQoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc05leHQoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnBhZ2VTdWJqZWN0Lm5leHQodGhpcy5sYXN0RGF0YS5uZXh0UGF0aCgpKTtcbiAgfVxuXG4gIGxhc3QoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc0xhc3QoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnBhZ2VTdWJqZWN0Lm5leHQodGhpcy5sYXN0RGF0YS5sYXN0UGF0aCgpKTtcbiAgfVxuXG4gIGhhc0ZpcnN0KCkge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzRmlyc3QoKTtcbiAgfVxuXG4gIGhhc1ByZXZpb3VzKCkge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzUHJldmlvdXMoKTtcbiAgfVxuXG4gIGhhc05leHQoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNOZXh0KCk7XG4gIH1cblxuICBoYXNMYXN0KCkge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzTGFzdCgpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFNlcnZpY2VPYnNlcnZhYmxlKFtwYWdlLCBkYXRhXTogYW55W10pIHtcbiAgICByZXR1cm4gcGFnZSA/IHRoaXMuc2VydmljZS5kb0dldChwYWdlLCBkYXRhKSA6IHN1cGVyLmdldFNlcnZpY2VPYnNlcnZhYmxlKFtkYXRhXSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U3ViamVjdHMoKTogQmVoYXZpb3JTdWJqZWN0PGFueT5bXSB7XG4gICAgdGhpcy5wYWdlU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPih1bmRlZmluZWQpO1xuICAgIGNvbnN0IHN1YmplY3RzID0gc3VwZXIuZ2V0U3ViamVjdHMoKTtcbiAgICBzdWJqZWN0cy51bnNoaWZ0KHRoaXMucGFnZVN1YmplY3QpO1xuICAgIHJldHVybiBzdWJqZWN0cztcbiAgfVxufVxuIl19