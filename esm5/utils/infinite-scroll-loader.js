/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BehaviorSubject } from 'rxjs';
import { Loader } from './loader';
/**
 * @abstract
 */
var /**
 * @abstract
 */
InfiniteScrollLoader = /** @class */ (function (_super) {
    tslib_1.__extends(InfiniteScrollLoader, _super);
    function InfiniteScrollLoader() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @return {?}
     */
    InfiniteScrollLoader.prototype.loadMore = /**
     * @return {?}
     */
    function () {
        this.loadMoreSubject.next(true);
    };
    /**
     * @return {?}
     */
    InfiniteScrollLoader.prototype.hasMore = /**
     * @return {?}
     */
    function () {
        return this.lastData && this.lastData.hasMore();
    };
    /**
     * @param {?} __0
     * @return {?}
     */
    InfiniteScrollLoader.prototype.getServiceObservable = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var _b = tslib_1.__read(_a, 2), loadMore = _b[0], data = _b[1];
        return loadMore ? this.lastData.loadMore() : _super.prototype.getServiceObservable.call(this, [data]);
    };
    /**
     * @return {?}
     */
    InfiniteScrollLoader.prototype.getSubjects = /**
     * @return {?}
     */
    function () {
        this.loadMoreSubject = new BehaviorSubject(false);
        var /** @type {?} */ subjects = _super.prototype.getSubjects.call(this);
        subjects.unshift(this.loadMoreSubject);
        return subjects;
    };
    return InfiniteScrollLoader;
}(Loader));
/**
 * @abstract
 */
export { InfiniteScrollLoader };
function InfiniteScrollLoader_tsickle_Closure_declarations() {
    /** @type {?} */
    InfiniteScrollLoader.prototype.loadMoreSubject;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5maW5pdGUtc2Nyb2xsLWxvYWRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsidXRpbHMvaW5maW5pdGUtc2Nyb2xsLWxvYWRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQWMsTUFBTSxNQUFNLENBQUM7QUFDbkQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLFVBQVUsQ0FBQzs7OztBQUVsQzs7O0FBQUE7SUFBbUQsZ0RBQU07Ozs7Ozs7SUFJdkQsdUNBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDakM7Ozs7SUFFRCxzQ0FBTzs7O0lBQVA7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ2pEOzs7OztJQUVTLG1EQUFvQjs7OztJQUE5QixVQUErQixFQUF1QjtZQUF2QiwwQkFBdUIsRUFBdEIsZ0JBQVEsRUFBRSxZQUFJO1FBQzVDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLGlCQUFNLG9CQUFvQixZQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUNqRjs7OztJQUVTLDBDQUFXOzs7SUFBckI7UUFDRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQzNELHFCQUFNLFFBQVEsR0FBRyxpQkFBTSxXQUFXLFdBQUUsQ0FBQztRQUNyQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN2QyxNQUFNLENBQUMsUUFBUSxDQUFDO0tBQ2pCOytCQXhCSDtFQUdtRCxNQUFNLEVBc0J4RCxDQUFBOzs7O0FBdEJELGdDQXNCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgTG9hZGVyIH0gZnJvbSAnLi9sb2FkZXInO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgSW5maW5pdGVTY3JvbGxMb2FkZXIgZXh0ZW5kcyBMb2FkZXIge1xuXG4gIHByaXZhdGUgbG9hZE1vcmVTdWJqZWN0O1xuXG4gIGxvYWRNb3JlKCkge1xuICAgIHRoaXMubG9hZE1vcmVTdWJqZWN0Lm5leHQodHJ1ZSk7XG4gIH1cblxuICBoYXNNb3JlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzTW9yZSgpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFNlcnZpY2VPYnNlcnZhYmxlKFtsb2FkTW9yZSwgZGF0YV06IGFueVtdKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gbG9hZE1vcmUgPyB0aGlzLmxhc3REYXRhLmxvYWRNb3JlKCkgOiBzdXBlci5nZXRTZXJ2aWNlT2JzZXJ2YWJsZShbZGF0YV0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFN1YmplY3RzKCk6IEJlaGF2aW9yU3ViamVjdDxhbnk+W10ge1xuICAgIHRoaXMubG9hZE1vcmVTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gICAgY29uc3Qgc3ViamVjdHMgPSBzdXBlci5nZXRTdWJqZWN0cygpO1xuICAgIHN1YmplY3RzLnVuc2hpZnQodGhpcy5sb2FkTW9yZVN1YmplY3QpO1xuICAgIHJldHVybiBzdWJqZWN0cztcbiAgfVxufVxuIl19