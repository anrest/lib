/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ResponseCache } from './response-cache';
import { Inject, Injectable } from '@angular/core';
import { ObjectCollector } from '../core/object-collector';
import { ApiProcessor } from '../core/api.processor';
import { AnRestConfig } from '../core/config';
import { CollectionInfo, DataInfoService, EntityInfo } from '../core';
import { MetaService } from '../meta';
import { ResponseNode } from '../response';
var LocalStorageResponseCache = /** @class */ (function (_super) {
    tslib_1.__extends(LocalStorageResponseCache, _super);
    function LocalStorageResponseCache(collector, processor, info, config) {
        var _this = _super.call(this) || this;
        _this.collector = collector;
        _this.processor = processor;
        _this.info = info;
        _this.config = config;
        _this.tokenPrefix = 'anrest:cache:';
        var /** @type {?} */ timestampPrefix = _this.tokenPrefix + 'timestamp:';
        Object.keys(localStorage).filter(function (e) { return e.indexOf(timestampPrefix) === 0; }).forEach(function (k) {
            var /** @type {?} */ id = k.substring(timestampPrefix.length);
            var /** @type {?} */ timestamp = _this.getTimestamp(id);
            if (!timestamp || timestamp + _this.config.cacheTTL < Date.now()) {
                _this.remove(id);
            }
        });
        return _this;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        if (this.has(id)) {
            return this.processor.process(this.getNode(id), 'GET');
        }
    };
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    LocalStorageResponseCache.prototype.add = /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    function (id, data) {
        this.doAdd(id, data, true);
    };
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    LocalStorageResponseCache.prototype.addAlias = /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    function (id, alias) {
        localStorage.setItem(this.tokenPrefix + 'alias:' + alias, id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        var /** @type {?} */ complete = JSON.parse(localStorage.getItem(this.tokenPrefix + 'complete:' + id));
        var /** @type {?} */ timestamp = this.getTimestamp(id);
        return !!timestamp && complete && timestamp + this.config.cacheTTL >= Date.now();
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.remove = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        localStorage.removeItem(this.tokenPrefix + id);
        localStorage.removeItem(this.tokenPrefix + 'type:' + id);
        localStorage.removeItem(this.tokenPrefix + 'collection:' + id);
        localStorage.removeItem(this.tokenPrefix + 'complete:' + id);
        localStorage.removeItem(this.tokenPrefix + 'references:' + id);
        this.removeTimestamp(id);
    };
    /**
     * @return {?}
     */
    LocalStorageResponseCache.prototype.clear = /**
     * @return {?}
     */
    function () {
        var _this = this;
        Object.keys(localStorage).filter(function (e) { return e.indexOf(_this.tokenPrefix) === 0; }).forEach(function (k) {
            localStorage.removeItem(k);
        });
    };
    /**
     * @param {?} id
     * @param {?} data
     * @param {?} complete
     * @return {?}
     */
    LocalStorageResponseCache.prototype.doAdd = /**
     * @param {?} id
     * @param {?} data
     * @param {?} complete
     * @return {?}
     */
    function (id, data, complete) {
        var /** @type {?} */ info = this.info.get(data);
        var /** @type {?} */ values;
        if (info instanceof CollectionInfo) {
            values = [];
            try {
                for (var data_1 = tslib_1.__values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                    var item = data_1_1.value;
                    var /** @type {?} */ itemPath = this.info.get(item).path;
                    values.push(itemPath);
                    this.doAdd(itemPath, item, false);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (data_1_1 && !data_1_1.done && (_a = data_1.return)) _a.call(data_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            localStorage.setItem(this.tokenPrefix + 'collection:' + id, JSON.stringify(info));
        }
        else {
            var /** @type {?} */ references = {};
            values = {};
            for (var /** @type {?} */ key in data) {
                var /** @type {?} */ itemInfo = void 0;
                if (typeof data[key] === 'object') {
                    itemInfo = this.info.get(data[key]);
                }
                if (itemInfo) {
                    this.doAdd(itemInfo.path, data[key], false);
                    references[key] = itemInfo.path;
                }
                else {
                    values[key] = data[key];
                }
            }
            localStorage.setItem(this.tokenPrefix + 'type:' + id, MetaService.get(data.constructor).name);
            localStorage.setItem(this.tokenPrefix + 'references:' + id, JSON.stringify(references));
        }
        localStorage.setItem(this.tokenPrefix + 'complete:' + id, JSON.stringify(complete));
        localStorage.setItem(this.tokenPrefix + id, JSON.stringify(values));
        this.setTimestamp(id);
        var e_1, _a;
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.getNode = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + id));
        var /** @type {?} */ collectionInfo = this.getCollectionInfo(id);
        var /** @type {?} */ response = new ResponseNode();
        if (collectionInfo) {
            response.collection = true;
            response.info = collectionInfo;
            response.meta = MetaService.getByName(collectionInfo.type);
            response.data = [];
            try {
                for (var data_2 = tslib_1.__values(data), data_2_1 = data_2.next(); !data_2_1.done; data_2_1 = data_2.next()) {
                    var itemId = data_2_1.value;
                    response.data.push(this.getNode(itemId));
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (data_2_1 && !data_2_1.done && (_a = data_2.return)) _a.call(data_2);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        else {
            response.collection = false;
            response.info = new EntityInfo(id);
            response.meta = MetaService.getByName(localStorage.getItem(this.tokenPrefix + 'type:' + id));
            response.data = data;
            var /** @type {?} */ references = JSON.parse(localStorage.getItem(this.tokenPrefix + 'references:' + id));
            if (references) {
                for (var /** @type {?} */ key in references) {
                    response.data[key] = this.getNode(references[key]);
                }
            }
        }
        return response;
        var e_2, _a;
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.getCollectionInfo = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + 'collection:' + id));
        if (data) {
            var /** @type {?} */ info = new CollectionInfo(id);
            for (var /** @type {?} */ key in data) {
                if (key !== '_path') {
                    info[key] = data[key];
                }
            }
            return info;
        }
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.setTimestamp = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        localStorage.setItem(this.tokenPrefix + 'timestamp:' + id, String(Date.now()));
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.getTimestamp = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return Number(localStorage.getItem(this.tokenPrefix + 'timestamp:' + id));
    };
    /**
     * @param {?} id
     * @return {?}
     */
    LocalStorageResponseCache.prototype.removeTimestamp = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return localStorage.removeItem(this.tokenPrefix + 'timestamp:' + id);
    };
    LocalStorageResponseCache.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    LocalStorageResponseCache.ctorParameters = function () { return [
        { type: ObjectCollector, },
        { type: ApiProcessor, },
        { type: DataInfoService, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return LocalStorageResponseCache;
}(ResponseCache));
export { LocalStorageResponseCache };
function LocalStorageResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    LocalStorageResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    LocalStorageResponseCache.ctorParameters;
    /** @type {?} */
    LocalStorageResponseCache.prototype.tokenPrefix;
    /** @type {?} */
    LocalStorageResponseCache.prototype.collector;
    /** @type {?} */
    LocalStorageResponseCache.prototype.processor;
    /** @type {?} */
    LocalStorageResponseCache.prototype.info;
    /** @type {?} */
    LocalStorageResponseCache.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc3RvcmFnZS1yZXNwb25zZS1jYWNoZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY2FjaGUvbG9jYWwtc3RvcmFnZS1yZXNwb25zZS1jYWNoZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxZQUFZLEVBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFBRSxVQUFVLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDdEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUN0QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sYUFBYSxDQUFDOztJQUdJLHFEQUFhO0lBSTFELG1DQUNVLFdBQ0EsV0FDQSxNQUNzQjtRQUpoQyxZQU1FLGlCQUFPLFNBU1I7UUFkUyxlQUFTLEdBQVQsU0FBUztRQUNULGVBQVMsR0FBVCxTQUFTO1FBQ1QsVUFBSSxHQUFKLElBQUk7UUFDa0IsWUFBTSxHQUFOLE1BQU07NEJBTmhCLGVBQWU7UUFTbkMscUJBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxDQUFDO1FBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEVBQWhDLENBQWdDLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFDO1lBQ2xGLHFCQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMvQyxxQkFBTSxTQUFTLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsSUFBSSxTQUFTLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDaEUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNqQjtTQUNGLENBQUMsQ0FBQzs7S0FDSjs7Ozs7SUFFRCx1Q0FBRzs7OztJQUFILFVBQUksRUFBVTtRQUNaLEVBQUUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN4RDtLQUNGOzs7Ozs7SUFFRCx1Q0FBRzs7Ozs7SUFBSCxVQUFJLEVBQVUsRUFBRSxJQUFTO1FBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztLQUM1Qjs7Ozs7O0lBRUQsNENBQVE7Ozs7O0lBQVIsVUFBUyxFQUFVLEVBQUUsS0FBYTtRQUNoQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztLQUMvRDs7Ozs7SUFFRCx1Q0FBRzs7OztJQUFILFVBQUksRUFBVTtRQUNaLEVBQUUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsRSxxQkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDdkYscUJBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDeEMsTUFBTSxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUksUUFBUSxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDbEY7Ozs7O0lBRUQsMENBQU07Ozs7SUFBTixVQUFPLEVBQVU7UUFDZixZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDL0MsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztRQUN6RCxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDN0QsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQzFCOzs7O0lBRUQseUNBQUs7OztJQUFMO1FBQUEsaUJBSUM7UUFIQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBakMsQ0FBaUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQUM7WUFDbkYsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM1QixDQUFDLENBQUM7S0FDSjs7Ozs7OztJQUVPLHlDQUFLOzs7Ozs7Y0FBQyxFQUFVLEVBQUUsSUFBUyxFQUFFLFFBQWlCO1FBQ3BELHFCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxxQkFBSSxNQUFXLENBQUM7UUFDaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxZQUFZLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDbkMsTUFBTSxHQUFHLEVBQUUsQ0FBQzs7Z0JBQ1osR0FBRyxDQUFDLENBQWUsSUFBQSxTQUFBLGlCQUFBLElBQUksQ0FBQSwwQkFBQTtvQkFBbEIsSUFBTSxJQUFJLGlCQUFBO29CQUNiLHFCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQzFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDbkM7Ozs7Ozs7OztZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNuRjtRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04scUJBQU0sVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUN0QixNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ1osR0FBRyxDQUFDLENBQUMscUJBQU0sR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLHFCQUFJLFFBQVEsU0FBSyxDQUFDO2dCQUNsQixFQUFFLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUNsQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQ3JDO2dCQUNELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDNUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7aUJBQ2pDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNOLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3pCO2FBQ0Y7WUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxHQUFHLEVBQUUsRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5RixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7U0FDekY7UUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDcEYsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Ozs7OztJQUdoQiwyQ0FBTzs7OztjQUFDLEVBQVU7UUFDeEIscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDckUscUJBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNsRCxxQkFBTSxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNwQyxFQUFFLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ25CLFFBQVEsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQzNCLFFBQVEsQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDO1lBQy9CLFFBQVEsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0QsUUFBUSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7O2dCQUNuQixHQUFHLENBQUMsQ0FBaUIsSUFBQSxTQUFBLGlCQUFBLElBQUksQ0FBQSwwQkFBQTtvQkFBcEIsSUFBTSxNQUFNLGlCQUFBO29CQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztpQkFDMUM7Ozs7Ozs7OztTQUNGO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixRQUFRLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUM1QixRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ25DLFFBQVEsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0YsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDckIscUJBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNGLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsR0FBRyxDQUFDLENBQUMscUJBQU0sR0FBRyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0JBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDcEQ7YUFDRjtTQUNGO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQzs7Ozs7OztJQUdWLHFEQUFpQjs7OztjQUFDLEVBQVU7UUFDbEMscUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3JGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDVCxxQkFBTSxJQUFJLEdBQUcsSUFBSSxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDcEMsR0FBRyxDQUFDLENBQUMscUJBQU0sR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLEVBQUUsQ0FBQyxDQUFDLEdBQUcsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN2QjthQUNGO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztTQUNiOzs7Ozs7SUFHSyxnREFBWTs7OztjQUFDLEVBQVU7UUFDN0IsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksR0FBRyxFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Ozs7OztJQUd6RSxnREFBWTs7OztjQUFDLEVBQVU7UUFDN0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Ozs7OztJQUdwRSxtREFBZTs7OztjQUFDLEVBQVU7UUFDaEMsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsRUFBRSxDQUFDLENBQUM7OztnQkE5SXhFLFVBQVU7Ozs7Z0JBUEYsZUFBZTtnQkFDZixZQUFZO2dCQUVJLGVBQWU7Z0RBYW5DLE1BQU0sU0FBQyxZQUFZOztvQ0FsQnhCO0VBVStDLGFBQWE7U0FBL0MseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vcmVzcG9uc2UtY2FjaGUnO1xuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYmplY3RDb2xsZWN0b3IgfSBmcm9tICcuLi9jb3JlL29iamVjdC1jb2xsZWN0b3InO1xuaW1wb3J0IHsgQXBpUHJvY2Vzc29yIH0gZnJvbSAnLi4vY29yZS9hcGkucHJvY2Vzc29yJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi4vY29yZS9jb25maWcnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbkluZm8sIERhdGFJbmZvU2VydmljZSwgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4uL3Jlc3BvbnNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvY2FsU3RvcmFnZVJlc3BvbnNlQ2FjaGUgZXh0ZW5kcyBSZXNwb25zZUNhY2hlIHtcblxuICBwcml2YXRlIHRva2VuUHJlZml4ID0gJ2FucmVzdDpjYWNoZTonO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgY29sbGVjdG9yOiBPYmplY3RDb2xsZWN0b3IsXG4gICAgcHJpdmF0ZSBwcm9jZXNzb3I6IEFwaVByb2Nlc3NvcixcbiAgICBwcml2YXRlIGluZm86IERhdGFJbmZvU2VydmljZSxcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJpdmF0ZSBjb25maWc6IEFwaUNvbmZpZ1xuICApIHtcbiAgICBzdXBlcigpO1xuICAgIGNvbnN0IHRpbWVzdGFtcFByZWZpeCA9IHRoaXMudG9rZW5QcmVmaXggKyAndGltZXN0YW1wOic7XG4gICAgT2JqZWN0LmtleXMobG9jYWxTdG9yYWdlKS5maWx0ZXIoKGUpID0+IGUuaW5kZXhPZih0aW1lc3RhbXBQcmVmaXgpID09PSAwKS5mb3JFYWNoKChrKSA9PiB7XG4gICAgICBjb25zdCBpZCA9IGsuc3Vic3RyaW5nKHRpbWVzdGFtcFByZWZpeC5sZW5ndGgpO1xuICAgICAgY29uc3QgdGltZXN0YW1wID0gdGhpcy5nZXRUaW1lc3RhbXAoaWQpO1xuICAgICAgaWYgKCF0aW1lc3RhbXAgfHwgdGltZXN0YW1wICsgdGhpcy5jb25maWcuY2FjaGVUVEwgPCBEYXRlLm5vdygpKSB7XG4gICAgICAgIHRoaXMucmVtb3ZlKGlkKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGdldChpZDogc3RyaW5nKSB7XG4gICAgaWQgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2FsaWFzOicgKyBpZCkgfHwgaWQ7XG4gICAgaWYgKHRoaXMuaGFzKGlkKSkge1xuICAgICAgcmV0dXJuIHRoaXMucHJvY2Vzc29yLnByb2Nlc3ModGhpcy5nZXROb2RlKGlkKSwgJ0dFVCcpO1xuICAgIH1cbiAgfVxuXG4gIGFkZChpZDogc3RyaW5nLCBkYXRhOiBhbnkpIHtcbiAgICB0aGlzLmRvQWRkKGlkLCBkYXRhLCB0cnVlKTtcbiAgfVxuXG4gIGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpIHtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2FsaWFzOicgKyBhbGlhcywgaWQpO1xuICB9XG5cbiAgaGFzKGlkOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBpZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGlkKSB8fCBpZDtcbiAgICBjb25zdCBjb21wbGV0ZSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb21wbGV0ZTonICsgaWQpKTtcbiAgICBjb25zdCB0aW1lc3RhbXAgPSB0aGlzLmdldFRpbWVzdGFtcChpZCk7XG4gICAgcmV0dXJuICEhdGltZXN0YW1wICYmIGNvbXBsZXRlICYmIHRpbWVzdGFtcCArIHRoaXMuY29uZmlnLmNhY2hlVFRMID49IERhdGUubm93KCk7XG4gIH1cblxuICByZW1vdmUoaWQ6IHN0cmluZykge1xuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICd0eXBlOicgKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb2xsZWN0aW9uOicgKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb21wbGV0ZTonICsgaWQpO1xuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAncmVmZXJlbmNlczonICsgaWQpO1xuICAgIHRoaXMucmVtb3ZlVGltZXN0YW1wKGlkKTtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICAgIE9iamVjdC5rZXlzKGxvY2FsU3RvcmFnZSkuZmlsdGVyKChlKSA9PiBlLmluZGV4T2YodGhpcy50b2tlblByZWZpeCkgPT09IDApLmZvckVhY2goKGspID0+IHtcbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGspO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBkb0FkZChpZDogc3RyaW5nLCBkYXRhOiBhbnksIGNvbXBsZXRlOiBib29sZWFuKSB7XG4gICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQoZGF0YSk7XG4gICAgbGV0IHZhbHVlczogYW55O1xuICAgIGlmIChpbmZvIGluc3RhbmNlb2YgQ29sbGVjdGlvbkluZm8pIHtcbiAgICAgIHZhbHVlcyA9IFtdO1xuICAgICAgZm9yIChjb25zdCBpdGVtIG9mIGRhdGEpIHtcbiAgICAgICAgY29uc3QgaXRlbVBhdGggPSB0aGlzLmluZm8uZ2V0KGl0ZW0pLnBhdGg7XG4gICAgICAgIHZhbHVlcy5wdXNoKGl0ZW1QYXRoKTtcbiAgICAgICAgdGhpcy5kb0FkZChpdGVtUGF0aCwgaXRlbSwgZmFsc2UpO1xuICAgICAgfVxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdjb2xsZWN0aW9uOicgKyBpZCwgSlNPTi5zdHJpbmdpZnkoaW5mbykpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zdCByZWZlcmVuY2VzID0ge307XG4gICAgICB2YWx1ZXMgPSB7fTtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGRhdGEpIHtcbiAgICAgICAgbGV0IGl0ZW1JbmZvOiBhbnk7XG4gICAgICAgIGlmICh0eXBlb2YgZGF0YVtrZXldID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgIGl0ZW1JbmZvID0gdGhpcy5pbmZvLmdldChkYXRhW2tleV0pO1xuICAgICAgICB9XG4gICAgICAgIGlmIChpdGVtSW5mbykge1xuICAgICAgICAgIHRoaXMuZG9BZGQoaXRlbUluZm8ucGF0aCwgZGF0YVtrZXldLCBmYWxzZSk7XG4gICAgICAgICAgcmVmZXJlbmNlc1trZXldID0gaXRlbUluZm8ucGF0aDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWx1ZXNba2V5XSA9IGRhdGFba2V5XTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICd0eXBlOicgKyBpZCwgTWV0YVNlcnZpY2UuZ2V0KGRhdGEuY29uc3RydWN0b3IpLm5hbWUpO1xuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCwgSlNPTi5zdHJpbmdpZnkocmVmZXJlbmNlcykpO1xuICAgIH1cbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCwgSlNPTi5zdHJpbmdpZnkoY29tcGxldGUpKTtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgaWQsIEpTT04uc3RyaW5naWZ5KHZhbHVlcykpO1xuICAgIHRoaXMuc2V0VGltZXN0YW1wKGlkKTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0Tm9kZShpZDogc3RyaW5nKTogUmVzcG9uc2VOb2RlIHtcbiAgICBjb25zdCBkYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgaWQpKTtcbiAgICBjb25zdCBjb2xsZWN0aW9uSW5mbyA9IHRoaXMuZ2V0Q29sbGVjdGlvbkluZm8oaWQpO1xuICAgIGNvbnN0IHJlc3BvbnNlID0gbmV3IFJlc3BvbnNlTm9kZSgpO1xuICAgIGlmIChjb2xsZWN0aW9uSW5mbykge1xuICAgICAgcmVzcG9uc2UuY29sbGVjdGlvbiA9IHRydWU7XG4gICAgICByZXNwb25zZS5pbmZvID0gY29sbGVjdGlvbkluZm87XG4gICAgICByZXNwb25zZS5tZXRhID0gTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKGNvbGxlY3Rpb25JbmZvLnR5cGUpO1xuICAgICAgcmVzcG9uc2UuZGF0YSA9IFtdO1xuICAgICAgZm9yIChjb25zdCBpdGVtSWQgb2YgZGF0YSkge1xuICAgICAgICByZXNwb25zZS5kYXRhLnB1c2godGhpcy5nZXROb2RlKGl0ZW1JZCkpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICByZXNwb25zZS5jb2xsZWN0aW9uID0gZmFsc2U7XG4gICAgICByZXNwb25zZS5pbmZvID0gbmV3IEVudGl0eUluZm8oaWQpO1xuICAgICAgcmVzcG9uc2UubWV0YSA9IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkKSk7XG4gICAgICByZXNwb25zZS5kYXRhID0gZGF0YTtcbiAgICAgIGNvbnN0IHJlZmVyZW5jZXMgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAncmVmZXJlbmNlczonICsgaWQpKTtcbiAgICAgIGlmIChyZWZlcmVuY2VzKSB7XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIHJlZmVyZW5jZXMpIHtcbiAgICAgICAgICByZXNwb25zZS5kYXRhW2tleV0gPSB0aGlzLmdldE5vZGUocmVmZXJlbmNlc1trZXldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcmVzcG9uc2U7XG4gIH1cblxuICBwcml2YXRlIGdldENvbGxlY3Rpb25JbmZvKGlkOiBzdHJpbmcpIHtcbiAgICBjb25zdCBkYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkKSk7XG4gICAgaWYgKGRhdGEpIHtcbiAgICAgIGNvbnN0IGluZm8gPSBuZXcgQ29sbGVjdGlvbkluZm8oaWQpO1xuICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuICAgICAgICBpZiAoa2V5ICE9PSAnX3BhdGgnKSB7XG4gICAgICAgICAgaW5mb1trZXldID0gZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gaW5mbztcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHNldFRpbWVzdGFtcChpZDogc3RyaW5nKSB7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JyArIGlkLCBTdHJpbmcoRGF0ZS5ub3coKSkpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRUaW1lc3RhbXAoaWQ6IHN0cmluZykge1xuICAgIHJldHVybiBOdW1iZXIobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JyArIGlkKSk7XG4gIH1cblxuICBwcml2YXRlIHJlbW92ZVRpbWVzdGFtcChpZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAndGltZXN0YW1wOicgKyBpZCk7XG4gIH1cbn1cbiJdfQ==