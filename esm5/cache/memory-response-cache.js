/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ResponseCache } from './response-cache';
import { Inject, Injectable } from '@angular/core';
import { ObjectCollector } from '../core/object-collector';
import { AnRestConfig } from '../core/config';
var MemoryResponseCache = /** @class */ (function (_super) {
    tslib_1.__extends(MemoryResponseCache, _super);
    function MemoryResponseCache(collector, config) {
        var _this = _super.call(this) || this;
        _this.collector = collector;
        _this.config = config;
        _this.times = {};
        _this.aliases = {};
        return _this;
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    MemoryResponseCache.prototype.add = /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    function (id, data) {
        this.times[id] = Date.now();
    };
    /**
     * @return {?}
     */
    MemoryResponseCache.prototype.clear = /**
     * @return {?}
     */
    function () {
        this.collector.clear();
    };
    /**
     * @param {?} id
     * @return {?}
     */
    MemoryResponseCache.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return this.collector.get(id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    MemoryResponseCache.prototype.remove = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        this.collector.remove(id);
    };
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    MemoryResponseCache.prototype.addAlias = /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    function (id, alias) {
        this.aliases[alias] = id;
    };
    /**
     * @param {?} id
     * @return {?}
     */
    MemoryResponseCache.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var /** @type {?} */ time = this.aliases[id] ? this.times[this.aliases[id]] : this.times[id];
        return time && (Date.now() <= this.config.cacheTTL + time);
    };
    MemoryResponseCache.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    MemoryResponseCache.ctorParameters = function () { return [
        { type: ObjectCollector, },
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    ]; };
    return MemoryResponseCache;
}(ResponseCache));
export { MemoryResponseCache };
function MemoryResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    MemoryResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    MemoryResponseCache.ctorParameters;
    /** @type {?} */
    MemoryResponseCache.prototype.times;
    /** @type {?} */
    MemoryResponseCache.prototype.aliases;
    /** @type {?} */
    MemoryResponseCache.prototype.collector;
    /** @type {?} */
    MemoryResponseCache.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVtb3J5LXJlc3BvbnNlLWNhY2hlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJjYWNoZS9tZW1vcnktcmVzcG9uc2UtY2FjaGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxZQUFZLEVBQWEsTUFBTSxnQkFBZ0IsQ0FBQzs7SUFHaEIsK0NBQWE7SUFNcEQsNkJBQ21CLFdBQ3dCO1FBRjNDLFlBSUUsaUJBQU8sU0FDUjtRQUprQixlQUFTLEdBQVQsU0FBUztRQUNlLFlBQU0sR0FBTixNQUFNO3NCQU5KLEVBQUU7d0JBRUEsRUFBRTs7S0FPaEQ7Ozs7OztJQUVELGlDQUFHOzs7OztJQUFILFVBQUksRUFBVSxFQUFFLElBQVM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDN0I7Ozs7SUFFRCxtQ0FBSzs7O0lBQUw7UUFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0tBQ3hCOzs7OztJQUVELGlDQUFHOzs7O0lBQUgsVUFBSSxFQUFVO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQy9COzs7OztJQUVELG9DQUFNOzs7O0lBQU4sVUFBTyxFQUFVO1FBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDM0I7Ozs7OztJQUVELHNDQUFROzs7OztJQUFSLFVBQVMsRUFBVSxFQUFFLEtBQWE7UUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDMUI7Ozs7O0lBRUQsaUNBQUc7Ozs7SUFBSCxVQUFJLEVBQVU7UUFDWixxQkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDOUUsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQztLQUM1RDs7Z0JBckNGLFVBQVU7Ozs7Z0JBSEYsZUFBZTtnREFZbkIsTUFBTSxTQUFDLFlBQVk7OzhCQWR4QjtFQU15QyxhQUFhO1NBQXpDLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL3Jlc3BvbnNlLWNhY2hlJztcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JqZWN0Q29sbGVjdG9yIH0gZnJvbSAnLi4vY29yZS9vYmplY3QtY29sbGVjdG9yJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi4vY29yZS9jb25maWcnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTWVtb3J5UmVzcG9uc2VDYWNoZSBleHRlbmRzIFJlc3BvbnNlQ2FjaGUge1xuXG4gIHByaXZhdGUgdGltZXM6IHsgW2luZGV4OiBzdHJpbmddOiBudW1iZXIgfSA9IHt9O1xuXG4gIHByaXZhdGUgYWxpYXNlczogeyBbaW5kZXg6IHN0cmluZ106IHN0cmluZyB9ID0ge307XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByZWFkb25seSBjb2xsZWN0b3I6IE9iamVjdENvbGxlY3RvcixcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIHJlYWRvbmx5IGNvbmZpZzogQXBpQ29uZmlnXG4gICkge1xuICAgIHN1cGVyKCk7XG4gIH1cblxuICBhZGQoaWQ6IHN0cmluZywgZGF0YTogYW55KSB7XG4gICAgdGhpcy50aW1lc1tpZF0gPSBEYXRlLm5vdygpO1xuICB9XG5cbiAgY2xlYXIoKSB7XG4gICAgdGhpcy5jb2xsZWN0b3IuY2xlYXIoKTtcbiAgfVxuXG4gIGdldChpZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIHRoaXMuY29sbGVjdG9yLmdldChpZCk7XG4gIH1cblxuICByZW1vdmUoaWQ6IHN0cmluZykge1xuICAgIHRoaXMuY29sbGVjdG9yLnJlbW92ZShpZCk7XG4gIH1cblxuICBhZGRBbGlhcyhpZDogc3RyaW5nLCBhbGlhczogc3RyaW5nKSB7XG4gICAgdGhpcy5hbGlhc2VzW2FsaWFzXSA9IGlkO1xuICB9XG5cbiAgaGFzKGlkOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBjb25zdCB0aW1lID0gdGhpcy5hbGlhc2VzW2lkXSA/IHRoaXMudGltZXNbdGhpcy5hbGlhc2VzW2lkXV0gOiB0aGlzLnRpbWVzW2lkXTtcbiAgICByZXR1cm4gdGltZSAmJiAoRGF0ZS5ub3coKSA8PSB0aGlzLmNvbmZpZy5jYWNoZVRUTCArIHRpbWUpO1xuICB9XG5cbn1cbiJdfQ==