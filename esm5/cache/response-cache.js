/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable, InjectionToken } from '@angular/core';
/**
 * @abstract
 */
var ResponseCache = /** @class */ (function () {
    function ResponseCache() {
    }
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.replace = /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    function (data, id) {
        this.remove(id);
        this.add(data, id);
    };
    ResponseCache.decorators = [
        { type: Injectable },
    ];
    return ResponseCache;
}());
export { ResponseCache };
function ResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ResponseCache.ctorParameters;
    /**
     * @abstract
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.has = function (id) { };
    /**
     * @abstract
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.get = function (id) { };
    /**
     * @abstract
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    ResponseCache.prototype.add = function (id, data) { };
    /**
     * @abstract
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.remove = function (id) { };
    /**
     * @abstract
     * @return {?}
     */
    ResponseCache.prototype.clear = function () { };
    /**
     * @abstract
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    ResponseCache.prototype.addAlias = function (id, alias) { };
}
export var /** @type {?} */ ANREST_CACHE_SERVICES = new InjectionToken('anrest.cache_services');

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UtY2FjaGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNhY2hlL3Jlc3BvbnNlLWNhY2hlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7Ozs7Ozs7Ozs7O0lBaUJ6RCwrQkFBTzs7Ozs7SUFBUCxVQUFRLElBQVMsRUFBRSxFQUFVO1FBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDcEI7O2dCQWxCRixVQUFVOzt3QkFGWDs7U0FHc0IsYUFBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQm5DLE1BQU0sQ0FBQyxxQkFBSSxxQkFBcUIsR0FBRyxJQUFJLGNBQWMsQ0FBa0IsdUJBQXVCLENBQUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBSZXNwb25zZUNhY2hlIHtcblxuICBhYnN0cmFjdCBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW47XG5cbiAgYWJzdHJhY3QgZ2V0KGlkOiBzdHJpbmcpO1xuXG4gIGFic3RyYWN0IGFkZChpZDogc3RyaW5nLCBkYXRhOiBhbnkpO1xuXG4gIGFic3RyYWN0IHJlbW92ZShpZDogc3RyaW5nKTtcblxuICBhYnN0cmFjdCBjbGVhcigpO1xuXG4gIGFic3RyYWN0IGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpO1xuXG4gIHJlcGxhY2UoZGF0YTogYW55LCBpZDogc3RyaW5nKSB7XG4gICAgdGhpcy5yZW1vdmUoaWQpO1xuICAgIHRoaXMuYWRkKGRhdGEsIGlkKTtcbiAgfVxufVxuXG5leHBvcnQgbGV0IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyA9IG5ldyBJbmplY3Rpb25Ub2tlbjxSZXNwb25zZUNhY2hlW10+KCdhbnJlc3QuY2FjaGVfc2VydmljZXMnKTtcbiJdfQ==