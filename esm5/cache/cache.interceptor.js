/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AnRestConfig, DataInfoService } from '../core';
import { ANREST_CACHE_SERVICES } from './response-cache';
import { MetaService } from '../meta';
import { NoResponseCache } from './no-response-cache';
var CacheInterceptor = /** @class */ (function () {
    function CacheInterceptor(config, info, cacheServices) {
        this.config = config;
        this.info = info;
        this.cacheServices = cacheServices;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    CacheInterceptor.prototype.intercept = /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    function (request, next) {
        var _this = this;
        var /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        var /** @type {?} */ cacheType = MetaService.getByName(request.headers.get('X-AnRest-Type')).cache || this.config.cache || NoResponseCache;
        var /** @type {?} */ cache;
        try {
            for (var _a = tslib_1.__values(this.cacheServices), _b = _a.next(); !_b.done; _b = _a.next()) {
                var cacheService = _b.value;
                if (cacheService instanceof cacheType) {
                    cache = cacheService;
                    break;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        if (request.method !== 'GET') {
            cache.remove(path);
            return next.handle(request);
        }
        if (cache.has(path)) {
            return of(new HttpResponse({
                body: cache.get(path),
                status: 200,
                url: request.url
            }));
        }
        return next.handle(request).pipe(tap(function (response) {
            if (response.body) {
                var /** @type {?} */ info = _this.info.get(response.body);
                if (info) {
                    cache.add(info.path, response.body);
                    if (info && info.alias) {
                        cache.addAlias(info.path, info.alias);
                    }
                }
            }
        }));
        var e_1, _c;
    };
    CacheInterceptor.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    CacheInterceptor.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
        { type: DataInfoService, },
        { type: Array, decorators: [{ type: Inject, args: [ANREST_CACHE_SERVICES,] },] },
    ]; };
    return CacheInterceptor;
}());
export { CacheInterceptor };
function CacheInterceptor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    CacheInterceptor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    CacheInterceptor.ctorParameters;
    /** @type {?} */
    CacheInterceptor.prototype.config;
    /** @type {?} */
    CacheInterceptor.prototype.info;
    /** @type {?} */
    CacheInterceptor.prototype.cacheServices;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FjaGUuaW50ZXJjZXB0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNhY2hlL2NhY2hlLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUlZLFlBQVksRUFDOUIsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QixPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsWUFBWSxFQUFhLGVBQWUsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUNuRSxPQUFPLEVBQUUscUJBQXFCLEVBQWlCLE1BQU0sa0JBQWtCLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUN0QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7O0lBS3BELDBCQUMyQyxRQUN4QixNQUMrQjtRQUZQLFdBQU0sR0FBTixNQUFNO1FBQzlCLFNBQUksR0FBSixJQUFJO1FBQzJCLGtCQUFhLEdBQWIsYUFBYTtLQUMzRDs7Ozs7O0lBRUosb0NBQVM7Ozs7O0lBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO1FBQXRELGlCQXNDQztRQXJDQyxxQkFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckUsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87ZUFDcEYsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUM3QjtRQUNELHFCQUFNLFNBQVMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLGVBQWUsQ0FBQztRQUM1SCxxQkFBSSxLQUFVLENBQUM7O1lBQ2YsR0FBRyxDQUFDLENBQXVCLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsYUFBYSxDQUFBLGdCQUFBO2dCQUF4QyxJQUFNLFlBQVksV0FBQTtnQkFDckIsRUFBRSxDQUFDLENBQUMsWUFBWSxZQUFZLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLEtBQUssR0FBRyxZQUFZLENBQUM7b0JBQ3JCLEtBQUssQ0FBQztpQkFDUDthQUNGOzs7Ozs7Ozs7UUFDRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDN0IsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUM3QjtRQUNELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxZQUFZLENBQUM7Z0JBQ3pCLElBQUksRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztnQkFDckIsTUFBTSxFQUFFLEdBQUc7Z0JBQ1gsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHO2FBQ2pCLENBQUMsQ0FBQyxDQUFDO1NBQ0w7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQzlCLEdBQUcsQ0FBQyxVQUFDLFFBQTJCO1lBQzlCLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixxQkFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMxQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNULEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BDLEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDdkIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDdkM7aUJBQ0Y7YUFDRjtTQUNGLENBQUMsQ0FDSCxDQUFDOztLQUNIOztnQkEvQ0YsVUFBVTs7OztnREFJTixNQUFNLFNBQUMsWUFBWTtnQkFUVSxlQUFlOzRDQVc1QyxNQUFNLFNBQUMscUJBQXFCOzsyQkFwQmpDOztTQWVhLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZywgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsIFJlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL3Jlc3BvbnNlLWNhY2hlJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBOb1Jlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL25vLXJlc3BvbnNlLWNhY2hlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIENhY2hlSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgcmVhZG9ubHkgY29uZmlnOiBBcGlDb25maWcsXG4gICAgcHJpdmF0ZSByZWFkb25seSBpbmZvOiBEYXRhSW5mb1NlcnZpY2UsXG4gICAgQEluamVjdChBTlJFU1RfQ0FDSEVfU0VSVklDRVMpIHByaXZhdGUgcmVhZG9ubHkgY2FjaGVTZXJ2aWNlczogUmVzcG9uc2VDYWNoZVtdLFxuICApIHt9XG5cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGNvbnN0IHBhdGggPSByZXF1ZXN0LnVybFdpdGhQYXJhbXMuc2xpY2UodGhpcy5jb25maWcuYmFzZVVybC5sZW5ndGgpO1xuICAgIGlmIChyZXF1ZXN0LnVybC5pbmRleE9mKHRoaXMuY29uZmlnLmJhc2VVcmwpICE9PSAwIHx8IHJlcXVlc3QudXJsID09PSB0aGlzLmNvbmZpZy5hdXRoVXJsXG4gICAgICB8fCAoQXJyYXkuaXNBcnJheSh0aGlzLmNvbmZpZy5leGNsdWRlZFVybHMpICYmIHRoaXMuY29uZmlnLmV4Y2x1ZGVkVXJscy5pbmRleE9mKHJlcXVlc3QudXJsKSAhPT0gLTEpKSB7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIGNvbnN0IGNhY2hlVHlwZSA9IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShyZXF1ZXN0LmhlYWRlcnMuZ2V0KCdYLUFuUmVzdC1UeXBlJykpLmNhY2hlIHx8IHRoaXMuY29uZmlnLmNhY2hlIHx8IE5vUmVzcG9uc2VDYWNoZTtcbiAgICBsZXQgY2FjaGU6IGFueTtcbiAgICBmb3IgKGNvbnN0IGNhY2hlU2VydmljZSBvZiB0aGlzLmNhY2hlU2VydmljZXMpIHtcbiAgICAgIGlmIChjYWNoZVNlcnZpY2UgaW5zdGFuY2VvZiBjYWNoZVR5cGUpIHtcbiAgICAgICAgY2FjaGUgPSBjYWNoZVNlcnZpY2U7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAocmVxdWVzdC5tZXRob2QgIT09ICdHRVQnKSB7XG4gICAgICBjYWNoZS5yZW1vdmUocGF0aCk7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIGlmIChjYWNoZS5oYXMocGF0aCkpIHtcbiAgICAgIHJldHVybiBvZihuZXcgSHR0cFJlc3BvbnNlKHtcbiAgICAgICAgYm9keTogY2FjaGUuZ2V0KHBhdGgpLFxuICAgICAgICBzdGF0dXM6IDIwMCxcbiAgICAgICAgdXJsOiByZXF1ZXN0LnVybFxuICAgICAgfSkpO1xuICAgIH1cbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCkucGlwZShcbiAgICAgIHRhcCgocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XG4gICAgICAgIGlmIChyZXNwb25zZS5ib2R5KSB7XG4gICAgICAgICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQocmVzcG9uc2UuYm9keSk7XG4gICAgICAgICAgaWYgKGluZm8pIHtcbiAgICAgICAgICAgIGNhY2hlLmFkZChpbmZvLnBhdGgsIHJlc3BvbnNlLmJvZHkpO1xuICAgICAgICAgICAgaWYgKGluZm8gJiYgaW5mby5hbGlhcykge1xuICAgICAgICAgICAgICBjYWNoZS5hZGRBbGlhcyhpbmZvLnBhdGgsIGluZm8uYWxpYXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApO1xuICB9XG59XG4iXX0=