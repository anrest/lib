/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ResponseCache } from './response-cache';
import { Injectable } from '@angular/core';
var NoResponseCache = /** @class */ (function (_super) {
    tslib_1.__extends(NoResponseCache, _super);
    function NoResponseCache() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.has = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        return false;
    };
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    NoResponseCache.prototype.add = /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    function (id, data) {
    };
    /**
     * @return {?}
     */
    NoResponseCache.prototype.clear = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.get = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.remove = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
    };
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    NoResponseCache.prototype.addAlias = /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    function (id, alias) {
    };
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    NoResponseCache.prototype.replace = /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    function (data, id) {
    };
    NoResponseCache.decorators = [
        { type: Injectable },
    ];
    return NoResponseCache;
}(ResponseCache));
export { NoResponseCache };
function NoResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    NoResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    NoResponseCache.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm8tcmVzcG9uc2UtY2FjaGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNhY2hlL25vLXJlc3BvbnNlLWNhY2hlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0lBR04sMkNBQWE7Ozs7Ozs7O0lBQ2hELDZCQUFHOzs7O0lBQUgsVUFBSSxFQUFVO1FBQ1osTUFBTSxDQUFDLEtBQUssQ0FBQztLQUNkOzs7Ozs7SUFFRCw2QkFBRzs7Ozs7SUFBSCxVQUFJLEVBQVUsRUFBRSxJQUFTO0tBQ3hCOzs7O0lBRUQsK0JBQUs7OztJQUFMO0tBQ0M7Ozs7O0lBRUQsNkJBQUc7Ozs7SUFBSCxVQUFJLEVBQVU7S0FDYjs7Ozs7SUFFRCxnQ0FBTTs7OztJQUFOLFVBQU8sRUFBVTtLQUNoQjs7Ozs7O0lBRUQsa0NBQVE7Ozs7O0lBQVIsVUFBUyxFQUFVLEVBQUUsS0FBYTtLQUNqQzs7Ozs7O0lBRUQsaUNBQU87Ozs7O0lBQVAsVUFBUSxJQUFTLEVBQUUsRUFBVTtLQUM1Qjs7Z0JBdEJGLFVBQVU7OzBCQUhYO0VBSXFDLGFBQWE7U0FBckMsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL3Jlc3BvbnNlLWNhY2hlJztcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE5vUmVzcG9uc2VDYWNoZSBleHRlbmRzIFJlc3BvbnNlQ2FjaGUge1xuICBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGFkZChpZDogc3RyaW5nLCBkYXRhOiBhbnkpIHtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICB9XG5cbiAgZ2V0KGlkOiBzdHJpbmcpIHtcbiAgfVxuXG4gIHJlbW92ZShpZDogc3RyaW5nKSB7XG4gIH1cblxuICBhZGRBbGlhcyhpZDogc3RyaW5nLCBhbGlhczogc3RyaW5nKSB7XG4gIH1cblxuICByZXBsYWNlKGRhdGE6IGFueSwgaWQ6IHN0cmluZyk6IHZvaWQge1xuICB9XG59XG4iXX0=