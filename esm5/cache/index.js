/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
export { CacheInterceptor } from './cache.interceptor';
export { ResponseCache, ANREST_CACHE_SERVICES } from './response-cache';
export { NoResponseCache } from './no-response-cache';
export { MemoryResponseCache } from './memory-response-cache';
export { LocalStorageResponseCache } from './local-storage-response-cache';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNhY2hlL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxpQ0FBYyxxQkFBcUIsQ0FBQztBQUNwQyxxREFBYyxrQkFBa0IsQ0FBQztBQUNqQyxnQ0FBYyxxQkFBcUIsQ0FBQztBQUNwQyxvQ0FBYyx5QkFBeUIsQ0FBQztBQUN4QywwQ0FBYyxnQ0FBZ0MsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGZyb20gJy4vY2FjaGUuaW50ZXJjZXB0b3InO1xuZXhwb3J0ICogZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5leHBvcnQgKiBmcm9tICcuL25vLXJlc3BvbnNlLWNhY2hlJztcbmV4cG9ydCAqIGZyb20gJy4vbWVtb3J5LXJlc3BvbnNlLWNhY2hlJztcbmV4cG9ydCAqIGZyb20gJy4vbG9jYWwtc3RvcmFnZS1yZXNwb25zZS1jYWNoZSc7XG4iXX0=