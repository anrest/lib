/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Inject, Injectable, Optional } from '@angular/core';
import { ANREST_HTTP_EVENT_LISTENERS } from './events';
import { MetaService } from '../meta';
var EventsService = /** @class */ (function () {
    function EventsService(handlers) {
        this.handlers = handlers;
        if (!Array.isArray(this.handlers)) {
            this.handlers = [];
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    EventsService.prototype.handle = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.handlers.forEach(function (handler) {
            var /** @type {?} */ meta = MetaService.getOrCreateForEventListener(handler.constructor);
            try {
                for (var _a = tslib_1.__values(meta.events), _b = _a.next(); !_b.done; _b = _a.next()) {
                    var eventMeta = _b.value;
                    if (eventMeta.type === event.constructor && eventMeta.entity === event.entity()) {
                        handler.handle(event);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
                }
                finally { if (e_1) throw e_1.error; }
            }
            var e_1, _c;
        });
        return event;
    };
    EventsService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    EventsService.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_EVENT_LISTENERS,] },] },
    ]; };
    return EventsService;
}());
export { EventsService };
function EventsService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    EventsService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    EventsService.ctorParameters;
    /** @type {?} */
    EventsService.prototype.handlers;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImV2ZW50cy9ldmVudHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQ0wsMkJBQTJCLEVBQzVCLE1BQU0sVUFBVSxDQUFDO0FBQ2xCLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxTQUFTLENBQUM7O0lBS3BDLHVCQUNvRTtRQUFBLGFBQVEsR0FBUixRQUFRO1FBRTFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1NBQ3BCO0tBQ0Y7Ozs7O0lBRU0sOEJBQU07Ozs7Y0FBQyxLQUFZO1FBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBc0I7WUFDM0MscUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7O2dCQUMxRSxHQUFHLENBQUMsQ0FBb0IsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxNQUFNLENBQUEsZ0JBQUE7b0JBQTlCLElBQU0sU0FBUyxXQUFBO29CQUNsQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLEtBQUssQ0FBQyxXQUFXLElBQUksU0FBUyxDQUFDLE1BQU0sS0FBSyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUNoRixPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN2QjtpQkFDRjs7Ozs7Ozs7OztTQUNGLENBQUMsQ0FBQztRQUNILE1BQU0sQ0FBQyxLQUFLLENBQUM7OztnQkFwQmhCLFVBQVU7Ozs7NENBSU4sUUFBUSxZQUFJLE1BQU0sU0FBQywyQkFBMkI7O3dCQVZuRDs7U0FPYSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgQU5SRVNUX0hUVFBfRVZFTlRfTElTVEVORVJTLCBFdmVudCwgRXZlbnRMaXN0ZW5lclxufSBmcm9tICcuL2V2ZW50cyc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRXZlbnRzU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChBTlJFU1RfSFRUUF9FVkVOVF9MSVNURU5FUlMpIHByaXZhdGUgcmVhZG9ubHkgaGFuZGxlcnM6IEV2ZW50TGlzdGVuZXJbXVxuICApIHtcbiAgICBpZiAoIUFycmF5LmlzQXJyYXkodGhpcy5oYW5kbGVycykpIHtcbiAgICAgIHRoaXMuaGFuZGxlcnMgPSBbXTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgaGFuZGxlKGV2ZW50OiBFdmVudCk6IEV2ZW50IHtcbiAgICB0aGlzLmhhbmRsZXJzLmZvckVhY2goKGhhbmRsZXI6IEV2ZW50TGlzdGVuZXIpID0+IHtcbiAgICAgIGNvbnN0IG1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckV2ZW50TGlzdGVuZXIoaGFuZGxlci5jb25zdHJ1Y3Rvcik7XG4gICAgICBmb3IgKGNvbnN0IGV2ZW50TWV0YSBvZiBtZXRhLmV2ZW50cykge1xuICAgICAgICBpZiAoZXZlbnRNZXRhLnR5cGUgPT09IGV2ZW50LmNvbnN0cnVjdG9yICYmIGV2ZW50TWV0YS5lbnRpdHkgPT09IGV2ZW50LmVudGl0eSgpKSB7XG4gICAgICAgICAgaGFuZGxlci5oYW5kbGUoZXZlbnQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIGV2ZW50O1xuICB9XG59XG4iXX0=