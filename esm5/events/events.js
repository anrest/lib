/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { InjectionToken } from '@angular/core';
/**
 * @record
 */
export function Event() { }
function Event_tsickle_Closure_declarations() {
    /** @type {?} */
    Event.prototype.entity;
    /** @type {?} */
    Event.prototype.data;
}
var BaseEvent = /** @class */ (function () {
    function BaseEvent(data, type) {
        this._data = data;
        this.type = type;
    }
    /**
     * @return {?}
     */
    BaseEvent.prototype.data = /**
     * @return {?}
     */
    function () {
        return this._data;
    };
    /**
     * @return {?}
     */
    BaseEvent.prototype.entity = /**
     * @return {?}
     */
    function () {
        return this.type || (Array.isArray(this._data) ? this._data[0].constructor : this._data.constructor);
    };
    return BaseEvent;
}());
export { BaseEvent };
function BaseEvent_tsickle_Closure_declarations() {
    /** @type {?} */
    BaseEvent.prototype._data;
    /** @type {?} */
    BaseEvent.prototype.type;
}
var BeforeGetEvent = /** @class */ (function (_super) {
    tslib_1.__extends(BeforeGetEvent, _super);
    function BeforeGetEvent(path, type, filter) {
        var _this = _super.call(this, path, type) || this;
        _this._filter = filter;
        return _this;
    }
    /**
     * @return {?}
     */
    BeforeGetEvent.prototype.entity = /**
     * @return {?}
     */
    function () {
        return this.type;
    };
    /**
     * @return {?}
     */
    BeforeGetEvent.prototype.filter = /**
     * @return {?}
     */
    function () {
        return this._filter;
    };
    return BeforeGetEvent;
}(BaseEvent));
export { BeforeGetEvent };
function BeforeGetEvent_tsickle_Closure_declarations() {
    /** @type {?} */
    BeforeGetEvent.prototype._filter;
}
var AfterGetEvent = /** @class */ (function (_super) {
    tslib_1.__extends(AfterGetEvent, _super);
    function AfterGetEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AfterGetEvent;
}(BaseEvent));
export { AfterGetEvent };
var BeforeSaveEvent = /** @class */ (function (_super) {
    tslib_1.__extends(BeforeSaveEvent, _super);
    function BeforeSaveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return BeforeSaveEvent;
}(BaseEvent));
export { BeforeSaveEvent };
var AfterSaveEvent = /** @class */ (function (_super) {
    tslib_1.__extends(AfterSaveEvent, _super);
    function AfterSaveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AfterSaveEvent;
}(BaseEvent));
export { AfterSaveEvent };
var BeforeRemoveEvent = /** @class */ (function (_super) {
    tslib_1.__extends(BeforeRemoveEvent, _super);
    function BeforeRemoveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return BeforeRemoveEvent;
}(BaseEvent));
export { BeforeRemoveEvent };
var AfterRemoveEvent = /** @class */ (function (_super) {
    tslib_1.__extends(AfterRemoveEvent, _super);
    function AfterRemoveEvent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AfterRemoveEvent;
}(BaseEvent));
export { AfterRemoveEvent };
/**
 * @record
 */
export function EventListener() { }
function EventListener_tsickle_Closure_declarations() {
    /** @type {?} */
    EventListener.prototype.handle;
}
export var /** @type {?} */ ANREST_HTTP_EVENT_LISTENERS = new InjectionToken('anrest.http_event_listeners');

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJldmVudHMvZXZlbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7Ozs7Ozs7Ozs7QUFRL0MsSUFBQTtJQUlFLG1CQUFZLElBQVMsRUFBRSxJQUFlO1FBQ3BDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0tBQ2xCOzs7O0lBRUQsd0JBQUk7OztJQUFKO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDbkI7Ozs7SUFFRCwwQkFBTTs7O0lBQU47UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUN0RztvQkF2Qkg7SUF3QkMsQ0FBQTtBQWhCRCxxQkFnQkM7Ozs7Ozs7QUFFRCxJQUFBO0lBQW9DLDBDQUFTO0lBRzNDLHdCQUFZLElBQVksRUFBRSxJQUFlLEVBQUUsTUFBbUI7UUFBOUQsWUFDRSxrQkFBTSxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBRWxCO1FBREMsS0FBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7O0tBQ3ZCOzs7O0lBRUQsK0JBQU07OztJQUFOO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7S0FDbEI7Ozs7SUFFRCwrQkFBTTs7O0lBQU47UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztLQUNyQjt5QkF4Q0g7RUEwQm9DLFNBQVMsRUFlNUMsQ0FBQTtBQWZELDBCQWVDOzs7OztBQUNELElBQUE7SUFBbUMseUNBQVM7Ozs7d0JBMUM1QztFQTBDbUMsU0FBUyxFQUFHLENBQUE7QUFBL0MseUJBQStDO0FBQy9DLElBQUE7SUFBcUMsMkNBQVM7Ozs7MEJBM0M5QztFQTJDcUMsU0FBUyxFQUFHLENBQUE7QUFBakQsMkJBQWlEO0FBQ2pELElBQUE7SUFBb0MsMENBQVM7Ozs7eUJBNUM3QztFQTRDb0MsU0FBUyxFQUFHLENBQUE7QUFBaEQsMEJBQWdEO0FBQ2hELElBQUE7SUFBdUMsNkNBQVM7Ozs7NEJBN0NoRDtFQTZDdUMsU0FBUyxFQUFHLENBQUE7QUFBbkQsNkJBQW1EO0FBQ25ELElBQUE7SUFBc0MsNENBQVM7Ozs7MkJBOUMvQztFQThDc0MsU0FBUyxFQUFHLENBQUE7QUFBbEQsNEJBQWtEOzs7Ozs7Ozs7QUFNbEQsTUFBTSxDQUFDLHFCQUFJLDJCQUEyQixHQUFHLElBQUksY0FBYyxDQUFrQiw2QkFBNkIsQ0FBQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRXZlbnQge1xuICBlbnRpdHkoKTogRnVuY3Rpb247XG4gIGRhdGEoKTogYW55O1xufVxuXG5leHBvcnQgY2xhc3MgQmFzZUV2ZW50IGltcGxlbWVudHMgRXZlbnQge1xuICBfZGF0YT86IGFueTtcbiAgcHJvdGVjdGVkIHJlYWRvbmx5IHR5cGU6IEZ1bmN0aW9uO1xuXG4gIGNvbnN0cnVjdG9yKGRhdGE6IGFueSwgdHlwZT86IEZ1bmN0aW9uKSB7XG4gICAgdGhpcy5fZGF0YSA9IGRhdGE7XG4gICAgdGhpcy50eXBlID0gdHlwZTtcbiAgfVxuXG4gIGRhdGEoKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5fZGF0YTtcbiAgfVxuXG4gIGVudGl0eSgpOiBGdW5jdGlvbiB7XG4gICAgcmV0dXJuIHRoaXMudHlwZSB8fCAoQXJyYXkuaXNBcnJheSh0aGlzLl9kYXRhKSA/IHRoaXMuX2RhdGFbMF0uY29uc3RydWN0b3IgOiB0aGlzLl9kYXRhLmNvbnN0cnVjdG9yKTtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgQmVmb3JlR2V0RXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge1xuICBwcml2YXRlIHJlYWRvbmx5IF9maWx0ZXI6IEh0dHBQYXJhbXM7XG5cbiAgY29uc3RydWN0b3IocGF0aDogc3RyaW5nLCB0eXBlPzogRnVuY3Rpb24sIGZpbHRlcj86IEh0dHBQYXJhbXMpIHtcbiAgICBzdXBlcihwYXRoLCB0eXBlKTtcbiAgICB0aGlzLl9maWx0ZXIgPSBmaWx0ZXI7XG4gIH1cblxuICBlbnRpdHkoKTogRnVuY3Rpb24ge1xuICAgIHJldHVybiB0aGlzLnR5cGU7XG4gIH1cblxuICBmaWx0ZXIoKTogSHR0cFBhcmFtcyB7XG4gICAgcmV0dXJuIHRoaXMuX2ZpbHRlcjtcbiAgfVxufVxuZXhwb3J0IGNsYXNzIEFmdGVyR2V0RXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBCZWZvcmVTYXZlRXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBBZnRlclNhdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuZXhwb3J0IGNsYXNzIEJlZm9yZVJlbW92ZUV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHt9XG5leHBvcnQgY2xhc3MgQWZ0ZXJSZW1vdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuXG5leHBvcnQgaW50ZXJmYWNlIEV2ZW50TGlzdGVuZXIge1xuICBoYW5kbGUoZXZlbnQ6IEV2ZW50KTtcbn1cblxuZXhwb3J0IGxldCBBTlJFU1RfSFRUUF9FVkVOVF9MSVNURU5FUlMgPSBuZXcgSW5qZWN0aW9uVG9rZW48RXZlbnRMaXN0ZW5lcltdPignYW5yZXN0Lmh0dHBfZXZlbnRfbGlzdGVuZXJzJyk7XG4iXX0=