/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
export { EventsService } from './events.service';
export { BaseEvent, BeforeGetEvent, AfterGetEvent, BeforeSaveEvent, AfterSaveEvent, BeforeRemoveEvent, AfterRemoveEvent, ANREST_HTTP_EVENT_LISTENERS } from './events';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImV2ZW50cy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsOEJBQWMsa0JBQWtCLENBQUM7QUFDakMsNEpBQWMsVUFBVSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0ICogZnJvbSAnLi9ldmVudHMuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2V2ZW50cyc7XG4iXX0=