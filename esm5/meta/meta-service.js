/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { EntityMetaInfo, PropertyMetaInfo, ServiceMetaInfo, EventListenerMetaInfo } from './meta-info';
var MetaService = /** @class */ (function () {
    function MetaService() {
    }
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.get = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return MetaService.map.get(key);
    };
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.getByName = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        try {
            for (var _a = tslib_1.__values(MetaService.map.values()), _b = _a.next(); !_b.done; _b = _a.next()) {
                var meta = _b.value;
                if (meta instanceof EntityMetaInfo && meta.name === key) {
                    return meta;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        var e_1, _c;
    };
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.getOrCreateForEntity = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        var /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EntityMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    };
    /**
     * @param {?} key
     * @return {?}
     */
    MetaService.getOrCreateForEventListener = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        var /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EventListenerMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    };
    /**
     * @param {?} key
     * @param {?} property
     * @param {?=} type
     * @return {?}
     */
    MetaService.getOrCreateForProperty = /**
     * @param {?} key
     * @param {?} property
     * @param {?=} type
     * @return {?}
     */
    function (key, property, type) {
        var /** @type {?} */ entityMeta = MetaService.getOrCreateForEntity(key.constructor);
        if (!entityMeta.properties[property]) {
            entityMeta.properties[property] = new PropertyMetaInfo(type || Reflect.getMetadata('design:type', key, property));
        }
        return entityMeta.properties[property];
    };
    /**
     * @param {?} key
     * @param {?} entity
     * @return {?}
     */
    MetaService.getOrCreateForHttpService = /**
     * @param {?} key
     * @param {?} entity
     * @return {?}
     */
    function (key, entity) {
        var /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new ServiceMetaInfo(key, MetaService.getOrCreateForEntity(entity));
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    };
    /**
     * @param {?} meta
     * @return {?}
     */
    MetaService.set = /**
     * @param {?} meta
     * @return {?}
     */
    function (meta) {
        MetaService.map.set(meta.type, meta);
    };
    MetaService.map = new Map();
    return MetaService;
}());
export { MetaService };
function MetaService_tsickle_Closure_declarations() {
    /** @type {?} */
    MetaService.map;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJtZXRhL21ldGEtc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQVksZUFBZSxFQUFFLHFCQUFxQixFQUFFLE1BQU0sYUFBYSxDQUFDOzs7Ozs7OztJQU14RyxlQUFHOzs7O0lBQVYsVUFBVyxHQUFRO1FBQ2pCLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUNqQzs7Ozs7SUFFTSxxQkFBUzs7OztJQUFoQixVQUFpQixHQUFXOztZQUMxQixHQUFHLENBQUMsQ0FBZSxJQUFBLEtBQUEsaUJBQUEsV0FBVyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQSxnQkFBQTtnQkFBdEMsSUFBTSxJQUFJLFdBQUE7Z0JBQ2IsRUFBRSxDQUFDLENBQUMsSUFBSSxZQUFZLGNBQWMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUM7aUJBQ2I7YUFDRjs7Ozs7Ozs7OztLQUNGOzs7OztJQUVNLGdDQUFvQjs7OztJQUEzQixVQUE0QixHQUFhO1FBQ3ZDLHFCQUFJLElBQUksR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNWLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQixXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QsTUFBTSxtQkFBaUIsSUFBSSxFQUFDO0tBQzdCOzs7OztJQUVNLHVDQUEyQjs7OztJQUFsQyxVQUFtQyxHQUFhO1FBQzlDLHFCQUFJLElBQUksR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNWLElBQUksR0FBRyxJQUFJLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RDLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCxNQUFNLG1CQUF3QixJQUFJLEVBQUM7S0FDcEM7Ozs7Ozs7SUFFTSxrQ0FBc0I7Ozs7OztJQUE3QixVQUE4QixHQUFRLEVBQUUsUUFBZ0IsRUFBRSxJQUFVO1FBQ2xFLHFCQUFNLFVBQVUsR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JFLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLGdCQUFnQixDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUNuSDtRQUNELE1BQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ3hDOzs7Ozs7SUFFTSxxQ0FBeUI7Ozs7O0lBQWhDLFVBQWlDLEdBQWEsRUFBRSxNQUFnQjtRQUM5RCxxQkFBSSxJQUFJLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDVixJQUFJLEdBQUcsSUFBSSxlQUFlLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzFFLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCxNQUFNLG1CQUFrQixJQUFJLEVBQUM7S0FDOUI7Ozs7O0lBRU0sZUFBRzs7OztJQUFWLFVBQVcsSUFBYztRQUN2QixXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3RDO3NCQW5Ed0MsSUFBSSxHQUFHLEVBQWlCO3NCQUpuRTs7U0FFYSxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRW50aXR5TWV0YUluZm8sIFByb3BlcnR5TWV0YUluZm8sIE1ldGFJbmZvLCBTZXJ2aWNlTWV0YUluZm8sIEV2ZW50TGlzdGVuZXJNZXRhSW5mbyB9IGZyb20gJy4vbWV0YS1pbmZvJztcblxuZXhwb3J0IGNsYXNzIE1ldGFTZXJ2aWNlIHtcblxuICBwcml2YXRlIHN0YXRpYyBtYXA6IE1hcDxhbnksIE1ldGFJbmZvPiA9IG5ldyBNYXA8YW55LCBNZXRhSW5mbz4oKTtcblxuICBzdGF0aWMgZ2V0KGtleTogYW55KTogYW55IHtcbiAgICByZXR1cm4gTWV0YVNlcnZpY2UubWFwLmdldChrZXkpO1xuICB9XG5cbiAgc3RhdGljIGdldEJ5TmFtZShrZXk6IHN0cmluZyk6IGFueSB7XG4gICAgZm9yIChjb25zdCBtZXRhIG9mIE1ldGFTZXJ2aWNlLm1hcC52YWx1ZXMoKSkge1xuICAgICAgaWYgKG1ldGEgaW5zdGFuY2VvZiBFbnRpdHlNZXRhSW5mbyAmJiBtZXRhLm5hbWUgPT09IGtleSkge1xuICAgICAgICByZXR1cm4gbWV0YTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBzdGF0aWMgZ2V0T3JDcmVhdGVGb3JFbnRpdHkoa2V5OiBGdW5jdGlvbik6IEVudGl0eU1ldGFJbmZvIHtcbiAgICBsZXQgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldChrZXkpO1xuICAgIGlmICghbWV0YSkge1xuICAgICAgbWV0YSA9IG5ldyBFbnRpdHlNZXRhSW5mbyhrZXkpO1xuICAgICAgTWV0YVNlcnZpY2Uuc2V0KG1ldGEpO1xuICAgIH1cbiAgICByZXR1cm4gPEVudGl0eU1ldGFJbmZvPm1ldGE7XG4gIH1cblxuICBzdGF0aWMgZ2V0T3JDcmVhdGVGb3JFdmVudExpc3RlbmVyKGtleTogRnVuY3Rpb24pOiBFdmVudExpc3RlbmVyTWV0YUluZm8ge1xuICAgIGxldCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0KGtleSk7XG4gICAgaWYgKCFtZXRhKSB7XG4gICAgICBtZXRhID0gbmV3IEV2ZW50TGlzdGVuZXJNZXRhSW5mbyhrZXkpO1xuICAgICAgTWV0YVNlcnZpY2Uuc2V0KG1ldGEpO1xuICAgIH1cbiAgICByZXR1cm4gPEV2ZW50TGlzdGVuZXJNZXRhSW5mbz5tZXRhO1xuICB9XG5cbiAgc3RhdGljIGdldE9yQ3JlYXRlRm9yUHJvcGVydHkoa2V5OiBhbnksIHByb3BlcnR5OiBzdHJpbmcsIHR5cGU/OiBhbnkpOiBQcm9wZXJ0eU1ldGFJbmZvIHtcbiAgICBjb25zdCBlbnRpdHlNZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkoa2V5LmNvbnN0cnVjdG9yKTtcbiAgICBpZiAoIWVudGl0eU1ldGEucHJvcGVydGllc1twcm9wZXJ0eV0pIHtcbiAgICAgIGVudGl0eU1ldGEucHJvcGVydGllc1twcm9wZXJ0eV0gPSBuZXcgUHJvcGVydHlNZXRhSW5mbyh0eXBlIHx8IFJlZmxlY3QuZ2V0TWV0YWRhdGEoJ2Rlc2lnbjp0eXBlJywga2V5LCBwcm9wZXJ0eSkpO1xuICAgIH1cbiAgICByZXR1cm4gZW50aXR5TWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRPckNyZWF0ZUZvckh0dHBTZXJ2aWNlKGtleTogRnVuY3Rpb24sIGVudGl0eTogRnVuY3Rpb24pOiBTZXJ2aWNlTWV0YUluZm8ge1xuICAgIGxldCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0KGtleSk7XG4gICAgaWYgKCFtZXRhKSB7XG4gICAgICBtZXRhID0gbmV3IFNlcnZpY2VNZXRhSW5mbyhrZXksIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KGVudGl0eSkpO1xuICAgICAgTWV0YVNlcnZpY2Uuc2V0KG1ldGEpO1xuICAgIH1cbiAgICByZXR1cm4gPFNlcnZpY2VNZXRhSW5mbz5tZXRhO1xuICB9XG5cbiAgc3RhdGljIHNldChtZXRhOiBNZXRhSW5mbykge1xuICAgIE1ldGFTZXJ2aWNlLm1hcC5zZXQobWV0YS50eXBlLCBtZXRhKTtcbiAgfVxufVxuIl19