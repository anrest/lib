/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpHeaders } from '@angular/common/http';
var MetaInfo = /** @class */ (function () {
    function MetaInfo(_type) {
        this._type = _type;
    }
    Object.defineProperty(MetaInfo.prototype, "type", {
        get: /**
         * @return {?}
         */
        function () {
            return this._type;
        },
        enumerable: true,
        configurable: true
    });
    return MetaInfo;
}());
export { MetaInfo };
function MetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    MetaInfo.prototype._type;
}
var PropertyMetaInfo = /** @class */ (function (_super) {
    tslib_1.__extends(PropertyMetaInfo, _super);
    function PropertyMetaInfo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return PropertyMetaInfo;
}(MetaInfo));
export { PropertyMetaInfo };
function PropertyMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    PropertyMetaInfo.prototype.isCollection;
    /** @type {?} */
    PropertyMetaInfo.prototype.excludeWhenSaving;
}
var EventListenerMetaInfo = /** @class */ (function (_super) {
    tslib_1.__extends(EventListenerMetaInfo, _super);
    function EventListenerMetaInfo() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.events = [];
        return _this;
    }
    return EventListenerMetaInfo;
}(MetaInfo));
export { EventListenerMetaInfo };
function EventListenerMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    EventListenerMetaInfo.prototype.events;
}
var EntityMetaInfo = /** @class */ (function (_super) {
    tslib_1.__extends(EntityMetaInfo, _super);
    function EntityMetaInfo() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.id = 'id';
        _this.headers = [];
        _this.properties = {};
        _this.subresources = {};
        return _this;
    }
    /**
     * @param {?=} object
     * @return {?}
     */
    EntityMetaInfo.prototype.getHeaders = /**
     * @param {?=} object
     * @return {?}
     */
    function (object) {
        var /** @type {?} */ headers = new HttpHeaders();
        headers = headers.set('X-AnRest-Type', this.name);
        try {
            for (var _a = tslib_1.__values(this.headers), _b = _a.next(); !_b.done; _b = _a.next()) {
                var header = _b.value;
                if (!header.dynamic || object) {
                    headers = headers[header.append ? 'append' : 'set'](header.name, header.value.call(object));
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_b && !_b.done && (_c = _a.return)) _c.call(_a);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return headers;
        var e_1, _c;
    };
    /**
     * @return {?}
     */
    EntityMetaInfo.prototype.getPropertiesForSave = /**
     * @return {?}
     */
    function () {
        var /** @type {?} */ included = [];
        for (var /** @type {?} */ property in this.properties) {
            if (!this.properties[property].excludeWhenSaving) {
                included.push(property);
            }
        }
        return included;
    };
    return EntityMetaInfo;
}(MetaInfo));
export { EntityMetaInfo };
function EntityMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    EntityMetaInfo.prototype.name;
    /** @type {?} */
    EntityMetaInfo.prototype.id;
    /** @type {?} */
    EntityMetaInfo.prototype.service;
    /** @type {?} */
    EntityMetaInfo.prototype.headers;
    /** @type {?} */
    EntityMetaInfo.prototype.properties;
    /** @type {?} */
    EntityMetaInfo.prototype.subresources;
    /** @type {?} */
    EntityMetaInfo.prototype.body;
    /** @type {?} */
    EntityMetaInfo.prototype.cache;
}
var ServiceMetaInfo = /** @class */ (function (_super) {
    tslib_1.__extends(ServiceMetaInfo, _super);
    function ServiceMetaInfo(type, _entityMeta) {
        var _this = _super.call(this, type) || this;
        _this._entityMeta = _entityMeta;
        return _this;
    }
    Object.defineProperty(ServiceMetaInfo.prototype, "entityMeta", {
        get: /**
         * @return {?}
         */
        function () {
            return this._entityMeta;
        },
        enumerable: true,
        configurable: true
    });
    return ServiceMetaInfo;
}(MetaInfo));
export { ServiceMetaInfo };
function ServiceMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    ServiceMetaInfo.prototype.path;
    /** @type {?} */
    ServiceMetaInfo.prototype._entityMeta;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS1pbmZvLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJtZXRhL21ldGEtaW5mby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVuRCxJQUFBO0lBRUUsa0JBQW9CLEtBQWU7UUFBZixVQUFLLEdBQUwsS0FBSyxDQUFVO0tBQUk7SUFFdkMsc0JBQUksMEJBQUk7Ozs7UUFBUjtZQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQ25COzs7T0FBQTttQkFSSDtJQVNDLENBQUE7QUFQRCxvQkFPQzs7Ozs7QUFFRCxJQUFBO0lBQXNDLDRDQUFROzs7OzJCQVg5QztFQVdzQyxRQUFRLEVBRzdDLENBQUE7QUFIRCw0QkFHQzs7Ozs7OztBQUVELElBQUE7SUFBMkMsaURBQVE7Ozt1QkFDTyxFQUFFOzs7Z0NBakI1RDtFQWdCMkMsUUFBUSxFQUVsRCxDQUFBO0FBRkQsaUNBRUM7Ozs7O0FBRUQsSUFBQTtJQUFvQywwQ0FBUTs7O21CQUU5QixJQUFJO3dCQUU2RSxFQUFFOzJCQUNwQyxFQUFFOzZCQUNxQixFQUFFOzs7Ozs7O0lBSTdFLG1DQUFVOzs7O2NBQUMsTUFBWTtRQUM1QixxQkFBSSxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNoQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOztZQUNsRCxHQUFHLENBQUMsQ0FBaUIsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxPQUFPLENBQUEsZ0JBQUE7Z0JBQTVCLElBQU0sTUFBTSxXQUFBO2dCQUNmLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUM5QixPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUM3RjthQUNGOzs7Ozs7Ozs7UUFDRCxNQUFNLENBQUMsT0FBTyxDQUFDOzs7Ozs7SUFHViw2Q0FBb0I7Ozs7UUFDekIscUJBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNwQixHQUFHLENBQUMsQ0FBQyxxQkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDdkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDakQsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN6QjtTQUNGO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQzs7eUJBaERwQjtFQW9Cb0MsUUFBUSxFQThCM0MsQ0FBQTtBQTlCRCwwQkE4QkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFRCxJQUFBO0lBQXFDLDJDQUFRO0lBRzNDLHlCQUFZLElBQWMsRUFBVSxXQUEyQjtRQUEvRCxZQUNFLGtCQUFNLElBQUksQ0FBQyxTQUNaO1FBRm1DLGlCQUFXLEdBQVgsV0FBVyxDQUFnQjs7S0FFOUQ7SUFFRCxzQkFBSSx1Q0FBVTs7OztRQUFkO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7U0FDekI7OztPQUFBOzBCQTdESDtFQW9EcUMsUUFBUSxFQVU1QyxDQUFBO0FBVkQsMkJBVUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuZXhwb3J0IGNsYXNzIE1ldGFJbmZvIHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF90eXBlOiBGdW5jdGlvbikge31cblxuICBnZXQgdHlwZSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLl90eXBlO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBQcm9wZXJ0eU1ldGFJbmZvIGV4dGVuZHMgTWV0YUluZm8ge1xuICBwdWJsaWMgaXNDb2xsZWN0aW9uOiBib29sZWFuO1xuICBwdWJsaWMgZXhjbHVkZVdoZW5TYXZpbmc6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBjbGFzcyBFdmVudExpc3RlbmVyTWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBldmVudHM6IHsgZW50aXR5OiBGdW5jdGlvbiwgdHlwZTogRnVuY3Rpb24gfVtdID0gW107XG59XG5cbmV4cG9ydCBjbGFzcyBFbnRpdHlNZXRhSW5mbyBleHRlbmRzIE1ldGFJbmZvIHtcbiAgcHVibGljIG5hbWU6IHN0cmluZztcbiAgcHVibGljIGlkID0gJ2lkJztcbiAgcHVibGljIHNlcnZpY2U6IEZ1bmN0aW9uO1xuICBwdWJsaWMgaGVhZGVyczogeyBuYW1lOiBzdHJpbmcsIHZhbHVlOiAoKSA9PiBzdHJpbmcsIGFwcGVuZDogYm9vbGVhbiwgZHluYW1pYzogYm9vbGVhbiB9W10gPSBbXTtcbiAgcHVibGljIHByb3BlcnRpZXM6IHsgW2luZGV4OiBzdHJpbmddOiBQcm9wZXJ0eU1ldGFJbmZvIH0gPSB7fTtcbiAgcHVibGljIHN1YnJlc291cmNlczogeyBbaW5kZXg6IHN0cmluZ106IHsgbWV0YTogRW50aXR5TWV0YUluZm8sIHBhdGg6IHN0cmluZ30gfSA9IHt9O1xuICBwdWJsaWMgYm9keTogKG9iamVjdDogYW55KSA9PiBzdHJpbmc7XG4gIHB1YmxpYyBjYWNoZTogRnVuY3Rpb247XG5cbiAgcHVibGljIGdldEhlYWRlcnMob2JqZWN0PzogYW55KTogSHR0cEhlYWRlcnMge1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XG4gICAgaGVhZGVycyA9IGhlYWRlcnMuc2V0KCdYLUFuUmVzdC1UeXBlJywgdGhpcy5uYW1lKTtcbiAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiB0aGlzLmhlYWRlcnMpIHtcbiAgICAgIGlmICghaGVhZGVyLmR5bmFtaWMgfHwgb2JqZWN0KSB7XG4gICAgICAgIGhlYWRlcnMgPSBoZWFkZXJzW2hlYWRlci5hcHBlbmQgPyAnYXBwZW5kJyA6ICdzZXQnXShoZWFkZXIubmFtZSwgaGVhZGVyLnZhbHVlLmNhbGwob2JqZWN0KSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBoZWFkZXJzO1xuICB9XG5cbiAgcHVibGljIGdldFByb3BlcnRpZXNGb3JTYXZlKCkge1xuICAgIGNvbnN0IGluY2x1ZGVkID0gW107XG4gICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiB0aGlzLnByb3BlcnRpZXMpIHtcbiAgICAgIGlmICghdGhpcy5wcm9wZXJ0aWVzW3Byb3BlcnR5XS5leGNsdWRlV2hlblNhdmluZykge1xuICAgICAgICBpbmNsdWRlZC5wdXNoKHByb3BlcnR5KTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGluY2x1ZGVkO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTZXJ2aWNlTWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBwYXRoOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IodHlwZTogRnVuY3Rpb24sIHByaXZhdGUgX2VudGl0eU1ldGE6IEVudGl0eU1ldGFJbmZvKSB7XG4gICAgc3VwZXIodHlwZSk7XG4gIH1cblxuICBnZXQgZW50aXR5TWV0YSgpOiBFbnRpdHlNZXRhSW5mbyB7XG4gICAgcmV0dXJuIHRoaXMuX2VudGl0eU1ldGE7XG4gIH1cbn1cbiJdfQ==