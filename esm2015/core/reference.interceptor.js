/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnRestConfig } from './config';
import { MetaService } from '../meta';
export class ReferenceInterceptor {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl || !this.isReferenceRequest(request)) {
            return next.handle(request);
        }
        return this.createReferenceResponse(request);
    }
    /**
     * @param {?} request
     * @return {?}
     */
    isReferenceRequest(request) {
        return request.method === 'GET' && request.url.slice(this.config.baseUrl.length).indexOf('&') === 0;
    }
    /**
     * @param {?} request
     * @return {?}
     */
    createReferenceResponse(request) {
        const /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        return of(new HttpResponse({
            body: { 'path': path.substring(1, path.indexOf('#')), 'meta': MetaService.getByName(path.substring(path.indexOf('#') + 1)) },
            status: 200,
            url: request.url
        })).pipe(map((response) => {
            if (response instanceof HttpResponse) {
                return response.clone({
                    headers: response.headers.set('Content-Type', '@reference')
                });
            }
        }));
    }
}
ReferenceInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ReferenceInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];
function ReferenceInterceptor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ReferenceInterceptor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ReferenceInterceptor.ctorParameters;
    /** @type {?} */
    ReferenceInterceptor.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmZXJlbmNlLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJjb3JlL3JlZmVyZW5jZS5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUlZLFlBQVksRUFDOUIsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QixPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsWUFBWSxFQUFhLE1BQU0sVUFBVSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFHdEMsTUFBTTs7OztJQUVKLFlBQzJDO1FBQUEsV0FBTSxHQUFOLE1BQU07S0FDN0M7Ozs7OztJQUVKLFNBQVMsQ0FBQyxPQUF5QixFQUFFLElBQWlCO1FBQ3BELEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9ILE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUM5Qzs7Ozs7SUFFTyxrQkFBa0IsQ0FBQyxPQUF5QjtRQUNsRCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxLQUFLLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Ozs7O0lBRzlGLHVCQUF1QixDQUFDLE9BQXlCO1FBQ3ZELHVCQUFNLElBQUksR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyRSxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksWUFBWSxDQUFDO1lBQ3pCLElBQUksRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUM7WUFDMUgsTUFBTSxFQUFFLEdBQUc7WUFDWCxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUc7U0FDakIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNOLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ2YsRUFBRSxDQUFDLENBQUMsUUFBUSxZQUFZLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29CQUNwQixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQztpQkFDNUQsQ0FBQyxDQUFDO2FBQ0o7U0FDRixDQUFDLENBQ0gsQ0FBQzs7OztZQWhDTCxVQUFVOzs7OzRDQUlOLE1BQU0sU0FBQyxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1xuICBIdHRwUmVxdWVzdCxcbiAgSHR0cEhhbmRsZXIsXG4gIEh0dHBFdmVudCxcbiAgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVzcG9uc2Vcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFJlZmVyZW5jZUludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIHJlYWRvbmx5IGNvbmZpZzogQXBpQ29uZmlnXG4gICkge31cblxuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgaWYgKHJlcXVlc3QudXJsLmluZGV4T2YodGhpcy5jb25maWcuYmFzZVVybCkgIT09IDAgfHwgcmVxdWVzdC51cmwgPT09IHRoaXMuY29uZmlnLmF1dGhVcmwgfHwgIXRoaXMuaXNSZWZlcmVuY2VSZXF1ZXN0KHJlcXVlc3QpKSB7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIHJldHVybiB0aGlzLmNyZWF0ZVJlZmVyZW5jZVJlc3BvbnNlKHJlcXVlc3QpO1xuICB9XG5cbiAgcHJpdmF0ZSBpc1JlZmVyZW5jZVJlcXVlc3QocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55Pik6IGJvb2xlYW4ge1xuICAgIHJldHVybiByZXF1ZXN0Lm1ldGhvZCA9PT0gJ0dFVCcgJiYgcmVxdWVzdC51cmwuc2xpY2UodGhpcy5jb25maWcuYmFzZVVybC5sZW5ndGgpLmluZGV4T2YoJyYnKSA9PT0gMDtcbiAgfVxuXG4gIHByaXZhdGUgY3JlYXRlUmVmZXJlbmNlUmVzcG9uc2UocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55Pikge1xuICAgIGNvbnN0IHBhdGggPSByZXF1ZXN0LnVybFdpdGhQYXJhbXMuc2xpY2UodGhpcy5jb25maWcuYmFzZVVybC5sZW5ndGgpO1xuICAgIHJldHVybiBvZihuZXcgSHR0cFJlc3BvbnNlKHtcbiAgICAgIGJvZHk6IHsncGF0aCc6IHBhdGguc3Vic3RyaW5nKDEsIHBhdGguaW5kZXhPZignIycpKSwgJ21ldGEnOiBNZXRhU2VydmljZS5nZXRCeU5hbWUocGF0aC5zdWJzdHJpbmcocGF0aC5pbmRleE9mKCcjJykgKyAxKSl9LFxuICAgICAgc3RhdHVzOiAyMDAsXG4gICAgICB1cmw6IHJlcXVlc3QudXJsXG4gICAgfSkpLnBpcGUoXG4gICAgICBtYXAoKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGlmIChyZXNwb25zZSBpbnN0YW5jZW9mIEh0dHBSZXNwb25zZSkge1xuICAgICAgICAgIHJldHVybiByZXNwb25zZS5jbG9uZSh7XG4gICAgICAgICAgICBoZWFkZXJzOiByZXNwb25zZS5oZWFkZXJzLnNldCgnQ29udGVudC1UeXBlJywgJ0ByZWZlcmVuY2UnKVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KVxuICAgICk7XG4gIH1cbn1cbiJdfQ==