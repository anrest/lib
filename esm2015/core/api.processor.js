/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable, Injector } from '@angular/core';
import { ResponseNode } from '../response/response';
import { DataInfoService } from './data-info.service';
import { EntityMetaInfo } from '../meta';
import { ObjectCollector } from './object-collector';
import { Collection } from './collection';
export class ApiProcessor {
    /**
     * @param {?} injector
     * @param {?} info
     * @param {?} collector
     */
    constructor(injector, info, collector) {
        this.injector = injector;
        this.info = info;
        this.collector = collector;
    }
    /**
     * @param {?} node
     * @param {?} method
     * @return {?}
     */
    process(node, method) {
        return node.collection ? this.processCollection(node) : this.processObject(node);
    }
    /**
     * @param {?} node
     * @return {?}
     */
    processCollection(node) {
        let /** @type {?} */ data;
        if (node.info.path !== undefined) {
            data = this.collector.get(node.info.path) || new Collection(this.injector.get(node.meta.service), node.info);
        }
        else {
            data = new Collection(this.injector.get(node.meta.service), node.info);
        }
        data.length = 0;
        for (const /** @type {?} */ object of node.data) {
            data.push(this.processObject(object));
        }
        this.info.setForCollection(data, node.info);
        this.collector.set(data);
        return data;
    }
    /**
     * @param {?} node
     * @return {?}
     */
    processObject(node) {
        if (node.meta instanceof EntityMetaInfo) {
            const /** @type {?} */ object = this.collector.get(node.info.path) || new node.meta.type();
            for (const /** @type {?} */ property in node.meta.properties) {
                if (object.hasOwnProperty(property) && !node.data.hasOwnProperty(property)) {
                    continue;
                }
                if (node.data[property] instanceof ResponseNode) {
                    object[property] = node.meta.properties[property].isCollection ?
                        this.processCollection(node.data[property]) :
                        this.processObject(node.data[property]);
                }
                else {
                    const /** @type {?} */ type = node.meta.properties[property].type;
                    switch (type) {
                        case Number:
                            object[property] = Number(node.data[property]);
                            break;
                        case Date:
                            object[property] = new Date(node.data[property]);
                            break;
                        case String:
                        default:
                            object[property] = node.data[property];
                            break;
                    }
                }
            }
            for (const /** @type {?} */ property in node.meta.subresources) {
                const /** @type {?} */ service = this.injector.get(node.meta.subresources[property].meta.service);
                object[property] = service.doGet.bind(service, node.info.path + node.meta.subresources[property].path);
            }
            this.info.setForEntity(object, node.info);
            this.collector.set(object);
            return object;
        }
        else {
            return node.data;
        }
    }
}
ApiProcessor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ApiProcessor.ctorParameters = () => [
    { type: Injector, },
    { type: DataInfoService, },
    { type: ObjectCollector, },
];
function ApiProcessor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ApiProcessor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ApiProcessor.ctorParameters;
    /** @type {?} */
    ApiProcessor.prototype.injector;
    /** @type {?} */
    ApiProcessor.prototype.info;
    /** @type {?} */
    ApiProcessor.prototype.collector;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLnByb2Nlc3Nvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY29yZS9hcGkucHJvY2Vzc29yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDcEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDekMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFHMUMsTUFBTTs7Ozs7O0lBRUosWUFDVSxVQUNBLE1BQ0E7UUFGQSxhQUFRLEdBQVIsUUFBUTtRQUNSLFNBQUksR0FBSixJQUFJO1FBQ0osY0FBUyxHQUFULFNBQVM7S0FDZjs7Ozs7O0lBRUosT0FBTyxDQUFDLElBQWtCLEVBQUUsTUFBYztRQUN4QyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ2xGOzs7OztJQUVPLGlCQUFpQixDQUFDLElBQWtCO1FBQzFDLHFCQUFJLElBQUksQ0FBQztRQUNULEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDOUc7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN4RTtRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLEdBQUcsQ0FBQyxDQUFDLHVCQUFNLE1BQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDOzs7Ozs7SUFHTixhQUFhLENBQUMsSUFBa0I7UUFDdEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksWUFBWSxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLHVCQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxRSxHQUFHLENBQUMsQ0FBQyx1QkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUM1QyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMzRSxRQUFRLENBQUM7aUJBQ1Y7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUNoRCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBQzlELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7aUJBQzNDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNOLHVCQUFNLElBQUksR0FBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQ3RELE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ2IsS0FBSyxNQUFNOzRCQUNULE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDOzRCQUMvQyxLQUFLLENBQUM7d0JBQ1IsS0FBSyxJQUFJOzRCQUNQLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7NEJBQ2pELEtBQUssQ0FBQzt3QkFDUixLQUFLLE1BQU0sQ0FBQzt3QkFDWjs0QkFDRSxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDdkMsS0FBSyxDQUFDO3FCQUNUO2lCQUNGO2FBQ0Y7WUFDRCxHQUFHLENBQUMsQ0FBQyx1QkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUM5Qyx1QkFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNqRixNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hHO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMzQixNQUFNLENBQUMsTUFBTSxDQUFDO1NBQ2Y7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2xCOzs7O1lBakVKLFVBQVU7Ozs7WUFQVSxRQUFRO1lBRXBCLGVBQWU7WUFFZixlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0b3IgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4uL3Jlc3BvbnNlL3Jlc3BvbnNlJztcbmltcG9ydCB7IERhdGFJbmZvU2VydmljZSB9IGZyb20gJy4vZGF0YS1pbmZvLnNlcnZpY2UnO1xuaW1wb3J0IHsgRW50aXR5TWV0YUluZm8gfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCB7IE9iamVjdENvbGxlY3RvciB9IGZyb20gJy4vb2JqZWN0LWNvbGxlY3Rvcic7XG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi9jb2xsZWN0aW9uJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFwaVByb2Nlc3NvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IsXG4gICAgcHJpdmF0ZSBpbmZvOiBEYXRhSW5mb1NlcnZpY2UsXG4gICAgcHJpdmF0ZSBjb2xsZWN0b3I6IE9iamVjdENvbGxlY3RvclxuICApIHt9XG5cbiAgcHJvY2Vzcyhub2RlOiBSZXNwb25zZU5vZGUsIG1ldGhvZDogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gbm9kZS5jb2xsZWN0aW9uID8gdGhpcy5wcm9jZXNzQ29sbGVjdGlvbihub2RlKSA6IHRoaXMucHJvY2Vzc09iamVjdChub2RlKTtcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2Vzc0NvbGxlY3Rpb24obm9kZTogUmVzcG9uc2VOb2RlKSB7XG4gICAgbGV0IGRhdGE7XG4gICAgaWYgKG5vZGUuaW5mby5wYXRoICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIGRhdGEgPSB0aGlzLmNvbGxlY3Rvci5nZXQobm9kZS5pbmZvLnBhdGgpIHx8IG5ldyBDb2xsZWN0aW9uKHRoaXMuaW5qZWN0b3IuZ2V0KG5vZGUubWV0YS5zZXJ2aWNlKSwgbm9kZS5pbmZvKTtcbiAgICB9IGVsc2Uge1xuICAgICAgZGF0YSA9IG5ldyBDb2xsZWN0aW9uKHRoaXMuaW5qZWN0b3IuZ2V0KG5vZGUubWV0YS5zZXJ2aWNlKSwgbm9kZS5pbmZvKTtcbiAgICB9XG4gICAgZGF0YS5sZW5ndGggPSAwO1xuICAgIGZvciAoY29uc3Qgb2JqZWN0IG9mIG5vZGUuZGF0YSkge1xuICAgICAgZGF0YS5wdXNoKHRoaXMucHJvY2Vzc09iamVjdChvYmplY3QpKTtcbiAgICB9XG4gICAgdGhpcy5pbmZvLnNldEZvckNvbGxlY3Rpb24oZGF0YSwgbm9kZS5pbmZvKTtcbiAgICB0aGlzLmNvbGxlY3Rvci5zZXQoZGF0YSk7XG4gICAgcmV0dXJuIGRhdGE7XG4gIH1cblxuICBwcml2YXRlIHByb2Nlc3NPYmplY3Qobm9kZTogUmVzcG9uc2VOb2RlKSB7XG4gICAgaWYgKG5vZGUubWV0YSBpbnN0YW5jZW9mIEVudGl0eU1ldGFJbmZvKSB7XG4gICAgICBjb25zdCBvYmplY3QgPSB0aGlzLmNvbGxlY3Rvci5nZXQobm9kZS5pbmZvLnBhdGgpIHx8IG5ldyBub2RlLm1ldGEudHlwZSgpO1xuICAgICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiBub2RlLm1ldGEucHJvcGVydGllcykge1xuICAgICAgICBpZiAob2JqZWN0Lmhhc093blByb3BlcnR5KHByb3BlcnR5KSAmJiAhbm9kZS5kYXRhLmhhc093blByb3BlcnR5KHByb3BlcnR5KSkge1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChub2RlLmRhdGFbcHJvcGVydHldIGluc3RhbmNlb2YgUmVzcG9uc2VOb2RlKSB7XG4gICAgICAgICAgb2JqZWN0W3Byb3BlcnR5XSA9IG5vZGUubWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XS5pc0NvbGxlY3Rpb24gP1xuICAgICAgICAgICAgdGhpcy5wcm9jZXNzQ29sbGVjdGlvbihub2RlLmRhdGFbcHJvcGVydHldKSA6XG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NPYmplY3Qobm9kZS5kYXRhW3Byb3BlcnR5XSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uc3QgdHlwZTogYW55ID0gbm9kZS5tZXRhLnByb3BlcnRpZXNbcHJvcGVydHldLnR5cGU7XG4gICAgICAgICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgICAgICBjYXNlIE51bWJlcjpcbiAgICAgICAgICAgICAgb2JqZWN0W3Byb3BlcnR5XSA9IE51bWJlcihub2RlLmRhdGFbcHJvcGVydHldKTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIERhdGU6XG4gICAgICAgICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBuZXcgRGF0ZShub2RlLmRhdGFbcHJvcGVydHldKTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFN0cmluZzpcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBub2RlLmRhdGFbcHJvcGVydHldO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGZvciAoY29uc3QgcHJvcGVydHkgaW4gbm9kZS5tZXRhLnN1YnJlc291cmNlcykge1xuICAgICAgICBjb25zdCBzZXJ2aWNlID0gdGhpcy5pbmplY3Rvci5nZXQobm9kZS5tZXRhLnN1YnJlc291cmNlc1twcm9wZXJ0eV0ubWV0YS5zZXJ2aWNlKTtcbiAgICAgICAgb2JqZWN0W3Byb3BlcnR5XSA9IHNlcnZpY2UuZG9HZXQuYmluZChzZXJ2aWNlLCBub2RlLmluZm8ucGF0aCArIG5vZGUubWV0YS5zdWJyZXNvdXJjZXNbcHJvcGVydHldLnBhdGgpO1xuICAgICAgfVxuICAgICAgdGhpcy5pbmZvLnNldEZvckVudGl0eShvYmplY3QsIG5vZGUuaW5mbyk7XG4gICAgICB0aGlzLmNvbGxlY3Rvci5zZXQob2JqZWN0KTtcbiAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBub2RlLmRhdGE7XG4gICAgfVxuICB9XG59XG4iXX0=