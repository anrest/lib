/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { DataInfoService } from './data-info.service';
export class ObjectCollector {
    /**
     * @param {?} info
     */
    constructor(info) {
        this.info = info;
        this.map = new Map();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    set(data) {
        this.map.set(this.info.get(data).path, data);
    }
    /**
     * @param {?} data
     * @return {?}
     */
    remove(data) {
        const /** @type {?} */ info = this.info.get(data);
        if (info) {
            data = info.path;
        }
        this.map.delete(data);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        return this.map.get(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        return this.map.has(id);
    }
    /**
     * @return {?}
     */
    clear() {
        this.map.clear();
    }
}
ObjectCollector.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ObjectCollector.ctorParameters = () => [
    { type: DataInfoService, },
];
function ObjectCollector_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ObjectCollector.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ObjectCollector.ctorParameters;
    /** @type {?} */
    ObjectCollector.prototype.map;
    /** @type {?} */
    ObjectCollector.prototype.info;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LWNvbGxlY3Rvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY29yZS9vYmplY3QtY29sbGVjdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUd0RCxNQUFNOzs7O0lBR0YsWUFBb0IsSUFBcUI7UUFBckIsU0FBSSxHQUFKLElBQUksQ0FBaUI7bUJBRmhCLElBQUksR0FBRyxFQUFlO0tBRUY7Ozs7O0lBRTdDLEdBQUcsQ0FBQyxJQUFTO1FBQ1gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQzlDOzs7OztJQUVELE1BQU0sQ0FBQyxJQUFTO1FBQ2QsdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDVCxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNsQjtRQUNELElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3ZCOzs7OztJQUVELEdBQUcsQ0FBQyxFQUFVO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3pCOzs7OztJQUVELEdBQUcsQ0FBQyxFQUFVO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3pCOzs7O0lBRUQsS0FBSztRQUNILElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDbEI7OztZQTVCSixVQUFVOzs7O1lBRkYsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERhdGFJbmZvU2VydmljZSB9IGZyb20gJy4vZGF0YS1pbmZvLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT2JqZWN0Q29sbGVjdG9yIHtcbiAgICBtYXA6IE1hcDxzdHJpbmcsIGFueT4gID0gbmV3IE1hcDxzdHJpbmcsIGFueT4oKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlKSB7fVxuXG4gICAgc2V0KGRhdGE6IGFueSkge1xuICAgICAgdGhpcy5tYXAuc2V0KHRoaXMuaW5mby5nZXQoZGF0YSkucGF0aCwgZGF0YSk7XG4gICAgfVxuXG4gICAgcmVtb3ZlKGRhdGE6IGFueSkge1xuICAgICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQoZGF0YSk7XG4gICAgICBpZiAoaW5mbykge1xuICAgICAgICBkYXRhID0gaW5mby5wYXRoO1xuICAgICAgfVxuICAgICAgdGhpcy5tYXAuZGVsZXRlKGRhdGEpO1xuICAgIH1cblxuICAgIGdldChpZDogc3RyaW5nKSB7XG4gICAgICByZXR1cm4gdGhpcy5tYXAuZ2V0KGlkKTtcbiAgICB9XG5cbiAgICBoYXMoaWQ6IHN0cmluZykge1xuICAgICAgcmV0dXJuIHRoaXMubWFwLmhhcyhpZCk7XG4gICAgfVxuXG4gICAgY2xlYXIoKSB7XG4gICAgICB0aGlzLm1hcC5jbGVhcigpO1xuICAgIH1cbn1cbiJdfQ==