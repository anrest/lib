/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { CollectionInfo } from './data-info.service';
import { map } from 'rxjs/operators';
/**
 * @template T
 */
export class Collection extends Array {
    /**
     * @param {?} service
     * @param {?} info
     * @param {...?} items
     */
    constructor(service, info, ...items) {
        super(...items);
        this.service = service;
        this.info = info;
        Object.setPrototypeOf(this, Collection.prototype);
    }
    /**
     * @template T
     * @param {?} c1
     * @param {?} c2
     * @return {?}
     */
    static merge(c1, c2) {
        const /** @type {?} */ info = new CollectionInfo(c1.info.path);
        info.type = c1.info.type;
        info.next = c2.info.next;
        info.previous = c1.info.previous;
        info.first = c1.info.first;
        info.last = c1.info.last;
        info.total = c1.info.total;
        info.page = c1.info.page;
        info.lastPage = c1.info.lastPage;
        const /** @type {?} */ c = new Collection(c1.service, info);
        c1.forEach((v) => c.push(v));
        c2.forEach((v) => c.push(v));
        return c;
    }
    /**
     * @return {?}
     */
    get total() {
        return this.info.total;
    }
    /**
     * @return {?}
     */
    get page() {
        return this.info.page;
    }
    /**
     * @return {?}
     */
    get pageTotal() {
        return this.info.lastPage;
    }
    /**
     * @return {?}
     */
    first() {
        return /** @type {?} */ (this.service.doGet(this.info.first));
    }
    /**
     * @return {?}
     */
    previous() {
        return /** @type {?} */ (this.service.doGet(this.info.previous));
    }
    /**
     * @return {?}
     */
    next() {
        return /** @type {?} */ (this.service.doGet(this.info.next));
    }
    /**
     * @return {?}
     */
    last() {
        return /** @type {?} */ (this.service.doGet(this.info.last));
    }
    /**
     * @return {?}
     */
    firstPath() {
        return this.info.first;
    }
    /**
     * @return {?}
     */
    previousPath() {
        return this.info.previous;
    }
    /**
     * @return {?}
     */
    nextPath() {
        return this.info.next;
    }
    /**
     * @return {?}
     */
    lastPath() {
        return this.info.last;
    }
    /**
     * @return {?}
     */
    loadMore() {
        return this.next().pipe(map((result) => Collection.merge(this, result)));
    }
    /**
     * @return {?}
     */
    hasMore() {
        return this.hasNext();
    }
    /**
     * @return {?}
     */
    hasNext() {
        return !!this.info.next;
    }
    /**
     * @return {?}
     */
    hasPrevious() {
        return !!this.info.previous;
    }
    /**
     * @return {?}
     */
    hasFirst() {
        return this.hasPrevious();
    }
    /**
     * @return {?}
     */
    hasLast() {
        return this.hasNext();
    }
}
function Collection_tsickle_Closure_declarations() {
    /** @type {?} */
    Collection.prototype.service;
    /** @type {?} */
    Collection.prototype.info;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY29yZS9jb2xsZWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFHckQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRXJDLE1BQU0saUJBQXFCLFNBQVEsS0FBUTs7Ozs7O0lBa0J6QyxZQUFvQixPQUFtQixFQUFVLElBQW9CLEVBQUUsR0FBRyxLQUFVO1FBQ2xGLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBREUsWUFBTyxHQUFQLE9BQU8sQ0FBWTtRQUFVLFNBQUksR0FBSixJQUFJLENBQWdCO1FBRW5FLE1BQU0sQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUNuRDs7Ozs7OztJQW5CRCxNQUFNLENBQUMsS0FBSyxDQUFJLEVBQWlCLEVBQUUsRUFBaUI7UUFDbEQsdUJBQU0sSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2pDLHVCQUFNLENBQUMsR0FBRyxJQUFJLFVBQVUsQ0FBSSxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM3QixFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0IsTUFBTSxDQUFDLENBQUMsQ0FBQztLQUNWOzs7O0lBT0QsSUFBSSxLQUFLO1FBQ1AsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ3hCOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ04sTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3ZCOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1gsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0tBQzNCOzs7O0lBRUQsS0FBSztRQUNILE1BQU0sbUJBQWtCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUM7S0FDN0Q7Ozs7SUFFRCxRQUFRO1FBQ04sTUFBTSxtQkFBa0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQztLQUNoRTs7OztJQUVELElBQUk7UUFDRixNQUFNLG1CQUFrQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFDO0tBQzVEOzs7O0lBRUQsSUFBSTtRQUNGLE1BQU0sbUJBQWtCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUM7S0FDNUQ7Ozs7SUFFRCxTQUFTO1FBQ1AsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ3hCOzs7O0lBRUQsWUFBWTtRQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztLQUMzQjs7OztJQUVELFFBQVE7UUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7S0FDdkI7Ozs7SUFFRCxRQUFRO1FBQ04sTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3ZCOzs7O0lBRUQsUUFBUTtRQUNOLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUNyQixHQUFHLENBQUMsQ0FBQyxNQUFxQixFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFJLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUNsRSxDQUFDO0tBQ0g7Ozs7SUFFRCxPQUFPO1FBQ0wsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUN2Qjs7OztJQUVELE9BQU87UUFDTCxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3pCOzs7O0lBRUQsV0FBVztRQUNULE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7S0FDN0I7Ozs7SUFFRCxRQUFRO1FBQ04sTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztLQUMzQjs7OztJQUVELE9BQU87UUFDTCxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ3ZCO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb2xsZWN0aW9uSW5mbyB9IGZyb20gJy4vZGF0YS1pbmZvLnNlcnZpY2UnO1xuaW1wb3J0IHsgQXBpU2VydmljZSB9IGZyb20gJy4vYXBpLnNlcnZpY2UnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5leHBvcnQgY2xhc3MgQ29sbGVjdGlvbjxUPiBleHRlbmRzIEFycmF5PFQ+IHtcblxuICBzdGF0aWMgbWVyZ2U8VD4oYzE6IENvbGxlY3Rpb248VD4sIGMyOiBDb2xsZWN0aW9uPFQ+KTogQ29sbGVjdGlvbjxUPiB7XG4gICAgY29uc3QgaW5mbyA9IG5ldyBDb2xsZWN0aW9uSW5mbyhjMS5pbmZvLnBhdGgpO1xuICAgIGluZm8udHlwZSA9IGMxLmluZm8udHlwZTtcbiAgICBpbmZvLm5leHQgPSBjMi5pbmZvLm5leHQ7XG4gICAgaW5mby5wcmV2aW91cyA9IGMxLmluZm8ucHJldmlvdXM7XG4gICAgaW5mby5maXJzdCA9IGMxLmluZm8uZmlyc3Q7XG4gICAgaW5mby5sYXN0ID0gYzEuaW5mby5sYXN0O1xuICAgIGluZm8udG90YWwgPSBjMS5pbmZvLnRvdGFsO1xuICAgIGluZm8ucGFnZSA9IGMxLmluZm8ucGFnZTtcbiAgICBpbmZvLmxhc3RQYWdlID0gYzEuaW5mby5sYXN0UGFnZTtcbiAgICBjb25zdCBjID0gbmV3IENvbGxlY3Rpb248VD4oYzEuc2VydmljZSwgaW5mbyk7XG4gICAgYzEuZm9yRWFjaCgodikgPT4gYy5wdXNoKHYpKTtcbiAgICBjMi5mb3JFYWNoKCh2KSA9PiBjLnB1c2godikpO1xuICAgIHJldHVybiBjO1xuICB9XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzZXJ2aWNlOiBBcGlTZXJ2aWNlLCBwcml2YXRlIGluZm86IENvbGxlY3Rpb25JbmZvLCAuLi5pdGVtczogVFtdKSB7XG4gICAgc3VwZXIoLi4uaXRlbXMpO1xuICAgIE9iamVjdC5zZXRQcm90b3R5cGVPZih0aGlzLCBDb2xsZWN0aW9uLnByb3RvdHlwZSk7XG4gIH1cblxuICBnZXQgdG90YWwoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLnRvdGFsO1xuICB9XG5cbiAgZ2V0IHBhZ2UoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLnBhZ2U7XG4gIH1cblxuICBnZXQgcGFnZVRvdGFsKCk6IG51bWJlciB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5sYXN0UGFnZTtcbiAgfVxuXG4gIGZpcnN0KCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5maXJzdCk7XG4gIH1cblxuICBwcmV2aW91cygpOiBPYnNlcnZhYmxlPFRbXT4ge1xuICAgIHJldHVybiA8T2JzZXJ2YWJsZTxUW10+PnRoaXMuc2VydmljZS5kb0dldCh0aGlzLmluZm8ucHJldmlvdXMpO1xuICB9XG5cbiAgbmV4dCgpOiBPYnNlcnZhYmxlPFRbXT4ge1xuICAgIHJldHVybiA8T2JzZXJ2YWJsZTxUW10+PnRoaXMuc2VydmljZS5kb0dldCh0aGlzLmluZm8ubmV4dCk7XG4gIH1cblxuICBsYXN0KCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5sYXN0KTtcbiAgfVxuXG4gIGZpcnN0UGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8uZmlyc3Q7XG4gIH1cblxuICBwcmV2aW91c1BhdGgoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLnByZXZpb3VzO1xuICB9XG5cbiAgbmV4dFBhdGgoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLm5leHQ7XG4gIH1cblxuICBsYXN0UGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ubGFzdDtcbiAgfVxuXG4gIGxvYWRNb3JlKCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIHRoaXMubmV4dCgpLnBpcGUoXG4gICAgICBtYXAoKHJlc3VsdDogQ29sbGVjdGlvbjxUPikgPT4gQ29sbGVjdGlvbi5tZXJnZTxUPih0aGlzLCByZXN1bHQpKVxuICAgICk7XG4gIH1cblxuICBoYXNNb3JlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc05leHQoKTtcbiAgfVxuXG4gIGhhc05leHQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuICEhdGhpcy5pbmZvLm5leHQ7XG4gIH1cblxuICBoYXNQcmV2aW91cygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gISF0aGlzLmluZm8ucHJldmlvdXM7XG4gIH1cblxuICBoYXNGaXJzdCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5oYXNQcmV2aW91cygpO1xuICB9XG5cbiAgaGFzTGFzdCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5oYXNOZXh0KCk7XG4gIH1cbn1cbiJdfQ==