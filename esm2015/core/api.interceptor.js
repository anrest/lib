/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable, Optional } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AnRestConfig } from './config';
import { ANREST_HTTP_DATA_NORMALIZERS } from '../response/data-normalizer';
import { ApiProcessor } from './api.processor';
export class ApiInterceptor {
    /**
     * @param {?} config
     * @param {?} normalizers
     * @param {?} processor
     */
    constructor(config, normalizers, processor) {
        this.config = config;
        this.normalizers = normalizers;
        this.processor = processor;
        if (!Array.isArray(this.normalizers)) {
            this.normalizers = [];
        }
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        let /** @type {?} */ type;
        let /** @type {?} */ headers = request.headers;
        if (headers.has('X-AnRest-Type')) {
            type = headers.get('X-AnRest-Type');
            headers = headers.delete('x-AnRest-Type');
        }
        if (this.config.defaultHeaders) {
            for (const /** @type {?} */ header of this.config.defaultHeaders) {
                if (!headers.has(header.name) || header.append) {
                    headers = headers[header.append ? 'append' : 'set'](header.name, header.value);
                }
            }
        }
        request = request.clone({
            headers: headers
        });
        return next.handle(request).pipe(map((response) => {
            if (response instanceof HttpResponse) {
                const /** @type {?} */ contentType = response.headers.get('Content-Type');
                for (const /** @type {?} */ normalizer of this.normalizers) {
                    if (normalizer.supports(contentType)) {
                        return response.clone({ body: this.processor.process(normalizer.normalize(response, type), request.method) });
                    }
                }
            }
            return response;
        }));
    }
}
ApiInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ApiInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_DATA_NORMALIZERS,] },] },
    { type: ApiProcessor, },
];
function ApiInterceptor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ApiInterceptor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ApiInterceptor.ctorParameters;
    /** @type {?} */
    ApiInterceptor.prototype.config;
    /** @type {?} */
    ApiInterceptor.prototype.normalizers;
    /** @type {?} */
    ApiInterceptor.prototype.processor;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJjb3JlL2FwaS5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFJWSxZQUFZLEVBQzlCLE1BQU0sc0JBQXNCLENBQUM7QUFFOUIsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxZQUFZLEVBQWEsTUFBTSxVQUFVLENBQUM7QUFDbkQsT0FBTyxFQUFFLDRCQUE0QixFQUFrQixNQUFNLDZCQUE2QixDQUFDO0FBQzNGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUcvQyxNQUFNOzs7Ozs7SUFFSixZQUMyQyxRQUMwQixhQUNsRDtRQUZ3QixXQUFNLEdBQU4sTUFBTTtRQUNvQixnQkFBVyxHQUFYLFdBQVc7UUFDN0QsY0FBUyxHQUFULFNBQVM7UUFFMUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7U0FDdkI7S0FDRjs7Ozs7O0lBRUQsU0FBUyxDQUFDLE9BQXlCLEVBQUUsSUFBaUI7UUFDcEQsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87ZUFDcEYsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUM3QjtRQUNELHFCQUFJLElBQVksQ0FBQztRQUNqQixxQkFBSSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztRQUM5QixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNwQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUMzQztRQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUMvQixHQUFHLENBQUMsQ0FBQyx1QkFBTSxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUMvQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2hGO2FBQ0Y7U0FDRjtRQUNELE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1lBQ3RCLE9BQU8sRUFBRSxPQUFPO1NBQ2pCLENBQUMsQ0FBQztRQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDOUIsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDZixFQUFFLENBQUMsQ0FBQyxRQUFRLFlBQVksWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDckMsdUJBQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN6RCxHQUFHLENBQUMsQ0FBQyx1QkFBTSxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQzFDLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNyQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3FCQUMvRztpQkFDRjthQUNGO1lBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztTQUNqQixDQUFDLENBQ0gsQ0FBQztLQUNIOzs7WUEvQ0YsVUFBVTs7Ozs0Q0FJTixNQUFNLFNBQUMsWUFBWTt3Q0FDbkIsUUFBUSxZQUFJLE1BQU0sU0FBQyw0QkFBNEI7WUFQM0MsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIEh0dHBSZXF1ZXN0LFxuICBIdHRwSGFuZGxlcixcbiAgSHR0cEV2ZW50LFxuICBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXNwb25zZVxufSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsIERhdGFOb3JtYWxpemVyIH0gZnJvbSAnLi4vcmVzcG9uc2UvZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEFwaVByb2Nlc3NvciB9IGZyb20gJy4vYXBpLnByb2Nlc3Nvcic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBcGlJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZyxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMpIHByaXZhdGUgcmVhZG9ubHkgbm9ybWFsaXplcnM6IERhdGFOb3JtYWxpemVyW10sXG4gICAgcHJpdmF0ZSByZWFkb25seSBwcm9jZXNzb3I6IEFwaVByb2Nlc3NvclxuICApIHtcbiAgICBpZiAoIUFycmF5LmlzQXJyYXkodGhpcy5ub3JtYWxpemVycykpIHtcbiAgICAgIHRoaXMubm9ybWFsaXplcnMgPSBbXTtcbiAgICB9XG4gIH1cblxuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgaWYgKHJlcXVlc3QudXJsLmluZGV4T2YodGhpcy5jb25maWcuYmFzZVVybCkgIT09IDAgfHwgcmVxdWVzdC51cmwgPT09IHRoaXMuY29uZmlnLmF1dGhVcmxcbiAgICAgIHx8IChBcnJheS5pc0FycmF5KHRoaXMuY29uZmlnLmV4Y2x1ZGVkVXJscykgJiYgdGhpcy5jb25maWcuZXhjbHVkZWRVcmxzLmluZGV4T2YocmVxdWVzdC51cmwpICE9PSAtMSkpIHtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgbGV0IHR5cGU6IHN0cmluZztcbiAgICBsZXQgaGVhZGVycyA9IHJlcXVlc3QuaGVhZGVycztcbiAgICBpZiAoaGVhZGVycy5oYXMoJ1gtQW5SZXN0LVR5cGUnKSkge1xuICAgICAgdHlwZSA9IGhlYWRlcnMuZ2V0KCdYLUFuUmVzdC1UeXBlJyk7XG4gICAgICBoZWFkZXJzID0gaGVhZGVycy5kZWxldGUoJ3gtQW5SZXN0LVR5cGUnKTtcbiAgICB9XG4gICAgaWYgKHRoaXMuY29uZmlnLmRlZmF1bHRIZWFkZXJzKSB7XG4gICAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiB0aGlzLmNvbmZpZy5kZWZhdWx0SGVhZGVycykge1xuICAgICAgICBpZiAoIWhlYWRlcnMuaGFzKGhlYWRlci5uYW1lKSB8fCBoZWFkZXIuYXBwZW5kKSB7XG4gICAgICAgICAgaGVhZGVycyA9IGhlYWRlcnNbaGVhZGVyLmFwcGVuZCA/ICdhcHBlbmQnIDogJ3NldCddKGhlYWRlci5uYW1lLCBoZWFkZXIudmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcbiAgICAgIGhlYWRlcnM6IGhlYWRlcnNcbiAgICB9KTtcbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCkucGlwZShcbiAgICAgIG1hcCgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlIGluc3RhbmNlb2YgSHR0cFJlc3BvbnNlKSB7XG4gICAgICAgICAgY29uc3QgY29udGVudFR5cGUgPSByZXNwb25zZS5oZWFkZXJzLmdldCgnQ29udGVudC1UeXBlJyk7XG4gICAgICAgICAgZm9yIChjb25zdCBub3JtYWxpemVyIG9mIHRoaXMubm9ybWFsaXplcnMpIHtcbiAgICAgICAgICAgIGlmIChub3JtYWxpemVyLnN1cHBvcnRzKGNvbnRlbnRUeXBlKSkge1xuICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuY2xvbmUoeyBib2R5OiB0aGlzLnByb2Nlc3Nvci5wcm9jZXNzKG5vcm1hbGl6ZXIubm9ybWFsaXplKHJlc3BvbnNlLCB0eXBlKSwgcmVxdWVzdC5tZXRob2QpIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICB9KVxuICAgICk7XG4gIH1cbn1cbiJdfQ==