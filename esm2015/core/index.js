/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
export { AnRestConfig } from './config';
export { Collection } from './collection';
export { ApiService } from './api.service';
export { EntityInfo, CollectionInfo, DataInfoService } from './data-info.service';
export { ReferenceInterceptor } from './reference.interceptor';
export { ApiInterceptor } from './api.interceptor';
export { ApiProcessor } from './api.processor';
export { ObjectCollector } from './object-collector';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNvcmUvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLDZCQUFjLFVBQVUsQ0FBQztBQUN6QiwyQkFBYyxjQUFjLENBQUM7QUFDN0IsMkJBQWMsZUFBZSxDQUFDO0FBQzlCLDREQUFjLHFCQUFxQixDQUFDO0FBQ3BDLHFDQUFjLHlCQUF5QixDQUFDO0FBQ3hDLCtCQUFjLG1CQUFtQixDQUFDO0FBQ2xDLDZCQUFjLGlCQUFpQixDQUFDO0FBQ2hDLGdDQUFjLG9CQUFvQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0ICogZnJvbSAnLi9jb25maWcnO1xuZXhwb3J0ICogZnJvbSAnLi9jb2xsZWN0aW9uJztcbmV4cG9ydCAqIGZyb20gJy4vYXBpLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL3JlZmVyZW5jZS5pbnRlcmNlcHRvcic7XG5leHBvcnQgKiBmcm9tICcuL2FwaS5pbnRlcmNlcHRvcic7XG5leHBvcnQgKiBmcm9tICcuL2FwaS5wcm9jZXNzb3InO1xuZXhwb3J0ICogZnJvbSAnLi9vYmplY3QtY29sbGVjdG9yJztcbiJdfQ==