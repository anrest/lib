/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
export class EntityInfo {
    /**
     * @param {?} _path
     */
    constructor(_path) {
        this._path = _path;
    }
    /**
     * @return {?}
     */
    get path() {
        return this._path;
    }
}
function EntityInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    EntityInfo.prototype._path;
}
export class CollectionInfo {
    /**
     * @param {?} _path
     */
    constructor(_path) {
        this._path = _path;
    }
    /**
     * @return {?}
     */
    get path() {
        return this._path;
    }
}
function CollectionInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    CollectionInfo.prototype.total;
    /** @type {?} */
    CollectionInfo.prototype.page;
    /** @type {?} */
    CollectionInfo.prototype.lastPage;
    /** @type {?} */
    CollectionInfo.prototype.first;
    /** @type {?} */
    CollectionInfo.prototype.previous;
    /** @type {?} */
    CollectionInfo.prototype.next;
    /** @type {?} */
    CollectionInfo.prototype.last;
    /** @type {?} */
    CollectionInfo.prototype.alias;
    /** @type {?} */
    CollectionInfo.prototype.type;
    /** @type {?} */
    CollectionInfo.prototype._path;
}
export class DataInfoService {
    /**
     * @param {?} object
     * @return {?}
     */
    get(object) {
        return Reflect.getMetadata('anrest:info', object);
    }
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    set(object, info) {
        Reflect.defineMetadata('anrest:info', info, object);
    }
    /**
     * @param {?} object
     * @return {?}
     */
    getForCollection(object) {
        return /** @type {?} */ (this.get(object));
    }
    /**
     * @param {?} object
     * @return {?}
     */
    getForEntity(object) {
        return /** @type {?} */ (this.get(object));
    }
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    setForCollection(object, info) {
        this.set(object, info);
    }
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    setForEntity(object, info) {
        this.set(object, info);
    }
}
DataInfoService.decorators = [
    { type: Injectable },
];
function DataInfoService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    DataInfoService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    DataInfoService.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1pbmZvLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNvcmUvZGF0YS1pbmZvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsTUFBTTs7OztJQUVKLFlBQW9CLEtBQUs7UUFBTCxVQUFLLEdBQUwsS0FBSyxDQUFBO0tBQUk7Ozs7SUFFN0IsSUFBSSxJQUFJO1FBQ04sTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDbkI7Q0FDRjs7Ozs7QUFFRCxNQUFNOzs7O0lBV0osWUFBb0IsS0FBSztRQUFMLFVBQUssR0FBTCxLQUFLLENBQUE7S0FBSTs7OztJQUU3QixJQUFJLElBQUk7UUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztLQUNuQjtDQUNGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdELE1BQU07Ozs7O0lBRUosR0FBRyxDQUFDLE1BQU07UUFDUixNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUM7S0FDbkQ7Ozs7OztJQUVELEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBUztRQUNuQixPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7S0FDckQ7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsTUFBTTtRQUNyQixNQUFNLG1CQUFpQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFDO0tBQ3pDOzs7OztJQUVELFlBQVksQ0FBQyxNQUFNO1FBQ2pCLE1BQU0sbUJBQWEsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBQztLQUNyQzs7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLElBQW9CO1FBQzNDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3hCOzs7Ozs7SUFFRCxZQUFZLENBQUMsTUFBTSxFQUFFLElBQWdCO1FBQ25DLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3hCOzs7WUF6QkYsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGNsYXNzIEVudGl0eUluZm8ge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX3BhdGgpIHt9XG5cbiAgZ2V0IHBhdGgoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3BhdGg7XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIENvbGxlY3Rpb25JbmZvIHtcbiAgcHVibGljIHRvdGFsOiBudW1iZXI7XG4gIHB1YmxpYyBwYWdlOiBudW1iZXI7XG4gIHB1YmxpYyBsYXN0UGFnZTogbnVtYmVyO1xuICBwdWJsaWMgZmlyc3Q6IHN0cmluZztcbiAgcHVibGljIHByZXZpb3VzOiBzdHJpbmc7XG4gIHB1YmxpYyBuZXh0OiBzdHJpbmc7XG4gIHB1YmxpYyBsYXN0OiBzdHJpbmc7XG4gIHB1YmxpYyBhbGlhczogc3RyaW5nO1xuICBwdWJsaWMgdHlwZTogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX3BhdGgpIHt9XG5cbiAgZ2V0IHBhdGgoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3BhdGg7XG4gIH1cbn1cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIERhdGFJbmZvU2VydmljZSB7XG5cbiAgZ2V0KG9iamVjdCk6IGFueSB7XG4gICAgcmV0dXJuIFJlZmxlY3QuZ2V0TWV0YWRhdGEoJ2FucmVzdDppbmZvJywgb2JqZWN0KTtcbiAgfVxuXG4gIHNldChvYmplY3QsIGluZm86IGFueSkge1xuICAgIFJlZmxlY3QuZGVmaW5lTWV0YWRhdGEoJ2FucmVzdDppbmZvJywgaW5mbywgb2JqZWN0KTtcbiAgfVxuXG4gIGdldEZvckNvbGxlY3Rpb24ob2JqZWN0KTogQ29sbGVjdGlvbkluZm8ge1xuICAgIHJldHVybiA8Q29sbGVjdGlvbkluZm8+dGhpcy5nZXQob2JqZWN0KTtcbiAgfVxuXG4gIGdldEZvckVudGl0eShvYmplY3QpOiBFbnRpdHlJbmZvIHtcbiAgICByZXR1cm4gPEVudGl0eUluZm8+dGhpcy5nZXQob2JqZWN0KTtcbiAgfVxuXG4gIHNldEZvckNvbGxlY3Rpb24ob2JqZWN0LCBpbmZvOiBDb2xsZWN0aW9uSW5mbykge1xuICAgIHRoaXMuc2V0KG9iamVjdCwgaW5mbyk7XG4gIH1cblxuICBzZXRGb3JFbnRpdHkob2JqZWN0LCBpbmZvOiBFbnRpdHlJbmZvKSB7XG4gICAgdGhpcy5zZXQob2JqZWN0LCBpbmZvKTtcbiAgfVxufVxuIl19