/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { DataInfoService } from './data-info.service';
import { AfterGetEvent, AfterRemoveEvent, AfterSaveEvent, EventsService, BeforeGetEvent, BeforeRemoveEvent, BeforeSaveEvent } from '../events';
import { AnRestConfig } from './config';
import { MetaService } from '../meta';
export class ApiService {
    /**
     * @param {?} http
     * @param {?} events
     * @param {?} infoService
     * @param {?} config
     */
    constructor(http, events, infoService, config) {
        this.http = http;
        this.events = events;
        this.infoService = infoService;
        this.config = config;
        this.meta = MetaService.get(this.constructor);
    }
    /**
     * @param {?=} filter
     * @return {?}
     */
    getList(filter) {
        return this.doGet(this.meta.path, filter);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        return this.doGet(this.meta.path + '/' + id);
    }
    /**
     * @param {?} entity
     * @return {?}
     */
    reload(entity) {
        return this.doGet(this.infoService.getForEntity(entity).path);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getReference(id) {
        return this.doGet('&' + this.meta.path + '/' + id + '#' + this.meta.entityMeta.name);
    }
    /**
     * @param {?} path
     * @param {?=} filter
     * @return {?}
     */
    doGet(path, filter) {
        let /** @type {?} */ f;
        if (filter instanceof HttpParams) {
            f = filter;
        }
        else {
            f = new HttpParams();
            for (const /** @type {?} */ key in filter) {
                f = f.set(key, typeof filter[key] === 'boolean' ? (filter[key] ? 'true' : 'false') : String(filter[key]));
            }
        }
        this.events.handle(new BeforeGetEvent(path, this.meta.entityMeta.type, f));
        return this.http.get(this.config.baseUrl + path, { headers: this.meta.entityMeta.getHeaders(), params: f }).pipe(tap((data) => this.events.handle(new AfterGetEvent(data, this.meta.entityMeta.type))));
    }
    /**
     * @param {?} entity
     * @return {?}
     */
    save(entity) {
        this.events.handle(new BeforeSaveEvent(entity));
        let /** @type {?} */ body = {};
        if (this.meta.entityMeta.body) {
            body = this.meta.entityMeta.body(entity);
        }
        else {
            this.meta.entityMeta.getPropertiesForSave().forEach((property) => body[property] = entity[property]);
        }
        let /** @type {?} */ meta = this.infoService.getForEntity(entity);
        return this.http[meta ? 'put' : 'post'](meta ? this.config.baseUrl + meta.path : this.config.baseUrl + this.meta.path, body, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap((data) => {
            meta = this.infoService.getForEntity(data);
            if (!meta) {
                this.infoService.setForEntity(entity, meta);
            }
        }), tap(() => this.events.handle(new AfterSaveEvent(entity))));
    }
    /**
     * @param {?} entity
     * @return {?}
     */
    remove(entity) {
        this.events.handle(new BeforeRemoveEvent(entity));
        const /** @type {?} */ meta = this.infoService.getForEntity(entity);
        if (meta) {
            return this.http.delete(this.config.baseUrl + meta.path, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap(() => this.events.handle(new AfterRemoveEvent(entity))));
        }
    }
}
ApiService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ApiService.ctorParameters = () => [
    { type: HttpClient, },
    { type: EventsService, },
    { type: DataInfoService, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];
function ApiService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ApiService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ApiService.ctorParameters;
    /** @type {?} */
    ApiService.prototype.meta;
    /** @type {?} */
    ApiService.prototype.http;
    /** @type {?} */
    ApiService.prototype.events;
    /** @type {?} */
    ApiService.prototype.infoService;
    /** @type {?} */
    ApiService.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNvcmUvYXBpLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFOUQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQ0wsYUFBYSxFQUNiLGdCQUFnQixFQUNoQixjQUFjLEVBQ2QsYUFBYSxFQUNiLGNBQWMsRUFDZCxpQkFBaUIsRUFDakIsZUFBZSxFQUNoQixNQUFNLFdBQVcsQ0FBQztBQUNuQixPQUFPLEVBQUUsWUFBWSxFQUFhLE1BQU0sVUFBVSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxXQUFXLEVBQW1CLE1BQU0sU0FBUyxDQUFDO0FBR3ZELE1BQU07Ozs7Ozs7SUFHSixZQUNZLElBQWdCLEVBQ2hCLE1BQXFCLEVBQ3JCLFdBQTRCLEVBQ047UUFIdEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLGdCQUFXLEdBQVgsV0FBVyxDQUFpQjtRQUNOLFdBQU0sR0FBTixNQUFNO1FBRXRDLElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDL0M7Ozs7O0lBRUQsT0FBTyxDQUFDLE1BQThEO1FBQ3BFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0tBQzNDOzs7OztJQUVELEdBQUcsQ0FBQyxFQUFtQjtRQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUM7S0FDOUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7UUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDL0Q7Ozs7O0lBRUQsWUFBWSxDQUFDLEVBQW1CO1FBQzlCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUN0Rjs7Ozs7O0lBRUQsS0FBSyxDQUFDLElBQVksRUFBRSxNQUE4RDtRQUNoRixxQkFBSSxDQUFhLENBQUM7UUFDbEIsRUFBRSxDQUFDLENBQUMsTUFBTSxZQUFZLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDakMsQ0FBQyxHQUFHLE1BQU0sQ0FBQztTQUNaO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixDQUFDLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztZQUNyQixHQUFHLENBQUMsQ0FBQyx1QkFBTSxHQUFHLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDekIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzNHO1NBQ0Y7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQzVHLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxhQUFhLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FDdEYsQ0FBQztLQUNIOzs7OztJQUVELElBQUksQ0FBQyxNQUFXO1FBQ2QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNoRCxxQkFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUM5QixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1NBQ3RHO1FBQ0QscUJBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRWpELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FDbkMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFDN0UsSUFBSSxFQUNKLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUNuRCxDQUFDLElBQUksQ0FDSixHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNYLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQzdDO1NBQ0YsQ0FBQyxFQUNGLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQzFELENBQUM7S0FDTDs7Ozs7SUFFRCxNQUFNLENBQUMsTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDbEQsdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25ELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FDL0csR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUM1RCxDQUFDO1NBQ0g7S0FDRjs7O1lBOUVGLFVBQVU7Ozs7WUFoQkYsVUFBVTtZQVFqQixhQUFhO1lBTE4sZUFBZTs0Q0FxQm5CLE1BQU0sU0FBQyxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQge1xuICBBZnRlckdldEV2ZW50LFxuICBBZnRlclJlbW92ZUV2ZW50LFxuICBBZnRlclNhdmVFdmVudCxcbiAgRXZlbnRzU2VydmljZSxcbiAgQmVmb3JlR2V0RXZlbnQsXG4gIEJlZm9yZVJlbW92ZUV2ZW50LFxuICBCZWZvcmVTYXZlRXZlbnRcbn0gZnJvbSAnLi4vZXZlbnRzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UsIFNlcnZpY2VNZXRhSW5mbyB9IGZyb20gJy4uL21ldGEnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXBpU2VydmljZSB7XG4gIHByb3RlY3RlZCBtZXRhOiBTZXJ2aWNlTWV0YUluZm87XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJvdGVjdGVkIGV2ZW50czogRXZlbnRzU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgaW5mb1NlcnZpY2U6IERhdGFJbmZvU2VydmljZSxcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge1xuICAgIHRoaXMubWV0YSA9IE1ldGFTZXJ2aWNlLmdldCh0aGlzLmNvbnN0cnVjdG9yKTtcbiAgfVxuXG4gIGdldExpc3QoZmlsdGVyPzogSHR0cFBhcmFtc3x7IFtpbmRleDogc3RyaW5nXTogc3RyaW5nfG51bWJlcnxib29sZWFuIH0pOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KHRoaXMubWV0YS5wYXRoLCBmaWx0ZXIpO1xuICB9XG5cbiAgZ2V0KGlkOiBudW1iZXIgfCBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KHRoaXMubWV0YS5wYXRoICsgJy8nICsgaWQpO1xuICB9XG5cbiAgcmVsb2FkKGVudGl0eTogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5kb0dldCh0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShlbnRpdHkpLnBhdGgpO1xuICB9XG5cbiAgZ2V0UmVmZXJlbmNlKGlkOiBudW1iZXIgfCBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KCcmJyArIHRoaXMubWV0YS5wYXRoICsgJy8nICsgaWQgKyAnIycgKyB0aGlzLm1ldGEuZW50aXR5TWV0YS5uYW1lKTtcbiAgfVxuXG4gIGRvR2V0KHBhdGg6IHN0cmluZywgZmlsdGVyPzogSHR0cFBhcmFtc3x7IFtpbmRleDogc3RyaW5nXTogc3RyaW5nfG51bWJlcnxib29sZWFuIH0pIHtcbiAgICBsZXQgZjogSHR0cFBhcmFtcztcbiAgICBpZiAoZmlsdGVyIGluc3RhbmNlb2YgSHR0cFBhcmFtcykge1xuICAgICAgZiA9IGZpbHRlcjtcbiAgICB9IGVsc2Uge1xuICAgICAgZiA9IG5ldyBIdHRwUGFyYW1zKCk7XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBmaWx0ZXIpIHtcbiAgICAgICAgZiA9IGYuc2V0KGtleSwgdHlwZW9mIGZpbHRlcltrZXldID09PSAnYm9vbGVhbicgPyAoZmlsdGVyW2tleV0gPyAndHJ1ZScgOiAnZmFsc2UnKSA6IFN0cmluZyhmaWx0ZXJba2V5XSkpO1xuICAgICAgfVxuICAgIH1cbiAgICB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEJlZm9yZUdldEV2ZW50KHBhdGgsIHRoaXMubWV0YS5lbnRpdHlNZXRhLnR5cGUsIGYpKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLmNvbmZpZy5iYXNlVXJsICsgcGF0aCwge2hlYWRlcnM6IHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldEhlYWRlcnMoKSwgcGFyYW1zOiBmfSkucGlwZShcbiAgICAgIHRhcCgoZGF0YSkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlckdldEV2ZW50KGRhdGEsIHRoaXMubWV0YS5lbnRpdHlNZXRhLnR5cGUpKSlcbiAgICApO1xuICB9XG5cbiAgc2F2ZShlbnRpdHk6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBCZWZvcmVTYXZlRXZlbnQoZW50aXR5KSk7XG4gICAgbGV0IGJvZHkgPSB7fTtcbiAgICBpZiAodGhpcy5tZXRhLmVudGl0eU1ldGEuYm9keSkge1xuICAgICAgYm9keSA9IHRoaXMubWV0YS5lbnRpdHlNZXRhLmJvZHkoZW50aXR5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5tZXRhLmVudGl0eU1ldGEuZ2V0UHJvcGVydGllc0ZvclNhdmUoKS5mb3JFYWNoKChwcm9wZXJ0eSkgPT4gYm9keVtwcm9wZXJ0eV0gPSBlbnRpdHlbcHJvcGVydHldKTtcbiAgICB9XG4gICAgbGV0IG1ldGEgPSB0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShlbnRpdHkpO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cFttZXRhID8gJ3B1dCcgOiAncG9zdCddKFxuICAgICAgICBtZXRhID8gdGhpcy5jb25maWcuYmFzZVVybCArIG1ldGEucGF0aCA6IHRoaXMuY29uZmlnLmJhc2VVcmwgKyB0aGlzLm1ldGEucGF0aCxcbiAgICAgICAgYm9keSxcbiAgICAgICAge2hlYWRlcnM6IHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldEhlYWRlcnMoZW50aXR5KX1cbiAgICAgICkucGlwZShcbiAgICAgICAgdGFwKChkYXRhKSA9PiB7XG4gICAgICAgICAgbWV0YSA9IHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGRhdGEpO1xuICAgICAgICAgIGlmICghbWV0YSkge1xuICAgICAgICAgICAgdGhpcy5pbmZvU2VydmljZS5zZXRGb3JFbnRpdHkoZW50aXR5LCBtZXRhKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pLFxuICAgICAgICB0YXAoKCkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlclNhdmVFdmVudChlbnRpdHkpKSlcbiAgICAgICk7XG4gIH1cblxuICByZW1vdmUoZW50aXR5OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQmVmb3JlUmVtb3ZlRXZlbnQoZW50aXR5KSk7XG4gICAgY29uc3QgbWV0YSA9IHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGVudGl0eSk7XG4gICAgaWYgKG1ldGEpIHtcbiAgICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlKHRoaXMuY29uZmlnLmJhc2VVcmwgKyBtZXRhLnBhdGgsIHtoZWFkZXJzOiB0aGlzLm1ldGEuZW50aXR5TWV0YS5nZXRIZWFkZXJzKGVudGl0eSl9KS5waXBlKFxuICAgICAgICB0YXAoKCkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlclJlbW92ZUV2ZW50KGVudGl0eSkpKVxuICAgICAgKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==