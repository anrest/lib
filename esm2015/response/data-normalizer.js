/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/**
 * @record
 */
export function DataNormalizer() { }
function DataNormalizer_tsickle_Closure_declarations() {
    /** @type {?} */
    DataNormalizer.prototype.normalize;
    /** @type {?} */
    DataNormalizer.prototype.supports;
}
export let /** @type {?} */ ANREST_HTTP_DATA_NORMALIZERS = new InjectionToken('anrest.http_data_normalizers');

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1ub3JtYWxpemVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJyZXNwb25zZS9kYXRhLW5vcm1hbGl6ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7Ozs7O0FBUS9DLE1BQU0sQ0FBQyxxQkFBSSw0QkFBNEIsR0FBRyxJQUFJLGNBQWMsQ0FBbUIsOEJBQThCLENBQUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuL3Jlc3BvbnNlJztcblxuZXhwb3J0IGludGVyZmFjZSBEYXRhTm9ybWFsaXplciB7XG4gIG5vcm1hbGl6ZShyZXNwb25zZTogSHR0cFJlc3BvbnNlPGFueT4sIHR5cGU/OiBzdHJpbmcpOiBSZXNwb25zZU5vZGU7XG4gIHN1cHBvcnRzKHR5cGU6IHN0cmluZyk6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBsZXQgQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyA9IG5ldyBJbmplY3Rpb25Ub2tlbjxEYXRhTm9ybWFsaXplcltdPignYW5yZXN0Lmh0dHBfZGF0YV9ub3JtYWxpemVycycpO1xuIl19