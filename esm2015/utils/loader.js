/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { BehaviorSubject, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, mergeMap, share, shareReplay, tap } from 'rxjs/operators';
/**
 * @abstract
 */
export class Loader {
    /**
     * @param {?} service
     * @param {?=} waitFor
     * @param {?=} replay
     */
    constructor(service, waitFor = 0, replay = 0) {
        this.service = service;
        this.fields = {};
        this.loading = false;
        this.observable = combineLatest(this.getSubjects())
            .pipe(replay ? shareReplay(replay) : share(), debounceTime(waitFor), distinctUntilChanged((d1, d2) => this.checkDistinct(d1, d2)), map((values) => this.mapParams(values)), mergeMap((data) => {
            this.loading = true;
            return this.getServiceObservable(data).pipe(tap((v) => {
                this.lastData = v;
                this.loading = false;
            }));
        }));
    }
    /**
     * @param {?} name
     * @return {?}
     */
    field(name) {
        return this.fields[name];
    }
    /**
     * @return {?}
     */
    get data() {
        return this.observable;
    }
    /**
     * @return {?}
     */
    get isLoading() {
        return this.loading;
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    getServiceObservable([data]) {
        return this.service.getList(data);
    }
    /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    checkDistinct(d1, d2) {
        const /** @type {?} */ offset = d1.length - this.getAvailableFields().length;
        for (let /** @type {?} */ i = d1.length; i >= 0; i--) {
            if ((i < offset && d2[i]) || (i >= offset && d1[i] !== d2[i])) {
                if (i >= offset) {
                    d2.fill(undefined, 0, offset);
                }
                return false;
            }
        }
        return true;
    }
    /**
     * @param {?} values
     * @return {?}
     */
    mapParams(values) {
        const /** @type {?} */ fields = this.getAvailableFields();
        const /** @type {?} */ offset = values.length - fields.length;
        const /** @type {?} */ data = {};
        values.slice(offset).forEach((v, i) => {
            if (v !== undefined && v !== null && v !== '') {
                if (typeof v === 'boolean') {
                    v = v ? 'true' : 'false';
                }
                data[fields[i]] = String(v);
            }
        });
        return values.slice(0, offset).concat([data]);
    }
    /**
     * @return {?}
     */
    getSubjects() {
        return this.getAvailableFields().map((name) => {
            return this.fields[name] = new BehaviorSubject(undefined);
        });
    }
}
function Loader_tsickle_Closure_declarations() {
    /** @type {?} */
    Loader.prototype.fields;
    /** @type {?} */
    Loader.prototype.observable;
    /** @type {?} */
    Loader.prototype.loading;
    /** @type {?} */
    Loader.prototype.lastData;
    /** @type {?} */
    Loader.prototype.service;
    /**
     * @abstract
     * @return {?}
     */
    Loader.prototype.getAvailableFields = function () { };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJ1dGlscy9sb2FkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxZQUFZLEVBQUUsb0JBQW9CLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBSTVHLE1BQU07Ozs7OztJQVNKLFlBQXNCLE9BQW1CLEVBQUUsT0FBTyxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsQ0FBQztRQUE1QyxZQUFPLEdBQVAsT0FBTyxDQUFZO3NCQVBmLEVBQUU7dUJBRVYsS0FBSztRQU1yQixJQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDaEQsSUFBSSxDQUNILE1BQU0sQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFDdEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxFQUNyQixvQkFBb0IsQ0FBQyxDQUFDLEVBQVMsRUFBRSxFQUFTLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQzFFLEdBQUcsQ0FBQyxDQUFDLE1BQWEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUM5QyxRQUFRLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNwQixNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FDekMsR0FBRyxDQUFDLENBQUMsQ0FBa0IsRUFBRSxFQUFFO2dCQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEIsQ0FBQyxDQUNILENBQUM7U0FDSCxDQUFDLENBQ0gsQ0FBQztLQUNMOzs7OztJQUVELEtBQUssQ0FBQyxJQUFJO1FBQ1IsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDMUI7Ozs7SUFFRCxJQUFJLElBQUk7UUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztLQUN4Qjs7OztJQUVELElBQUksU0FBUztRQUNYLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO0tBQ3JCOzs7OztJQUVTLG9CQUFvQixDQUFDLENBQUMsSUFBSSxDQUFRO1FBQzFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNuQzs7Ozs7O0lBRVMsYUFBYSxDQUFDLEVBQVMsRUFBRSxFQUFTO1FBQzFDLHVCQUFNLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLE1BQU0sQ0FBQztRQUM1RCxHQUFHLENBQUMsQ0FBQyxxQkFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDcEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLE1BQU0sSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5RCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDaEIsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2lCQUMvQjtnQkFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO2FBQ2Q7U0FDRjtRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7S0FDYjs7Ozs7SUFFUyxTQUFTLENBQUMsTUFBYTtRQUMvQix1QkFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDekMsdUJBQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM3Qyx1QkFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxTQUFTLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDOUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDM0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7aUJBQzFCO2dCQUNELElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDN0I7U0FDRixDQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUMvQzs7OztJQUVTLFdBQVc7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQzVDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksZUFBZSxDQUFNLFNBQVMsQ0FBQyxDQUFDO1NBQ2hFLENBQUMsQ0FBQztLQUNKO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIGNvbWJpbmVMYXRlc3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGRlYm91bmNlVGltZSwgZGlzdGluY3RVbnRpbENoYW5nZWQsIG1hcCwgbWVyZ2VNYXAsIHNoYXJlLCBzaGFyZVJlcGxheSwgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXBpU2VydmljZSB9IGZyb20gJy4uL2NvcmUnO1xuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4uL2NvcmUvY29sbGVjdGlvbic7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBMb2FkZXIge1xuXG4gIHByaXZhdGUgcmVhZG9ubHkgZmllbGRzID0ge307XG4gIHByaXZhdGUgcmVhZG9ubHkgb2JzZXJ2YWJsZTogT2JzZXJ2YWJsZTxhbnk+O1xuICBwcml2YXRlIGxvYWRpbmcgPSBmYWxzZTtcbiAgcHJvdGVjdGVkIGxhc3REYXRhOiBDb2xsZWN0aW9uPGFueT47XG5cbiAgYWJzdHJhY3QgZ2V0QXZhaWxhYmxlRmllbGRzKCk6IHN0cmluZ1tdO1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBzZXJ2aWNlOiBBcGlTZXJ2aWNlLCB3YWl0Rm9yID0gMCwgcmVwbGF5ID0gMCkge1xuICAgIHRoaXMub2JzZXJ2YWJsZSA9IGNvbWJpbmVMYXRlc3QodGhpcy5nZXRTdWJqZWN0cygpKVxuICAgICAgLnBpcGUoXG4gICAgICAgIHJlcGxheSA/IHNoYXJlUmVwbGF5KHJlcGxheSkgOiBzaGFyZSgpLFxuICAgICAgICBkZWJvdW5jZVRpbWUod2FpdEZvciksXG4gICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKChkMTogYW55W10sIGQyOiBhbnlbXSkgPT4gdGhpcy5jaGVja0Rpc3RpbmN0KGQxLCBkMikpLFxuICAgICAgICBtYXAoKHZhbHVlczogYW55W10pID0+IHRoaXMubWFwUGFyYW1zKHZhbHVlcykpLFxuICAgICAgICBtZXJnZU1hcCgoZGF0YSkgPT4ge1xuICAgICAgICAgIHRoaXMubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U2VydmljZU9ic2VydmFibGUoZGF0YSkucGlwZShcbiAgICAgICAgICAgIHRhcCgodjogQ29sbGVjdGlvbjxhbnk+KSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMubGFzdERhdGEgPSB2O1xuICAgICAgICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgKTtcbiAgICAgICAgfSksXG4gICAgICApO1xuICB9XG5cbiAgZmllbGQobmFtZSk6IEJlaGF2aW9yU3ViamVjdDxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5maWVsZHNbbmFtZV07XG4gIH1cblxuICBnZXQgZGF0YSgpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLm9ic2VydmFibGU7XG4gIH1cblxuICBnZXQgaXNMb2FkaW5nKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmxvYWRpbmc7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U2VydmljZU9ic2VydmFibGUoW2RhdGFdOiBhbnlbXSkge1xuICAgIHJldHVybiB0aGlzLnNlcnZpY2UuZ2V0TGlzdChkYXRhKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBjaGVja0Rpc3RpbmN0KGQxOiBhbnlbXSwgZDI6IGFueVtdKSB7XG4gICAgY29uc3Qgb2Zmc2V0ID0gZDEubGVuZ3RoIC0gdGhpcy5nZXRBdmFpbGFibGVGaWVsZHMoKS5sZW5ndGg7XG4gICAgZm9yIChsZXQgaSA9IGQxLmxlbmd0aDsgaSA+PSAwOyBpLS0pIHtcbiAgICAgIGlmICgoaSA8IG9mZnNldCAmJiBkMltpXSkgfHwgKGkgPj0gb2Zmc2V0ICYmIGQxW2ldICE9PSBkMltpXSkpIHtcbiAgICAgICAgaWYgKGkgPj0gb2Zmc2V0KSB7XG4gICAgICAgICAgZDIuZmlsbCh1bmRlZmluZWQsIDAsIG9mZnNldCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHByb3RlY3RlZCBtYXBQYXJhbXModmFsdWVzOiBhbnlbXSkge1xuICAgIGNvbnN0IGZpZWxkcyA9IHRoaXMuZ2V0QXZhaWxhYmxlRmllbGRzKCk7XG4gICAgY29uc3Qgb2Zmc2V0ID0gdmFsdWVzLmxlbmd0aCAtIGZpZWxkcy5sZW5ndGg7XG4gICAgY29uc3QgZGF0YSA9IHt9O1xuICAgIHZhbHVlcy5zbGljZShvZmZzZXQpLmZvckVhY2goKHYsIGkpID0+IHtcbiAgICAgIGlmICh2ICE9PSB1bmRlZmluZWQgJiYgdiAhPT0gbnVsbCAmJiB2ICE9PSAnJykge1xuICAgICAgICBpZiAodHlwZW9mIHYgPT09ICdib29sZWFuJykge1xuICAgICAgICAgIHYgPSB2ID8gJ3RydWUnIDogJ2ZhbHNlJztcbiAgICAgICAgfVxuICAgICAgICBkYXRhW2ZpZWxkc1tpXV0gPSBTdHJpbmcodik7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIHZhbHVlcy5zbGljZSgwLCBvZmZzZXQpLmNvbmNhdChbZGF0YV0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFN1YmplY3RzKCk6IEJlaGF2aW9yU3ViamVjdDxhbnk+W10ge1xuICAgIHJldHVybiB0aGlzLmdldEF2YWlsYWJsZUZpZWxkcygpLm1hcCgobmFtZSkgPT4ge1xuICAgICAgcmV0dXJuIHRoaXMuZmllbGRzW25hbWVdID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KHVuZGVmaW5lZCk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==