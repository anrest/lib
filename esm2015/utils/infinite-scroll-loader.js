/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { BehaviorSubject } from 'rxjs';
import { Loader } from './loader';
/**
 * @abstract
 */
export class InfiniteScrollLoader extends Loader {
    /**
     * @return {?}
     */
    loadMore() {
        this.loadMoreSubject.next(true);
    }
    /**
     * @return {?}
     */
    hasMore() {
        return this.lastData && this.lastData.hasMore();
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    getServiceObservable([loadMore, data]) {
        return loadMore ? this.lastData.loadMore() : super.getServiceObservable([data]);
    }
    /**
     * @return {?}
     */
    getSubjects() {
        this.loadMoreSubject = new BehaviorSubject(false);
        const /** @type {?} */ subjects = super.getSubjects();
        subjects.unshift(this.loadMoreSubject);
        return subjects;
    }
}
function InfiniteScrollLoader_tsickle_Closure_declarations() {
    /** @type {?} */
    InfiniteScrollLoader.prototype.loadMoreSubject;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5maW5pdGUtc2Nyb2xsLWxvYWRlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsidXRpbHMvaW5maW5pdGUtc2Nyb2xsLWxvYWRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGVBQWUsRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUNuRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sVUFBVSxDQUFDOzs7O0FBRWxDLE1BQU0sMkJBQXFDLFNBQVEsTUFBTTs7OztJQUl2RCxRQUFRO1FBQ04sSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDakM7Ozs7SUFFRCxPQUFPO1FBQ0wsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUNqRDs7Ozs7SUFFUyxvQkFBb0IsQ0FBQyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQVE7UUFDcEQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUNqRjs7OztJQUVTLFdBQVc7UUFDbkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLGVBQWUsQ0FBVSxLQUFLLENBQUMsQ0FBQztRQUMzRCx1QkFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3ZDLE1BQU0sQ0FBQyxRQUFRLENBQUM7S0FDakI7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgTG9hZGVyIH0gZnJvbSAnLi9sb2FkZXInO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgSW5maW5pdGVTY3JvbGxMb2FkZXIgZXh0ZW5kcyBMb2FkZXIge1xuXG4gIHByaXZhdGUgbG9hZE1vcmVTdWJqZWN0O1xuXG4gIGxvYWRNb3JlKCkge1xuICAgIHRoaXMubG9hZE1vcmVTdWJqZWN0Lm5leHQodHJ1ZSk7XG4gIH1cblxuICBoYXNNb3JlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzTW9yZSgpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFNlcnZpY2VPYnNlcnZhYmxlKFtsb2FkTW9yZSwgZGF0YV06IGFueVtdKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gbG9hZE1vcmUgPyB0aGlzLmxhc3REYXRhLmxvYWRNb3JlKCkgOiBzdXBlci5nZXRTZXJ2aWNlT2JzZXJ2YWJsZShbZGF0YV0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFN1YmplY3RzKCk6IEJlaGF2aW9yU3ViamVjdDxhbnk+W10ge1xuICAgIHRoaXMubG9hZE1vcmVTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gICAgY29uc3Qgc3ViamVjdHMgPSBzdXBlci5nZXRTdWJqZWN0cygpO1xuICAgIHN1YmplY3RzLnVuc2hpZnQodGhpcy5sb2FkTW9yZVN1YmplY3QpO1xuICAgIHJldHVybiBzdWJqZWN0cztcbiAgfVxufVxuIl19