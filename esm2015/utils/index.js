/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
export { Loader } from './loader';
export { InfiniteScrollLoader } from './infinite-scroll-loader';
export { PageLoader } from './page-loader';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbInV0aWxzL2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSx1QkFBYyxVQUFVLENBQUM7QUFDekIscUNBQWMsMEJBQTBCLENBQUM7QUFDekMsMkJBQWMsZUFBZSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0ICogZnJvbSAnLi9sb2FkZXInO1xuZXhwb3J0ICogZnJvbSAnLi9pbmZpbml0ZS1zY3JvbGwtbG9hZGVyJztcbmV4cG9ydCAqIGZyb20gJy4vcGFnZS1sb2FkZXInO1xuIl19