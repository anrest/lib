/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { BehaviorSubject } from 'rxjs';
import { Loader } from './loader';
/**
 * @abstract
 */
export class PageLoader extends Loader {
    /**
     * @return {?}
     */
    first() {
        if (!this.lastData || !this.lastData.hasFirst()) {
            return;
        }
        this.pageSubject.next(this.lastData.firstPath());
    }
    /**
     * @return {?}
     */
    previous() {
        if (!this.lastData || !this.lastData.hasPrevious()) {
            return;
        }
        this.pageSubject.next(this.lastData.previousPath());
    }
    /**
     * @return {?}
     */
    next() {
        if (!this.lastData || !this.lastData.hasNext()) {
            return;
        }
        this.pageSubject.next(this.lastData.nextPath());
    }
    /**
     * @return {?}
     */
    last() {
        if (!this.lastData || !this.lastData.hasLast()) {
            return;
        }
        this.pageSubject.next(this.lastData.lastPath());
    }
    /**
     * @return {?}
     */
    hasFirst() {
        return this.lastData && this.lastData.hasFirst();
    }
    /**
     * @return {?}
     */
    hasPrevious() {
        return this.lastData && this.lastData.hasPrevious();
    }
    /**
     * @return {?}
     */
    hasNext() {
        return this.lastData && this.lastData.hasNext();
    }
    /**
     * @return {?}
     */
    hasLast() {
        return this.lastData && this.lastData.hasLast();
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    getServiceObservable([page, data]) {
        return page ? this.service.doGet(page, data) : super.getServiceObservable([data]);
    }
    /**
     * @return {?}
     */
    getSubjects() {
        this.pageSubject = new BehaviorSubject(undefined);
        const /** @type {?} */ subjects = super.getSubjects();
        subjects.unshift(this.pageSubject);
        return subjects;
    }
}
function PageLoader_tsickle_Closure_declarations() {
    /** @type {?} */
    PageLoader.prototype.pageSubject;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS1sb2FkZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbInV0aWxzL3BhZ2UtbG9hZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsZUFBZSxFQUE2QixNQUFNLE1BQU0sQ0FBQztBQUlsRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sVUFBVSxDQUFDOzs7O0FBRWxDLE1BQU0saUJBQTJCLFNBQVEsTUFBTTs7OztJQUk3QyxLQUFLO1FBQ0gsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDaEQsTUFBTSxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7S0FDbEQ7Ozs7SUFFRCxRQUFRO1FBQ04sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbkQsTUFBTSxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7S0FDckQ7Ozs7SUFFRCxJQUFJO1FBQ0YsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDL0MsTUFBTSxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7S0FDakQ7Ozs7SUFFRCxJQUFJO1FBQ0YsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDL0MsTUFBTSxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7S0FDakQ7Ozs7SUFFRCxRQUFRO1FBQ04sTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztLQUNsRDs7OztJQUVELFdBQVc7UUFDVCxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO0tBQ3JEOzs7O0lBRUQsT0FBTztRQUNMLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDakQ7Ozs7SUFFRCxPQUFPO1FBQ0wsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztLQUNqRDs7Ozs7SUFFUyxvQkFBb0IsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQVE7UUFDaEQsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0tBQ25GOzs7O0lBRVMsV0FBVztRQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksZUFBZSxDQUFTLFNBQVMsQ0FBQyxDQUFDO1FBQzFELHVCQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDckMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkMsTUFBTSxDQUFDLFFBQVEsQ0FBQztLQUNqQjtDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBjb21iaW5lTGF0ZXN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUsIGRpc3RpbmN0VW50aWxDaGFuZ2VkLCBtYXAsIG1lcmdlTWFwLCBzaGFyZSwgc2hhcmVSZXBsYXksIHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFwaVNlcnZpY2UgfSBmcm9tICcuLi9jb3JlJztcbmltcG9ydCB7IENvbGxlY3Rpb24gfSBmcm9tICcuLi9jb3JlL2NvbGxlY3Rpb24nO1xuaW1wb3J0IHsgTG9hZGVyIH0gZnJvbSAnLi9sb2FkZXInO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgUGFnZUxvYWRlciBleHRlbmRzIExvYWRlciB7XG5cbiAgcHJpdmF0ZSBwYWdlU3ViamVjdDtcblxuICBmaXJzdCgpIHtcbiAgICBpZiAoIXRoaXMubGFzdERhdGEgfHwgIXRoaXMubGFzdERhdGEuaGFzRmlyc3QoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnBhZ2VTdWJqZWN0Lm5leHQodGhpcy5sYXN0RGF0YS5maXJzdFBhdGgoKSk7XG4gIH1cblxuICBwcmV2aW91cygpIHtcbiAgICBpZiAoIXRoaXMubGFzdERhdGEgfHwgIXRoaXMubGFzdERhdGEuaGFzUHJldmlvdXMoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnBhZ2VTdWJqZWN0Lm5leHQodGhpcy5sYXN0RGF0YS5wcmV2aW91c1BhdGgoKSk7XG4gIH1cblxuICBuZXh0KCkge1xuICAgIGlmICghdGhpcy5sYXN0RGF0YSB8fCAhdGhpcy5sYXN0RGF0YS5oYXNOZXh0KCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5wYWdlU3ViamVjdC5uZXh0KHRoaXMubGFzdERhdGEubmV4dFBhdGgoKSk7XG4gIH1cblxuICBsYXN0KCkge1xuICAgIGlmICghdGhpcy5sYXN0RGF0YSB8fCAhdGhpcy5sYXN0RGF0YS5oYXNMYXN0KCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5wYWdlU3ViamVjdC5uZXh0KHRoaXMubGFzdERhdGEubGFzdFBhdGgoKSk7XG4gIH1cblxuICBoYXNGaXJzdCgpIHtcbiAgICByZXR1cm4gdGhpcy5sYXN0RGF0YSAmJiB0aGlzLmxhc3REYXRhLmhhc0ZpcnN0KCk7XG4gIH1cblxuICBoYXNQcmV2aW91cygpIHtcbiAgICByZXR1cm4gdGhpcy5sYXN0RGF0YSAmJiB0aGlzLmxhc3REYXRhLmhhc1ByZXZpb3VzKCk7XG4gIH1cblxuICBoYXNOZXh0KCkge1xuICAgIHJldHVybiB0aGlzLmxhc3REYXRhICYmIHRoaXMubGFzdERhdGEuaGFzTmV4dCgpO1xuICB9XG5cbiAgaGFzTGFzdCgpIHtcbiAgICByZXR1cm4gdGhpcy5sYXN0RGF0YSAmJiB0aGlzLmxhc3REYXRhLmhhc0xhc3QoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTZXJ2aWNlT2JzZXJ2YWJsZShbcGFnZSwgZGF0YV06IGFueVtdKSB7XG4gICAgcmV0dXJuIHBhZ2UgPyB0aGlzLnNlcnZpY2UuZG9HZXQocGFnZSwgZGF0YSkgOiBzdXBlci5nZXRTZXJ2aWNlT2JzZXJ2YWJsZShbZGF0YV0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFN1YmplY3RzKCk6IEJlaGF2aW9yU3ViamVjdDxhbnk+W10ge1xuICAgIHRoaXMucGFnZVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PHN0cmluZz4odW5kZWZpbmVkKTtcbiAgICBjb25zdCBzdWJqZWN0cyA9IHN1cGVyLmdldFN1YmplY3RzKCk7XG4gICAgc3ViamVjdHMudW5zaGlmdCh0aGlzLnBhZ2VTdWJqZWN0KTtcbiAgICByZXR1cm4gc3ViamVjdHM7XG4gIH1cbn1cbiJdfQ==