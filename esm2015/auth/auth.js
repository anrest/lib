/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */
export function Auth() { }
function Auth_tsickle_Closure_declarations() {
    /** @type {?} */
    Auth.prototype.getToken;
    /** @type {?} */
    Auth.prototype.setToken;
    /** @type {?} */
    Auth.prototype.remove;
    /** @type {?} */
    Auth.prototype.isSignedIn;
    /** @type {?} */
    Auth.prototype.getInfo;
    /** @type {?} */
    Auth.prototype.signIn;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYXV0aC9hdXRoLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQXV0aCB7XG4gIGdldFRva2VuKCk6IHN0cmluZztcbiAgc2V0VG9rZW4odG9rZW46IHN0cmluZyk7XG4gIHJlbW92ZSgpO1xuICBpc1NpZ25lZEluKCk6IGJvb2xlYW47XG4gIGdldEluZm8oKTogYW55O1xuICBzaWduSW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55Pjtcbn1cbiJdfQ==