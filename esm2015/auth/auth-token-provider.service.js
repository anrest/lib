/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
export class AuthTokenProviderService {
}
function AuthTokenProviderService_tsickle_Closure_declarations() {
    /**
     * @abstract
     * @return {?}
     */
    AuthTokenProviderService.prototype.get = function () { };
    /**
     * @abstract
     * @param {?} token
     * @return {?}
     */
    AuthTokenProviderService.prototype.set = function (token) { };
    /**
     * @abstract
     * @return {?}
     */
    AuthTokenProviderService.prototype.remove = function () { };
    /**
     * @abstract
     * @return {?}
     */
    AuthTokenProviderService.prototype.isSet = function () { };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC10b2tlbi1wcm92aWRlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJhdXRoL2F1dGgtdG9rZW4tcHJvdmlkZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBRUEsTUFBTTtDQUtMIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXV0aFRva2VuIH0gZnJvbSAnLi9hdXRoLXRva2VuJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEF1dGhUb2tlblByb3ZpZGVyU2VydmljZSBpbXBsZW1lbnRzIEF1dGhUb2tlbntcbiAgYWJzdHJhY3QgZ2V0KCk6IHN0cmluZztcbiAgYWJzdHJhY3Qgc2V0KHRva2VuOiBzdHJpbmcpO1xuICBhYnN0cmFjdCByZW1vdmUoKTtcbiAgYWJzdHJhY3QgaXNTZXQoKTogYm9vbGVhbjtcbn1cbiJdfQ==