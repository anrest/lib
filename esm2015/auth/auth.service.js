/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
export class AuthService {
}
function AuthService_tsickle_Closure_declarations() {
    /**
     * @abstract
     * @return {?}
     */
    AuthService.prototype.getInfo = function () { };
    /**
     * @abstract
     * @return {?}
     */
    AuthService.prototype.getToken = function () { };
    /**
     * @abstract
     * @return {?}
     */
    AuthService.prototype.isSignedIn = function () { };
    /**
     * @abstract
     * @return {?}
     */
    AuthService.prototype.remove = function () { };
    /**
     * @abstract
     * @param {?} token
     * @return {?}
     */
    AuthService.prototype.setToken = function (token) { };
    /**
     * @abstract
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    AuthService.prototype.signIn = function (username, password) { };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJhdXRoL2F1dGguc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBR0EsTUFBTTtDQWFMIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgQXV0aCB9IGZyb20gJy4vYXV0aCc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBdXRoU2VydmljZSBpbXBsZW1lbnRzIEF1dGgge1xuICBhYnN0cmFjdCBnZXRJbmZvKCk6IGFueTtcblxuICBhYnN0cmFjdCBnZXRUb2tlbigpOiBzdHJpbmc7XG5cbiAgYWJzdHJhY3QgaXNTaWduZWRJbigpOiBib29sZWFuO1xuXG4gIGFic3RyYWN0IHJlbW92ZSgpO1xuXG4gIGFic3RyYWN0IHNldFRva2VuKHRva2VuOiBzdHJpbmcpO1xuXG4gIGFic3RyYWN0IHNpZ25Jbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+O1xuXG59XG4iXX0=