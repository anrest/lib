/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
export class LocalStorageAuthTokenProviderService {
    constructor() {
        this.tokenName = 'anrest:auth_token';
    }
    /**
     * @return {?}
     */
    get() {
        return localStorage.getItem(this.tokenName);
    }
    /**
     * @param {?} token
     * @return {?}
     */
    set(token) {
        localStorage.setItem(this.tokenName, token);
    }
    /**
     * @return {?}
     */
    remove() {
        localStorage.removeItem(this.tokenName);
    }
    /**
     * @return {?}
     */
    isSet() {
        return localStorage.getItem(this.tokenName) != null;
    }
}
LocalStorageAuthTokenProviderService.decorators = [
    { type: Injectable },
];
function LocalStorageAuthTokenProviderService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    LocalStorageAuthTokenProviderService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    LocalStorageAuthTokenProviderService.ctorParameters;
    /** @type {?} */
    LocalStorageAuthTokenProviderService.prototype.tokenName;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc3RvcmFnZS1hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImF1dGgvbG9jYWwtc3RvcmFnZS1hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0MsTUFBTTs7eUJBQ2dCLG1CQUFtQjs7Ozs7SUFFdkMsR0FBRztRQUNELE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUM3Qzs7Ozs7SUFFRCxHQUFHLENBQUMsS0FBYTtRQUNmLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztLQUM3Qzs7OztJQUVELE1BQU07UUFDSixZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUN6Qzs7OztJQUVELEtBQUs7UUFDSCxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDO0tBQ3JEOzs7WUFsQkYsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEF1dGhUb2tlbiB9IGZyb20gJy4vYXV0aC10b2tlbic7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UgaW1wbGVtZW50cyBBdXRoVG9rZW4ge1xuICBwcml2YXRlIHRva2VuTmFtZSA9ICdhbnJlc3Q6YXV0aF90b2tlbic7XG5cbiAgZ2V0KCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5OYW1lKTtcbiAgfVxuXG4gIHNldCh0b2tlbjogc3RyaW5nKSB7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy50b2tlbk5hbWUsIHRva2VuKTtcbiAgfVxuXG4gIHJlbW92ZSgpIHtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuTmFtZSk7XG4gIH1cblxuICBpc1NldCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlbk5hbWUpICE9IG51bGw7XG4gIH1cbn1cbiJdfQ==