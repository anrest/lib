/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import { AnRestConfig } from '../core/config';
import { AuthTokenProviderService } from './auth-token-provider.service';
export class JwtAuthService {
    /**
     * @param {?} http
     * @param {?} token
     * @param {?} config
     */
    constructor(http, token, config) {
        this.http = http;
        this.token = token;
        this.config = config;
    }
    /**
     * @return {?}
     */
    getToken() {
        return this.token.get();
    }
    /**
     * @param {?} token
     * @return {?}
     */
    setToken(token) {
        this.token.set(token);
    }
    /**
     * @return {?}
     */
    remove() {
        this.token.remove();
    }
    /**
     * @return {?}
     */
    isSignedIn() {
        return this.token.isSet();
    }
    /**
     * @return {?}
     */
    getInfo() {
        return jwt_decode.call(this.getToken());
    }
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    signIn(username, password) {
        return this.http.post(this.config.authUrl, {
            login: username,
            password: password
        }).pipe(tap((data) => {
            this.setToken(data.token);
        }));
    }
}
JwtAuthService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
JwtAuthService.ctorParameters = () => [
    { type: HttpClient, },
    { type: AuthTokenProviderService, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];
function JwtAuthService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    JwtAuthService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    JwtAuthService.ctorParameters;
    /** @type {?} */
    JwtAuthService.prototype.http;
    /** @type {?} */
    JwtAuthService.prototype.token;
    /** @type {?} */
    JwtAuthService.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LWF1dGguc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYXV0aC9qd3QtYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sS0FBSyxVQUFVLE1BQU0sWUFBWSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUd6RCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUd6RSxNQUFNOzs7Ozs7SUFFSixZQUNVLE1BQ0EsT0FDd0I7UUFGeEIsU0FBSSxHQUFKLElBQUk7UUFDSixVQUFLLEdBQUwsS0FBSztRQUNtQixXQUFNLEdBQU4sTUFBTTtLQUNwQzs7OztJQUVKLFFBQVE7UUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztLQUN6Qjs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBYTtRQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUN2Qjs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO0tBQ3JCOzs7O0lBRUQsVUFBVTtRQUNSLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO0tBQzNCOzs7O0lBRUQsT0FBTztRQUNMLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0tBQ3pDOzs7Ozs7SUFFRCxNQUFNLENBQUMsUUFBZ0IsRUFBRSxRQUFnQjtRQUN2QyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDekMsS0FBSyxFQUFFLFFBQVE7WUFDZixRQUFRLEVBQUUsUUFBUTtTQUNuQixDQUFDLENBQUMsSUFBSSxDQUNMLEdBQUcsQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzNCLENBQUMsQ0FDSCxDQUFDO0tBQ0g7OztZQXRDRixVQUFVOzs7O1lBUkYsVUFBVTtZQU1WLHdCQUF3Qjs0Q0FRNUIsTUFBTSxTQUFDLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgKiBhcyBqd3RfZGVjb2RlIGZyb20gJ2p3dC1kZWNvZGUnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBBdXRoIH0gZnJvbSAnLi9hdXRoJztcbmltcG9ydCB7IEF1dGhUb2tlblByb3ZpZGVyU2VydmljZSB9IGZyb20gJy4vYXV0aC10b2tlbi1wcm92aWRlci5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEp3dEF1dGhTZXJ2aWNlIGltcGxlbWVudHMgQXV0aCB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByaXZhdGUgdG9rZW46IEF1dGhUb2tlblByb3ZpZGVyU2VydmljZSxcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge31cblxuICBnZXRUb2tlbigpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLnRva2VuLmdldCgpO1xuICB9XG5cbiAgc2V0VG9rZW4odG9rZW46IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMudG9rZW4uc2V0KHRva2VuKTtcbiAgfVxuXG4gIHJlbW92ZSgpOiB2b2lkIHtcbiAgICB0aGlzLnRva2VuLnJlbW92ZSgpO1xuICB9XG5cbiAgaXNTaWduZWRJbigpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy50b2tlbi5pc1NldCgpO1xuICB9XG5cbiAgZ2V0SW5mbygpOiBhbnkge1xuICAgIHJldHVybiBqd3RfZGVjb2RlLmNhbGwodGhpcy5nZXRUb2tlbigpKTtcbiAgfVxuXG4gIHNpZ25Jbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWcuYXV0aFVybCwge1xuICAgICAgbG9naW46IHVzZXJuYW1lLFxuICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkXG4gICAgfSkucGlwZShcbiAgICAgIHRhcCgoZGF0YTogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuc2V0VG9rZW4oZGF0YS50b2tlbik7XG4gICAgICB9KVxuICAgICk7XG4gIH1cblxufVxuIl19