/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { HttpHeaders } from '@angular/common/http';
export class MetaInfo {
    /**
     * @param {?} _type
     */
    constructor(_type) {
        this._type = _type;
    }
    /**
     * @return {?}
     */
    get type() {
        return this._type;
    }
}
function MetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    MetaInfo.prototype._type;
}
export class PropertyMetaInfo extends MetaInfo {
}
function PropertyMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    PropertyMetaInfo.prototype.isCollection;
    /** @type {?} */
    PropertyMetaInfo.prototype.excludeWhenSaving;
}
export class EventListenerMetaInfo extends MetaInfo {
    constructor() {
        super(...arguments);
        this.events = [];
    }
}
function EventListenerMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    EventListenerMetaInfo.prototype.events;
}
export class EntityMetaInfo extends MetaInfo {
    constructor() {
        super(...arguments);
        this.id = 'id';
        this.headers = [];
        this.properties = {};
        this.subresources = {};
    }
    /**
     * @param {?=} object
     * @return {?}
     */
    getHeaders(object) {
        let /** @type {?} */ headers = new HttpHeaders();
        headers = headers.set('X-AnRest-Type', this.name);
        for (const /** @type {?} */ header of this.headers) {
            if (!header.dynamic || object) {
                headers = headers[header.append ? 'append' : 'set'](header.name, header.value.call(object));
            }
        }
        return headers;
    }
    /**
     * @return {?}
     */
    getPropertiesForSave() {
        const /** @type {?} */ included = [];
        for (const /** @type {?} */ property in this.properties) {
            if (!this.properties[property].excludeWhenSaving) {
                included.push(property);
            }
        }
        return included;
    }
}
function EntityMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    EntityMetaInfo.prototype.name;
    /** @type {?} */
    EntityMetaInfo.prototype.id;
    /** @type {?} */
    EntityMetaInfo.prototype.service;
    /** @type {?} */
    EntityMetaInfo.prototype.headers;
    /** @type {?} */
    EntityMetaInfo.prototype.properties;
    /** @type {?} */
    EntityMetaInfo.prototype.subresources;
    /** @type {?} */
    EntityMetaInfo.prototype.body;
    /** @type {?} */
    EntityMetaInfo.prototype.cache;
}
export class ServiceMetaInfo extends MetaInfo {
    /**
     * @param {?} type
     * @param {?} _entityMeta
     */
    constructor(type, _entityMeta) {
        super(type);
        this._entityMeta = _entityMeta;
    }
    /**
     * @return {?}
     */
    get entityMeta() {
        return this._entityMeta;
    }
}
function ServiceMetaInfo_tsickle_Closure_declarations() {
    /** @type {?} */
    ServiceMetaInfo.prototype.path;
    /** @type {?} */
    ServiceMetaInfo.prototype._entityMeta;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS1pbmZvLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJtZXRhL21ldGEtaW5mby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRW5ELE1BQU07Ozs7SUFFSixZQUFvQixLQUFlO1FBQWYsVUFBSyxHQUFMLEtBQUssQ0FBVTtLQUFJOzs7O0lBRXZDLElBQUksSUFBSTtRQUNOLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ25CO0NBQ0Y7Ozs7O0FBRUQsTUFBTSx1QkFBd0IsU0FBUSxRQUFRO0NBRzdDOzs7Ozs7O0FBRUQsTUFBTSw0QkFBNkIsU0FBUSxRQUFROzs7c0JBQ08sRUFBRTs7Q0FDM0Q7Ozs7O0FBRUQsTUFBTSxxQkFBc0IsU0FBUSxRQUFROzs7a0JBRTlCLElBQUk7dUJBRTZFLEVBQUU7MEJBQ3BDLEVBQUU7NEJBQ3FCLEVBQUU7Ozs7OztJQUk3RSxVQUFVLENBQUMsTUFBWTtRQUM1QixxQkFBSSxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNoQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xELEdBQUcsQ0FBQyxDQUFDLHVCQUFNLE1BQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNsQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUM3RjtTQUNGO1FBQ0QsTUFBTSxDQUFDLE9BQU8sQ0FBQzs7Ozs7SUFHVixvQkFBb0I7UUFDekIsdUJBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNwQixHQUFHLENBQUMsQ0FBQyx1QkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDdkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDakQsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN6QjtTQUNGO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQzs7Q0FFbkI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFRCxNQUFNLHNCQUF1QixTQUFRLFFBQVE7Ozs7O0lBRzNDLFlBQVksSUFBYyxFQUFVLFdBQTJCO1FBQzdELEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQURzQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7S0FFOUQ7Ozs7SUFFRCxJQUFJLFVBQVU7UUFDWixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztLQUN6QjtDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbmV4cG9ydCBjbGFzcyBNZXRhSW5mbyB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfdHlwZTogRnVuY3Rpb24pIHt9XG5cbiAgZ2V0IHR5cGUoKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5fdHlwZTtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgUHJvcGVydHlNZXRhSW5mbyBleHRlbmRzIE1ldGFJbmZvIHtcbiAgcHVibGljIGlzQ29sbGVjdGlvbjogYm9vbGVhbjtcbiAgcHVibGljIGV4Y2x1ZGVXaGVuU2F2aW5nOiBib29sZWFuO1xufVxuXG5leHBvcnQgY2xhc3MgRXZlbnRMaXN0ZW5lck1ldGFJbmZvIGV4dGVuZHMgTWV0YUluZm8ge1xuICBwdWJsaWMgZXZlbnRzOiB7IGVudGl0eTogRnVuY3Rpb24sIHR5cGU6IEZ1bmN0aW9uIH1bXSA9IFtdO1xufVxuXG5leHBvcnQgY2xhc3MgRW50aXR5TWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBuYW1lOiBzdHJpbmc7XG4gIHB1YmxpYyBpZCA9ICdpZCc7XG4gIHB1YmxpYyBzZXJ2aWNlOiBGdW5jdGlvbjtcbiAgcHVibGljIGhlYWRlcnM6IHsgbmFtZTogc3RyaW5nLCB2YWx1ZTogKCkgPT4gc3RyaW5nLCBhcHBlbmQ6IGJvb2xlYW4sIGR5bmFtaWM6IGJvb2xlYW4gfVtdID0gW107XG4gIHB1YmxpYyBwcm9wZXJ0aWVzOiB7IFtpbmRleDogc3RyaW5nXTogUHJvcGVydHlNZXRhSW5mbyB9ID0ge307XG4gIHB1YmxpYyBzdWJyZXNvdXJjZXM6IHsgW2luZGV4OiBzdHJpbmddOiB7IG1ldGE6IEVudGl0eU1ldGFJbmZvLCBwYXRoOiBzdHJpbmd9IH0gPSB7fTtcbiAgcHVibGljIGJvZHk6IChvYmplY3Q6IGFueSkgPT4gc3RyaW5nO1xuICBwdWJsaWMgY2FjaGU6IEZ1bmN0aW9uO1xuXG4gIHB1YmxpYyBnZXRIZWFkZXJzKG9iamVjdD86IGFueSk6IEh0dHBIZWFkZXJzIHtcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIdHRwSGVhZGVycygpO1xuICAgIGhlYWRlcnMgPSBoZWFkZXJzLnNldCgnWC1BblJlc3QtVHlwZScsIHRoaXMubmFtZSk7XG4gICAgZm9yIChjb25zdCBoZWFkZXIgb2YgdGhpcy5oZWFkZXJzKSB7XG4gICAgICBpZiAoIWhlYWRlci5keW5hbWljIHx8IG9iamVjdCkge1xuICAgICAgICBoZWFkZXJzID0gaGVhZGVyc1toZWFkZXIuYXBwZW5kID8gJ2FwcGVuZCcgOiAnc2V0J10oaGVhZGVyLm5hbWUsIGhlYWRlci52YWx1ZS5jYWxsKG9iamVjdCkpO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gaGVhZGVycztcbiAgfVxuXG4gIHB1YmxpYyBnZXRQcm9wZXJ0aWVzRm9yU2F2ZSgpIHtcbiAgICBjb25zdCBpbmNsdWRlZCA9IFtdO1xuICAgIGZvciAoY29uc3QgcHJvcGVydHkgaW4gdGhpcy5wcm9wZXJ0aWVzKSB7XG4gICAgICBpZiAoIXRoaXMucHJvcGVydGllc1twcm9wZXJ0eV0uZXhjbHVkZVdoZW5TYXZpbmcpIHtcbiAgICAgICAgaW5jbHVkZWQucHVzaChwcm9wZXJ0eSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBpbmNsdWRlZDtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgU2VydmljZU1ldGFJbmZvIGV4dGVuZHMgTWV0YUluZm8ge1xuICBwdWJsaWMgcGF0aDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHR5cGU6IEZ1bmN0aW9uLCBwcml2YXRlIF9lbnRpdHlNZXRhOiBFbnRpdHlNZXRhSW5mbykge1xuICAgIHN1cGVyKHR5cGUpO1xuICB9XG5cbiAgZ2V0IGVudGl0eU1ldGEoKTogRW50aXR5TWV0YUluZm8ge1xuICAgIHJldHVybiB0aGlzLl9lbnRpdHlNZXRhO1xuICB9XG59XG4iXX0=