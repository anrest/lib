/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { EntityMetaInfo, PropertyMetaInfo, ServiceMetaInfo, EventListenerMetaInfo } from './meta-info';
export class MetaService {
    /**
     * @param {?} key
     * @return {?}
     */
    static get(key) {
        return MetaService.map.get(key);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    static getByName(key) {
        for (const /** @type {?} */ meta of MetaService.map.values()) {
            if (meta instanceof EntityMetaInfo && meta.name === key) {
                return meta;
            }
        }
    }
    /**
     * @param {?} key
     * @return {?}
     */
    static getOrCreateForEntity(key) {
        let /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EntityMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    static getOrCreateForEventListener(key) {
        let /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EventListenerMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    }
    /**
     * @param {?} key
     * @param {?} property
     * @param {?=} type
     * @return {?}
     */
    static getOrCreateForProperty(key, property, type) {
        const /** @type {?} */ entityMeta = MetaService.getOrCreateForEntity(key.constructor);
        if (!entityMeta.properties[property]) {
            entityMeta.properties[property] = new PropertyMetaInfo(type || Reflect.getMetadata('design:type', key, property));
        }
        return entityMeta.properties[property];
    }
    /**
     * @param {?} key
     * @param {?} entity
     * @return {?}
     */
    static getOrCreateForHttpService(key, entity) {
        let /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new ServiceMetaInfo(key, MetaService.getOrCreateForEntity(entity));
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    }
    /**
     * @param {?} meta
     * @return {?}
     */
    static set(meta) {
        MetaService.map.set(meta.type, meta);
    }
}
MetaService.map = new Map();
function MetaService_tsickle_Closure_declarations() {
    /** @type {?} */
    MetaService.map;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0YS1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJtZXRhL21ldGEtc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxnQkFBZ0IsRUFBWSxlQUFlLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFakgsTUFBTTs7Ozs7SUFJSixNQUFNLENBQUMsR0FBRyxDQUFDLEdBQVE7UUFDakIsTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0tBQ2pDOzs7OztJQUVELE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBVztRQUMxQixHQUFHLENBQUMsQ0FBQyx1QkFBTSxJQUFJLElBQUksV0FBVyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDNUMsRUFBRSxDQUFDLENBQUMsSUFBSSxZQUFZLGNBQWMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUM7YUFDYjtTQUNGO0tBQ0Y7Ozs7O0lBRUQsTUFBTSxDQUFDLG9CQUFvQixDQUFDLEdBQWE7UUFDdkMscUJBQUksSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1YsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9CLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCxNQUFNLG1CQUFpQixJQUFJLEVBQUM7S0FDN0I7Ozs7O0lBRUQsTUFBTSxDQUFDLDJCQUEyQixDQUFDLEdBQWE7UUFDOUMscUJBQUksSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1YsSUFBSSxHQUFHLElBQUkscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdEMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2QjtRQUNELE1BQU0sbUJBQXdCLElBQUksRUFBQztLQUNwQzs7Ozs7OztJQUVELE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxHQUFRLEVBQUUsUUFBZ0IsRUFBRSxJQUFVO1FBQ2xFLHVCQUFNLFVBQVUsR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JFLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLGdCQUFnQixDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUNuSDtRQUNELE1BQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ3hDOzs7Ozs7SUFFRCxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBYSxFQUFFLE1BQWdCO1FBQzlELHFCQUFJLElBQUksR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNWLElBQUksR0FBRyxJQUFJLGVBQWUsQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDMUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2QjtRQUNELE1BQU0sbUJBQWtCLElBQUksRUFBQztLQUM5Qjs7Ozs7SUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDLElBQWM7UUFDdkIsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztLQUN0Qzs7a0JBbkR3QyxJQUFJLEdBQUcsRUFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFbnRpdHlNZXRhSW5mbywgUHJvcGVydHlNZXRhSW5mbywgTWV0YUluZm8sIFNlcnZpY2VNZXRhSW5mbywgRXZlbnRMaXN0ZW5lck1ldGFJbmZvIH0gZnJvbSAnLi9tZXRhLWluZm8nO1xuXG5leHBvcnQgY2xhc3MgTWV0YVNlcnZpY2Uge1xuXG4gIHByaXZhdGUgc3RhdGljIG1hcDogTWFwPGFueSwgTWV0YUluZm8+ID0gbmV3IE1hcDxhbnksIE1ldGFJbmZvPigpO1xuXG4gIHN0YXRpYyBnZXQoa2V5OiBhbnkpOiBhbnkge1xuICAgIHJldHVybiBNZXRhU2VydmljZS5tYXAuZ2V0KGtleSk7XG4gIH1cblxuICBzdGF0aWMgZ2V0QnlOYW1lKGtleTogc3RyaW5nKTogYW55IHtcbiAgICBmb3IgKGNvbnN0IG1ldGEgb2YgTWV0YVNlcnZpY2UubWFwLnZhbHVlcygpKSB7XG4gICAgICBpZiAobWV0YSBpbnN0YW5jZW9mIEVudGl0eU1ldGFJbmZvICYmIG1ldGEubmFtZSA9PT0ga2V5KSB7XG4gICAgICAgIHJldHVybiBtZXRhO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBnZXRPckNyZWF0ZUZvckVudGl0eShrZXk6IEZ1bmN0aW9uKTogRW50aXR5TWV0YUluZm8ge1xuICAgIGxldCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0KGtleSk7XG4gICAgaWYgKCFtZXRhKSB7XG4gICAgICBtZXRhID0gbmV3IEVudGl0eU1ldGFJbmZvKGtleSk7XG4gICAgICBNZXRhU2VydmljZS5zZXQobWV0YSk7XG4gICAgfVxuICAgIHJldHVybiA8RW50aXR5TWV0YUluZm8+bWV0YTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRPckNyZWF0ZUZvckV2ZW50TGlzdGVuZXIoa2V5OiBGdW5jdGlvbik6IEV2ZW50TGlzdGVuZXJNZXRhSW5mbyB7XG4gICAgbGV0IG1ldGEgPSBNZXRhU2VydmljZS5nZXQoa2V5KTtcbiAgICBpZiAoIW1ldGEpIHtcbiAgICAgIG1ldGEgPSBuZXcgRXZlbnRMaXN0ZW5lck1ldGFJbmZvKGtleSk7XG4gICAgICBNZXRhU2VydmljZS5zZXQobWV0YSk7XG4gICAgfVxuICAgIHJldHVybiA8RXZlbnRMaXN0ZW5lck1ldGFJbmZvPm1ldGE7XG4gIH1cblxuICBzdGF0aWMgZ2V0T3JDcmVhdGVGb3JQcm9wZXJ0eShrZXk6IGFueSwgcHJvcGVydHk6IHN0cmluZywgdHlwZT86IGFueSk6IFByb3BlcnR5TWV0YUluZm8ge1xuICAgIGNvbnN0IGVudGl0eU1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eShrZXkuY29uc3RydWN0b3IpO1xuICAgIGlmICghZW50aXR5TWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XSkge1xuICAgICAgZW50aXR5TWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XSA9IG5ldyBQcm9wZXJ0eU1ldGFJbmZvKHR5cGUgfHwgUmVmbGVjdC5nZXRNZXRhZGF0YSgnZGVzaWduOnR5cGUnLCBrZXksIHByb3BlcnR5KSk7XG4gICAgfVxuICAgIHJldHVybiBlbnRpdHlNZXRhLnByb3BlcnRpZXNbcHJvcGVydHldO1xuICB9XG5cbiAgc3RhdGljIGdldE9yQ3JlYXRlRm9ySHR0cFNlcnZpY2Uoa2V5OiBGdW5jdGlvbiwgZW50aXR5OiBGdW5jdGlvbik6IFNlcnZpY2VNZXRhSW5mbyB7XG4gICAgbGV0IG1ldGEgPSBNZXRhU2VydmljZS5nZXQoa2V5KTtcbiAgICBpZiAoIW1ldGEpIHtcbiAgICAgIG1ldGEgPSBuZXcgU2VydmljZU1ldGFJbmZvKGtleSwgTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkoZW50aXR5KSk7XG4gICAgICBNZXRhU2VydmljZS5zZXQobWV0YSk7XG4gICAgfVxuICAgIHJldHVybiA8U2VydmljZU1ldGFJbmZvPm1ldGE7XG4gIH1cblxuICBzdGF0aWMgc2V0KG1ldGE6IE1ldGFJbmZvKSB7XG4gICAgTWV0YVNlcnZpY2UubWFwLnNldChtZXRhLnR5cGUsIG1ldGEpO1xuICB9XG59XG4iXX0=