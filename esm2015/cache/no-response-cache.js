/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { ResponseCache } from './response-cache';
import { Injectable } from '@angular/core';
export class NoResponseCache extends ResponseCache {
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        return false;
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    add(id, data) {
    }
    /**
     * @return {?}
     */
    clear() {
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
    }
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    addAlias(id, alias) {
    }
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    replace(data, id) {
    }
}
NoResponseCache.decorators = [
    { type: Injectable },
];
function NoResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    NoResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    NoResponseCache.ctorParameters;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm8tcmVzcG9uc2UtY2FjaGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNhY2hlL25vLXJlc3BvbnNlLWNhY2hlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxNQUFNLHNCQUF1QixTQUFRLGFBQWE7Ozs7O0lBQ2hELEdBQUcsQ0FBQyxFQUFVO1FBQ1osTUFBTSxDQUFDLEtBQUssQ0FBQztLQUNkOzs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBVSxFQUFFLElBQVM7S0FDeEI7Ozs7SUFFRCxLQUFLO0tBQ0o7Ozs7O0lBRUQsR0FBRyxDQUFDLEVBQVU7S0FDYjs7Ozs7SUFFRCxNQUFNLENBQUMsRUFBVTtLQUNoQjs7Ozs7O0lBRUQsUUFBUSxDQUFDLEVBQVUsRUFBRSxLQUFhO0tBQ2pDOzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBUyxFQUFFLEVBQVU7S0FDNUI7OztZQXRCRixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vcmVzcG9uc2UtY2FjaGUnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTm9SZXNwb25zZUNhY2hlIGV4dGVuZHMgUmVzcG9uc2VDYWNoZSB7XG4gIGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICB9XG5cbiAgY2xlYXIoKSB7XG4gIH1cblxuICBnZXQoaWQ6IHN0cmluZykge1xuICB9XG5cbiAgcmVtb3ZlKGlkOiBzdHJpbmcpIHtcbiAgfVxuXG4gIGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpIHtcbiAgfVxuXG4gIHJlcGxhY2UoZGF0YTogYW55LCBpZDogc3RyaW5nKTogdm9pZCB7XG4gIH1cbn1cbiJdfQ==