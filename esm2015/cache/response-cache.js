/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable, InjectionToken } from '@angular/core';
/**
 * @abstract
 */
export class ResponseCache {
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    replace(data, id) {
        this.remove(id);
        this.add(data, id);
    }
}
ResponseCache.decorators = [
    { type: Injectable },
];
function ResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    ResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    ResponseCache.ctorParameters;
    /**
     * @abstract
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.has = function (id) { };
    /**
     * @abstract
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.get = function (id) { };
    /**
     * @abstract
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    ResponseCache.prototype.add = function (id, data) { };
    /**
     * @abstract
     * @param {?} id
     * @return {?}
     */
    ResponseCache.prototype.remove = function (id) { };
    /**
     * @abstract
     * @return {?}
     */
    ResponseCache.prototype.clear = function () { };
    /**
     * @abstract
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    ResponseCache.prototype.addAlias = function (id, alias) { };
}
export let /** @type {?} */ ANREST_CACHE_SERVICES = new InjectionToken('anrest.cache_services');

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UtY2FjaGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNhY2hlL3Jlc3BvbnNlLWNhY2hlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7OztBQUczRCxNQUFNOzs7Ozs7SUFjSixPQUFPLENBQUMsSUFBUyxFQUFFLEVBQVU7UUFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztLQUNwQjs7O1lBbEJGLFVBQVU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXFCWCxNQUFNLENBQUMscUJBQUkscUJBQXFCLEdBQUcsSUFBSSxjQUFjLENBQWtCLHVCQUF1QixDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgUmVzcG9uc2VDYWNoZSB7XG5cbiAgYWJzdHJhY3QgaGFzKGlkOiBzdHJpbmcpOiBib29sZWFuO1xuXG4gIGFic3RyYWN0IGdldChpZDogc3RyaW5nKTtcblxuICBhYnN0cmFjdCBhZGQoaWQ6IHN0cmluZywgZGF0YTogYW55KTtcblxuICBhYnN0cmFjdCByZW1vdmUoaWQ6IHN0cmluZyk7XG5cbiAgYWJzdHJhY3QgY2xlYXIoKTtcblxuICBhYnN0cmFjdCBhZGRBbGlhcyhpZDogc3RyaW5nLCBhbGlhczogc3RyaW5nKTtcblxuICByZXBsYWNlKGRhdGE6IGFueSwgaWQ6IHN0cmluZykge1xuICAgIHRoaXMucmVtb3ZlKGlkKTtcbiAgICB0aGlzLmFkZChkYXRhLCBpZCk7XG4gIH1cbn1cblxuZXhwb3J0IGxldCBBTlJFU1RfQ0FDSEVfU0VSVklDRVMgPSBuZXcgSW5qZWN0aW9uVG9rZW48UmVzcG9uc2VDYWNoZVtdPignYW5yZXN0LmNhY2hlX3NlcnZpY2VzJyk7XG4iXX0=