/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AnRestConfig, DataInfoService } from '../core';
import { ANREST_CACHE_SERVICES } from './response-cache';
import { MetaService } from '../meta';
import { NoResponseCache } from './no-response-cache';
export class CacheInterceptor {
    /**
     * @param {?} config
     * @param {?} info
     * @param {?} cacheServices
     */
    constructor(config, info, cacheServices) {
        this.config = config;
        this.info = info;
        this.cacheServices = cacheServices;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        const /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        const /** @type {?} */ cacheType = MetaService.getByName(request.headers.get('X-AnRest-Type')).cache || this.config.cache || NoResponseCache;
        let /** @type {?} */ cache;
        for (const /** @type {?} */ cacheService of this.cacheServices) {
            if (cacheService instanceof cacheType) {
                cache = cacheService;
                break;
            }
        }
        if (request.method !== 'GET') {
            cache.remove(path);
            return next.handle(request);
        }
        if (cache.has(path)) {
            return of(new HttpResponse({
                body: cache.get(path),
                status: 200,
                url: request.url
            }));
        }
        return next.handle(request).pipe(tap((response) => {
            if (response.body) {
                const /** @type {?} */ info = this.info.get(response.body);
                if (info) {
                    cache.add(info.path, response.body);
                    if (info && info.alias) {
                        cache.addAlias(info.path, info.alias);
                    }
                }
            }
        }));
    }
}
CacheInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
CacheInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    { type: DataInfoService, },
    { type: Array, decorators: [{ type: Inject, args: [ANREST_CACHE_SERVICES,] },] },
];
function CacheInterceptor_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    CacheInterceptor.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    CacheInterceptor.ctorParameters;
    /** @type {?} */
    CacheInterceptor.prototype.config;
    /** @type {?} */
    CacheInterceptor.prototype.info;
    /** @type {?} */
    CacheInterceptor.prototype.cacheServices;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FjaGUuaW50ZXJjZXB0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImNhY2hlL2NhY2hlLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBSVksWUFBWSxFQUM5QixNQUFNLHNCQUFzQixDQUFDO0FBQzlCLE9BQU8sRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdEMsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxZQUFZLEVBQWEsZUFBZSxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxxQkFBcUIsRUFBaUIsTUFBTSxrQkFBa0IsQ0FBQztBQUN4RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUd0RCxNQUFNOzs7Ozs7SUFFSixZQUMyQyxRQUN4QixNQUMrQjtRQUZQLFdBQU0sR0FBTixNQUFNO1FBQzlCLFNBQUksR0FBSixJQUFJO1FBQzJCLGtCQUFhLEdBQWIsYUFBYTtLQUMzRDs7Ozs7O0lBRUosU0FBUyxDQUFDLE9BQXlCLEVBQUUsSUFBaUI7UUFDcEQsdUJBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPO2VBQ3BGLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkcsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDN0I7UUFDRCx1QkFBTSxTQUFTLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxlQUFlLENBQUM7UUFDNUgscUJBQUksS0FBVSxDQUFDO1FBQ2YsR0FBRyxDQUFDLENBQUMsdUJBQU0sWUFBWSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzlDLEVBQUUsQ0FBQyxDQUFDLFlBQVksWUFBWSxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxLQUFLLEdBQUcsWUFBWSxDQUFDO2dCQUNyQixLQUFLLENBQUM7YUFDUDtTQUNGO1FBQ0QsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzdCLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDN0I7UUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwQixNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksWUFBWSxDQUFDO2dCQUN6QixJQUFJLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQ3JCLE1BQU0sRUFBRSxHQUFHO2dCQUNYLEdBQUcsRUFBRSxPQUFPLENBQUMsR0FBRzthQUNqQixDQUFDLENBQUMsQ0FBQztTQUNMO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUM5QixHQUFHLENBQUMsQ0FBQyxRQUEyQixFQUFFLEVBQUU7WUFDbEMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLHVCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ1QsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO3dCQUN2QixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN2QztpQkFDRjthQUNGO1NBQ0YsQ0FBQyxDQUNILENBQUM7S0FDSDs7O1lBL0NGLFVBQVU7Ozs7NENBSU4sTUFBTSxTQUFDLFlBQVk7WUFUVSxlQUFlO3dDQVc1QyxNQUFNLFNBQUMscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1xuICBIdHRwUmVxdWVzdCxcbiAgSHR0cEhhbmRsZXIsXG4gIEh0dHBFdmVudCxcbiAgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVzcG9uc2Vcbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnLCBEYXRhSW5mb1NlcnZpY2UgfSBmcm9tICcuLi9jb3JlJztcbmltcG9ydCB7IEFOUkVTVF9DQUNIRV9TRVJWSUNFUywgUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vcmVzcG9uc2UtY2FjaGUnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCB7IE5vUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vbm8tcmVzcG9uc2UtY2FjaGUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ2FjaGVJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZyxcbiAgICBwcml2YXRlIHJlYWRvbmx5IGluZm86IERhdGFJbmZvU2VydmljZSxcbiAgICBASW5qZWN0KEFOUkVTVF9DQUNIRV9TRVJWSUNFUykgcHJpdmF0ZSByZWFkb25seSBjYWNoZVNlcnZpY2VzOiBSZXNwb25zZUNhY2hlW10sXG4gICkge31cblxuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgY29uc3QgcGF0aCA9IHJlcXVlc3QudXJsV2l0aFBhcmFtcy5zbGljZSh0aGlzLmNvbmZpZy5iYXNlVXJsLmxlbmd0aCk7XG4gICAgaWYgKHJlcXVlc3QudXJsLmluZGV4T2YodGhpcy5jb25maWcuYmFzZVVybCkgIT09IDAgfHwgcmVxdWVzdC51cmwgPT09IHRoaXMuY29uZmlnLmF1dGhVcmxcbiAgICAgIHx8IChBcnJheS5pc0FycmF5KHRoaXMuY29uZmlnLmV4Y2x1ZGVkVXJscykgJiYgdGhpcy5jb25maWcuZXhjbHVkZWRVcmxzLmluZGV4T2YocmVxdWVzdC51cmwpICE9PSAtMSkpIHtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgY29uc3QgY2FjaGVUeXBlID0gTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKHJlcXVlc3QuaGVhZGVycy5nZXQoJ1gtQW5SZXN0LVR5cGUnKSkuY2FjaGUgfHwgdGhpcy5jb25maWcuY2FjaGUgfHwgTm9SZXNwb25zZUNhY2hlO1xuICAgIGxldCBjYWNoZTogYW55O1xuICAgIGZvciAoY29uc3QgY2FjaGVTZXJ2aWNlIG9mIHRoaXMuY2FjaGVTZXJ2aWNlcykge1xuICAgICAgaWYgKGNhY2hlU2VydmljZSBpbnN0YW5jZW9mIGNhY2hlVHlwZSkge1xuICAgICAgICBjYWNoZSA9IGNhY2hlU2VydmljZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICAgIGlmIChyZXF1ZXN0Lm1ldGhvZCAhPT0gJ0dFVCcpIHtcbiAgICAgIGNhY2hlLnJlbW92ZShwYXRoKTtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgaWYgKGNhY2hlLmhhcyhwYXRoKSkge1xuICAgICAgcmV0dXJuIG9mKG5ldyBIdHRwUmVzcG9uc2Uoe1xuICAgICAgICBib2R5OiBjYWNoZS5nZXQocGF0aCksXG4gICAgICAgIHN0YXR1czogMjAwLFxuICAgICAgICB1cmw6IHJlcXVlc3QudXJsXG4gICAgICB9KSk7XG4gICAgfVxuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KS5waXBlKFxuICAgICAgdGFwKChyZXNwb25zZTogSHR0cFJlc3BvbnNlPGFueT4pID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkpIHtcbiAgICAgICAgICBjb25zdCBpbmZvID0gdGhpcy5pbmZvLmdldChyZXNwb25zZS5ib2R5KTtcbiAgICAgICAgICBpZiAoaW5mbykge1xuICAgICAgICAgICAgY2FjaGUuYWRkKGluZm8ucGF0aCwgcmVzcG9uc2UuYm9keSk7XG4gICAgICAgICAgICBpZiAoaW5mbyAmJiBpbmZvLmFsaWFzKSB7XG4gICAgICAgICAgICAgIGNhY2hlLmFkZEFsaWFzKGluZm8ucGF0aCwgaW5mby5hbGlhcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KVxuICAgICk7XG4gIH1cbn1cbiJdfQ==