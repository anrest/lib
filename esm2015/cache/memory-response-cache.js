/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { ResponseCache } from './response-cache';
import { Inject, Injectable } from '@angular/core';
import { ObjectCollector } from '../core/object-collector';
import { AnRestConfig } from '../core/config';
export class MemoryResponseCache extends ResponseCache {
    /**
     * @param {?} collector
     * @param {?} config
     */
    constructor(collector, config) {
        super();
        this.collector = collector;
        this.config = config;
        this.times = {};
        this.aliases = {};
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    add(id, data) {
        this.times[id] = Date.now();
    }
    /**
     * @return {?}
     */
    clear() {
        this.collector.clear();
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        return this.collector.get(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        this.collector.remove(id);
    }
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    addAlias(id, alias) {
        this.aliases[alias] = id;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        const /** @type {?} */ time = this.aliases[id] ? this.times[this.aliases[id]] : this.times[id];
        return time && (Date.now() <= this.config.cacheTTL + time);
    }
}
MemoryResponseCache.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MemoryResponseCache.ctorParameters = () => [
    { type: ObjectCollector, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];
function MemoryResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    MemoryResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    MemoryResponseCache.ctorParameters;
    /** @type {?} */
    MemoryResponseCache.prototype.times;
    /** @type {?} */
    MemoryResponseCache.prototype.aliases;
    /** @type {?} */
    MemoryResponseCache.prototype.collector;
    /** @type {?} */
    MemoryResponseCache.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVtb3J5LXJlc3BvbnNlLWNhY2hlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJjYWNoZS9tZW1vcnktcmVzcG9uc2UtY2FjaGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLFlBQVksRUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBR3pELE1BQU0sMEJBQTJCLFNBQVEsYUFBYTs7Ozs7SUFNcEQsWUFDbUIsV0FDd0I7UUFFekMsS0FBSyxFQUFFLENBQUM7UUFIUyxjQUFTLEdBQVQsU0FBUztRQUNlLFdBQU0sR0FBTixNQUFNO3FCQU5KLEVBQUU7dUJBRUEsRUFBRTtLQU9oRDs7Ozs7O0lBRUQsR0FBRyxDQUFDLEVBQVUsRUFBRSxJQUFTO1FBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQzdCOzs7O0lBRUQsS0FBSztRQUNILElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDeEI7Ozs7O0lBRUQsR0FBRyxDQUFDLEVBQVU7UUFDWixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDL0I7Ozs7O0lBRUQsTUFBTSxDQUFDLEVBQVU7UUFDZixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUMzQjs7Ozs7O0lBRUQsUUFBUSxDQUFDLEVBQVUsRUFBRSxLQUFhO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQzFCOzs7OztJQUVELEdBQUcsQ0FBQyxFQUFVO1FBQ1osdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzlFLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUM7S0FDNUQ7OztZQXJDRixVQUFVOzs7O1lBSEYsZUFBZTs0Q0FZbkIsTUFBTSxTQUFDLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9iamVjdENvbGxlY3RvciB9IGZyb20gJy4uL2NvcmUvb2JqZWN0LWNvbGxlY3Rvcic7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4uL2NvcmUvY29uZmlnJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE1lbW9yeVJlc3BvbnNlQ2FjaGUgZXh0ZW5kcyBSZXNwb25zZUNhY2hlIHtcblxuICBwcml2YXRlIHRpbWVzOiB7IFtpbmRleDogc3RyaW5nXTogbnVtYmVyIH0gPSB7fTtcblxuICBwcml2YXRlIGFsaWFzZXM6IHsgW2luZGV4OiBzdHJpbmddOiBzdHJpbmcgfSA9IHt9O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcmVhZG9ubHkgY29sbGVjdG9yOiBPYmplY3RDb2xsZWN0b3IsXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZ1xuICApIHtcbiAgICBzdXBlcigpO1xuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICAgIHRoaXMudGltZXNbaWRdID0gRGF0ZS5ub3coKTtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICAgIHRoaXMuY29sbGVjdG9yLmNsZWFyKCk7XG4gIH1cblxuICBnZXQoaWQ6IHN0cmluZykge1xuICAgIHJldHVybiB0aGlzLmNvbGxlY3Rvci5nZXQoaWQpO1xuICB9XG5cbiAgcmVtb3ZlKGlkOiBzdHJpbmcpIHtcbiAgICB0aGlzLmNvbGxlY3Rvci5yZW1vdmUoaWQpO1xuICB9XG5cbiAgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZykge1xuICAgIHRoaXMuYWxpYXNlc1thbGlhc10gPSBpZDtcbiAgfVxuXG4gIGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgY29uc3QgdGltZSA9IHRoaXMuYWxpYXNlc1tpZF0gPyB0aGlzLnRpbWVzW3RoaXMuYWxpYXNlc1tpZF1dIDogdGhpcy50aW1lc1tpZF07XG4gICAgcmV0dXJuIHRpbWUgJiYgKERhdGUubm93KCkgPD0gdGhpcy5jb25maWcuY2FjaGVUVEwgKyB0aW1lKTtcbiAgfVxuXG59XG4iXX0=