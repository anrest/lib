/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { ResponseCache } from './response-cache';
import { Inject, Injectable } from '@angular/core';
import { ObjectCollector } from '../core/object-collector';
import { ApiProcessor } from '../core/api.processor';
import { AnRestConfig } from '../core/config';
import { CollectionInfo, DataInfoService, EntityInfo } from '../core';
import { MetaService } from '../meta';
import { ResponseNode } from '../response';
export class LocalStorageResponseCache extends ResponseCache {
    /**
     * @param {?} collector
     * @param {?} processor
     * @param {?} info
     * @param {?} config
     */
    constructor(collector, processor, info, config) {
        super();
        this.collector = collector;
        this.processor = processor;
        this.info = info;
        this.config = config;
        this.tokenPrefix = 'anrest:cache:';
        const /** @type {?} */ timestampPrefix = this.tokenPrefix + 'timestamp:';
        Object.keys(localStorage).filter((e) => e.indexOf(timestampPrefix) === 0).forEach((k) => {
            const /** @type {?} */ id = k.substring(timestampPrefix.length);
            const /** @type {?} */ timestamp = this.getTimestamp(id);
            if (!timestamp || timestamp + this.config.cacheTTL < Date.now()) {
                this.remove(id);
            }
        });
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        if (this.has(id)) {
            return this.processor.process(this.getNode(id), 'GET');
        }
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    add(id, data) {
        this.doAdd(id, data, true);
    }
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    addAlias(id, alias) {
        localStorage.setItem(this.tokenPrefix + 'alias:' + alias, id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        const /** @type {?} */ complete = JSON.parse(localStorage.getItem(this.tokenPrefix + 'complete:' + id));
        const /** @type {?} */ timestamp = this.getTimestamp(id);
        return !!timestamp && complete && timestamp + this.config.cacheTTL >= Date.now();
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        localStorage.removeItem(this.tokenPrefix + id);
        localStorage.removeItem(this.tokenPrefix + 'type:' + id);
        localStorage.removeItem(this.tokenPrefix + 'collection:' + id);
        localStorage.removeItem(this.tokenPrefix + 'complete:' + id);
        localStorage.removeItem(this.tokenPrefix + 'references:' + id);
        this.removeTimestamp(id);
    }
    /**
     * @return {?}
     */
    clear() {
        Object.keys(localStorage).filter((e) => e.indexOf(this.tokenPrefix) === 0).forEach((k) => {
            localStorage.removeItem(k);
        });
    }
    /**
     * @param {?} id
     * @param {?} data
     * @param {?} complete
     * @return {?}
     */
    doAdd(id, data, complete) {
        const /** @type {?} */ info = this.info.get(data);
        let /** @type {?} */ values;
        if (info instanceof CollectionInfo) {
            values = [];
            for (const /** @type {?} */ item of data) {
                const /** @type {?} */ itemPath = this.info.get(item).path;
                values.push(itemPath);
                this.doAdd(itemPath, item, false);
            }
            localStorage.setItem(this.tokenPrefix + 'collection:' + id, JSON.stringify(info));
        }
        else {
            const /** @type {?} */ references = {};
            values = {};
            for (const /** @type {?} */ key in data) {
                let /** @type {?} */ itemInfo;
                if (typeof data[key] === 'object') {
                    itemInfo = this.info.get(data[key]);
                }
                if (itemInfo) {
                    this.doAdd(itemInfo.path, data[key], false);
                    references[key] = itemInfo.path;
                }
                else {
                    values[key] = data[key];
                }
            }
            localStorage.setItem(this.tokenPrefix + 'type:' + id, MetaService.get(data.constructor).name);
            localStorage.setItem(this.tokenPrefix + 'references:' + id, JSON.stringify(references));
        }
        localStorage.setItem(this.tokenPrefix + 'complete:' + id, JSON.stringify(complete));
        localStorage.setItem(this.tokenPrefix + id, JSON.stringify(values));
        this.setTimestamp(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getNode(id) {
        const /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + id));
        const /** @type {?} */ collectionInfo = this.getCollectionInfo(id);
        const /** @type {?} */ response = new ResponseNode();
        if (collectionInfo) {
            response.collection = true;
            response.info = collectionInfo;
            response.meta = MetaService.getByName(collectionInfo.type);
            response.data = [];
            for (const /** @type {?} */ itemId of data) {
                response.data.push(this.getNode(itemId));
            }
        }
        else {
            response.collection = false;
            response.info = new EntityInfo(id);
            response.meta = MetaService.getByName(localStorage.getItem(this.tokenPrefix + 'type:' + id));
            response.data = data;
            const /** @type {?} */ references = JSON.parse(localStorage.getItem(this.tokenPrefix + 'references:' + id));
            if (references) {
                for (const /** @type {?} */ key in references) {
                    response.data[key] = this.getNode(references[key]);
                }
            }
        }
        return response;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getCollectionInfo(id) {
        const /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + 'collection:' + id));
        if (data) {
            const /** @type {?} */ info = new CollectionInfo(id);
            for (const /** @type {?} */ key in data) {
                if (key !== '_path') {
                    info[key] = data[key];
                }
            }
            return info;
        }
    }
    /**
     * @param {?} id
     * @return {?}
     */
    setTimestamp(id) {
        localStorage.setItem(this.tokenPrefix + 'timestamp:' + id, String(Date.now()));
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTimestamp(id) {
        return Number(localStorage.getItem(this.tokenPrefix + 'timestamp:' + id));
    }
    /**
     * @param {?} id
     * @return {?}
     */
    removeTimestamp(id) {
        return localStorage.removeItem(this.tokenPrefix + 'timestamp:' + id);
    }
}
LocalStorageResponseCache.decorators = [
    { type: Injectable },
];
/** @nocollapse */
LocalStorageResponseCache.ctorParameters = () => [
    { type: ObjectCollector, },
    { type: ApiProcessor, },
    { type: DataInfoService, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];
function LocalStorageResponseCache_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    LocalStorageResponseCache.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    LocalStorageResponseCache.ctorParameters;
    /** @type {?} */
    LocalStorageResponseCache.prototype.tokenPrefix;
    /** @type {?} */
    LocalStorageResponseCache.prototype.collector;
    /** @type {?} */
    LocalStorageResponseCache.prototype.processor;
    /** @type {?} */
    LocalStorageResponseCache.prototype.info;
    /** @type {?} */
    LocalStorageResponseCache.prototype.config;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc3RvcmFnZS1yZXNwb25zZS1jYWNoZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiY2FjaGUvbG9jYWwtc3RvcmFnZS1yZXNwb25zZS1jYWNoZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUFFLFlBQVksRUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsZUFBZSxFQUFFLFVBQVUsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFHM0MsTUFBTSxnQ0FBaUMsU0FBUSxhQUFhOzs7Ozs7O0lBSTFELFlBQ1UsV0FDQSxXQUNBLE1BQ3NCO1FBRTlCLEtBQUssRUFBRSxDQUFDO1FBTEEsY0FBUyxHQUFULFNBQVM7UUFDVCxjQUFTLEdBQVQsU0FBUztRQUNULFNBQUksR0FBSixJQUFJO1FBQ2tCLFdBQU0sR0FBTixNQUFNOzJCQU5oQixlQUFlO1FBU25DLHVCQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQztRQUN4RCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUN0Rix1QkFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDL0MsdUJBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDeEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hFLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDakI7U0FDRixDQUFDLENBQUM7S0FDSjs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBVTtRQUNaLEVBQUUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN4RDtLQUNGOzs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBVSxFQUFFLElBQVM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQzVCOzs7Ozs7SUFFRCxRQUFRLENBQUMsRUFBVSxFQUFFLEtBQWE7UUFDaEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsR0FBRyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDL0Q7Ozs7O0lBRUQsR0FBRyxDQUFDLEVBQVU7UUFDWixFQUFFLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsR0FBRyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDbEUsdUJBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3ZGLHVCQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3hDLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLFFBQVEsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ2xGOzs7OztJQUVELE1BQU0sQ0FBQyxFQUFVO1FBQ2YsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQy9DLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDekQsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUMvRCxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzdELFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUMxQjs7OztJQUVELEtBQUs7UUFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDdkYsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM1QixDQUFDLENBQUM7S0FDSjs7Ozs7OztJQUVPLEtBQUssQ0FBQyxFQUFVLEVBQUUsSUFBUyxFQUFFLFFBQWlCO1FBQ3BELHVCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxxQkFBSSxNQUFXLENBQUM7UUFDaEIsRUFBRSxDQUFDLENBQUMsSUFBSSxZQUFZLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDbkMsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNaLEdBQUcsQ0FBQyxDQUFDLHVCQUFNLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN4Qix1QkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDbkM7WUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDbkY7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLHVCQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDdEIsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNaLEdBQUcsQ0FBQyxDQUFDLHVCQUFNLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixxQkFBSSxRQUFhLENBQUM7Z0JBQ2xCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ2xDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDckM7Z0JBQ0QsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDYixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM1QyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDakM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDekI7YUFDRjtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsRUFBRSxFQUFFLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlGLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztTQUN6RjtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUNwRixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzs7Ozs7SUFHaEIsT0FBTyxDQUFDLEVBQVU7UUFDeEIsdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDckUsdUJBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNsRCx1QkFBTSxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNwQyxFQUFFLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ25CLFFBQVEsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQzNCLFFBQVEsQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDO1lBQy9CLFFBQVEsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0QsUUFBUSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDbkIsR0FBRyxDQUFDLENBQUMsdUJBQU0sTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUMxQztTQUNGO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixRQUFRLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUM1QixRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ25DLFFBQVEsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0YsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDckIsdUJBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNGLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsR0FBRyxDQUFDLENBQUMsdUJBQU0sR0FBRyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0JBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDcEQ7YUFDRjtTQUNGO1FBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQzs7Ozs7O0lBR1YsaUJBQWlCLENBQUMsRUFBVTtRQUNsQyx1QkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDckYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNULHVCQUFNLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNwQyxHQUFHLENBQUMsQ0FBQyx1QkFBTSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsRUFBRSxDQUFDLENBQUMsR0FBRyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3ZCO2FBQ0Y7WUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO1NBQ2I7Ozs7OztJQUdLLFlBQVksQ0FBQyxFQUFVO1FBQzdCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDOzs7Ozs7SUFHekUsWUFBWSxDQUFDLEVBQVU7UUFDN0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7Ozs7OztJQUdwRSxlQUFlLENBQUMsRUFBVTtRQUNoQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksR0FBRyxFQUFFLENBQUMsQ0FBQzs7OztZQTlJeEUsVUFBVTs7OztZQVBGLGVBQWU7WUFDZixZQUFZO1lBRUksZUFBZTs0Q0FhbkMsTUFBTSxTQUFDLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9iamVjdENvbGxlY3RvciB9IGZyb20gJy4uL2NvcmUvb2JqZWN0LWNvbGxlY3Rvcic7XG5pbXBvcnQgeyBBcGlQcm9jZXNzb3IgfSBmcm9tICcuLi9jb3JlL2FwaS5wcm9jZXNzb3InO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uSW5mbywgRGF0YUluZm9TZXJ2aWNlLCBFbnRpdHlJbmZvIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi4vcmVzcG9uc2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSBleHRlbmRzIFJlc3BvbnNlQ2FjaGUge1xuXG4gIHByaXZhdGUgdG9rZW5QcmVmaXggPSAnYW5yZXN0OmNhY2hlOic7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjb2xsZWN0b3I6IE9iamVjdENvbGxlY3RvcixcbiAgICBwcml2YXRlIHByb2Nlc3NvcjogQXBpUHJvY2Vzc29yLFxuICAgIHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcml2YXRlIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge1xuICAgIHN1cGVyKCk7XG4gICAgY29uc3QgdGltZXN0YW1wUHJlZml4ID0gdGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JztcbiAgICBPYmplY3Qua2V5cyhsb2NhbFN0b3JhZ2UpLmZpbHRlcigoZSkgPT4gZS5pbmRleE9mKHRpbWVzdGFtcFByZWZpeCkgPT09IDApLmZvckVhY2goKGspID0+IHtcbiAgICAgIGNvbnN0IGlkID0gay5zdWJzdHJpbmcodGltZXN0YW1wUHJlZml4Lmxlbmd0aCk7XG4gICAgICBjb25zdCB0aW1lc3RhbXAgPSB0aGlzLmdldFRpbWVzdGFtcChpZCk7XG4gICAgICBpZiAoIXRpbWVzdGFtcCB8fCB0aW1lc3RhbXAgKyB0aGlzLmNvbmZpZy5jYWNoZVRUTCA8IERhdGUubm93KCkpIHtcbiAgICAgICAgdGhpcy5yZW1vdmUoaWQpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgZ2V0KGlkOiBzdHJpbmcpIHtcbiAgICBpZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGlkKSB8fCBpZDtcbiAgICBpZiAodGhpcy5oYXMoaWQpKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9jZXNzb3IucHJvY2Vzcyh0aGlzLmdldE5vZGUoaWQpLCAnR0VUJyk7XG4gICAgfVxuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICAgIHRoaXMuZG9BZGQoaWQsIGRhdGEsIHRydWUpO1xuICB9XG5cbiAgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZykge1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGFsaWFzLCBpZCk7XG4gIH1cblxuICBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIGlkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdhbGlhczonICsgaWQpIHx8IGlkO1xuICAgIGNvbnN0IGNvbXBsZXRlID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCkpO1xuICAgIGNvbnN0IHRpbWVzdGFtcCA9IHRoaXMuZ2V0VGltZXN0YW1wKGlkKTtcbiAgICByZXR1cm4gISF0aW1lc3RhbXAgJiYgY29tcGxldGUgJiYgdGltZXN0YW1wICsgdGhpcy5jb25maWcuY2FjaGVUVEwgPj0gRGF0ZS5ub3coKTtcbiAgfVxuXG4gIHJlbW92ZShpZDogc3RyaW5nKSB7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCk7XG4gICAgdGhpcy5yZW1vdmVUaW1lc3RhbXAoaWQpO1xuICB9XG5cbiAgY2xlYXIoKSB7XG4gICAgT2JqZWN0LmtleXMobG9jYWxTdG9yYWdlKS5maWx0ZXIoKGUpID0+IGUuaW5kZXhPZih0aGlzLnRva2VuUHJlZml4KSA9PT0gMCkuZm9yRWFjaCgoaykgPT4ge1xuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oayk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGRvQWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSwgY29tcGxldGU6IGJvb2xlYW4pIHtcbiAgICBjb25zdCBpbmZvID0gdGhpcy5pbmZvLmdldChkYXRhKTtcbiAgICBsZXQgdmFsdWVzOiBhbnk7XG4gICAgaWYgKGluZm8gaW5zdGFuY2VvZiBDb2xsZWN0aW9uSW5mbykge1xuICAgICAgdmFsdWVzID0gW107XG4gICAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YSkge1xuICAgICAgICBjb25zdCBpdGVtUGF0aCA9IHRoaXMuaW5mby5nZXQoaXRlbSkucGF0aDtcbiAgICAgICAgdmFsdWVzLnB1c2goaXRlbVBhdGgpO1xuICAgICAgICB0aGlzLmRvQWRkKGl0ZW1QYXRoLCBpdGVtLCBmYWxzZSk7XG4gICAgICB9XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkLCBKU09OLnN0cmluZ2lmeShpbmZvKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHJlZmVyZW5jZXMgPSB7fTtcbiAgICAgIHZhbHVlcyA9IHt9O1xuICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuICAgICAgICBsZXQgaXRlbUluZm86IGFueTtcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhW2tleV0gPT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgaXRlbUluZm8gPSB0aGlzLmluZm8uZ2V0KGRhdGFba2V5XSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGl0ZW1JbmZvKSB7XG4gICAgICAgICAgdGhpcy5kb0FkZChpdGVtSW5mby5wYXRoLCBkYXRhW2tleV0sIGZhbHNlKTtcbiAgICAgICAgICByZWZlcmVuY2VzW2tleV0gPSBpdGVtSW5mby5wYXRoO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZhbHVlc1trZXldID0gZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkLCBNZXRhU2VydmljZS5nZXQoZGF0YS5jb25zdHJ1Y3RvcikubmFtZSk7XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3JlZmVyZW5jZXM6JyArIGlkLCBKU09OLnN0cmluZ2lmeShyZWZlcmVuY2VzKSk7XG4gICAgfVxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnY29tcGxldGU6JyArIGlkLCBKU09OLnN0cmluZ2lmeShjb21wbGV0ZSkpO1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCwgSlNPTi5zdHJpbmdpZnkodmFsdWVzKSk7XG4gICAgdGhpcy5zZXRUaW1lc3RhbXAoaWQpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXROb2RlKGlkOiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCkpO1xuICAgIGNvbnN0IGNvbGxlY3Rpb25JbmZvID0gdGhpcy5nZXRDb2xsZWN0aW9uSW5mbyhpZCk7XG4gICAgY29uc3QgcmVzcG9uc2UgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgaWYgKGNvbGxlY3Rpb25JbmZvKSB7XG4gICAgICByZXNwb25zZS5jb2xsZWN0aW9uID0gdHJ1ZTtcbiAgICAgIHJlc3BvbnNlLmluZm8gPSBjb2xsZWN0aW9uSW5mbztcbiAgICAgIHJlc3BvbnNlLm1ldGEgPSBNZXRhU2VydmljZS5nZXRCeU5hbWUoY29sbGVjdGlvbkluZm8udHlwZSk7XG4gICAgICByZXNwb25zZS5kYXRhID0gW107XG4gICAgICBmb3IgKGNvbnN0IGl0ZW1JZCBvZiBkYXRhKSB7XG4gICAgICAgIHJlc3BvbnNlLmRhdGEucHVzaCh0aGlzLmdldE5vZGUoaXRlbUlkKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3BvbnNlLmNvbGxlY3Rpb24gPSBmYWxzZTtcbiAgICAgIHJlc3BvbnNlLmluZm8gPSBuZXcgRW50aXR5SW5mbyhpZCk7XG4gICAgICByZXNwb25zZS5tZXRhID0gTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAndHlwZTonICsgaWQpKTtcbiAgICAgIHJlc3BvbnNlLmRhdGEgPSBkYXRhO1xuICAgICAgY29uc3QgcmVmZXJlbmNlcyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCkpO1xuICAgICAgaWYgKHJlZmVyZW5jZXMpIHtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gcmVmZXJlbmNlcykge1xuICAgICAgICAgIHJlc3BvbnNlLmRhdGFba2V5XSA9IHRoaXMuZ2V0Tm9kZShyZWZlcmVuY2VzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXNwb25zZTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0Q29sbGVjdGlvbkluZm8oaWQ6IHN0cmluZykge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnY29sbGVjdGlvbjonICsgaWQpKTtcbiAgICBpZiAoZGF0YSkge1xuICAgICAgY29uc3QgaW5mbyA9IG5ldyBDb2xsZWN0aW9uSW5mbyhpZCk7XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgIGlmIChrZXkgIT09ICdfcGF0aCcpIHtcbiAgICAgICAgICBpbmZvW2tleV0gPSBkYXRhW2tleV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBpbmZvO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0VGltZXN0YW1wKGlkOiBzdHJpbmcpIHtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3RpbWVzdGFtcDonICsgaWQsIFN0cmluZyhEYXRlLm5vdygpKSk7XG4gIH1cblxuICBwcml2YXRlIGdldFRpbWVzdGFtcChpZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIE51bWJlcihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3RpbWVzdGFtcDonICsgaWQpKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVtb3ZlVGltZXN0YW1wKGlkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JyArIGlkKTtcbiAgfVxufVxuIl19