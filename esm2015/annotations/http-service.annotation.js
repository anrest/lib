/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
import * as pluralize from 'pluralize';
/**
 * @param {?} entity
 * @param {?=} path
 * @return {?}
 */
export function HttpService(entity, path) {
    return (target) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForHttpService(target, entity);
        meta.path = path || '/' + pluralize.plural(entity.name).replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase();
        meta.entityMeta.service = target;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC1zZXJ2aWNlLmFubm90YXRpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImFubm90YXRpb25zL2h0dHAtc2VydmljZS5hbm5vdGF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQ3RDLE9BQU8sS0FBSyxTQUFTLE1BQU0sV0FBVyxDQUFDOzs7Ozs7QUFFdkMsTUFBTSxzQkFBc0IsTUFBZ0IsRUFBRSxJQUFhO0lBRXpELE1BQU0sQ0FBQyxDQUFDLE1BQWdCLEVBQUUsRUFBRTtRQUMxQix1QkFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDLHlCQUF5QixDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxHQUFHLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RILElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztLQUNsQyxDQUFDO0NBQ0giLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0ICogYXMgcGx1cmFsaXplIGZyb20gJ3BsdXJhbGl6ZSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBIdHRwU2VydmljZShlbnRpdHk6IEZ1bmN0aW9uLCBwYXRoPzogc3RyaW5nKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IEZ1bmN0aW9uKSA9PiB7XG4gICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9ySHR0cFNlcnZpY2UodGFyZ2V0LCBlbnRpdHkpO1xuICAgIG1ldGEucGF0aCA9IHBhdGggfHwgJy8nICsgcGx1cmFsaXplLnBsdXJhbChlbnRpdHkubmFtZSkucmVwbGFjZSgvXFwuPyhbQS1aXSkvZywgJy0kMScpLnJlcGxhY2UoL14tLywgJycpLnRvTG93ZXJDYXNlKCk7XG4gICAgbWV0YS5lbnRpdHlNZXRhLnNlcnZpY2UgPSB0YXJnZXQ7XG4gIH07XG59XG4iXX0=