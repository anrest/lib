/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @record
 */
export function PropertyInfo() { }
function PropertyInfo_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    PropertyInfo.prototype.collection;
    /** @type {?|undefined} */
    PropertyInfo.prototype.excludeWhenSaving;
    /** @type {?|undefined} */
    PropertyInfo.prototype.type;
}
/**
 * @param {?=} info
 * @return {?}
 */
export function Property(info = {}) {
    return (target, key) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForProperty(target, key, info.type);
        meta.isCollection = info.collection || false;
        meta.excludeWhenSaving = info.excludeWhenSaving || false;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvcGVydHkuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvcHJvcGVydHkuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRdEMsTUFBTSxtQkFBb0IsT0FBcUIsRUFBRTtJQUUvQyxNQUFNLENBQUMsQ0FBQyxNQUFXLEVBQUUsR0FBVyxFQUFFLEVBQUU7UUFDbEMsdUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxDQUFDO1FBQzdDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUksS0FBSyxDQUFDO0tBQzFELENBQUM7Q0FDSCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgUHJvcGVydHlJbmZvIHtcbiAgY29sbGVjdGlvbj86IGJvb2xlYW47XG4gIGV4Y2x1ZGVXaGVuU2F2aW5nPzogYm9vbGVhbjtcbiAgdHlwZT86IGFueTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIFByb3BlcnR5IChpbmZvOiBQcm9wZXJ0eUluZm8gPSB7fSkge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nKSA9PiB7XG4gICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yUHJvcGVydHkodGFyZ2V0LCBrZXksIGluZm8udHlwZSk7XG4gICAgbWV0YS5pc0NvbGxlY3Rpb24gPSBpbmZvLmNvbGxlY3Rpb24gfHwgZmFsc2U7XG4gICAgbWV0YS5leGNsdWRlV2hlblNhdmluZyA9IGluZm8uZXhjbHVkZVdoZW5TYXZpbmcgfHwgZmFsc2U7XG4gIH07XG59XG4iXX0=