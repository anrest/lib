/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @record
 */
export function ResourceInfo() { }
function ResourceInfo_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    ResourceInfo.prototype.path;
    /** @type {?|undefined} */
    ResourceInfo.prototype.name;
    /** @type {?|undefined} */
    ResourceInfo.prototype.cache;
}
/**
 * @param {?=} info
 * @return {?}
 */
export function Resource(info = {}) {
    return (target) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        meta.name = info.name || target.name;
        meta.cache = info.cache;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvcmVzb3VyY2UuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRdEMsTUFBTSxtQkFBb0IsT0FBcUIsRUFBRTtJQUUvQyxNQUFNLENBQUMsQ0FBQyxNQUFnQixFQUFFLEVBQUU7UUFDMUIsdUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQztRQUNyQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDekIsQ0FBQztDQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGludGVyZmFjZSBSZXNvdXJjZUluZm8ge1xuICBwYXRoPzogc3RyaW5nO1xuICBuYW1lPzogc3RyaW5nO1xuICBjYWNoZT86IEZ1bmN0aW9uO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gUmVzb3VyY2UgKGluZm86IFJlc291cmNlSW5mbyA9IHt9KSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IEZ1bmN0aW9uKSA9PiB7XG4gICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldCk7XG4gICAgbWV0YS5uYW1lID0gaW5mby5uYW1lIHx8IHRhcmdldC5uYW1lO1xuICAgIG1ldGEuY2FjaGUgPSBpbmZvLmNhY2hlO1xuICB9O1xufVxuIl19