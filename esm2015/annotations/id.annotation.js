/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @return {?}
 */
export function Id() {
    return (target, key) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForEntity(target.constructor);
        meta.id = key;
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWQuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvaWQuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7OztBQUV0QyxNQUFNO0lBRUosTUFBTSxDQUFDLENBQUMsTUFBVyxFQUFFLEdBQVcsRUFBRSxFQUFFO1FBQ2xDLHVCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDO0tBQ2YsQ0FBQztDQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGZ1bmN0aW9uIElkICgpIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZykgPT4ge1xuICAgIGNvbnN0IG1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0YXJnZXQuY29uc3RydWN0b3IpO1xuICAgIG1ldGEuaWQgPSBrZXk7XG4gIH07XG59XG4iXX0=