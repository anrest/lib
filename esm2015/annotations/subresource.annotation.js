/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @param {?} type
 * @param {?=} path
 * @return {?}
 */
export function Subresource(type, path) {
    return (target, key, value) => {
        MetaService.getOrCreateForEntity(target.constructor).subresources[key] = {
            meta: MetaService.getOrCreateForEntity(type),
            path: path || '/' + key.replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase()
        };
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VicmVzb3VyY2UuYW5ub3RhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbnJlc3QvbGliLyIsInNvdXJjZXMiOlsiYW5ub3RhdGlvbnMvc3VicmVzb3VyY2UuYW5ub3RhdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQzs7Ozs7O0FBRXRDLE1BQU0sc0JBQXVCLElBQWMsRUFBRSxJQUFhO0lBRXhELE1BQU0sQ0FBQyxDQUFDLE1BQVcsRUFBRSxHQUFXLEVBQUUsS0FBVSxFQUFFLEVBQUU7UUFDOUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEdBQUc7WUFDdkUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7WUFDNUMsSUFBSSxFQUFFLElBQUksSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUU7U0FDdEYsQ0FBQztLQUNILENBQUM7Q0FDSCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBTdWJyZXNvdXJjZSAodHlwZTogRnVuY3Rpb24sIHBhdGg/OiBzdHJpbmcpIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZywgdmFsdWU6IGFueSkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3Rvcikuc3VicmVzb3VyY2VzW2tleV0gPSB7XG4gICAgICBtZXRhOiBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0eXBlKSxcbiAgICAgIHBhdGg6IHBhdGggfHwgJy8nICsga2V5LnJlcGxhY2UoL1xcLj8oW0EtWl0pL2csICctJDEnKS5yZXBsYWNlKC9eLS8sICcnKS50b0xvd2VyQ2FzZSgpXG4gICAgfTtcbiAgfTtcbn1cbiJdfQ==