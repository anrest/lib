/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @param {?} headers
 * @return {?}
 */
export function Headers(headers) {
    return (target) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        for (const /** @type {?} */ header of headers) {
            meta.headers.push({
                name: header.name,
                value: () => header.value,
                append: header.append || false,
                dynamic: false
            });
        }
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVycy5hbm5vdGF0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJhbm5vdGF0aW9ucy9oZWFkZXJzLmFubm90YXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxTQUFTLENBQUM7Ozs7O0FBRXRDLE1BQU0sa0JBQW1CLE9BQTJEO0lBRWxGLE1BQU0sQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO1FBQ3JCLHVCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsR0FBRyxDQUFDLENBQUMsdUJBQU0sTUFBTSxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTtnQkFDakIsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLO2dCQUN6QixNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sSUFBSSxLQUFLO2dCQUM5QixPQUFPLEVBQUUsS0FBSzthQUNmLENBQUMsQ0FBQztTQUNKO0tBQ0YsQ0FBQztDQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGZ1bmN0aW9uIEhlYWRlcnMgKGhlYWRlcnM6IHsgbmFtZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBhcHBlbmQ/OiBib29sZWFufVtdKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSkgPT4ge1xuICAgIGNvbnN0IG1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0YXJnZXQpO1xuICAgIGZvciAoY29uc3QgaGVhZGVyIG9mIGhlYWRlcnMpIHtcbiAgICAgIG1ldGEuaGVhZGVycy5wdXNoKHtcbiAgICAgICAgbmFtZTogaGVhZGVyLm5hbWUsXG4gICAgICAgIHZhbHVlOiAoKSA9PiBoZWFkZXIudmFsdWUsXG4gICAgICAgIGFwcGVuZDogaGVhZGVyLmFwcGVuZCB8fCBmYWxzZSxcbiAgICAgICAgZHluYW1pYzogZmFsc2VcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcbn1cbiJdfQ==