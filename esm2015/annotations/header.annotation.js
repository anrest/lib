/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { MetaService } from '../meta';
/**
 * @param {?} headerName
 * @param {?=} append
 * @return {?}
 */
export function Header(headerName, append) {
    return (target, key, value = undefined) => {
        MetaService.getOrCreateForEntity(target.constructor).headers.push({
            name: headerName,
            value: value ? value.value : function () { return this[key]; },
            append: append || false,
            dynamic: true
        });
    };
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmFubm90YXRpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImFubm90YXRpb25zL2hlYWRlci5hbm5vdGF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sU0FBUyxDQUFDOzs7Ozs7QUFFdEMsTUFBTSxpQkFBaUIsVUFBa0IsRUFBRSxNQUFnQjtJQUV6RCxNQUFNLENBQUMsQ0FBQyxNQUFXLEVBQUUsR0FBVyxFQUFFLFFBQWEsU0FBUyxFQUFFLEVBQUU7UUFDMUQsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2hFLElBQUksRUFBRSxVQUFVO1lBQ2hCLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLGNBQWMsTUFBTSxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQy9ELE1BQU0sRUFBRSxNQUFNLElBQUksS0FBSztZQUN2QixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUMsQ0FBQztLQUNKLENBQUM7Q0FDSCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBIZWFkZXIoaGVhZGVyTmFtZTogc3RyaW5nLCBhcHBlbmQ/OiBib29sZWFuKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkgPSB1bmRlZmluZWQpID0+IHtcbiAgICBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0YXJnZXQuY29uc3RydWN0b3IpLmhlYWRlcnMucHVzaCh7XG4gICAgICBuYW1lOiBoZWFkZXJOYW1lLFxuICAgICAgdmFsdWU6IHZhbHVlID8gdmFsdWUudmFsdWUgOiBmdW5jdGlvbiAoKSB7IHJldHVybiAgdGhpc1trZXldOyB9LFxuICAgICAgYXBwZW5kOiBhcHBlbmQgfHwgZmFsc2UsXG4gICAgICBkeW5hbWljOiB0cnVlXG4gICAgfSk7XG4gIH07XG59XG4iXX0=