/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/**
 * @record
 */
export function Event() { }
function Event_tsickle_Closure_declarations() {
    /** @type {?} */
    Event.prototype.entity;
    /** @type {?} */
    Event.prototype.data;
}
export class BaseEvent {
    /**
     * @param {?} data
     * @param {?=} type
     */
    constructor(data, type) {
        this._data = data;
        this.type = type;
    }
    /**
     * @return {?}
     */
    data() {
        return this._data;
    }
    /**
     * @return {?}
     */
    entity() {
        return this.type || (Array.isArray(this._data) ? this._data[0].constructor : this._data.constructor);
    }
}
function BaseEvent_tsickle_Closure_declarations() {
    /** @type {?} */
    BaseEvent.prototype._data;
    /** @type {?} */
    BaseEvent.prototype.type;
}
export class BeforeGetEvent extends BaseEvent {
    /**
     * @param {?} path
     * @param {?=} type
     * @param {?=} filter
     */
    constructor(path, type, filter) {
        super(path, type);
        this._filter = filter;
    }
    /**
     * @return {?}
     */
    entity() {
        return this.type;
    }
    /**
     * @return {?}
     */
    filter() {
        return this._filter;
    }
}
function BeforeGetEvent_tsickle_Closure_declarations() {
    /** @type {?} */
    BeforeGetEvent.prototype._filter;
}
export class AfterGetEvent extends BaseEvent {
}
export class BeforeSaveEvent extends BaseEvent {
}
export class AfterSaveEvent extends BaseEvent {
}
export class BeforeRemoveEvent extends BaseEvent {
}
export class AfterRemoveEvent extends BaseEvent {
}
/**
 * @record
 */
export function EventListener() { }
function EventListener_tsickle_Closure_declarations() {
    /** @type {?} */
    EventListener.prototype.handle;
}
export let /** @type {?} */ ANREST_HTTP_EVENT_LISTENERS = new InjectionToken('anrest.http_event_listeners');

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFucmVzdC9saWIvIiwic291cmNlcyI6WyJldmVudHMvZXZlbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7Ozs7OztBQVEvQyxNQUFNOzs7OztJQUlKLFlBQVksSUFBUyxFQUFFLElBQWU7UUFDcEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7S0FDbEI7Ozs7SUFFRCxJQUFJO1FBQ0YsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDbkI7Ozs7SUFFRCxNQUFNO1FBQ0osTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDdEc7Q0FDRjs7Ozs7OztBQUVELE1BQU0scUJBQXNCLFNBQVEsU0FBUzs7Ozs7O0lBRzNDLFlBQVksSUFBWSxFQUFFLElBQWUsRUFBRSxNQUFtQjtRQUM1RCxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO0tBQ3ZCOzs7O0lBRUQsTUFBTTtRQUNKLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ2xCOzs7O0lBRUQsTUFBTTtRQUNKLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO0tBQ3JCO0NBQ0Y7Ozs7O0FBQ0QsTUFBTSxvQkFBcUIsU0FBUSxTQUFTO0NBQUc7QUFDL0MsTUFBTSxzQkFBdUIsU0FBUSxTQUFTO0NBQUc7QUFDakQsTUFBTSxxQkFBc0IsU0FBUSxTQUFTO0NBQUc7QUFDaEQsTUFBTSx3QkFBeUIsU0FBUSxTQUFTO0NBQUc7QUFDbkQsTUFBTSx1QkFBd0IsU0FBUSxTQUFTO0NBQUc7Ozs7Ozs7OztBQU1sRCxNQUFNLENBQUMscUJBQUksMkJBQTJCLEdBQUcsSUFBSSxjQUFjLENBQWtCLDZCQUE2QixDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuZXhwb3J0IGludGVyZmFjZSBFdmVudCB7XG4gIGVudGl0eSgpOiBGdW5jdGlvbjtcbiAgZGF0YSgpOiBhbnk7XG59XG5cbmV4cG9ydCBjbGFzcyBCYXNlRXZlbnQgaW1wbGVtZW50cyBFdmVudCB7XG4gIF9kYXRhPzogYW55O1xuICBwcm90ZWN0ZWQgcmVhZG9ubHkgdHlwZTogRnVuY3Rpb247XG5cbiAgY29uc3RydWN0b3IoZGF0YTogYW55LCB0eXBlPzogRnVuY3Rpb24pIHtcbiAgICB0aGlzLl9kYXRhID0gZGF0YTtcbiAgICB0aGlzLnR5cGUgPSB0eXBlO1xuICB9XG5cbiAgZGF0YSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLl9kYXRhO1xuICB9XG5cbiAgZW50aXR5KCk6IEZ1bmN0aW9uIHtcbiAgICByZXR1cm4gdGhpcy50eXBlIHx8IChBcnJheS5pc0FycmF5KHRoaXMuX2RhdGEpID8gdGhpcy5fZGF0YVswXS5jb25zdHJ1Y3RvciA6IHRoaXMuX2RhdGEuY29uc3RydWN0b3IpO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBCZWZvcmVHZXRFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7XG4gIHByaXZhdGUgcmVhZG9ubHkgX2ZpbHRlcjogSHR0cFBhcmFtcztcblxuICBjb25zdHJ1Y3RvcihwYXRoOiBzdHJpbmcsIHR5cGU/OiBGdW5jdGlvbiwgZmlsdGVyPzogSHR0cFBhcmFtcykge1xuICAgIHN1cGVyKHBhdGgsIHR5cGUpO1xuICAgIHRoaXMuX2ZpbHRlciA9IGZpbHRlcjtcbiAgfVxuXG4gIGVudGl0eSgpOiBGdW5jdGlvbiB7XG4gICAgcmV0dXJuIHRoaXMudHlwZTtcbiAgfVxuXG4gIGZpbHRlcigpOiBIdHRwUGFyYW1zIHtcbiAgICByZXR1cm4gdGhpcy5fZmlsdGVyO1xuICB9XG59XG5leHBvcnQgY2xhc3MgQWZ0ZXJHZXRFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuZXhwb3J0IGNsYXNzIEJlZm9yZVNhdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuZXhwb3J0IGNsYXNzIEFmdGVyU2F2ZUV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHt9XG5leHBvcnQgY2xhc3MgQmVmb3JlUmVtb3ZlRXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBBZnRlclJlbW92ZUV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHt9XG5cbmV4cG9ydCBpbnRlcmZhY2UgRXZlbnRMaXN0ZW5lciB7XG4gIGhhbmRsZShldmVudDogRXZlbnQpO1xufVxuXG5leHBvcnQgbGV0IEFOUkVTVF9IVFRQX0VWRU5UX0xJU1RFTkVSUyA9IG5ldyBJbmplY3Rpb25Ub2tlbjxFdmVudExpc3RlbmVyW10+KCdhbnJlc3QuaHR0cF9ldmVudF9saXN0ZW5lcnMnKTtcbiJdfQ==