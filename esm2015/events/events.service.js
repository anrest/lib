/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Inject, Injectable, Optional } from '@angular/core';
import { ANREST_HTTP_EVENT_LISTENERS } from './events';
import { MetaService } from '../meta';
export class EventsService {
    /**
     * @param {?} handlers
     */
    constructor(handlers) {
        this.handlers = handlers;
        if (!Array.isArray(this.handlers)) {
            this.handlers = [];
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    handle(event) {
        this.handlers.forEach((handler) => {
            const /** @type {?} */ meta = MetaService.getOrCreateForEventListener(handler.constructor);
            for (const /** @type {?} */ eventMeta of meta.events) {
                if (eventMeta.type === event.constructor && eventMeta.entity === event.entity()) {
                    handler.handle(event);
                }
            }
        });
        return event;
    }
}
EventsService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
EventsService.ctorParameters = () => [
    { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_EVENT_LISTENERS,] },] },
];
function EventsService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    EventsService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    EventsService.ctorParameters;
    /** @type {?} */
    EventsService.prototype.handlers;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYW5yZXN0L2xpYi8iLCJzb3VyY2VzIjpbImV2ZW50cy9ldmVudHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFDTCwyQkFBMkIsRUFDNUIsTUFBTSxVQUFVLENBQUM7QUFDbEIsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUd0QyxNQUFNOzs7O0lBRUosWUFDb0U7UUFBQSxhQUFRLEdBQVIsUUFBUTtRQUUxRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUNwQjtLQUNGOzs7OztJQUVNLE1BQU0sQ0FBQyxLQUFZO1FBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBc0IsRUFBRSxFQUFFO1lBQy9DLHVCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzFFLEdBQUcsQ0FBQyxDQUFDLHVCQUFNLFNBQVMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDcEMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsV0FBVyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEtBQUssS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDaEYsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDdkI7YUFDRjtTQUNGLENBQUMsQ0FBQztRQUNILE1BQU0sQ0FBQyxLQUFLLENBQUM7Ozs7WUFwQmhCLFVBQVU7Ozs7d0NBSU4sUUFBUSxZQUFJLE1BQU0sU0FBQywyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1xuICBBTlJFU1RfSFRUUF9FVkVOVF9MSVNURU5FUlMsIEV2ZW50LCBFdmVudExpc3RlbmVyXG59IGZyb20gJy4vZXZlbnRzJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBFdmVudHNTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEFOUkVTVF9IVFRQX0VWRU5UX0xJU1RFTkVSUykgcHJpdmF0ZSByZWFkb25seSBoYW5kbGVyczogRXZlbnRMaXN0ZW5lcltdXG4gICkge1xuICAgIGlmICghQXJyYXkuaXNBcnJheSh0aGlzLmhhbmRsZXJzKSkge1xuICAgICAgdGhpcy5oYW5kbGVycyA9IFtdO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBoYW5kbGUoZXZlbnQ6IEV2ZW50KTogRXZlbnQge1xuICAgIHRoaXMuaGFuZGxlcnMuZm9yRWFjaCgoaGFuZGxlcjogRXZlbnRMaXN0ZW5lcikgPT4ge1xuICAgICAgY29uc3QgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRXZlbnRMaXN0ZW5lcihoYW5kbGVyLmNvbnN0cnVjdG9yKTtcbiAgICAgIGZvciAoY29uc3QgZXZlbnRNZXRhIG9mIG1ldGEuZXZlbnRzKSB7XG4gICAgICAgIGlmIChldmVudE1ldGEudHlwZSA9PT0gZXZlbnQuY29uc3RydWN0b3IgJiYgZXZlbnRNZXRhLmVudGl0eSA9PT0gZXZlbnQuZW50aXR5KCkpIHtcbiAgICAgICAgICBoYW5kbGVyLmhhbmRsZShldmVudCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gZXZlbnQ7XG4gIH1cbn1cbiJdfQ==