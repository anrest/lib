import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from '../core';
import { Collection } from '../core/collection';
export declare abstract class Loader {
    protected service: ApiService;
    private readonly fields;
    private readonly observable;
    private loading;
    protected lastData: Collection<any>;
    abstract getAvailableFields(): string[];
    constructor(service: ApiService, waitFor?: number, replay?: number);
    field(name: any): BehaviorSubject<any>;
    readonly data: Observable<any>;
    readonly isLoading: boolean;
    protected getServiceObservable([data]: any[]): Observable<any>;
    protected checkDistinct(d1: any[], d2: any[]): boolean;
    protected mapParams(values: any[]): any[];
    protected getSubjects(): BehaviorSubject<any>[];
}
