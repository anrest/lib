import { BehaviorSubject, Observable } from 'rxjs';
import { Loader } from './loader';
export declare abstract class PageLoader extends Loader {
    private pageSubject;
    first(): void;
    previous(): void;
    next(): void;
    last(): void;
    hasFirst(): boolean;
    hasPrevious(): boolean;
    hasNext(): boolean;
    hasLast(): boolean;
    protected getServiceObservable([page, data]: any[]): Observable<any>;
    protected getSubjects(): BehaviorSubject<any>[];
}
