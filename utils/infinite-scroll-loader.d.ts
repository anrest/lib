import { BehaviorSubject, Observable } from 'rxjs';
import { Loader } from './loader';
export declare abstract class InfiniteScrollLoader extends Loader {
    private loadMoreSubject;
    loadMore(): void;
    hasMore(): boolean;
    protected getServiceObservable([loadMore, data]: any[]): Observable<any>;
    protected getSubjects(): BehaviorSubject<any>[];
}
