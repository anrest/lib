import { InjectionToken, Injectable, Inject, Optional, Injector, NgModule } from '@angular/core';
import { map, tap, debounceTime, distinctUntilChanged, mergeMap, share, shareReplay } from 'rxjs/operators';
import { HttpHeaders, HttpClient, HttpParams, HttpResponse, HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { of, BehaviorSubject, combineLatest } from 'rxjs';
import { call } from 'jwt-decode';
import { plural } from 'pluralize';
import 'reflect-metadata';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const /** @type {?} */ AnRestConfig = new InjectionToken('anrest.api_config');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class EntityInfo {
    /**
     * @param {?} _path
     */
    constructor(_path) {
        this._path = _path;
    }
    /**
     * @return {?}
     */
    get path() {
        return this._path;
    }
}
class CollectionInfo {
    /**
     * @param {?} _path
     */
    constructor(_path) {
        this._path = _path;
    }
    /**
     * @return {?}
     */
    get path() {
        return this._path;
    }
}
class DataInfoService {
    /**
     * @param {?} object
     * @return {?}
     */
    get(object) {
        return Reflect.getMetadata('anrest:info', object);
    }
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    set(object, info) {
        Reflect.defineMetadata('anrest:info', info, object);
    }
    /**
     * @param {?} object
     * @return {?}
     */
    getForCollection(object) {
        return /** @type {?} */ (this.get(object));
    }
    /**
     * @param {?} object
     * @return {?}
     */
    getForEntity(object) {
        return /** @type {?} */ (this.get(object));
    }
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    setForCollection(object, info) {
        this.set(object, info);
    }
    /**
     * @param {?} object
     * @param {?} info
     * @return {?}
     */
    setForEntity(object, info) {
        this.set(object, info);
    }
}
DataInfoService.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @template T
 */
class Collection extends Array {
    /**
     * @param {?} service
     * @param {?} info
     * @param {...?} items
     */
    constructor(service, info, ...items) {
        super(...items);
        this.service = service;
        this.info = info;
        Object.setPrototypeOf(this, Collection.prototype);
    }
    /**
     * @template T
     * @param {?} c1
     * @param {?} c2
     * @return {?}
     */
    static merge(c1, c2) {
        const /** @type {?} */ info = new CollectionInfo(c1.info.path);
        info.type = c1.info.type;
        info.next = c2.info.next;
        info.previous = c1.info.previous;
        info.first = c1.info.first;
        info.last = c1.info.last;
        info.total = c1.info.total;
        info.page = c1.info.page;
        info.lastPage = c1.info.lastPage;
        const /** @type {?} */ c = new Collection(c1.service, info);
        c1.forEach((v) => c.push(v));
        c2.forEach((v) => c.push(v));
        return c;
    }
    /**
     * @return {?}
     */
    get total() {
        return this.info.total;
    }
    /**
     * @return {?}
     */
    get page() {
        return this.info.page;
    }
    /**
     * @return {?}
     */
    get pageTotal() {
        return this.info.lastPage;
    }
    /**
     * @return {?}
     */
    first() {
        return /** @type {?} */ (this.service.doGet(this.info.first));
    }
    /**
     * @return {?}
     */
    previous() {
        return /** @type {?} */ (this.service.doGet(this.info.previous));
    }
    /**
     * @return {?}
     */
    next() {
        return /** @type {?} */ (this.service.doGet(this.info.next));
    }
    /**
     * @return {?}
     */
    last() {
        return /** @type {?} */ (this.service.doGet(this.info.last));
    }
    /**
     * @return {?}
     */
    firstPath() {
        return this.info.first;
    }
    /**
     * @return {?}
     */
    previousPath() {
        return this.info.previous;
    }
    /**
     * @return {?}
     */
    nextPath() {
        return this.info.next;
    }
    /**
     * @return {?}
     */
    lastPath() {
        return this.info.last;
    }
    /**
     * @return {?}
     */
    loadMore() {
        return this.next().pipe(map((result) => Collection.merge(this, result)));
    }
    /**
     * @return {?}
     */
    hasMore() {
        return this.hasNext();
    }
    /**
     * @return {?}
     */
    hasNext() {
        return !!this.info.next;
    }
    /**
     * @return {?}
     */
    hasPrevious() {
        return !!this.info.previous;
    }
    /**
     * @return {?}
     */
    hasFirst() {
        return this.hasPrevious();
    }
    /**
     * @return {?}
     */
    hasLast() {
        return this.hasNext();
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class BaseEvent {
    /**
     * @param {?} data
     * @param {?=} type
     */
    constructor(data, type) {
        this._data = data;
        this.type = type;
    }
    /**
     * @return {?}
     */
    data() {
        return this._data;
    }
    /**
     * @return {?}
     */
    entity() {
        return this.type || (Array.isArray(this._data) ? this._data[0].constructor : this._data.constructor);
    }
}
class BeforeGetEvent extends BaseEvent {
    /**
     * @param {?} path
     * @param {?=} type
     * @param {?=} filter
     */
    constructor(path, type, filter) {
        super(path, type);
        this._filter = filter;
    }
    /**
     * @return {?}
     */
    entity() {
        return this.type;
    }
    /**
     * @return {?}
     */
    filter() {
        return this._filter;
    }
}
class AfterGetEvent extends BaseEvent {
}
class BeforeSaveEvent extends BaseEvent {
}
class AfterSaveEvent extends BaseEvent {
}
class BeforeRemoveEvent extends BaseEvent {
}
class AfterRemoveEvent extends BaseEvent {
}
let /** @type {?} */ ANREST_HTTP_EVENT_LISTENERS = new InjectionToken('anrest.http_event_listeners');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MetaInfo {
    /**
     * @param {?} _type
     */
    constructor(_type) {
        this._type = _type;
    }
    /**
     * @return {?}
     */
    get type() {
        return this._type;
    }
}
class PropertyMetaInfo extends MetaInfo {
}
class EventListenerMetaInfo extends MetaInfo {
    constructor() {
        super(...arguments);
        this.events = [];
    }
}
class EntityMetaInfo extends MetaInfo {
    constructor() {
        super(...arguments);
        this.id = 'id';
        this.headers = [];
        this.properties = {};
        this.subresources = {};
    }
    /**
     * @param {?=} object
     * @return {?}
     */
    getHeaders(object) {
        let /** @type {?} */ headers = new HttpHeaders();
        headers = headers.set('X-AnRest-Type', this.name);
        for (const /** @type {?} */ header of this.headers) {
            if (!header.dynamic || object) {
                headers = headers[header.append ? 'append' : 'set'](header.name, header.value.call(object));
            }
        }
        return headers;
    }
    /**
     * @return {?}
     */
    getPropertiesForSave() {
        const /** @type {?} */ included = [];
        for (const /** @type {?} */ property in this.properties) {
            if (!this.properties[property].excludeWhenSaving) {
                included.push(property);
            }
        }
        return included;
    }
}
class ServiceMetaInfo extends MetaInfo {
    /**
     * @param {?} type
     * @param {?} _entityMeta
     */
    constructor(type, _entityMeta) {
        super(type);
        this._entityMeta = _entityMeta;
    }
    /**
     * @return {?}
     */
    get entityMeta() {
        return this._entityMeta;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MetaService {
    /**
     * @param {?} key
     * @return {?}
     */
    static get(key) {
        return MetaService.map.get(key);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    static getByName(key) {
        for (const /** @type {?} */ meta of MetaService.map.values()) {
            if (meta instanceof EntityMetaInfo && meta.name === key) {
                return meta;
            }
        }
    }
    /**
     * @param {?} key
     * @return {?}
     */
    static getOrCreateForEntity(key) {
        let /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EntityMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    static getOrCreateForEventListener(key) {
        let /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new EventListenerMetaInfo(key);
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    }
    /**
     * @param {?} key
     * @param {?} property
     * @param {?=} type
     * @return {?}
     */
    static getOrCreateForProperty(key, property, type) {
        const /** @type {?} */ entityMeta = MetaService.getOrCreateForEntity(key.constructor);
        if (!entityMeta.properties[property]) {
            entityMeta.properties[property] = new PropertyMetaInfo(type || Reflect.getMetadata('design:type', key, property));
        }
        return entityMeta.properties[property];
    }
    /**
     * @param {?} key
     * @param {?} entity
     * @return {?}
     */
    static getOrCreateForHttpService(key, entity) {
        let /** @type {?} */ meta = MetaService.get(key);
        if (!meta) {
            meta = new ServiceMetaInfo(key, MetaService.getOrCreateForEntity(entity));
            MetaService.set(meta);
        }
        return /** @type {?} */ (meta);
    }
    /**
     * @param {?} meta
     * @return {?}
     */
    static set(meta) {
        MetaService.map.set(meta.type, meta);
    }
}
MetaService.map = new Map();

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class EventsService {
    /**
     * @param {?} handlers
     */
    constructor(handlers) {
        this.handlers = handlers;
        if (!Array.isArray(this.handlers)) {
            this.handlers = [];
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    handle(event) {
        this.handlers.forEach((handler) => {
            const /** @type {?} */ meta = MetaService.getOrCreateForEventListener(handler.constructor);
            for (const /** @type {?} */ eventMeta of meta.events) {
                if (eventMeta.type === event.constructor && eventMeta.entity === event.entity()) {
                    handler.handle(event);
                }
            }
        });
        return event;
    }
}
EventsService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
EventsService.ctorParameters = () => [
    { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_EVENT_LISTENERS,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ApiService {
    /**
     * @param {?} http
     * @param {?} events
     * @param {?} infoService
     * @param {?} config
     */
    constructor(http, events, infoService, config) {
        this.http = http;
        this.events = events;
        this.infoService = infoService;
        this.config = config;
        this.meta = MetaService.get(this.constructor);
    }
    /**
     * @param {?=} filter
     * @return {?}
     */
    getList(filter) {
        return this.doGet(this.meta.path, filter);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        return this.doGet(this.meta.path + '/' + id);
    }
    /**
     * @param {?} entity
     * @return {?}
     */
    reload(entity) {
        return this.doGet(this.infoService.getForEntity(entity).path);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getReference(id) {
        return this.doGet('&' + this.meta.path + '/' + id + '#' + this.meta.entityMeta.name);
    }
    /**
     * @param {?} path
     * @param {?=} filter
     * @return {?}
     */
    doGet(path, filter) {
        let /** @type {?} */ f;
        if (filter instanceof HttpParams) {
            f = filter;
        }
        else {
            f = new HttpParams();
            for (const /** @type {?} */ key in filter) {
                f = f.set(key, typeof filter[key] === 'boolean' ? (filter[key] ? 'true' : 'false') : String(filter[key]));
            }
        }
        this.events.handle(new BeforeGetEvent(path, this.meta.entityMeta.type, f));
        return this.http.get(this.config.baseUrl + path, { headers: this.meta.entityMeta.getHeaders(), params: f }).pipe(tap((data) => this.events.handle(new AfterGetEvent(data, this.meta.entityMeta.type))));
    }
    /**
     * @param {?} entity
     * @return {?}
     */
    save(entity) {
        this.events.handle(new BeforeSaveEvent(entity));
        let /** @type {?} */ body = {};
        if (this.meta.entityMeta.body) {
            body = this.meta.entityMeta.body(entity);
        }
        else {
            this.meta.entityMeta.getPropertiesForSave().forEach((property) => body[property] = entity[property]);
        }
        let /** @type {?} */ meta = this.infoService.getForEntity(entity);
        return this.http[meta ? 'put' : 'post'](meta ? this.config.baseUrl + meta.path : this.config.baseUrl + this.meta.path, body, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap((data) => {
            meta = this.infoService.getForEntity(data);
            if (!meta) {
                this.infoService.setForEntity(entity, meta);
            }
        }), tap(() => this.events.handle(new AfterSaveEvent(entity))));
    }
    /**
     * @param {?} entity
     * @return {?}
     */
    remove(entity) {
        this.events.handle(new BeforeRemoveEvent(entity));
        const /** @type {?} */ meta = this.infoService.getForEntity(entity);
        if (meta) {
            return this.http.delete(this.config.baseUrl + meta.path, { headers: this.meta.entityMeta.getHeaders(entity) }).pipe(tap(() => this.events.handle(new AfterRemoveEvent(entity))));
        }
    }
}
ApiService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ApiService.ctorParameters = () => [
    { type: HttpClient, },
    { type: EventsService, },
    { type: DataInfoService, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ReferenceInterceptor {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.config = config;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl || !this.isReferenceRequest(request)) {
            return next.handle(request);
        }
        return this.createReferenceResponse(request);
    }
    /**
     * @param {?} request
     * @return {?}
     */
    isReferenceRequest(request) {
        return request.method === 'GET' && request.url.slice(this.config.baseUrl.length).indexOf('&') === 0;
    }
    /**
     * @param {?} request
     * @return {?}
     */
    createReferenceResponse(request) {
        const /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        return of(new HttpResponse({
            body: { 'path': path.substring(1, path.indexOf('#')), 'meta': MetaService.getByName(path.substring(path.indexOf('#') + 1)) },
            status: 200,
            url: request.url
        })).pipe(map((response) => {
            if (response instanceof HttpResponse) {
                return response.clone({
                    headers: response.headers.set('Content-Type', '@reference')
                });
            }
        }));
    }
}
ReferenceInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ReferenceInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
let /** @type {?} */ ANREST_HTTP_DATA_NORMALIZERS = new InjectionToken('anrest.http_data_normalizers');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ResponseNode {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ObjectCollector {
    /**
     * @param {?} info
     */
    constructor(info) {
        this.info = info;
        this.map = new Map();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    set(data) {
        this.map.set(this.info.get(data).path, data);
    }
    /**
     * @param {?} data
     * @return {?}
     */
    remove(data) {
        const /** @type {?} */ info = this.info.get(data);
        if (info) {
            data = info.path;
        }
        this.map.delete(data);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        return this.map.get(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        return this.map.has(id);
    }
    /**
     * @return {?}
     */
    clear() {
        this.map.clear();
    }
}
ObjectCollector.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ObjectCollector.ctorParameters = () => [
    { type: DataInfoService, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ApiProcessor {
    /**
     * @param {?} injector
     * @param {?} info
     * @param {?} collector
     */
    constructor(injector, info, collector) {
        this.injector = injector;
        this.info = info;
        this.collector = collector;
    }
    /**
     * @param {?} node
     * @param {?} method
     * @return {?}
     */
    process(node, method) {
        return node.collection ? this.processCollection(node) : this.processObject(node);
    }
    /**
     * @param {?} node
     * @return {?}
     */
    processCollection(node) {
        let /** @type {?} */ data;
        if (node.info.path !== undefined) {
            data = this.collector.get(node.info.path) || new Collection(this.injector.get(node.meta.service), node.info);
        }
        else {
            data = new Collection(this.injector.get(node.meta.service), node.info);
        }
        data.length = 0;
        for (const /** @type {?} */ object of node.data) {
            data.push(this.processObject(object));
        }
        this.info.setForCollection(data, node.info);
        this.collector.set(data);
        return data;
    }
    /**
     * @param {?} node
     * @return {?}
     */
    processObject(node) {
        if (node.meta instanceof EntityMetaInfo) {
            const /** @type {?} */ object = this.collector.get(node.info.path) || new node.meta.type();
            for (const /** @type {?} */ property in node.meta.properties) {
                if (object.hasOwnProperty(property) && !node.data.hasOwnProperty(property)) {
                    continue;
                }
                if (node.data[property] instanceof ResponseNode) {
                    object[property] = node.meta.properties[property].isCollection ?
                        this.processCollection(node.data[property]) :
                        this.processObject(node.data[property]);
                }
                else {
                    const /** @type {?} */ type = node.meta.properties[property].type;
                    switch (type) {
                        case Number:
                            object[property] = Number(node.data[property]);
                            break;
                        case Date:
                            object[property] = new Date(node.data[property]);
                            break;
                        case String:
                        default:
                            object[property] = node.data[property];
                            break;
                    }
                }
            }
            for (const /** @type {?} */ property in node.meta.subresources) {
                const /** @type {?} */ service = this.injector.get(node.meta.subresources[property].meta.service);
                object[property] = service.doGet.bind(service, node.info.path + node.meta.subresources[property].path);
            }
            this.info.setForEntity(object, node.info);
            this.collector.set(object);
            return object;
        }
        else {
            return node.data;
        }
    }
}
ApiProcessor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ApiProcessor.ctorParameters = () => [
    { type: Injector, },
    { type: DataInfoService, },
    { type: ObjectCollector, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ApiInterceptor {
    /**
     * @param {?} config
     * @param {?} normalizers
     * @param {?} processor
     */
    constructor(config, normalizers, processor) {
        this.config = config;
        this.normalizers = normalizers;
        this.processor = processor;
        if (!Array.isArray(this.normalizers)) {
            this.normalizers = [];
        }
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        let /** @type {?} */ type;
        let /** @type {?} */ headers = request.headers;
        if (headers.has('X-AnRest-Type')) {
            type = headers.get('X-AnRest-Type');
            headers = headers.delete('x-AnRest-Type');
        }
        if (this.config.defaultHeaders) {
            for (const /** @type {?} */ header of this.config.defaultHeaders) {
                if (!headers.has(header.name) || header.append) {
                    headers = headers[header.append ? 'append' : 'set'](header.name, header.value);
                }
            }
        }
        request = request.clone({
            headers: headers
        });
        return next.handle(request).pipe(map((response) => {
            if (response instanceof HttpResponse) {
                const /** @type {?} */ contentType = response.headers.get('Content-Type');
                for (const /** @type {?} */ normalizer of this.normalizers) {
                    if (normalizer.supports(contentType)) {
                        return response.clone({ body: this.processor.process(normalizer.normalize(response, type), request.method) });
                    }
                }
            }
            return response;
        }));
    }
}
ApiInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
ApiInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [ANREST_HTTP_DATA_NORMALIZERS,] },] },
    { type: ApiProcessor, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
class ResponseCache {
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    replace(data, id) {
        this.remove(id);
        this.add(data, id);
    }
}
ResponseCache.decorators = [
    { type: Injectable },
];
let /** @type {?} */ ANREST_CACHE_SERVICES = new InjectionToken('anrest.cache_services');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NoResponseCache extends ResponseCache {
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        return false;
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    add(id, data) {
    }
    /**
     * @return {?}
     */
    clear() {
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
    }
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    addAlias(id, alias) {
    }
    /**
     * @param {?} data
     * @param {?} id
     * @return {?}
     */
    replace(data, id) {
    }
}
NoResponseCache.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CacheInterceptor {
    /**
     * @param {?} config
     * @param {?} info
     * @param {?} cacheServices
     */
    constructor(config, info, cacheServices) {
        this.config = config;
        this.info = info;
        this.cacheServices = cacheServices;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        const /** @type {?} */ path = request.urlWithParams.slice(this.config.baseUrl.length);
        if (request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl
            || (Array.isArray(this.config.excludedUrls) && this.config.excludedUrls.indexOf(request.url) !== -1)) {
            return next.handle(request);
        }
        const /** @type {?} */ cacheType = MetaService.getByName(request.headers.get('X-AnRest-Type')).cache || this.config.cache || NoResponseCache;
        let /** @type {?} */ cache;
        for (const /** @type {?} */ cacheService of this.cacheServices) {
            if (cacheService instanceof cacheType) {
                cache = cacheService;
                break;
            }
        }
        if (request.method !== 'GET') {
            cache.remove(path);
            return next.handle(request);
        }
        if (cache.has(path)) {
            return of(new HttpResponse({
                body: cache.get(path),
                status: 200,
                url: request.url
            }));
        }
        return next.handle(request).pipe(tap((response) => {
            if (response.body) {
                const /** @type {?} */ info = this.info.get(response.body);
                if (info) {
                    cache.add(info.path, response.body);
                    if (info && info.alias) {
                        cache.addAlias(info.path, info.alias);
                    }
                }
            }
        }));
    }
}
CacheInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
CacheInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
    { type: DataInfoService, },
    { type: Array, decorators: [{ type: Inject, args: [ANREST_CACHE_SERVICES,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MemoryResponseCache extends ResponseCache {
    /**
     * @param {?} collector
     * @param {?} config
     */
    constructor(collector, config) {
        super();
        this.collector = collector;
        this.config = config;
        this.times = {};
        this.aliases = {};
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    add(id, data) {
        this.times[id] = Date.now();
    }
    /**
     * @return {?}
     */
    clear() {
        this.collector.clear();
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        return this.collector.get(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        this.collector.remove(id);
    }
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    addAlias(id, alias) {
        this.aliases[alias] = id;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        const /** @type {?} */ time = this.aliases[id] ? this.times[this.aliases[id]] : this.times[id];
        return time && (Date.now() <= this.config.cacheTTL + time);
    }
}
MemoryResponseCache.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MemoryResponseCache.ctorParameters = () => [
    { type: ObjectCollector, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class ReferenceDataNormalizer {
    /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    normalize(response, type) {
        const /** @type {?} */ r = new ResponseNode();
        r.collection = false;
        r.info = new EntityInfo(response.body.path);
        r.meta = response.body.meta;
        r.data = {};
        return r;
    }
    /**
     * @param {?} type
     * @return {?}
     */
    supports(type) {
        return type === '@reference';
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LdJsonDataNormalizer {
    /**
     * @param {?} response
     * @param {?=} type
     * @return {?}
     */
    normalize(response, type) {
        return this.process(response.body);
    }
    /**
     * @param {?} type
     * @return {?}
     */
    supports(type) {
        return type.split(';')[0] === 'application/ld+json';
    }
    /**
     * @param {?} data
     * @param {?=} meta
     * @param {?=} collection
     * @return {?}
     */
    process(data, meta, collection) {
        const /** @type {?} */ r = new ResponseNode();
        r.collection = collection || data['@type'] === 'hydra:Collection';
        const /** @type {?} */ path = data['@id'];
        r.meta = meta || MetaService.getByName(r.collection ? /\/contexts\/(\w+)/g.exec(data['@context'])[1] : data['@type']);
        if (r.collection) {
            const /** @type {?} */ items = data['hydra:member'] || data;
            r.info = new CollectionInfo(data['hydra:view'] ? data['hydra:view']['@id'] : path);
            r.info.type = r.meta.name;
            if (data['hydra:view']) {
                r.info.first = data['hydra:view']['hydra:first'];
                r.info.previous = data['hydra:view']['hydra:previous'];
                r.info.next = data['hydra:view']['hydra:next'];
                r.info.last = data['hydra:view']['hydra:last'];
                if (r.info.path === r.info.first) {
                    r.info.alias = data['@id'];
                }
            }
            r.info.total = data['hydra:totalItems'] || items.length;
            const /** @type {?} */ regexPage = /page=(\d+)/g;
            let /** @type {?} */ matches;
            matches = regexPage.exec(r.info.path);
            r.info.page = matches ? Number(matches[1]) : 1;
            matches = regexPage.exec(r.info.last);
            r.info.lastPage = matches ? Number(matches[1]) : 1;
            r.data = [];
            for (const /** @type {?} */ object of items) {
                r.data.push(this.process(object));
            }
        }
        else {
            r.info = new EntityInfo(path);
            r.data = {};
            if (r.meta === undefined && Array.isArray(data)) {
                r.data = data;
            }
            else {
                for (const /** @type {?} */ key in data) {
                    if (key.indexOf('@') === 0 || key.indexOf('hydra:') === 0) {
                        continue;
                    }
                    const /** @type {?} */ propertyMeta = r.meta.properties[key];
                    r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                        this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
                }
            }
        }
        return r;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class JsonDataNormalizer {
    /**
     * @param {?} response
     * @param {?} type
     * @return {?}
     */
    normalize(response, type) {
        return this.process(response.body, MetaService.getByName(type), Array.isArray(response.body), response.headers);
    }
    /**
     * @param {?} type
     * @return {?}
     */
    supports(type) {
        return type.split(';')[0] === 'application/json';
    }
    /**
     * @param {?} data
     * @param {?} meta
     * @param {?} collection
     * @param {?=} headers
     * @return {?}
     */
    process(data, meta, collection, headers) {
        const /** @type {?} */ r = new ResponseNode();
        r.collection = collection;
        const /** @type {?} */ collectionPath = MetaService.get(meta.service) ? MetaService.get(meta.service).path : undefined;
        const /** @type {?} */ path = collectionPath ? (collection ? collectionPath : collectionPath + '/' + data[meta.id]) : undefined;
        r.meta = meta;
        if (r.collection) {
            r.info = new CollectionInfo(headers.get('X-Current-Page') || path);
            r.info.type = r.meta.name;
            r.info.first = headers.get('X-First-Page') || undefined;
            r.info.previous = headers.get('X-Prev-Page') || undefined;
            r.info.next = headers.get('X-Next-Page') || undefined;
            r.info.last = headers.get('X-Last-Page') || undefined;
            if (r.info.path === r.info.first) {
                r.info.alias = path;
            }
            r.info.total = headers.get('X-Total') || data.length;
            const /** @type {?} */ regexPage = /page=(\d+)/g;
            let /** @type {?} */ matches;
            matches = regexPage.exec(r.info.path);
            r.info.page = matches ? Number(matches[1]) : 1;
            matches = regexPage.exec(r.info.last);
            r.info.lastPage = matches ? Number(matches[1]) : 1;
            r.data = [];
            for (const /** @type {?} */ object of data) {
                r.data.push(this.process(object, r.meta, false));
            }
        }
        else {
            r.info = new EntityInfo(path);
            r.data = {};
            for (const /** @type {?} */ key in data) {
                const /** @type {?} */ propertyMeta = r.meta.properties[key];
                r.data[key] = (Array.isArray(data[key]) || (data[key] !== null && propertyMeta && MetaService.get(propertyMeta.type))) ?
                    this.process(data[key], MetaService.get(propertyMeta.type), propertyMeta.isCollection) : data[key];
            }
        }
        return r;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LocalStorageResponseCache extends ResponseCache {
    /**
     * @param {?} collector
     * @param {?} processor
     * @param {?} info
     * @param {?} config
     */
    constructor(collector, processor, info, config) {
        super();
        this.collector = collector;
        this.processor = processor;
        this.info = info;
        this.config = config;
        this.tokenPrefix = 'anrest:cache:';
        const /** @type {?} */ timestampPrefix = this.tokenPrefix + 'timestamp:';
        Object.keys(localStorage).filter((e) => e.indexOf(timestampPrefix) === 0).forEach((k) => {
            const /** @type {?} */ id = k.substring(timestampPrefix.length);
            const /** @type {?} */ timestamp = this.getTimestamp(id);
            if (!timestamp || timestamp + this.config.cacheTTL < Date.now()) {
                this.remove(id);
            }
        });
    }
    /**
     * @param {?} id
     * @return {?}
     */
    get(id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        if (this.has(id)) {
            return this.processor.process(this.getNode(id), 'GET');
        }
    }
    /**
     * @param {?} id
     * @param {?} data
     * @return {?}
     */
    add(id, data) {
        this.doAdd(id, data, true);
    }
    /**
     * @param {?} id
     * @param {?} alias
     * @return {?}
     */
    addAlias(id, alias) {
        localStorage.setItem(this.tokenPrefix + 'alias:' + alias, id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    has(id) {
        id = localStorage.getItem(this.tokenPrefix + 'alias:' + id) || id;
        const /** @type {?} */ complete = JSON.parse(localStorage.getItem(this.tokenPrefix + 'complete:' + id));
        const /** @type {?} */ timestamp = this.getTimestamp(id);
        return !!timestamp && complete && timestamp + this.config.cacheTTL >= Date.now();
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        localStorage.removeItem(this.tokenPrefix + id);
        localStorage.removeItem(this.tokenPrefix + 'type:' + id);
        localStorage.removeItem(this.tokenPrefix + 'collection:' + id);
        localStorage.removeItem(this.tokenPrefix + 'complete:' + id);
        localStorage.removeItem(this.tokenPrefix + 'references:' + id);
        this.removeTimestamp(id);
    }
    /**
     * @return {?}
     */
    clear() {
        Object.keys(localStorage).filter((e) => e.indexOf(this.tokenPrefix) === 0).forEach((k) => {
            localStorage.removeItem(k);
        });
    }
    /**
     * @param {?} id
     * @param {?} data
     * @param {?} complete
     * @return {?}
     */
    doAdd(id, data, complete) {
        const /** @type {?} */ info = this.info.get(data);
        let /** @type {?} */ values;
        if (info instanceof CollectionInfo) {
            values = [];
            for (const /** @type {?} */ item of data) {
                const /** @type {?} */ itemPath = this.info.get(item).path;
                values.push(itemPath);
                this.doAdd(itemPath, item, false);
            }
            localStorage.setItem(this.tokenPrefix + 'collection:' + id, JSON.stringify(info));
        }
        else {
            const /** @type {?} */ references = {};
            values = {};
            for (const /** @type {?} */ key in data) {
                let /** @type {?} */ itemInfo;
                if (typeof data[key] === 'object') {
                    itemInfo = this.info.get(data[key]);
                }
                if (itemInfo) {
                    this.doAdd(itemInfo.path, data[key], false);
                    references[key] = itemInfo.path;
                }
                else {
                    values[key] = data[key];
                }
            }
            localStorage.setItem(this.tokenPrefix + 'type:' + id, MetaService.get(data.constructor).name);
            localStorage.setItem(this.tokenPrefix + 'references:' + id, JSON.stringify(references));
        }
        localStorage.setItem(this.tokenPrefix + 'complete:' + id, JSON.stringify(complete));
        localStorage.setItem(this.tokenPrefix + id, JSON.stringify(values));
        this.setTimestamp(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getNode(id) {
        const /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + id));
        const /** @type {?} */ collectionInfo = this.getCollectionInfo(id);
        const /** @type {?} */ response = new ResponseNode();
        if (collectionInfo) {
            response.collection = true;
            response.info = collectionInfo;
            response.meta = MetaService.getByName(collectionInfo.type);
            response.data = [];
            for (const /** @type {?} */ itemId of data) {
                response.data.push(this.getNode(itemId));
            }
        }
        else {
            response.collection = false;
            response.info = new EntityInfo(id);
            response.meta = MetaService.getByName(localStorage.getItem(this.tokenPrefix + 'type:' + id));
            response.data = data;
            const /** @type {?} */ references = JSON.parse(localStorage.getItem(this.tokenPrefix + 'references:' + id));
            if (references) {
                for (const /** @type {?} */ key in references) {
                    response.data[key] = this.getNode(references[key]);
                }
            }
        }
        return response;
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getCollectionInfo(id) {
        const /** @type {?} */ data = JSON.parse(localStorage.getItem(this.tokenPrefix + 'collection:' + id));
        if (data) {
            const /** @type {?} */ info = new CollectionInfo(id);
            for (const /** @type {?} */ key in data) {
                if (key !== '_path') {
                    info[key] = data[key];
                }
            }
            return info;
        }
    }
    /**
     * @param {?} id
     * @return {?}
     */
    setTimestamp(id) {
        localStorage.setItem(this.tokenPrefix + 'timestamp:' + id, String(Date.now()));
    }
    /**
     * @param {?} id
     * @return {?}
     */
    getTimestamp(id) {
        return Number(localStorage.getItem(this.tokenPrefix + 'timestamp:' + id));
    }
    /**
     * @param {?} id
     * @return {?}
     */
    removeTimestamp(id) {
        return localStorage.removeItem(this.tokenPrefix + 'timestamp:' + id);
    }
}
LocalStorageResponseCache.decorators = [
    { type: Injectable },
];
/** @nocollapse */
LocalStorageResponseCache.ctorParameters = () => [
    { type: ObjectCollector, },
    { type: ApiProcessor, },
    { type: DataInfoService, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
class AuthService {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
class AuthTokenProviderService {
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class LocalStorageAuthTokenProviderService {
    constructor() {
        this.tokenName = 'anrest:auth_token';
    }
    /**
     * @return {?}
     */
    get() {
        return localStorage.getItem(this.tokenName);
    }
    /**
     * @param {?} token
     * @return {?}
     */
    set(token) {
        localStorage.setItem(this.tokenName, token);
    }
    /**
     * @return {?}
     */
    remove() {
        localStorage.removeItem(this.tokenName);
    }
    /**
     * @return {?}
     */
    isSet() {
        return localStorage.getItem(this.tokenName) != null;
    }
}
LocalStorageAuthTokenProviderService.decorators = [
    { type: Injectable },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AuthInterceptor {
    /**
     * @param {?} auth
     * @param {?} config
     */
    constructor(auth, config) {
        this.auth = auth;
        this.config = config;
    }
    /**
     * @param {?} request
     * @param {?} next
     * @return {?}
     */
    intercept(request, next) {
        if (!this.config.authUrl || request.url.indexOf(this.config.baseUrl) !== 0 || request.url === this.config.authUrl) {
            return next.handle(request);
        }
        if (this.auth.isSignedIn()) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.auth.getToken()}`
                }
            });
        }
        return next.handle(request);
    }
}
AuthInterceptor.decorators = [
    { type: Injectable },
];
/** @nocollapse */
AuthInterceptor.ctorParameters = () => [
    { type: AuthService, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class JwtAuthService {
    /**
     * @param {?} http
     * @param {?} token
     * @param {?} config
     */
    constructor(http, token, config) {
        this.http = http;
        this.token = token;
        this.config = config;
    }
    /**
     * @return {?}
     */
    getToken() {
        return this.token.get();
    }
    /**
     * @param {?} token
     * @return {?}
     */
    setToken(token) {
        this.token.set(token);
    }
    /**
     * @return {?}
     */
    remove() {
        this.token.remove();
    }
    /**
     * @return {?}
     */
    isSignedIn() {
        return this.token.isSet();
    }
    /**
     * @return {?}
     */
    getInfo() {
        return call(this.getToken());
    }
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    signIn(username, password) {
        return this.http.post(this.config.authUrl, {
            login: username,
            password: password
        }).pipe(tap((data) => {
            this.setToken(data.token);
        }));
    }
}
JwtAuthService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
JwtAuthService.ctorParameters = () => [
    { type: HttpClient, },
    { type: AuthTokenProviderService, },
    { type: undefined, decorators: [{ type: Inject, args: [AnRestConfig,] },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?=} info
 * @return {?}
 */
function Resource(info = {}) {
    return (target) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        meta.name = info.name || target.name;
        meta.cache = info.cache;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?=} info
 * @return {?}
 */
function Property(info = {}) {
    return (target, key) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForProperty(target, key, info.type);
        meta.isCollection = info.collection || false;
        meta.excludeWhenSaving = info.excludeWhenSaving || false;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} entity
 * @param {?=} path
 * @return {?}
 */
function HttpService(entity, path) {
    return (target) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForHttpService(target, entity);
        meta.path = path || '/' + plural(entity.name).replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase();
        meta.entityMeta.service = target;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} headerName
 * @param {?=} append
 * @return {?}
 */
function Header(headerName, append) {
    return (target, key, value = undefined) => {
        MetaService.getOrCreateForEntity(target.constructor).headers.push({
            name: headerName,
            value: value ? value.value : function () { return this[key]; },
            append: append || false,
            dynamic: true
        });
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} headers
 * @return {?}
 */
function Headers(headers) {
    return (target) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForEntity(target);
        for (const /** @type {?} */ header of headers) {
            meta.headers.push({
                name: header.name,
                value: () => header.value,
                append: header.append || false,
                dynamic: false
            });
        }
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @return {?}
 */
function Body() {
    return (target, key = undefined, value = undefined) => {
        MetaService.getOrCreateForEntity(target.constructor).body = (object) => {
            return (value ? value.value : function () { return this[key]; }).call(object);
        };
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @param {?} type
 * @param {?=} path
 * @return {?}
 */
function Subresource(type, path) {
    return (target, key, value) => {
        MetaService.getOrCreateForEntity(target.constructor).subresources[key] = {
            meta: MetaService.getOrCreateForEntity(type),
            path: path || '/' + key.replace(/\.?([A-Z])/g, '-$1').replace(/^-/, '').toLowerCase()
        };
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @return {?}
 */
function Id() {
    return (target, key) => {
        const /** @type {?} */ meta = MetaService.getOrCreateForEntity(target.constructor);
        meta.id = key;
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
class Loader {
    /**
     * @param {?} service
     * @param {?=} waitFor
     * @param {?=} replay
     */
    constructor(service, waitFor = 0, replay = 0) {
        this.service = service;
        this.fields = {};
        this.loading = false;
        this.observable = combineLatest(this.getSubjects())
            .pipe(replay ? shareReplay(replay) : share(), debounceTime(waitFor), distinctUntilChanged((d1, d2) => this.checkDistinct(d1, d2)), map((values) => this.mapParams(values)), mergeMap((data) => {
            this.loading = true;
            return this.getServiceObservable(data).pipe(tap((v) => {
                this.lastData = v;
                this.loading = false;
            }));
        }));
    }
    /**
     * @param {?} name
     * @return {?}
     */
    field(name) {
        return this.fields[name];
    }
    /**
     * @return {?}
     */
    get data() {
        return this.observable;
    }
    /**
     * @return {?}
     */
    get isLoading() {
        return this.loading;
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    getServiceObservable([data]) {
        return this.service.getList(data);
    }
    /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    checkDistinct(d1, d2) {
        const /** @type {?} */ offset = d1.length - this.getAvailableFields().length;
        for (let /** @type {?} */ i = d1.length; i >= 0; i--) {
            if ((i < offset && d2[i]) || (i >= offset && d1[i] !== d2[i])) {
                if (i >= offset) {
                    d2.fill(undefined, 0, offset);
                }
                return false;
            }
        }
        return true;
    }
    /**
     * @param {?} values
     * @return {?}
     */
    mapParams(values) {
        const /** @type {?} */ fields = this.getAvailableFields();
        const /** @type {?} */ offset = values.length - fields.length;
        const /** @type {?} */ data = {};
        values.slice(offset).forEach((v, i) => {
            if (v !== undefined && v !== null && v !== '') {
                if (typeof v === 'boolean') {
                    v = v ? 'true' : 'false';
                }
                data[fields[i]] = String(v);
            }
        });
        return values.slice(0, offset).concat([data]);
    }
    /**
     * @return {?}
     */
    getSubjects() {
        return this.getAvailableFields().map((name) => {
            return this.fields[name] = new BehaviorSubject(undefined);
        });
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
class InfiniteScrollLoader extends Loader {
    /**
     * @return {?}
     */
    loadMore() {
        this.loadMoreSubject.next(true);
    }
    /**
     * @return {?}
     */
    hasMore() {
        return this.lastData && this.lastData.hasMore();
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    getServiceObservable([loadMore, data]) {
        return loadMore ? this.lastData.loadMore() : super.getServiceObservable([data]);
    }
    /**
     * @return {?}
     */
    getSubjects() {
        this.loadMoreSubject = new BehaviorSubject(false);
        const /** @type {?} */ subjects = super.getSubjects();
        subjects.unshift(this.loadMoreSubject);
        return subjects;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @abstract
 */
class PageLoader extends Loader {
    /**
     * @return {?}
     */
    first() {
        if (!this.lastData || !this.lastData.hasFirst()) {
            return;
        }
        this.pageSubject.next(this.lastData.firstPath());
    }
    /**
     * @return {?}
     */
    previous() {
        if (!this.lastData || !this.lastData.hasPrevious()) {
            return;
        }
        this.pageSubject.next(this.lastData.previousPath());
    }
    /**
     * @return {?}
     */
    next() {
        if (!this.lastData || !this.lastData.hasNext()) {
            return;
        }
        this.pageSubject.next(this.lastData.nextPath());
    }
    /**
     * @return {?}
     */
    last() {
        if (!this.lastData || !this.lastData.hasLast()) {
            return;
        }
        this.pageSubject.next(this.lastData.lastPath());
    }
    /**
     * @return {?}
     */
    hasFirst() {
        return this.lastData && this.lastData.hasFirst();
    }
    /**
     * @return {?}
     */
    hasPrevious() {
        return this.lastData && this.lastData.hasPrevious();
    }
    /**
     * @return {?}
     */
    hasNext() {
        return this.lastData && this.lastData.hasNext();
    }
    /**
     * @return {?}
     */
    hasLast() {
        return this.lastData && this.lastData.hasLast();
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    getServiceObservable([page, data]) {
        return page ? this.service.doGet(page, data) : super.getServiceObservable([data]);
    }
    /**
     * @return {?}
     */
    getSubjects() {
        this.pageSubject = new BehaviorSubject(undefined);
        const /** @type {?} */ subjects = super.getSubjects();
        subjects.unshift(this.pageSubject);
        return subjects;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AnRestModule {
    /**
     * @param {?=} config
     * @return {?}
     */
    static config(config) {
        return {
            ngModule: AnRestModule,
            providers: [
                {
                    provide: AnRestConfig,
                    useValue: config,
                },
                EventsService,
                DataInfoService,
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: LocalStorageResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: MemoryResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: NoResponseCache,
                    multi: true
                },
                {
                    provide: ResponseCache,
                    useClass: config.cache || NoResponseCache
                },
                {
                    provide: AuthTokenProviderService,
                    useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                },
                {
                    provide: AuthService,
                    useClass: config.authService || JwtAuthService
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ReferenceInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CacheInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ApiInterceptor,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: ReferenceDataNormalizer,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: LdJsonDataNormalizer,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: JsonDataNormalizer,
                    multi: true
                },
                ObjectCollector,
                ApiProcessor,
                ApiService
            ]
        };
    }
    /**
     * @param {?=} config
     * @return {?}
     */
    static configWithoutNormalizers(config) {
        return {
            ngModule: AnRestModule,
            providers: [
                {
                    provide: AnRestConfig,
                    useValue: config,
                },
                EventsService,
                DataInfoService,
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: LocalStorageResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: MemoryResponseCache,
                    multi: true
                },
                {
                    provide: ANREST_CACHE_SERVICES,
                    useClass: NoResponseCache,
                    multi: true
                },
                {
                    provide: ResponseCache,
                    useClass: config.cache || NoResponseCache
                },
                {
                    provide: AuthTokenProviderService,
                    useClass: config.tokenProvider || LocalStorageAuthTokenProviderService
                },
                {
                    provide: AuthService,
                    useClass: config.authService || JwtAuthService
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ReferenceInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CacheInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ApiInterceptor,
                    multi: true
                },
                {
                    provide: ANREST_HTTP_DATA_NORMALIZERS,
                    useClass: ReferenceDataNormalizer,
                    multi: true
                },
                ObjectCollector,
                ApiProcessor,
                ApiService
            ]
        };
    }
}
AnRestModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    HttpClientModule,
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { AnRestConfig, Collection, ApiService, EntityInfo, CollectionInfo, DataInfoService, ReferenceInterceptor, ApiInterceptor, ApiProcessor, ObjectCollector, CacheInterceptor, ResponseCache, ANREST_CACHE_SERVICES, NoResponseCache, MemoryResponseCache, LocalStorageResponseCache, AuthService, AuthTokenProviderService, LocalStorageAuthTokenProviderService, AuthInterceptor, JwtAuthService, Resource, Property, HttpService, Header, Headers, Body, Subresource, Id, EventsService, BaseEvent, BeforeGetEvent, AfterGetEvent, BeforeSaveEvent, AfterSaveEvent, BeforeRemoveEvent, AfterRemoveEvent, ANREST_HTTP_EVENT_LISTENERS, MetaService, MetaInfo, PropertyMetaInfo, EventListenerMetaInfo, EntityMetaInfo, ServiceMetaInfo, ResponseNode, ANREST_HTTP_DATA_NORMALIZERS, ReferenceDataNormalizer, LdJsonDataNormalizer, JsonDataNormalizer, Loader, InfiniteScrollLoader, PageLoader, AnRestModule, AuthInterceptor as ɵm, AuthService as ɵk, AuthTokenProviderService as ɵi, JwtAuthService as ɵl, LocalStorageAuthTokenProviderService as ɵj, ANREST_CACHE_SERVICES as ɵd, CacheInterceptor as ɵo, LocalStorageResponseCache as ɵe, MemoryResponseCache as ɵf, NoResponseCache as ɵg, ResponseCache as ɵh, AnRestConfig as ɵa, ApiInterceptor as ɵp, ApiProcessor as ɵv, ApiService as ɵw, DataInfoService as ɵc, ObjectCollector as ɵu, ReferenceInterceptor as ɵn, EventsService as ɵb, ANREST_HTTP_DATA_NORMALIZERS as ɵq, JsonDataNormalizer as ɵt, LdJsonDataNormalizer as ɵs, ReferenceDataNormalizer as ɵr };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5yZXN0LWxpYi5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vQGFucmVzdC9saWIvY29yZS9jb25maWcudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvZGF0YS1pbmZvLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvY29sbGVjdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvZXZlbnRzL2V2ZW50cy50cyIsIm5nOi8vQGFucmVzdC9saWIvbWV0YS9tZXRhLWluZm8udHMiLCJuZzovL0BhbnJlc3QvbGliL21ldGEvbWV0YS1zZXJ2aWNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9ldmVudHMvZXZlbnRzLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvYXBpLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvcmVmZXJlbmNlLmludGVyY2VwdG9yLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9yZXNwb25zZS9kYXRhLW5vcm1hbGl6ZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3Jlc3BvbnNlL3Jlc3BvbnNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jb3JlL29iamVjdC1jb2xsZWN0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NvcmUvYXBpLnByb2Nlc3Nvci50cyIsIm5nOi8vQGFucmVzdC9saWIvY29yZS9hcGkuaW50ZXJjZXB0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NhY2hlL3Jlc3BvbnNlLWNhY2hlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jYWNoZS9uby1yZXNwb25zZS1jYWNoZS50cyIsIm5nOi8vQGFucmVzdC9saWIvY2FjaGUvY2FjaGUuaW50ZXJjZXB0b3IudHMiLCJuZzovL0BhbnJlc3QvbGliL2NhY2hlL21lbW9yeS1yZXNwb25zZS1jYWNoZS50cyIsIm5nOi8vQGFucmVzdC9saWIvcmVzcG9uc2UvcmVmZXJlbmNlLWRhdGEtbm9ybWFsaXplci50cyIsIm5nOi8vQGFucmVzdC9saWIvcmVzcG9uc2UvbGQtanNvbi1kYXRhLW5vcm1hbGl6ZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3Jlc3BvbnNlL2pzb24tZGF0YS1ub3JtYWxpemVyLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9jYWNoZS9sb2NhbC1zdG9yYWdlLXJlc3BvbnNlLWNhY2hlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hdXRoL2F1dGguc2VydmljZS50cyIsIm5nOi8vQGFucmVzdC9saWIvYXV0aC9hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2F1dGgvbG9jYWwtc3RvcmFnZS1hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UudHMiLCJuZzovL0BhbnJlc3QvbGliL2F1dGgvYXV0aC5pbnRlcmNlcHRvci50cyIsIm5nOi8vQGFucmVzdC9saWIvYXV0aC9qd3QtYXV0aC5zZXJ2aWNlLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9yZXNvdXJjZS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9wcm9wZXJ0eS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9odHRwLXNlcnZpY2UuYW5ub3RhdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvYW5ub3RhdGlvbnMvaGVhZGVyLmFubm90YXRpb24udHMiLCJuZzovL0BhbnJlc3QvbGliL2Fubm90YXRpb25zL2hlYWRlcnMuYW5ub3RhdGlvbi50cyIsIm5nOi8vQGFucmVzdC9saWIvYW5ub3RhdGlvbnMvYm9keS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9zdWJyZXNvdXJjZS5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbm5vdGF0aW9ucy9pZC5hbm5vdGF0aW9uLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi91dGlscy9sb2FkZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3V0aWxzL2luZmluaXRlLXNjcm9sbC1sb2FkZXIudHMiLCJuZzovL0BhbnJlc3QvbGliL3V0aWxzL3BhZ2UtbG9hZGVyLnRzIiwibmc6Ly9AYW5yZXN0L2xpYi9hbnJlc3QubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQXBpQ29uZmlnIHtcbiAgcmVzcG9uc2VUeXBlPzogc3RyaW5nO1xuICBiYXNlVXJsOiBzdHJpbmc7XG4gIGF1dGhVcmw/OiBzdHJpbmc7XG4gIGNhY2hlPzogYW55O1xuICB0b2tlblByb3ZpZGVyPzogYW55O1xuICBhdXRoU2VydmljZT86IGFueTtcbiAgY2FjaGVUVEw/OiBudW1iZXI7XG4gIGV4Y2x1ZGVkVXJscz86IHN0cmluZ1tdO1xuICBkZWZhdWx0SGVhZGVycz86IHsgbmFtZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBhcHBlbmQ/OiBib29sZWFufVtdO1xufVxuXG5leHBvcnQgY29uc3QgQW5SZXN0Q29uZmlnID0gbmV3IEluamVjdGlvblRva2VuPEFwaUNvbmZpZz4oJ2FucmVzdC5hcGlfY29uZmlnJyk7XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBjbGFzcyBFbnRpdHlJbmZvIHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9wYXRoKSB7fVxuXG4gIGdldCBwYXRoKCkge1xuICAgIHJldHVybiB0aGlzLl9wYXRoO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBDb2xsZWN0aW9uSW5mbyB7XG4gIHB1YmxpYyB0b3RhbDogbnVtYmVyO1xuICBwdWJsaWMgcGFnZTogbnVtYmVyO1xuICBwdWJsaWMgbGFzdFBhZ2U6IG51bWJlcjtcbiAgcHVibGljIGZpcnN0OiBzdHJpbmc7XG4gIHB1YmxpYyBwcmV2aW91czogc3RyaW5nO1xuICBwdWJsaWMgbmV4dDogc3RyaW5nO1xuICBwdWJsaWMgbGFzdDogc3RyaW5nO1xuICBwdWJsaWMgYWxpYXM6IHN0cmluZztcbiAgcHVibGljIHR5cGU6IGFueTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9wYXRoKSB7fVxuXG4gIGdldCBwYXRoKCkge1xuICAgIHJldHVybiB0aGlzLl9wYXRoO1xuICB9XG59XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBEYXRhSW5mb1NlcnZpY2Uge1xuXG4gIGdldChvYmplY3QpOiBhbnkge1xuICAgIHJldHVybiBSZWZsZWN0LmdldE1ldGFkYXRhKCdhbnJlc3Q6aW5mbycsIG9iamVjdCk7XG4gIH1cblxuICBzZXQob2JqZWN0LCBpbmZvOiBhbnkpIHtcbiAgICBSZWZsZWN0LmRlZmluZU1ldGFkYXRhKCdhbnJlc3Q6aW5mbycsIGluZm8sIG9iamVjdCk7XG4gIH1cblxuICBnZXRGb3JDb2xsZWN0aW9uKG9iamVjdCk6IENvbGxlY3Rpb25JbmZvIHtcbiAgICByZXR1cm4gPENvbGxlY3Rpb25JbmZvPnRoaXMuZ2V0KG9iamVjdCk7XG4gIH1cblxuICBnZXRGb3JFbnRpdHkob2JqZWN0KTogRW50aXR5SW5mbyB7XG4gICAgcmV0dXJuIDxFbnRpdHlJbmZvPnRoaXMuZ2V0KG9iamVjdCk7XG4gIH1cblxuICBzZXRGb3JDb2xsZWN0aW9uKG9iamVjdCwgaW5mbzogQ29sbGVjdGlvbkluZm8pIHtcbiAgICB0aGlzLnNldChvYmplY3QsIGluZm8pO1xuICB9XG5cbiAgc2V0Rm9yRW50aXR5KG9iamVjdCwgaW5mbzogRW50aXR5SW5mbykge1xuICAgIHRoaXMuc2V0KG9iamVjdCwgaW5mbyk7XG4gIH1cbn1cbiIsImltcG9ydCB7IENvbGxlY3Rpb25JbmZvIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQgeyBBcGlTZXJ2aWNlIH0gZnJvbSAnLi9hcGkuc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmV4cG9ydCBjbGFzcyBDb2xsZWN0aW9uPFQ+IGV4dGVuZHMgQXJyYXk8VD4ge1xuXG4gIHN0YXRpYyBtZXJnZTxUPihjMTogQ29sbGVjdGlvbjxUPiwgYzI6IENvbGxlY3Rpb248VD4pOiBDb2xsZWN0aW9uPFQ+IHtcbiAgICBjb25zdCBpbmZvID0gbmV3IENvbGxlY3Rpb25JbmZvKGMxLmluZm8ucGF0aCk7XG4gICAgaW5mby50eXBlID0gYzEuaW5mby50eXBlO1xuICAgIGluZm8ubmV4dCA9IGMyLmluZm8ubmV4dDtcbiAgICBpbmZvLnByZXZpb3VzID0gYzEuaW5mby5wcmV2aW91cztcbiAgICBpbmZvLmZpcnN0ID0gYzEuaW5mby5maXJzdDtcbiAgICBpbmZvLmxhc3QgPSBjMS5pbmZvLmxhc3Q7XG4gICAgaW5mby50b3RhbCA9IGMxLmluZm8udG90YWw7XG4gICAgaW5mby5wYWdlID0gYzEuaW5mby5wYWdlO1xuICAgIGluZm8ubGFzdFBhZ2UgPSBjMS5pbmZvLmxhc3RQYWdlO1xuICAgIGNvbnN0IGMgPSBuZXcgQ29sbGVjdGlvbjxUPihjMS5zZXJ2aWNlLCBpbmZvKTtcbiAgICBjMS5mb3JFYWNoKCh2KSA9PiBjLnB1c2godikpO1xuICAgIGMyLmZvckVhY2goKHYpID0+IGMucHVzaCh2KSk7XG4gICAgcmV0dXJuIGM7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNlcnZpY2U6IEFwaVNlcnZpY2UsIHByaXZhdGUgaW5mbzogQ29sbGVjdGlvbkluZm8sIC4uLml0ZW1zOiBUW10pIHtcbiAgICBzdXBlciguLi5pdGVtcyk7XG4gICAgT2JqZWN0LnNldFByb3RvdHlwZU9mKHRoaXMsIENvbGxlY3Rpb24ucHJvdG90eXBlKTtcbiAgfVxuXG4gIGdldCB0b3RhbCgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLmluZm8udG90YWw7XG4gIH1cblxuICBnZXQgcGFnZSgpOiBudW1iZXIge1xuICAgIHJldHVybiB0aGlzLmluZm8ucGFnZTtcbiAgfVxuXG4gIGdldCBwYWdlVG90YWwoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gdGhpcy5pbmZvLmxhc3RQYWdlO1xuICB9XG5cbiAgZmlyc3QoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gPE9ic2VydmFibGU8VFtdPj50aGlzLnNlcnZpY2UuZG9HZXQodGhpcy5pbmZvLmZpcnN0KTtcbiAgfVxuXG4gIHByZXZpb3VzKCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5wcmV2aW91cyk7XG4gIH1cblxuICBuZXh0KCk6IE9ic2VydmFibGU8VFtdPiB7XG4gICAgcmV0dXJuIDxPYnNlcnZhYmxlPFRbXT4+dGhpcy5zZXJ2aWNlLmRvR2V0KHRoaXMuaW5mby5uZXh0KTtcbiAgfVxuXG4gIGxhc3QoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gPE9ic2VydmFibGU8VFtdPj50aGlzLnNlcnZpY2UuZG9HZXQodGhpcy5pbmZvLmxhc3QpO1xuICB9XG5cbiAgZmlyc3RQYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5maXJzdDtcbiAgfVxuXG4gIHByZXZpb3VzUGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ucHJldmlvdXM7XG4gIH1cblxuICBuZXh0UGF0aCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmluZm8ubmV4dDtcbiAgfVxuXG4gIGxhc3RQYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuaW5mby5sYXN0O1xuICB9XG5cbiAgbG9hZE1vcmUoKTogT2JzZXJ2YWJsZTxUW10+IHtcbiAgICByZXR1cm4gdGhpcy5uZXh0KCkucGlwZShcbiAgICAgIG1hcCgocmVzdWx0OiBDb2xsZWN0aW9uPFQ+KSA9PiBDb2xsZWN0aW9uLm1lcmdlPFQ+KHRoaXMsIHJlc3VsdCkpXG4gICAgKTtcbiAgfVxuXG4gIGhhc01vcmUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuaGFzTmV4dCgpO1xuICB9XG5cbiAgaGFzTmV4dCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gISF0aGlzLmluZm8ubmV4dDtcbiAgfVxuXG4gIGhhc1ByZXZpb3VzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAhIXRoaXMuaW5mby5wcmV2aW91cztcbiAgfVxuXG4gIGhhc0ZpcnN0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc1ByZXZpb3VzKCk7XG4gIH1cblxuICBoYXNMYXN0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmhhc05leHQoKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRXZlbnQge1xuICBlbnRpdHkoKTogRnVuY3Rpb247XG4gIGRhdGEoKTogYW55O1xufVxuXG5leHBvcnQgY2xhc3MgQmFzZUV2ZW50IGltcGxlbWVudHMgRXZlbnQge1xuICBfZGF0YT86IGFueTtcbiAgcHJvdGVjdGVkIHJlYWRvbmx5IHR5cGU6IEZ1bmN0aW9uO1xuXG4gIGNvbnN0cnVjdG9yKGRhdGE6IGFueSwgdHlwZT86IEZ1bmN0aW9uKSB7XG4gICAgdGhpcy5fZGF0YSA9IGRhdGE7XG4gICAgdGhpcy50eXBlID0gdHlwZTtcbiAgfVxuXG4gIGRhdGEoKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5fZGF0YTtcbiAgfVxuXG4gIGVudGl0eSgpOiBGdW5jdGlvbiB7XG4gICAgcmV0dXJuIHRoaXMudHlwZSB8fCAoQXJyYXkuaXNBcnJheSh0aGlzLl9kYXRhKSA/IHRoaXMuX2RhdGFbMF0uY29uc3RydWN0b3IgOiB0aGlzLl9kYXRhLmNvbnN0cnVjdG9yKTtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgQmVmb3JlR2V0RXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge1xuICBwcml2YXRlIHJlYWRvbmx5IF9maWx0ZXI6IEh0dHBQYXJhbXM7XG5cbiAgY29uc3RydWN0b3IocGF0aDogc3RyaW5nLCB0eXBlPzogRnVuY3Rpb24sIGZpbHRlcj86IEh0dHBQYXJhbXMpIHtcbiAgICBzdXBlcihwYXRoLCB0eXBlKTtcbiAgICB0aGlzLl9maWx0ZXIgPSBmaWx0ZXI7XG4gIH1cblxuICBlbnRpdHkoKTogRnVuY3Rpb24ge1xuICAgIHJldHVybiB0aGlzLnR5cGU7XG4gIH1cblxuICBmaWx0ZXIoKTogSHR0cFBhcmFtcyB7XG4gICAgcmV0dXJuIHRoaXMuX2ZpbHRlcjtcbiAgfVxufVxuZXhwb3J0IGNsYXNzIEFmdGVyR2V0RXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBCZWZvcmVTYXZlRXZlbnQgZXh0ZW5kcyBCYXNlRXZlbnQge31cbmV4cG9ydCBjbGFzcyBBZnRlclNhdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuZXhwb3J0IGNsYXNzIEJlZm9yZVJlbW92ZUV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50IHt9XG5leHBvcnQgY2xhc3MgQWZ0ZXJSZW1vdmVFdmVudCBleHRlbmRzIEJhc2VFdmVudCB7fVxuXG5leHBvcnQgaW50ZXJmYWNlIEV2ZW50TGlzdGVuZXIge1xuICBoYW5kbGUoZXZlbnQ6IEV2ZW50KTtcbn1cblxuZXhwb3J0IGxldCBBTlJFU1RfSFRUUF9FVkVOVF9MSVNURU5FUlMgPSBuZXcgSW5qZWN0aW9uVG9rZW48RXZlbnRMaXN0ZW5lcltdPignYW5yZXN0Lmh0dHBfZXZlbnRfbGlzdGVuZXJzJyk7XG4iLCJpbXBvcnQgeyBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuZXhwb3J0IGNsYXNzIE1ldGFJbmZvIHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF90eXBlOiBGdW5jdGlvbikge31cblxuICBnZXQgdHlwZSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLl90eXBlO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBQcm9wZXJ0eU1ldGFJbmZvIGV4dGVuZHMgTWV0YUluZm8ge1xuICBwdWJsaWMgaXNDb2xsZWN0aW9uOiBib29sZWFuO1xuICBwdWJsaWMgZXhjbHVkZVdoZW5TYXZpbmc6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBjbGFzcyBFdmVudExpc3RlbmVyTWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBldmVudHM6IHsgZW50aXR5OiBGdW5jdGlvbiwgdHlwZTogRnVuY3Rpb24gfVtdID0gW107XG59XG5cbmV4cG9ydCBjbGFzcyBFbnRpdHlNZXRhSW5mbyBleHRlbmRzIE1ldGFJbmZvIHtcbiAgcHVibGljIG5hbWU6IHN0cmluZztcbiAgcHVibGljIGlkID0gJ2lkJztcbiAgcHVibGljIHNlcnZpY2U6IEZ1bmN0aW9uO1xuICBwdWJsaWMgaGVhZGVyczogeyBuYW1lOiBzdHJpbmcsIHZhbHVlOiAoKSA9PiBzdHJpbmcsIGFwcGVuZDogYm9vbGVhbiwgZHluYW1pYzogYm9vbGVhbiB9W10gPSBbXTtcbiAgcHVibGljIHByb3BlcnRpZXM6IHsgW2luZGV4OiBzdHJpbmddOiBQcm9wZXJ0eU1ldGFJbmZvIH0gPSB7fTtcbiAgcHVibGljIHN1YnJlc291cmNlczogeyBbaW5kZXg6IHN0cmluZ106IHsgbWV0YTogRW50aXR5TWV0YUluZm8sIHBhdGg6IHN0cmluZ30gfSA9IHt9O1xuICBwdWJsaWMgYm9keTogKG9iamVjdDogYW55KSA9PiBzdHJpbmc7XG4gIHB1YmxpYyBjYWNoZTogRnVuY3Rpb247XG5cbiAgcHVibGljIGdldEhlYWRlcnMob2JqZWN0PzogYW55KTogSHR0cEhlYWRlcnMge1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XG4gICAgaGVhZGVycyA9IGhlYWRlcnMuc2V0KCdYLUFuUmVzdC1UeXBlJywgdGhpcy5uYW1lKTtcbiAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiB0aGlzLmhlYWRlcnMpIHtcbiAgICAgIGlmICghaGVhZGVyLmR5bmFtaWMgfHwgb2JqZWN0KSB7XG4gICAgICAgIGhlYWRlcnMgPSBoZWFkZXJzW2hlYWRlci5hcHBlbmQgPyAnYXBwZW5kJyA6ICdzZXQnXShoZWFkZXIubmFtZSwgaGVhZGVyLnZhbHVlLmNhbGwob2JqZWN0KSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBoZWFkZXJzO1xuICB9XG5cbiAgcHVibGljIGdldFByb3BlcnRpZXNGb3JTYXZlKCkge1xuICAgIGNvbnN0IGluY2x1ZGVkID0gW107XG4gICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiB0aGlzLnByb3BlcnRpZXMpIHtcbiAgICAgIGlmICghdGhpcy5wcm9wZXJ0aWVzW3Byb3BlcnR5XS5leGNsdWRlV2hlblNhdmluZykge1xuICAgICAgICBpbmNsdWRlZC5wdXNoKHByb3BlcnR5KTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGluY2x1ZGVkO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTZXJ2aWNlTWV0YUluZm8gZXh0ZW5kcyBNZXRhSW5mbyB7XG4gIHB1YmxpYyBwYXRoOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IodHlwZTogRnVuY3Rpb24sIHByaXZhdGUgX2VudGl0eU1ldGE6IEVudGl0eU1ldGFJbmZvKSB7XG4gICAgc3VwZXIodHlwZSk7XG4gIH1cblxuICBnZXQgZW50aXR5TWV0YSgpOiBFbnRpdHlNZXRhSW5mbyB7XG4gICAgcmV0dXJuIHRoaXMuX2VudGl0eU1ldGE7XG4gIH1cbn1cbiIsImltcG9ydCB7IEVudGl0eU1ldGFJbmZvLCBQcm9wZXJ0eU1ldGFJbmZvLCBNZXRhSW5mbywgU2VydmljZU1ldGFJbmZvLCBFdmVudExpc3RlbmVyTWV0YUluZm8gfSBmcm9tICcuL21ldGEtaW5mbyc7XG5cbmV4cG9ydCBjbGFzcyBNZXRhU2VydmljZSB7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgbWFwOiBNYXA8YW55LCBNZXRhSW5mbz4gPSBuZXcgTWFwPGFueSwgTWV0YUluZm8+KCk7XG5cbiAgc3RhdGljIGdldChrZXk6IGFueSk6IGFueSB7XG4gICAgcmV0dXJuIE1ldGFTZXJ2aWNlLm1hcC5nZXQoa2V5KTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRCeU5hbWUoa2V5OiBzdHJpbmcpOiBhbnkge1xuICAgIGZvciAoY29uc3QgbWV0YSBvZiBNZXRhU2VydmljZS5tYXAudmFsdWVzKCkpIHtcbiAgICAgIGlmIChtZXRhIGluc3RhbmNlb2YgRW50aXR5TWV0YUluZm8gJiYgbWV0YS5uYW1lID09PSBrZXkpIHtcbiAgICAgICAgcmV0dXJuIG1ldGE7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGdldE9yQ3JlYXRlRm9yRW50aXR5KGtleTogRnVuY3Rpb24pOiBFbnRpdHlNZXRhSW5mbyB7XG4gICAgbGV0IG1ldGEgPSBNZXRhU2VydmljZS5nZXQoa2V5KTtcbiAgICBpZiAoIW1ldGEpIHtcbiAgICAgIG1ldGEgPSBuZXcgRW50aXR5TWV0YUluZm8oa2V5KTtcbiAgICAgIE1ldGFTZXJ2aWNlLnNldChtZXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIDxFbnRpdHlNZXRhSW5mbz5tZXRhO1xuICB9XG5cbiAgc3RhdGljIGdldE9yQ3JlYXRlRm9yRXZlbnRMaXN0ZW5lcihrZXk6IEZ1bmN0aW9uKTogRXZlbnRMaXN0ZW5lck1ldGFJbmZvIHtcbiAgICBsZXQgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldChrZXkpO1xuICAgIGlmICghbWV0YSkge1xuICAgICAgbWV0YSA9IG5ldyBFdmVudExpc3RlbmVyTWV0YUluZm8oa2V5KTtcbiAgICAgIE1ldGFTZXJ2aWNlLnNldChtZXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIDxFdmVudExpc3RlbmVyTWV0YUluZm8+bWV0YTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRPckNyZWF0ZUZvclByb3BlcnR5KGtleTogYW55LCBwcm9wZXJ0eTogc3RyaW5nLCB0eXBlPzogYW55KTogUHJvcGVydHlNZXRhSW5mbyB7XG4gICAgY29uc3QgZW50aXR5TWV0YSA9IE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KGtleS5jb25zdHJ1Y3Rvcik7XG4gICAgaWYgKCFlbnRpdHlNZXRhLnByb3BlcnRpZXNbcHJvcGVydHldKSB7XG4gICAgICBlbnRpdHlNZXRhLnByb3BlcnRpZXNbcHJvcGVydHldID0gbmV3IFByb3BlcnR5TWV0YUluZm8odHlwZSB8fCBSZWZsZWN0LmdldE1ldGFkYXRhKCdkZXNpZ246dHlwZScsIGtleSwgcHJvcGVydHkpKTtcbiAgICB9XG4gICAgcmV0dXJuIGVudGl0eU1ldGEucHJvcGVydGllc1twcm9wZXJ0eV07XG4gIH1cblxuICBzdGF0aWMgZ2V0T3JDcmVhdGVGb3JIdHRwU2VydmljZShrZXk6IEZ1bmN0aW9uLCBlbnRpdHk6IEZ1bmN0aW9uKTogU2VydmljZU1ldGFJbmZvIHtcbiAgICBsZXQgbWV0YSA9IE1ldGFTZXJ2aWNlLmdldChrZXkpO1xuICAgIGlmICghbWV0YSkge1xuICAgICAgbWV0YSA9IG5ldyBTZXJ2aWNlTWV0YUluZm8oa2V5LCBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eShlbnRpdHkpKTtcbiAgICAgIE1ldGFTZXJ2aWNlLnNldChtZXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIDxTZXJ2aWNlTWV0YUluZm8+bWV0YTtcbiAgfVxuXG4gIHN0YXRpYyBzZXQobWV0YTogTWV0YUluZm8pIHtcbiAgICBNZXRhU2VydmljZS5tYXAuc2V0KG1ldGEudHlwZSwgbWV0YSk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIEFOUkVTVF9IVFRQX0VWRU5UX0xJU1RFTkVSUywgRXZlbnQsIEV2ZW50TGlzdGVuZXJcbn0gZnJvbSAnLi9ldmVudHMnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEV2ZW50c1NlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoQU5SRVNUX0hUVFBfRVZFTlRfTElTVEVORVJTKSBwcml2YXRlIHJlYWRvbmx5IGhhbmRsZXJzOiBFdmVudExpc3RlbmVyW11cbiAgKSB7XG4gICAgaWYgKCFBcnJheS5pc0FycmF5KHRoaXMuaGFuZGxlcnMpKSB7XG4gICAgICB0aGlzLmhhbmRsZXJzID0gW107XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGhhbmRsZShldmVudDogRXZlbnQpOiBFdmVudCB7XG4gICAgdGhpcy5oYW5kbGVycy5mb3JFYWNoKChoYW5kbGVyOiBFdmVudExpc3RlbmVyKSA9PiB7XG4gICAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFdmVudExpc3RlbmVyKGhhbmRsZXIuY29uc3RydWN0b3IpO1xuICAgICAgZm9yIChjb25zdCBldmVudE1ldGEgb2YgbWV0YS5ldmVudHMpIHtcbiAgICAgICAgaWYgKGV2ZW50TWV0YS50eXBlID09PSBldmVudC5jb25zdHJ1Y3RvciAmJiBldmVudE1ldGEuZW50aXR5ID09PSBldmVudC5lbnRpdHkoKSkge1xuICAgICAgICAgIGhhbmRsZXIuaGFuZGxlKGV2ZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBldmVudDtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi9kYXRhLWluZm8uc2VydmljZSc7XG5pbXBvcnQge1xuICBBZnRlckdldEV2ZW50LFxuICBBZnRlclJlbW92ZUV2ZW50LFxuICBBZnRlclNhdmVFdmVudCxcbiAgRXZlbnRzU2VydmljZSxcbiAgQmVmb3JlR2V0RXZlbnQsXG4gIEJlZm9yZVJlbW92ZUV2ZW50LFxuICBCZWZvcmVTYXZlRXZlbnRcbn0gZnJvbSAnLi4vZXZlbnRzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UsIFNlcnZpY2VNZXRhSW5mbyB9IGZyb20gJy4uL21ldGEnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXBpU2VydmljZSB7XG4gIHByb3RlY3RlZCBtZXRhOiBTZXJ2aWNlTWV0YUluZm87XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJvdGVjdGVkIGV2ZW50czogRXZlbnRzU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgaW5mb1NlcnZpY2U6IERhdGFJbmZvU2VydmljZSxcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge1xuICAgIHRoaXMubWV0YSA9IE1ldGFTZXJ2aWNlLmdldCh0aGlzLmNvbnN0cnVjdG9yKTtcbiAgfVxuXG4gIGdldExpc3QoZmlsdGVyPzogSHR0cFBhcmFtc3x7IFtpbmRleDogc3RyaW5nXTogc3RyaW5nfG51bWJlcnxib29sZWFuIH0pOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KHRoaXMubWV0YS5wYXRoLCBmaWx0ZXIpO1xuICB9XG5cbiAgZ2V0KGlkOiBudW1iZXIgfCBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KHRoaXMubWV0YS5wYXRoICsgJy8nICsgaWQpO1xuICB9XG5cbiAgcmVsb2FkKGVudGl0eTogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5kb0dldCh0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShlbnRpdHkpLnBhdGgpO1xuICB9XG5cbiAgZ2V0UmVmZXJlbmNlKGlkOiBudW1iZXIgfCBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmRvR2V0KCcmJyArIHRoaXMubWV0YS5wYXRoICsgJy8nICsgaWQgKyAnIycgKyB0aGlzLm1ldGEuZW50aXR5TWV0YS5uYW1lKTtcbiAgfVxuXG4gIGRvR2V0KHBhdGg6IHN0cmluZywgZmlsdGVyPzogSHR0cFBhcmFtc3x7IFtpbmRleDogc3RyaW5nXTogc3RyaW5nfG51bWJlcnxib29sZWFuIH0pIHtcbiAgICBsZXQgZjogSHR0cFBhcmFtcztcbiAgICBpZiAoZmlsdGVyIGluc3RhbmNlb2YgSHR0cFBhcmFtcykge1xuICAgICAgZiA9IGZpbHRlcjtcbiAgICB9IGVsc2Uge1xuICAgICAgZiA9IG5ldyBIdHRwUGFyYW1zKCk7XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBmaWx0ZXIpIHtcbiAgICAgICAgZiA9IGYuc2V0KGtleSwgdHlwZW9mIGZpbHRlcltrZXldID09PSAnYm9vbGVhbicgPyAoZmlsdGVyW2tleV0gPyAndHJ1ZScgOiAnZmFsc2UnKSA6IFN0cmluZyhmaWx0ZXJba2V5XSkpO1xuICAgICAgfVxuICAgIH1cbiAgICB0aGlzLmV2ZW50cy5oYW5kbGUobmV3IEJlZm9yZUdldEV2ZW50KHBhdGgsIHRoaXMubWV0YS5lbnRpdHlNZXRhLnR5cGUsIGYpKTtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLmNvbmZpZy5iYXNlVXJsICsgcGF0aCwge2hlYWRlcnM6IHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldEhlYWRlcnMoKSwgcGFyYW1zOiBmfSkucGlwZShcbiAgICAgIHRhcCgoZGF0YSkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlckdldEV2ZW50KGRhdGEsIHRoaXMubWV0YS5lbnRpdHlNZXRhLnR5cGUpKSlcbiAgICApO1xuICB9XG5cbiAgc2F2ZShlbnRpdHk6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBCZWZvcmVTYXZlRXZlbnQoZW50aXR5KSk7XG4gICAgbGV0IGJvZHkgPSB7fTtcbiAgICBpZiAodGhpcy5tZXRhLmVudGl0eU1ldGEuYm9keSkge1xuICAgICAgYm9keSA9IHRoaXMubWV0YS5lbnRpdHlNZXRhLmJvZHkoZW50aXR5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5tZXRhLmVudGl0eU1ldGEuZ2V0UHJvcGVydGllc0ZvclNhdmUoKS5mb3JFYWNoKChwcm9wZXJ0eSkgPT4gYm9keVtwcm9wZXJ0eV0gPSBlbnRpdHlbcHJvcGVydHldKTtcbiAgICB9XG4gICAgbGV0IG1ldGEgPSB0aGlzLmluZm9TZXJ2aWNlLmdldEZvckVudGl0eShlbnRpdHkpO1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cFttZXRhID8gJ3B1dCcgOiAncG9zdCddKFxuICAgICAgICBtZXRhID8gdGhpcy5jb25maWcuYmFzZVVybCArIG1ldGEucGF0aCA6IHRoaXMuY29uZmlnLmJhc2VVcmwgKyB0aGlzLm1ldGEucGF0aCxcbiAgICAgICAgYm9keSxcbiAgICAgICAge2hlYWRlcnM6IHRoaXMubWV0YS5lbnRpdHlNZXRhLmdldEhlYWRlcnMoZW50aXR5KX1cbiAgICAgICkucGlwZShcbiAgICAgICAgdGFwKChkYXRhKSA9PiB7XG4gICAgICAgICAgbWV0YSA9IHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGRhdGEpO1xuICAgICAgICAgIGlmICghbWV0YSkge1xuICAgICAgICAgICAgdGhpcy5pbmZvU2VydmljZS5zZXRGb3JFbnRpdHkoZW50aXR5LCBtZXRhKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pLFxuICAgICAgICB0YXAoKCkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlclNhdmVFdmVudChlbnRpdHkpKSlcbiAgICAgICk7XG4gIH1cblxuICByZW1vdmUoZW50aXR5OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHRoaXMuZXZlbnRzLmhhbmRsZShuZXcgQmVmb3JlUmVtb3ZlRXZlbnQoZW50aXR5KSk7XG4gICAgY29uc3QgbWV0YSA9IHRoaXMuaW5mb1NlcnZpY2UuZ2V0Rm9yRW50aXR5KGVudGl0eSk7XG4gICAgaWYgKG1ldGEpIHtcbiAgICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlKHRoaXMuY29uZmlnLmJhc2VVcmwgKyBtZXRhLnBhdGgsIHtoZWFkZXJzOiB0aGlzLm1ldGEuZW50aXR5TWV0YS5nZXRIZWFkZXJzKGVudGl0eSl9KS5waXBlKFxuICAgICAgICB0YXAoKCkgPT4gdGhpcy5ldmVudHMuaGFuZGxlKG5ldyBBZnRlclJlbW92ZUV2ZW50KGVudGl0eSkpKVxuICAgICAgKTtcbiAgICB9XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSZWZlcmVuY2VJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZ1xuICApIHt9XG5cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGlmIChyZXF1ZXN0LnVybC5pbmRleE9mKHRoaXMuY29uZmlnLmJhc2VVcmwpICE9PSAwIHx8IHJlcXVlc3QudXJsID09PSB0aGlzLmNvbmZpZy5hdXRoVXJsIHx8ICF0aGlzLmlzUmVmZXJlbmNlUmVxdWVzdChyZXF1ZXN0KSkge1xuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5jcmVhdGVSZWZlcmVuY2VSZXNwb25zZShyZXF1ZXN0KTtcbiAgfVxuXG4gIHByaXZhdGUgaXNSZWZlcmVuY2VSZXF1ZXN0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4pOiBib29sZWFuIHtcbiAgICByZXR1cm4gcmVxdWVzdC5tZXRob2QgPT09ICdHRVQnICYmIHJlcXVlc3QudXJsLnNsaWNlKHRoaXMuY29uZmlnLmJhc2VVcmwubGVuZ3RoKS5pbmRleE9mKCcmJykgPT09IDA7XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZVJlZmVyZW5jZVJlc3BvbnNlKHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4pIHtcbiAgICBjb25zdCBwYXRoID0gcmVxdWVzdC51cmxXaXRoUGFyYW1zLnNsaWNlKHRoaXMuY29uZmlnLmJhc2VVcmwubGVuZ3RoKTtcbiAgICByZXR1cm4gb2YobmV3IEh0dHBSZXNwb25zZSh7XG4gICAgICBib2R5OiB7J3BhdGgnOiBwYXRoLnN1YnN0cmluZygxLCBwYXRoLmluZGV4T2YoJyMnKSksICdtZXRhJzogTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKHBhdGguc3Vic3RyaW5nKHBhdGguaW5kZXhPZignIycpICsgMSkpfSxcbiAgICAgIHN0YXR1czogMjAwLFxuICAgICAgdXJsOiByZXF1ZXN0LnVybFxuICAgIH0pKS5waXBlKFxuICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2UgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcbiAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuY2xvbmUoe1xuICAgICAgICAgICAgaGVhZGVyczogcmVzcG9uc2UuaGVhZGVycy5zZXQoJ0NvbnRlbnQtVHlwZScsICdAcmVmZXJlbmNlJylcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApO1xuICB9XG59XG4iLCJpbXBvcnQgeyBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi9yZXNwb25zZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRGF0YU5vcm1hbGl6ZXIge1xuICBub3JtYWxpemUocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+LCB0eXBlPzogc3RyaW5nKTogUmVzcG9uc2VOb2RlO1xuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuO1xufVxuXG5leHBvcnQgbGV0IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMgPSBuZXcgSW5qZWN0aW9uVG9rZW48RGF0YU5vcm1hbGl6ZXJbXT4oJ2FucmVzdC5odHRwX2RhdGFfbm9ybWFsaXplcnMnKTtcbiIsImV4cG9ydCBjbGFzcyBSZXNwb25zZU5vZGUge1xuICBwdWJsaWMgZGF0YTogYW55O1xuICBwdWJsaWMgY29sbGVjdGlvbjogYm9vbGVhbjtcbiAgcHVibGljIG1ldGE6IGFueTtcbiAgcHVibGljIGluZm86IGFueTtcbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERhdGFJbmZvU2VydmljZSB9IGZyb20gJy4vZGF0YS1pbmZvLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT2JqZWN0Q29sbGVjdG9yIHtcbiAgICBtYXA6IE1hcDxzdHJpbmcsIGFueT4gID0gbmV3IE1hcDxzdHJpbmcsIGFueT4oKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlKSB7fVxuXG4gICAgc2V0KGRhdGE6IGFueSkge1xuICAgICAgdGhpcy5tYXAuc2V0KHRoaXMuaW5mby5nZXQoZGF0YSkucGF0aCwgZGF0YSk7XG4gICAgfVxuXG4gICAgcmVtb3ZlKGRhdGE6IGFueSkge1xuICAgICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQoZGF0YSk7XG4gICAgICBpZiAoaW5mbykge1xuICAgICAgICBkYXRhID0gaW5mby5wYXRoO1xuICAgICAgfVxuICAgICAgdGhpcy5tYXAuZGVsZXRlKGRhdGEpO1xuICAgIH1cblxuICAgIGdldChpZDogc3RyaW5nKSB7XG4gICAgICByZXR1cm4gdGhpcy5tYXAuZ2V0KGlkKTtcbiAgICB9XG5cbiAgICBoYXMoaWQ6IHN0cmluZykge1xuICAgICAgcmV0dXJuIHRoaXMubWFwLmhhcyhpZCk7XG4gICAgfVxuXG4gICAgY2xlYXIoKSB7XG4gICAgICB0aGlzLm1hcC5jbGVhcigpO1xuICAgIH1cbn1cbiIsImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuLi9yZXNwb25zZS9yZXNwb25zZSc7XG5pbXBvcnQgeyBEYXRhSW5mb1NlcnZpY2UgfSBmcm9tICcuL2RhdGEtaW5mby5zZXJ2aWNlJztcbmltcG9ydCB7IEVudGl0eU1ldGFJbmZvIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBPYmplY3RDb2xsZWN0b3IgfSBmcm9tICcuL29iamVjdC1jb2xsZWN0b3InO1xuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4vY29sbGVjdGlvbic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBcGlQcm9jZXNzb3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yLFxuICAgIHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlLFxuICAgIHByaXZhdGUgY29sbGVjdG9yOiBPYmplY3RDb2xsZWN0b3JcbiAgKSB7fVxuXG4gIHByb2Nlc3Mobm9kZTogUmVzcG9uc2VOb2RlLCBtZXRob2Q6IHN0cmluZyk6IGFueSB7XG4gICAgcmV0dXJuIG5vZGUuY29sbGVjdGlvbiA/IHRoaXMucHJvY2Vzc0NvbGxlY3Rpb24obm9kZSkgOiB0aGlzLnByb2Nlc3NPYmplY3Qobm9kZSk7XG4gIH1cblxuICBwcml2YXRlIHByb2Nlc3NDb2xsZWN0aW9uKG5vZGU6IFJlc3BvbnNlTm9kZSkge1xuICAgIGxldCBkYXRhO1xuICAgIGlmIChub2RlLmluZm8ucGF0aCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBkYXRhID0gdGhpcy5jb2xsZWN0b3IuZ2V0KG5vZGUuaW5mby5wYXRoKSB8fCBuZXcgQ29sbGVjdGlvbih0aGlzLmluamVjdG9yLmdldChub2RlLm1ldGEuc2VydmljZSksIG5vZGUuaW5mbyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGRhdGEgPSBuZXcgQ29sbGVjdGlvbih0aGlzLmluamVjdG9yLmdldChub2RlLm1ldGEuc2VydmljZSksIG5vZGUuaW5mbyk7XG4gICAgfVxuICAgIGRhdGEubGVuZ3RoID0gMDtcbiAgICBmb3IgKGNvbnN0IG9iamVjdCBvZiBub2RlLmRhdGEpIHtcbiAgICAgIGRhdGEucHVzaCh0aGlzLnByb2Nlc3NPYmplY3Qob2JqZWN0KSk7XG4gICAgfVxuICAgIHRoaXMuaW5mby5zZXRGb3JDb2xsZWN0aW9uKGRhdGEsIG5vZGUuaW5mbyk7XG4gICAgdGhpcy5jb2xsZWN0b3Iuc2V0KGRhdGEpO1xuICAgIHJldHVybiBkYXRhO1xuICB9XG5cbiAgcHJpdmF0ZSBwcm9jZXNzT2JqZWN0KG5vZGU6IFJlc3BvbnNlTm9kZSkge1xuICAgIGlmIChub2RlLm1ldGEgaW5zdGFuY2VvZiBFbnRpdHlNZXRhSW5mbykge1xuICAgICAgY29uc3Qgb2JqZWN0ID0gdGhpcy5jb2xsZWN0b3IuZ2V0KG5vZGUuaW5mby5wYXRoKSB8fCBuZXcgbm9kZS5tZXRhLnR5cGUoKTtcbiAgICAgIGZvciAoY29uc3QgcHJvcGVydHkgaW4gbm9kZS5tZXRhLnByb3BlcnRpZXMpIHtcbiAgICAgICAgaWYgKG9iamVjdC5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkgJiYgIW5vZGUuZGF0YS5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobm9kZS5kYXRhW3Byb3BlcnR5XSBpbnN0YW5jZW9mIFJlc3BvbnNlTm9kZSkge1xuICAgICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBub2RlLm1ldGEucHJvcGVydGllc1twcm9wZXJ0eV0uaXNDb2xsZWN0aW9uID9cbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc0NvbGxlY3Rpb24obm9kZS5kYXRhW3Byb3BlcnR5XSkgOlxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzT2JqZWN0KG5vZGUuZGF0YVtwcm9wZXJ0eV0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnN0IHR5cGU6IGFueSA9IG5vZGUubWV0YS5wcm9wZXJ0aWVzW3Byb3BlcnR5XS50eXBlO1xuICAgICAgICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICAgICAgY2FzZSBOdW1iZXI6XG4gICAgICAgICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBOdW1iZXIobm9kZS5kYXRhW3Byb3BlcnR5XSk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBEYXRlOlxuICAgICAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gbmV3IERhdGUobm9kZS5kYXRhW3Byb3BlcnR5XSk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBTdHJpbmc6XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICBvYmplY3RbcHJvcGVydHldID0gbm9kZS5kYXRhW3Byb3BlcnR5XTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBmb3IgKGNvbnN0IHByb3BlcnR5IGluIG5vZGUubWV0YS5zdWJyZXNvdXJjZXMpIHtcbiAgICAgICAgY29uc3Qgc2VydmljZSA9IHRoaXMuaW5qZWN0b3IuZ2V0KG5vZGUubWV0YS5zdWJyZXNvdXJjZXNbcHJvcGVydHldLm1ldGEuc2VydmljZSk7XG4gICAgICAgIG9iamVjdFtwcm9wZXJ0eV0gPSBzZXJ2aWNlLmRvR2V0LmJpbmQoc2VydmljZSwgbm9kZS5pbmZvLnBhdGggKyBub2RlLm1ldGEuc3VicmVzb3VyY2VzW3Byb3BlcnR5XS5wYXRoKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuaW5mby5zZXRGb3JFbnRpdHkob2JqZWN0LCBub2RlLmluZm8pO1xuICAgICAgdGhpcy5jb2xsZWN0b3Iuc2V0KG9iamVjdCk7XG4gICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbm9kZS5kYXRhO1xuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFuUmVzdENvbmZpZywgQXBpQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xuaW1wb3J0IHsgQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUywgRGF0YU5vcm1hbGl6ZXIgfSBmcm9tICcuLi9yZXNwb25zZS9kYXRhLW5vcm1hbGl6ZXInO1xuaW1wb3J0IHsgQXBpUHJvY2Vzc29yIH0gZnJvbSAnLi9hcGkucHJvY2Vzc29yJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFwaUludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KEFuUmVzdENvbmZpZykgcHJvdGVjdGVkIHJlYWRvbmx5IGNvbmZpZzogQXBpQ29uZmlnLFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUykgcHJpdmF0ZSByZWFkb25seSBub3JtYWxpemVyczogRGF0YU5vcm1hbGl6ZXJbXSxcbiAgICBwcml2YXRlIHJlYWRvbmx5IHByb2Nlc3NvcjogQXBpUHJvY2Vzc29yXG4gICkge1xuICAgIGlmICghQXJyYXkuaXNBcnJheSh0aGlzLm5vcm1hbGl6ZXJzKSkge1xuICAgICAgdGhpcy5ub3JtYWxpemVycyA9IFtdO1xuICAgIH1cbiAgfVxuXG4gIGludGVyY2VwdChyZXF1ZXN0OiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICBpZiAocmVxdWVzdC51cmwuaW5kZXhPZih0aGlzLmNvbmZpZy5iYXNlVXJsKSAhPT0gMCB8fCByZXF1ZXN0LnVybCA9PT0gdGhpcy5jb25maWcuYXV0aFVybFxuICAgICAgfHwgKEFycmF5LmlzQXJyYXkodGhpcy5jb25maWcuZXhjbHVkZWRVcmxzKSAmJiB0aGlzLmNvbmZpZy5leGNsdWRlZFVybHMuaW5kZXhPZihyZXF1ZXN0LnVybCkgIT09IC0xKSkge1xuICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICAgIH1cbiAgICBsZXQgdHlwZTogc3RyaW5nO1xuICAgIGxldCBoZWFkZXJzID0gcmVxdWVzdC5oZWFkZXJzO1xuICAgIGlmIChoZWFkZXJzLmhhcygnWC1BblJlc3QtVHlwZScpKSB7XG4gICAgICB0eXBlID0gaGVhZGVycy5nZXQoJ1gtQW5SZXN0LVR5cGUnKTtcbiAgICAgIGhlYWRlcnMgPSBoZWFkZXJzLmRlbGV0ZSgneC1BblJlc3QtVHlwZScpO1xuICAgIH1cbiAgICBpZiAodGhpcy5jb25maWcuZGVmYXVsdEhlYWRlcnMpIHtcbiAgICAgIGZvciAoY29uc3QgaGVhZGVyIG9mIHRoaXMuY29uZmlnLmRlZmF1bHRIZWFkZXJzKSB7XG4gICAgICAgIGlmICghaGVhZGVycy5oYXMoaGVhZGVyLm5hbWUpIHx8IGhlYWRlci5hcHBlbmQpIHtcbiAgICAgICAgICBoZWFkZXJzID0gaGVhZGVyc1toZWFkZXIuYXBwZW5kID8gJ2FwcGVuZCcgOiAnc2V0J10oaGVhZGVyLm5hbWUsIGhlYWRlci52YWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xuICAgICAgaGVhZGVyczogaGVhZGVyc1xuICAgIH0pO1xuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KS5waXBlKFxuICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2UgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcbiAgICAgICAgICBjb25zdCBjb250ZW50VHlwZSA9IHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdDb250ZW50LVR5cGUnKTtcbiAgICAgICAgICBmb3IgKGNvbnN0IG5vcm1hbGl6ZXIgb2YgdGhpcy5ub3JtYWxpemVycykge1xuICAgICAgICAgICAgaWYgKG5vcm1hbGl6ZXIuc3VwcG9ydHMoY29udGVudFR5cGUpKSB7XG4gICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5jbG9uZSh7IGJvZHk6IHRoaXMucHJvY2Vzc29yLnByb2Nlc3Mobm9ybWFsaXplci5ub3JtYWxpemUocmVzcG9uc2UsIHR5cGUpLCByZXF1ZXN0Lm1ldGhvZCkgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIFJlc3BvbnNlQ2FjaGUge1xuXG4gIGFic3RyYWN0IGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbjtcblxuICBhYnN0cmFjdCBnZXQoaWQ6IHN0cmluZyk7XG5cbiAgYWJzdHJhY3QgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSk7XG5cbiAgYWJzdHJhY3QgcmVtb3ZlKGlkOiBzdHJpbmcpO1xuXG4gIGFic3RyYWN0IGNsZWFyKCk7XG5cbiAgYWJzdHJhY3QgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZyk7XG5cbiAgcmVwbGFjZShkYXRhOiBhbnksIGlkOiBzdHJpbmcpIHtcbiAgICB0aGlzLnJlbW92ZShpZCk7XG4gICAgdGhpcy5hZGQoZGF0YSwgaWQpO1xuICB9XG59XG5cbmV4cG9ydCBsZXQgQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTID0gbmV3IEluamVjdGlvblRva2VuPFJlc3BvbnNlQ2FjaGVbXT4oJ2FucmVzdC5jYWNoZV9zZXJ2aWNlcycpO1xuIiwiaW1wb3J0IHsgUmVzcG9uc2VDYWNoZSB9IGZyb20gJy4vcmVzcG9uc2UtY2FjaGUnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTm9SZXNwb25zZUNhY2hlIGV4dGVuZHMgUmVzcG9uc2VDYWNoZSB7XG4gIGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICB9XG5cbiAgY2xlYXIoKSB7XG4gIH1cblxuICBnZXQoaWQ6IHN0cmluZykge1xuICB9XG5cbiAgcmVtb3ZlKGlkOiBzdHJpbmcpIHtcbiAgfVxuXG4gIGFkZEFsaWFzKGlkOiBzdHJpbmcsIGFsaWFzOiBzdHJpbmcpIHtcbiAgfVxuXG4gIHJlcGxhY2UoZGF0YTogYW55LCBpZDogc3RyaW5nKTogdm9pZCB7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgSHR0cFJlcXVlc3QsXG4gIEh0dHBIYW5kbGVyLFxuICBIdHRwRXZlbnQsXG4gIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlc3BvbnNlXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZywgRGF0YUluZm9TZXJ2aWNlIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsIFJlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL3Jlc3BvbnNlLWNhY2hlJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBOb1Jlc3BvbnNlQ2FjaGUgfSBmcm9tICcuL25vLXJlc3BvbnNlLWNhY2hlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIENhY2hlSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgcmVhZG9ubHkgY29uZmlnOiBBcGlDb25maWcsXG4gICAgcHJpdmF0ZSByZWFkb25seSBpbmZvOiBEYXRhSW5mb1NlcnZpY2UsXG4gICAgQEluamVjdChBTlJFU1RfQ0FDSEVfU0VSVklDRVMpIHByaXZhdGUgcmVhZG9ubHkgY2FjaGVTZXJ2aWNlczogUmVzcG9uc2VDYWNoZVtdLFxuICApIHt9XG5cbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIGNvbnN0IHBhdGggPSByZXF1ZXN0LnVybFdpdGhQYXJhbXMuc2xpY2UodGhpcy5jb25maWcuYmFzZVVybC5sZW5ndGgpO1xuICAgIGlmIChyZXF1ZXN0LnVybC5pbmRleE9mKHRoaXMuY29uZmlnLmJhc2VVcmwpICE9PSAwIHx8IHJlcXVlc3QudXJsID09PSB0aGlzLmNvbmZpZy5hdXRoVXJsXG4gICAgICB8fCAoQXJyYXkuaXNBcnJheSh0aGlzLmNvbmZpZy5leGNsdWRlZFVybHMpICYmIHRoaXMuY29uZmlnLmV4Y2x1ZGVkVXJscy5pbmRleE9mKHJlcXVlc3QudXJsKSAhPT0gLTEpKSB7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIGNvbnN0IGNhY2hlVHlwZSA9IE1ldGFTZXJ2aWNlLmdldEJ5TmFtZShyZXF1ZXN0LmhlYWRlcnMuZ2V0KCdYLUFuUmVzdC1UeXBlJykpLmNhY2hlIHx8IHRoaXMuY29uZmlnLmNhY2hlIHx8IE5vUmVzcG9uc2VDYWNoZTtcbiAgICBsZXQgY2FjaGU6IGFueTtcbiAgICBmb3IgKGNvbnN0IGNhY2hlU2VydmljZSBvZiB0aGlzLmNhY2hlU2VydmljZXMpIHtcbiAgICAgIGlmIChjYWNoZVNlcnZpY2UgaW5zdGFuY2VvZiBjYWNoZVR5cGUpIHtcbiAgICAgICAgY2FjaGUgPSBjYWNoZVNlcnZpY2U7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAocmVxdWVzdC5tZXRob2QgIT09ICdHRVQnKSB7XG4gICAgICBjYWNoZS5yZW1vdmUocGF0aCk7XG4gICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gICAgfVxuICAgIGlmIChjYWNoZS5oYXMocGF0aCkpIHtcbiAgICAgIHJldHVybiBvZihuZXcgSHR0cFJlc3BvbnNlKHtcbiAgICAgICAgYm9keTogY2FjaGUuZ2V0KHBhdGgpLFxuICAgICAgICBzdGF0dXM6IDIwMCxcbiAgICAgICAgdXJsOiByZXF1ZXN0LnVybFxuICAgICAgfSkpO1xuICAgIH1cbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCkucGlwZShcbiAgICAgIHRhcCgocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+KSA9PiB7XG4gICAgICAgIGlmIChyZXNwb25zZS5ib2R5KSB7XG4gICAgICAgICAgY29uc3QgaW5mbyA9IHRoaXMuaW5mby5nZXQocmVzcG9uc2UuYm9keSk7XG4gICAgICAgICAgaWYgKGluZm8pIHtcbiAgICAgICAgICAgIGNhY2hlLmFkZChpbmZvLnBhdGgsIHJlc3BvbnNlLmJvZHkpO1xuICAgICAgICAgICAgaWYgKGluZm8gJiYgaW5mby5hbGlhcykge1xuICAgICAgICAgICAgICBjYWNoZS5hZGRBbGlhcyhpbmZvLnBhdGgsIGluZm8uYWxpYXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICApO1xuICB9XG59XG4iLCJpbXBvcnQgeyBSZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9iamVjdENvbGxlY3RvciB9IGZyb20gJy4uL2NvcmUvb2JqZWN0LWNvbGxlY3Rvcic7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4uL2NvcmUvY29uZmlnJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE1lbW9yeVJlc3BvbnNlQ2FjaGUgZXh0ZW5kcyBSZXNwb25zZUNhY2hlIHtcblxuICBwcml2YXRlIHRpbWVzOiB7IFtpbmRleDogc3RyaW5nXTogbnVtYmVyIH0gPSB7fTtcblxuICBwcml2YXRlIGFsaWFzZXM6IHsgW2luZGV4OiBzdHJpbmddOiBzdHJpbmcgfSA9IHt9O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcmVhZG9ubHkgY29sbGVjdG9yOiBPYmplY3RDb2xsZWN0b3IsXG4gICAgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCByZWFkb25seSBjb25maWc6IEFwaUNvbmZpZ1xuICApIHtcbiAgICBzdXBlcigpO1xuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICAgIHRoaXMudGltZXNbaWRdID0gRGF0ZS5ub3coKTtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICAgIHRoaXMuY29sbGVjdG9yLmNsZWFyKCk7XG4gIH1cblxuICBnZXQoaWQ6IHN0cmluZykge1xuICAgIHJldHVybiB0aGlzLmNvbGxlY3Rvci5nZXQoaWQpO1xuICB9XG5cbiAgcmVtb3ZlKGlkOiBzdHJpbmcpIHtcbiAgICB0aGlzLmNvbGxlY3Rvci5yZW1vdmUoaWQpO1xuICB9XG5cbiAgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZykge1xuICAgIHRoaXMuYWxpYXNlc1thbGlhc10gPSBpZDtcbiAgfVxuXG4gIGhhcyhpZDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgY29uc3QgdGltZSA9IHRoaXMuYWxpYXNlc1tpZF0gPyB0aGlzLnRpbWVzW3RoaXMuYWxpYXNlc1tpZF1dIDogdGhpcy50aW1lc1tpZF07XG4gICAgcmV0dXJuIHRpbWUgJiYgKERhdGUubm93KCkgPD0gdGhpcy5jb25maWcuY2FjaGVUVEwgKyB0aW1lKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBEYXRhTm9ybWFsaXplciB9IGZyb20gJy4vZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4vcmVzcG9uc2UnO1xuaW1wb3J0IHsgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIgaW1wbGVtZW50cyBEYXRhTm9ybWFsaXplciB7XG4gIG5vcm1hbGl6ZShyZXNwb25zZTogSHR0cFJlc3BvbnNlPGFueT4sIHR5cGU/OiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIGNvbnN0IHIgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgci5jb2xsZWN0aW9uID0gZmFsc2U7XG4gICAgci5pbmZvID0gbmV3IEVudGl0eUluZm8ocmVzcG9uc2UuYm9keS5wYXRoKTtcbiAgICByLm1ldGEgPSByZXNwb25zZS5ib2R5Lm1ldGE7XG4gICAgci5kYXRhID0ge307XG4gICAgcmV0dXJuIHI7XG4gIH1cblxuICBzdXBwb3J0cyh0eXBlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHlwZSA9PT0gJ0ByZWZlcmVuY2UnO1xuICB9XG59XG4iLCJpbXBvcnQgeyBEYXRhTm9ybWFsaXplciB9IGZyb20gJy4vZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IFJlc3BvbnNlTm9kZSB9IGZyb20gJy4vcmVzcG9uc2UnO1xuaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCB7IENvbGxlY3Rpb25JbmZvLCBFbnRpdHlJbmZvIH0gZnJvbSAnLi4vY29yZSc7XG5cbmV4cG9ydCBjbGFzcyBMZEpzb25EYXRhTm9ybWFsaXplciBpbXBsZW1lbnRzIERhdGFOb3JtYWxpemVyIHtcbiAgbm9ybWFsaXplKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2U8YW55PiwgdHlwZT86IHN0cmluZyk6IFJlc3BvbnNlTm9kZSB7XG4gICAgcmV0dXJuIHRoaXMucHJvY2VzcyhyZXNwb25zZS5ib2R5KTtcbiAgfVxuXG4gIHN1cHBvcnRzKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0eXBlLnNwbGl0KCc7JylbMF0gPT09ICdhcHBsaWNhdGlvbi9sZCtqc29uJztcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2VzcyhkYXRhLCBtZXRhPzogYW55LCBjb2xsZWN0aW9uPzogYm9vbGVhbikge1xuICAgIGNvbnN0IHIgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgci5jb2xsZWN0aW9uID0gY29sbGVjdGlvbiB8fCBkYXRhWydAdHlwZSddID09PSAnaHlkcmE6Q29sbGVjdGlvbic7XG4gICAgY29uc3QgcGF0aCA9IGRhdGFbJ0BpZCddO1xuICAgIHIubWV0YSA9IG1ldGEgfHwgTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKHIuY29sbGVjdGlvbiA/IC9cXC9jb250ZXh0c1xcLyhcXHcrKS9nLmV4ZWMoZGF0YVsnQGNvbnRleHQnXSlbMV0gOiBkYXRhWydAdHlwZSddKTtcbiAgICBpZiAoci5jb2xsZWN0aW9uKSB7XG4gICAgICBjb25zdCBpdGVtcyA9IGRhdGFbJ2h5ZHJhOm1lbWJlciddIHx8IGRhdGE7XG4gICAgICByLmluZm8gPSBuZXcgQ29sbGVjdGlvbkluZm8oZGF0YVsnaHlkcmE6dmlldyddID8gZGF0YVsnaHlkcmE6dmlldyddWydAaWQnXSA6IHBhdGgpO1xuICAgICAgci5pbmZvLnR5cGUgPSByLm1ldGEubmFtZTtcbiAgICAgIGlmIChkYXRhWydoeWRyYTp2aWV3J10pIHtcbiAgICAgICAgci5pbmZvLmZpcnN0ID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpmaXJzdCddO1xuICAgICAgICByLmluZm8ucHJldmlvdXMgPSBkYXRhWydoeWRyYTp2aWV3J11bJ2h5ZHJhOnByZXZpb3VzJ107XG4gICAgICAgIHIuaW5mby5uZXh0ID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpuZXh0J107XG4gICAgICAgIHIuaW5mby5sYXN0ID0gZGF0YVsnaHlkcmE6dmlldyddWydoeWRyYTpsYXN0J107XG4gICAgICAgIGlmIChyLmluZm8ucGF0aCA9PT0gci5pbmZvLmZpcnN0KSB7XG4gICAgICAgICAgci5pbmZvLmFsaWFzID0gZGF0YVsnQGlkJ107XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHIuaW5mby50b3RhbCA9IGRhdGFbJ2h5ZHJhOnRvdGFsSXRlbXMnXSB8fCBpdGVtcy5sZW5ndGg7XG4gICAgICBjb25zdCByZWdleFBhZ2UgPSAvcGFnZT0oXFxkKykvZztcbiAgICAgIGxldCBtYXRjaGVzO1xuICAgICAgbWF0Y2hlcyA9IHJlZ2V4UGFnZS5leGVjKHIuaW5mby5wYXRoKTtcbiAgICAgIHIuaW5mby5wYWdlID0gbWF0Y2hlcyA/IE51bWJlcihtYXRjaGVzWzFdKSA6IDE7XG4gICAgICBtYXRjaGVzID0gcmVnZXhQYWdlLmV4ZWMoci5pbmZvLmxhc3QpO1xuICAgICAgci5pbmZvLmxhc3RQYWdlID0gbWF0Y2hlcyA/IE51bWJlcihtYXRjaGVzWzFdKSA6IDE7XG4gICAgICByLmRhdGEgPSBbXTtcbiAgICAgIGZvciAoY29uc3Qgb2JqZWN0IG9mIGl0ZW1zKSB7XG4gICAgICAgIHIuZGF0YS5wdXNoKHRoaXMucHJvY2VzcyhvYmplY3QpKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgci5pbmZvID0gbmV3IEVudGl0eUluZm8ocGF0aCk7XG4gICAgICByLmRhdGEgPSB7fTtcbiAgICAgIGlmIChyLm1ldGEgPT09IHVuZGVmaW5lZCAmJiBBcnJheS5pc0FycmF5KGRhdGEpKSB7XG4gICAgICAgIHIuZGF0YSA9IGRhdGE7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgICAgaWYgKGtleS5pbmRleE9mKCdAJykgPT09IDAgfHwga2V5LmluZGV4T2YoJ2h5ZHJhOicpID09PSAwKSB7XG4gICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgY29uc3QgcHJvcGVydHlNZXRhID0gci5tZXRhLnByb3BlcnRpZXNba2V5XTtcbiAgICAgICAgICByLmRhdGFba2V5XSA9IChBcnJheS5pc0FycmF5KGRhdGFba2V5XSkgfHwgKGRhdGFba2V5XSAhPT0gbnVsbCAmJiBwcm9wZXJ0eU1ldGEgJiYgTWV0YVNlcnZpY2UuZ2V0KHByb3BlcnR5TWV0YS50eXBlKSkpID9cbiAgICAgICAgICAgIHRoaXMucHJvY2VzcyhkYXRhW2tleV0sIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSksIHByb3BlcnR5TWV0YS5pc0NvbGxlY3Rpb24pIDogZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByO1xuICB9XG59XG4iLCJpbXBvcnQgeyBEYXRhTm9ybWFsaXplciB9IGZyb20gJy4vZGF0YS1ub3JtYWxpemVyJztcbmltcG9ydCB7IEh0dHBIZWFkZXJzLCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBSZXNwb25zZU5vZGUgfSBmcm9tICcuL3Jlc3BvbnNlJztcbmltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uSW5mbywgRW50aXR5SW5mbyB9IGZyb20gJy4uL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgSnNvbkRhdGFOb3JtYWxpemVyIGltcGxlbWVudHMgRGF0YU5vcm1hbGl6ZXIge1xuICBub3JtYWxpemUocmVzcG9uc2U6IEh0dHBSZXNwb25zZTxhbnk+LCB0eXBlOiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIHJldHVybiB0aGlzLnByb2Nlc3MocmVzcG9uc2UuYm9keSwgTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKHR5cGUpLCBBcnJheS5pc0FycmF5KHJlc3BvbnNlLmJvZHkpLCByZXNwb25zZS5oZWFkZXJzKTtcbiAgfVxuXG4gIHN1cHBvcnRzKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0eXBlLnNwbGl0KCc7JylbMF0gPT09ICdhcHBsaWNhdGlvbi9qc29uJztcbiAgfVxuXG4gIHByaXZhdGUgcHJvY2VzcyhkYXRhLCBtZXRhOiBhbnksIGNvbGxlY3Rpb246IGJvb2xlYW4sIGhlYWRlcnM/OiBIdHRwSGVhZGVycykge1xuICAgIGNvbnN0IHIgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgci5jb2xsZWN0aW9uID0gY29sbGVjdGlvbjtcbiAgICBjb25zdCBjb2xsZWN0aW9uUGF0aCA9IE1ldGFTZXJ2aWNlLmdldChtZXRhLnNlcnZpY2UpID8gTWV0YVNlcnZpY2UuZ2V0KG1ldGEuc2VydmljZSkucGF0aCA6IHVuZGVmaW5lZDtcbiAgICBjb25zdCBwYXRoID0gY29sbGVjdGlvblBhdGggPyAoY29sbGVjdGlvbiA/IGNvbGxlY3Rpb25QYXRoIDogY29sbGVjdGlvblBhdGggKyAnLycgKyBkYXRhW21ldGEuaWRdKSA6IHVuZGVmaW5lZDtcbiAgICByLm1ldGEgPSBtZXRhO1xuICAgIGlmIChyLmNvbGxlY3Rpb24pIHtcbiAgICAgIHIuaW5mbyA9IG5ldyBDb2xsZWN0aW9uSW5mbyhoZWFkZXJzLmdldCgnWC1DdXJyZW50LVBhZ2UnKSB8fCBwYXRoKTtcbiAgICAgIHIuaW5mby50eXBlID0gci5tZXRhLm5hbWU7XG4gICAgICByLmluZm8uZmlyc3QgPSBoZWFkZXJzLmdldCgnWC1GaXJzdC1QYWdlJykgfHwgdW5kZWZpbmVkO1xuICAgICAgci5pbmZvLnByZXZpb3VzID0gaGVhZGVycy5nZXQoJ1gtUHJldi1QYWdlJykgfHwgdW5kZWZpbmVkO1xuICAgICAgci5pbmZvLm5leHQgPSBoZWFkZXJzLmdldCgnWC1OZXh0LVBhZ2UnKSB8fCB1bmRlZmluZWQ7XG4gICAgICByLmluZm8ubGFzdCA9IGhlYWRlcnMuZ2V0KCdYLUxhc3QtUGFnZScpIHx8IHVuZGVmaW5lZDtcbiAgICAgIGlmIChyLmluZm8ucGF0aCA9PT0gci5pbmZvLmZpcnN0KSB7XG4gICAgICAgIHIuaW5mby5hbGlhcyA9IHBhdGg7XG4gICAgICB9XG4gICAgICByLmluZm8udG90YWwgPSBoZWFkZXJzLmdldCgnWC1Ub3RhbCcpIHx8IGRhdGEubGVuZ3RoO1xuICAgICAgY29uc3QgcmVnZXhQYWdlID0gL3BhZ2U9KFxcZCspL2c7XG4gICAgICBsZXQgbWF0Y2hlcztcbiAgICAgIG1hdGNoZXMgPSByZWdleFBhZ2UuZXhlYyhyLmluZm8ucGF0aCk7XG4gICAgICByLmluZm8ucGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgbWF0Y2hlcyA9IHJlZ2V4UGFnZS5leGVjKHIuaW5mby5sYXN0KTtcbiAgICAgIHIuaW5mby5sYXN0UGFnZSA9IG1hdGNoZXMgPyBOdW1iZXIobWF0Y2hlc1sxXSkgOiAxO1xuICAgICAgci5kYXRhID0gW107XG4gICAgICBmb3IgKGNvbnN0IG9iamVjdCBvZiBkYXRhKSB7XG4gICAgICAgIHIuZGF0YS5wdXNoKHRoaXMucHJvY2VzcyhvYmplY3QsIHIubWV0YSwgZmFsc2UpKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgci5pbmZvID0gbmV3IEVudGl0eUluZm8ocGF0aCk7XG4gICAgICByLmRhdGEgPSB7fTtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGRhdGEpIHtcbiAgICAgICAgY29uc3QgcHJvcGVydHlNZXRhID0gci5tZXRhLnByb3BlcnRpZXNba2V5XTtcbiAgICAgICAgci5kYXRhW2tleV0gPSAoQXJyYXkuaXNBcnJheShkYXRhW2tleV0pIHx8IChkYXRhW2tleV0gIT09IG51bGwgJiYgcHJvcGVydHlNZXRhICYmIE1ldGFTZXJ2aWNlLmdldChwcm9wZXJ0eU1ldGEudHlwZSkpKSA/XG4gICAgICAgICAgdGhpcy5wcm9jZXNzKGRhdGFba2V5XSwgTWV0YVNlcnZpY2UuZ2V0KHByb3BlcnR5TWV0YS50eXBlKSwgcHJvcGVydHlNZXRhLmlzQ29sbGVjdGlvbikgOiBkYXRhW2tleV07XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByO1xuICB9XG59XG4iLCJpbXBvcnQgeyBSZXNwb25zZUNhY2hlIH0gZnJvbSAnLi9yZXNwb25zZS1jYWNoZSc7XG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9iamVjdENvbGxlY3RvciB9IGZyb20gJy4uL2NvcmUvb2JqZWN0LWNvbGxlY3Rvcic7XG5pbXBvcnQgeyBBcGlQcm9jZXNzb3IgfSBmcm9tICcuLi9jb3JlL2FwaS5wcm9jZXNzb3InO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uSW5mbywgRGF0YUluZm9TZXJ2aWNlLCBFbnRpdHlJbmZvIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuaW1wb3J0IHsgUmVzcG9uc2VOb2RlIH0gZnJvbSAnLi4vcmVzcG9uc2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSBleHRlbmRzIFJlc3BvbnNlQ2FjaGUge1xuXG4gIHByaXZhdGUgdG9rZW5QcmVmaXggPSAnYW5yZXN0OmNhY2hlOic7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjb2xsZWN0b3I6IE9iamVjdENvbGxlY3RvcixcbiAgICBwcml2YXRlIHByb2Nlc3NvcjogQXBpUHJvY2Vzc29yLFxuICAgIHByaXZhdGUgaW5mbzogRGF0YUluZm9TZXJ2aWNlLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcml2YXRlIGNvbmZpZzogQXBpQ29uZmlnXG4gICkge1xuICAgIHN1cGVyKCk7XG4gICAgY29uc3QgdGltZXN0YW1wUHJlZml4ID0gdGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JztcbiAgICBPYmplY3Qua2V5cyhsb2NhbFN0b3JhZ2UpLmZpbHRlcigoZSkgPT4gZS5pbmRleE9mKHRpbWVzdGFtcFByZWZpeCkgPT09IDApLmZvckVhY2goKGspID0+IHtcbiAgICAgIGNvbnN0IGlkID0gay5zdWJzdHJpbmcodGltZXN0YW1wUHJlZml4Lmxlbmd0aCk7XG4gICAgICBjb25zdCB0aW1lc3RhbXAgPSB0aGlzLmdldFRpbWVzdGFtcChpZCk7XG4gICAgICBpZiAoIXRpbWVzdGFtcCB8fCB0aW1lc3RhbXAgKyB0aGlzLmNvbmZpZy5jYWNoZVRUTCA8IERhdGUubm93KCkpIHtcbiAgICAgICAgdGhpcy5yZW1vdmUoaWQpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgZ2V0KGlkOiBzdHJpbmcpIHtcbiAgICBpZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGlkKSB8fCBpZDtcbiAgICBpZiAodGhpcy5oYXMoaWQpKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9jZXNzb3IucHJvY2Vzcyh0aGlzLmdldE5vZGUoaWQpLCAnR0VUJyk7XG4gICAgfVxuICB9XG5cbiAgYWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSkge1xuICAgIHRoaXMuZG9BZGQoaWQsIGRhdGEsIHRydWUpO1xuICB9XG5cbiAgYWRkQWxpYXMoaWQ6IHN0cmluZywgYWxpYXM6IHN0cmluZykge1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnYWxpYXM6JyArIGFsaWFzLCBpZCk7XG4gIH1cblxuICBoYXMoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIGlkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdhbGlhczonICsgaWQpIHx8IGlkO1xuICAgIGNvbnN0IGNvbXBsZXRlID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCkpO1xuICAgIGNvbnN0IHRpbWVzdGFtcCA9IHRoaXMuZ2V0VGltZXN0YW1wKGlkKTtcbiAgICByZXR1cm4gISF0aW1lc3RhbXAgJiYgY29tcGxldGUgJiYgdGltZXN0YW1wICsgdGhpcy5jb25maWcuY2FjaGVUVEwgPj0gRGF0ZS5ub3coKTtcbiAgfVxuXG4gIHJlbW92ZShpZDogc3RyaW5nKSB7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkKTtcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbXBsZXRlOicgKyBpZCk7XG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCk7XG4gICAgdGhpcy5yZW1vdmVUaW1lc3RhbXAoaWQpO1xuICB9XG5cbiAgY2xlYXIoKSB7XG4gICAgT2JqZWN0LmtleXMobG9jYWxTdG9yYWdlKS5maWx0ZXIoKGUpID0+IGUuaW5kZXhPZih0aGlzLnRva2VuUHJlZml4KSA9PT0gMCkuZm9yRWFjaCgoaykgPT4ge1xuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oayk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGRvQWRkKGlkOiBzdHJpbmcsIGRhdGE6IGFueSwgY29tcGxldGU6IGJvb2xlYW4pIHtcbiAgICBjb25zdCBpbmZvID0gdGhpcy5pbmZvLmdldChkYXRhKTtcbiAgICBsZXQgdmFsdWVzOiBhbnk7XG4gICAgaWYgKGluZm8gaW5zdGFuY2VvZiBDb2xsZWN0aW9uSW5mbykge1xuICAgICAgdmFsdWVzID0gW107XG4gICAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgZGF0YSkge1xuICAgICAgICBjb25zdCBpdGVtUGF0aCA9IHRoaXMuaW5mby5nZXQoaXRlbSkucGF0aDtcbiAgICAgICAgdmFsdWVzLnB1c2goaXRlbVBhdGgpO1xuICAgICAgICB0aGlzLmRvQWRkKGl0ZW1QYXRoLCBpdGVtLCBmYWxzZSk7XG4gICAgICB9XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ2NvbGxlY3Rpb246JyArIGlkLCBKU09OLnN0cmluZ2lmeShpbmZvKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHJlZmVyZW5jZXMgPSB7fTtcbiAgICAgIHZhbHVlcyA9IHt9O1xuICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuICAgICAgICBsZXQgaXRlbUluZm86IGFueTtcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhW2tleV0gPT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgaXRlbUluZm8gPSB0aGlzLmluZm8uZ2V0KGRhdGFba2V5XSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGl0ZW1JbmZvKSB7XG4gICAgICAgICAgdGhpcy5kb0FkZChpdGVtSW5mby5wYXRoLCBkYXRhW2tleV0sIGZhbHNlKTtcbiAgICAgICAgICByZWZlcmVuY2VzW2tleV0gPSBpdGVtSW5mby5wYXRoO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZhbHVlc1trZXldID0gZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3R5cGU6JyArIGlkLCBNZXRhU2VydmljZS5nZXQoZGF0YS5jb25zdHJ1Y3RvcikubmFtZSk7XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3JlZmVyZW5jZXM6JyArIGlkLCBKU09OLnN0cmluZ2lmeShyZWZlcmVuY2VzKSk7XG4gICAgfVxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnY29tcGxldGU6JyArIGlkLCBKU09OLnN0cmluZ2lmeShjb21wbGV0ZSkpO1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCwgSlNPTi5zdHJpbmdpZnkodmFsdWVzKSk7XG4gICAgdGhpcy5zZXRUaW1lc3RhbXAoaWQpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXROb2RlKGlkOiBzdHJpbmcpOiBSZXNwb25zZU5vZGUge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyBpZCkpO1xuICAgIGNvbnN0IGNvbGxlY3Rpb25JbmZvID0gdGhpcy5nZXRDb2xsZWN0aW9uSW5mbyhpZCk7XG4gICAgY29uc3QgcmVzcG9uc2UgPSBuZXcgUmVzcG9uc2VOb2RlKCk7XG4gICAgaWYgKGNvbGxlY3Rpb25JbmZvKSB7XG4gICAgICByZXNwb25zZS5jb2xsZWN0aW9uID0gdHJ1ZTtcbiAgICAgIHJlc3BvbnNlLmluZm8gPSBjb2xsZWN0aW9uSW5mbztcbiAgICAgIHJlc3BvbnNlLm1ldGEgPSBNZXRhU2VydmljZS5nZXRCeU5hbWUoY29sbGVjdGlvbkluZm8udHlwZSk7XG4gICAgICByZXNwb25zZS5kYXRhID0gW107XG4gICAgICBmb3IgKGNvbnN0IGl0ZW1JZCBvZiBkYXRhKSB7XG4gICAgICAgIHJlc3BvbnNlLmRhdGEucHVzaCh0aGlzLmdldE5vZGUoaXRlbUlkKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3BvbnNlLmNvbGxlY3Rpb24gPSBmYWxzZTtcbiAgICAgIHJlc3BvbnNlLmluZm8gPSBuZXcgRW50aXR5SW5mbyhpZCk7XG4gICAgICByZXNwb25zZS5tZXRhID0gTWV0YVNlcnZpY2UuZ2V0QnlOYW1lKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAndHlwZTonICsgaWQpKTtcbiAgICAgIHJlc3BvbnNlLmRhdGEgPSBkYXRhO1xuICAgICAgY29uc3QgcmVmZXJlbmNlcyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlblByZWZpeCArICdyZWZlcmVuY2VzOicgKyBpZCkpO1xuICAgICAgaWYgKHJlZmVyZW5jZXMpIHtcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gcmVmZXJlbmNlcykge1xuICAgICAgICAgIHJlc3BvbnNlLmRhdGFba2V5XSA9IHRoaXMuZ2V0Tm9kZShyZWZlcmVuY2VzW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXNwb25zZTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0Q29sbGVjdGlvbkluZm8oaWQ6IHN0cmluZykge1xuICAgIGNvbnN0IGRhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMudG9rZW5QcmVmaXggKyAnY29sbGVjdGlvbjonICsgaWQpKTtcbiAgICBpZiAoZGF0YSkge1xuICAgICAgY29uc3QgaW5mbyA9IG5ldyBDb2xsZWN0aW9uSW5mbyhpZCk7XG4gICAgICBmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG4gICAgICAgIGlmIChrZXkgIT09ICdfcGF0aCcpIHtcbiAgICAgICAgICBpbmZvW2tleV0gPSBkYXRhW2tleV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBpbmZvO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0VGltZXN0YW1wKGlkOiBzdHJpbmcpIHtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3RpbWVzdGFtcDonICsgaWQsIFN0cmluZyhEYXRlLm5vdygpKSk7XG4gIH1cblxuICBwcml2YXRlIGdldFRpbWVzdGFtcChpZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIE51bWJlcihsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuUHJlZml4ICsgJ3RpbWVzdGFtcDonICsgaWQpKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVtb3ZlVGltZXN0YW1wKGlkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odGhpcy50b2tlblByZWZpeCArICd0aW1lc3RhbXA6JyArIGlkKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgQXV0aCB9IGZyb20gJy4vYXV0aCc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBdXRoU2VydmljZSBpbXBsZW1lbnRzIEF1dGgge1xuICBhYnN0cmFjdCBnZXRJbmZvKCk6IGFueTtcblxuICBhYnN0cmFjdCBnZXRUb2tlbigpOiBzdHJpbmc7XG5cbiAgYWJzdHJhY3QgaXNTaWduZWRJbigpOiBib29sZWFuO1xuXG4gIGFic3RyYWN0IHJlbW92ZSgpO1xuXG4gIGFic3RyYWN0IHNldFRva2VuKHRva2VuOiBzdHJpbmcpO1xuXG4gIGFic3RyYWN0IHNpZ25Jbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+O1xuXG59XG4iLCJpbXBvcnQgeyBBdXRoVG9rZW4gfSBmcm9tICcuL2F1dGgtdG9rZW4nO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlIGltcGxlbWVudHMgQXV0aFRva2Vue1xuICBhYnN0cmFjdCBnZXQoKTogc3RyaW5nO1xuICBhYnN0cmFjdCBzZXQodG9rZW46IHN0cmluZyk7XG4gIGFic3RyYWN0IHJlbW92ZSgpO1xuICBhYnN0cmFjdCBpc1NldCgpOiBib29sZWFuO1xufVxuIiwiaW1wb3J0IHsgQXV0aFRva2VuIH0gZnJvbSAnLi9hdXRoLXRva2VuJztcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvY2FsU3RvcmFnZUF1dGhUb2tlblByb3ZpZGVyU2VydmljZSBpbXBsZW1lbnRzIEF1dGhUb2tlbiB7XG4gIHByaXZhdGUgdG9rZW5OYW1lID0gJ2FucmVzdDphdXRoX3Rva2VuJztcblxuICBnZXQoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy50b2tlbk5hbWUpO1xuICB9XG5cbiAgc2V0KHRva2VuOiBzdHJpbmcpIHtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnRva2VuTmFtZSwgdG9rZW4pO1xuICB9XG5cbiAgcmVtb3ZlKCkge1xuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMudG9rZW5OYW1lKTtcbiAgfVxuXG4gIGlzU2V0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnRva2VuTmFtZSkgIT0gbnVsbDtcbiAgfVxufVxuIiwiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1xuICBIdHRwUmVxdWVzdCxcbiAgSHR0cEhhbmRsZXIsXG4gIEh0dHBFdmVudCxcbiAgSHR0cEludGVyY2VwdG9yXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHsgQW5SZXN0Q29uZmlnLCBBcGlDb25maWcgfSBmcm9tICcuLi9jb3JlL2NvbmZpZyc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBdXRoSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBhdXRoOiBBdXRoU2VydmljZSwgQEluamVjdChBblJlc3RDb25maWcpIHByb3RlY3RlZCBjb25maWc6IEFwaUNvbmZpZykge31cblxuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgaWYgKCF0aGlzLmNvbmZpZy5hdXRoVXJsIHx8IHJlcXVlc3QudXJsLmluZGV4T2YodGhpcy5jb25maWcuYmFzZVVybCkgIT09IDAgfHwgcmVxdWVzdC51cmwgPT09IHRoaXMuY29uZmlnLmF1dGhVcmwpIHtcbiAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcbiAgICB9XG4gICAgaWYgKHRoaXMuYXV0aC5pc1NpZ25lZEluKCkpIHtcbiAgICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcbiAgICAgICAgc2V0SGVhZGVyczoge1xuICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHt0aGlzLmF1dGguZ2V0VG9rZW4oKX1gXG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCAqIGFzIGp3dF9kZWNvZGUgZnJvbSAnand0LWRlY29kZSc7XG5pbXBvcnQgeyBBblJlc3RDb25maWcsIEFwaUNvbmZpZyB9IGZyb20gJy4uL2NvcmUvY29uZmlnJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGggfSBmcm9tICcuL2F1dGgnO1xuaW1wb3J0IHsgQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoLXRva2VuLXByb3ZpZGVyLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgSnd0QXV0aFNlcnZpY2UgaW1wbGVtZW50cyBBdXRoIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXG4gICAgcHJpdmF0ZSB0b2tlbjogQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICAgIEBJbmplY3QoQW5SZXN0Q29uZmlnKSBwcm90ZWN0ZWQgY29uZmlnOiBBcGlDb25maWdcbiAgKSB7fVxuXG4gIGdldFRva2VuKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMudG9rZW4uZ2V0KCk7XG4gIH1cblxuICBzZXRUb2tlbih0b2tlbjogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy50b2tlbi5zZXQodG9rZW4pO1xuICB9XG5cbiAgcmVtb3ZlKCk6IHZvaWQge1xuICAgIHRoaXMudG9rZW4ucmVtb3ZlKCk7XG4gIH1cblxuICBpc1NpZ25lZEluKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnRva2VuLmlzU2V0KCk7XG4gIH1cblxuICBnZXRJbmZvKCk6IGFueSB7XG4gICAgcmV0dXJuIGp3dF9kZWNvZGUuY2FsbCh0aGlzLmdldFRva2VuKCkpO1xuICB9XG5cbiAgc2lnbkluKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZy5hdXRoVXJsLCB7XG4gICAgICBsb2dpbjogdXNlcm5hbWUsXG4gICAgICBwYXNzd29yZDogcGFzc3dvcmRcbiAgICB9KS5waXBlKFxuICAgICAgdGFwKChkYXRhOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5zZXRUb2tlbihkYXRhLnRva2VuKTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFJlc291cmNlSW5mbyB7XG4gIHBhdGg/OiBzdHJpbmc7XG4gIG5hbWU/OiBzdHJpbmc7XG4gIGNhY2hlPzogRnVuY3Rpb247XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBSZXNvdXJjZSAoaW5mbzogUmVzb3VyY2VJbmZvID0ge30pIHtcblxuICByZXR1cm4gKHRhcmdldDogRnVuY3Rpb24pID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0KTtcbiAgICBtZXRhLm5hbWUgPSBpbmZvLm5hbWUgfHwgdGFyZ2V0Lm5hbWU7XG4gICAgbWV0YS5jYWNoZSA9IGluZm8uY2FjaGU7XG4gIH07XG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFByb3BlcnR5SW5mbyB7XG4gIGNvbGxlY3Rpb24/OiBib29sZWFuO1xuICBleGNsdWRlV2hlblNhdmluZz86IGJvb2xlYW47XG4gIHR5cGU/OiBhbnk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBQcm9wZXJ0eSAoaW5mbzogUHJvcGVydHlJbmZvID0ge30pIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZykgPT4ge1xuICAgIGNvbnN0IG1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvclByb3BlcnR5KHRhcmdldCwga2V5LCBpbmZvLnR5cGUpO1xuICAgIG1ldGEuaXNDb2xsZWN0aW9uID0gaW5mby5jb2xsZWN0aW9uIHx8IGZhbHNlO1xuICAgIG1ldGEuZXhjbHVkZVdoZW5TYXZpbmcgPSBpbmZvLmV4Y2x1ZGVXaGVuU2F2aW5nIHx8IGZhbHNlO1xuICB9O1xufVxuIiwiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcbmltcG9ydCAqIGFzIHBsdXJhbGl6ZSBmcm9tICdwbHVyYWxpemUnO1xuXG5leHBvcnQgZnVuY3Rpb24gSHR0cFNlcnZpY2UoZW50aXR5OiBGdW5jdGlvbiwgcGF0aD86IHN0cmluZykge1xuXG4gIHJldHVybiAodGFyZ2V0OiBGdW5jdGlvbikgPT4ge1xuICAgIGNvbnN0IG1ldGEgPSBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckh0dHBTZXJ2aWNlKHRhcmdldCwgZW50aXR5KTtcbiAgICBtZXRhLnBhdGggPSBwYXRoIHx8ICcvJyArIHBsdXJhbGl6ZS5wbHVyYWwoZW50aXR5Lm5hbWUpLnJlcGxhY2UoL1xcLj8oW0EtWl0pL2csICctJDEnKS5yZXBsYWNlKC9eLS8sICcnKS50b0xvd2VyQ2FzZSgpO1xuICAgIG1ldGEuZW50aXR5TWV0YS5zZXJ2aWNlID0gdGFyZ2V0O1xuICB9O1xufVxuIiwiaW1wb3J0IHsgTWV0YVNlcnZpY2UgfSBmcm9tICcuLi9tZXRhJztcblxuZXhwb3J0IGZ1bmN0aW9uIEhlYWRlcihoZWFkZXJOYW1lOiBzdHJpbmcsIGFwcGVuZD86IGJvb2xlYW4pIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZywgdmFsdWU6IGFueSA9IHVuZGVmaW5lZCkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3RvcikuaGVhZGVycy5wdXNoKHtcbiAgICAgIG5hbWU6IGhlYWRlck5hbWUsXG4gICAgICB2YWx1ZTogdmFsdWUgPyB2YWx1ZS52YWx1ZSA6IGZ1bmN0aW9uICgpIHsgcmV0dXJuICB0aGlzW2tleV07IH0sXG4gICAgICBhcHBlbmQ6IGFwcGVuZCB8fCBmYWxzZSxcbiAgICAgIGR5bmFtaWM6IHRydWVcbiAgICB9KTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBIZWFkZXJzIChoZWFkZXJzOiB7IG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZywgYXBwZW5kPzogYm9vbGVhbn1bXSkge1xuXG4gIHJldHVybiAodGFyZ2V0OiBhbnkpID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0KTtcbiAgICBmb3IgKGNvbnN0IGhlYWRlciBvZiBoZWFkZXJzKSB7XG4gICAgICBtZXRhLmhlYWRlcnMucHVzaCh7XG4gICAgICAgIG5hbWU6IGhlYWRlci5uYW1lLFxuICAgICAgICB2YWx1ZTogKCkgPT4gaGVhZGVyLnZhbHVlLFxuICAgICAgICBhcHBlbmQ6IGhlYWRlci5hcHBlbmQgfHwgZmFsc2UsXG4gICAgICAgIGR5bmFtaWM6IGZhbHNlXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG59XG4iLCJpbXBvcnQgeyBNZXRhU2VydmljZSB9IGZyb20gJy4uL21ldGEnO1xuXG5leHBvcnQgZnVuY3Rpb24gQm9keSgpIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IGFueSA9IHVuZGVmaW5lZCwgdmFsdWU6IGFueSA9IHVuZGVmaW5lZCkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3RvcikuYm9keSA9IChvYmplY3Q6IGFueSkgPT4ge1xuICAgICAgcmV0dXJuICh2YWx1ZSA/IHZhbHVlLnZhbHVlIDogZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpc1trZXldOyB9KS5jYWxsKG9iamVjdCk7XG4gICAgfTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBTdWJyZXNvdXJjZSAodHlwZTogRnVuY3Rpb24sIHBhdGg/OiBzdHJpbmcpIHtcblxuICByZXR1cm4gKHRhcmdldDogYW55LCBrZXk6IHN0cmluZywgdmFsdWU6IGFueSkgPT4ge1xuICAgIE1ldGFTZXJ2aWNlLmdldE9yQ3JlYXRlRm9yRW50aXR5KHRhcmdldC5jb25zdHJ1Y3Rvcikuc3VicmVzb3VyY2VzW2tleV0gPSB7XG4gICAgICBtZXRhOiBNZXRhU2VydmljZS5nZXRPckNyZWF0ZUZvckVudGl0eSh0eXBlKSxcbiAgICAgIHBhdGg6IHBhdGggfHwgJy8nICsga2V5LnJlcGxhY2UoL1xcLj8oW0EtWl0pL2csICctJDEnKS5yZXBsYWNlKC9eLS8sICcnKS50b0xvd2VyQ2FzZSgpXG4gICAgfTtcbiAgfTtcbn1cbiIsImltcG9ydCB7IE1ldGFTZXJ2aWNlIH0gZnJvbSAnLi4vbWV0YSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBJZCAoKSB7XG5cbiAgcmV0dXJuICh0YXJnZXQ6IGFueSwga2V5OiBzdHJpbmcpID0+IHtcbiAgICBjb25zdCBtZXRhID0gTWV0YVNlcnZpY2UuZ2V0T3JDcmVhdGVGb3JFbnRpdHkodGFyZ2V0LmNvbnN0cnVjdG9yKTtcbiAgICBtZXRhLmlkID0ga2V5O1xuICB9O1xufVxuIiwiaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBjb21iaW5lTGF0ZXN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUsIGRpc3RpbmN0VW50aWxDaGFuZ2VkLCBtYXAsIG1lcmdlTWFwLCBzaGFyZSwgc2hhcmVSZXBsYXksIHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEFwaVNlcnZpY2UgfSBmcm9tICcuLi9jb3JlJztcbmltcG9ydCB7IENvbGxlY3Rpb24gfSBmcm9tICcuLi9jb3JlL2NvbGxlY3Rpb24nO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgTG9hZGVyIHtcblxuICBwcml2YXRlIHJlYWRvbmx5IGZpZWxkcyA9IHt9O1xuICBwcml2YXRlIHJlYWRvbmx5IG9ic2VydmFibGU6IE9ic2VydmFibGU8YW55PjtcbiAgcHJpdmF0ZSBsb2FkaW5nID0gZmFsc2U7XG4gIHByb3RlY3RlZCBsYXN0RGF0YTogQ29sbGVjdGlvbjxhbnk+O1xuXG4gIGFic3RyYWN0IGdldEF2YWlsYWJsZUZpZWxkcygpOiBzdHJpbmdbXTtcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgc2VydmljZTogQXBpU2VydmljZSwgd2FpdEZvciA9IDAsIHJlcGxheSA9IDApIHtcbiAgICB0aGlzLm9ic2VydmFibGUgPSBjb21iaW5lTGF0ZXN0KHRoaXMuZ2V0U3ViamVjdHMoKSlcbiAgICAgIC5waXBlKFxuICAgICAgICByZXBsYXkgPyBzaGFyZVJlcGxheShyZXBsYXkpIDogc2hhcmUoKSxcbiAgICAgICAgZGVib3VuY2VUaW1lKHdhaXRGb3IpLFxuICAgICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgoZDE6IGFueVtdLCBkMjogYW55W10pID0+IHRoaXMuY2hlY2tEaXN0aW5jdChkMSwgZDIpKSxcbiAgICAgICAgbWFwKCh2YWx1ZXM6IGFueVtdKSA9PiB0aGlzLm1hcFBhcmFtcyh2YWx1ZXMpKSxcbiAgICAgICAgbWVyZ2VNYXAoKGRhdGEpID0+IHtcbiAgICAgICAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgIHJldHVybiB0aGlzLmdldFNlcnZpY2VPYnNlcnZhYmxlKGRhdGEpLnBpcGUoXG4gICAgICAgICAgICB0YXAoKHY6IENvbGxlY3Rpb248YW55PikgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmxhc3REYXRhID0gdjtcbiAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICk7XG4gICAgICAgIH0pLFxuICAgICAgKTtcbiAgfVxuXG4gIGZpZWxkKG5hbWUpOiBCZWhhdmlvclN1YmplY3Q8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuZmllbGRzW25hbWVdO1xuICB9XG5cbiAgZ2V0IGRhdGEoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5vYnNlcnZhYmxlO1xuICB9XG5cbiAgZ2V0IGlzTG9hZGluZygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5sb2FkaW5nO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFNlcnZpY2VPYnNlcnZhYmxlKFtkYXRhXTogYW55W10pIHtcbiAgICByZXR1cm4gdGhpcy5zZXJ2aWNlLmdldExpc3QoZGF0YSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgY2hlY2tEaXN0aW5jdChkMTogYW55W10sIGQyOiBhbnlbXSkge1xuICAgIGNvbnN0IG9mZnNldCA9IGQxLmxlbmd0aCAtIHRoaXMuZ2V0QXZhaWxhYmxlRmllbGRzKCkubGVuZ3RoO1xuICAgIGZvciAobGV0IGkgPSBkMS5sZW5ndGg7IGkgPj0gMDsgaS0tKSB7XG4gICAgICBpZiAoKGkgPCBvZmZzZXQgJiYgZDJbaV0pIHx8IChpID49IG9mZnNldCAmJiBkMVtpXSAhPT0gZDJbaV0pKSB7XG4gICAgICAgIGlmIChpID49IG9mZnNldCkge1xuICAgICAgICAgIGQyLmZpbGwodW5kZWZpbmVkLCAwLCBvZmZzZXQpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBwcm90ZWN0ZWQgbWFwUGFyYW1zKHZhbHVlczogYW55W10pIHtcbiAgICBjb25zdCBmaWVsZHMgPSB0aGlzLmdldEF2YWlsYWJsZUZpZWxkcygpO1xuICAgIGNvbnN0IG9mZnNldCA9IHZhbHVlcy5sZW5ndGggLSBmaWVsZHMubGVuZ3RoO1xuICAgIGNvbnN0IGRhdGEgPSB7fTtcbiAgICB2YWx1ZXMuc2xpY2Uob2Zmc2V0KS5mb3JFYWNoKCh2LCBpKSA9PiB7XG4gICAgICBpZiAodiAhPT0gdW5kZWZpbmVkICYmIHYgIT09IG51bGwgJiYgdiAhPT0gJycpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB2ID09PSAnYm9vbGVhbicpIHtcbiAgICAgICAgICB2ID0gdiA/ICd0cnVlJyA6ICdmYWxzZSc7XG4gICAgICAgIH1cbiAgICAgICAgZGF0YVtmaWVsZHNbaV1dID0gU3RyaW5nKHYpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiB2YWx1ZXMuc2xpY2UoMCwgb2Zmc2V0KS5jb25jYXQoW2RhdGFdKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTdWJqZWN0cygpOiBCZWhhdmlvclN1YmplY3Q8YW55PltdIHtcbiAgICByZXR1cm4gdGhpcy5nZXRBdmFpbGFibGVGaWVsZHMoKS5tYXAoKG5hbWUpID0+IHtcbiAgICAgIHJldHVybiB0aGlzLmZpZWxkc1tuYW1lXSA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55Pih1bmRlZmluZWQpO1xuICAgIH0pO1xuICB9XG59XG4iLCJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IExvYWRlciB9IGZyb20gJy4vbG9hZGVyJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEluZmluaXRlU2Nyb2xsTG9hZGVyIGV4dGVuZHMgTG9hZGVyIHtcblxuICBwcml2YXRlIGxvYWRNb3JlU3ViamVjdDtcblxuICBsb2FkTW9yZSgpIHtcbiAgICB0aGlzLmxvYWRNb3JlU3ViamVjdC5uZXh0KHRydWUpO1xuICB9XG5cbiAgaGFzTW9yZSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5sYXN0RGF0YSAmJiB0aGlzLmxhc3REYXRhLmhhc01vcmUoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTZXJ2aWNlT2JzZXJ2YWJsZShbbG9hZE1vcmUsIGRhdGFdOiBhbnlbXSk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIGxvYWRNb3JlID8gdGhpcy5sYXN0RGF0YS5sb2FkTW9yZSgpIDogc3VwZXIuZ2V0U2VydmljZU9ic2VydmFibGUoW2RhdGFdKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTdWJqZWN0cygpOiBCZWhhdmlvclN1YmplY3Q8YW55PltdIHtcbiAgICB0aGlzLmxvYWRNb3JlU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICAgIGNvbnN0IHN1YmplY3RzID0gc3VwZXIuZ2V0U3ViamVjdHMoKTtcbiAgICBzdWJqZWN0cy51bnNoaWZ0KHRoaXMubG9hZE1vcmVTdWJqZWN0KTtcbiAgICByZXR1cm4gc3ViamVjdHM7XG4gIH1cbn1cbiIsImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgY29tYmluZUxhdGVzdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgZGVib3VuY2VUaW1lLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCwgbWFwLCBtZXJnZU1hcCwgc2hhcmUsIHNoYXJlUmVwbGF5LCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBBcGlTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZSc7XG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi4vY29yZS9jb2xsZWN0aW9uJztcbmltcG9ydCB7IExvYWRlciB9IGZyb20gJy4vbG9hZGVyJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIFBhZ2VMb2FkZXIgZXh0ZW5kcyBMb2FkZXIge1xuXG4gIHByaXZhdGUgcGFnZVN1YmplY3Q7XG5cbiAgZmlyc3QoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc0ZpcnN0KCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5wYWdlU3ViamVjdC5uZXh0KHRoaXMubGFzdERhdGEuZmlyc3RQYXRoKCkpO1xuICB9XG5cbiAgcHJldmlvdXMoKSB7XG4gICAgaWYgKCF0aGlzLmxhc3REYXRhIHx8ICF0aGlzLmxhc3REYXRhLmhhc1ByZXZpb3VzKCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5wYWdlU3ViamVjdC5uZXh0KHRoaXMubGFzdERhdGEucHJldmlvdXNQYXRoKCkpO1xuICB9XG5cbiAgbmV4dCgpIHtcbiAgICBpZiAoIXRoaXMubGFzdERhdGEgfHwgIXRoaXMubGFzdERhdGEuaGFzTmV4dCgpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLm5leHRQYXRoKCkpO1xuICB9XG5cbiAgbGFzdCgpIHtcbiAgICBpZiAoIXRoaXMubGFzdERhdGEgfHwgIXRoaXMubGFzdERhdGEuaGFzTGFzdCgpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMucGFnZVN1YmplY3QubmV4dCh0aGlzLmxhc3REYXRhLmxhc3RQYXRoKCkpO1xuICB9XG5cbiAgaGFzRmlyc3QoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNGaXJzdCgpO1xuICB9XG5cbiAgaGFzUHJldmlvdXMoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNQcmV2aW91cygpO1xuICB9XG5cbiAgaGFzTmV4dCgpIHtcbiAgICByZXR1cm4gdGhpcy5sYXN0RGF0YSAmJiB0aGlzLmxhc3REYXRhLmhhc05leHQoKTtcbiAgfVxuXG4gIGhhc0xhc3QoKSB7XG4gICAgcmV0dXJuIHRoaXMubGFzdERhdGEgJiYgdGhpcy5sYXN0RGF0YS5oYXNMYXN0KCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U2VydmljZU9ic2VydmFibGUoW3BhZ2UsIGRhdGFdOiBhbnlbXSkge1xuICAgIHJldHVybiBwYWdlID8gdGhpcy5zZXJ2aWNlLmRvR2V0KHBhZ2UsIGRhdGEpIDogc3VwZXIuZ2V0U2VydmljZU9ic2VydmFibGUoW2RhdGFdKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTdWJqZWN0cygpOiBCZWhhdmlvclN1YmplY3Q8YW55PltdIHtcbiAgICB0aGlzLnBhZ2VTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+KHVuZGVmaW5lZCk7XG4gICAgY29uc3Qgc3ViamVjdHMgPSBzdXBlci5nZXRTdWJqZWN0cygpO1xuICAgIHN1YmplY3RzLnVuc2hpZnQodGhpcy5wYWdlU3ViamVjdCk7XG4gICAgcmV0dXJuIHN1YmplY3RzO1xuICB9XG59XG4iLCJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMsIEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgJ3JlZmxlY3QtbWV0YWRhdGEnO1xuXG5pbXBvcnQge1xuICBBcGlTZXJ2aWNlLFxuICBEYXRhSW5mb1NlcnZpY2UsXG4gIEFuUmVzdENvbmZpZyxcbiAgQXBpQ29uZmlnLFxuICBBcGlJbnRlcmNlcHRvcixcbiAgQXBpUHJvY2Vzc29yLFxuICBPYmplY3RDb2xsZWN0b3IsXG4gIFJlZmVyZW5jZUludGVyY2VwdG9yXG59IGZyb20gJy4vY29yZSc7XG5pbXBvcnQgeyBFdmVudHNTZXJ2aWNlIH0gZnJvbSAnLi9ldmVudHMnO1xuaW1wb3J0IHtcbiAgQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIsXG4gIExkSnNvbkRhdGFOb3JtYWxpemVyLFxuICBKc29uRGF0YU5vcm1hbGl6ZXJcbn0gZnJvbSAnLi9yZXNwb25zZSc7XG5pbXBvcnQge1xuICBBdXRoSW50ZXJjZXB0b3IsXG4gIEF1dGhTZXJ2aWNlLFxuICBBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UsXG4gIEp3dEF1dGhTZXJ2aWNlLFxuICBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2Vcbn0gZnJvbSAnLi9hdXRoJztcbmltcG9ydCB7XG4gIEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgQ2FjaGVJbnRlcmNlcHRvcixcbiAgTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSxcbiAgTWVtb3J5UmVzcG9uc2VDYWNoZSxcbiAgTm9SZXNwb25zZUNhY2hlLFxuICBSZXNwb25zZUNhY2hlXG59IGZyb20gJy4vY2FjaGUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgSHR0cENsaWVudE1vZHVsZSxcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBblJlc3RNb2R1bGUge1xuICBzdGF0aWMgY29uZmlnKGNvbmZpZz86IEFwaUNvbmZpZyk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogQW5SZXN0TW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBblJlc3RDb25maWcsXG4gICAgICAgICAgdXNlVmFsdWU6IGNvbmZpZyxcbiAgICAgICAgfSxcbiAgICAgICAgRXZlbnRzU2VydmljZSxcbiAgICAgICAgRGF0YUluZm9TZXJ2aWNlLFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBMb2NhbFN0b3JhZ2VSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IE1lbW9yeVJlc3BvbnNlQ2FjaGUsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTm9SZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcuY2FjaGUgfHwgTm9SZXNwb25zZUNhY2hlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2UsXG4gICAgICAgICAgdXNlQ2xhc3M6IGNvbmZpZy50b2tlblByb3ZpZGVyIHx8IExvY2FsU3RvcmFnZUF1dGhUb2tlblByb3ZpZGVyU2VydmljZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQXV0aFNlcnZpY2UsXG4gICAgICAgICAgdXNlQ2xhc3M6IGNvbmZpZy5hdXRoU2VydmljZSB8fCBKd3RBdXRoU2VydmljZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEF1dGhJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IFJlZmVyZW5jZUludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBIVFRQX0lOVEVSQ0VQVE9SUyxcbiAgICAgICAgICB1c2VDbGFzczogQ2FjaGVJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEFwaUludGVyY2VwdG9yLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfSFRUUF9EQVRBX05PUk1BTElaRVJTLFxuICAgICAgICAgIHVzZUNsYXNzOiBSZWZlcmVuY2VEYXRhTm9ybWFsaXplcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgICAgICAgICB1c2VDbGFzczogTGRKc29uRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9IVFRQX0RBVEFfTk9STUFMSVpFUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IEpzb25EYXRhTm9ybWFsaXplcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICBPYmplY3RDb2xsZWN0b3IsXG4gICAgICAgIEFwaVByb2Nlc3NvcixcbiAgICAgICAgQXBpU2VydmljZVxuICAgICAgXVxuICAgIH07XG4gIH1cbiAgc3RhdGljIGNvbmZpZ1dpdGhvdXROb3JtYWxpemVycyhjb25maWc/OiBBcGlDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IEFuUmVzdE1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQW5SZXN0Q29uZmlnLFxuICAgICAgICAgIHVzZVZhbHVlOiBjb25maWcsXG4gICAgICAgIH0sXG4gICAgICAgIEV2ZW50c1NlcnZpY2UsXG4gICAgICAgIERhdGFJbmZvU2VydmljZSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEFOUkVTVF9DQUNIRV9TRVJWSUNFUyxcbiAgICAgICAgICB1c2VDbGFzczogTG9jYWxTdG9yYWdlUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0NBQ0hFX1NFUlZJQ0VTLFxuICAgICAgICAgIHVzZUNsYXNzOiBNZW1vcnlSZXNwb25zZUNhY2hlLFxuICAgICAgICAgIG11bHRpOiB0cnVlXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBBTlJFU1RfQ0FDSEVfU0VSVklDRVMsXG4gICAgICAgICAgdXNlQ2xhc3M6IE5vUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogUmVzcG9uc2VDYWNoZSxcbiAgICAgICAgICB1c2VDbGFzczogY29uZmlnLmNhY2hlIHx8IE5vUmVzcG9uc2VDYWNoZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQXV0aFRva2VuUHJvdmlkZXJTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcudG9rZW5Qcm92aWRlciB8fCBMb2NhbFN0b3JhZ2VBdXRoVG9rZW5Qcm92aWRlclNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEF1dGhTZXJ2aWNlLFxuICAgICAgICAgIHVzZUNsYXNzOiBjb25maWcuYXV0aFNlcnZpY2UgfHwgSnd0QXV0aFNlcnZpY2VcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBdXRoSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBSZWZlcmVuY2VJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICAgICAgdXNlQ2xhc3M6IENhY2hlSW50ZXJjZXB0b3IsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgICAgIHVzZUNsYXNzOiBBcGlJbnRlcmNlcHRvcixcbiAgICAgICAgICBtdWx0aTogdHJ1ZVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogQU5SRVNUX0hUVFBfREFUQV9OT1JNQUxJWkVSUyxcbiAgICAgICAgICB1c2VDbGFzczogUmVmZXJlbmNlRGF0YU5vcm1hbGl6ZXIsXG4gICAgICAgICAgbXVsdGk6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAgT2JqZWN0Q29sbGVjdG9yLFxuICAgICAgICBBcGlQcm9jZXNzb3IsXG4gICAgICAgIEFwaVNlcnZpY2VcbiAgICAgIF1cbiAgICB9O1xuICB9XG59XG4iXSwibmFtZXMiOlsiand0X2RlY29kZS5jYWxsIiwicGx1cmFsaXplLnBsdXJhbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsdUJBY2EsWUFBWSxHQUFHLElBQUksY0FBYyxDQUFZLG1CQUFtQixDQUFDOzs7Ozs7QUNkOUU7Ozs7SUFJRSxZQUFvQixLQUFLO1FBQUwsVUFBSyxHQUFMLEtBQUssQ0FBQTtLQUFJOzs7O0lBRTdCLElBQUksSUFBSTtRQUNOLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztLQUNuQjtDQUNGOzs7OztJQWFDLFlBQW9CLEtBQUs7UUFBTCxVQUFLLEdBQUwsS0FBSyxDQUFBO0tBQUk7Ozs7SUFFN0IsSUFBSSxJQUFJO1FBQ04sT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ25CO0NBQ0Y7Ozs7OztJQUtDLEdBQUcsQ0FBQyxNQUFNO1FBQ1IsT0FBTyxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQztLQUNuRDs7Ozs7O0lBRUQsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFTO1FBQ25CLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztLQUNyRDs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxNQUFNO1FBQ3JCLHlCQUF1QixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFDO0tBQ3pDOzs7OztJQUVELFlBQVksQ0FBQyxNQUFNO1FBQ2pCLHlCQUFtQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFDO0tBQ3JDOzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsSUFBb0I7UUFDM0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDeEI7Ozs7OztJQUVELFlBQVksQ0FBQyxNQUFNLEVBQUUsSUFBZ0I7UUFDbkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDeEI7OztZQXpCRixVQUFVOzs7Ozs7O0FDN0JYOzs7QUFLQSxnQkFBMkIsU0FBUSxLQUFROzs7Ozs7SUFrQnpDLFlBQW9CLE9BQW1CLEVBQVUsSUFBb0IsRUFBRSxHQUFHLEtBQVU7UUFDbEYsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFERSxZQUFPLEdBQVAsT0FBTyxDQUFZO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBZ0I7UUFFbkUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0tBQ25EOzs7Ozs7O0lBbkJELE9BQU8sS0FBSyxDQUFJLEVBQWlCLEVBQUUsRUFBaUI7UUFDbEQsdUJBQU0sSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2pDLHVCQUFNLENBQUMsR0FBRyxJQUFJLFVBQVUsQ0FBSSxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdCLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdCLE9BQU8sQ0FBQyxDQUFDO0tBQ1Y7Ozs7SUFPRCxJQUFJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ3hCOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ04sT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztLQUN2Qjs7OztJQUVELElBQUksU0FBUztRQUNYLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7S0FDM0I7Ozs7SUFFRCxLQUFLO1FBQ0gseUJBQXdCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUM7S0FDN0Q7Ozs7SUFFRCxRQUFRO1FBQ04seUJBQXdCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUM7S0FDaEU7Ozs7SUFFRCxJQUFJO1FBQ0YseUJBQXdCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUM7S0FDNUQ7Ozs7SUFFRCxJQUFJO1FBQ0YseUJBQXdCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUM7S0FDNUQ7Ozs7SUFFRCxTQUFTO1FBQ1AsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztLQUN4Qjs7OztJQUVELFlBQVk7UUFDVixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0tBQzNCOzs7O0lBRUQsUUFBUTtRQUNOLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7S0FDdkI7Ozs7SUFFRCxRQUFRO1FBQ04sT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztLQUN2Qjs7OztJQUVELFFBQVE7UUFDTixPQUFPLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQ3JCLEdBQUcsQ0FBQyxDQUFDLE1BQXFCLEtBQUssVUFBVSxDQUFDLEtBQUssQ0FBSSxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FDbEUsQ0FBQztLQUNIOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ3ZCOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ3pCOzs7O0lBRUQsV0FBVztRQUNULE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0tBQzdCOzs7O0lBRUQsUUFBUTtRQUNOLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0tBQzNCOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ3ZCO0NBQ0Y7Ozs7OztBQ2pHRDs7Ozs7SUFZRSxZQUFZLElBQVMsRUFBRSxJQUFlO1FBQ3BDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0tBQ2xCOzs7O0lBRUQsSUFBSTtRQUNGLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztLQUNuQjs7OztJQUVELE1BQU07UUFDSixPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztLQUN0RztDQUNGO29CQUUyQixTQUFRLFNBQVM7Ozs7OztJQUczQyxZQUFZLElBQVksRUFBRSxJQUFlLEVBQUUsTUFBbUI7UUFDNUQsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztLQUN2Qjs7OztJQUVELE1BQU07UUFDSixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7S0FDbEI7Ozs7SUFFRCxNQUFNO1FBQ0osT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0tBQ3JCO0NBQ0Y7bUJBQzBCLFNBQVEsU0FBUztDQUFHO0FBQy9DLHFCQUE2QixTQUFRLFNBQVM7Q0FBRztBQUNqRCxvQkFBNEIsU0FBUSxTQUFTO0NBQUc7QUFDaEQsdUJBQStCLFNBQVEsU0FBUztDQUFHO0FBQ25ELHNCQUE4QixTQUFRLFNBQVM7Q0FBRztxQkFNdkMsMkJBQTJCLEdBQUcsSUFBSSxjQUFjLENBQWtCLDZCQUE2QixDQUFDOzs7Ozs7QUNwRDNHOzs7O0lBSUUsWUFBb0IsS0FBZTtRQUFmLFVBQUssR0FBTCxLQUFLLENBQVU7S0FBSTs7OztJQUV2QyxJQUFJLElBQUk7UUFDTixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7S0FDbkI7Q0FDRjtzQkFFNkIsU0FBUSxRQUFRO0NBRzdDOzJCQUVrQyxTQUFRLFFBQVE7OztzQkFDTyxFQUFFOztDQUMzRDtvQkFFMkIsU0FBUSxRQUFROzs7a0JBRTlCLElBQUk7dUJBRTZFLEVBQUU7MEJBQ3BDLEVBQUU7NEJBQ3FCLEVBQUU7Ozs7OztJQUk3RSxVQUFVLENBQUMsTUFBWTtRQUM1QixxQkFBSSxPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUNoQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xELEtBQUssdUJBQU0sTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxFQUFFO2dCQUM3QixPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUM3RjtTQUNGO1FBQ0QsT0FBTyxPQUFPLENBQUM7Ozs7O0lBR1Ysb0JBQW9CO1FBQ3pCLHVCQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDcEIsS0FBSyx1QkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxpQkFBaUIsRUFBRTtnQkFDaEQsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN6QjtTQUNGO1FBQ0QsT0FBTyxRQUFRLENBQUM7O0NBRW5CO3FCQUU0QixTQUFRLFFBQVE7Ozs7O0lBRzNDLFlBQVksSUFBYyxFQUFVLFdBQTJCO1FBQzdELEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQURzQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7S0FFOUQ7Ozs7SUFFRCxJQUFJLFVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7S0FDekI7Q0FDRjs7Ozs7O0FDOUREOzs7OztJQU1FLE9BQU8sR0FBRyxDQUFDLEdBQVE7UUFDakIsT0FBTyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUNqQzs7Ozs7SUFFRCxPQUFPLFNBQVMsQ0FBQyxHQUFXO1FBQzFCLEtBQUssdUJBQU0sSUFBSSxJQUFJLFdBQVcsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDM0MsSUFBSSxJQUFJLFlBQVksY0FBYyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssR0FBRyxFQUFFO2dCQUN2RCxPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7S0FDRjs7Ozs7SUFFRCxPQUFPLG9CQUFvQixDQUFDLEdBQWE7UUFDdkMscUJBQUksSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQixXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZCO1FBQ0QseUJBQXVCLElBQUksRUFBQztLQUM3Qjs7Ozs7SUFFRCxPQUFPLDJCQUEyQixDQUFDLEdBQWE7UUFDOUMscUJBQUksSUFBSSxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULElBQUksR0FBRyxJQUFJLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RDLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCx5QkFBOEIsSUFBSSxFQUFDO0tBQ3BDOzs7Ozs7O0lBRUQsT0FBTyxzQkFBc0IsQ0FBQyxHQUFRLEVBQUUsUUFBZ0IsRUFBRSxJQUFVO1FBQ2xFLHVCQUFNLFVBQVUsR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BDLFVBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7U0FDbkg7UUFDRCxPQUFPLFVBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDeEM7Ozs7OztJQUVELE9BQU8seUJBQXlCLENBQUMsR0FBYSxFQUFFLE1BQWdCO1FBQzlELHFCQUFJLElBQUksR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDVCxJQUFJLEdBQUcsSUFBSSxlQUFlLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzFFLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkI7UUFDRCx5QkFBd0IsSUFBSSxFQUFDO0tBQzlCOzs7OztJQUVELE9BQU8sR0FBRyxDQUFDLElBQWM7UUFDdkIsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztLQUN0Qzs7a0JBbkR3QyxJQUFJLEdBQUcsRUFBaUI7Ozs7Ozs7Ozs7O0FDSm5FOzs7O0lBU0UsWUFDb0U7UUFBQSxhQUFRLEdBQVIsUUFBUTtRQUUxRSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7U0FDcEI7S0FDRjs7Ozs7SUFFTSxNQUFNLENBQUMsS0FBWTtRQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQXNCO1lBQzNDLHVCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzFFLEtBQUssdUJBQU0sU0FBUyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ25DLElBQUksU0FBUyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsV0FBVyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEtBQUssS0FBSyxDQUFDLE1BQU0sRUFBRSxFQUFFO29CQUMvRSxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN2QjthQUNGO1NBQ0YsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxLQUFLLENBQUM7Ozs7WUFwQmhCLFVBQVU7Ozs7d0NBSU4sUUFBUSxZQUFJLE1BQU0sU0FBQywyQkFBMkI7Ozs7Ozs7Ozs7OztBQ1ZuRDs7Ozs7OztJQXFCRSxZQUNZLElBQWdCLEVBQ2hCLE1BQXFCLEVBQ3JCLFdBQTRCLEVBQ047UUFIdEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLGdCQUFXLEdBQVgsV0FBVyxDQUFpQjtRQUNOLFdBQU0sR0FBTixNQUFNO1FBRXRDLElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDL0M7Ozs7O0lBRUQsT0FBTyxDQUFDLE1BQThEO1FBQ3BFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztLQUMzQzs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBbUI7UUFDckIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQztLQUM5Qzs7Ozs7SUFFRCxNQUFNLENBQUMsTUFBVztRQUNoQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDL0Q7Ozs7O0lBRUQsWUFBWSxDQUFDLEVBQW1CO1FBQzlCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLEVBQUUsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDdEY7Ozs7OztJQUVELEtBQUssQ0FBQyxJQUFZLEVBQUUsTUFBOEQ7UUFDaEYscUJBQUksQ0FBYSxDQUFDO1FBQ2xCLElBQUksTUFBTSxZQUFZLFVBQVUsRUFBRTtZQUNoQyxDQUFDLEdBQUcsTUFBTSxDQUFDO1NBQ1o7YUFBTTtZQUNMLENBQUMsR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1lBQ3JCLEtBQUssdUJBQU0sR0FBRyxJQUFJLE1BQU0sRUFBRTtnQkFDeEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxHQUFHLE9BQU8sSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMzRztTQUNGO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDLElBQUksQ0FDNUcsR0FBRyxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQ3RGLENBQUM7S0FDSDs7Ozs7SUFFRCxJQUFJLENBQUMsTUFBVztRQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDaEQscUJBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNkLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFO1lBQzdCLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDMUM7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUN0RztRQUNELHFCQUFJLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVqRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FDbkMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQzdFLElBQUksRUFDSixFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FDbkQsQ0FBQyxJQUFJLENBQ0osR0FBRyxDQUFDLENBQUMsSUFBSTtZQUNQLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNULElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM3QztTQUNGLENBQUMsRUFDRixHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQzFELENBQUM7S0FDTDs7Ozs7SUFFRCxNQUFNLENBQUMsTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDbEQsdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25ELElBQUksSUFBSSxFQUFFO1lBQ1IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUFDLENBQUMsSUFBSSxDQUMvRyxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FDNUQsQ0FBQztTQUNIO0tBQ0Y7OztZQTlFRixVQUFVOzs7O1lBaEJGLFVBQVU7WUFRakIsYUFBYTtZQUxOLGVBQWU7NENBcUJuQixNQUFNLFNBQUMsWUFBWTs7Ozs7OztBQ3pCeEI7Ozs7SUFlRSxZQUMyQztRQUFBLFdBQU0sR0FBTixNQUFNO0tBQzdDOzs7Ozs7SUFFSixTQUFTLENBQUMsT0FBeUIsRUFBRSxJQUFpQjtRQUNwRCxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDOUgsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDOUM7Ozs7O0lBRU8sa0JBQWtCLENBQUMsT0FBeUI7UUFDbEQsT0FBTyxPQUFPLENBQUMsTUFBTSxLQUFLLEtBQUssSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7SUFHOUYsdUJBQXVCLENBQUMsT0FBeUI7UUFDdkQsdUJBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLE9BQU8sRUFBRSxDQUFDLElBQUksWUFBWSxDQUFDO1lBQ3pCLElBQUksRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUM7WUFDMUgsTUFBTSxFQUFFLEdBQUc7WUFDWCxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUc7U0FDakIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNOLEdBQUcsQ0FBQyxDQUFDLFFBQVE7WUFDWCxJQUFJLFFBQVEsWUFBWSxZQUFZLEVBQUU7Z0JBQ3BDLE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDcEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxZQUFZLENBQUM7aUJBQzVELENBQUMsQ0FBQzthQUNKO1NBQ0YsQ0FBQyxDQUNILENBQUM7Ozs7WUFoQ0wsVUFBVTs7Ozs0Q0FJTixNQUFNLFNBQUMsWUFBWTs7Ozs7OztBQ2Z4QixxQkFRVyw0QkFBNEIsR0FBRyxJQUFJLGNBQWMsQ0FBbUIsOEJBQThCLENBQUM7Ozs7OztBQ1Q5RztDQUtDOzs7Ozs7QUNMRDs7OztJQU9JLFlBQW9CLElBQXFCO1FBQXJCLFNBQUksR0FBSixJQUFJLENBQWlCO21CQUZoQixJQUFJLEdBQUcsRUFBZTtLQUVGOzs7OztJQUU3QyxHQUFHLENBQUMsSUFBUztRQUNYLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztLQUM5Qzs7Ozs7SUFFRCxNQUFNLENBQUMsSUFBUztRQUNkLHVCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxJQUFJLElBQUksRUFBRTtZQUNSLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2xCO1FBQ0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDdkI7Ozs7O0lBRUQsR0FBRyxDQUFDLEVBQVU7UUFDWixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3pCOzs7OztJQUVELEdBQUcsQ0FBQyxFQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUN6Qjs7OztJQUVELEtBQUs7UUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO0tBQ2xCOzs7WUE1QkosVUFBVTs7OztZQUZGLGVBQWU7Ozs7Ozs7QUNEeEI7Ozs7OztJQVVFLFlBQ1UsVUFDQSxNQUNBO1FBRkEsYUFBUSxHQUFSLFFBQVE7UUFDUixTQUFJLEdBQUosSUFBSTtRQUNKLGNBQVMsR0FBVCxTQUFTO0tBQ2Y7Ozs7OztJQUVKLE9BQU8sQ0FBQyxJQUFrQixFQUFFLE1BQWM7UUFDeEMsT0FBTyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ2xGOzs7OztJQUVPLGlCQUFpQixDQUFDLElBQWtCO1FBQzFDLHFCQUFJLElBQUksQ0FBQztRQUNULElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1lBQ2hDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzlHO2FBQU07WUFDTCxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEU7UUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNoQixLQUFLLHVCQUFNLE1BQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3ZDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sSUFBSSxDQUFDOzs7Ozs7SUFHTixhQUFhLENBQUMsSUFBa0I7UUFDdEMsSUFBSSxJQUFJLENBQUMsSUFBSSxZQUFZLGNBQWMsRUFBRTtZQUN2Qyx1QkFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUUsS0FBSyx1QkFBTSxRQUFRLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQzNDLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMxRSxTQUFTO2lCQUNWO2dCQUNELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxZQUFZLEVBQUU7b0JBQy9DLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZO3dCQUM1RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7aUJBQzNDO3FCQUFNO29CQUNMLHVCQUFNLElBQUksR0FBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQ3RELFFBQVEsSUFBSTt3QkFDVixLQUFLLE1BQU07NEJBQ1QsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7NEJBQy9DLE1BQU07d0JBQ1IsS0FBSyxJQUFJOzRCQUNQLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7NEJBQ2pELE1BQU07d0JBQ1IsS0FBSyxNQUFNLENBQUM7d0JBQ1o7NEJBQ0UsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQ3ZDLE1BQU07cUJBQ1Q7aUJBQ0Y7YUFDRjtZQUNELEtBQUssdUJBQU0sUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUM3Qyx1QkFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNqRixNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3hHO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMzQixPQUFPLE1BQU0sQ0FBQztTQUNmO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDbEI7Ozs7WUFqRUosVUFBVTs7OztZQVBVLFFBQVE7WUFFcEIsZUFBZTtZQUVmLGVBQWU7Ozs7Ozs7QUNKeEI7Ozs7OztJQWdCRSxZQUMyQyxRQUMwQixhQUNsRDtRQUZ3QixXQUFNLEdBQU4sTUFBTTtRQUNvQixnQkFBVyxHQUFYLFdBQVc7UUFDN0QsY0FBUyxHQUFULFNBQVM7UUFFMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1NBQ3ZCO0tBQ0Y7Ozs7OztJQUVELFNBQVMsQ0FBQyxPQUF5QixFQUFFLElBQWlCO1FBQ3BELElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87Z0JBQ25GLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEcsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QscUJBQUksSUFBWSxDQUFDO1FBQ2pCLHFCQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBQzlCLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUNoQyxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNwQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUU7WUFDOUIsS0FBSyx1QkFBTSxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO29CQUM5QyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNoRjthQUNGO1NBQ0Y7UUFDRCxPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUN0QixPQUFPLEVBQUUsT0FBTztTQUNqQixDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUM5QixHQUFHLENBQUMsQ0FBQyxRQUFRO1lBQ1gsSUFBSSxRQUFRLFlBQVksWUFBWSxFQUFFO2dCQUNwQyx1QkFBTSxXQUFXLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3pELEtBQUssdUJBQU0sVUFBVSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ3pDLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFBRTt3QkFDcEMsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7cUJBQy9HO2lCQUNGO2FBQ0Y7WUFDRCxPQUFPLFFBQVEsQ0FBQztTQUNqQixDQUFDLENBQ0gsQ0FBQztLQUNIOzs7WUEvQ0YsVUFBVTs7Ozs0Q0FJTixNQUFNLFNBQUMsWUFBWTt3Q0FDbkIsUUFBUSxZQUFJLE1BQU0sU0FBQyw0QkFBNEI7WUFQM0MsWUFBWTs7Ozs7Ozs7Ozs7O0FDWHJCOzs7QUFHQTs7Ozs7O0lBY0UsT0FBTyxDQUFDLElBQVMsRUFBRSxFQUFVO1FBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDcEI7OztZQWxCRixVQUFVOztxQkFxQkEscUJBQXFCLEdBQUcsSUFBSSxjQUFjLENBQWtCLHVCQUF1QixDQUFDOzs7Ozs7QUN2Qi9GLHFCQUk2QixTQUFRLGFBQWE7Ozs7O0lBQ2hELEdBQUcsQ0FBQyxFQUFVO1FBQ1osT0FBTyxLQUFLLENBQUM7S0FDZDs7Ozs7O0lBRUQsR0FBRyxDQUFDLEVBQVUsRUFBRSxJQUFTO0tBQ3hCOzs7O0lBRUQsS0FBSztLQUNKOzs7OztJQUVELEdBQUcsQ0FBQyxFQUFVO0tBQ2I7Ozs7O0lBRUQsTUFBTSxDQUFDLEVBQVU7S0FDaEI7Ozs7OztJQUVELFFBQVEsQ0FBQyxFQUFVLEVBQUUsS0FBYTtLQUNqQzs7Ozs7O0lBRUQsT0FBTyxDQUFDLElBQVMsRUFBRSxFQUFVO0tBQzVCOzs7WUF0QkYsVUFBVTs7Ozs7OztBQ0hYOzs7Ozs7SUFpQkUsWUFDMkMsUUFDeEIsTUFDK0I7UUFGUCxXQUFNLEdBQU4sTUFBTTtRQUM5QixTQUFJLEdBQUosSUFBSTtRQUMyQixrQkFBYSxHQUFiLGFBQWE7S0FDM0Q7Ozs7OztJQUVKLFNBQVMsQ0FBQyxPQUF5QixFQUFFLElBQWlCO1FBQ3BELHVCQUFNLElBQUksR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyRSxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPO2dCQUNuRixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3RHLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUM3QjtRQUNELHVCQUFNLFNBQVMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLGVBQWUsQ0FBQztRQUM1SCxxQkFBSSxLQUFVLENBQUM7UUFDZixLQUFLLHVCQUFNLFlBQVksSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQzdDLElBQUksWUFBWSxZQUFZLFNBQVMsRUFBRTtnQkFDckMsS0FBSyxHQUFHLFlBQVksQ0FBQztnQkFDckIsTUFBTTthQUNQO1NBQ0Y7UUFDRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO1lBQzVCLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkIsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25CLE9BQU8sRUFBRSxDQUFDLElBQUksWUFBWSxDQUFDO2dCQUN6QixJQUFJLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQ3JCLE1BQU0sRUFBRSxHQUFHO2dCQUNYLEdBQUcsRUFBRSxPQUFPLENBQUMsR0FBRzthQUNqQixDQUFDLENBQUMsQ0FBQztTQUNMO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDOUIsR0FBRyxDQUFDLENBQUMsUUFBMkI7WUFDOUIsSUFBSSxRQUFRLENBQUMsSUFBSSxFQUFFO2dCQUNqQix1QkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLElBQUksRUFBRTtvQkFDUixLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO3dCQUN0QixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN2QztpQkFDRjthQUNGO1NBQ0YsQ0FBQyxDQUNILENBQUM7S0FDSDs7O1lBL0NGLFVBQVU7Ozs7NENBSU4sTUFBTSxTQUFDLFlBQVk7WUFUVSxlQUFlO3dDQVc1QyxNQUFNLFNBQUMscUJBQXFCOzs7Ozs7O0FDcEJqQyx5QkFNaUMsU0FBUSxhQUFhOzs7OztJQU1wRCxZQUNtQixXQUN3QjtRQUV6QyxLQUFLLEVBQUUsQ0FBQztRQUhTLGNBQVMsR0FBVCxTQUFTO1FBQ2UsV0FBTSxHQUFOLE1BQU07cUJBTkosRUFBRTt1QkFFQSxFQUFFO0tBT2hEOzs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBVSxFQUFFLElBQVM7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDN0I7Ozs7SUFFRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztLQUN4Qjs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBVTtRQUNaLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDL0I7Ozs7O0lBRUQsTUFBTSxDQUFDLEVBQVU7UUFDZixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUMzQjs7Ozs7O0lBRUQsUUFBUSxDQUFDLEVBQVUsRUFBRSxLQUFhO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQzFCOzs7OztJQUVELEdBQUcsQ0FBQyxFQUFVO1FBQ1osdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM5RSxPQUFPLElBQUksS0FBSyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUM7S0FDNUQ7OztZQXJDRixVQUFVOzs7O1lBSEYsZUFBZTs0Q0FZbkIsTUFBTSxTQUFDLFlBQVk7Ozs7Ozs7QUNaeEI7Ozs7OztJQUlFLFNBQVMsQ0FBQyxRQUEyQixFQUFFLElBQWE7UUFDbEQsdUJBQU0sQ0FBQyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDckIsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVDLENBQUMsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDNUIsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDWixPQUFPLENBQUMsQ0FBQztLQUNWOzs7OztJQUVELFFBQVEsQ0FBQyxJQUFZO1FBQ25CLE9BQU8sSUFBSSxLQUFLLFlBQVksQ0FBQztLQUM5QjtDQUNGOzs7Ozs7QUNoQkQ7Ozs7OztJQUtFLFNBQVMsQ0FBQyxRQUEyQixFQUFFLElBQWE7UUFDbEQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNwQzs7Ozs7SUFFRCxRQUFRLENBQUMsSUFBWTtRQUNuQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUsscUJBQXFCLENBQUM7S0FDckQ7Ozs7Ozs7SUFFTyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQVUsRUFBRSxVQUFvQjtRQUNwRCx1QkFBTSxDQUFDLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsVUFBVSxHQUFHLFVBQVUsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssa0JBQWtCLENBQUM7UUFDbEUsdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3RILElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRTtZQUNoQix1QkFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQztZQUMzQyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDbkYsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ3RCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDakQsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3ZELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDL0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUNoQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzVCO2FBQ0Y7WUFDRCxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDO1lBQ3hELHVCQUFNLFNBQVMsR0FBRyxhQUFhLENBQUM7WUFDaEMscUJBQUksT0FBTyxDQUFDO1lBQ1osT0FBTyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQyxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25ELENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ1osS0FBSyx1QkFBTSxNQUFNLElBQUksS0FBSyxFQUFFO2dCQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDbkM7U0FDRjthQUFNO1lBQ0wsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNaLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDL0MsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDZjtpQkFBTTtnQkFDTCxLQUFLLHVCQUFNLEdBQUcsSUFBSSxJQUFJLEVBQUU7b0JBQ3RCLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ3pELFNBQVM7cUJBQ1Y7b0JBQ0QsdUJBQU0sWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUM1QyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLFlBQVksSUFBSSxXQUFXLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbkgsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsWUFBWSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDdEc7YUFDRjtTQUNGO1FBQ0QsT0FBTyxDQUFDLENBQUM7O0NBRVo7Ozs7OztBQzVERDs7Ozs7O0lBS0UsU0FBUyxDQUFDLFFBQTJCLEVBQUUsSUFBWTtRQUNqRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUNqSDs7Ozs7SUFFRCxRQUFRLENBQUMsSUFBWTtRQUNuQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssa0JBQWtCLENBQUM7S0FDbEQ7Ozs7Ozs7O0lBRU8sT0FBTyxDQUFDLElBQUksRUFBRSxJQUFTLEVBQUUsVUFBbUIsRUFBRSxPQUFxQjtRQUN6RSx1QkFBTSxDQUFDLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUMxQix1QkFBTSxjQUFjLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQztRQUN0Ryx1QkFBTSxJQUFJLEdBQUcsY0FBYyxJQUFJLFVBQVUsR0FBRyxjQUFjLEdBQUcsY0FBYyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLFNBQVMsQ0FBQztRQUMvRyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNkLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRTtZQUNoQixDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztZQUNuRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUN4RCxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUMxRCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUN0RCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLFNBQVMsQ0FBQztZQUN0RCxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO2dCQUNoQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7YUFDckI7WUFDRCxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDckQsdUJBQU0sU0FBUyxHQUFHLGFBQWEsQ0FBQztZQUNoQyxxQkFBSSxPQUFPLENBQUM7WUFDWixPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9DLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkQsQ0FBQyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDWixLQUFLLHVCQUFNLE1BQU0sSUFBSSxJQUFJLEVBQUU7Z0JBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQzthQUNsRDtTQUNGO2FBQU07WUFDTCxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ1osS0FBSyx1QkFBTSxHQUFHLElBQUksSUFBSSxFQUFFO2dCQUN0Qix1QkFBTSxZQUFZLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxJQUFJLElBQUksWUFBWSxJQUFJLFdBQVcsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuSCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxXQUFXLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxZQUFZLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3RHO1NBQ0Y7UUFDRCxPQUFPLENBQUMsQ0FBQzs7Q0FFWjs7Ozs7Ozs7Ozs7QUNyREQsK0JBVXVDLFNBQVEsYUFBYTs7Ozs7OztJQUkxRCxZQUNVLFdBQ0EsV0FDQSxNQUNzQjtRQUU5QixLQUFLLEVBQUUsQ0FBQztRQUxBLGNBQVMsR0FBVCxTQUFTO1FBQ1QsY0FBUyxHQUFULFNBQVM7UUFDVCxTQUFJLEdBQUosSUFBSTtRQUNrQixXQUFNLEdBQU4sTUFBTTsyQkFOaEIsZUFBZTtRQVNuQyx1QkFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLENBQUM7UUFDeEQsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2xGLHVCQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMvQyx1QkFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsU0FBUyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQy9ELElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDakI7U0FDRixDQUFDLENBQUM7S0FDSjs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBVTtRQUNaLEVBQUUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsRSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDaEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3hEO0tBQ0Y7Ozs7OztJQUVELEdBQUcsQ0FBQyxFQUFVLEVBQUUsSUFBUztRQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDNUI7Ozs7OztJQUVELFFBQVEsQ0FBQyxFQUFVLEVBQUUsS0FBYTtRQUNoQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztLQUMvRDs7Ozs7SUFFRCxHQUFHLENBQUMsRUFBVTtRQUNaLEVBQUUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNsRSx1QkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDdkYsdUJBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDeEMsT0FBTyxDQUFDLENBQUMsU0FBUyxJQUFJLFFBQVEsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0tBQ2xGOzs7OztJQUVELE1BQU0sQ0FBQyxFQUFVO1FBQ2YsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQy9DLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDekQsWUFBWSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUMvRCxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzdELFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUMxQjs7OztJQUVELEtBQUs7UUFDSCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ25GLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDNUIsQ0FBQyxDQUFDO0tBQ0o7Ozs7Ozs7SUFFTyxLQUFLLENBQUMsRUFBVSxFQUFFLElBQVMsRUFBRSxRQUFpQjtRQUNwRCx1QkFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMscUJBQUksTUFBVyxDQUFDO1FBQ2hCLElBQUksSUFBSSxZQUFZLGNBQWMsRUFBRTtZQUNsQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ1osS0FBSyx1QkFBTSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUN2Qix1QkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDbkM7WUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDbkY7YUFBTTtZQUNMLHVCQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDdEIsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNaLEtBQUssdUJBQU0sR0FBRyxJQUFJLElBQUksRUFBRTtnQkFDdEIscUJBQUksUUFBYSxDQUFDO2dCQUNsQixJQUFJLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLFFBQVEsRUFBRTtvQkFDakMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2lCQUNyQztnQkFDRCxJQUFJLFFBQVEsRUFBRTtvQkFDWixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM1QyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDakM7cUJBQU07b0JBQ0wsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDekI7YUFDRjtZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsRUFBRSxFQUFFLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlGLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztTQUN6RjtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUNwRixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzs7Ozs7SUFHaEIsT0FBTyxDQUFDLEVBQVU7UUFDeEIsdUJBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDckUsdUJBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNsRCx1QkFBTSxRQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNwQyxJQUFJLGNBQWMsRUFBRTtZQUNsQixRQUFRLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUMzQixRQUFRLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQztZQUMvQixRQUFRLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNELFFBQVEsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ25CLEtBQUssdUJBQU0sTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2FBQzFDO1NBQ0Y7YUFBTTtZQUNMLFFBQVEsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbkMsUUFBUSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUM3RixRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNyQix1QkFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBYSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDM0YsSUFBSSxVQUFVLEVBQUU7Z0JBQ2QsS0FBSyx1QkFBTSxHQUFHLElBQUksVUFBVSxFQUFFO29CQUM1QixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQ3BEO2FBQ0Y7U0FDRjtRQUNELE9BQU8sUUFBUSxDQUFDOzs7Ozs7SUFHVixpQkFBaUIsQ0FBQyxFQUFVO1FBQ2xDLHVCQUFNLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNyRixJQUFJLElBQUksRUFBRTtZQUNSLHVCQUFNLElBQUksR0FBRyxJQUFJLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNwQyxLQUFLLHVCQUFNLEdBQUcsSUFBSSxJQUFJLEVBQUU7Z0JBQ3RCLElBQUksR0FBRyxLQUFLLE9BQU8sRUFBRTtvQkFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDdkI7YUFDRjtZQUNELE9BQU8sSUFBSSxDQUFDO1NBQ2I7Ozs7OztJQUdLLFlBQVksQ0FBQyxFQUFVO1FBQzdCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDOzs7Ozs7SUFHekUsWUFBWSxDQUFDLEVBQVU7UUFDN0IsT0FBTyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDOzs7Ozs7SUFHcEUsZUFBZSxDQUFDLEVBQVU7UUFDaEMsT0FBTyxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxHQUFHLEVBQUUsQ0FBQyxDQUFDOzs7O1lBOUl4RSxVQUFVOzs7O1lBUEYsZUFBZTtZQUNmLFlBQVk7WUFFSSxlQUFlOzRDQWFuQyxNQUFNLFNBQUMsWUFBWTs7Ozs7Ozs7Ozs7Ozs7O0FDZnhCO0NBYUM7Ozs7Ozs7OztBQ2REO0NBS0M7Ozs7OztBQ05EOzt5QkFJc0IsbUJBQW1COzs7OztJQUV2QyxHQUFHO1FBQ0QsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUM3Qzs7Ozs7SUFFRCxHQUFHLENBQUMsS0FBYTtRQUNmLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztLQUM3Qzs7OztJQUVELE1BQU07UUFDSixZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztLQUN6Qzs7OztJQUVELEtBQUs7UUFDSCxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQztLQUNyRDs7O1lBbEJGLFVBQVU7Ozs7Ozs7QUNIWDs7Ozs7SUFjRSxZQUFtQixJQUFpQixFQUFrQztRQUFuRCxTQUFJLEdBQUosSUFBSSxDQUFhO1FBQWtDLFdBQU0sR0FBTixNQUFNO0tBQWU7Ozs7OztJQUUzRixTQUFTLENBQUMsT0FBeUIsRUFBRSxJQUFpQjtRQUNwRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUNqSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDN0I7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDMUIsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ3RCLFVBQVUsRUFBRTtvQkFDVixhQUFhLEVBQUUsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFO2lCQUNoRDthQUNGLENBQUMsQ0FBQztTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQzdCOzs7WUFqQkYsVUFBVTs7OztZQUhGLFdBQVc7NENBTXFCLE1BQU0sU0FBQyxZQUFZOzs7Ozs7O0FDZDVEOzs7Ozs7SUFZRSxZQUNVLE1BQ0EsT0FDd0I7UUFGeEIsU0FBSSxHQUFKLElBQUk7UUFDSixVQUFLLEdBQUwsS0FBSztRQUNtQixXQUFNLEdBQU4sTUFBTTtLQUNwQzs7OztJQUVKLFFBQVE7UUFDTixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7S0FDekI7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQWE7UUFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDdkI7Ozs7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztLQUNyQjs7OztJQUVELFVBQVU7UUFDUixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7S0FDM0I7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBT0EsSUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0tBQ3pDOzs7Ozs7SUFFRCxNQUFNLENBQUMsUUFBZ0IsRUFBRSxRQUFnQjtRQUN2QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFO1lBQ3pDLEtBQUssRUFBRSxRQUFRO1lBQ2YsUUFBUSxFQUFFLFFBQVE7U0FDbkIsQ0FBQyxDQUFDLElBQUksQ0FDTCxHQUFHLENBQUMsQ0FBQyxJQUFTO1lBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0IsQ0FBQyxDQUNILENBQUM7S0FDSDs7O1lBdENGLFVBQVU7Ozs7WUFSRixVQUFVO1lBTVYsd0JBQXdCOzRDQVE1QixNQUFNLFNBQUMsWUFBWTs7Ozs7Ozs7Ozs7O0FDZnhCOzs7O0FBUUEsa0JBQTBCLE9BQXFCLEVBQUU7SUFFL0MsT0FBTyxDQUFDLE1BQWdCO1FBQ3RCLHVCQUFNLElBQUksR0FBRyxXQUFXLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDckMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ3pCLENBQUM7Q0FDSDs7Ozs7O0FDZkQ7Ozs7QUFRQSxrQkFBMEIsT0FBcUIsRUFBRTtJQUUvQyxPQUFPLENBQUMsTUFBVyxFQUFFLEdBQVc7UUFDOUIsdUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSyxDQUFDO1FBQzdDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUksS0FBSyxDQUFDO0tBQzFELENBQUM7Q0FDSDs7Ozs7O0FDZkQ7Ozs7O0FBR0EscUJBQTRCLE1BQWdCLEVBQUUsSUFBYTtJQUV6RCxPQUFPLENBQUMsTUFBZ0I7UUFDdEIsdUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksR0FBRyxHQUFHQyxNQUFnQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdEgsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO0tBQ2xDLENBQUM7Q0FDSDs7Ozs7O0FDVkQ7Ozs7O0FBRUEsZ0JBQXVCLFVBQWtCLEVBQUUsTUFBZ0I7SUFFekQsT0FBTyxDQUFDLE1BQVcsRUFBRSxHQUFXLEVBQUUsUUFBYSxTQUFTO1FBQ3RELFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNoRSxJQUFJLEVBQUUsVUFBVTtZQUNoQixLQUFLLEVBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsY0FBYyxPQUFRLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQy9ELE1BQU0sRUFBRSxNQUFNLElBQUksS0FBSztZQUN2QixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUMsQ0FBQztLQUNKLENBQUM7Q0FDSDs7Ozs7O0FDWkQ7Ozs7QUFFQSxpQkFBeUIsT0FBMkQ7SUFFbEYsT0FBTyxDQUFDLE1BQVc7UUFDakIsdUJBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0RCxLQUFLLHVCQUFNLE1BQU0sSUFBSSxPQUFPLEVBQUU7WUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTtnQkFDakIsS0FBSyxFQUFFLE1BQU0sTUFBTSxDQUFDLEtBQUs7Z0JBQ3pCLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTSxJQUFJLEtBQUs7Z0JBQzlCLE9BQU8sRUFBRSxLQUFLO2FBQ2YsQ0FBQyxDQUFDO1NBQ0o7S0FDRixDQUFDO0NBQ0g7Ozs7OztBQ2ZEOzs7QUFFQTtJQUVFLE9BQU8sQ0FBQyxNQUFXLEVBQUUsTUFBVyxTQUFTLEVBQUUsUUFBYSxTQUFTO1FBQy9ELFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsTUFBVztZQUN0RSxPQUFPLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsY0FBYyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9FLENBQUM7S0FDSCxDQUFDO0NBQ0g7Ozs7OztBQ1REOzs7OztBQUVBLHFCQUE2QixJQUFjLEVBQUUsSUFBYTtJQUV4RCxPQUFPLENBQUMsTUFBVyxFQUFFLEdBQVcsRUFBRSxLQUFVO1FBQzFDLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHO1lBQ3ZFLElBQUksRUFBRSxXQUFXLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO1lBQzVDLElBQUksRUFBRSxJQUFJLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFO1NBQ3RGLENBQUM7S0FDSCxDQUFDO0NBQ0g7Ozs7OztBQ1ZEOzs7QUFFQTtJQUVFLE9BQU8sQ0FBQyxNQUFXLEVBQUUsR0FBVztRQUM5Qix1QkFBTSxJQUFJLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsQ0FBQztLQUNmLENBQUM7Q0FDSDs7Ozs7Ozs7Ozs7QUNSRDs7O0FBS0E7Ozs7OztJQVNFLFlBQXNCLE9BQW1CLEVBQUUsT0FBTyxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsQ0FBQztRQUE1QyxZQUFPLEdBQVAsT0FBTyxDQUFZO3NCQVBmLEVBQUU7dUJBRVYsS0FBSztRQU1yQixJQUFJLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDaEQsSUFBSSxDQUNILE1BQU0sR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxFQUFFLEVBQ3RDLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFDckIsb0JBQW9CLENBQUMsQ0FBQyxFQUFTLEVBQUUsRUFBUyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQzFFLEdBQUcsQ0FBQyxDQUFDLE1BQWEsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQzlDLFFBQVEsQ0FBQyxDQUFDLElBQUk7WUFDWixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNwQixPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQ3pDLEdBQUcsQ0FBQyxDQUFDLENBQWtCO2dCQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEIsQ0FBQyxDQUNILENBQUM7U0FDSCxDQUFDLENBQ0gsQ0FBQztLQUNMOzs7OztJQUVELEtBQUssQ0FBQyxJQUFJO1FBQ1IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQzFCOzs7O0lBRUQsSUFBSSxJQUFJO1FBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0tBQ3hCOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1gsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0tBQ3JCOzs7OztJQUVTLG9CQUFvQixDQUFDLENBQUMsSUFBSSxDQUFRO1FBQzFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDbkM7Ozs7OztJQUVTLGFBQWEsQ0FBQyxFQUFTLEVBQUUsRUFBUztRQUMxQyx1QkFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxNQUFNLENBQUM7UUFDNUQsS0FBSyxxQkFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25DLElBQUksQ0FBQyxDQUFDLEdBQUcsTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDN0QsSUFBSSxDQUFDLElBQUksTUFBTSxFQUFFO29CQUNmLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztpQkFDL0I7Z0JBQ0QsT0FBTyxLQUFLLENBQUM7YUFDZDtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7S0FDYjs7Ozs7SUFFUyxTQUFTLENBQUMsTUFBYTtRQUMvQix1QkFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDekMsdUJBQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM3Qyx1QkFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDaEMsSUFBSSxDQUFDLEtBQUssU0FBUyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDN0MsSUFBSSxPQUFPLENBQUMsS0FBSyxTQUFTLEVBQUU7b0JBQzFCLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxHQUFHLE9BQU8sQ0FBQztpQkFDMUI7Z0JBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM3QjtTQUNGLENBQUMsQ0FBQztRQUNILE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUMvQzs7OztJQUVTLFdBQVc7UUFDbkIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJO1lBQ3hDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLGVBQWUsQ0FBTSxTQUFTLENBQUMsQ0FBQztTQUNoRSxDQUFDLENBQUM7S0FDSjtDQUNGOzs7Ozs7QUNsRkQ7OztBQUdBLDBCQUEyQyxTQUFRLE1BQU07Ozs7SUFJdkQsUUFBUTtRQUNOLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ2pDOzs7O0lBRUQsT0FBTztRQUNMLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ2pEOzs7OztJQUVTLG9CQUFvQixDQUFDLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBUTtRQUNwRCxPQUFPLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFHLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDakY7Ozs7SUFFUyxXQUFXO1FBQ25CLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDM0QsdUJBQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNyQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN2QyxPQUFPLFFBQVEsQ0FBQztLQUNqQjtDQUNGOzs7Ozs7QUN6QkQ7OztBQU1BLGdCQUFpQyxTQUFRLE1BQU07Ozs7SUFJN0MsS0FBSztRQUNILElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUMvQyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7S0FDbEQ7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUFFO1lBQ2xELE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztLQUNyRDs7OztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDOUMsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0tBQ2pEOzs7O0lBRUQsSUFBSTtRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUM5QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7S0FDakQ7Ozs7SUFFRCxRQUFRO1FBQ04sT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7S0FDbEQ7Ozs7SUFFRCxXQUFXO1FBQ1QsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7S0FDckQ7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDakQ7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7S0FDakQ7Ozs7O0lBRVMsb0JBQW9CLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFRO1FBQ2hELE9BQU8sSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0tBQ25GOzs7O0lBRVMsV0FBVztRQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksZUFBZSxDQUFTLFNBQVMsQ0FBQyxDQUFDO1FBQzFELHVCQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDckMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkMsT0FBTyxRQUFRLENBQUM7S0FDakI7Q0FDRjs7Ozs7Ozs7Ozs7QUNoRUQ7Ozs7O0lBMkNFLE9BQU8sTUFBTSxDQUFDLE1BQWtCO1FBQzlCLE9BQU87WUFDTCxRQUFRLEVBQUUsWUFBWTtZQUN0QixTQUFTLEVBQUU7Z0JBQ1Q7b0JBQ0UsT0FBTyxFQUFFLFlBQVk7b0JBQ3JCLFFBQVEsRUFBRSxNQUFNO2lCQUNqQjtnQkFDRCxhQUFhO2dCQUNiLGVBQWU7Z0JBQ2Y7b0JBQ0UsT0FBTyxFQUFFLHFCQUFxQjtvQkFDOUIsUUFBUSxFQUFFLHlCQUF5QjtvQkFDbkMsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLHFCQUFxQjtvQkFDOUIsUUFBUSxFQUFFLG1CQUFtQjtvQkFDN0IsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLHFCQUFxQjtvQkFDOUIsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxhQUFhO29CQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxlQUFlO2lCQUMxQztnQkFDRDtvQkFDRSxPQUFPLEVBQUUsd0JBQXdCO29CQUNqQyxRQUFRLEVBQUUsTUFBTSxDQUFDLGFBQWEsSUFBSSxvQ0FBb0M7aUJBQ3ZFO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxXQUFXO29CQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxjQUFjO2lCQUMvQztnQkFDRDtvQkFDRSxPQUFPLEVBQUUsaUJBQWlCO29CQUMxQixRQUFRLEVBQUUsZUFBZTtvQkFDekIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtvQkFDMUIsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtvQkFDMUIsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsS0FBSyxFQUFFLElBQUk7aUJBQ1o7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtvQkFDMUIsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSw0QkFBNEI7b0JBQ3JDLFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSw0QkFBNEI7b0JBQ3JDLFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSw0QkFBNEI7b0JBQ3JDLFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNELGVBQWU7Z0JBQ2YsWUFBWTtnQkFDWixVQUFVO2FBQ1g7U0FDRixDQUFDO0tBQ0g7Ozs7O0lBQ0QsT0FBTyx3QkFBd0IsQ0FBQyxNQUFrQjtRQUNoRCxPQUFPO1lBQ0wsUUFBUSxFQUFFLFlBQVk7WUFDdEIsU0FBUyxFQUFFO2dCQUNUO29CQUNFLE9BQU8sRUFBRSxZQUFZO29CQUNyQixRQUFRLEVBQUUsTUFBTTtpQkFDakI7Z0JBQ0QsYUFBYTtnQkFDYixlQUFlO2dCQUNmO29CQUNFLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLFFBQVEsRUFBRSx5QkFBeUI7b0JBQ25DLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLFFBQVEsRUFBRSxtQkFBbUI7b0JBQzdCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLFFBQVEsRUFBRSxlQUFlO29CQUN6QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsYUFBYTtvQkFDdEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxLQUFLLElBQUksZUFBZTtpQkFDMUM7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLHdCQUF3QjtvQkFDakMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxhQUFhLElBQUksb0NBQW9DO2lCQUN2RTtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsV0FBVztvQkFDcEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxXQUFXLElBQUksY0FBYztpQkFDL0M7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjtvQkFDMUIsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLEtBQUssRUFBRSxJQUFJO2lCQUNaO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxpQkFBaUI7b0JBQzFCLFFBQVEsRUFBRSxjQUFjO29CQUN4QixLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsNEJBQTRCO29CQUNyQyxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyxLQUFLLEVBQUUsSUFBSTtpQkFDWjtnQkFDRCxlQUFlO2dCQUNmLFlBQVk7Z0JBQ1osVUFBVTthQUNYO1NBQ0YsQ0FBQztLQUNIOzs7WUF2SkYsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRTtvQkFDUCxnQkFBZ0I7aUJBQ2pCO2FBQ0Y7Ozs7Ozs7Ozs7Ozs7OzsifQ==