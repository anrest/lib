export declare function Subresource(type: Function, path?: string): (target: any, key: string, value: any) => void;
