export declare function Header(headerName: string, append?: boolean): (target: any, key: string, value?: any) => void;
