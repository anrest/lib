export interface ResourceInfo {
    path?: string;
    name?: string;
    cache?: Function;
}
export declare function Resource(info?: ResourceInfo): (target: Function) => void;
