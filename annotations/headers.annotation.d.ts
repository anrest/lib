export declare function Headers(headers: {
    name: string;
    value: string;
    append?: boolean;
}[]): (target: any) => void;
