export interface PropertyInfo {
    collection?: boolean;
    excludeWhenSaving?: boolean;
    type?: any;
}
export declare function Property(info?: PropertyInfo): (target: any, key: string) => void;
