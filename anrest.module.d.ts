import { ModuleWithProviders } from '@angular/core';
import 'reflect-metadata';
import { ApiConfig } from './core';
export declare class AnRestModule {
    static config(config?: ApiConfig): ModuleWithProviders;
    static configWithoutNormalizers(config?: ApiConfig): ModuleWithProviders;
}
