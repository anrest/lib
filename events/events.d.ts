import { InjectionToken } from '@angular/core';
import { HttpParams } from '@angular/common/http';
export interface Event {
    entity(): Function;
    data(): any;
}
export declare class BaseEvent implements Event {
    _data?: any;
    protected readonly type: Function;
    constructor(data: any, type?: Function);
    data(): any;
    entity(): Function;
}
export declare class BeforeGetEvent extends BaseEvent {
    private readonly _filter;
    constructor(path: string, type?: Function, filter?: HttpParams);
    entity(): Function;
    filter(): HttpParams;
}
export declare class AfterGetEvent extends BaseEvent {
}
export declare class BeforeSaveEvent extends BaseEvent {
}
export declare class AfterSaveEvent extends BaseEvent {
}
export declare class BeforeRemoveEvent extends BaseEvent {
}
export declare class AfterRemoveEvent extends BaseEvent {
}
export interface EventListener {
    handle(event: Event): any;
}
export declare let ANREST_HTTP_EVENT_LISTENERS: InjectionToken<EventListener[]>;
