import { Event, EventListener } from './events';
export declare class EventsService {
    private readonly handlers;
    constructor(handlers: EventListener[]);
    handle(event: Event): Event;
}
